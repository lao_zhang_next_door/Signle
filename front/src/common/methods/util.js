/**
 *  data 需要传入的去除null值的对象或者值
 *  defaultStr 将null值转为该字符串, 不传默认为 空字符串 ''
 */
export function removeNull(data, defaultStr = '') {
  // 普通数据类型
  if (typeof data !== 'object' || data == null) {
    if ((data == null || data === 'null')) {
      return defaultStr
    } else {
      return data
    }
  }
  // 引用数据类型
  for (const v in data) {
    if (data[v] == null || data[v] === 'null') {
      data[v] = defaultStr
    }
    if (typeof data[v] === 'object') {
      removeNull(data[v])
    }
  }
}

/**
 * uuid生成
 * @param {*} len
 * @param {*} radix
 * @returns
 */
export function uuid(len, radix) {
  var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')
  var uuid = []; var i
  radix = radix || chars.length

  if (len) {
    // Compact form
    for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix]
  } else {
    // rfc4122, version 4 form
    var r
    // rfc4122 requires these characters
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
    uuid[14] = '4'
    // Fill in random data.  At i==19 set the high bits of clock sequence as
    // per rfc4122, sec. 4.1.5
    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | Math.random() * 16
        uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r]
      }
    }
  }
  return uuid.join('')
}
