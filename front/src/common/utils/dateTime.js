const dateTime = {
  /**
     * 获取当前时间戳(毫秒数)
     */
  getTimeStamp: function() {
    return Date.now()
  },
  /**
     * 时间格式替换
     * changeDate(date,'yyyy-MM-dd');
     * isSJC : 是否时间戳
     */
  changeDate: function(val, format) {
    if (val) {
      var t = new Date(val)
      var tf = function(i) {
        return (i < 10 ? '0' : '') + i
      }
      return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a) {
        switch (a) {
          case 'yyyy':
            return tf(t.getFullYear())
          case 'MM':
            return tf(t.getMonth() + 1)
          case 'mm':
            return tf(t.getMinutes())
          case 'dd':
            return tf(t.getDate())
          case 'HH':
            return tf(t.getHours())
          case 'ss':
            return tf(t.getSeconds())
        }
      })
    }
  }
}

export default dateTime

