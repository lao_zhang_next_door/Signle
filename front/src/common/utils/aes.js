import CryptoJS from 'crypto-js'

export default {
  // iv:[97, 97, 98, 98, 99, 99, 100, 100, 101, 101, 102, 102, 103, 103, 104, 104],
  // iv:"aabbccddeeffgghh",
  key: 'sinoway1234',
  // 随机生成指定数量的16进制key
  // generatekey(num) {
  //     let library = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  //     let key = "";
  //     for (var i = 0; i < num; i++) {
  //         let randomPoz = Math.floor(Math.random() * library.length);
  //         key += library.substring(randomPoz, randomPoz + 1);
  //     }
  //     return key;
  // },

  // 加密
  encrypt(word, keyStr) {
    keyStr = keyStr || this.key // 判断是否存在ksy，不存在就用定义好的key
    const mode = CryptoJS.mode.ECB
    const pad = CryptoJS.pad.Pkcs7
    var keyHex = CryptoJS.enc.Utf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    var wordHex = CryptoJS.enc.Utf8.parse(word)
    var encrypted = CryptoJS.AES.encrypt(wordHex, keyHex, {
      mode: mode,
      padding: pad
    })
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64)
  },
  // 解密
  decrypt(word, keyStr) {
    keyStr = keyStr || this.key
    const mode = CryptoJS.mode.ECB
    const pad = CryptoJS.pad.Pkcs7
    var keyHex = CryptoJS.enc.Utf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    var decrypted = CryptoJS.AES.decrypt(word, keyHex, {
      mode: mode,
      padding: pad
    })
    return CryptoJS.enc.Utf8.stringify(decrypted).toString()
  },

  // 加密
  encryptMD5(word) {
    return CryptoJS.MD5(word).toString().toLowerCase()
  }
}

/**
* 滑动验证
* @word 要加密的内容
* @keyWord String  服务器随机返回的关键字
*  */
export function captchaAesEncrypt(word, keyWord) {
  keyWord = keyWord || this.key
  var key = CryptoJS.enc.Utf8.parse(keyWord)
  var srcs = CryptoJS.enc.Utf8.parse(word)
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  return encrypted.toString()
}
