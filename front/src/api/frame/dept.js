import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameDept/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameDept/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameDept/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameDept/update',
    method: 'put',
    data
  })
}

export function treeData(data) {
  return request({
    url: '/frame/FrameDept/treeData',
    method: 'post',
    data
  })
}

export function selectParentCode(data) {
  return request({
    url: '/frame/FrameDept/selectParentCode',
    method: 'get',
    params: data
  })
}
