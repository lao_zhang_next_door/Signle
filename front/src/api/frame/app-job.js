import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/application/AppJob/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/application/AppJob/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/application/AppJob/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/application/AppJob/update',
    method: 'POST',
    data
  })
}

export function changeTaskRunTime(data) {
  return request({
    url: '/application/AppJob/startJob',
    method: 'post',
    data: data
  })
}
