import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameRole/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameRole/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameRole/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameRole/update',
    method: 'put',
    data
  })
}

export function getAllRole() {
  return request({
    url: '/frame/FrameRole/getAllRole',
    method: 'get'
  })
}

export function getModuleByRoleGuid(data) {
  return request({
    url: '/frame/FrameRole/getModuleByRoleGuid',
    method: 'post',
    params: data
  })
}

