import request from '@/common/utils/request'

/**
 * 根据formRowGuid查询附件列表
 * @param {*} data {formRowGuid:''}
 * @returns
 */
export function getListByFormRowGuid(data) {
  return request({
    url: '/frame/FrameAttach/getListByFormRowGuid',
    method: 'get',
    params: data
  })
}

export function deleteFile(data) {
  return request({
    url: '/frame/FrameAttach/deleteFileByRowGuid',
    method: 'delete',
    params: data
  })
}
