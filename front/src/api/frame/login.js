import request from '@/common/utils/request'

export function login(data) {
  return request({
    url: '/frame/FrameLogin/login',
    method: 'post',
    data
  })
}

export function getUserInfo() {
  return request({
    url: '/frame/FrameLogin/getUserInfo',
    method: 'get'
  })
}

