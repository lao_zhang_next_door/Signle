import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameUserDeptRelation/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameUserDeptRelation/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameUserDeptRelation/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameUserDeptRelation/update',
    method: 'put',
    data
  })
}

