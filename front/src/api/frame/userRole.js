import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameUserRole/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameUserRole/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameUserRole/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameUserRole/update',
    method: 'put',
    data
  })
}
