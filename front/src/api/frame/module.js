import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameModule/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameModule/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameModule/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameModule/update',
    method: 'put',
    data
  })
}

export function treeData(data) {
  return request({
    url: '/frame/FrameModule/treeData',
    method: 'post',
    data
  })
}

export function selectParentCode(data) {
  return request({
    url: '/frame/FrameModule/selectParentCode',
    method: 'get',
    params: data
  })
}

export function allTreeData() {
  return request({
    url: '/frame/FrameModule/allTreeData',
    method: 'get'
  })
}

// 全部路由
export function allTreeDataRoute() {
  return request({
    url: '/frame/FrameModule/allTreeDataRoute',
    method: 'get'
  })
}

