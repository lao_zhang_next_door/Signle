import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameRoleModule/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameRoleModule/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameRoleModule/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameRoleModule/update',
    method: 'put',
    data
  })
}

export function addBatchData(data, roleGuid) {
  return request({
    url: '/frame/FrameRoleModule/addBatch?roleGuid=' + roleGuid,
    method: 'post',
    data
  })
}

