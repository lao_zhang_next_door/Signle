import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameConfig/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameConfig/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameConfig/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameConfig/update',
    method: 'put',
    data
  })
}

export function getSystemConfig(data) {
  return request({
    url: '/frame/FrameConfig/getSystemConfig?configName=' + data,
    method: 'get'
  })
}

export function checkPicCaptcha() {
  return request({
    url: '/frame/FrameConfig/checkPicCaptcha',
    method: 'get'
  })
}

