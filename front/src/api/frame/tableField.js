import request from '@/common/utils/request'

/**
 * 数据列表
 * @param {Object} data
 */
export function listData(data) {
  return request({
    url: '/frame/FormTableField/listData',
    method: 'get',
    params: data
  })
}
/**
 * 删除
 * @param {Object} data
 */
export function deleteData(data) {
  return request({
    url: '/frame/FormTableField/deleteTableField',
    method: 'post',
    data
  })
}
/**
 * 更新
 * @param {Object} data
 */
export function updateData(data) {
  return request({
    url: '/frame/FormTableField/updateTableField',
    method: 'put',
    data
  })
}
/**
 * 新增
 * @param {Object} data
 */
export function addData(data) {
  return request({
    url: '/frame/FormTableField/add',
    method: 'post',
    data
  })
}
