import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameCode/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameCode/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameCode/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameCode/update',
    method: 'put',
    data
  })
}

export function selectByCodeName(data) {
  return request({
    url: '/frame/FrameCode/selectByCodeName',
    method: 'get',
    params: data
  })
}

export function selectCacheByCodeName(data) {
  return request({
    url: '/frame/FrameCode/selectCacheByCodeName',
    method: 'get',
    params: data
  })
}

