import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameCodeValue/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameCodeValue/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameCodeValue/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameCodeValue/update',
    method: 'put',
    data
  })
}

export function treeData(data) {
  return request({
    url: '/frame/FrameCodeValue/treeData',
    method: 'post',
    data
  })
}

export function selectParentCode(data) {
  return request({
    url: '/frame/FrameCodeValue/selectParentCode',
    method: 'get',
    params: data
  })
}
