import request from '@/common/utils/request'

/**
 * 数据列表
 * @param {Object} data
 */
export function listData(data) {
  return request({
    url: '/frame/FormTableInfo/listData',
    method: 'get',
    params: data
  })
}
/**
 * 删除
 * @param {Object} data
 */
export function deleteData(data) {
  return request({
    url: '/frame/FormTableInfo/delete',
    method: 'delete',
    data
  })
}
/**
 * 更新
 * @param {Object} data
 */
export function updateData(data) {
  return request({
    url: '/frame/FormTableInfo/update',
    method: 'put',
    data
  })
}
/**
 * 新增
 * @param {Object} data
 */
export function addData(data) {
  return request({
    url: '/frame/FormTableInfo/add',
    method: 'post',
    data
  })
}
/**
 * 生成代码
 * @param {Object} data
 */
export function generateCode(data) {
  return request({
    url: '/frame/FormTableInfo/generateCode',
    method: 'post',
    params: data,
    responseType: 'blob'
  })
}
