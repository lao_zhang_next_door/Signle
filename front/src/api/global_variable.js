const requiredRules = [value => !!value || '此项必填']
// const requiredRules = v => (v || '').indexOf(' ') < 0 || 'No spaces are allowed'

export default {
  requiredRules
}
