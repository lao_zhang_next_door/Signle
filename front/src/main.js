import Vue from 'vue'

// 修正大部分现代浏览器 默认css
import 'normalize.css/normalize.css'
// global css 全局css
import '@/styles/index.scss'

import '@/plugins/elementui/index'
import vuetify from '@/plugins/vuetify/index'

import App from './App'
import store from './store'
import router from './router'

import '@/icons'
import '@/permission'

// 自定义函数
import { routerto } from '@/common/methods/routerto'
import { autoHeight } from '@/common/methods/autoHeight'
import { customAlert } from '@/common/methods/customAlert'
// 自定义全局变量
import globalVariable from '@/api/global_variable.js'
Vue.prototype.GLOBAL = globalVariable
Vue.prototype.$routerto = routerto
Vue.prototype.$autoHeight = autoHeight
Vue.prototype.$customAlert = customAlert

import deleteAlter from './components/DeletePop'
import datePicker from './components/DatePicker/index'
import searchPicker from './components/DatePicker/search'
import vselectOption from './components/Vuetify/SelectOption/index'
import dialogConfirm from './components/DialogConfirm/index'
import OpenWin from './components/OpenWin/index'
import vradioGroup from '@/components/Vuetify/RadioGroup/index'
import eselectOption from '@/components/ElementUI/SelectOption/index'
import eradioGroup from '@/components/ElementUI/RadioGroup/index'
import euploader from '@/components/ElementUI/upload/ClickUploadComponent'
import vuploaderImg from '@/components/Vuetify/upload/uploadImg'
import avatarUpload from '@/components/ImageCropper/avatar-upload'

Vue.component('DeleteAlter', deleteAlter)
Vue.component('DatePicker', datePicker)
Vue.component('SearchPicker', searchPicker)
Vue.component('VselectOption', vselectOption)
Vue.component('DialogConfirm', dialogConfirm)
Vue.component('OpenWin', OpenWin)
Vue.component('VradioGroup', vradioGroup)
Vue.component('EselectOption', eselectOption)
Vue.component('EradioGroup', eradioGroup)
Vue.component('Euploader', euploader)
Vue.component('VuploaderImg', vuploaderImg)
Vue.component('AvatarUpload', avatarUpload)

// 全局filters
import * as filters from './common/filters/index.js'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])// 插入过滤器名和对应方法
})
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
import { mockXHR } from '../mock'
if (process.env.NODE_ENV === 'production') {
  mockXHR()
}

// 生产模式false  开发模式true
Vue.config.productionTip = true

new Vue({
  el: '#app',
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
