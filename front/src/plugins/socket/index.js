import Vue from 'vue'
import io from 'socket.io-client' // 引入socket.io-client

export default new Vue({
  data: {
    hasRealAlarm: false,
    socket: null,
    nInstance: null
  },
  methods: {
    switchRealAlarm(is) {
      this.hasRealAlarm = is
    },
    initSocket() {
      const _this = this
      const socket = io('http://localhost:9528/socketIo') // 路径
      this.socket = socket
      console.log('初始化：socket', socket)
      this.socket.on('connect', (data) => {
        console.log('open:', data)
      })
      this.socket.on('close', () => {
        console.log('socket连接关闭')
      })

      /* Listeners */
      this.socket.on('alarmToWeb', (data) => {
        // console.log('后端推送的实时告警信息', data);
        console.log('alarmToWeb：socket', data)
      })

      this.socket.on('AccessResponseToWeb', (data) => {
        console.log('AccessResponseToWeb：socket', data)
      })

      // 试用期使用时长
      this.socket.on('lisenceEndToWeb', (data) => {
        console.log('试用期数据', data)
      })

      this.socket.on('freshDataToWeb', (data) => {
        // console.log('设备收到的新数据', data);
        // alert(data);
      })

      _this.$on('close', () => {
        console.log('socket-close')
        this.socket.close()
      })
    }

  }
})
