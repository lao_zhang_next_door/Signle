const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  roles: state => state.user.roles,
  userInfo: state => state.user.userInfo,
  dept: state => state.dept,
  permission_routes: state => state.permission.routes,
  formData: state => {
    return state.code.formData
  },
  getTagsRouter: state => {
    // 获取标签路由信息
    return state.tagrouter.tagsRouter
  },
  getTagsActiveIndex: state => state.tagrouter.activeIndex,
  // getKeepAlivePages: state => state.tagrouter.keepAlivePages
}
export default getters
