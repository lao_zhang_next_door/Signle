import { login, getUserInfo } from '@/api/frame/login'
import { getToken, setToken, removeToken } from '@/common/utils/auth'
import { resetRouter } from '@/router'
import { customAlert } from '@/common/methods/customAlert'

const state = {
  token: getToken(),
  name: '',
  avatar: '',
  roles: [],
  rolesGuids: [],
  deptGuids: [],
  deptNames: [],
  userInfo: {}
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_ROLESGUIDS: (state, rolesGuids) => {
    state.rolesGuids = rolesGuids
  },
  SET_DEPTNAMES: (state, deptNames) => {
    state.deptNames = deptNames
  },
  SET_DEPTGUIDS: (state, deptGuids) => {
    state.deptGuids = deptGuids
  },
  SET_USERINFO: (state, userInfo) => {
    state.userInfo = userInfo
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { loginId, password, captchaData } = userInfo
    return new Promise((resolve, reject) => {
      login({
        loginId: loginId.trim(),
        password: password,
        captchaData: captchaData
      })
        .then((response) => {
          setToken(response.data.token)
          customAlert('登陆成功', 'success')
          resolve(response)
          console.log(response)
        })
        .catch((error) => {
          console.log(error)
          reject(error)
        })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getUserInfo()
        .then((response) => {
          const { data } = response

          if (!data) {
            reject('Verification failed, please Login again.')
          }
          const { roleNameList, userName, roleGuidList, avatar, deptNames, deptGuids } = data
          // roles must be a non-empty array
          if (!roleNameList || roleNameList.length <= 0) {
            reject('getInfo: roleNameList must be a non-null array!')
          }
          commit('SET_ROLES', roleNameList)
          commit('SET_ROLESGUIDS', roleGuidList)
          commit('SET_DEPTNAMES', deptNames)
          commit('SET_DEPTGUIDS', deptGuids)
          commit('SET_NAME', userName)
          commit('SET_USERINFO', data)
          commit('SET_AVATAR', avatar)
          // commit('SET_AVATAR', avatar)

          resolve(data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      commit('SET_TOKEN', '')
      commit('SET_NAME', '')
      commit('SET_AVATAR', '')
      commit('SET_ROLES', [])
      commit('SET_USERINFO', {})
      removeToken()
      resetRouter()
      resolve()
      // logout(state.token).then(() => {
      //   commit('SET_TOKEN', '')
      //   commit('SET_ROLES', [])
      //   removeToken()
      //   resetRouter()
      //   resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
