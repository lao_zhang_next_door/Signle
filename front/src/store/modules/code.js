const state = sessionStorage.getItem('state') ? JSON.parse(sessionStorage.getItem('state')).code : {
  formData: {}
}

const mutations = {
  set_formData: (state, formData) => {
  	state.formData = formData
  }
}

const actions = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

