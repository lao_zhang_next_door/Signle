import { asyncRoutes, constantRoutes } from '@/router'
import { allTreeDataRoute } from '@/api/frame/module'
import { deepCopy } from '@/common/utils/objectCopy.js'

// /**
//  * Use meta.role to determine if the current user has permission
//  * @param roles
//  * @param route
//  */
// function hasPermission(roles, route) {
//   if (route.meta && route.meta.roles) {
//     return roles.some(role => route.meta.roles.includes(role))
//   } else {
//     return true
//   }
// }

/**
 * 使用meta.role确定当前用户是否具有权限
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else if (route.meta.role) {
    return roles.some(role => route.meta.role.includes(role))
  } else {
    return true
  }
}

// /**
//  * Filter asynchronous routing tables by recursion
//  * @param routes asyncRoutes
//  * @param roles
//  */
// export function filterAsyncRoutes(routes, roles) {
//   const res = []

//   routes.forEach(route => {
//     const tmp = { ...route }
//     if (hasPermission(roles, tmp)) {
//       if (tmp.children) {
//         tmp.children = filterAsyncRoutes(tmp.children, roles)
//       }
//       res.push(tmp)
//     }
//   })

//   return res
// }

/**
 * 递归过滤异步路由表
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []
  const _import = require('@/router/_import_' + process.env.NODE_ENV) // 获取组件的方法
  routes.forEach(route => {
    const tmp = { ...route }
    if (tmp.component) {
      tmp.component = _import(tmp.component)
      // if (tmp.component === 'Layout') {
      //   // Layout组件特殊处理
      //   tmp.component = Layout
      // } else {
      //   tmp.component = _import(tmp.component)
      // }
    }
    // 处理true，false字符串
    // if (tmp.hidden) {
    //   tmp.hidden = tmp.hidden === 'true'
    // }
    // if (tmp.meta.breadcrumb) {
    //   tmp.meta.breadcrumb = tmp.meta.breadcrumb === 'true'
    // }
    // if (tmp.meta.noCache) {
    //   tmp.meta.noCache = tmp.meta.noCache === 'true'
    // }
    if (hasPermission(roles, tmp)) {
      if (tmp.children.length && tmp.children.length) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    // state.addRoutes = routes
    state.routes = routes
  }
}

const actions = {
  generateRoutes({ commit }, roleNames) {
    return new Promise((resolve, reject) => {
      // let accessedRoutes
      // if (roles.includes('admin')) {
      //   accessedRoutes = asyncRoutes || []
      // } else {
      //   accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      // }
      // commit('SET_ROUTES', accessedRoutes)
      // resolve(accessedRoutes)

      allTreeDataRoute().then(response => {
        const { data } = response
        // 对自定义路由进行修改
        // 清空原数组
        asyncRoutes.splice(0)
        data.forEach(d => {
          asyncRoutes.unshift(d)
        })
        const accessedRoutes = filterAsyncRoutes(asyncRoutes, roleNames)
        const arr = constantRoutes[0].children.concat(accessedRoutes)

        const routerCp = deepCopy(constantRoutes)

        routerCp[0].children = arr

        console.log(routerCp)

        commit('SET_ROUTES', routerCp)
        resolve(routerCp)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
