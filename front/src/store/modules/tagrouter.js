
const state = {
  tagsRouter: [{
    path: '/dashboard',
    name: '首页',
    meta: {
      keepAlive: true
    }
  }],
  activeIndex: 0
  // keepAlivePages: []
}
const mutations = {
  setRouterTagActive(state, routeItem) {
    // console.log(state.tagsRouter)
    // console.log(routeItem)
    // 设置当前的tag页为活动页
    for (let i = 0; i < state.tagsRouter.length; i++) {
      // let item = state.tagsRouter[i]
      state.tagsRouter[i].isActive = false
      if (state.tagsRouter[i].name === routeItem.name) {
        state.tagsRouter[i].isActive = true
        state.activeIndex = i
      }
    }
  },
  addRoute(state, routeItem) {
    // 选择菜单后，添加至本地路由信息中
    let flog = false
    for (let i = 0; i < state.tagsRouter.length; i++) {
      // let item = state.tagsRouter[i]
      if (state.tagsRouter[i].name === routeItem.name) {
        flog = true
        break
      }
    }
    if (!flog) {
      state.tagsRouter.push({
        path: routeItem.path,
        name: routeItem.name,
        meta: routeItem.meta
      })
      // if (routeItem.meta.keepAlive && routeItem.meta.routerName !== null) {
      //   state.keepAlivePages.push(routeItem.meta.routerName)
      // }
      // console.log(state.keepAlivePages)
    }
  },
  delRoute(state, params) {
    // 删除tags路由项
    if (state.tagsRouter.length !== 0) {
      // let itemIndex = 0
      for (let i = 0; i < state.tagsRouter.length; i++) {
        // let item = state.tagsRouter[i]
        if (state.tagsRouter[i].meta.menuKey === params.item.meta.menuKey) {
          //
          if (state.activeIndex === i) {
            state.activeIndex--
          }
          // itemIndex = i
          state.tagsRouter.splice(i, 1)
          break
        }
      }
      // for (let j = 0; j < state.keepAlivePages.length; j++) {
      //   if (state.keepAlivePages[j] === params.item.meta.routerName) {
      //     //
      //     // itemIndex = i
      //     state.keepAlivePages.splice(j, 1)
      //     break
      //   }
      // }
    }
  },
  cleanRoute(state) {
    // 全部清除
    state.tagsRouter.length = 0
    state.tagsRouter.push({
      path: '/dashboard',
      name: '首页',
      meta: {
        keepAlive: true
      }
    })
    state.activeIndex = 0
    // state.keepAlivePages.length = 0
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
