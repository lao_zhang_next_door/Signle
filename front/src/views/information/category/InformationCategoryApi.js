import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/InformationCategory/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/InformationCategory/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/InformationCategory/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/InformationCategory/update',
    method: 'put',
    data
  })
}

