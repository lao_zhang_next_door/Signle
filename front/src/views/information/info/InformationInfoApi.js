import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/InformationInfo/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/InformationInfo/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/InformationInfo/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/InformationInfo/update',
    method: 'put',
    data
  })
}

/**
 * 信息发布
 * @param {} data
 * @returns
 */
export function deliveryInfo(data) {
  return request({
    url: '/frame/InformationInfo/deliveryInfo',
    method: 'post',
    data
  })
}

