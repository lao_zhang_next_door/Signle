import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/XnwClient/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/XnwClient/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/XnwClient/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/XnwClient/update',
    method: 'put',
    data
  })
}

