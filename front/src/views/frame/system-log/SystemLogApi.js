import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/SystemLog/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/SystemLog/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/SystemLog/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/SystemLog/update',
    method: 'put',
    data
  })
}

