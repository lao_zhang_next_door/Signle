import request from '@/common/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameLog/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameLog/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameLog/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameLog/update',
    method: 'put',
    data
  })
}

