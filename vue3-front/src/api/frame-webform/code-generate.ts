import request from '@/config/axios'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { FrameTableField } from '@/types/frame-system/frame-table-field'
import { CodegenReqVO } from '@/types/frame-webform/code-generate-table'

/**
 * 获得表定义分页
 * @param params
 * @returns
 */
export const getCodegenTablePage = (params: FrameSearch) => {
  return request.get({
    url: '/form/codegen/table/page',
    params
  })
}

/**
 * 获得字段定义分页
 * @param params
 * @returns
 */
export const getColumnsList = (params: FrameSearch) => {
  return request.get({
    url: '/form/codegen/getColumnsList',
    params
  })
}

/**
 * 获得表和字段的明细
 * @param tableId
 * @returns
 */
export const getCodegenDetail = (tableId: number) => {
  return request.get({
    url: '/form/codegen/detail?tableId=' + tableId,
    method: 'get'
  })
}

/**
 * 修改代码生成信息
 * @param data
 * @returns
 */
export const updateCodegen = (data: CodegenReqVO) => {
  return request.put({
    url: '/form/codegen/update',
    data: data
  })
}

/**
 * 基于数据库的表结构，同步数据库的表和字段定义
 * @param tableId
 * @returns
 */
export const syncCodegenFromDB = (tableId: number) => {
  return request.put({
    url: '/form/codegen/sync-from-db?tableId=' + tableId
  })
}

/**
 * 基于 SQL 建表语句，同步数据库的表和字段定义
 * @param tableId
 * @param sql
 * @returns
 */
export const syncCodegenFromSQL = (tableId: number, sql: string) => {
  return request.put({
    url: '/form/codegen/sync-from-sql?tableId=' + tableId,
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    },
    data: 'tableId=' + tableId + '&sql=' + sql
  })
}

/**
 * 预览生成代码
 * @param tableId
 * @returns
 */
export const previewCodegen = (tableId: number) => {
  return request.get({
    url: '/form/codegen/preview?tableId=' + tableId
  })
}

/**
 * 下载生成代码
 * @param tableId
 * @returns
 */
export const downloadCodegen = (tableId: number) => {
  return request.get({
    url: '/form/codegen/download?tableId=' + tableId,
    responseType: 'blob'
  })
}

/**
 * 获得表定义分页
 * @param query
 * @returns
 */
export const getSchemaTableList = (query: FrameSearch) => {
  return request.get({
    url: '/form/codegen/db/table/list',
    params: query
  })
}

/**
 * 基于数据库的表结构，创建代码生成器的表定义
 * @param data
 * @returns
 */
export const createCodegenList = (data) => {
  return request.post({
    url: '/form/codegen/create-list',
    data: data
  })
}

/**
 * 删除数据库的表和字段定义
 * @param tableId
 * @returns
 */
export const deleteCodegen = (tableId: number) => {
  return request.delete({
    url: '/form/codegen/delete?tableId=' + tableId
  })
}

/**
 * 创建数据库表 以及 代码生成器的表的定义
 * @param data
 * @returns
 */
export const createCodegenTable = (data) => {
  return request.post({
    url: '/form/codegen/create',
    data: data
  })
}

/**
 * 添加数据库字段 并同步至字段表
 * @param data
 * @param tableId
 * @returns
 */
export const addCodegenFields = (data: FrameTableField[], tableId: number) => {
  return request.post({
    url: '/form/codegen/addCodegenFields?tableId=' + tableId,
    data
  })
}
