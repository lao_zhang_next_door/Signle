import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameModule, FrameModuleTree } from '@/types/frame-system/frame-module'
import { FrameSearch } from '@/types/frame-system/frame-search'

/**
 * 获取数据列表
 * @param FrameSearch 参数
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameModule/listData', data })
}

/**
 * 新增模块
 * @returns info
 */
export const addData = (data: FrameModule) => {
  return request.post({
    url: 'frame/FrameModule/add',
    data
  })
}

/**
 * 修改模块
 * @returns info
 */
export const updateData = (data: FrameModule) => {
  return request.put({
    url: 'frame/FrameModule/update',
    data
  })
}

/**
 * 删除模块
 * @returns info
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameModule/delete',
    data: data.rowGuids
  })
}

/**
 * 获取用户权限路由
 * @returns AppRouteRecordRaw[]
 */
export const getUserRoleRoute = (data: string[]): Promise<IResponse<AppCustomRouteRecordRaw[]>> => {
  return request.post({
    url: 'frame/FrameModule/getVue3PermissionRoute',
    data
  })
}

/**
 * 获取树数据
 * @returns moduleInfo
 */
export const moduleTreeData = (data: FrameModuleTree): Promise<IResponse> => {
  return request.post({
    url: 'frame/FrameModule/allTreeData',
    data
  })
}

/**
 * 根据rowGuid 联查查询上级模块
 * @returns FrameDept
 */
export const selectParentCode = (params: any): Promise<IResponse> => {
  return request.get({
    url: 'frame/FrameModule/selectParentCode',
    params
  })
}
