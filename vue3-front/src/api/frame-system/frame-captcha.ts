import request from '@/config/axios'

/**
 * 获取验证码图片
 * @param {*} data
 * @returns
 */
export const reqGet = (data) => {
  return request.post({
    url: '/support/captcha/get',
    data
  })
}

/**
 * 滑动或者点选验证
 * @param {*} data
 * @returns
 */
export const reqCheck = (data) => {
  return request.post({
    url: '/support/captcha/check',
    data
  })
}
