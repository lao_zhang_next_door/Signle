import request from '@/config/axios'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { FrameTableFieldVo } from '@/types/frame-system/frame-table-field'

/**
 * 获取表单字段列表
 * @param data
 * @returns list
 */
export const getFormTableFieldList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'webform/FormTablefield/listData', data })
}

/**
 * 新增表单字段
 * @returns void
 */
export const addData = (data: FrameTableFieldVo) => {
  return request.post({
    url: 'webform/FormTablefield/add',
    data
  })
}

/**
 * 修改表单字段
 * @returns void
 */
export const updateData = (data: FrameTableFieldVo) => {
  return request.put({
    url: 'webform/FormTablefield/updateTableField',
    data
  })
}

/**
 * 删除表单字段
 * @returns void
 */
export const deleteData = (data: FrameTableFieldVo) => {
  return request.delete({
    url: 'webform/FormTablefield/deleteTableField',
    data
  })
}
