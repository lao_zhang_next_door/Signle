import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameDept, FrameDeptTree } from '@/types/frame-system/frame-dept'

/**
 * 部门数据列表
 * @param any 参数
 * @returns list
 */
export const listData = (data: any): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameDept/listData', data })
}

/**
 * 新增部门
 * @returns void
 */
export const addData = (data: FrameDept) => {
  return request.post({
    url: 'frame/FrameDept/add',
    data
  })
}

/**
 * 修改部门
 * @returns void
 */
export const updateData = (data: FrameDept) => {
  return request.put({
    url: 'frame/FrameDept/update',
    data
  })
}

/**
 * 删除部门
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.put({
    url: 'frame/FrameDept/delete',
    data: data.rowGuids
  })
}

/**
 * 获取树数据
 * @returns FrameDept
 */
export const deptTreeData = (data: FrameDeptTree): Promise<IResponse> => {
  return request.post({
    url: 'frame/FrameDept/allTreeData',
    data
  })
}

/**
 * 根据rowGuid 联查查询上级部门
 * @returns FrameDept
 */
export const selectParentCode = (params: any): Promise<IResponse> => {
  return request.get({
    url: 'frame/FrameDept/selectParentCode',
    params
  })
}
