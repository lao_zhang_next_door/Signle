import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { FrameUserVo } from '@/types/frame-system/frame-user'

/**
 * 获取用户列表
 * @param FrameSearch 参数
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameUser/listData', data })
}

/**
 * 新增用户
 * @returns info
 */
export const addData = (data: FrameUserVo) => {
  return request.post({
    url: 'frame/FrameUser/addFrameUser',
    data
  })
}

/**
 * 修改用户
 * @returns info
 */
export const updateData = (data: FrameUserVo) => {
  return request.put({
    url: 'frame/FrameUser/updateFrameUser',
    data
  })
}

/**
 * 逻辑删除用户
 * @returns info
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameUser/deleteLogic',
    data: data.rowGuids
  })
}

/**
 * 启用或者禁用用户
 * @param userGuid
 * @param type enable/disable
 */
export const enableOrDisable = (userGuid: string, type: string) => {
  return request.put({
    url: 'frame/FrameUser/enableOrDisable?userGuid=' + userGuid + '&type=' + type
  })
}

/**
 * 查询guid用户详情信息 包括角色,头像,部门等信息
 * @returns info
 */
export const getUserDataByGuid = (rowGuid: string) => {
  return request.get({
    url: 'frame/FrameUser/getUserDataByGuid?rowGuid=' + rowGuid
  })
}
