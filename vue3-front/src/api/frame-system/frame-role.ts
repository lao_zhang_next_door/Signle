import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameRole, DataPermissionVo } from '@/types/frame-system/frame-role'
import { FrameSearch } from '@/types/frame-system/frame-search'

/**
 * 获取角色列表
 * @param data
 * @returns list
 */
export const getFrameRoleList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameRole/listData', data })
}

/**
 * 新增角色
 * @returns void
 */
export const addData = (data: FrameRole) => {
  return request.post({
    url: 'frame/FrameRole/add',
    data
  })
}

/**
 * 修改角色
 * @returns void
 */
export const updateData = (data: FrameRole) => {
  return request.put({
    url: 'frame/FrameRole/update',
    data
  })
}

/**
 * 删除角色
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameRole/delete',
    data: data.rowGuids
  })
}

/**
 * 获取所有角色
 * @returns void
 */
export const getAllRole = () => {
  return request.get({
    url: 'frame/FrameRole/getAllRole'
  })
}

/**
 * 保存角色数据权限
 * @returns void
 */
export const saveDataPermission = (data: DataPermissionVo) => {
  return request.post({
    url: 'frame/FrameRole/saveDataPermission',
    data
  })
}
