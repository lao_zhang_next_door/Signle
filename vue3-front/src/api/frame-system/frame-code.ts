import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameCodeName, FrameCode } from '@/types/frame-system/frame-code'
import { FrameSearch } from '@/types/frame-system/frame-search'

/**
 * 获取代码项参数
 * @param codeName 代码项名称
 * @returns list
 */
export const getFrameCodeValue = (params: FrameCodeName): Promise<IResponse> => {
  return request.get({
    url: 'frame/FrameCode/selectCacheByCodeName',
    params
  })
}

/**
 * 获取代码列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameCodeList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameCode/listData', data })
}

/**
 * 新增代码项
 * @returns void
 */
export const addData = (data: FrameCode) => {
  return request.post({
    url: 'frame/FrameCode/add',
    data
  })
}

/**
 * 修改代码项
 * @returns void
 */
export const updateData = (data: FrameCode) => {
  return request.put({
    url: 'frame/FrameCode/update',
    data
  })
}

/**
 * 删除代码项
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameCode/delete',
    data: data.rowGuids
  })
}
