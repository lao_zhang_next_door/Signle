import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { FrameTableInfo, FormTableVo } from '@/types/frame-system/frame-table-info'

/**
 * 获取表单列表
 * @param data
 * @returns list
 */
export const getFormTableInfoList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'webform/FormTableInfo/listData', data })
}

/**
 * 新增表单
 * @returns void
 */
export const addData = (data: FrameTableInfo) => {
  return request.post({
    url: 'webform/FormTableInfo/add',
    data
  })
}

/**
 * 修改表单
 * @returns void
 */
export const updateData = (data: FrameTableInfo) => {
  return request.put({
    url: 'webform/FormTableInfo/updateTableField',
    data
  })
}

/**
 * 删除表单
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'webform/FormTableInfo/delete',
    data: data.rowGuids
  })
}

/**
 * 代码生成
 * @returns void
 */
export const generateCode = (data: FormTableVo) => {
  return request.post({
    url: 'webform/FormTableInfo/generateCode',
    data
  })
}
