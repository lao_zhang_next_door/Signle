import request from '@/config/axios'
import { FrameAttachConfig } from '@/types/frame-system/frame-attach-config'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'

/**
 * 获取附件配置项列表
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameAttachConfig/listData', data })
}

/**
 * 新增附件配置项
 * @returns void
 */
export const addData = (data: FrameAttachConfig) => {
  return request.post({
    url: 'frame/FrameAttachConfig/add',
    data
  })
}

/**
 * 修改附件配置项
 * @returns void
 */
export const updateData = (data: FrameAttachConfig) => {
  return request.put({
    url: 'frame/FrameAttachConfig/update',
    data
  })
}

/**
 * 删除附件配置项
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameAttachConfig/delete',
    data: data.rowGuids
  })
}

/**
 * 更新为主配置
 * @returns void
 */
export const updateMaster = (masterRowGuid: string) => {
  return request.put({
    url: 'frame/FrameAttachConfig/updateMaster?masterRowGuid=' + masterRowGuid
  })
}
