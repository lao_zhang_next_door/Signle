import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameModule } from '@/types/frame-system/frame-module'
import { FrameModulePermission } from '@/types/frame-system/frame-module-permission'
import { FrameSearch } from '@/types/frame-system/frame-search'

/**
 * 获取数据列表权限
 * @param FrameSearch 参数
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameRoleModule/listData', data })
}

/**
 * 获取数据列表权限(无分页)
 * @param allowTo 参数
 * @returns list
 */
export const getListByAllowTo = (allowTo: string): Promise<IResponse<FrameModulePermission[]>> => {
  return request.get({ url: 'frame/FrameRoleModule/getListByAllowTo?allowTo=' + allowTo })
}

/**
 * 新增模块权限
 * @returns info
 */
export const addData = (data: FrameModule) => {
  return request.post({
    url: 'frame/FrameRoleModule/add',
    data
  })
}

/**
 * 修改模块权限
 * @returns info
 */
export const updateData = (data: FrameModule) => {
  return request.put({
    url: 'frame/FrameRoleModule/update',
    data
  })
}

/**
 * 删除模块权限
 * @returns info
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.put({
    url: 'frame/FrameRoleModule/delete',
    data: data.rowGuids
  })
}

/**
 * 批量新增模块权限
 * @returns info
 */
export const addBatch = (data: FrameModulePermission[], roleGuid: string) => {
  return request.post({
    url: 'frame/FrameRoleModule/addBatch?roleGuid=' + roleGuid,
    data
  })
}
