//login.ts
import request from '@/config/axios'
import { FrameAttachUpload } from '@/types/frame-system/frame-attach'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'
/**
 * 获取附件列表
 * @param FrameSearch 参数
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameAttach/listData', data })
}

/**
 * 上传附件
 * @param FrameAttachUpload 文件对象
 * @returns list
 */
export const attachFileUpload = (params: FrameAttachUpload) => {
  // 上传文件对象 名称file与后台控制器参数要一致
  return request.post({ url: 'frame/FrameAttach/vue3uploadFile', params })
}
/**
 * 获取关联附件列表
 * @param FrameAttach
 * @returns list
 */
export const getAttachList = (formRowGuid: string): Promise<IResponse> => {
  return request.get({ url: 'frame/FrameAttach/getListByFormRowGuid?formRowGuid=' + formRowGuid })
}
/**
 * 批量删除附件
 * @param FrameAttach
 * @returns
 */
export const deleteAttach = (data: FrameDeleteBase) => {
  return request.post({ url: 'frame/FrameAttach/deleteFileByRowGuids', data: data.rowGuids })
}

/**
 * 删除单个附件
 * @param rowGuid
 * @returns
 */
export const deleteFileByRowGuid = (rowGuid: string) => {
  return request.delete({ url: 'frame/FrameAttach/deleteFileByRowGuid?rowGuid=' + rowGuid })
}
