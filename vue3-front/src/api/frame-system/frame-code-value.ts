import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameCodeValue } from '@/types/frame-system/frame-code-value'

/**
 * 获取代码值列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameCodeValueList = (data: any): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameCodeValue/listData', data })
}

/**
 * 新增代码项值
 * @returns void
 */
export const addData = (data: FrameCodeValue) => {
  return request.post({
    url: 'frame/FrameCodeValue/add',
    data
  })
}

/**
 * 修改代码项值
 * @returns void
 */
export const updateData = (data: FrameCodeValue) => {
  return request.put({
    url: 'frame/FrameCodeValue/update',
    data
  })
}

/**
 * 删除代码项值
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameCodeValue/delete?codeName=' + data.codeName,
    data: data.rowGuids
  })
}

/**
 * 多维代码树
 * @param data
 * @returns
 */
export function codeValuetreeData(data) {
  return request.post({
    url: '/frame/FrameCodeValue/treeData',
    data
  })
}
