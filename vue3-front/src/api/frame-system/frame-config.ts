import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameConfigName, FrameConfig } from '@/types/frame-system/frame-config'

/**
 * 获取配置项参数
 * @param codeName 配置项名称
 * @returns list
 */
export const getFrameConfigValue = (params: FrameConfigName): Promise<IResponse> => {
  return request.get({
    url: 'frame/FrameConfig/getFrameConfigValue',
    params
  })
}

/**
 * 获取配置项列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameConfigList = (data: any): Promise<IResponse> => {
  return request.post({ url: 'frame/FrameConfig/listData', data })
}

/**
 * 新增配置项
 * @returns void
 */
export const addData = (data: FrameConfig) => {
  return request.post({
    url: 'frame/FrameConfig/add',
    data
  })
}

/**
 * 修改配置项
 * @returns void
 */
export const updateData = (data: FrameConfig) => {
  return request.put({
    url: 'frame/FrameConfig/update',
    data
  })
}

/**
 * 删除配置项
 * @returns void
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'frame/FrameConfig/delete',
    data: data.rowGuids
  })
}

/**
 * 获取是否启用验证码信息
 * @returns boolean
 */
export const checkCaptchaEnable = () => {
  return request.get({
    url: '/frame/FrameConfig/checkCaptchaEnable'
  })
}
