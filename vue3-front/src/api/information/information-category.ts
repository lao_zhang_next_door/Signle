import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { InformationCategory } from '@/types/information/information-category'

/**
 * 获取信息栏目列表
 * @param FrameSearch 参数
 * @returns list
 */
export const listData = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'information/informationCategory/listData', data })
}

/**
 * 新增信息栏目
 * @returns info
 */
export const addData = (data: InformationCategory) => {
  return request.post({
    url: 'information/informationCategory/add',
    data
  })
}

/**
 * 修改信息栏目
 * @returns info
 */
export const updateData = (data: InformationCategory) => {
  return request.put({
    url: 'information/informationCategory/update',
    data
  })
}

/**
 * 逻辑删除信息栏目
 * @returns info
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'information/informationCategory/delete',
    data: data.rowGuids
  })
}

/**
 * 获取信息栏目列表
 * @param FrameSearch 参数
 * @returns list
 */
export const getCategoryList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'information/informationCategory/getCategoryList', data })
}

/**
 * 查询所有栏目树
 * @param FrameSearch 参数
 * @returns list
 */
export const getAllCategoryTree = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'information/informationCategory/getAllCategoryTree', data })
}

/**
 * 根据用户角色查询栏目复选框模块树
 * @param FrameSearch 参数
 * @returns list
 */
export const getCategoryTreeByRole = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'information/informationCategory/getCategoryTreeByRole', data })
}
