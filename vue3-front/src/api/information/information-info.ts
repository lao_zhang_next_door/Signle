import request from '@/config/axios'
import { FrameDeleteBase } from '@/types/frame-system/frame-base'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { InformationInfo } from '@/types/information/information-info'

/**
 * 获取信息列表
 * @param FrameSearch 参数
 * @returns list
 */
export const getInfoRelationList = (data: FrameSearch): Promise<IResponse> => {
  return request.post({ url: 'information/informationInfo/getInfoRelationList', data })
}

/**
 * 新增信息
 * @returns info
 */
export const addData = (data: InformationInfo) => {
  return request.post({
    url: 'information/informationInfo/add',
    data
  })
}

/**
 * 修改信息
 * @returns info
 */
export const updateData = (data: InformationInfo) => {
  return request.put({
    url: 'information/informationInfo/update',
    data
  })
}

/**
 * 逻辑删除信息
 * @returns info
 */
export const deleteData = (data: FrameDeleteBase) => {
  return request.delete({
    url: 'information/informationInfo/delete',
    data: data.rowGuids
  })
}

/**
 * 信息停止发布
 * @returns info
 */
export const batchStopRelease = (data: string[]) => {
  return request.post({
    url: 'information/informationInfo/batchStopRelease',
    data: data
  })
}

/**
 * 信息批量发布
 * @returns info
 */
export const batchStartRelease = (data: string[]) => {
  return request.post({
    url: 'information/informationInfo/batchStartRelease',
    data: data
  })
}
