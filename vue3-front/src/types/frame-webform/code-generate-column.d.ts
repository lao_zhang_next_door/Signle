import { FrameBase } from '../frame-system/frame-base'
/**
 * 代码生成 column 字段定义
 */
export interface CodegenColumn extends FrameBase {
  //表编号
  tableId: string
  //字段名称
  columnName: string
  //字段类型
  dataType: string
  //字段描述
  columnComment: string
  //是否允许为空
  nullable: number
  //是否主键
  primaryKey: number
  //是否自增
  autoIncrement: number
  //排序
  ordinalPosition: number
  //Java 属性类型
  javaType: string
  //Java 属性名
  javaField: string
  //字典类型
  dictType: string
  //数据示例
  example?: string
  //是否为 Create 创建操作的字段
  createOperation: number
  //是否为 Update 更新操作的字段
  updateOperation: number
  //是否为 List 查询操作的字段
  listOperation: number
  //List 查询操作的条件类型
  listOperationCondition: number
  //是否为 List 查询操作的返回字段
  listOperationResult: number
  //显示类型
  htmlType: string
}
