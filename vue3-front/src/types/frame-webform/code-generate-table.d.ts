import { FrameBase } from '../frame-system/frame-base'
/**
 * 代码生成 table 表定义
 */
export interface CodegenTable extends FrameBase {
  //数据源编号
  dataSourceConfigId: string
  //生成场景
  scene: number
  //表名称
  tableName: string
  //表描述
  tableComment: string
  //备注
  remark?: string
  //模块名，即一级目录
  moduleName: string
  //业务名，即二级目录
  businessName: string
  //类名称（首字母大写）
  className: string
  //类描述
  classComment: string
  //作者
  author: string
  //模板类型
  templateType: number
  //父菜单编号
  parentMenuId?: string
}

/**
 * 表单传输对象
 */
export interface CodegenReqVO {
  //表定义
  table: CodegenTable
  //字段定义
  columns: CodegenColumn[]
}
