import { FrameBase } from '../frame-system/frame-base'
export interface InformationCategory extends FrameBase {
  categoryName: string
  pcategoryCode: string
  description: string
  isNeedAudit: string
}
