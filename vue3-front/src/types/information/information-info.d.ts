import { FrameBase } from '../frame-system/frame-base'
export interface InformationInfo extends FrameBase {
  infoTitle: string
  infoContent: string
  releaseTime: string
  infoCreateUser: string
  infoCreateUserGuid: string
  infoFile?: string
  clickTimes?: number
  infoPic?: string
  infoCategoryGuid: string
  infoType: string
  infoUrl?: string
  infoStatus: string
  isShowInformation: string
  categoryCode?: string
  categoryGuids?: string[]
}
