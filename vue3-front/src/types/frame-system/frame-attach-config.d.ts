import { FrameBase } from './frame-base'
export interface FrameAttachConfig extends FrameBase {
  // 配置名
  name?: string
  // 存储器
  storage?: number | string
  //备注
  remark?: string
  //是否为主配置
  master?: boolean
  // 存储配置
  config?: string
}
