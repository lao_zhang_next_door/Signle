import { FrameBase } from './frame-base'
export interface FrameTableField extends FrameBase {
  fieldName: string
  fieldType: string
  fieldLength: string
  decimalLength: string
  fieldDisplayName: string
  isQueryCondition: string
  mustFill: string
  fieldDisplayType: string
  showInadd: string
  allowTo: string
  codesGuid: string
}

export interface FrameTableFieldVo {
  field: FrameTableField
  tableInfo: string
  originalName?: string
  tableName?: string
  rowGuid: string | null
  fieldNames?: string[]
}
