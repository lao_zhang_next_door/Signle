import { FrameBase } from './frame-base'
export interface FrameCode extends FrameBase {
  codeName: string
  codeMask: string
}
export interface FrameCodeName {
  // 代码项名称
  codeName: string
}
