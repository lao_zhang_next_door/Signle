import { FrameBase } from './frame-base'
export interface FrameModulePermission extends FrameBase {
  moduleGuid: string
  allowTo: string
  allowType?: string
}
