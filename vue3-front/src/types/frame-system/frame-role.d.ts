import { FrameBase } from './frame-base'
export interface FrameRole extends FrameBase {
  roleName: string
  roleTypeText?: string
  roleType?: string | number
  dataScope?: string
  memo?: string
}

export interface FrameRoleName {
  // 角色名称
  roleNameVague?: string
}

/**
 * 角色部门关联表
 */
export interface FrameRoleDept {
  roleGuid: string
  deptGuid: string
}

export interface DataPermissionVo {
  frameRole: FrameRole
  frameRoleDeptList: FrameRoleDept[]
}
