export interface FrameBase {
  //是否需要RSA加密
  needRSA?: boolean
  rowId?: number
  rowGuid: string | null
  sortSq?: number
}

export type FrameDeleteBase = {
  rowGuids?: string[]
} & Recordable
