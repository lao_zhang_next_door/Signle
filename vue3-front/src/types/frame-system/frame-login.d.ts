import { FrameBase } from './frame-base'

export interface UserLoginType extends FrameBase {
  username: string
  password: string
  client_id?: string
  client_secret?: string
  grant_type?: string
  captchaVO?: any
}

export interface UserType {
  username: string
  password: string
  role: string
  roleId: string
  permissions: string | string[]
}
