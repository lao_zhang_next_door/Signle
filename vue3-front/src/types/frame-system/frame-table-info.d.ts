import { FrameBase } from './frame-base'
export interface FrameTableInfo extends FrameBase {
  tableName: string
  physicalName: string
  projectName: string
  controllerName: string
  formStyle?: string
  outPutDir?: string
}

export interface FormTableVo {
  // 包名
  packageName: string
  //输出文件位置
  outPutDir: string
  // 数据表名
  tableName: string
}
