import { FrameBase } from './frame-base'
export interface FrameConfig extends FrameBase {
  categoryType: string
  categoryTypeText: string
  configName: string
  configValue: string | number
  description?: string
}

export interface FrameConfigName {
  // 配置项项名称
  configName?: string
}
