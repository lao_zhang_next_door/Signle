import {
  ERightToolAttrType,
  IConfigComponentItem,
  IDomExtProps
} from '@/views/PageDesigner/model/model'
import { defaultSchemaConfig } from '../schema'

export const D_ElRadio: IConfigComponentItem = {
  title: '单选框',
  icon: 'material-symbols:radio-button-checked',
  domInfo: {
    tag: 'el-radio',
    componentName: 'Radio',
    title: 'el单选框',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      label: {
        type: ERightToolAttrType.Input,
        value: 'Option1',
        default: 'Option1',
        title: 'label',
        tips: '单选框对应的值'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      border: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '显示边框',
        tips: '是否显示边框'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: 'Checkbox 的尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      name: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'name属性',
        tips: '原生name属性'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发的事件',
        val: "console.log('Checkbox--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    schemaConfig: defaultSchemaConfig,
    childrens: []
  }
}

export const D_ElRadioButton: IConfigComponentItem = {
  title: '单选按钮',
  icon: 'material-symbols:radio-button-checked-outline',
  domInfo: {
    tag: 'el-radio-button',
    componentName: 'RadioButton',
    title: 'el单选按钮',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      label: {
        type: ERightToolAttrType.Input,
        value: 'Option1',
        default: 'Option1',
        title: 'label',
        tips: '单选框对应的值'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      name: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'name属性',
        tips: '原生name属性'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发的事件',
        val: "console.log('Checkbox--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    schemaConfig: defaultSchemaConfig,
    childrens: []
  }
}

export const D_ElRadioGroup: IConfigComponentItem = {
  title: '单选框组',
  icon: 'mdi:format-list-group-plus',
  domInfo: {
    tag: 'el-radio-group',
    componentName: 'Radio',
    title: 'el单选框组',
    slots: true,
    canMove: true,
    canAllowTo: true,
    ClearStyle: true,
    props: {
      'v-model': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'v-model',
        tips: '绑定值'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: 'Checkbox 的尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      'text-color': {
        type: ERightToolAttrType.ColorPicker,
        value: '#ffffff',
        default: '#ffffff',
        title: '文本颜色',
        tips: '按钮形式的 Radio 激活时的文本颜色'
      },
      fill: {
        type: ERightToolAttrType.ColorPicker,
        value: '#409EFF',
        default: '#409EFF',
        title: '填充颜色',
        tips: '按钮形式的 Radio 激活时的填充色和边框色'
      },
      'validate-event': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '表单校验',
        tips: '输入时是否触发表单的校验'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发的事件',
        val: "console.log('Radiogroup--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    schemaConfig: defaultSchemaConfig,
    childrens: []
  }
}
