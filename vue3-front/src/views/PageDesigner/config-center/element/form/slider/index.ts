import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'

export const D_ElSlider: IConfigComponentItem = {
  title: '滑块',
  icon: 'bi:sliders',
  domInfo: {
    tag: 'el-slider',
    componentName: 'Slider',
    title: 'el滑块',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      min: {
        type: ERightToolAttrType.InputNumber,
        value: 0,
        default: 0,
        title: '最小值',
        tips: '最小值'
      },
      max: {
        type: ERightToolAttrType.InputNumber,
        value: 100,
        default: 100,
        title: '最大值',
        tips: '最大值'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      step: {
        type: ERightToolAttrType.InputNumber,
        value: 1,
        default: 1,
        title: '步长',
        tips: '步长'
      },
      'show-input': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否显示输入框',
        tips: '是否显示输入框，仅在非范围选择时有效'
      },
      'show-input-controls': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否显示控制按钮',
        tips: '在显示输入框的情况下，是否显示输入框的控制按钮'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: 'select 的尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      'input-size': {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '输入框大小',
        tips: '输入框的大小，如果设置了 size 属性，默认值自动取 size',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      'show-stops': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否显示间断点',
        tips: '是否显示间断点'
      },
      'show-tooltip': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '是否显示提示信息',
        tips: '是否显示提示信息'
      },
      'format-tooltip': {
        type: ERightToolAttrType.Function,
        value: '',
        default: '',
        title: '格式化提示信息',
        tips: '格式化提示信息'
      },
      'multiple-limit': {
        type: ERightToolAttrType.InputNumber,
        value: 0,
        default: 0,
        title: '多选上限',
        tips: 'multiple 属性设置为 true 时，代表多选场景下用户最多可以选择的项目数， 为 0 则不限制'
      },
      range: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否开启选择范围',
        tips: '是否开启选择范围'
      },
      vertical: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '垂直模式',
        tips: '垂直模式'
      },
      height: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '滑块高度',
        tips: '滑块高度，垂直模式必填'
      },
      label: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'label',
        tips: '屏幕阅读器标签'
      },
      'range-start-label': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '标签开始标记',
        tips: '当 range 为true时，屏幕阅读器标签开始的标记'
      },
      'range-end-label': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '标签结尾标记',
        tips: '当 range 为true时，屏幕阅读器标签结尾的标记'
      },
      debounce: {
        type: ERightToolAttrType.InputNumber,
        value: 300,
        default: 300,
        title: '去抖延迟',
        tips: '输入时的去抖延迟，毫秒，仅在 show-input 等于 true 时有效'
      },
      'tooltip-class': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'tooltip 的自定义类名',
        tips: 'tooltip 的自定义类名'
      },
      'validate-event': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '触发表单校验',
        tips: '输入时是否触发表单的校验'
      },
      placement: {
        type: ERightToolAttrType.Select,
        value: 'bottom-start',
        default: 'bottom-start',
        title: '出现位置',
        tips: 'Tooltip 出现的位置',
        options: [
          {
            value: 'top',
            label: 'top'
          },
          {
            value: 'top-start',
            label: 'top-start'
          },
          {
            value: 'top-end',
            label: 'top-end'
          },
          {
            value: 'bottom',
            label: 'bottom'
          },
          {
            value: 'bottom-start',
            label: 'bottom-start'
          },
          {
            value: 'bottom-end',
            label: 'bottom-end'
          },
          {
            value: 'left',
            label: 'left'
          },
          {
            value: 'left-start',
            label: 'left-start'
          },
          {
            value: 'left-end',
            label: 'left-end'
          },
          {
            value: 'right',
            label: 'right'
          },
          {
            value: 'right-start',
            label: 'right-start'
          },
          {
            value: 'right-end',
            label: 'right-end'
          }
        ]
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '值改变时触发（使用鼠标拖曳时，只在松开鼠标后触发）',
        val: "console.log('Select--change:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      input: {
        custom: true,
        tips: '数据改变时触发（使用鼠标拖曳时，活动过程实时触发）',
        val: "console.log('Select--input:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    childrens: [],
    compatibility: true
  }
}
