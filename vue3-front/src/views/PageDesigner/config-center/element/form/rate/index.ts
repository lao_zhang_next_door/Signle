import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'

export const D_ElRate: IConfigComponentItem = {
  title: '评分',
  icon: 'ic:outline-star-rate',
  domInfo: {
    tag: 'el-rate',
    componentName: 'Rate',
    title: 'el评分框',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      max: {
        type: ERightToolAttrType.InputNumber,
        value: 5,
        default: 5,
        title: '最大分值',
        tips: '最大分值	'
      },
      'allow-half': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否允许半选',
        tips: '是否允许半选'
      },
      'low-threshold': {
        type: ERightToolAttrType.InputNumber,
        value: 2,
        default: 2,
        title: '低分界限',
        tips: '低分和中等分数的界限值， 值本身被划分在低分中'
      },
      'high-threshold': {
        type: ERightToolAttrType.InputNumber,
        value: 4,
        default: 4,
        title: '高分界限',
        tips: '高分和中等分数的界限值， 值本身被划分在高分中'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: '尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      readonly: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '原生只读',
        tips: '原生属性,是否只读'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '原生禁用',
        tips: '原生属性,是否禁用状态'
      },
      clearable: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '可重置',
        tips: '是否可以重置值为 0'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发',
        val: "console.log('rate--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    childrens: [],
    compatibility: true
  }
}
