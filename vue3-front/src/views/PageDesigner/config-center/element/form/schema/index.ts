import { ERightToolAttrType } from '@/views/PageDesigner/model/model'

export const defaultSchemaConfig = {
  optionsAliasLabelField: {
    type: ERightToolAttrType.Input,
    value: 'itemText',
    default: 'itemText',
    title: 'LabelField',
    tips: '数据源匹配label'
  },
  optionsAliasLabelValue: {
    type: ERightToolAttrType.Input,
    value: 'itemValue',
    default: 'itemValue',
    title: 'LabelValue',
    tips: '数据源匹配value'
  },
  dictName: {
    type: ERightToolAttrType.Input,
    value: '',
    default: '',
    title: 'dictName',
    tips: '代码项名称'
  },
  options: {
    type: ERightToolAttrType.JsonEdit,
    value: '',
    default: '',
    title: 'options',
    tips: '数据项'
  }
}
