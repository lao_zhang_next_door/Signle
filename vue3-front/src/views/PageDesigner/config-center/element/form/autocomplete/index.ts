import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'
import { defaultSchemaConfig } from '../schema'

export const D_ElAutoComplete: IConfigComponentItem = {
  title: '自动补全',
  icon: 'fluent:cloud-sync-complete-16-regular',
  domInfo: {
    tag: 'el-autocomplete',
    componentName: 'Autocomplete',
    title: 'el自动补全输入框',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      placeholder: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '占位文本',
        tips: 'string'
      },
      clearable: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '可清空',
        tips: '是否可清空	'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否禁用',
        tips: '自动补全组件是否被禁用'
      },
      lazy: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '动态加载子节点',
        tips: '是否动态加载子节点,需与 lazyLoad 方法结合使用,暂未实现方法关联'
      },
      'value-key': {
        type: ERightToolAttrType.Input,
        value: 'value',
        default: 'value',
        title: 'props键名',
        tips: '输入建议对象中用于显示的键名'
      },
      debounce: {
        type: ERightToolAttrType.InputNumber,
        value: 300,
        default: 300,
        title: '防抖延时',
        tips: '获取输入建议的防抖延时，单位为毫秒'
      },
      placement: {
        type: ERightToolAttrType.Select,
        value: 'bottom-start',
        default: 'bottom-start',
        title: '弹出位置',
        tips: '菜单弹出位置',
        options: [
          {
            value: 'top',
            label: 'top'
          },
          {
            value: 'top-start',
            label: 'top-start'
          },
          {
            value: 'top-end',
            label: 'top-end'
          },
          {
            value: 'bottom',
            label: 'bottom'
          },
          {
            value: 'bottom-start',
            label: 'bottom-start'
          },
          {
            value: 'bottom-end',
            label: 'bottom-end'
          }
        ]
      },
      'fetch-suggestions': {
        type: ERightToolAttrType.Function,
        value: '',
        default: '',
        title: '远程调用方法',
        tips: '获取输入建议的方法， 仅当你的输入建议数据 resolve 时，通过调用 callback(data:[])  来返回它'
      },
      'popper-class': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '类名',
        tips: '下拉列表的类名'
      },
      'trigger-on-focus': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: 'focus扳机',
        tips: 'whether show suggestions when input focus'
      },
      name: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'input name',
        tips: '等价于原生 input name 属性'
      },
      'select-when-unmatched': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '回车触发',
        tips: '在输入没有任何匹配建议的情况下，按下回车是否触发 select 事件'
      },
      label: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'label',
        tips: '输入框关联的 label 文字'
      },
      'hide-loading': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '隐藏加载图标',
        tips: '是否隐藏远程加载时的加载图标'
      },
      'popper-append-to-body': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: 'append-to-body',
        tips: '是否将下拉列表插入至 body 元素。 在下拉列表的定位出现问题时，可将该属性设置为 false'
      },
      teleported: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: 'append-to',
        tips: '是否将下拉列表元素插入 append-to 指向的元素下'
      },
      'highlight-first-item': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '第一条高亮',
        tips: '是否默认高亮远程搜索结果的第一项'
      },
      'fit-input-width': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '宽度同步',
        tips: '下拉框的宽度是否与输入框相同'
      }
    },
    eventAttr: {
      select: {
        custom: true,
        tips: '点击选中建议项时触发',
        val: "console.log('autocomplete--下拉框状态:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      change: {
        custom: true,
        tips: '在 Input 值改变时触发',
        val: "console.log('autocomplete--input值:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    schemaConfig: defaultSchemaConfig,
    childrens: [],
    compatibility: true
  }
}
