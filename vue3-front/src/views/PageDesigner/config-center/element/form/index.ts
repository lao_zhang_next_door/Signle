import { IConfigComponentGroup } from '@/views/PageDesigner/model/model'
import { D_ElCascader, D_ElCascaderPanel } from './cascader'
import { D_ElCheckbox, D_ElCheckboxGroup } from './checkbox'
import { D_ElColorPicker } from './colorpicker'
import { D_ElDatePicker } from './datepicker'
import { D_ElForm, D_ElFormItem } from './form'
import { D_ElInput } from './input'
import { D_ElAutoComplete } from './autocomplete'
import { D_ElInputNumber } from './input-number'
import { D_ElRate } from './rate'
import { D_ElSelect } from './select'
import { D_ElSlider } from './slider'
import { D_ElSwitch } from './switch'
import { D_ElRadio, D_ElRadioButton, D_ElRadioGroup } from './radio'

export const ElFormGroup: IConfigComponentGroup = {
  groupType: 'form',
  title: '表单组件',
  list: [
    D_ElCascader,
    D_ElCascaderPanel,
    D_ElCheckbox,
    D_ElCheckboxGroup,
    D_ElColorPicker,
    D_ElDatePicker,
    D_ElForm,
    D_ElFormItem,
    D_ElInput,
    D_ElAutoComplete,
    D_ElInputNumber,
    D_ElRate,
    D_ElSelect,
    D_ElSlider,
    D_ElSwitch,
    D_ElRadio,
    D_ElRadioButton,
    D_ElRadioGroup
  ]
}
