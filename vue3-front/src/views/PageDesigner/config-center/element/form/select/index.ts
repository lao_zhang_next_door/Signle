import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'
import { defaultSchemaConfig } from '../schema'

export const D_ElSelect: IConfigComponentItem = {
  title: '下拉框',
  icon: 'fluent:multiselect-ltr-24-regular',
  domInfo: {
    tag: 'el-select',
    componentName: 'Select',
    title: 'el下拉框',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      multiple: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '是否多选',
        tips: '是否多选'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      'value-key': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '对象键名',
        tips: '作为 value 唯一标识的键名，绑定值为对象类型时必填'
      },
      size: {
        type: ERightToolAttrType.Select,
        default: 'default',
        value: 'default',
        title: '尺寸',
        tips: 'select 的尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      clearable: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '可清空',
        tips: '是否可以清空选项'
      },
      'collapse-tags': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '文字形式展示',
        tips: '多选时是否将选中值按文字的形式展示'
      },
      'collapse-tags-tooltip': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '悬停显示标签',
        tips: '当鼠标悬停于折叠标签的文本时，是否显示所有选中的标签。 要使用此属性，collapse-tags属性必须设定为 true'
      },
      'multiple-limit': {
        type: ERightToolAttrType.InputNumber,
        value: 0,
        default: 0,
        title: '多选上限',
        tips: 'multiple 属性设置为 true 时，代表多选场景下用户最多可以选择的项目数， 为 0 则不限制'
      },
      border: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '显示边框',
        tips: '是否显示边框'
      },
      name: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'name属性',
        tips: '原生name属性'
      },
      effect: {
        type: ERightToolAttrType.Select,
        value: 'light',
        default: 'light',
        title: '主题',
        tips: 'Tooltip 主题，内置了 dark / light 两种',
        options: [
          {
            value: 'light',
            label: 'light'
          },
          {
            value: 'dark',
            label: 'dark'
          }
        ]
      },
      autocomplete: {
        type: ERightToolAttrType.Input,
        value: 'off',
        default: 'off',
        title: 'autocomplete',
        tips: 'Select 输入框的原生 autocomplete 属性'
      },
      placeholder: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'placeholder',
        tips: '占位文字'
      },
      filterable: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '可筛选',
        tips: 'Select 组件是否可筛选'
      },
      'allow-create': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '允许创建新条目',
        tips: '是否允许用户创建新条目， 只有当 filterable 设置为 true 时才会生效'
      },
      'filter-method': {
        type: ERightToolAttrType.Function,
        value: '',
        default: '',
        title: '自定义筛选方法',
        tips: 'Function'
      },
      remote: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '选项远程加载',
        tips: '其中的选项是否从服务器远程加载'
      },
      'remote-method': {
        type: ERightToolAttrType.Function,
        value: '',
        default: '',
        title: '远程搜索方法',
        tips: '自定义远程搜索方法'
      },
      'fit-input-width': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '宽度同步',
        tips: '下拉框的宽度是否与输入框相同'
      },
      'validate-event': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '是否触发表单验证	',
        tips: '是否触发表单验证	'
      },
      placement: {
        type: ERightToolAttrType.Select,
        value: 'bottom-start',
        default: 'bottom-start',
        title: '出现位置',
        tips: '下拉框出现的位置',
        options: [
          {
            value: 'top',
            label: 'top'
          },
          {
            value: 'top-start',
            label: 'top-start'
          },
          {
            value: 'top-end',
            label: 'top-end'
          },
          {
            value: 'bottom',
            label: 'bottom'
          },
          {
            value: 'bottom-start',
            label: 'bottom-start'
          },
          {
            value: 'bottom-end',
            label: 'bottom-end'
          },
          {
            value: 'left',
            label: 'left'
          },
          {
            value: 'left-start',
            label: 'left-start'
          },
          {
            value: 'left-end',
            label: 'left-end'
          },
          {
            value: 'right',
            label: 'right'
          },
          {
            value: 'right-start',
            label: 'right-start'
          },
          {
            value: 'right-end',
            label: 'right-end'
          }
        ]
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发的事件',
        val: "console.log('Select--change:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      'visible-change': {
        custom: true,
        tips: '下拉框出现/隐藏时触发',
        val: "console.log('Select--visible-change:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      'remove-tag': {
        custom: true,
        tips: '多选模式下移除tag时触发',
        val: "console.log('Select--remove-tag:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      clear: {
        custom: true,
        tips: '可清空的单选模式下用户点击清空按钮时触发',
        val: "console.log('Select--clear:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      blur: {
        custom: true,
        tips: '当 input 失去焦点时触发',
        val: "console.log('Select--blur:'+event);",
        anonymous_params: ['event'],
        list: []
      },
      focus: {
        custom: true,
        tips: '当 input 获得焦点时触发',
        val: "console.log('Select--focus:'+event);",
        anonymous_params: ['event'],
        list: []
      }
    },
    schemaConfig: defaultSchemaConfig,
    childrens: [],
    compatibility: true
  }
}
export const D_ElSelectGroup: IConfigComponentItem = {
  title: '下拉框组',
  icon: 'select-group',
  domInfo: {
    tag: 'el-select-group',
    title: 'el下拉框组',
    slots: true,
    canMove: true,
    canAllowTo: false,
    ClearStyle: true,
    props: {
      label: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '分组的组名',
        tips: '分组的组名'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: 0,
        default: 0,
        title: '分组禁用',
        tips: '是否将该分组下所有选项置为禁用'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发的事件',
        val: "console.log('Selectgroup--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    childrens: [
      {
        ...D_ElSelect.domInfo,
        props: {
          ...D_ElSelect.domInfo.props,
          label: { ...D_ElSelect.domInfo.props.label, default: 'Option1' }
        },
        title: '多选框组一',
        canMove: false,
        ClearStyle: true
      },
      {
        ...D_ElSelect.domInfo,
        props: {
          ...D_ElSelect.domInfo.props,
          label: { ...D_ElSelect.domInfo.props.label, default: 'Option2' }
        },
        title: '多选框组二',
        canMove: false,
        ClearStyle: true
      }
    ]
  }
}
