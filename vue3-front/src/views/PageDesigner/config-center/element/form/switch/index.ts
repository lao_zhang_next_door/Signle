import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'
import { defaultSchemaConfig } from '../schema'

export const D_ElSwitch: IConfigComponentItem = {
  title: '开关',
  icon: 'mdi:toggle-switch',
  domInfo: {
    tag: 'el-switch',
    componentName: 'Switch',
    title: 'el开关',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '是否禁用'
      },
      width: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '宽度',
        tips: 'switch 的宽度'
      },
      'inline-prompt': {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '呈现文本的第一个字符',
        tips: '无论图标或文本是否显示在点内，只会呈现文本的第一个字符'
      },
      'active-icon': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '激活图标样式',
        tips: 'switch 状态为 on 时所显示图标，设置此项会忽略 active-text'
      },
      'inactive-icon': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '不激活图标样式',
        tips: 'switch 状态为 on 时所显示图标，设置此项会忽略 active-text'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: 'select 的尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      'active-text': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '激活时文字描述',
        tips: 'switch 打开时的文字描述'
      },
      'inactive-text': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '不激活时文字描述',
        tips: 'switch 的状态为 off 时的文字描述'
      },
      'active-value': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '激活值',
        tips: 'switch 状态为 on 时的值'
      },
      'inactive-value': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '不激活值',
        tips: 'switch 状态为 off 时的值'
      },
      name: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'name 属性',
        tips: 'switch 对应的 name 属性'
      },
      'validate-event': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '触发表单校验',
        tips: '输入时是否触发表单的校验'
      },
      'before-change': {
        type: ERightToolAttrType.Function,
        value: '',
        default: '',
        title: 'switch 状态改变前钩子函数',
        tips: 'switch 状态改变前的钩子， 返回 false 或者返回 Promise 且被 reject 则停止切换'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '值改变时触发（使用鼠标拖曳时，只在松开鼠标后触发）',
        val: "console.log('Select--change:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    schemaConfig: defaultSchemaConfig,
    childrens: [],
    compatibility: true
  }
}
