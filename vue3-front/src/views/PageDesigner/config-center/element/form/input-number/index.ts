import { ERightToolAttrType, IConfigComponentItem } from '@/views/PageDesigner/model/model'

export const D_ElInputNumber: IConfigComponentItem = {
  title: '数字输入框',
  icon: 'mdi:number-0-box-multiple-outline',
  domInfo: {
    tag: 'el-input-number',
    componentName: 'InputNumber',
    title: 'el数字输入框',
    slots: false,
    canMove: true,
    canAllowTo: false,
    ClearStyle: false,
    props: {
      min: {
        type: ERightToolAttrType.InputNumber,
        value: 0,
        default: 0,
        title: '最小值',
        tips: '设置计数器允许的最小值'
      },
      max: {
        type: ERightToolAttrType.InputNumber,
        value: 100,
        default: 100,
        title: '最大值',
        tips: '设置计数器允许的最大值	'
      },
      step: {
        type: ERightToolAttrType.InputNumber,
        value: 1,
        default: 1,
        title: '步长',
        tips: '计数器步长'
      },
      'step-strictly': {
        type: ERightToolAttrType.InputNumber,
        value: 1,
        default: 1,
        title: '精度',
        tips: '数值精度'
      },
      size: {
        type: ERightToolAttrType.Select,
        value: 'default',
        default: 'default',
        title: '尺寸',
        tips: '尺寸',
        options: [
          {
            value: 'large',
            label: '大'
          },
          {
            value: 'default',
            label: '默认'
          },
          {
            value: 'small',
            label: '小'
          }
        ]
      },
      readonly: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '只读',
        tips: '原生属性,是否只读'
      },
      disabled: {
        type: ERightToolAttrType.Switch,
        value: false,
        default: false,
        title: '禁用',
        tips: '原生属性,是否禁用状态'
      },
      controls: {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '控制按钮',
        tips: '是否使用控制按钮'
      },
      'controls-position': {
        type: ERightToolAttrType.Select,
        value: 'right',
        default: 'right',
        title: '控制按钮位置',
        tips: '控制按钮位置',
        options: [
          {
            value: 'right',
            label: '右'
          }
        ]
      },
      name: {
        type: ERightToolAttrType.Input,
        value: ``,
        default: ``,
        title: '原生属性name',
        tips: '原生属性name'
      },
      label: {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: 'label文字',
        tips: '输入框关联的 label 文字'
      },
      autocomplete: {
        type: ERightToolAttrType.Input,
        value: `off`,
        default: `off`,
        title: '自动补全',
        tips: '原生属性,自动补全'
      },
      'value-on-clear': {
        type: ERightToolAttrType.Input,
        value: '',
        default: '',
        title: '清空时显示的值',
        tips: '当输入框被清空时显示的值'
      },
      'validate-event': {
        type: ERightToolAttrType.Switch,
        value: true,
        default: true,
        title: '触发校验',
        tips: '输入时是否触发表单的校验'
      }
    },
    tagSlots: {
      enable: false,
      val: ''
    },
    eventAttr: {
      change: {
        custom: true,
        tips: '当绑定值变化时触发',
        val: "console.log('InputNumber--change:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      blur: {
        custom: true,
        tips: '在组件 Input 失去焦点时触发',
        val: "console.log('InputNumber--blur:'+value);",
        anonymous_params: ['value'],
        list: []
      },
      focus: {
        custom: true,
        tips: '在组件 Input 获得焦点时触发',
        val: "console.log('InputNumber--focus:'+value);",
        anonymous_params: ['value'],
        list: []
      }
    },
    childrens: [],
    compatibility: true
  }
}
