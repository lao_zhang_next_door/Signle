import {
  IConfigComponentItem,
  IConfigComponentItemInfo,
  IConfigComponentItemProps,
  IDoneComponent,
  IDoneComponentProps
} from '@/views/PageDesigner/model/model'

export const objectDeepClone = (object: object, default_val: any = {}) => {
  if (!object) {
    return default_val
  }
  return JSON.parse(JSON.stringify(object))
}
/**
 * 生成随机字符串
 * @param len 生成个数
 */
export const randomString = (len?: number) => {
  len = len || 10
  const str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const maxPos = str.length
  let random_str = ''
  for (let i = 0; i < len; i++) {
    random_str += str.charAt(Math.floor(Math.random() * maxPos))
  }
  return random_str
}
export const cssToJson = (css_str: string) => {
  //记录第一个花括号的位置
  const firstbrackets_index = css_str.indexOf('{')
  const lastbrackets_index = css_str.indexOf('}')
  //将括号外的都视为垃圾数据
  const effective_str = css_str
    .substring(firstbrackets_index + 1, lastbrackets_index)
    .replace(/\s/g, '')
  //根据分号分隔成数组
  const effective_arr = effective_str.split(';')
  let _json = ''
  effective_arr.forEach((f) => {
    if (f.includes(':')) {
      const temp_json_arr = f.split(':')
      _json += `,"${temp_json_arr[0]}":"${temp_json_arr[1]}"`
    }
  })
  _json = `{${_json.substring(1)}}`
  return _json
}
export const objectToCss = (ob: { [key: string]: string }) => {
  let _css = ''
  for (const ob_item in ob) {
    _css += `;\n\t${ob_item}:${ob[ob_item]}`
  }
  if (_css) {
    _css = `{${_css.substring(1)};\n}`
  } else {
    _css = `{\n\n}`
  }

  return _css
}

/**
 * 处理组件默认配置的子项 实际上就是为了生成id
 * @param childrens
 */
export const handleConfigChildrens = (childrens: IConfigComponentItemInfo[]) => {
  const temp: IDoneComponent[] = []
  childrens.forEach((children) => {
    const { childrens, props, ...temp_params } = children
    temp.push({
      id: temp_params.tag + '-' + randomString(),
      childrens: handleConfigChildrens(childrens),
      props: handleConfigProps(props),
      ...temp_params
    })
  })
  return temp
}

/**
 * 组件props
 * @param props
 * @returns
 */
export const handleConfigProps = (props: IConfigComponentItemProps) => {
  let temp: IDoneComponentProps = {}
  for (const key in props) {
    temp = {
      ...temp,
      ...{
        [key]: { default: props[key].default, config: props[key].config, value: props[key].value }
      }
    }
  }
  return temp
}

/**
 * 组件数据clone
 * @param e
 * @returns
 */
export const setCloneData = (e: IConfigComponentItem) => {
  // console.log(e.domInfo)
  const { childrens, props, ...component_info } = e.domInfo
  //将组件默认属性处理为完成组件的格式
  const doneComponent: IDoneComponent = {
    id: e.domInfo.tag + '-' + randomString(),
    childrens: handleConfigChildrens(e.domInfo.childrens),
    props: handleConfigProps(props),
    ...component_info
  }
  //这个数据就是完成组件的格式
  return {
    ...objectDeepClone(doneComponent)
  }
}
