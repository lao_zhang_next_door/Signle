import { IDoneComponent } from '@/views/PageDesigner/model/model'
import { CrudSchema } from '@/hooks/web/useCrudSchemas'
import _ from 'lodash-es'
import { isBoolean, isNumber } from '@/utils/is'

/**
 * form表单转换schema数据
 * @param IDoneComponentData
 * @returns errorMsg or formSchema
 */
export const refDataToFormSchema = (IDoneComponentData: IDoneComponent[]) => {
  let errorMsg = ''
  const schemaFormDataArray: CrudSchema[] = []
  _.forEach(IDoneComponentData, function (value) {
    console.log(value)
    const formContainerCheck = checkFormContainer(value)
    if (formContainerCheck === null) {
      //获取表单项外层的row-rol元素信息
      const rowChildrens = value.childrens
      _.forEach(rowChildrens, function (rowItem) {
        const rolChildrens = rowItem.childrens
        _.forEach(rolChildrens, function (rolItem) {
          //这里默认在rol下面的子元素中，第一个为表单Item元素
          const formItemData = rolItem.childrens[0]
          console.log(formItemData)
          if (typeof formItemData !== 'undefined') {
            const formItemCheck = checkFormItem(formItemData)
            if (formItemCheck === null) {
              const formElement = formItemData.childrens[0]
              const formItemProps = initFormProps(formItemData)
              const formElementProps = initFormProps(formElement)
              const rolProps = initFormProps(rolItem)
              //获取表单元素内容
              const schemaFormDataItem: CrudSchema = {
                field: formElement.columnName,
                label: (formItemProps as any).label,
                form: {
                  component: formElement.componentName,
                  labelMessage: '',
                  formItemProps: {
                    //formItem参数赋值
                    ...formItemProps
                  },
                  //col参数
                  colProps: {
                    ...rolProps
                  },
                  componentProps: {
                    ...formElementProps
                  },
                  value: formElement.v_model
                  //hidden: false,
                  // api: async () => {
                  //   // const res = await getFrameCodeValue({ codeName: t('frameModule.menuType') })
                  //   return ''
                  // }
                }
              }
              if (formElement?.schemaConfig?.dictName.value !== '') {
                ;(schemaFormDataItem.form?.componentProps?.optionsAlias as any).labelField =
                  formElement?.schemaConfig?.optionsAliasLabelField.value
              }
              schemaFormDataArray.push(schemaFormDataItem)
            } else {
              errorMsg = formItemCheck
              return false
            }
          }
          // const form
        })
      })
    } else {
      errorMsg = formContainerCheck
      return false
    }
  })
  console.log(errorMsg)
  if (errorMsg !== '') {
    return errorMsg
  } else {
    return schemaFormDataArray
  }
}

/**
 * form表单转换template代码数据
 * @param IDoneComponentData
 * @returns resultTemplateCode
 */
export const refDataToTemplateCode = (IDoneComponentData: IDoneComponent[]) => {
  let resultTemplateCode = ''
  _.forEach(IDoneComponentData, function (value) {
    const result = getComponentChildrenTemplate(value)
    resultTemplateCode += result
  })
  // console.log(resultTemplateCode)
  return resultTemplateCode
}

/**
 * 组件生成template代码递归
 * @param fatherComponent
 * @returns
 */
const getComponentChildrenTemplate = (fatherComponent: IDoneComponent) => {
  let resultTemplate = ''
  const resultProps = initFormProps(fatherComponent)
  const fatherComponentName = fatherComponent.tag
  let resultPropsStr = ''
  _.forEach(resultProps, function (value, key) {
    let f = ' '
    if (isBoolean(value) || isNumber(value)) {
      f = ' :'
    }
    resultPropsStr += f + key + '="' + value + '"'
  })
  const templateCodeHead = '<' + fatherComponentName + ' ' + resultPropsStr + '>'
  let childrenCode = ''
  _.forEach(fatherComponent.childrens, function (value) {
    const childrenTemplateCode = getComponentChildrenTemplate(value)
    childrenCode += childrenTemplateCode
  })
  resultTemplate += templateCodeHead + childrenCode + '</' + fatherComponentName + '>'
  // console.log(resultTemplate)
  return formatHTML(resultTemplate)
}

/**
 * 检查表单容器是否存在
 * @param formComponentData
 * @returns null or alarm
 */
const checkFormContainer = (formComponentData: IDoneComponent) => {
  //最外层默认包裹表单元素
  if (formComponentData.tag !== 'el-form') {
    return '请在最外层包裹表单容器'
  } else {
    return null
  }
}

/**
 * 检查表单Item是否存在
 * @param formComponentData
 * @returns null or alarm
 */
const checkFormItem = (formComponentData: IDoneComponent) => {
  //在表单元素外面默认有form-item元素
  if (formComponentData.tag !== 'el-form-item') {
    return '请在' + formComponentData.id + '表单元素外层包裹表单Item'
  } else {
    return null
  }
}

/**
 * 生成表单元素props
 * @param formComponentData
 * @returns null or alarm
 */
const initFormProps = (formComponentData: IDoneComponent) => {
  //在表单元素外面默认有form-item元素
  const itemProps = formComponentData.props
  let resultProps = new Object()
  if (typeof itemProps.label !== 'undefined') {
    resultProps = {
      label: formComponentData.props.label
    }
  }
  _.forEach(itemProps, function (value, key) {
    if (value.default !== value.value) {
      //若不是默认值，进行props赋值
      resultProps = Object.assign(resultProps, { [key]: value.value })
    }
  })
  return resultProps
}

/**
 * html元素格式化
 * @param html
 * @returns
 */
const formatHTML = (html: string) => {
  const indentSize = 2
  const indent = ' '.repeat(indentSize)
  const indentLevel = 0
  let result = ''
  let inTag = false
  let inString = false
  let quoteChar = ''
  const currentIndent = ''
  for (let i = 0; i < html.length; i++) {
    const c = html[i]
    if (inTag) {
      if (c === '>') {
        inTag = false
        result += c
        if (html[i - 1] !== '/') {
          result += '\n' + currentIndent
        }
      } else if (c === '"') {
        if (inString && c === quoteChar) {
          inString = false
        } else if (!inString) {
          inString = true
          quoteChar = c
        }
        result += c
      } else {
        result += c
      }
    } else {
      if (c === '<') {
        inTag = true
        result += c
      } else if (c === ' ' || c === '\n') {
        result += c
      } else {
        result += c
        if (html[i + 1] === '<') {
          result += '\n' + currentIndent
        }
      }
    }
  }
  return result
}
