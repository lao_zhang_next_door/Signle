import { getColumnsList } from '@/api/frame-webform/code-generate'
import _ from 'lodash-es'
import { ElFormGroup } from '@/views/PageDesigner/config-center/element/form/index'
import { ElLayoutGroup } from '@/views/PageDesigner/config-center/element/layout/index'
import { IConfigComponentItem } from '@/views/PageDesigner/model/model'
import { setCloneData } from '@/views/PageDesigner/utils'
import { FrameSearch } from '@/types/frame-system/frame-search'
import { CodegenColumn } from '@/types/frame-webform/code-generate-column'

const ElementPlusFormList = ElFormGroup.list

const ElementPlusForm = _.filter(ElementPlusFormList, function (value) {
  if (value.title === '表单容器') {
    return value
  }
}) as unknown as IConfigComponentItem[]

const ElementPlusFormItem = _.filter(ElementPlusFormList, function (value) {
  if (value.title === '表单项') {
    return value
  }
}) as unknown as IConfigComponentItem[]

const ElementPlusRow = _.filter(ElLayoutGroup.list, function (value) {
  if (value.title === '双列栅格') {
    return value
  }
}) as unknown as IConfigComponentItem[]

/**
 * 根据tableId生成表单数据信息
 * @param tableId
 * @returns IConfigComponentItem[]
 */
export const initFormConfigData = async (tableId: number) => {
  const params: FrameSearch = {
    params: {
      tableId_equal: tableId
    },
    currentPage: 1,
    pageSize: 100
  }
  const formComponentArray: IConfigComponentItem[] = []
  let allFormData
  await getColumnsList(params).then((res) => {
    const tableColumns = res.data.records as CodegenColumn[]
    _.forEach(tableColumns, function (value) {
      const formComponent = setFormItemConfig(value)
      // console.log(formComponent.domInfo)
      if (typeof formComponent.domInfo !== 'undefined') {
        formComponentArray.push(formComponent)
      }
    })
    console.log(formComponentArray)
    //这里获取到了所有表单组件，现在进行对外层进行form-item包裹，row默认一行两个
    allFormData = setFormRowConfig(formComponentArray)
    console.log(allFormData)
  })
  return allFormData
}

/**
 * 对表单中的props等属性赋值
 * @param codegenColumn
 * @returns IConfigComponentItem
 */
const setFormItemConfig = (codegenColumn: CodegenColumn) => {
  const htmlType = codegenColumn.htmlType
  let component = {}
  _.forEach(ElementPlusFormList, function (value) {
    const tag = value.domInfo.tag
    // console.log(value.domInfo)
    // console.log(htmlType)
    if (tag === htmlType) {
      //若找到，进行赋值
      value.domInfo.columnName = codegenColumn.javaField
      value.domInfo.title = codegenColumn.columnComment
      component = _.cloneDeep(value)
      // console.log(component)
      return false
    }
  })
  return component as IConfigComponentItem
}

/**
 * 对表单中的Row-rol属性赋值
 * @param codegenColumn
 * @returns IConfigComponentItem
 */
const setFormRowConfig = (iConfigComponentItems: IConfigComponentItem[]) => {
  const form = _.cloneDeep(ElementPlusForm[0])
  let rowRol
  let formItem
  for (let index = 0; index < iConfigComponentItems.length; index++) {
    let flag = false
    formItem = _.cloneDeep(ElementPlusFormItem[0])
    formItem.domInfo.props.label.value = iConfigComponentItems[index].domInfo.title as any
    formItem.domInfo.childrens.push(iConfigComponentItems[index].domInfo)
    if (index % 2 === 0) {
      // console.log(flag)
      rowRol = _.cloneDeep(ElementPlusRow[0])
      //如果数字能被2整除
      rowRol.domInfo.childrens[0].childrens[0] = setCloneData(formItem)
      console.log(rowRol.domInfo.childrens)
    } else {
      // console.log(formItem)
      rowRol.domInfo.childrens[1].childrens[0] = setCloneData(formItem)
      console.log(rowRol.domInfo.childrens)
      flag = true
    }
    if (flag) {
      form.domInfo.childrens.push(setCloneData(rowRol))
    }
  }
  return [setCloneData(form)]
}
