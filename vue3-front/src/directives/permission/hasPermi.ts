import type { App, Directive, DirectiveBinding } from 'vue'
import { useI18n } from '@/hooks/web/useI18n'
import { useCache } from '@/hooks/web/useCache'
import { intersection } from 'lodash-es'
import { isArray } from '@/utils/is'

const { t } = useI18n()
const { wsCache } = useCache()

const hasPermission = (value: string | string[]): boolean => {
  if (!value) {
    throw new Error(t('permission.hasPermission'))
  }
  const buttonPermission = wsCache.get('buttonPermission')
  if (!isArray(value)) {
    const valueArray = value.split(':')
    if (valueArray.length < 2) {
      throw new Error('字符串权限标识无法索引')
      // return false
    }
    const result = buttonPermission[valueArray[0]]?.includes(value) as boolean
    return result
  }

  if (value[0].split(':').length < 2) {
    // return false
    throw new Error('数组权限标识无法索引')
  }
  const indexes = value[0].split(':')[0]
  // console.log(buttonPermission)
  // console.log(indexes)
  const buttonPermissionIndexes = buttonPermission[indexes]
  // console.log(buttonPermissionIndexes)
  return intersection(value, buttonPermissionIndexes).length > 0
}
function hasPermi(el: Element, binding: DirectiveBinding) {
  const value = binding.value

  const flag = hasPermission(value)
  if (!flag) {
    el.parentNode?.removeChild(el)
  }
}
const mounted = (el: Element, binding: DirectiveBinding<any>) => {
  hasPermi(el, binding)
}

const permiDirective: Directive = {
  mounted
}

export const setupPermissionDirective = (app: App<Element>) => {
  app.directive('hasPermi', permiDirective)
}

export default permiDirective
