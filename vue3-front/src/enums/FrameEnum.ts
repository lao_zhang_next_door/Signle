/**
 * 是否枚举
 */
export enum TrueOrFalseEnum {
  /**是**/
  True = 1,
  /**否**/
  False = 0
}

/**
 * html表单类型枚举
 */
export const FormTypeEnum = {
  input: { value: '文本框', elName: 'el-input', configName: 'D_ElInput' },
  select: { value: '下拉框', elName: 'el-select', configName: 'D_ElSelect' },
  autocomplete: { value: '自动补全输入框', elName: 'el-autocomplete' },
  cascader: { value: '级联选择框', elName: 'el-cascader' },
  radio: { value: '单选框', elName: 'el-radio' },
  checkbox: { value: '复选框', elName: 'el-checkbox' },
  inputNumber: { value: '数字输入框', elName: 'el-input-number' },
  datePicker: { value: '日期选择', elName: 'el-date-picker' },
  rate: { value: '评分框', elName: 'el-rate' },
  slider: { value: '滑块', elName: 'el-slider' },
  switch: { value: '开关按钮', elName: 'el-switch' },
  fileUpload: { value: '文件上传', elName: 'el-upload' },
  editor: { value: '富文本控件', elName: 'editor' },
  colorPicker: { value: '取色器', elName: 'el-color-picker' }
}
