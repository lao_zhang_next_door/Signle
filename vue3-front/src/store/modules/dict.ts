import { ComponentOptionsAlias } from '@/types/components'
import { defineStore } from 'pinia'
import { store } from '../index'
import { useI18n } from '@/hooks/web/useI18n'

export interface DictState {
  isSetDict: boolean
  dictObj: Recordable
}

interface DictObjType extends Recordable {
  color?: string
  //用于区分值的别名
  valueName?: string
  //用于区分显示的别名
  labelName?: string
}

export const useDictStore = defineStore({
  id: 'dict',
  state: (): DictState => ({
    isSetDict: false,
    dictObj: {}
  }),
  getters: {
    getDictObj(): Recordable {
      return this.dictObj
    },
    getIsSetDict(): boolean {
      return this.isSetDict
    }
  },
  actions: {
    setDictObj(dictObj: Recordable, labelOrValue: ComponentOptionsAlias | undefined) {
      // console.log(dictObj)
      if (typeof labelOrValue === 'undefined') {
        return
      }
      const newArray: Array<any> = []
      for (const item of dictObj.dictValues) {
        const obj: DictObjType = {
          color: item.colorStyle,
          valueName: labelOrValue?.valueField as string,
          labelName: labelOrValue?.labelField as string
        }
        obj[labelOrValue?.valueField as string] = item[labelOrValue?.valueField as string]
        obj[labelOrValue?.labelField as string] = item[labelOrValue?.labelField as string]
        newArray.push(obj)
      }
      this.dictObj[dictObj.dictName] = newArray
    },
    setIsSetDict(isSetDict: boolean) {
      this.isSetDict = isSetDict
    },
    getDictObjByName(name: string, value: string | number): DictObjType {
      let result: DictObjType = {}
      const { t } = useI18n()
      //console.log(name, value)
      if (name.indexOf('.') !== -1) {
        name = t(name)
      }
      //console.log(name, value)
      const dictValueArray = this.dictObj[name]
      // console.log(name, dictValueArray)
      for (const item of dictValueArray) {
        const valueName = item.valueName
        if (item[valueName] == value) {
          result = item
          break
        }
      }
      return result
    }
  }
})

export const useDictStoreWithOut = () => {
  return useDictStore(store)
}
