import { defineStore } from 'pinia'
import { getAccessToken, removeAccessToken } from '@/utils/auth'
import { getUserInfo } from '@/api/frame-system/frame-login'
import { useCache } from '@/hooks/web/useCache'
import { useAppStore } from '@/store/modules/app'
import { userLogout } from '@/api/frame-system/frame-login'
import { FrameUserVo } from '@/types/frame-system/frame-user'
interface UserState {
  token: string
  userInfo: FrameUserVo
}

export const useUserStore = defineStore({
  id: 'user',
  persist: {
    storage: sessionStorage,
    key: 'user'
  },
  state: (): UserState => ({
    token: getAccessToken(),
    userInfo: {
      rowGuid: '',
      userName: '',
      userGuid: '',
      deptName: '',
      deptGuid: '',
      mobile: '',
      loginId: '',
      roleNameList: [],
      roleGuidList: [],
      token: '',
      headimgGuid: '',
      headimgGuidVal: '',
      deptCode: '',
      status: 0
    }
  }),
  getters: {
    getHeadImgGuid(): string | undefined {
      return this.userInfo.headimgGuid
    },
    getName(): string {
      return this.userInfo.userName
    },
    getDeptGuid(): string | undefined {
      return this.userInfo.deptGuid
    },
    getRoles(): Array<string> {
      return this.userInfo.roleNameList as string[]
    },
    getUserInfo(): FrameUserVo {
      return this.userInfo
    }
  },
  actions: {
    setCurrentUserInfo(data: FrameUserVo) {
      Object.keys(this.userInfo).forEach((key) => (this.userInfo[key] = data[key]))
    },
    getCurrentUserInfo() {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then((res) => {
            console.log(res.data)
            if (!res.data) {
              reject('Verification failed, please Login again.')
            }
            const { wsCache } = useCache()
            const appStore = useAppStore()
            const { roleNameList, userName } = res.data
            // roles must be a non-empty array
            if (!roleNameList || roleNameList.length <= 0) {
              reject('getInfo: roleNames must be a non-null array!')
            }
            Object.keys(this.userInfo).forEach((key) => (this.userInfo[key] = res.data[key]))
            wsCache.set(appStore.getUserInfo, res.data)
            // this.roles = roleNameList
            // this.rolesGuids = roleGuidList
            // this.name = userName
            // this.userInfo = data
            // commit('SET_AVATAR', avatar)
            resolve(res.data)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    LogOut() {
      return new Promise((resolve) => {
        console.log('登出...')
        userLogout().then(() => {
          Object.keys(this.userInfo).forEach((key) => (this.userInfo[key] = ''))
          //this.userInfo = {}
          removeAccessToken()
        })
        resolve(1)
      })
    }
  }
})
