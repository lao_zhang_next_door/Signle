/**
 * 信息发布中文字符串定义
 */
export default {
  informationInfo: {
    infoTitle: '信息标题',
    infoContent: '信息内容',
    releaseTime: '发布时间',
    infoCreateUser: '创建人',
    infoCreateUserGuid: '创建人guid',
    infoFile: '信息附件',
    clickTimes: '点击次数',
    infoPic: '图片附件',
    infoCategoryGuid: '信息栏目',
    infoType: '信息类型',
    infoUrl: '链接地址',
    infoStatus: '信息状态',
    isShowInformation: '是否显示信息'
  },
  informationCategory: {
    categoryName: '栏目名称',
    pcategoryCode: '父栏目',
    description: '栏目描述',
    isNeedAudit: '是否需要审核'
  }
}
