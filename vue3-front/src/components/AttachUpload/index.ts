import AvatarUpload from './src/AvatarUpload.vue'
import FileUpload from './src/FileUpload.vue'
import PictureUpload from './src/PictureUpload.vue'

export { AvatarUpload, FileUpload, PictureUpload }
