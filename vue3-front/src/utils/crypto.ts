import EcbMode from 'crypto-js/mode-ecb'
import MD5 from 'crypto-js/md5'
import ZeroPadding from 'crypto-js/pad-zeropadding'
import SHA256 from 'crypto-js/sha256'
import PadPkcs7 from 'crypto-js/pad-pkcs7'
import EncUtf8 from 'crypto-js/enc-utf8'
import EncHex from 'crypto-js/enc-hex'
import AES from 'crypto-js/aes'
import EncBase64 from 'crypto-js/enc-base64'
import { JSEncrypt } from 'encryptlong'
import moment from 'moment'
import { uuid } from './uuid'
export interface CryptoType {
  encrypt: any
  decrypt: any
  encryptMD5: any
}

class CryptoUtil implements CryptoType {
  private key = 'sinoway1234'
  // 私钥，用于防止别人冒充签名
  private secret = '13245678'
  // RSA公钥，后端接口获取, 储存在前端
  private RSA_KEY = import.meta.env.VITE_PUBLIC_KEY
  // AES密钥，在前端生成
  private AES_KEY = this.getKeyAES()
  // 加密
  encrypt(word: string, keyStr: string) {
    keyStr = keyStr || this.key // 判断是否存在ksy，不存在就用定义好的key
    const keyHex = EncUtf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    const wordHex = EncUtf8.parse(word)
    const encrypted = AES.encrypt(wordHex, keyHex, {
      mode: EcbMode,
      padding: PadPkcs7
    })
    return encrypted.ciphertext.toString(EncBase64)
  }
  // 解密
  decrypt(word: string, keyStr: string) {
    keyStr = keyStr || this.key
    const keyHex = EncUtf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    const decrypted = AES.decrypt(word, keyHex, {
      mode: EcbMode,
      padding: PadPkcs7
    })
    return EncUtf8.stringify(decrypted).toString()
  }

  /**
   * 随机生成16位的AES密钥
   */
  getKeyAES() {
    const key: string[] = []
    for (let i = 0; i < 16; i++) {
      const num = Math.floor(Math.random() * 26)
      const charStr = String.fromCharCode(97 + num)
      key.push(charStr.toUpperCase())
    }
    const result = key.join('')
    return result
  }

  /**
   * AES加密
   * 转Utf8编码: CryptoJS.enc.Utf8.parse();
   * 转Base64: CryptoJS.enc.Base64.stringify();
   * @param data 需要加密的数据
   * @param key 密钥
   * @returns 加密后的数据
   */
  encodeAES(data: any, key: string) {
    if (typeof data !== 'string') {
      data = JSON.stringify(data)
    }
    // AES加密
    const result = AES.encrypt(data, EncUtf8.parse(key), {
      // 向量。使用CBC模式时，需要使用向量。
      iv: EncUtf8.parse('1234567812345678'),
      mode: CryptoJS.mode.CBC,
      // 偏移量。使用非补码方式时，需要使用ZeroPadding。默认为PKCS5Padding。
      padding: ZeroPadding
    })
    // base64转码
    return EncBase64.stringify(result.ciphertext)
  }

  /**
   * AES解密
   * @param data 需要解密的数据
   * @param key 密钥
   * @returns 解密后的数据
   */
  decodeAES(data, key) {
    if (typeof data !== 'string') {
      data = JSON.stringify(data)
    }
    const result = AES.decrypt(data, EncUtf8.parse(key), {
      iv: EncUtf8.parse('1234567812345678'), // 向量。使用CBC模式时，需要使用向量。
      mode: CryptoJS.mode.CBC,
      padding: ZeroPadding // 偏移量。使用非补码方式时，需要使用ZeroPadding。默认为PKCS5Padding。
    })
    // 转为utf-8编码
    return EncUtf8.stringify(result)
  }

  /**
   * MD5加密
   * @word
   */
  encryptMD5(word: string) {
    const bytes = MD5(word)
    return bytes.toString().toLowerCase()
  }
  /**
   * RSA加密
   * @param data 需要加密的数据
   * @param key 密钥
   * @returns 加密后的数据
   */
  encodeRSA(data: string, key: string) {
    const encryptTool = new JSEncrypt()
    encryptTool.setPublicKey(key)
    const result = encryptTool.encryptLong(data) as string
    return result
  }

  /**
   * RSA解密
   * @param data 需要解密的数据
   * @param key 密钥
   * @returns 解密后的数据
   */
  decodeRSA(data: string, key: string) {
    const encryptTool = new JSEncrypt()
    encryptTool.setPrivateKey(key)
    const result = encryptTool.decrypt(data)
    return result as string
  }

  /**
   * 签名,支持SHA256签名与SHA1签名
   * @param data 需要签名的数据
   * @param key SHA1签名的密钥
   */
  signature(data: any, key: string) {
    let params = ''
    // 先对data排序，然后拼接字符串
    Object.keys(data)
      .sort()
      .forEach((item) => {
        params += `${item}=${data[item]}, `
      })
    params = params.slice(0, -2)
    // SHA256签名：使用CryptoJS.SHA256(),先将SHA256加密,然后转Hex的16进制
    return SHA256(`{${params}}`).toString(EncHex)
  }

  /**
   * 数据签名加密
   * @param data
   */
  normalsignatureEncode(data: string) {
    // 当前时间戳
    const timestamp = moment().format('X')
    const uuidStr = uuid(null, null)
    const dataForm: signatureParams = {
      timestamp: timestamp,
      nonce: uuidStr,
      data: data,
      key: this.secret
    }
    let params = ''
    // 先对data排序，然后拼接字符串
    Object.keys(dataForm)
      .sort()
      .forEach((item) => {
        params += `${item}=${dataForm[item]}, `
      })
    params = params.slice(0, -2)
    dataForm.signStr = this.encryptMD5(params)
    delete dataForm.key
    return dataForm
  }

  /**
   * 加密
   * @param data 接口请求参数
   * 说明：请求参数data + 公共参数、AES加密、RSA加密、SHA签名
   * 另外密码等敏感参数, 需要提前单独加密
   */
  encode(data: any) {
    const { secret, AES_KEY, RSA_KEY } = this
    const requestDataEnc = this.encodeAES(data, AES_KEY) // 所有请求参数进行AES加密
    const encodeKey = this.encodeRSA(AES_KEY, RSA_KEY) // AES的密钥进行RSA加密
    const timestamp = moment(Date.now(), 'YYYY-MM-DD HH:mm:ss').valueOf() // 当前时间戳
    const result = {
      secret: secret,
      encodeKey: encodeKey,
      requestDataEnc: requestDataEnc,
      timestamp: timestamp,
      sign: ''
    }
    // 签名
    result.sign = this.signature(result, '')
    result.secret = ''
    // 防止私钥泄露移除。
    console.log(result)
    return result
  }
  /**
   * 解密
   * @param 返回json数据
   */
  decode(json: any) {
    const { AES_KEY } = this
    let result = this.decodeAES(json, AES_KEY)
    result = JSON.parse(result)
    return result
  }
  // 加密密码
  encodePwd(data: string) {
    const { RSA_KEY } = this
    return this.encodeRSA(data, RSA_KEY)
  }
}

export default new CryptoUtil()
