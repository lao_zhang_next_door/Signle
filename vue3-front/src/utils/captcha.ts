import CryptoJS from 'crypto-js'
/**
 * 图形验证码工具
 * @param {*} vm
 * @returns
 */
export function resetSize(vm) {
  let img_width, img_height, bar_width, bar_height
  // 图片的宽度、高度，移动条的宽度、高度
  console.log(vm)
  const parentWidth = vm.ctx.$el.parentNode.offsetWidth || window.offsetWidth
  const parentHeight = vm.ctx.$el.parentNode.offsetHeight || window.offsetHeight

  if (vm.ctx.imgSize.width.indexOf('%') !== -1) {
    img_width = (parseInt(vm.ctx.imgSize.width) / 100) * parentWidth + 'px'
  } else {
    img_width = vm.ctx.imgSize.width
  }

  if (vm.ctx.imgSize.height.indexOf('%') !== -1) {
    img_height = (parseInt(vm.ctx.imgSize.height) / 100) * parentHeight + 'px'
  } else {
    img_height = vm.ctx.imgSize.height
  }

  if (vm.ctx.barSize.width.indexOf('%') !== -1) {
    bar_width = (parseInt(vm.ctx.barSize.width) / 100) * parentWidth + 'px'
  } else {
    bar_width = vm.ctx.barSize.width
  }

  if (vm.ctx.barSize.height.indexOf('%') !== -1) {
    bar_height = (parseInt(vm.ctx.barSize.height) / 100) * parentHeight + 'px'
  } else {
    bar_height = vm.ctx.barSize.height
  }
  console.log({
    imgWidth: img_width,
    imgHeight: img_height,
    barWidth: bar_width,
    barHeight: bar_height
  })
  return { imgWidth: img_width, imgHeight: img_height, barWidth: bar_width, barHeight: bar_height }
}

export const _code_chars = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z'
]
export const _code_color1 = ['#fffff0', '#f0ffff', '#f0fff0', '#fff0f0']
export const _code_color2 = ['#FF0033', '#006699', '#993366', '#FF9900', '#66CC66', '#FF33CC']

/**
 * 滑动验证
 * @word 要加密的内容
 * @keyWord String  服务器随机返回的关键字
 *  */
export function captchaAesEncrypt(word, keyWord) {
  console.log(word)
  console.log(keyWord)
  const key = CryptoJS.enc.Utf8.parse(keyWord)
  const srcs = CryptoJS.enc.Utf8.parse(word)
  const encrypted = CryptoJS.AES.encrypt(srcs, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  const encryptedBase64Data = CryptoJS.enc.Base64.stringify(encrypted.ciphertext)
  return encryptedBase64Data
}
