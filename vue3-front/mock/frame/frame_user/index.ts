import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const moduleList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    userName: '阿力1',
    deptName: '摸鱼部',
    mobile: '123456',
    loginId: 'wzl1',
    statusText: '正常'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    userName: '阿力2',
    deptName: '摸鱼部',
    mobile: '1234567',
    loginId: 'wzl2',
    statusText: '正常'
  },
  {
    rowGuid: '3',
    sortSq: '3',
    userName: '阿力3',
    deptName: '摸鱼部',
    mobile: '12345678',
    loginId: 'wzl3',
    statusText: '正常'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameUser/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: moduleList, total: moduleList.length }
  //     }
  //   }
  // }
] as MockMethod[]
