import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const codeValueList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    itemValue: '1',
    itemText: '测试1',
    colorStyle: 'red'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    itemValue: '2',
    itemText: '测试2',
    colorStyle: 'blue'
  },
  {
    rowGuid: '3',
    sortSq: '3',
    itemValue: '3',
    itemText: '测试3',
    colorStyle: 'green'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameCode/selectCacheByCodeName',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: codeValueList
  //     }
  //   }
  // },
  // {
  //   url: '/sino-api/frame/FrameCodeValue/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: codeValueList, total: codeValueList.length }
  //     }
  //   }
  // }
] as MockMethod[]
