import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const deptList: Recordable = [
  {
    rowGuid: '123',
    sortSq: '1',
    // 部门编号
    deptCode: '001',
    // 部门名称
    deptName: '测试1',
    // 上级部门编号
    pdeptCode: '000',
    // 部门简称
    shortName: 'test1',
    // 部门编码
    ouCode: 't001',
    // 电话
    tel: '110',
    // 传真
    fax: '110110',
    // 地址
    address: '???????',
    // 描述
    description: '没有',
    // 父级部门名称
    pdeptName: '根目录'
  },
  {
    rowGuid: '123456',
    sortSq: '2',
    // 部门编号
    deptCode: '002',
    // 部门名称
    deptName: '测试2',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test2',
    // 部门编码
    ouCode: 't002',
    // 电话
    tel: '220',
    // 传真
    fax: '220220',
    // 地址
    address: '???????2',
    // 描述
    description: '没有2',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '123456789',
    sortSq: '3',
    // 部门编号
    deptCode: '003',
    // 部门名称
    deptName: '测试3',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test3',
    // 部门编码
    ouCode: 't003',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567894',
    sortSq: '4',
    // 部门编号
    deptCode: '004',
    // 部门名称
    deptName: '测试4',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test4',
    // 部门编码
    ouCode: 't004',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567895',
    sortSq: '5',
    // 部门编号
    deptCode: '005',
    // 部门名称
    deptName: '测试5',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test5',
    // 部门编码
    ouCode: 't005',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567896',
    sortSq: '6',
    // 部门编号
    deptCode: '006',
    // 部门名称
    deptName: '测试6',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test6',
    // 部门编码
    ouCode: 't006',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567897',
    sortSq: '7',
    // 部门编号
    deptCode: '007',
    // 部门名称
    deptName: '测试7',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test7',
    // 部门编码
    ouCode: 't003',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567898',
    sortSq: '8',
    // 部门编号
    deptCode: '008',
    // 部门名称
    deptName: '测试8',
    // 上级部门编号
    pdeptCode: '008',
    // 部门简称
    shortName: 'test8',
    // 部门编码
    ouCode: 't008',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  },
  {
    rowGuid: '1234567899',
    sortSq: '9',
    // 部门编号
    deptCode: '009',
    // 部门名称
    deptName: '测试9',
    // 上级部门编号
    pdeptCode: '001',
    // 部门简称
    shortName: 'test9',
    // 部门编码
    ouCode: 't009',
    // 电话
    tel: '330',
    // 传真
    fax: '330330',
    // 地址
    address: '???????3',
    // 描述
    description: '没有3',
    // 父级部门名称
    pdeptName: '测试1'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameDept/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: deptList, total: deptList.length }
  //     }
  //   }
  // },
  // {
  //   url: '/sino-api/frame/FrameDept/allTreeData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: [
  //         {
  //           rowGuid: '123',
  //           sortSq: '1',
  //           // 部门编号
  //           deptCode: '001',
  //           // 部门名称
  //           deptName: '测试1',
  //           // 上级部门编号
  //           pdeptCode: '000',
  //           // 部门简称
  //           shortName: 'test1',
  //           // 部门编码
  //           ouCode: 't001',
  //           // 电话
  //           tel: '110',
  //           // 传真
  //           fax: '110110',
  //           // 地址
  //           address: '???????',
  //           // 描述
  //           description: '没有',
  //           // 父级部门名称
  //           pdeptName: '根目录',
  //           chilren: [
  //             {
  //               rowGuid: '123456',
  //               sortSq: '2',
  //               // 部门编号
  //               deptCode: '002',
  //               // 部门名称
  //               deptName: '测试2',
  //               // 上级部门编号
  //               pdeptCode: '001',
  //               // 部门简称
  //               shortName: 'test2',
  //               // 部门编码
  //               ouCode: 't002',
  //               // 电话
  //               tel: '220',
  //               // 传真
  //               fax: '220220',
  //               // 地址
  //               address: '???????2',
  //               // 描述
  //               description: '没有2',
  //               // 父级部门名称
  //               pdeptName: '测试1'
  //             },
  //             {
  //               rowGuid: '123456789',
  //               sortSq: '3',
  //               // 部门编号
  //               deptCode: '003',
  //               // 部门名称
  //               deptName: '测试3',
  //               // 上级部门编号
  //               pdeptCode: '001',
  //               // 部门简称
  //               shortName: 'test3',
  //               // 部门编码
  //               ouCode: 't003',
  //               // 电话
  //               tel: '330',
  //               // 传真
  //               fax: '330330',
  //               // 地址
  //               address: '???????3',
  //               // 描述
  //               description: '没有3',
  //               // 父级部门名称
  //               pdeptName: '测试1'
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   }
  // }
] as MockMethod[]
