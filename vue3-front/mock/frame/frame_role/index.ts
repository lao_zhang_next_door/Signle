import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const roleList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    roleName: '测试1',
    roleType: '1',
    roleTypeText: '类型1'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    roleName: '测试2',
    roleType: '2',
    roleTypeText: '类型2'
  },
  {
    rowGuid: '3',
    sortSq: '3',
    roleName: '测试3',
    roleType: '3',
    roleTypeText: '类型3'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameRole/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: roleList, total: roleList.length }
  //     }
  //   }
  // }
] as MockMethod[]
