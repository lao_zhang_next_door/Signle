import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const codeList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    codeName: '测试1',
    codeMask: '1'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    codeName: '测试2',
    codeMask: '2'
  },
  {
    rowGuid: '3',
    sortSq: '3',
    codeName: '测试3',
    codeMask: '1'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameCode/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: codeList, total: codeList.length }
  //     }
  //   }
  // }
] as MockMethod[]
