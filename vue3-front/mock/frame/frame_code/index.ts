import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const configList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    codeName: '测试1',
    configValue: '1',
    categoryTypeText: '系统'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    codeName: '测试2',
    configValue: '2',
    categoryTypeText: '微信'
  },
  {
    rowGuid: '3',
    sortSq: '3',
    codeName: '测试3',
    configValue: '3',
    categoryTypeText: '系统'
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameConfig/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: configList, total: configList.length }
  //     }
  //   }
  // }
] as MockMethod[]
