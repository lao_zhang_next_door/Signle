import { config } from '@/config/axios/config'
import { MockMethod } from 'vite-plugin-mock'

const { result_code } = config

const timeout = 500

const moduleList: Recordable = [
  {
    rowGuid: '1',
    sortSq: '1',
    moduleName: '测试1',
    parentGuid: '123',
    hasChild: 1,
    moduleAddress: '123',
    smallIcon: '',
    bigIcon: '',
    target: '',
    isVisible: '1',
    redirect: '123',
    path: '123'
  },
  {
    rowGuid: '2',
    sortSq: '2',
    moduleName: '测试2',
    parentGuid: '1',
    hasChild: 1,
    moduleAddress: '2',
    smallIcon: '',
    bigIcon: '',
    target: '',
    isVisible: '1',
    redirect: '',
    path: ''
  },
  {
    rowGuid: '3',
    sortSq: '3',
    moduleName: '测试3',
    parentGuid: '3',
    hasChild: 1,
    moduleAddress: '3',
    smallIcon: '',
    bigIcon: '',
    target: '',
    isVisible: '1',
    redirect: '',
    path: ''
  }
]

export default [
  // {
  //   url: '/sino-api/frame/FrameModule/listData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: { list: moduleList, total: moduleList.length }
  //     }
  //   }
  // },
  // {
  //   url: '/sino-api/frame/FrameModule/allTreeData',
  //   method: 'get',
  //   timeout,
  //   response: () => {
  //     return {
  //       code: result_code,
  //       data: [
  //         {
  //           rowGuid: '1',
  //           sortSq: '1',
  //           moduleName: '测试1',
  //           parentGuid: '123',
  //           hasChild: 1,
  //           moduleAddress: '123',
  //           smallIcon: '',
  //           bigIcon: '',
  //           target: '',
  //           isVisible: '1',
  //           redirect: '123',
  //           path: '123',
  //           children: [
  //             {
  //               rowGuid: '2',
  //               sortSq: '2',
  //               moduleName: '测试2',
  //               parentGuid: '1',
  //               hasChild: 1,
  //               moduleAddress: '2',
  //               smallIcon: '',
  //               bigIcon: '',
  //               target: '',
  //               isVisible: '1',
  //               redirect: '',
  //               path: ''
  //             },
  //             {
  //               rowGuid: '3',
  //               sortSq: '3',
  //               moduleName: '测试3',
  //               parentGuid: '3',
  //               hasChild: 1,
  //               moduleAddress: '3',
  //               smallIcon: '',
  //               bigIcon: '',
  //               target: '',
  //               isVisible: '1',
  //               redirect: '',
  //               path: ''
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   }
  // }
] as MockMethod[]
