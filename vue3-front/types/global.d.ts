import type { CSSProperties } from 'vue'
declare global {
  declare interface Fn<T = any> {
    (...arg: T[]): T
  }

  declare type Nullable<T> = T | null

  declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>

  declare type Recordable<T = any, K = string> = Record<K extends null | undefined ? string : K, T>

  declare type ComponentRef<T> = InstanceType<T>

  declare type LocaleType = 'zh-CN' | 'en'

  declare type AxiosHeaders =
    | 'application/json'
    | 'application/x-www-form-urlencoded'
    | 'multipart/form-data'

  declare type AxiosMethod = 'get' | 'post' | 'delete' | 'put'

  declare type AxiosResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'

  declare interface AxiosConfig {
    params?: any
    data?: any
    url?: string
    method?: AxiosMethod
    headersType?: string
    responseType?: AxiosResponseType
    //是否需要签名加密
    needSignature?: boolean
    //是否需要RSA加密
    needRSA?: boolean
  }

  declare interface IResponse<T = any> {
    code: string
    data: T extends any ? T : T & any
    userInfo?: T extends any ? T : T & any
    accessToken?: string
  }

  declare interface signatureParams {
    //数据
    data?: string
    //签名字符串
    signStr?: string
    //时间戳
    timestamp?: string
    //加密秘钥
    key?: string
    //随机字符串
    nonce?: string
  }

  declare interface Window {
    $loginOutFunction: any
    offsetWidth: any
    offsetHeight: any
  }
}
