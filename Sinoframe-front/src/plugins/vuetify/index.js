import Vue from "vue";

import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader
import Vuetify from "vuetify";

import "vuetify/dist/vuetify.min.css";
import zhHans from "vuetify/lib/locale/zh-Hans";
import preset from "./default-preset/preset";

Vue.use(Vuetify);

export default new Vuetify({
  preset,
  lang: {
    locales: { zhHans },
    current: "zhHans",
  },
  icons: {
    iconfont: "mdi",
  },
  theme: {
    options: {
      customProperties: true,
      variations: false,
    },
    themeCache: {
      get: (key) => localStorage.getItem(key),
      set: (key, value) => localStorage.setItem(key, value),
    },
  },
});
