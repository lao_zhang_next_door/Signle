import request from "@/utils/request";

// // 获取模块
// export function getKeyDefineList() {
//   return request({
//     url: "/monitor/redis/get-key-define-list",
//     method: "get",
//   });
// }

// // 获取键名列表
// export function getKeyList(keyTemplate) {
//   return request({
//     url: "/monitor/redis/get-key-list",
//     method: "get",
//     params: {
//       keyTemplate,
//     },
//   });
// }

// // 获取缓存内容
// export function getKeyValue(key) {
//   return request({
//     url: "/monitor/redis/get-key-value?key=" + key,
//     method: "get",
//   });
// }

// // 根据键名删除缓存
// export function deleteKey(key) {
//   return request({
//     url: "/monitor/redis/delete-key?key=" + key,
//     method: "delete",
//   });
// }

// export function deleteKeys(keyTemplate) {
//   return request({
//     url: "/monitor/redis/delete-keys?",
//     method: "delete",
//     params: {
//       keyTemplate,
//     },
//   });
// }

// 查询缓存详细
export function getCache() {
  return request({
    url: "/monitor/redis",
    method: "get",
  });
}

// 查询缓存名称列表
export function listCacheName() {
  return request({
    url: "/monitor/redis/getNames",
    method: "get",
  });
}

// 查询缓存键名列表
export function listCacheKey(cacheName, keyType) {
  return request({
    url: "/monitor/redis/getKeys/" + keyType + "/" + cacheName,
    method: "get",
  });
}

// 查询缓存内容
export function getCacheValue(cacheName, cacheKey, keyType) {
  return request({
    url:
      "/monitor/redis/getValue/" + keyType + "/" + cacheName + "/" + cacheKey,
    method: "get",
  });
}

// 清理指定名称缓存
export function clearCacheName(cacheName) {
  return request({
    url: "/monitor/redis/clearCacheName/" + cacheName,
    method: "delete",
  });
}

// 清理指定键名缓存
export function clearCacheKey(cacheName, cacheKey, keyType) {
  return request({
    url:
      "/monitor/redis/clearCacheKey/" +
      keyType +
      "/" +
      cacheName +
      "/" +
      cacheKey,
    method: "delete",
  });
}

// 清理全部缓存
export function clearCacheAll() {
  return request({
    url: "/monitor/redis/clearCacheAll",
    method: "delete",
  });
}
