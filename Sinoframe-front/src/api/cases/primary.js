import request from '@/utils/request'

// 创建案件
export function createPrimary(data) {
  return request({
    url: '/cases/primary/create',
    method: 'post',
    data: data
  })
}

// 更新案件
export function updatePrimary(data) {
  return request({
    url: '/cases/primary/update',
    method: 'put',
    data: data
  })
}

// 删除案件
export function deletePrimary(rowId) {
  return request({
    url: '/cases/primary/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除案件
export function deletePrimaryBatch(data) {
  return request({
    url: '/cases/primary/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得案件
export function getPrimary(id) {
  return request({
    url: '/cases/primary/get?id=' + id,
    method: 'get'
  })
}

// 获得案件分页
export function getPrimaryPage(query) {
  return request({
    url: '/cases/primary/page',
    method: 'get',
    params: query
  })
}

