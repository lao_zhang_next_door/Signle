import request from '@/utils/request'

// 创建机构变更记录
export function createFirmChangeRecord(data) {
  return request({
    url: '/law/firm-change-record/create',
    method: 'post',
    data: data
  })
}

// 更新机构变更记录
export function updateFirmChangeRecord(data) {
  return request({
    url: '/law/firm-change-record/update',
    method: 'put',
    data: data
  })
}

// 删除机构变更记录
export function deleteFirmChangeRecord(rowId) {
  return request({
    url: '/law/firm-change-record/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除机构变更记录
export function deleteFirmChangeRecordBatch(data) {
  return request({
    url: '/law/firm-change-record/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得机构变更记录
export function getFirmChangeRecord(id) {
  return request({
    url: '/law/firm-change-record/get?id=' + id,
    method: 'get'
  })
}

// 获得机构变更记录分页
export function getFirmChangeRecordPage(query) {
  return request({
    url: '/law/firm-change-record/page',
    method: 'get',
    params: query
  })
}

