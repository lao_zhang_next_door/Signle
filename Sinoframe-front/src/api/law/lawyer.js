import request from '@/utils/request'

// 创建律师
export function createLawyer(data) {
  return request({
    url: '/law/lawyer/create',
    method: 'post',
    data: data
  })
}

// 更新律师
export function updateLawyer(data) {
  return request({
    url: '/law/lawyer/update',
    method: 'put',
    data: data
  })
}

// 删除律师
export function deleteLawyer(rowId) {
  return request({
    url: '/law/lawyer/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除律师
export function deleteLawyerBatch(data) {
  return request({
    url: '/law/lawyer/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得律师
export function getLawyer(id) {
  return request({
    url: '/law/lawyer/get?id=' + id,
    method: 'get'
  })
}

// 获得律师分页
export function getLawyerPage(query) {
  return request({
    url: '/law/lawyer/page',
    method: 'get',
    params: query
  })
}

