import request from '@/utils/request'

// 创建律师事务所
export function createFirm(data) {
  return request({
    url: '/law/firm/create',
    method: 'post',
    data: data
  })
}

// 更新律师事务所
export function updateFirm(data) {
  return request({
    url: '/law/firm/update',
    method: 'put',
    data: data
  })
}

// 删除律师事务所
export function deleteFirm(id) {
  return request({
    url: '/law/firm/delete?rowId=' + id,
    method: 'delete'
  })
}

// 批量删除律师事务所
export function deleteFirmBatch(data) {
  return request({
    url: '/law/firm/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得律师事务所
export function getFirm(id) {
  return request({
    url: '/law/firm/get?id=' + id,
    method: 'get'
  })
}

// 获得律师事务所分页
export function getFirmPage(query) {
  return request({
    url: '/law/firm/page',
    method: 'get',
    params: query
  })
}

// 获得所有的律所数据
export function getAllFirmData() {
  return request({
    url: '/law/firm/getAllFirmData',
    method: 'get'
  })
}

