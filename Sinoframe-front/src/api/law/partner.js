import request from '@/utils/request'

// 创建事务所合伙人
export function createPartner(data) {
  return request({
    url: '/law/partner/create',
    method: 'post',
    data: data
  })
}

// 更新事务所合伙人
export function updatePartner(data) {
  return request({
    url: '/law/partner/update',
    method: 'put',
    data: data
  })
}

// 删除事务所合伙人
export function deletePartner(rowId) {
  return request({
    url: '/law/partner/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除事务所合伙人
export function deletePartnerBatch(data) {
  return request({
    url: '/law/partner/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得事务所合伙人
export function getPartner(id) {
  return request({
    url: '/law/partner/get?id=' + id,
    method: 'get'
  })
}

// 获得事务所合伙人分页
export function getPartnerPage(query) {
  return request({
    url: '/law/partner/page',
    method: 'get',
    params: query
  })
}

