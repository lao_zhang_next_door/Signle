import request from '@/utils/request'

// 创建实习经历
export function createApprenticeLawyer(data) {
  return request({
    url: '/law/apprentice-lawyer/create',
    method: 'post',
    data: data
  })
}

// 更新实习经历
export function updateApprenticeLawyer(data) {
  return request({
    url: '/law/apprentice-lawyer/update',
    method: 'put',
    data: data
  })
}

// 删除实习经历
export function deleteApprenticeLawyer(rowId) {
  return request({
    url: '/law/apprentice-lawyer/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除实习经历
export function deleteApprenticeLawyerBatch(data) {
  return request({
    url: '/law/apprentice-lawyer/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得实习经历
export function getApprenticeLawyer(id) {
  return request({
    url: '/law/apprentice-lawyer/get?id=' + id,
    method: 'get'
  })
}

// 获得实习经历分页
export function getApprenticeLawyerPage(query) {
  return request({
    url: '/law/apprentice-lawyer/page',
    method: 'get',
    params: query
  })
}

