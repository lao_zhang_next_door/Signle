import request from '@/utils/request'

/**
 * 数据列表
 * @param {Object} data
 */
export function listData(data) {
  return request({
    url: '/webform/FormTablefield/listData',
    method: 'post',
    data
  })
}
/**
 * 删除
 * @param {Object} data
 */
export function deleteData(data) {
  return request({
    url: '/webform/FormTablefield/deleteTableField',
    method: 'post',
    data
  })
}
/**
 * 更新
 * @param {Object} data
 */
export function updateData(data) {
  return request({
    url: '/webform/FormTablefield/updateTableField',
    method: 'put',
    data
  })
}
/**
 * 新增
 * @param {Object} data
 */
export function addData(data) {
  return request({
    url: '/webform/FormTablefield/add',
    method: 'post',
    data
  })
}
