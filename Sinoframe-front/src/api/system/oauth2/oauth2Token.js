import request from "@/utils/request";

// 获得访问令牌分页
export function getAccessTokenPage(data) {
  return request({
    url: "/frame/oauth2-token/page",
    method: "post",
    data,
  });
}

// 删除访问令牌
export function deleteAccessToken(accessToken) {
  return request({
    url: "/frame/oauth2-token/delete?accessToken=" + accessToken,
    method: "delete",
  });
}
