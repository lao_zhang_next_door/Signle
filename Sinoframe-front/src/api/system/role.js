import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameRole/listData',
    method: 'post',
    data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameRole/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameRole/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameRole/update',
    method: 'put',
    data
  })
}

export function getAllRole() {
  return request({
    url: '/frame/FrameRole/getAllRole',
    method: 'get'
  })
}

export function getModuleByRoleGuid(data) {
  return request({
    url: '/frame/FrameRole/getModuleByRoleGuid',
    method: 'post',
    params: data
  })
}

/**
 * 根据角色guid 获取 数据权限列表
 * @param {*} data 
 * @returns 
 */
export function getDeptByRoleGuid(data) {
  return request({
    url: '/frame/FrameRole/getDeptByRoleGuid',
    method: 'post',
    params: data
  })
}

/**
 * 保存数据权限
 */
export function saveDataPermission(data) {
  return request({
    url: '/frame/FrameRole/saveDataPermission',
    method: 'post',
    data
  })
}

