import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameAttachConfig/listData',
    method: 'post',
    data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameAttachConfig/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameAttachConfig/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameAttachConfig/update',
    method: 'put',
    data
  })
}

/**
 * 变更主配置
 */
export function updateMaster(masterRowGuid) {
  return request({
    url: '/frame/FrameAttachConfig/updateMaster',
    method: 'put',
    params: { masterRowGuid: masterRowGuid }
  })
}

