import request from "@/utils/request";

export function listData(data) {
  return request({
    url: "/frame/FrameCode/listData",
    method: "post",
    data,
  });
}

export function addData(data) {
  return request({
    url: "/frame/FrameCode/add",
    method: "post",
    data,
  });
}

export function deleteData(data) {
  return request({
    url: "/frame/FrameCode/delete",
    method: "delete",
    data,
  });
}

export function editData(data) {
  return request({
    url: "/frame/FrameCode/update",
    method: "put",
    data,
  });
}

export function selectByCodeName(data) {
  return request({
    url: "/frame/FrameCode/selectByCodeName",
    method: "get",
    params: data,
  });
}

export function selectCacheByCodeName(data) {
  return request({
    url: "/frame/FrameCode/selectCacheByCodeName",
    method: "get",
    params: data,
  });
}

/**
 * 查询所有字典 所有字典值 [{text:'男',val:'0'},{text:'女',val:'1'},...]
 * @param {*} data
 * @returns
 */
export function listSimpleDictDatas() {
  return request({
    url: "/frame/FrameCode/listSimpleDictDatas",
    method: "get",
  });
}

/**
 * 查询所有字典项  所有项 没有值  [{性别},{状态}...]
 * @returns 
 */
export function listAllSimple() {
  return request({
    url: "/frame/FrameCode/listAllSimple",
    method: "get",
  });
}


