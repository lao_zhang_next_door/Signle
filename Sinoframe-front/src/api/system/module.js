import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameModule/listData',
    method: 'post',
    data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameModule/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameModule/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameModule/update',
    method: 'put',
    data
  })
}

export function treeData(data) {
  return request({
    url: '/frame/FrameModule/treeData',
    method: 'post',
    data
  })
}

export function selectParentCode(data) {
  return request({
    url: '/frame/FrameModule/selectParentCode',
    method: 'get',
    params: data
  })
}

export function allTreeData(data) {
  return request({
    url: '/frame/FrameModule/allTreeData',
    method: 'post',
    data
  })
}

//全部路由
export function allTreeDataRoute() {
  return request({
    url: '/frame/FrameModule/allTreeDataRoute',
    method: 'get',
  })
}

/**
 * 获取所有path 为空的模块
 * @returns 
 */
export function getAllBlankPathModule() {
  return request({
    url: '/frame/FrameModule/getAllBlankPathModule',
    method: 'get',
  })
}

// 新获取路由
export const getRouters = () => {
  return request({
    url: '/getRouters',
    method: 'get'
  })
}

/**
 * 查询所有菜单
 * @returns 
 */
export function listAllSimple(){
  return request({
    url: "/frame/FrameModule/listAllSimple",
    method: "get",
  });
}

