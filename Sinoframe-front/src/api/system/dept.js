import request from "@/utils/request";

export function listData(data) {
  return request({
    url: "/frame/FrameDept/listData",
    method: "post",
    data,
  });
}

export function addData(data) {
  return request({
    url: "/frame/FrameDept/add",
    method: "post",
    data,
  });
}

export function deleteData(data) {
  return request({
    url: "/frame/FrameDept/delete",
    method: "delete",
    data,
  });
}

export function editData(data) {
  return request({
    url: "/frame/FrameDept/update",
    method: "put",
    data,
  });
}

export function treeData(data) {
  return request({
    url: "/frame/FrameDept/treeData",
    method: "post",
    data,
  });
}

/**
 * 懒加载树 根据parentGuid 加载
 * @param {*} data
 * @returns
 */
export function treeDataLazy(data) {
  return request({
    url: "/frame/FrameDept/treeDataLazy",
    method: "post",
    data,
  });
}

export function selectParentCode(data) {
  return request({
    url: "/frame/FrameDept/selectParentCode",
    method: "get",
    params: data,
  });
}

/**
 * 获取全部树数据
 * @returns
 */
export function allTreeData(data) {
  return request({
    url: "/frame/FrameDept/allTreeData",
    method: "post",
    data,
  });
}
