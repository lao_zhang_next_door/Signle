import request from '@/utils/request'

//获取所有路由
export function getAllRoute() {
  return request({
    url: '/frame/modular/getAllRoute',
    method: 'get'
  })
}