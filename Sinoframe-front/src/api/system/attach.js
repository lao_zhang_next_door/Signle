import request from "@/utils/request";

/**
 * 根据formRowGuid查询附件列表
 * @param {*} data {formRowGuid:''}
 * @returns
 */
export function getListByFormRowGuid(data) {
  return request({
    url: "/frame/FrameAttach/getListByFormRowGuid",
    method: "get",
    params: data,
  });
}

export function deleteFileByRowGuid(data) {
  return request({
    url: "/frame/FrameAttach/deleteFileByRowGuid",
    method: "delete",
    params: data,
  });
}

export function listData(data) {
  return request({
    url: "/frame/FrameAttach/listData",
    method: "post",
    data,
  });
}

export function addData(data) {
  return request({
    url: "/frame/FrameAttach/add",
    method: "post",
    data,
  });
}

export function deleteData(data) {
  return request({
    url: "/frame/FrameAttach/delete",
    method: "delete",
    data,
  });
}

export function editData(data) {
  return request({
    url: "/frame/FrameAttach/update",
    method: "put",
    data,
  });
}
