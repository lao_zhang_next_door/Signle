import request from '@/utils/request'

export function token(loginId, password) {
  let data = {
    client_id: "client_xnw",
    client_secret: "xnw_yyds",
    grant_type: "password",
    username: loginId,
    password: password
  }
  return request({
    url: '/oauth/login',
    method: 'post',
    data,
    encrypt: true
  })
}