import request from "@/utils/request";

export function listData(data) {
  return request({
    url: "/frame/FrameUser/listData",
    method: "get",
    params: data,
  });
}

export function getList(data) {
  return request({
    url: "/frame/FrameUser/getList",
    method: "get",
    params: data,
  });
}

export function addData(data) {
  return request({
    url: "/frame/FrameUser/add",
    method: "post",
    data,
  });
}

export function updateData(data) {
  return request({
    url: "/frame/FrameUser/update",
    method: "put",
    data,
  });
}

export function deleteData(data) {
  return request({
    url: "/frame/FrameUser/delete",
    method: "delete",
    data,
  });
}

/**
 * 验证用户唯一
 */
export function verifyUnique(data) {
  return request({
    url: "/frame/FrameUser/verifyUnique",
    method: "get",
    params: data,
  });
}

/**
 * 查询guid用户详情信息 包括角色,头像,部门等信息
 */
export function getUserDataByGuid(data) {
  return request({
    url: "/frame/FrameUser/getUserDataByGuid",
    method: "get",
    params: data,
  });
}

/**
 * 启用禁用用户
 */
export function enableOrDisable(data) {
  return request({
    url: "/frame/FrameUser/enableOrDisable",
    method: "put",
    params: data,
  });
}

/**
 * 逻辑删除
 */
export function deleteLogic(data) {
  return request({
    url: "/frame/FrameUser/deleteLogic",
    method: "delete",
    data,
  });
}

/**
 * 物理删除
 */
export function deletePhysics(data) {
  return request({
    url: "/frame/FrameUser/deletePhysics",
    method: "delete",
    data,
  });
}

/**
 * 重置用户密码
 * @param data
 * @returns
 */
export function resetPassword(data) {
  return request({
    url: "/frame/FrameUser/resetPassword",
    method: "put",
    params: data,
  });
}

/**
 * 新增框架用户
 * @param {} data
 * @returns
 */
export function addFrameUser(data) {
  return request({
    url: "/frame/FrameUser/addFrameUser",
    method: "post",
    data,
  });
}

/**
 * 更新框架用户
 * @param {} data
 * @returns
 */
export function updateFrameUser(data) {
  return request({
    url: "/frame/FrameUser/updateFrameUser",
    method: "put",
    data,
  });
}

/**
 * 更新用户密码
 * @param {} data
 * @returns
 */
export function changePassword(data) {
  return request({
    url: "/frame/FrameUser/changePassword",
    method: "put",
    data,
  });
}

/**
 * 导入用户
 * @param {} data
 * @returns
 */
export function importUser(data) {
  return request({
    url: "/frame/FrameUser/importUser",
    method: "post",
    data,
    subType: "formData",
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

/**
 * 获取用户信息
 * @returns
 */
export function getUserInfo() {
  return request({
    url: "/frame/FrameUser/getUserInfo",
    method: "get",
  });
}

/**
 * 设置角色
 * @param {*} userGuid
 * @param {*} roleGuids
 * @returns
 */
export function setRole(userGuid, roleGuids) {
  let data = {
    userGuid: userGuid,
    roleGuids: roleGuids,
  };
  return request({
    url: "/frame/FrameUser/setRole",
    method: "post",
    data,
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/frame/FrameUser/update-avatar',
    method: 'put',
    data: data
  })
}

/**
 * 单点登录
 * @param {} data
 * @returns
 */
export function singleSignOn(data) {
  return request({
    url: "/zjgsdacx/provider/singleSign/singleSignOn",
    method: "get",
    params: data,
  });
}


