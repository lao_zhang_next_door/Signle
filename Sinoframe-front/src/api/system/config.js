import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameConfig/listData',
    method: 'post',
    data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameConfig/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameConfig/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameConfig/update',
    method: 'put',
    data
  })
}

