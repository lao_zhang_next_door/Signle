import request from "@/utils/request";

export function listData(data) {
  return request({
    url: "/frame/FrameCodeValue/listData",
    method: "post",
    data,
  });
}

export function addData(data) {
  return request({
    url: "/frame/FrameCodeValue/add",
    method: "post",
    data,
  });
}

export function deleteData(data, codeName) {
  return request({
    url: "/frame/FrameCodeValue/delete?codeName=" + codeName,
    method: "delete",
    data,
  });
}

export function editData(data) {
  return request({
    url: "/frame/FrameCodeValue/update",
    method: "put",
    data,
  });
}

export function treeData(data) {
  return request({
    url: "/frame/FrameCodeValue/treeData",
    method: "post",
    data,
  });
}

export function selectParentCode(data) {
  return request({
    url: "/frame/FrameCodeValue/selectParentCode",
    method: "get",
    params: data,
  });
}
