import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameUserRole/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameUserRole/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameUserRole/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameUserRole/update',
    method: 'put',
    data
  })
}

/**
 * 根据角色guid 获取 所属用户列表
 * @param {*} data 
 * @returns 
 */
export function getUserByRoleGuid(data) {
  return request({
    url: '/frame/FrameUserRole/getUserByRoleGuid',
    method: 'get',
    params: data
  })
}
