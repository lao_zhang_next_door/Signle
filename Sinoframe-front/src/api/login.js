import request from "@/utils/request";

// 退出方法
export function logout() {
  return request({
    url: "/frame/auth/logout",
    method: "post",
  });
}

//登录
export function login(loginId, password) {
  let data = {
    username: loginId,
    password: password,
  };
  return request({
    url: "/frame/auth/login",
    method: "post",
    data,
    encrypt: true,
  });
}

//刷新令牌
export function refreshToken(refreshToken) {
  let data = {
    refreshToken: refreshToken,
  };
  return request({
    url: "/frame/auth/refresh-token",
    method: "post",
    params: data,
  });
}

//获取当前登录用户信息
export function getUserInfo() {
  return request({
    url: "/frame/FrameLogin/getUserInfo",
    method: "get",
  });
}
