import request from '@/utils/request'

// 创建律所重大事项报告
export function createFirmReport(data) {
  return request({
    url: '/report/firm-report/create',
    method: 'post',
    data: data
  })
}

// 更新律所重大事项报告
export function updateFirmReport(data) {
  return request({
    url: '/report/firm-report/update',
    method: 'put',
    data: data
  })
}

// 删除律所重大事项报告
export function deleteFirmReport(rowId) {
  return request({
    url: '/report/firm-report/delete?rowId=' + rowId,
    method: 'delete'
  })
}

// 批量删除律所重大事项报告
export function deleteFirmReportBatch(data) {
  return request({
    url: '/report/firm-report/deleteBatch',
    method: 'delete',
    data
  })
}

// 获得律所重大事项报告
export function getFirmReport(id) {
  return request({
    url: '/report/firm-report/get?id=' + id,
    method: 'get'
  })
}

// 获得律所重大事项报告分页
export function getFirmReportPage(query) {
  return request({
    url: '/report/firm-report/page',
    method: 'get',
    params: query
  })
}

