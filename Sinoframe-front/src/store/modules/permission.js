import { constantRoutes } from "@/router";
import { allTreeDataRoute } from "@/api/system/module";
import Layout from "@/layout/index";
import ParentView from "@/components/ParentView";
import { toCamelCase } from "@/utils";

const permission = {
  state: {
    routes: [],
    addRoutes: [],
    defaultRoutes: [],
    topbarRouters: [],
    sidebarRouters: [],
  },
  mutations: {
    SET_ROUTES: (state, routes) => {
      state.addRoutes = routes;
      state.routes = constantRoutes.concat(routes);
    },
    SET_DEFAULT_ROUTES: (state, routes) => {
      state.defaultRoutes = constantRoutes.concat(routes);
    },
    SET_TOPBAR_ROUTES: (state, routes) => {
      state.topbarRouters = routes;
    },
    SET_SIDEBAR_ROUTERS: (state, routes) => {
      state.sidebarRouters = routes;
    },
  },
  actions: {
    // 生成路由
    GenerateRoutes({ commit }, roleNameList) {
      return new Promise((resolve) => {
        // 向后端请求路由数据
        allTreeDataRoute().then((res) => {
          const sdata = JSON.parse(JSON.stringify(res.data));
          const rdata = JSON.parse(JSON.stringify(res.data));
          const sidebarRoutes = filterAsyncRouter(sdata, roleNameList);
          const rewriteRoutes = filterAsyncRouter(
            rdata,
            roleNameList,
            false,
            true
          );
          rewriteRoutes.push({ path: "*", redirect: "/404", hidden: true });
          commit("SET_ROUTES", rewriteRoutes);
          commit("SET_SIDEBAR_ROUTERS", constantRoutes.concat(sidebarRoutes));
          commit("SET_DEFAULT_ROUTES", sidebarRoutes);
          commit("SET_TOPBAR_ROUTES", sidebarRoutes);
          resolve(rewriteRoutes);
        });
      });
    },
  },
};

// 遍历后台传来的路由字符串，转换为组件对象
// function filterAsyncRouter(asyncRouterMap, lastRouter = false, type = false) {
//   return asyncRouterMap.filter(route => {
//     if (type && route.children) {
//       route.children = filterChildren(route.children)
//     }
//     if (route.component) {
//       // Layout ParentView 组件特殊处理
//       if (route.component === 'Layout') {
//         route.component = Layout
//       } else if (route.component === 'ParentView') {
//         route.component = ParentView
//       } else if (route.component === 'InnerLink') {
//         route.component = InnerLink
//       } else {
//         route.component = loadView(route.component)
//       }
//     }
//     if (route.children != null && route.children && route.children.length) {
//       route.children = filterAsyncRouter(route.children, route, type)
//     } else {
//       delete route['children']
//       delete route['redirect']
//     }
//     return true
//   })
// }

// 遍历后台传来的路由字符串，转换为组件对象
function filterAsyncRouter(
  asyncRouterMap,
  roleNameList,
  lastRouter = false,
  type = false
) {
  return asyncRouterMap.filter((route) => {
    //判断是否具有该权限
    if (!hasPermission(roleNameList, route)) {
      return false;
    }

    // 处理 meta 属性
    route.meta.icon = route.icon;
    route.meta.noCache = route.meta.keepAlive == '0'
    // 路由地址转首字母大写驼峰，作为路由名称，适配keepAlive

    if(!route.hasName){
      route.meta.title = route.name,
      route.name = toCamelCase(route.path, true);
    }
    route.hidden = !route.visible;

    // filterChildren
    if (type && route.children) {
      route.children = filterChildren(route.children);
    }

    // 处理 component 属性
    if (route.children && route.menuType != 'C') {
      // 父节点
      if (!route.parentModule) {
        route.component = Layout;
      } else {
        route.component = ParentView;
      }
    } else {
      // 根节点
      route.component = loadView(route.component);
    }

    if (route.children != null && route.children && route.children.length) {
      route.children = filterAsyncRouter(
        route.children,
        roleNameList,
        route,
        type
      );
    } else {
      delete route["children"];
      delete route['redirect']
    }
    return true;
  });
}

function filterChildren(childrenMap, lastRouter = false) {
  var children = [];
  childrenMap.forEach((el, index) => {
    if (el.children && el.children.length) {
      if(el.children && !!el.parentModule && !lastRouter){
      // if (el.component === "ParentView" && !lastRouter) {
        el.children.forEach((c) => {
          c.meta.title = c.name;
          c.name = toCamelCase(c.path, true);
          c.hasName = true;
          c.path = el.path + "/" + c.path;
          if (c.children && c.children.length) {
            children = children.concat(filterChildren(c.children, c));
            return;
          }
          children.push(c);
        });
        return;
      }
    }
    if (lastRouter) {
      el.path = lastRouter.path + "/" + el.path;
    }
    children = children.concat(el);
  });
  return children;
}

/**
 * 使用meta.role确定当前用户是否具有权限
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some((role) => route.meta.roles.includes(role));
  } else if (route.meta.role) {
    return roles.some((role) => route.meta.role.includes(role));
  } else {
    return true;
  }
}

export const loadView = (view) => {
  // 路由懒加载
  return (resolve) => require([`@/views/${view}`], resolve);
};

export default permission;
