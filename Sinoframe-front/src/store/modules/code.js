const code = {
  state: sessionStorage.getItem("state")
    ? JSON.parse(sessionStorage.getItem("state")).code
    : {
        formData: {},
      },
  actions: {},
  mutations: {
    SET_FORMDATA: (state, formData) => {
      state.formData = formData;
    },
  },
};

export default code;
