/**
 * 获取v-main 高度
 * @param {Object} e
 */
function getVMainHeight(){
	if(document.getElementById("mainIndex")){
		return document.getElementById("mainIndex").offsetHeight;
	}else{
		return null;
	}
	
}

/**
 * 表格高度自适应 自动撑满高度
 */
export async function autoHeight(e,th){
	var h;
	await this.$nextTick(function(){
		var mainHeight = getVMainHeight();
		if(mainHeight && e){
			h = mainHeight - e
		}
	})
	if(th){
		th.tableHeight = h;
	}
	return h;
}
