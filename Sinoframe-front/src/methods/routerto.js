//路由跳转
export function routerto(url,params,type,store){
	//this.$route.params
	if(params){
		//store:是否要全局存储跳转参数(vuex)
		if(store){
			this.$store.commit(store,params)
		}
		
		if(type && type == 'params'){
			this.$router.push({
				name:url,
				params:params
			});
		}else{
			this.$router.push({
				path:url,
				query:params
			});
		}
	}else{
		this.$router.push(url)
	}
}

