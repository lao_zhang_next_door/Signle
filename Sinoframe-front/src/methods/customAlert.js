import { Message } from 'element-ui';
//弹出框提示
//type:success,warning,空
export function customAlert(content,type,duration){
	if(type){
		let obj = {
	      message: content,
	      type: type,
	      showClose: true
	    }
		if(duration){
			obj.duration = duration;
		}
		Message(obj);
	}else{
		Message(content);
	}
}
