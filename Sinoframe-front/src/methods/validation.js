const requiredRules = [(value) => !!value || "此项必填"];

export default {
  requiredRules,
};
