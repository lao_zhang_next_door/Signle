/**
 * 替换图片路径
 */
export function getImg(val) {
  if (val) {
    return process.env.VUE_APP_BASE_FILEURL + val;
  } else {
    return "";
  }
}

/**
 * 时间格式替换
 * changeDate(date,'yyyy-MM-dd');
 * isSJC : 是否时间戳
 */
export function changeDate(val, format) {
  if (val) {
    var t = new Date(val);
    var tf = function (i) {
      return (i < 10 ? "0" : "") + i;
    };
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
      switch (a) {
        case "yyyy":
          return tf(t.getFullYear());
          break;
        case "MM":
          return tf(t.getMonth() + 1);
          break;
        case "mm":
          return tf(t.getMinutes());
          break;
        case "dd":
          return tf(t.getDate());
          break;
        case "HH":
          return tf(t.getHours());
          break;
        case "ss":
          return tf(t.getSeconds());
          break;
      }
    });
  }
}

import code from "@/methods/code";
/**
 * code:{
 * 	key:val
 * }
 * @param val
 * @returns
 */
export function codeToVal(val, codeName) {
  if (code && code[codeName]) {
    let arr = code[codeName];

    for (let i = 0; i < arr.length; i++) {
      if (arr[i].itemValue == val) {
        return arr[i].itemText;
      }
    }

    return "";
  } else {
    return "";
  }
}
