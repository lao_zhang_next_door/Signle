import store from '@/store'
/**
 * 权限判断工具类
 * @param  {...any} roleName 
 * @returns 
 */
export function hasPerm(...roleName){
    let roles = store.getters.roles;
    let exist = false;

    
    roleName.forEach((v)=>{
        let a = roles.find(i=>i==v);
        if(a){
            exist = true;
        }
    })
    return exist;
}