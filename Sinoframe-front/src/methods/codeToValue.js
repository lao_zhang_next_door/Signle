import { selectCacheByCodeName } from "@/api/system/code";

//待缓存的对象
let mapObj = {};

/**
 * list数据字典转化
 * @param {*} arr  数据源
 * @param {*} needFilterfield  需要转化的字段
 * @param {*} codeName  代码项名称
 */
export async function listDataFilter(arr, needFilterfield, codeName) {
  if (mapObj[codeName]) {
    return returnCodeVal(arr, needFilterfield, mapObj[codeName]);
  } else {
    //获取字典并新增
    const res = await selectCacheByCodeName({ codeName: codeName });
    mapObj[codeName] = {};
    res.data.every((it, idx) => {
      mapObj[codeName][it.itemValue] = it.itemText;
      return true;
    });
    return returnCodeVal(arr, needFilterfield, mapObj[codeName]);
  }
}

function returnCodeVal(arr, needFilterfield, mapCode) {
  for (let i = 0; i < arr.length; i++) {
    arr[i][needFilterfield + "Val"] = mapCode[arr[i][needFilterfield]];
  }

  return arr;
}
