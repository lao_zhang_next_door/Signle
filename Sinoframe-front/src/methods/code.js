let code = {
  模块类型: [
    {
      itemText: "目录",
      itemValue: "M",
    },
    {
      itemText: "菜单",
      itemValue: "C",
    },
    {
      itemText: "按钮",
      itemValue: "A",
    },
  ],
  数据权限: [
    {
      itemText: "全部数据权限",
      itemValue: "1",
    },
    {
      itemText: "仅本人数据权限",
      itemValue: "2",
    },
    {
      itemText: "本部门数据权限",
      itemValue: "3",
    },
    {
      itemText: "本部门及以下数据权限",
      itemValue: "4",
    },
    {
      itemText: "自定义部门数据权限",
      itemValue: "5",
    },
  ],
};

export default code;
