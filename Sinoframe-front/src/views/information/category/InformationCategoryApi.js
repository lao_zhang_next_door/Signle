import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/information/informationCategory/listData',
    method: 'post',
    data
  })
}

export function addData(data) {
  return request({
    url: '/information/informationCategory/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/information/informationCategory/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/information/informationCategory/update',
    method: 'put',
    data
  })
}

/**
 * 根据用户角色查询栏目树
 * @param {*}
 * @returns
 */
export function getCategoryTreeByRole(data) {
  return request({
    url: '/information/informationCategory/getCategoryTreeByRole',
    method: 'post',
    data
  })
}

/**
 * 查询所有栏目树
 * @param {*}
 * @returns
 */
export function getAllCategoryTree(data) {
  return request({
    url: '/information/informationCategory/getAllCategoryTree',
    method: 'post',
    data
  })
}

/**
 * 根据角色Guid联查栏目表
 * @param {*}
 * @returns
 */
export function getRoleCategoryPermission(allowTo) {
  return request({
    url: '/information/informationCategoryRight/getRoleCategoryPermission?allowTo=' + allowTo,
    method: 'get'
  })
}

/**
 * 调整用户角色栏目权限
 * @param {*}
 * @returns
 */
export function addCategoryRight(data) {
  return request({
    url: '/information/informationCategoryRight/addCategoryRight',
    method: 'post',
    data
  })
}

/**
 * 修改信息栏目审核状态
 * @param {*} data
 * @returns
 */
export function updateInfoCategoryStatus(data) {
  return request({
    url: '/information/informationInfoCategory/updateInfoCategoryStatus',
    method: 'post',
    data
  })
}
