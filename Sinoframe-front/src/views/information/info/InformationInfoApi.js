import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/information/informationInfo/listData',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/information/informationInfo/delete',
    method: 'delete',
    data
  })
}

/**
 * 信息发布
 * @param {} data
 * @returns
 */
export function deliveryInfo(data) {
  return request({
    url: '/information/informationInfo/deliveryInfo',
    method: 'post',
    data
  })
}

/**
 * 信息发布修改
 * @param {} data
 * @returns
 */
export function updateInfo(data) {
  return request({
    url: '/information/informationInfo/updateInfo',
    method: 'put',
    data
  })
}

/**
 * 信息权限联查
 * @param {*} data
 * @returns
 */
export function getInfoRelationList(data) {
  return request({
    url: '/information/informationInfo/getInfoRelationList',
    method: 'post',
    data
  })
}

/**
 * 批量停止发布信息
 * @param {*} data
 * @returns
 */
export function batchStopRelease(data) {
  return request({
    url: '/information/informationInfo/batchStopRelease',
    method: 'post',
    data
  })
}

/**
 * 批量发布信息
 * @param {*} data
 * @returns
 */
export function batchStartRelease(data) {
  return request({
    url: '/information/informationInfo/batchStartRelease',
    method: 'post',
    data
  })
}

/**
 * 更新信息点击次数
 * @param {} data
 * @returns
 */
export function updateClickTime(rowGuid) {
  return request({
    url: '/information/informationInfo/updateClickTime?rowGuid=' + rowGuid,
    method: 'post'
  })
}

/**
 * 我发布的关联信息联查
 * @param {*} data
 * @returns
 */
export function getMyInfoList(data) {
  return request({
    url: '/information/informationInfo/getMyInfoList',
    method: 'post',
    data
  })
}
