import request from '@/utils/request'

export function listData(data) {
  return request({
    url: '/frame/FrameSchedule/listData',
    method: 'get',
    params: data
  })
}

export function addData(data) {
  return request({
    url: '/frame/FrameSchedule/add',
    method: 'post',
    data
  })
}

export function deleteData(data) {
  return request({
    url: '/frame/FrameSchedule/delete',
    method: 'delete',
    data
  })
}

export function editData(data) {
  return request({
    url: '/frame/FrameSchedule/update',
    method: 'put',
    data
  })
}

/**
 * 获取未做代办项数量
 */
export function getInComplete() {
  return request({
    url: '/frame/FrameSchedule/getInComplete',
    method: 'get'
  })
}

/**
 * 处理事件 更新是否处理字段
 * @param {*} rowGuid 
 * @returns 
 */
export function dealSchedule(rowGuid) {
  return request({
    url: '/frame/FrameSchedule/dealSchedule',
    method: 'put',
    params: { rowGuid: rowGuid }
  })
}

