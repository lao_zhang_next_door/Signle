const fs = require("fs");
const { baseParse } = require("@vue/compiler-core");
module.exports = function (source, map) {
  // 1. 获取带有 <docs /> 标签的文件完整路径
  const { resourcePath } = this
  // 2. 读取文件内容
  console.log(resourcePath)
  const file = fs.readFileSync(resourcePath).toString();
  // 3. 通过 baseParse 将字符串模板转换成 AST 抽象语法树
  // 3.1获取源代码信息标签内容
  const sourceInfoTag = baseParse(file).children.find((n) => {
    return n.tag === "docs";
  });
  const sourceTitleTag = sourceInfoTag.children.find(
    (n) => n.tag === "source-title"
  );
  const sourceDescTag = sourceInfoTag.children.find(
    (n) => n.tag === "source-desc"
  );
  // // 4. 标题
  const title = (sourceTitleTag.children[0] || {}).content;
  // // 4.1描述
  const desc = (sourceDescTag.children[0] || {}).content;
  // const desc = (sourceDescTag.children[0] || {}).html;
  // 5. 将 <source-info></source-info> 标签和内容抽离
  const main = file.split(sourceInfoTag.loc.source).join("").trim();
  // Component.options.__sourceCodeTitle = ${JSON.stringify(title)}
  // Component.options.__sourceCodeTitle = ${JSON.stringify(desc)}
  // 6. 回到并添加到 组件对象上面
  this.callback(
      null,
      `export default function (Component) {
        Component.options.__sourceCode = ${JSON.stringify(main)}
        Component.options.__sourceCodeTitle = ${JSON.stringify(title)}
        Component.options.__sourceCodeDesc = ${JSON.stringify(desc)}
      }`,
      map
  )
};
