const { JSEncrypt } = require('encryptlong')
import { commonProps } from './frame.js'
export const Rsa = {

    //  加密
    encryptedData(data) {
        // 新建JSEncrypt对象
        let encryptor = new JSEncrypt();
        // 设置公钥
        encryptor.setPublicKey(commonProps.publicKey);
        // 加密数据
        return encryptor.encryptLong(data);
    },
    // // 解密
    // decryptData(privateKey, data) {
    //     // 新建JSEncrypt对象
    //     let decrypt = new JSEncrypt();
    //     // 设置私钥
    //     decrypt.setPrivateKey(privateKey);
    //     // 解密数据
    //     decrypt.decrypt(secretWord);
    // }
}