/**
 * 删除数组中指定对象
 * @param {OBject} arr
 * @param {function} func 返回ture或false
 */
export function arrDeleteObj(arr, func) {
  arr.forEach((item, i) => {
    if (func(item)) {
      arr.splice(i, 1);
    }
  });
}
