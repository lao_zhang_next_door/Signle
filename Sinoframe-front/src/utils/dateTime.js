let dateTime = {
  /**
   * 获取当前时间戳(毫秒数)
   */
  getTimeStamp: function () {
    return Date.now();
  },
  /**
   * 时间格式替换
   * changeDate(date,'yyyy-MM-dd');
   * isSJC : 是否时间戳
   */
  changeDate: function (val, format) {
    if (val) {
      var t = new Date(val);
      var tf = function (i) {
        return (i < 10 ? "0" : "") + i;
      };
      return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
        switch (a) {
          case "yyyy":
            return tf(t.getFullYear());
            break;
          case "MM":
            return tf(t.getMonth() + 1);
            break;
          case "mm":
            return tf(t.getMinutes());
            break;
          case "dd":
            return tf(t.getDate());
            break;
          case "HH":
            return tf(t.getHours());
            break;
          case "ss":
            return tf(t.getSeconds());
            break;
        }
      });
    }
  },
  /**
   * 将毫秒，转换成时间字符串。例如说，xx 分钟
   *
   * @param ms 毫秒
   * @returns {string} 字符串
   */
  getDate: function (ms) {
    const day = Math.floor(ms / (24 * 60 * 60 * 1000));
    const hour = Math.floor(ms / (60 * 60 * 1000) - day * 24);
    const minute = Math.floor(ms / (60 * 1000) - day * 24 * 60 - hour * 60);
    const second = Math.floor(
      ms / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - minute * 60
    );
    if (day > 0) {
      return day + "天" + hour + "小时" + minute + "分钟";
    }
    if (hour > 0) {
      return hour + "小时" + minute + "分钟";
    }
    if (minute > 0) {
      return minute + "分钟";
    }
    if (second > 0) {
      return second + "秒";
    } else {
      return 0 + "秒";
    }
  },
};

export default dateTime;
