import Vue from "vue";

import Element from "element-ui";
import "./assets/styles/element-variables.scss";

import "@/assets/styles/index.scss"; // global css
import App from "./App";
import store from "./store";
import router from "./router";
import directive from "./directive"; // directive
import plugins from "./plugins"; // plugins

//引入vuetify
import vuetify from "@/plugins/vuetify/index";

import "./assets/icons"; // icon
import "./permission"; // permission control
import "./tongji"; // 百度统计
// import { getDicts } from "@/api/system/dict/data";
// import { getConfigKey } from "@/api/infra/config";
import {
  parseTime,
  resetForm,
  handleTree,
  addBeginAndEndTime,
  divide,
} from "@/utils/frame";
import Pagination from "@/components/Pagination";
// 自定义表格工具扩展
import RightToolbar from "@/components/RightToolbar";
// 代码高亮插件
// import hljs from 'highlight.js'
// import 'highlight.js/styles/github-gist.css'
import {
  DICT_TYPE,
  getDictDataLabel,
  getDictDatas,
  getDictDatas2,
  getDictData,
  getDictDataByLabel,
} from "@/utils/dict";

//自定义函数
import { routerto } from "@/methods/routerto";
import { autoHeight } from "@/methods/autoHeight";
import { customAlert } from "@/methods/customAlert";
import validation from "@/methods/validation.js";

Vue.prototype.$validation = validation;

// 全局方法挂载
Vue.prototype.$routerto = routerto;
Vue.prototype.$autoHeight = autoHeight;
Vue.prototype.$customAlert = customAlert;
// Vue.prototype.getDicts = getDicts
// Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime;
Vue.prototype.resetForm = resetForm;
Vue.prototype.getDictDatas = getDictDatas;
Vue.prototype.getDictDatas2 = getDictDatas2;
Vue.prototype.getDictDataLabel = getDictDataLabel;
Vue.prototype.getDictData = getDictData;
Vue.prototype.getDictDataByLabel = getDictDataByLabel;
Vue.prototype.DICT_TYPE = DICT_TYPE;
Vue.prototype.handleTree = handleTree;
Vue.prototype.addBeginAndEndTime = addBeginAndEndTime;
Vue.prototype.divide = divide;

// 全局组件挂载
Vue.component("TableComp",TableComponent);
Vue.component("Tree",Tree);
Vue.component("DictTag", DictTag);
Vue.component("DocAlert", DocAlert);
Vue.component("Pagination", Pagination);
Vue.component("RightToolbar", RightToolbar);
// 字典标签组件
import DictTag from "@/components/DictTag";
import DocAlert from "@/components/DocAlert";
// 列表组件
import TableComponent from "@/components/Table/TableComponent";
// 树组件
import Tree from "@/components/Vuetify/Tree/index";
// 头部标签插件
import VueMeta from "vue-meta";

Vue.use(directive);
Vue.use(plugins);
Vue.use(VueMeta);
// Vue.use(hljs.vuePlugin);

import deleteAlter from "@/components/Vuetify/DeletePop";
import vselectOption from "@/components/Vuetify/SelectOption/index";
import OpenWin from "./components/Vuetify/OpenWin/index";
import vradioGroup from "@/components/Vuetify/RadioGroup/index";
import eselectOption from "@/components/ElementUI/SelectOption/index";
import eradioGroup from "@/components/ElementUI/RadioGroup/index";
import euploader from "@/components/ElementUI/upload/ClickUploadComponent";

import checkBox from "@/components/ElementUI/CheckBox";
import DatePicker from "@/components/ElementUI/DatePicker"; 
import vuploaderImg from "@/components/Vuetify/upload/uploadImg";
import avatarUpload from "@/components/ImageCropper/avatar-upload";

Vue.component("DeleteAlter", deleteAlter);
Vue.component("VselectOption", vselectOption);
Vue.component("OpenWin", OpenWin);
Vue.component("vradioGroup", vradioGroup);
Vue.component("EselectOption", eselectOption);
Vue.component("EradioGroup", eradioGroup);
Vue.component("Euploader", euploader);
Vue.component("VuploaderImg", vuploaderImg);
Vue.component("AvatarUpload", avatarUpload);
Vue.component("CheckBox", checkBox);
Vue.component("DatePicker", DatePicker);

//全局filters
import * as filters from "@/methods/filter";
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key]); //插入过滤器名和对应方法
});

// bpmnProcessDesigner 需要引入
import MyPD from "@/components/bpmnProcessDesigner/package/index.js";
Vue.use(MyPD);
import "@/components/bpmnProcessDesigner/package/theme/index.scss";
import "bpmn-js/dist/assets/diagram-js.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css";

// Form Generator 组件需要使用到 tinymce
import Tinymce from "@/components/tinymce/index.vue";
Vue.component("tinymce", Tinymce);
import "@/icons";
import request from "@/utils/request"; // 实现 form generator 使用自己定义的 axios request 对象
Vue.prototype.$axios = request;
import "@/styles/index.scss";

// 默认点击背景不关闭弹窗
import ElementUI from "element-ui";
ElementUI.Dialog.props.closeOnClickModal.default = false;

/**
 * elemenui
 * 设置默认组件大小
 */
Vue.use(Element, {
  size: localStorage.getItem("size") || "small",
});

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  store,
  vuetify,
  render: (h) => h(App),
});
