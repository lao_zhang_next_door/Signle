package com.sinoy.zwwx.entity;

import lombok.Data;

@Data
public class WxResp {

    // 错误码  0：成功，其他：失败
    private Integer errcode;

    // 错误信息
    private String errmsg;

    // 获取到的凭证
    private String access_token;

    // 凭证有效时间，单位：秒
    private Integer expires_in;

    // 无效的成员帐号
    private String invaliduser;

    //
    private String jobid;

}
