package com.sinoy.zwwx.service;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.zwwx.entity.WxResp;
import lombok.extern.slf4j.Slf4j;

//政务微信接口
@Slf4j
public class WxApiService {

    public WxApiService(String corpid, String corpsecret, String urlPrefix) {
        this.corpid = corpid;
        this.corpsecret = corpsecret;
        this.urlPrefix = urlPrefix;
    }

    private static String corpid = "";
    private static String corpsecret = "";
    private static String urlPrefix = "";

    /**
     * 获取access_token
     * @return
     */
    public WxResp getAccessToken(){
        String url = urlPrefix + "/cgi-bin/gettoken?corpid="+corpid+"&corpsecret="+corpsecret;
        log.info("获取access_token请求地址：{}", url);
        String result = HttpUtil.get(url);
        log.info("获取access_token返回结果：{}", result);
        // result用Resp接收
        WxResp wxResp = JSON.parseObject(result, WxResp.class);
        return wxResp;
    }

    /**
     * 主动发送文本消息  touser、toparty、totag不能同时为空，后面不再强调。
     * @param access_token 调用接口凭证
     * @param agentid 企业应用的id，整型。可在应用的设置页面查看
     * @param touser 接收者的userid 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该单位应用的全部成员发送
     * @param content 消息内容 单位应用的id，整型。可在应用的设置页面查看
     * @param toparty 接收者的部门id 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
     * @param totag 接收者的标签id 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
     * @return
     */
    public WxResp sendMessage(String access_token, String agentid, String touser, String toparty, String totag, String content){
        String url = urlPrefix + "/cgi-bin/message/send?access_token="+access_token;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("touser", touser);
        jsonObject.put("toparty", toparty);
        jsonObject.put("totag", totag);
        jsonObject.put("msgtype", "text");
        jsonObject.put("agentid", agentid);
        JSONObject text = new JSONObject();
        text.put("content", content);
        jsonObject.put("text", text);
        log.info("主动发送文本消息请求地址：{}", url);
        log.info("主动发送文本消息请求参数：{}", jsonObject.toJSONString());
        String result = HttpUtil.post(url, jsonObject.toString());
        log.info("主动发送文本消息返回结果：{}", result);
        WxResp wxResp = JSON.parseObject(result, WxResp.class);
        return wxResp;
    }


}
