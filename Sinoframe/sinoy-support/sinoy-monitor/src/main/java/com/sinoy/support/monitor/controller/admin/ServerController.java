package com.sinoy.support.monitor.controller.admin;


import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.monitor.entity.Server;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController {
    @GetMapping()
    public R getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return R.ok().put("data",server);
    }
}
