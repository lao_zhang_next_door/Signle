package com.sinoy.support.sms.controller.vo.template;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SmsTemplateRespVO extends SmsTemplateBaseVO {

    //编号
    private Long id;

    //短信渠道编码
    private String channelCode;

    //参数数组
    private List<String> params;

    //创建时间
    private Date createTime;

}
