package com.sinoy.support.sms.controller.vo.log;

import lombok.Data;

import java.util.Date;
import java.util.Map;

/**
 * 短信日志 Excel VO
 *
 */
@Data
public class SmsLogExcelVO {

    //编号
    private Long id;

    //短信渠道编号
    private Long channelId;

    //短信渠道编码
    private String channelCode;

    //模板编号
    private Long templateId;

    //模板编码
    private String templateCode;

//    @ExcelProperty(value = "短信类型", converter = DictConvert.class)
//    @DictFormat(DictTypeConstants.SMS_TEMPLATE_TYPE)
    //短信类型
    private Integer templateType;

    //短信内容
    private String templateContent;

//    @ExcelProperty(value = "短信参数", converter = JsonConvert.class)
    //短信参数
    private Map<String, Object> templateParams;

    //短信 API 的模板编号
    private String apiTemplateId;

    //手机号
    private String mobile;

    //用户编号
    private Long userId;

//    @ExcelProperty(value = "用户类型", converter = DictConvert.class)
//    @DictFormat(DictTypeConstants.USER_TYPE)
    //用户类型
    private Integer userType;

//    @ExcelProperty(value = "发送状态", converter = DictConvert.class)
//    @DictFormat(DictTypeConstants.SMS_SEND_STATUS)
    //发送状态
    private Integer sendStatus;

    //发送时间
    private Date sendTime;

    //发送结果的编码
    private Integer sendCode;

    //发送结果的提示
    private String sendMsg;

    //短信 API 发送结果的编码
    private String apiSendCode;

    //短信 API 发送失败的提示
    private String apiSendMsg;

    //短信 API 发送返回的唯一请求 ID
    private String apiRequestId;

    //短信 API 发送返回的序号
    private String apiSerialNo;

//    @ExcelProperty(value = "接收状态", converter = DictConvert.class)
//    @DictFormat(DictTypeConstants.SMS_RECEIVE_STATUS)
    //接收状态
    private Integer receiveStatus;

    //接收时间
    private Date receiveTime;

    //API 接收结果的编码
    private String apiReceiveCode;

    //API 接收结果的说明
    private String apiReceiveMsg;

    //创建时间
    private Date createTime;

}
