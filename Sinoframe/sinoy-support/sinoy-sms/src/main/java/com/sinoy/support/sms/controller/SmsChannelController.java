package com.sinoy.support.sms.controller;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelCreateReqVO;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelPageReqVO;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelUpdateReqVO;
import com.sinoy.support.sms.convert.SmsChannelConvert;
import com.sinoy.support.sms.dal.dataobject.SmsChannelDO;
import com.sinoy.support.sms.service.SmsChannelService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("support/sms-channel")
public class SmsChannelController {

    @Resource
    private SmsChannelService smsChannelService;

    //创建短信渠道
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('system:sms-channel:create')")
    public R createSmsChannel(@Valid @RequestBody SmsChannelCreateReqVO createReqVO) {
        smsChannelService.createSmsChannel(createReqVO);
        return R.ok();
    }

    //更新短信渠道
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('system:sms-channel:update')")
    public R updateSmsChannel(@Valid @RequestBody SmsChannelUpdateReqVO updateReqVO) {
        smsChannelService.updateSmsChannel(updateReqVO);
        return R.ok();
    }

    //删除短信渠道
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('system:sms-channel:delete')")
    public R deleteSmsChannel(@RequestParam("id") Long id) {
        smsChannelService.deleteSmsChannel(id);
        return R.ok();
    }

    //获得短信渠道
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('system:sms-channel:query')")
    public R getSmsChannel(@RequestParam("id") Long id) {
        SmsChannelDO smsChannel = smsChannelService.getSmsChannel(id);
        return R.ok().put("data",SmsChannelConvert.INSTANCE.convert(smsChannel));
    }

    //获得短信渠道分页
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sms-channel:query')")
    public R getSmsChannelPage(@Valid SmsChannelPageReqVO pageVO) {
        PageResult<SmsChannelDO> pageResult = smsChannelService.getSmsChannelPage(pageVO);
        return R.ok().put("data",SmsChannelConvert.INSTANCE.convertPage(pageResult));
    }

    //获得短信渠道精简列表
    @GetMapping("/list-all-simple")
    public R getSimpleSmsChannels() {
        List<SmsChannelDO> list = smsChannelService.getSmsChannelList();
        // 排序后，返回给前端
        list.sort(Comparator.comparing(SmsChannelDO::getId));
        return R.ok().put("data",SmsChannelConvert.INSTANCE.convertList03(list));
    }

}
