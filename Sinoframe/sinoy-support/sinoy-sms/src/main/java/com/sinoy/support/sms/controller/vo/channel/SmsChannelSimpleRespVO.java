package com.sinoy.support.sms.controller.vo.channel;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SmsChannelSimpleRespVO {

    @NotNull(message = "编号不能为空")
    private Long id;

    //短信签名
    @NotNull(message = "短信签名不能为空")
    private String signature;

    //渠道编码
    private String code;

}
