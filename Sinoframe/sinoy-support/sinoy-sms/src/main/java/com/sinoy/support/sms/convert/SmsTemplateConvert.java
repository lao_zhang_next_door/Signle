package com.sinoy.support.sms.convert;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateCreateReqVO;
//import com.sinoy.support.sms.controller.vo.template.SmsTemplateExcelVO;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateRespVO;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateUpdateReqVO;
import com.sinoy.support.sms.dal.dataobject.SmsTemplateDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SmsTemplateConvert {

    SmsTemplateConvert INSTANCE = Mappers.getMapper(SmsTemplateConvert.class);

    SmsTemplateDO convert(SmsTemplateCreateReqVO bean);

    SmsTemplateDO convert(SmsTemplateUpdateReqVO bean);

    SmsTemplateRespVO convert(SmsTemplateDO bean);

    List<SmsTemplateRespVO> convertList(List<SmsTemplateDO> list);

    PageResult<SmsTemplateRespVO> convertPage(PageResult<SmsTemplateDO> page);

//    List<SmsTemplateExcelVO> convertList02(List<SmsTemplateDO> list);

}
