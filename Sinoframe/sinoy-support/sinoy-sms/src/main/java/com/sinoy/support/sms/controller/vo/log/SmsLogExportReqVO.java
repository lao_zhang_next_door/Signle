package com.sinoy.support.sms.controller.vo.log;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class SmsLogExportReqVO {

    //短信渠道编号
    private Long channelId;

    //模板编号
    private Long templateId;

    //手机号
    private String mobile;

    //发送状态
    private Integer sendStatus;

    //开始发送时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date[] sendTime;

    //接收状态
    private Integer receiveStatus;

    //开始接收时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date[] receiveTime;

}
