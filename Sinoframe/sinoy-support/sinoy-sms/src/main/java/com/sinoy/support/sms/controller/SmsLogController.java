package com.sinoy.support.sms.controller;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.sms.controller.vo.log.SmsLogPageReqVO;
import com.sinoy.support.sms.convert.SmsLogConvert;
import com.sinoy.support.sms.dal.dataobject.SmsLogDO;
import com.sinoy.support.sms.service.SmsLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/support/sms-log")
@Validated
public class SmsLogController {

    @Resource
    private SmsLogService smsLogService;

    //获得短信日志分页
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sms-log:query')")
    public R getSmsLogPage(@Valid SmsLogPageReqVO pageVO) {
        PageResult<SmsLogDO> pageResult = smsLogService.getSmsLogPage(pageVO);
        return R.ok().put("data",SmsLogConvert.INSTANCE.convertPage(pageResult));
    }

}
