package com.sinoy.support.sms.config;

import com.sinoy.support.sms.core.client.SmsClientFactory;
import com.sinoy.support.sms.core.client.impl.SmsClientFactoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 短信配置类
 *
 */
@Configuration
public class SmsAutoConfiguration {

    @Bean
    public SmsClientFactory smsClientFactory() {
        return new SmsClientFactoryImpl();
    }

}
