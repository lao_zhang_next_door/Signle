package com.sinoy.support.sms.controller.vo.template;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SmsTemplatePageReqVO extends PageParam {

    //短信签名
    private Integer type;

    //开启状态
    private Integer status;

    //模板编码
    private String code;

    //模板内容
    private String content;

    //短信 API 的模板编号
    private String apiTemplateId;

    //短信渠道编号
    private Long channelId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //创建时间")
    private Date[] createTime;

}
