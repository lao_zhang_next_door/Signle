package com.sinoy.support.sms.controller.vo.channel;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
* 短信渠道 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
@Data
public class SmsChannelBaseVO {

    //短信签名
    @NotNull(message = "短信签名不能为空")
    private String signature;

    //启用状态
    @NotNull(message = "启用状态不能为空")
    private Integer status;

    //备注
    private String remark;

    //短信 API 的账号
    @NotNull(message = "短信 API 的账号不能为空")
    private String apiKey;

    //短信 API 的密钥
    private String apiSecret;

    //短信发送回调 URL
    private String callbackUrl;

}
