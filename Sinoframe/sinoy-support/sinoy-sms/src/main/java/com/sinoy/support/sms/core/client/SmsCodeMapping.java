package com.sinoy.support.sms.core.client;

import com.sinoy.core.common.exception.ErrorCode;
import java.util.function.Function;

/**
 * 将 API 的错误码，转换为通用的错误码
 *
 */
public interface SmsCodeMapping extends Function<String, ErrorCode> {
}
