package com.sinoy.support.sms.convert;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelCreateReqVO;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelRespVO;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelSimpleRespVO;
import com.sinoy.support.sms.controller.vo.channel.SmsChannelUpdateReqVO;
import com.sinoy.support.sms.core.property.SmsChannelProperties;
import com.sinoy.support.sms.dal.dataobject.SmsChannelDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 短信渠道 Convert
 *
 */
@Mapper
public interface SmsChannelConvert {

    SmsChannelConvert INSTANCE = Mappers.getMapper(SmsChannelConvert.class);

    SmsChannelDO convert(SmsChannelCreateReqVO bean);

    SmsChannelDO convert(SmsChannelUpdateReqVO bean);

    SmsChannelRespVO convert(SmsChannelDO bean);

    List<SmsChannelRespVO> convertList(List<SmsChannelDO> list);

    PageResult<SmsChannelRespVO> convertPage(PageResult<SmsChannelDO> page);

    List<SmsChannelProperties> convertList02(List<SmsChannelDO> list);

    List<SmsChannelSimpleRespVO> convertList03(List<SmsChannelDO> list);

}
