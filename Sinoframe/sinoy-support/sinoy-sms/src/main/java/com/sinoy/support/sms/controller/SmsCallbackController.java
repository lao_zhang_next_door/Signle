package com.sinoy.support.sms.controller;

import cn.hutool.core.util.URLUtil;
import com.sinoy.support.sms.core.enums.SmsChannelEnum;
import com.sinoy.support.sms.service.SmsSendService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;

//短信回调
@RestController
@RequestMapping("/support/sms/callback")
public class SmsCallbackController {

    @Resource
    private SmsSendService smsSendService;

    //云片短信的回调
    @PostMapping("/yunpian")
    @PermitAll
    public String receiveYunpianSmsStatus(@RequestParam("sms_status") String smsStatus) throws Throwable {
        String text = URLUtil.decode(smsStatus); // decode 解码参数，因为它被 encode
        smsSendService.receiveSmsStatus(SmsChannelEnum.YUN_PIAN.getCode(), text);
        return "SUCCESS"; // 约定返回 SUCCESS 为成功
    }


}
