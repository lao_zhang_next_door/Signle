package com.sinoy.support.sms.core.client.impl.zhengwuyun;

import com.sinoy.core.common.entity.KeyValue;
import com.sinoy.support.sms.core.client.SmsCodeMapping;
import com.sinoy.support.sms.core.client.SmsCommonResult;
import com.sinoy.support.sms.core.client.dto.SmsReceiveRespDTO;
import com.sinoy.support.sms.core.client.dto.SmsSendRespDTO;
import com.sinoy.support.sms.core.client.dto.SmsTemplateRespDTO;
import com.sinoy.support.sms.core.client.impl.AbstractSmsClient;
import com.sinoy.support.sms.core.property.SmsChannelProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 政务云短信
 */
@Slf4j
public class ZhengwuyunSmsClient extends AbstractSmsClient {

    private volatile ZhengwuyunClient client;

    public ZhengwuyunSmsClient(SmsChannelProperties properties, SmsCodeMapping codeMapping) {
        super(properties, codeMapping);
    }

    @Override
    protected void doInit() {
        // 初始化新的客户端
        ZhengwuyunClient newClient = new ZhengwuyunClient(null,properties.getUrl(),properties.getCode(),properties.getApiKey(),properties.getApiSecret());
        this.client = newClient;
    }

    @Override
    protected SmsCommonResult<SmsSendRespDTO> doSendSms(Long sendLogId, String mobile, String apiTemplateId, List<KeyValue<String, Object>> templateParams) throws Throwable {

//        Map<String, String> map = new HashMap<>();
//        map.put("MessageContent",msg);
//        map.put("UserNumber",phone);
//        map.put("fromUserID",appId);
//        map.put("targetUserID",appId);
//        map.put("appID",appId);
//        String s = JSON.toJSONString(map);
//        Map<String, String> header = new HashMap<>();
//        header.put("X-HW-ID",id);
//        header.put("X-HW-APPKEY",appKey);
//        header.put("Content-Type","application/json");
//        String result = doPost(url, s, header);
//        return result;
        return  null;
    }

    @Override
    protected List<SmsReceiveRespDTO> doParseSmsReceiveStatus(String text) throws Throwable {
        return null;
    }

    @Override
    protected SmsCommonResult<SmsTemplateRespDTO> doGetSmsTemplate(String apiTemplateId) throws Throwable {
        return null;
    }
}
