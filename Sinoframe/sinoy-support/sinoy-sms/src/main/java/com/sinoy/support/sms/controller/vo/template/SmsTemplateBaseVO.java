package com.sinoy.support.sms.controller.vo.template;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
* 短信模板 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
@Data
public class SmsTemplateBaseVO {

    //短信类型
    @NotNull(message = "短信类型不能为空")
    private Integer type;

    //开启状态
    @NotNull(message = "开启状态不能为空")
    private Integer status;

    //模板编码
    @NotNull(message = "模板编码不能为空")
    private String code;

    //模板名称
    @NotNull(message = "模板名称不能为空")
    private String name;

    //模板内容
    @NotNull(message = "模板内容不能为空")
    private String content;

    //备注
    private String remark;

    //短信 API 的模板编号
    @NotNull(message = "短信 API 的模板编号不能为空")
    private String apiTemplateId;

    //短信渠道编号
    @NotNull(message = "短信渠道编号不能为空")
    private Long channelId;

}
