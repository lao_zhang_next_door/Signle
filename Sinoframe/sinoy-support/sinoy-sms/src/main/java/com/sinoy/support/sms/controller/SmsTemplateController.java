package com.sinoy.support.sms.controller;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateCreateReqVO;
import com.sinoy.support.sms.controller.vo.template.SmsTemplatePageReqVO;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateSendReqVO;
import com.sinoy.support.sms.controller.vo.template.SmsTemplateUpdateReqVO;
import com.sinoy.support.sms.convert.SmsTemplateConvert;
import com.sinoy.support.sms.dal.dataobject.SmsTemplateDO;
import com.sinoy.support.sms.service.SmsSendService;
import com.sinoy.support.sms.service.SmsTemplateService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/support/sms-template")
public class SmsTemplateController {

    @Resource
    private SmsTemplateService smsTemplateService;
    @Resource
    private SmsSendService smsSendService;

    //创建短信模板
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('system:sms-template:create')")
    public R createSmsTemplate(@Valid @RequestBody SmsTemplateCreateReqVO createReqVO) {
        smsTemplateService.createSmsTemplate(createReqVO);
        return R.ok();
    }

    //更新短信模板
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('system:sms-template:update')")
    public R updateSmsTemplate(@Valid @RequestBody SmsTemplateUpdateReqVO updateReqVO) {
        smsTemplateService.updateSmsTemplate(updateReqVO);
        return R.ok();
    }

    //删除短信模板
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('system:sms-template:delete')")
    public R deleteSmsTemplate(@RequestParam("id") Long id) {
        smsTemplateService.deleteSmsTemplate(id);
        return R.ok();
    }

    //获得短信模板
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('system:sms-template:query')")
    public R getSmsTemplate(@RequestParam("id") Long id) {
        SmsTemplateDO smsTemplate = smsTemplateService.getSmsTemplate(id);
        return R.ok().put("data",SmsTemplateConvert.INSTANCE.convert(smsTemplate));
    }

    //获得短信模板分页
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sms-template:query')")
    public R getSmsTemplatePage(@Valid SmsTemplatePageReqVO pageVO) {
        PageResult<SmsTemplateDO> pageResult = smsTemplateService.getSmsTemplatePage(pageVO);
        return R.ok().put("data",SmsTemplateConvert.INSTANCE.convertPage(pageResult));
    }

    //发送短信
    @PostMapping("/send-sms")
    @PreAuthorize("@ss.hasPermission('system:sms-template:send-sms')")
    public R sendSms(@Valid @RequestBody SmsTemplateSendReqVO sendReqVO) {
        smsSendService.sendSingleSmsToAdmin(sendReqVO.getMobile(), null,
                sendReqVO.getTemplateCode(), sendReqVO.getTemplateParams());
        return R.ok();
    }

}
