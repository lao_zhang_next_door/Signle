package com.sinoy.support.sms.convert;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.support.sms.controller.vo.log.SmsLogExcelVO;
import com.sinoy.support.sms.controller.vo.log.SmsLogRespVO;
import com.sinoy.support.sms.dal.dataobject.SmsLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 短信日志 Convert
 *
 */
@Mapper
public interface SmsLogConvert {

    SmsLogConvert INSTANCE = Mappers.getMapper(SmsLogConvert.class);

    SmsLogRespVO convert(SmsLogDO bean);

    List<SmsLogRespVO> convertList(List<SmsLogDO> list);

    PageResult<SmsLogRespVO> convertPage(PageResult<SmsLogDO> page);

    List<SmsLogExcelVO> convertList02(List<SmsLogDO> list);

}
