package com.sinoy.support.sms.controller.vo.log;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SmsLogPageReqVO extends PageParam {

    //短信渠道编号
    private Long channelId;

    //模板编号
    private Long templateId;

    //手机号
    private String mobile;

    //发送状态
    private Integer sendStatus;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //发送时间
    private Date[] sendTime;

    //接收状态
    private Integer receiveStatus;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //接收时间
    private Date[] receiveTime;

}
