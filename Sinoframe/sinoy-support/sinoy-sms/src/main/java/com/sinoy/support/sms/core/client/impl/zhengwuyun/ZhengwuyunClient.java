package com.sinoy.support.sms.core.client.impl.zhengwuyun;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

@Data
public class ZhengwuyunClient {

    public ZhengwuyunClient(String onlineUrl, String queueUrl, String code,String XHWID,String XHWAPPKEY) {
        this.onlineUrl = onlineUrl;
        this.queueUrl = queueUrl;
        this.code = code;
        this.XHWAPPKEY = XHWAPPKEY;
        this.XHWID = XHWID;
    }

    private String onlineUrl;


    private String queueUrl;


    private String code;


    private String XHWAPPKEY;


    private String XHWID;


    static {
        disableSslVerification();
    }

    private static void disableSslVerification() {
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
    

    // 超时默认时长，单位：毫秒
    private static final int mTimeOut = 5000;

    public static String doPost(String requestUrl, String param, Map<String, String> headsMap) throws Exception {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL url = new URL(requestUrl);
            // 打开和URL之间的连接
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setReadTimeout(mTimeOut);
            conn.setConnectTimeout(mTimeOut);

            //设置header信息
            if (headsMap != null && !headsMap.isEmpty()) {
                for (Map.Entry<String, String> entry : headsMap.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            if (param != null && !param.trim().equals("")) {
                out = new PrintWriter(conn.getOutputStream());
                // 发送请求参数
                out.print(param.replace("+","%2B"));
                // flush输出流的缓冲
                out.flush();
            }
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("调用接口报错，错误信息："+e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

}
