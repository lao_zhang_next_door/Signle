package com.sinoy.support.sms.controller.vo.template;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
public class SmsTemplateSendReqVO {

    //手机号
    @NotNull(message = "手机号不能为空")
    private String mobile;

    //模板编码
    @NotNull(message = "模板编码不能为空")
    private String templateCode;

    //模板参数
    private Map<String, Object> templateParams;

}
