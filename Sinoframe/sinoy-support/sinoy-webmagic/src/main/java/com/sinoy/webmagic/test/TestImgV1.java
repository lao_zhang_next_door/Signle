package com.sinoy.webmagic.test;

import net.sourceforge.tess4j.Tesseract;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class TestImgV1 {
    /** tessdata 路径 */
    private static final String TESSDATA_PATH = "D:\\mywork\\work\\git\\myGit\\verifyCode\\tesseract-main\\tessdata";
    /** 验证码图片地址 */
    private static final String url = "http://abc.com/validateCodeForIndex.do";

    public static void main(String[] args) throws Exception {
        BufferedImage codeImage = ImageIO.read(new File("D:\\test\\1677121947592.jpg"));
//        BufferedImage codeImage = ImageIO.read(new URL(url));

        // 本地做个备份，好对比
        write2disk(codeImage);

        Tesseract tessreact = new Tesseract();
        tessreact.setDatapath(TESSDATA_PATH);

        String result = tessreact.doOCR(codeImage);
        System.out.println("测验结果：[" + result.replace(" ", "").trim() + "]");
    }

    private static void write2disk(BufferedImage image) throws IOException {
        File file = new File("D:\\test\\code.jpg");
        ImageIO.write(image, "jpg", file);
    }
}
