//package com.sinoy.webmagic.test;
//
//
//import net.sourceforge.tess4j.Tesseract;
//import net.sourceforge.tess4j.TesseractException;
//import org.openqa.selenium.*;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import sun.misc.BASE64Decoder;
//import us.codecraft.webmagic.Page;
//import us.codecraft.webmagic.Site;
//import us.codecraft.webmagic.Spider;
//import us.codecraft.webmagic.pipeline.ConsolePipeline;
//import us.codecraft.webmagic.processor.PageProcessor;
//
//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.Set;
//
//public class DemoPageProcessor implements PageProcessor {
//
//    //配置抓取时间、重试次数等
////    private Site site = Site.me().setRetryTimes(5).setTimeOut(3000).setSleepTime(1000);
//
//    private Site site = Site.me();
//
//    //用来存储cookie信息
//    private Set<Cookie> cookies;
//
//    private WebDriver driver;
//
//    /** 图片识别 tessdata 路径 */
//    private static final String TESSDATA_PATH = "D:\\mywork\\work\\git\\myGit\\verifyCode\\tesseract-main\\tessdata";
//
//    @Override
//    public void process(Page page) {
//
//        System.out.println("进入process方法");
//
//        //重新进入登录平台
//        System.out.println("重新进入登录页面模拟登录");
//
//        try {
//            login();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        driver.close();
//
////        //设置POST请求
////        Request request = new Request("http://180.101.234.64:7001/jmgcyl/hde57/getAllByAreacodeList");
////        //只有POST请求才可以添加附加参数
////        request.setMethod(HttpConstant.Method.POST);
////
////
////        Map<String,Object> params = new HashMap(){
////            {
////                put("pageNumber",1);
////                put("pageSize",40);
////                put("ahae0042","01,06");
////                put("axbe0002","");
////                put("axbe0003","");
////                put("axbe0023","320213000000");
////                put("_modulePartId_","1000000000000002240");
////                put("frontUrl","http://180.101.234.64:8081/menu1.html#/InstitutionBasicInformation#/?_modulePartId_=1000000000000002240");
////            }
////        };
////        request.setRequestBody(HttpRequestBody.form(params,"utf-8"));
////        page.addTargetRequest(request);
//
//    }
//
//
//    //使用 Selenium 来模拟用户的登录获取cookie信息
//    public void login() throws InterruptedException, IOException {
//
//        //设置驱动程序路径
//        //Chrome
//        System.setProperty("webdriver.chrome.driver","D:\\mywork\\work\\git\\myGit\\verifyCode\\chromedriver.exe");
////        WebDriver driver = new ChromeDriver();
//
//        // 创建类DesiredCapabilities的对象
//        DesiredCapabilities cap = DesiredCapabilities.chrome();
//        // 设置ACCEPT_SSL_CERTS 变量值为true
//        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//        // 新建一个带capability的chromedriver对象
//        driver=new ChromeDriver(cap);
//
//        //IE 8-10
////        System.setProperty("webdriver.ie.driver","D:\\IEDriverServer.exe");
////        WebDriver driver = new InternetExplorerDriver();
//
//        //登陆地址
//        driver.get("http://180.101.234.64:8081/login.html");
//
//        //设置等待时间,防止页面没有渲染完成导致抓取元素失败
//        Thread.sleep(3000);
//
////        若是https证书过期网页则需要 点击继续操作
////        driver.
//
//
//
//        //定义验证码变量
//        String verify = null;
//
//        //寻找账号编辑框
//        WebElement userName = driver.findElement(By.id("username"));
//        userName.clear();
//        userName.sendKeys("320213");
//
//        //寻找密码编辑框
//        WebElement password = driver.findElement(By.id("password"));
//        password.clear();
//        password.sendKeys("LXQYL12345");
//
//        //创建一个时间戳,防止验证码图片文件重名
//        String timestamp = System.currentTimeMillis()+"";
//
//        //设置等待时间,防止页面没有渲染完成导致抓取元素失败
//        Thread.sleep(3000);
//
//
//        //模拟验证码
//        if(false){
//
//            //寻找验证码容器
//            WebElement ele = driver.findElement(By.tagName("img"));
//
//            //获取 验证码base64 并保存至本地
//            String codeBase64 = ele.getAttribute("src");
//            codeBase64 = codeBase64.substring("data:image/gif;base64,".length());
//            System.out.println(codeBase64);
//
//            BASE64Decoder base64Decoder = new BASE64Decoder();
//            byte[] b = base64Decoder.decodeBuffer(codeBase64);
//            // 调整异常数据
//            for (int i = 0; i < b.length; i++) {
//                if(b[i] < 0){
//                    b[i] += 256;
//                }
//            }
//
//            File imageFile = new File("D:/test/"+ timestamp + ".jpg");
//            FileOutputStream fileOutputStream = new FileOutputStream(imageFile);
//            fileOutputStream.write(b);
//            BufferedImage codeImage = ImageIO.read(new File("D:\\test\\1677121947592.jpg"));
//
//            Tesseract tessreact = new Tesseract();
//            tessreact.setDatapath(TESSDATA_PATH);
//
//            String result = null;
//            try {
//                result = tessreact.doOCR(codeImage);
//                result = result.replace(" ", "").trim();
//                System.out.println("识别结果》》》》"+ result);
//            } catch (TesseractException e) {
//                e.printStackTrace();
//            }
//        }
//
//        //模拟点击登录按钮
//        driver.findElement(By.cssSelector("body > div > div > div:nth-child(2) > div > div:nth-child(2) > form > div:nth-child(4) > div > div > div.ant-form-item-children > button")).click();
//        Thread.sleep(3000);
//        //获取cookie信息
//        cookies = driver.manage().getCookies();
//
////        driver.close();
////        driver.findElement(By.xpath("/html/body/div/div/div[2]/div/div[2]/form/div[4]/div/div/div[1]/button")).click();
////        driver.findElement(By.className("ant-btn ant-btn-primary ant-btn-block")).click();
//
//        //转至目标页面
////        Thread.sleep(3000);
////        driver.get("http://180.101.234.64:8081/menu1.html#/InstitutionBasicInformation#/?_modulePartId_=1000000000000002240");
//
//
////        //创建一个快照
////        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
////        File file = new File("D:/test/screen.jpg");
////        FileUtils.copyFile(screenshot,file);
////
////        //读取截图
////        BufferedImage fullImg = ImageIO.read(screenshot);
////
////        //获取页面上元素的位置
////        org.openqa.selenium.Point point= ele.getLocation();
////
////        //获取元素宽高
////        int eleWidth= ele.getSize().getWidth();
////        int eleHeight= ele.getSize().getHeight();
////
////        //裁剪整个页面截图只得到元素截图
////        BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
////        ImageIO.write(eleScreenshot, "png", screenshot);
////
////        //将验证码截图保存到本地
////        File screenshotLocation = new File("D:/test/"+timestamp+".jpg");
////        FileUtils.copyFile(screenshot, screenshotLocation);
////        String otherHost = "https://aip.baidubce.com/rest/2.0/ocr/v1/webimage";
//        // 本地图片路径
////        String filePath = "D:\\test\\"+timestamp+".jpg";
////        try {
////            byte[] imgData = FileUtil.readFileByBytes(filePath);
////            String imgStr = Base64Util.encode(imgData);
////            String params = URLEncoder.encode("image", "UTF-8") +
////                    "=" + URLEncoder.encode(imgStr, "UTF-8");
////
////            //线上环境access_token有过期时间,客户端可自行缓存,过期后重新获取
////
////            String accessToken = AipOcrInit.getAuth("API_KEY", "SECRET_KEY");
////
////            String result = HttpUtil.post(otherHost, accessToken, params);
////            BaiDuOCRBean baiDuOCRBean =
////                    com.alibaba.fastjson.JSONObject.toJavaObject(JSON.parseObject(result),
////                            BaiDuOCRBean.class);
////
////            List<Words_result> list = baiDuOCRBean.getWords_result();
////            for (int i = 0; i < list.size(); i++) {
////                System.out.println(list.get(i).getWords());
////                verify = list.get(i).getWords().replace(".","");
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        //寻找验证码编辑框
////        driver.findElement(By.id("验证码编辑框ID")).clear();
////        driver.findElement(By.id("验证码编辑框ID")).sendKeys(verify);
////
////        //模拟点击登录按钮
////        driver.findElement(By.className("登陆按钮ID")).click();
////
//
////        driver.close();
//    }
//    @Override
//    public Site getSite() {
//        System.out.println("cookies信息》》》》" + cookies);
//        //将获取到的cookie信息添加到webmagic中
//        if(cookies != null){
//            for (Cookie cookie : cookies) {
//                site.addCookie(cookie.getName().toString(),cookie.getValue().toString());
//            }
//        }
////
////
////        site.addHeader("Content-Length","163");
//        site.setTimeOut(10000);
//
//
//        return site;
//    }
//
//    public static void main(String[] args) throws InterruptedException, IOException{
//
////        Spider.create(new DemoPageProcessor()).addPipeline(
////                new ConsolePipeline()).addUrl("http://180.101.234.64:8081/login.html").thread(5).run();
//
//
//        DemoPageProcessor dpp = new DemoPageProcessor();
//        //调用selenium，进行模拟登录
//        dpp.login();
//
//        // 开始执行
//        try {
//            Spider.create(dpp)
//                    .addUrl("http://180.101.234.64:8081/index.html").addPipeline(new ConsolePipeline()).thread(1).run();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//
//}
