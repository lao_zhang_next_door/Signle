package com.sinoy.webmagic.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.scheduling.annotation.Scheduled;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

/**
 * 最后需要转exe的主类
 */
public class DlPageProcessor implements PageProcessor {

    /**
     * 爬取配置 爬取重试次数 间隔时间 超时时间等
     */
    private Site site = Site.me().setRetryTimes(2).setSleepTime(5000);

    /**
     * 配置文件配置
     */
    public static Properties properties = null;

    /**
     * Chrom 驱动程序路径
     */
    private String ChromPath = "";

    /**
     * 爬取的需要模拟登陆的页面地址
     */
    private String loginUrl = "";

    /**
     * 需要爬取数据的 页面地址
     */
    private String indexUrl = "";

    /**
     * 事先准备好的 登录名
     */
    private String loginId = "";

    /**
     * 事先准备好的 密码
     */
    private String password = "";

    /**
     * 存储文件路径 例如验证码图片等
     */
    private String saveFilePath = "";

    /**
     * 用来存储cookie信息
     */
    private Set<Cookie> cookies;

    private WebDriver driver;


    /**
     * 定义爬取过程
     * @param page
     */
    @Override
    public void process(Page page) {

        System.out.println("进入爬取方法！！！！！！！！！");


    }

    /**
     * 定义请求用到的头部信息 以及 爬虫 爬取次数 重试次数 间隔时间
     * @return
     */
    @Override
    public Site getSite() {
        return site;
    }

    /**
     * 模拟登录流程
     * @throws InterruptedException
     * @throws IOException
     */
    public void login() throws InterruptedException, IOException {
        //设置驱动程序路径  用chrome为例
        //Chrome
        System.setProperty("webdriver.chrome.driver",properties.getProperty("ChromPath"));
        // 创建类DesiredCapabilities的对象
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        // 设置ACCEPT_SSL_CERTS 变量值为true
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        // 新建一个带capability的chromedriver对象
        driver=new ChromeDriver(cap);
        //登陆地址
        driver.get(properties.getProperty("loginUrl"));
        //设置等待时间,防止页面没有渲染完成导致抓取元素失败
        Thread.sleep(3000);

        //寻找账号编辑框
        WebElement userName = driver.findElement(By.id("loginId"));
        userName.clear();
        userName.sendKeys(properties.getProperty("loginId"));

        //寻找密码编辑框
        WebElement password = driver.findElement(By.id("password"));
        password.clear();
        password.sendKeys(properties.getProperty("password"));

        //模拟点击登录按钮
        driver.findElement(By.xpath("//*[@id=\"login-submit\"]")).click();
        Thread.sleep(3000);
        //获取cookie信息
        cookies = driver.manage().getCookies();
    }

    /**
     * 优先读取外部config目录下 配置文件
     * @return
     * @throws IOException
     */
    private Properties loadProperties() throws IOException {
        properties = new Properties();
        InputStream is = null;
        File directory = new File("./config/config.properties");
        String filePath = directory.getCanonicalPath(); //获取绝对路径
        System.out.println("读取的文件路径为》》》》" + filePath);
        if(directory.exists()){
            is = new FileInputStream(directory);
        }else{
            //读取当前模块下 src下的地址
            ClassLoader classLoader = DlPageProcessor.class.getClassLoader();
            is = classLoader.getResourceAsStream("config.properties");
        }
        properties.load(is);
        return  properties;
    }


    /**
     * 启动方法
     * @param args
     */
//    @Scheduled(cron = "0 0/1 * * * ? ")
    public static void main(String[] args) {
        DlPageProcessor dpp = new DlPageProcessor();
        //读取配置
        try {
            dpp.loadProperties();
        } catch (IOException e) {
            System.out.println("读取配置错误,请检查配置文件");
            e.printStackTrace();
            return;
        }

        //调用selenium，进行模拟登录
        try {
            dpp.login();
        } catch (Exception e) {
            System.out.println("模拟登录报错");
            e.printStackTrace();
            return;
        }

        // 开始执行
        try {
            Spider.create(dpp)
                    .addUrl(properties.getProperty("indexUrl")).addPipeline(new ConsolePipeline()).thread(2).run();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
