//package com.sinoy.visualization.jmreport.config;
//
//import com.sinoy.visualization.jmreport.core.service.JmReportTokenServiceImpl;
//import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//
///**
// * 积木报表的配置类
// *
// */
//@Configuration
//@ComponentScan(basePackages = "org.jeecg.modules.jmreport") // 扫描积木报表的包
//public class JmReportConfiguration {
//
//    @Bean
//    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
//    public JmReportTokenServiceI jmReportTokenService(Oauth2Token OAuth2TokenApi oAuth2TokenApi) {
//        return new JmReportTokenServiceImpl(oAuth2TokenApi);
//    }
//
//}
