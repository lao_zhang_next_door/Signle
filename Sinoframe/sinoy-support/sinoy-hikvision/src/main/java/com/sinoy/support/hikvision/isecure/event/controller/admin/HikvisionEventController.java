package com.sinoy.support.hikvision.isecure.event.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.props.WebProperties;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.hikvision.isecure.event.controller.dto.*;
import com.sinoy.support.hikvision.isecure.event.service.EventApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/hikvision/event")
public class HikvisionEventController {

    @Resource
    private EventApi eventApi;

    @Value("${framework.url}")
    private String url;

    /**
     * 查询事件订阅详情
     */
    @GetMapping("/eventSubscriptionView")
    public R eventSubscriptionView() {
        EventSubscriptionViewRes res = eventApi.eventSubscriptionView();
        return R.ok().put("data", res);
    }

    /**
     * 查询联动事件列表
     */
    @PostMapping("/search")
    public R search(@RequestBody EventSearch eventSearch) {
        SearchRes res = eventApi.search(eventSearch);
        return R.ok().put("data", res);
    }

    /**
     * 发起订阅
     */
    @PostMapping("/subscribe")
    public R subscribe(@RequestBody EventSubscription eventSubscription) {
        if (StrUtil.isBlank(eventSubscription.getEventDest())) {
            // 默认回调地址
            eventSubscription.setEventDest(url + WebProperties.ExternalApi.getPrefix() +"/hikvision/event/callback");
        }
        SubscriptRes res = eventApi.eventSubscriptionByEventTypes(eventSubscription);
        return R.ok().put("data", res);
    }

    /**
     * 取消订阅
     */
    @PostMapping("/unsubscribe")
    public R unsubscribe(@RequestBody Integer[] eventTypes) {
        SubscriptRes res =  eventApi.eventUnSubscriptionByEventTypes(eventTypes);
        return R.ok().put("data", res);
    }

}
