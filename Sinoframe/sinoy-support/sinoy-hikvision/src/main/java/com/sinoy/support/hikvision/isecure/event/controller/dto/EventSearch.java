package com.sinoy.support.hikvision.isecure.event.controller.dto;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;

/**
 * 联动事件查询参数
 */
@Data
public class EventSearch extends PageParam {

    /**
     * 事件规则id
     */
    private String eventRuleId;

    /**
     * 事件分类
     */
    private String ability;

    /**
     * 区域编号
     */
    private String regionIndexCode;

    /**
     * 所属位置
     */
    private String[] locationIndexCodes;

    /**
     * 事件源名称
     */
    private String resName;

    /**
     * 事件源编号
     */
    private String[] resIndexCodes;

    /**
     * 事件源类型
     */
    private String[] resTypes;

    /**
     * 事件类型
     */
    private String eventType;

    /**
     * 事件等级 1-低，2-中，3-高
     */
    private String[] eventLevels;

    /**
     * 处理意见
     */
    private String remark;

    /**
     * 处理状态，0-未处理，1-已处理
     */
    private Integer handleStatus;

    /**
     * 开始时间,ISO8601时间
     */
    private String startTime;

    /**
     * 结束时间,ISO8601时间
     */
    private String endTime;
}
