package com.sinoy.support.hikvision.isecure.event.controller.external;

import com.sinoy.support.hikvision.isecure.event.controller.dto.EventData;
import com.sinoy.support.hikvision.isecure.event.controller.dto.EventDataThird;
import com.sinoy.support.hikvision.isecure.event.controller.dto.Params;
import com.sinoy.support.hikvision.isecure.event.service.HikEventCallBackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;

@RestController
@RequestMapping("/hikvision/event")
@Slf4j
public class HikvisionEventCallBackController {

    @Resource
    private HikEventCallBackService hikEventCallBackService;

    /**
     * 消息回调
     * 订阅消息的时候 写入这个回调接口的地址，这个是官方的回调
     */
    @PermitAll
    @PostMapping("/callback")
    private void handleCallBack(@RequestBody EventData eventData) {
        log.info("receive callback: {}", eventData);
        hikEventCallBackService.callback(eventData);
    }

    /**
     * 消息回调
     * 这个是第三方的回调
     */
    @PermitAll
    @PostMapping("/callback_third")
    private void handleCallBack_third(@RequestBody EventDataThird eventData) {
        log.info("receive callback_third: {}", eventData);
        hikEventCallBackService.callback_third(eventData);
    }

}
