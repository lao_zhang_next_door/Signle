package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

@Data
public class EventData {
    /**
     * 方法名，用于标识报文用途
     */
    private String method;
    /**
     * 事件参数信息
     */
    private Params params;
}
