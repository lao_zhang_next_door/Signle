package com.sinoy.support.hikvision.isecure.event.service;


import com.sinoy.support.hikvision.isecure.ISecureApi;
import com.sinoy.support.hikvision.isecure.event.controller.dto.*;

/**
 * 事件相关
 */
public interface EventApi extends ISecureApi {

    /**
     * 查询事件订阅详情。
     * description: 查询事件订阅详情
     * version v1
     * url: /api/eventService/v1/eventSubscriptionView
     * method: post
     */
    EventSubscriptionViewRes eventSubscriptionView();

    /**
     * 获取联动事件列表
     * description: 通过输入查询条件获取事件列表，通过分页的形式获取事件日志大概描述信息。
     * version v1
     * url: /api/els/v1/events/search
     * method: post
     * content-type: application/json
     */
    SearchRes search(EventSearch eventSearch);

    /**
     * 按事件类型取消订阅
     * description: 应用方指定事件类型取消订阅。
     * version v1
     * url: /api/eventService/v1/eventUnSubscriptionByEventTypes
     * method: post
     * content-type: application/json
     */
    SubscriptRes eventUnSubscriptionByEventTypes(Integer[] eventTypes);

    /**
     * 按事件类型订阅
     * description: 该接口用于满足应用方按事件类型码订阅事件，同一个用户重复订阅相同的事件，接口内部逻辑自动去重，确保不重复投递事件。
     * version v1
     * url: /api/eventService/v1/eventSubscriptionByEventTypes
     * method: post
     * content-type: application/json
     */
    SubscriptRes eventSubscriptionByEventTypes(EventSubscription eventSubscription);
}
