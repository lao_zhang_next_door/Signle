package com.sinoy.support.hikvision.isecure.event.service;

import com.alibaba.fastjson2.JSON;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import com.sinoy.support.hikvision.isecure.event.controller.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 事件默认实现
 */
@Component
@Slf4j
public class EventApiImpl implements EventApi {

    @Value("${monitor.hikvision.iSecure_Center.host}")
    private String host;

    @Value("${monitor.hikvision.iSecure_Center.appKey}")
    private String appKey;

    @Value("${monitor.hikvision.iSecure_Center.appSecret}")
    private String appSecret;

    @Value("${monitor.hikvision.iSecure_Center.artemisPath}")
    private String artemisPath;

    @PostConstruct
    public void init() {
        ArtemisConfig.host = host;
        ArtemisConfig.appKey = appKey;
        ArtemisConfig.appSecret = appSecret;
    }

    @Override
    public EventSubscriptionViewRes eventSubscriptionView() {
        String result = doPost("/api/eventService/v1/eventSubscriptionView", "", "");
        return JSON.parseObject(result, EventSubscriptionViewRes.class);
    }

    @Override
    public SearchRes search(EventSearch eventSearch) {
        String result = doPost("/api/els/v1/events/search", JSON.toJSONString(eventSearch), "application/json");
        return JSON.parseObject(result, SearchRes.class);
    }

    @Override
    public SubscriptRes eventUnSubscriptionByEventTypes(Integer[] eventTypes) {
        String result = doPost("/api/eventService/v1/eventUnSubscriptionByEventTypes", JSON.toJSONString(eventTypes), "application/json");
        return JSON.parseObject(result, SubscriptRes.class);
    }

    @Override
    public SubscriptRes eventSubscriptionByEventTypes(EventSubscription eventSubscription) {
        String result = doPost("/api/eventService/v1/eventUnSubscriptionByEventTypes", JSON.toJSONString(eventSubscription), "application/json");
        return JSON.parseObject(result, SubscriptRes.class);
    }

    private String doPost(String url, String data, String contentType) {
        String regionsDataApi = artemisPath + url;
        Map<String,String> path = new HashMap<String,String>(2){
            {
                put("https://",regionsDataApi);
            }
        };

        log.info("api " + url + " params:{}","");
        String result = ArtemisHttpUtil.doPostStringArtemis(path,data,null,null,"application/json");
        log.info("api " + url + " result:{}",result);
        return result;
    }

    private String doGet() {
        return "";
    }
}
