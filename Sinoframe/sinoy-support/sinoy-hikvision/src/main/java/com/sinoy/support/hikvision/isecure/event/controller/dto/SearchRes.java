package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class SearchRes {

    private String code;

    private String msg;

    private ResData data;

    @Data
    public class ResData {

        // 当前页
        private Integer pageNo;

        // 每页数量
        private Integer pageSize;

        // 总记录数
        private Integer totalCount;

        // 结果集
        private List<ResList> list;

        @Data
        public class ResList {

            // 事件等级,1-低，2-中，3-高
            private Integer eventLevel;

            // 事件等级颜色
            private String eventLevelColor;

            // 事件等级value
            private String eventLevelValue;

            // 事件规则id
            private String eventRuleId;

            // 事件类型名称
            private String eventTypeName;

            // 事件处理意见
            private String remark;

            // 事件处理状态，0-未处理，1-已处理
            private Integer handleStatus;

            // 事件id
            private String id;

            // 事件规则名称
            private String ruleName;

            // 事件开始时间
            private String startTime;

            // 事件结束时间
            private String endTime;

            // 事件记录列表
            private List<EventLogSrcList> eventLogSrcList;

            @Data
            public class EventLogSrcList {

                // 事件分类
                private String ability;

                // 事件扩展信息
                private String data;

                // 事件id
                private String eventLogId;

                // 事件类型
                private Integer eventType;

                // 事件类型名称
                private String eventTypeName;

                // 扩展字段1
                private String extend1;

                // 扩展字段2
                private String extend2;

                // 事件源id
                private String id;

                // 所属位置
                private String locationIndexCode;

                // 所属位置名称
                private String locationName;

                // 所属区域编号
                private String regionIndexCode;

                // 所属区域名称
                private String regionName;

                // 事件源名称
                private String resName;

                // 事件源编号
                private String resIndexCode;

                // 事件源类型
                private String resType;

                // 事件开始时间
                private String startTime;
            }

        }
    }

}
