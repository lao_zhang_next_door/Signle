package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

@Data
public class EventDataThird {
    /**
     * 事件唯一标识。
     */
    private String eventId;

    /**
     * 事件类型。
     */
    private int eventType;

    /**
     * 事件发生时间（设备时间），ISO8601格式，示例：2018-08-15T15:53:47.000+08:00。
     */
    private String happenTime;

    /**
     * 设备编码
     */
    private String srcIndex;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 通道编号
     */
    private String channelID;
    /**
     * 通道名称
     */
    private String channelName;
    /**
     * 图片
     */
    private String imageUrls;
    /**
     * 学校名称
     */
    private String orgName;
    /**
     * 学校code
     */
    private String orgCode;

}
