package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

@Data
public class SubscriptRes {

    private String code;

    private String msg;

}
