package com.sinoy.support.hikvision.isecure.event.controller.dto;

import java.util.List;

@lombok.Data
public class Data {

    // 数据模型标识
    private String dataType;

    // 数据接收时间
    private String recvTime;

    // 数据发送时间
    private String sendTime;

    // 数据触发时间
    private String dateTime;

    // 设备的IP地址
    private String ipAddress;

    // 设备端口号
    private int portNo;

    // 设备通道号，默认从1开始
    private int channelID;

    // 事件类型名称
    private String eventDescription;

    // 事件类型
    private String eventType;

    // 分析结果
    private List<FieldDetection> fieldDetection;

    @lombok.Data
    class FieldDetection {

        // 可选的透传字段
        private TargetAttributes targetAttrs;

        // 背景图URL
        private String imageUrl;

        // 行为事件触发时间阈值
        private int duration;

        // 灵敏度参数
        private int sensitivityLevel;

        // 占比
        private int rate;

        // 检测目标
        private int detectionTarget;

        // 区域范围
        private List<RegionCoordinates> regionCoordinatesList;
    }

    // 透传字段内部类
    @lombok.Data
    public class TargetAttributes {
        private String imageServerCode; // 图片服务编号
        private String deviceIndexCode; // 设备编号，平台关联的编码
        private String cameraIndexCode; // 监控点编码，平台关联的编码
        private String channelName; // 通道名称
        private String cameraAddress; // 监控点安装地址
        private float longitude; // 监控点所在经度
        private float latitude; // 监控点所在纬度
    }

    // 区域坐标内部类
    @lombok.Data
    public class RegionCoordinates {
        private float positionX; // X轴坐标
        private float positionY; // Y轴坐标
    }

}
