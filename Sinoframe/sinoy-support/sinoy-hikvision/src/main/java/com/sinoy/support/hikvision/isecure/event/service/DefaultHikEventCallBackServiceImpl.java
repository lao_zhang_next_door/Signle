package com.sinoy.support.hikvision.isecure.event.service;

import com.sinoy.support.hikvision.isecure.event.controller.dto.EventData;
import com.sinoy.support.hikvision.isecure.event.controller.dto.EventDataThird;
import com.sinoy.support.hikvision.isecure.event.controller.dto.Params;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service("HikEventCallBackService")
public class DefaultHikEventCallBackServiceImpl implements HikEventCallBackService {

    /**
     * 默认回调
     * @param eventData
     */
    @Override
    public void callback(EventData eventData) {
        log.info("default callback data:{}", eventData);
    }

    /**
     * 海康第三方回调
     * @param eventData
     */
    @Override
    public void callback_third(EventDataThird eventData) {
        log.info("default callback data:{}", eventData);
    }

}
