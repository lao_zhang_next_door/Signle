package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class Params {

    // 事件从接收者（程序处理后）发出的时间
    String sendTime;

    // 事件类别
    String ability;

    // 事件信息
    List<Events> events;

}
