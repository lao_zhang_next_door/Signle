package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

@Data
public class Events {

    /**
     * 事件唯一标识。
     */
    private String eventId;

    /**
     * 事件源编号，物理设备是资源编号。
     */
    private String srcIndex;

    /**
     * 事件源类型。
     */
    private String srcType;

    /**
     * 事件源名称（可选）。
     */
    private String srcName;

    /**
     * 事件类型。
     */
    private int eventType;

    /**
     * 事件状态。
     * 0-瞬时
     * 1-开始
     * 2-停止
     * 4-事件联动结果更新
     * 5-事件图片异步上传
     */
    private int status;

    /**
     * 事件等级（可选）。
     * 0-未配置
     * 1-低
     * 2-中
     * 3-高
     * 注意：此处事件等级是指在事件联动中配置的等级，需要配置了事件联动，才返回这个字段。
     */
    private Integer eventLvl;

    /**
     * 脉冲超时时间，单位：秒。
     */
    private int timeout;

    /**
     * 事件发生时间（设备时间），ISO8601格式，示例：2018-08-15T15:53:47.000+08:00。
     */
    private String happenTime;

    /**
     * 事件发生的事件源父设备编码（可选）。
     */
    private String srcParentIndex;

    private com.sinoy.support.hikvision.isecure.event.controller.dto.Data data;

}
