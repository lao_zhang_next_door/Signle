package com.sinoy.support.hikvision.isecure.event.service;


import com.sinoy.support.hikvision.isecure.event.controller.dto.EventData;
import com.sinoy.support.hikvision.isecure.event.controller.dto.EventDataThird;
import com.sinoy.support.hikvision.isecure.event.controller.dto.Params;

public interface HikEventCallBackService {

    /**
     * 事件订阅回调
     * @param eventData
     */
    void callback(EventData eventData);

    /**
     * 事件订阅回调
     * @param eventData
     */
    void callback_third(EventDataThird eventData);

}
