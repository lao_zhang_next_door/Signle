package com.sinoy.support.hikvision.isecure.event.controller.dto;

import lombok.Data;

import java.util.List;

/**
 * 事件订阅详情返回
 */
@Data
public class EventSubscriptionViewRes {

    private String code;

    private String msg;

    private ResData data;

    @Data
    public class ResData {

        private Detail detail;

        @Data
        public class Detail {
            // 事件类型
            private List<Integer> eventTypes;

            // 事件接收地址
            private String eventDest;
        }

    }

}
