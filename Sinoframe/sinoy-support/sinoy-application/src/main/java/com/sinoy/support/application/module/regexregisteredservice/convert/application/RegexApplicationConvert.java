package com.sinoy.support.application.module.regexregisteredservice.convert.application;

import java.util.*;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.*;
import com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application.RegexApplicationDO;

/**
 * 应用注册 Convert
 *
 * @author 管理员
 */
@Mapper
public interface RegexApplicationConvert {

    RegexApplicationConvert INSTANCE = Mappers.getMapper(RegexApplicationConvert.class);

    RegexApplicationDO convert(RegexApplicationCreateReqVO bean);

    RegexApplicationDO convert(RegexApplicationUpdateReqVO bean);

    RegexApplicationRespVO convert(RegexApplicationDO bean);

    List<RegexApplicationRespVO> convertList(List<RegexApplicationDO> list);

    Page<RegexApplicationRespVO> convertPage(Page<RegexApplicationDO> page);


}
