package com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application;

import lombok.*;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

import javax.persistence.Transient;

/**
 * 应用注册 DO
 *
 * @author 管理员
 */
@TableName("regexregisteredservice")
@KeySequence("regexregisteredservice_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegexApplicationDO {


    /**
    * 行标
    */
    @TableId(type = IdType.AUTO)
    private Long id;



    /**
    * 通配规则  regex正则   ant类正则  
    */
    private String expressionType;



    /**
    * 1
    */
    private String accessStrategy;



    /**
    * 1
    */
    private String attributeRelease;



    /**
    * 描述
    */
    private String description;



    /**
    * 1
    */
    private Integer evaluationOrder;



    /**
    * 1
    */
    private String expirationPolicy;



    /**
    * 1
    */
    @TableField(value="informationUrl")
    private String informationUrl;



    /**
    * 应用图标
    */
    private String logo;



    /**
     * 应用图标url
     */
    @Transient
    @TableField(exist = false)
    private String logoUrl;



    /**
    * 1
    */
    private Integer logoutType;



    /**
    * 注销地址
    */
    private String logoutUrl;



    /**
    * 1
    */
    private String mfaPolicy;



    /**
    * 应用名称
    */
    private String name;



    /**
    * 1
    */
    @TableField(value="privacyUrl")
    private String privacyUrl;



    /**
    * 1
    */
    private String proxyPolicy;



    /**
    * 1
    */
    private String publicKey;



    /**
    * 1
    */
    private String requiredHandlers;



    /**
    * 1
    */
    @TableField(value="responseType")
    private String responseType;



    /**
    * 服务地址
    */
    @TableField(value="serviceId")
    private String serviceId;



    /**
    * 1
    */
    private String theme;



    /**
    * 1
    */
    private String usernameAttr;



    /**
     *  关联部门id
     */
    private Integer deptId;

}
