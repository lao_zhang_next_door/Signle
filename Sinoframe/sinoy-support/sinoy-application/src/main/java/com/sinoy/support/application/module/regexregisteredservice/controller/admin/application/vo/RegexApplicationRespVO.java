package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo;

import lombok.*;
import java.util.*;

//管理后台 - 应用注册 Response VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegexApplicationRespVO extends RegexApplicationBaseVO {

    //行标
    private Long id;

    //logo url
    private String logoUrl;

    //关联部门id
    private Integer deptId;

}
