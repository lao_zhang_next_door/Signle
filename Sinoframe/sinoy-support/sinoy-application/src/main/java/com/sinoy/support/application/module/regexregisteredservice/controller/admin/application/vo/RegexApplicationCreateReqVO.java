package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

//管理后台 - 应用注册创建 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegexApplicationCreateReqVO extends RegexApplicationBaseVO {

}
