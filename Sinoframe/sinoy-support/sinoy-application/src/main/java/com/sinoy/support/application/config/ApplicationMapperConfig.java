package com.sinoy.support.application.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan({"com.sinoy.support.application.mapper","com.sinoy.**.module.*.dal.mysql.*"})
@Configuration
public class ApplicationMapperConfig {
}
