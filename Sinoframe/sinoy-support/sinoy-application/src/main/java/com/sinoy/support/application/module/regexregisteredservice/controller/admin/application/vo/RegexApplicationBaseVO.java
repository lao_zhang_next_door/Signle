package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

/**
* 应用注册 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里
*/
@Data
public class RegexApplicationBaseVO {

    //通配规则  regex正则   ant类正则
    private String expressionType;


    //描述
    private String description;


    //应用图标
    @NotNull(message = "图标")
    private String logo;


    //注销地址
    @NotNull(message = "应用url不能为空")
    private String logoutUrl;


    //应用名称
    @NotNull(message = "应用名称不能为空")
    private String name;


    //服务地址
    private String serviceId;

}
