package com.sinoy.support.application.module.regexregisteredservice.service.application;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.core.common.utils.sql.BatisUtil;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.util.*;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.*;
import com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application.RegexApplicationDO;
import com.sinoy.support.application.module.regexregisteredservice.convert.application.RegexApplicationConvert;
import com.sinoy.support.application.module.regexregisteredservice.dal.mysql.application.RegexApplicationMapper;
import com.sinoy.core.common.utils.exception.BaseException;

/**
 * 应用注册 Service 实现类
 *
 * @author 管理员
 */
@Service
@Validated
public class RegexApplicationServiceImpl extends BaseServiceImpl<RegexApplicationMapper, RegexApplicationDO> implements RegexApplicationService {

    @Resource
    private RegexApplicationMapper regexApplicationMapper;

    @Resource
    private CommonPropAndMethods commonPropAndMethods;

    @Override
    public Long createRegexApplication(RegexApplicationCreateReqVO createReqVO) {

        // 验证应用url 是否已存在
        Long count = regexApplicationMapper.selectCount(new LambdaQueryWrapper<RegexApplicationDO>().eq(RegexApplicationDO::getLogoutUrl,createReqVO.getLogoutUrl()));
        if(count > 0){
            throw new BaseException("这个url已经存在");
        }

        // 插入
        RegexApplicationDO regexApplication = RegexApplicationConvert.INSTANCE.convert(createReqVO);

        //serviceId,可以配置为正则匹配
        String a = "^" + createReqVO.getLogoutUrl() + ".*";
        regexApplication.setServiceId(a);
        //设置通配符规则
        regexApplication.setExpressionType("regex");
        //设置服务执行顺序
        regexApplication.setEvaluationOrder(1000);
        regexApplicationMapper.insert(regexApplication);
        // 返回
        return regexApplication.getId();
    }

    @Override
    public void updateRegexApplication(RegexApplicationUpdateReqVO updateReqVO) {
        // 校验存在
        this.validateRegexApplicationExists(updateReqVO.getId());

        // 设置通配符规则
        String serviceId = "^" + updateReqVO.getLogoutUrl() + ".*";
        updateReqVO.setServiceId(serviceId);

        // 更新
        RegexApplicationDO updateObj = RegexApplicationConvert.INSTANCE.convert(updateReqVO);
        regexApplicationMapper.updateById(updateObj);
    }

    @Override
    public void deleteRegexApplication(Long id) {
        // 校验存在
        this.validateRegexApplicationExists(id);
        // 删除
        regexApplicationMapper.deleteById(id);
    }

    private void validateRegexApplicationExists(Long id) {
        if (regexApplicationMapper.selectById(id) == null) {
            throw new BaseException("该条记录不存在");
        }
    }

    @Override
    public RegexApplicationDO getRegexApplication(Long id) {
        return regexApplicationMapper.selectById(id);
    }

    @Override
    public List<RegexApplicationDO> getRegexApplicationList(Collection<Long> ids) {
        return regexApplicationMapper.selectBatchIds(ids);
    }

    @Override
    public Page<RegexApplicationDO> getRegexApplicationPage(RegexApplicationPageReqVO pageReqVO) {
        Page<RegexApplicationDO> page = new Page(pageReqVO.getCurrentPage(), pageReqVO.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
//        BatisPlusUtil.setOrders(queryWrapper, null, null);
        if(pageReqVO.getProp() != null){
            for (int i = 0; i < pageReqVO.getProp().size(); i++) {
                queryWrapper.orderBy(true, "ASC".equals(pageReqVO.getOrder().get(i).toUpperCase())
                        , BatisUtil.toDbField(pageReqVO.getProp().get(i)));
            }
        }
        BatisPlusUtil.checkParamsIncluded(queryWrapper, pageReqVO, pageReqVO.getParams());
//        return regexApplicationMapper.selectPage(page, queryWrapper);

        return regexApplicationMapper.getRegexApplicationPage(page,pageReqVO);
    }

    @Override
    public List<RegexApplicationDO> getService() {
        XnwUser user = commonPropAndMethods.getXnwUser();
        Optional.ofNullable(user).orElseThrow(()->new BaseException("用户不存在"));
        // 当前部门code
        String deptCode = user.getDeptCode();
        return regexApplicationMapper.getService(deptCode);
    }


}
