package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo;

import lombok.*;
import java.util.*;
import com.sinoy.core.common.utils.request.PageParam;

//管理后台 - 应用注册分页 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegexApplicationPageReqVO extends PageParam {

    //通配规则  regex正则   ant类正则  
    private String expressionType;

    //描述
    private String description;

    //应用图标
    private String logo;

    //注销地址
    private String logoutUrl;

    //应用名称
    private String name;

    //服务地址
    private String serviceId;

}
