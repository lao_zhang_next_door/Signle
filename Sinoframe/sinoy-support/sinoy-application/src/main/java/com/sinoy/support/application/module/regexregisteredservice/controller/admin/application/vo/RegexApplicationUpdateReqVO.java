package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

//管理后台 - 应用注册更新 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RegexApplicationUpdateReqVO extends RegexApplicationBaseVO {

    //行标
    @NotNull(message = "行标不能为空")
    private Long id;

    //部门id不能为空
    @NotNull(message = "部门id不能为空")
    private Integer deptId;


}
