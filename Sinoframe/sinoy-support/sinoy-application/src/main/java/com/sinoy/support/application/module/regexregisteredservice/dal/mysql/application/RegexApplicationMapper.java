package com.sinoy.support.application.module.regexregisteredservice.dal.mysql.application;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application.RegexApplicationDO;
import org.apache.ibatis.annotations.Mapper;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 应用注册 Mapper
 *
 * @author 管理员
 */
@Mapper
public interface RegexApplicationMapper extends ExtendMapper<RegexApplicationDO> {

    /**
     * 应用分页
     * @param page
     * @return
     */
    Page<RegexApplicationDO> getRegexApplicationPage(Page<RegexApplicationDO> page, RegexApplicationPageReqVO pageReqVO);

    /**
     * 获取当前用户的应用
     * @param deptCode
     * @return
     */
    @Select("select t.*,a.url as logoUrl,max(a.create_time) from regexregisteredservice t " +
            "left join frame_attach a on a.form_row_guid = t.logo " +
            "where locate(concat(t.dept_id,'.'),#{userDeptCode}) > 0 " +
            "or locate(concat('.',t.dept_id),${userDeptCode}) > 0 or t.dept_id = #{userDeptCode} GROUP BY a.create_time,t.id,a.url ")
    List<RegexApplicationDO> getService(@Param("userDeptCode") String deptCode);
}
