package com.sinoy.support.application.module.regexregisteredservice.controller.admin.application;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.RegexApplicationCreateReqVO;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.RegexApplicationPageReqVO;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.RegexApplicationUpdateReqVO;
import com.sinoy.support.application.module.regexregisteredservice.convert.application.RegexApplicationConvert;
import com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application.RegexApplicationDO;
import com.sinoy.support.application.module.regexregisteredservice.service.application.RegexApplicationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

//管理后台 - 应用注册
@RestController
@RequestMapping("/regexregisteredservice/regex-application")
@Validated
public class RegexApplicationController {

    @Resource
    private RegexApplicationService regexApplicationService;

    //创建应用注册
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:create')")
    public R createRegexApplication(@Valid @RequestBody RegexApplicationCreateReqVO createReqVO) {
        return R.ok().put("data", regexApplicationService.createRegexApplication(createReqVO));
    }

    //更新应用注册
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:update')")
    public R updateRegexApplication(@Valid @RequestBody RegexApplicationUpdateReqVO updateReqVO) {
        regexApplicationService.updateRegexApplication(updateReqVO);
        return R.ok();
    }

    //删除应用注册
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:delete')")
    public R deleteRegexApplication(@RequestParam("rowId") Long id) {
        regexApplicationService.deleteRegexApplication(id);
        return R.ok();
    }

    //批量删除应用注册
    @DeleteMapping("/deleteBatch")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:delete')")
    public R deleteRegexApplicationBatch(@RequestBody Long[] ids) {
        regexApplicationService.deleteBatchByIds(ids);
        return R.ok();
    }

    //获得应用注册
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:query')")
    public R getRegexApplication(@RequestParam("id") Long id) {
        RegexApplicationDO regexApplication = regexApplicationService.getRegexApplication(id);
        return R.ok().put("data", RegexApplicationConvert.INSTANCE.convert(regexApplication));
    }

    //获得应用注册列表
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:query')")
    public R getRegexApplicationList(@RequestParam("ids") Collection<Long> ids) {
        List<RegexApplicationDO> list = regexApplicationService.getRegexApplicationList(ids);
        return R.ok().put("data", RegexApplicationConvert.INSTANCE.convertList(list));
    }


    //获得应用注册分页
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('regexregisteredservice:regex-application:query')")
    public R getRegexApplicationPage(@Valid RegexApplicationPageReqVO pageVO) {
        Page<RegexApplicationDO> pageResult = regexApplicationService.getRegexApplicationPage(pageVO);
        return R.ok().put("data", RegexApplicationConvert.INSTANCE.convertPage(pageResult));
    }

    //获得当前人的所有应用
    @GetMapping("/getService")
    public R getService(){
        List<RegexApplicationDO> list = regexApplicationService.getService();
        return R.ok().put("data",RegexApplicationConvert.INSTANCE.convertList(list));
    }


}
