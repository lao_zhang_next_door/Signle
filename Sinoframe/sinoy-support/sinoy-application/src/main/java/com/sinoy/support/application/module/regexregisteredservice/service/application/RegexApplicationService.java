package com.sinoy.support.application.module.regexregisteredservice.service.application;

import java.util.*;
import javax.validation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.support.application.module.regexregisteredservice.controller.admin.application.vo.*;
import com.sinoy.support.application.module.regexregisteredservice.dal.dataobject.application.RegexApplicationDO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 应用注册 Service 接口
 *
 * @author 管理员
 */
public interface RegexApplicationService extends BaseService<RegexApplicationDO> {

    /**
     * 创建应用注册
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createRegexApplication(@Valid RegexApplicationCreateReqVO createReqVO);

    /**
     * 更新应用注册
     *
     * @param updateReqVO 更新信息
     */
    void updateRegexApplication(@Valid RegexApplicationUpdateReqVO updateReqVO);

    /**
     * 删除应用注册
     *
     * @param id 编号
     */
    void deleteRegexApplication(Long id);

    /**
     * 获得应用注册
     *
     * @param id 编号
     * @return 应用注册
     */
    RegexApplicationDO getRegexApplication(Long id);

    /**
     * 获得应用注册列表
     *
     * @param ids 编号
     * @return 应用注册列表
     */
    List<RegexApplicationDO> getRegexApplicationList(Collection<Long> ids);

    /**
     * 获得应用注册分页
     *
     * @param pageReqVO 分页查询
     * @return 应用注册分页
     */
    Page<RegexApplicationDO> getRegexApplicationPage(RegexApplicationPageReqVO pageReqVO);

    /**
     * 获取当前用户的所有应用
     * @return
     */
    List<RegexApplicationDO> getService();
}
