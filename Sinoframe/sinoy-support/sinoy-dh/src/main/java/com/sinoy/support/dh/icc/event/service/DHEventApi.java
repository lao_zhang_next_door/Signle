package com.sinoy.support.dh.icc.event.service;

import com.sinoy.support.dh.icc.event.controller.dto.EventSubscription;
import com.sinoy.support.dh.icc.event.controller.dto.SubscriptRes;

public interface DHEventApi {

    /**
     * 按事件类型取消订阅
     * description: 取消事件订阅接口,已保证不再接收推送消息，参数为json形式的对象（通用事件格式）。
     * version v1.0.0
     * url: /evo-apigw/evo-event/1.0.0/subscribe/mqinfo?name=10.33.75.97_8918
     * method: delete
     * content-type: application/json
     */
    SubscriptRes eventUnSubscriptionByEventTypes(String name);

    /**
     * 按事件类型订阅
     * description: 事件订阅接口,对外监听类型（monitorType）是url，要求post请求无鉴权或者鉴权固定带在url上能访问，参数为json形式的对象（通用事件格式）。
     * 订阅者名称需唯一，可识别，便于修改订阅信息，如示例：10.35.111.10_8010、10.35.111.10_8010_alarm
     * authorities/types/orgs/nodeCodes设置{}或字段不定义代表订阅所有，设置[]代表不订阅
     * orgs与nodeCodes分别是组织与设备/通道，两者是并集（条件是orgs || nodeCodes）
     * orgs=[A],nodeCodes=[B],则过滤条件是根据A||B判断 ，不是A&&B
     * 仅根据orgs过滤，则需设置nodeCodes=[],否则会订阅所有
     * 仅根据nodeCodes过滤，则需设置orgs=[],否则会订阅所有
     * version v1.0.0
     * url: /evo-apigw/evo-event/1.0.0/subscribe/mqinfo
     * method: post
     * content-type: application/json
     */
    SubscriptRes eventSubscriptionByEventTypes(EventSubscription eventSubscription) throws Exception;




}
