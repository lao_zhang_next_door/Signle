package com.sinoy.support.dh.icc.event.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.props.WebProperties;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.support.dh.icc.event.controller.dto.EventSubscription;
import com.sinoy.support.dh.icc.event.controller.dto.Monitors;
import com.sinoy.support.dh.icc.event.controller.dto.SubscriptRes;
import com.sinoy.support.dh.icc.event.service.DHEventApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/dh/event")
public class DHEventController {
    @Value("${framework.url}")
    private String url;

    @Resource
    private DHEventApi eventApi;
    /**
     * 发起订阅
     */
    @PostMapping("/subscribe")
    public R subscribe(@RequestBody EventSubscription eventSubscription) throws Exception {

        List<Monitors> monitorsList = eventSubscription.getMonitors();
        monitorsList.forEach(monitor -> {
           if (StrUtil.isBlank(monitor.getMonitor())) {
               // 设置默认回调地址
               monitor.setMonitor(url + WebProperties.ExternalApi.getPrefix() +"/dh/event/callback");
           }
        });
        SubscriptRes res = eventApi.eventSubscriptionByEventTypes(eventSubscription);
        return R.ok().put("data", res);
    }

    /**
     * 取消订阅
     */
    @DeleteMapping("/unsubscribe")
    public R unsubscribe(@RequestParam String name) {
        SubscriptRes res =  eventApi.eventUnSubscriptionByEventTypes(name);
        return R.ok().put("data", res);
    }
}
