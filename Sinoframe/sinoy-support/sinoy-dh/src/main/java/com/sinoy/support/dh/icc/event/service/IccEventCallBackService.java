package com.sinoy.support.dh.icc.event.service;

import com.sinoy.support.dh.icc.event.controller.dto.EventData;

public interface IccEventCallBackService {

    /**
     * 事件订阅回调
     * @param data
     */
    void callback(EventData data);
}
