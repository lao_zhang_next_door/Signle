package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

@Data
public class EventData {
    /**
     * 事件大类
     */
    private String category;
    /**
     * 方法名
     */
    private String method;
    /**
     * 具体业务信息
     */
    private InfoData info;
    /**
     * 序号
     */
    private Long id;
    /**
     * 子系统名称
     */
    private String subsystem;
    /**
     * 域ID
     */
    private String domainId;
}
