package com.sinoy.support.dh.icc.event.controller.dto;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;

@Data
public class SubscriptRes {
    /**
     * 接口返回是否成功
     */
    private Boolean success;
    /**
     * 错误码：0为无错误，非0为具体错误码；0代表事件订阅成功，不代表可以接收成功，需保证订阅监听地址在ICC服务器telnet通
     */
    private String code;
    /**
     * 错误消息
     */
    private String errMsg;
    /**
     * 返回数据对象
     */
    private Object data;
}
