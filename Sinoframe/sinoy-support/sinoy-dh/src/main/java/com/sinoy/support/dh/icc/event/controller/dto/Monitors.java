package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class Monitors {
    /**
     * 监听地址url地址(由第三方提供，必须在ICC服务器上telnet通)，要求Method=POST，请求头与响应头必须ContentType=application/json,无鉴权或者鉴权固定带在url上，参数为json形式的对象（通用事件格式）;
     * 订阅接收服务参考SDK demo的com.dahuatech.icc.demo.event.ReceiveSubscribeController中的event、event2、event3方法;
     * 实时接收的消息体示例参考通用事件格式说明
     */
    private String monitor;
    /**
     * 监听类型,固定值为：url
     */
    private String monitorType;
    /**
     * 监听对象
     */
    private List<Events> events;
}
