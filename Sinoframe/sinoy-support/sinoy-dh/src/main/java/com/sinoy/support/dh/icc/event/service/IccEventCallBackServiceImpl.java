package com.sinoy.support.dh.icc.event.service;

import com.sinoy.support.dh.icc.event.controller.dto.EventData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public  class IccEventCallBackServiceImpl implements IccEventCallBackService{
    /**
     * 默认回调
     * @param data
     */
    @Override
    public void callback( EventData data) {
        log.info(" 大华事件回调:{}, data:{}", data);
    }

}
