package com.sinoy.support.dh.icc.event.service;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.dahuatech.icc.oauth.http.DefaultClient;
import com.dahuatech.icc.oauth.http.IClient;
import com.dahuatech.icc.oauth.http.IccTokenResponse;
import com.sinoy.support.dh.icc.event.controller.dto.EventSubscription;
import com.sinoy.support.dh.icc.event.controller.dto.SubscriptRes;
import com.sinoy.support.dh.icc.event.controller.dto.TokenParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class DHEventApiImpl implements DHEventApi {
    @Value("${monitor.dh.icc.host}")
    private String url;
    @Value("${monitor.dh.icc.client_id}")
    private String client_id;
    @Value("${monitor.dh.icc.client_secret}")
    private String client_secret;
    @Value("${monitor.dh.icc.username}")
    private String username;
    @Value("${monitor.dh.icc.password}")
    private String password;
    @Value("${monitor.dh.icc.grant_type}")
    private String grant_type;
    @Override
    public SubscriptRes eventUnSubscriptionByEventTypes(String name) {
        HttpRequest request= HttpUtil.createRequest(Method.DELETE,url+"/evo-apigw/evo-event/1.0.0/subscribe/mqinfo?name="+name).contentType("application/json");
        return JSON.parseObject(request.toString(), SubscriptRes.class);
    }

    @Override
    public SubscriptRes eventSubscriptionByEventTypes(EventSubscription eventSubscription) throws Exception {
//        TokenParams tokenParams=new TokenParams();
//        tokenParams.setClient_id(client_id);
//        tokenParams.setUsername(username);
//        tokenParams.setPassword(encrypt(password,getPublicKey()));
//        tokenParams.setPublic_key(getPublicKey());
//        tokenParams.setGrant_type(grant_type);

        IClient iClient = new DefaultClient();
        IccTokenResponse.IccToken token = iClient.getAccessToken();
        String accessToken = token.getAccess_token();
        Map<String,Object> params = new HashMap(){
            {
                put("param", eventSubscription);
            }
        };

        HttpResponse response1 = HttpUtil.createPost("https://" + url+"/evo-apigw/evo-event/1.0.0/subscribe/mqinfo")
                .contentType("application/json")
                .header("Authorization","Bearer " + accessToken)
                .body(JSON.toJSONString(params)).execute();
        String msg=response1.body();
        JSONObject obj=JSONObject.parseObject(msg);
        if(Boolean.parseBoolean(obj.get("success").toString())){

        } else {
            log.info("大华事件订阅失败，错误信息：{}",obj.getString("errMsg"));
        }

        return JSON.parseObject(msg, SubscriptRes.class);
    }



    /**
     * 获取public-key
     */
    public String getPublicKey(){
        String msg = HttpUtil.get(url+"/evo-apigw/evo-oauth/1.0.0/oauth/public-key");
        JSONObject obj=JSONObject.parseObject(msg);
        if(Boolean.parseBoolean(obj.get("success").toString())){
            JSONObject dataObj=obj.getJSONObject("data");
            return dataObj.getString("publicKey");
        }
        else{
            log.info("获取public-key失败，返回值：{}",msg);
            return "";
        }
    }

    /**
     * 获取鉴权信息
     */
    public String getAuthorization(TokenParams tokenParams)  {
        String msg=HttpUtil.post(url+"/evo-apigw/evo-oauth/1.0.0/oauth/extend/token", JSONUtil.toJsonStr(tokenParams));
        JSONObject obj=JSONObject.parseObject(msg);
        if(Boolean.parseBoolean(obj.get("success").toString())){
            JSONObject dataObj=obj.getJSONObject("data");
            return dataObj.getString("token_type")+" "+dataObj.getString("access_token");
        }
        else{
            log.info("获取鉴权信息失败，返回值：{}",msg);
            return "";
        }

    }

    /**
     * 基于原生RSA公钥加密
     * @param password 用户密码
     * @param publicKey 公钥
     * @return
     * @throws Exception
     */
    public  String encrypt(String password, String publicKey) throws Exception {
        //base64编码的公钥
        byte[] decoded = java.util.Base64.getDecoder().decode(publicKey);
        RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
        //RSA加密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        String outStr;
        byte[] inputArray = password.getBytes("UTF-8");
        int inputLength = inputArray.length;
        // 最大加密字节数，超出最大字节数需要分组加密
        int MAX_ENCRYPT_BLOCK = 117;
        // 标识
        int offSet = 0;
        byte[] resultBytes = {};
        byte[] cache = {};
        while (inputLength - offSet > 0) {
            if (inputLength - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(inputArray, offSet, MAX_ENCRYPT_BLOCK);
                offSet += MAX_ENCRYPT_BLOCK;
            } else {
                cache = cipher.doFinal(inputArray, offSet, inputLength - offSet);
                offSet = inputLength;
            }
            resultBytes = Arrays.copyOf(resultBytes, resultBytes.length + cache.length);
            System.arraycopy(cache, 0, resultBytes, resultBytes.length - cache.length, cache.length);
        }
        outStr = java.util.Base64.getEncoder().encodeToString(resultBytes);
        return outStr;
    }
}
