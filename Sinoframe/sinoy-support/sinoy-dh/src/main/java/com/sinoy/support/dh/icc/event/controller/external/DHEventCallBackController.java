package com.sinoy.support.dh.icc.event.controller.external;

import com.sinoy.support.dh.icc.event.controller.dto.EventData;
import com.sinoy.support.dh.icc.event.service.IccEventCallBackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;

@RestController
@RequestMapping("/dh/event")
@Slf4j
public class DHEventCallBackController {

    @Resource
    private IccEventCallBackService iccEventCallBackService;

    /**
     * 消息回调
     * 订阅消息的时候 写入这个回调接口的地址
     */
    @PermitAll
    @PostMapping("/callback")
    private void handleCallBack(@RequestBody EventData data) {
        log.info("大华获取事件 callback: , {}", data);
        iccEventCallBackService.callback(data);
    }
}
