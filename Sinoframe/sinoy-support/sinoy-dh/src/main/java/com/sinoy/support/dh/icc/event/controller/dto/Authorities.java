package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

import java.util.List;

/**
 * 权限组
 */
@Data
public class Authorities {
    /**
     * 类型列表,没有该字段就是订阅所有，空数组代表不订阅
     * 当category是alarm时，为设备或子系统自定义的报警消息，types参考事件类型-报警类型；
     * 当category为business时，为业务事件，一般是子系统上报的业务消息，types参考事件类型-业务类型；
     * 当category为perception时，为移动设备上报的消息，types参考事件类型-感知类类型；
     * 当category为state时，设备状态变化推送消息，无需送
     */
    private List<String> types;
}
