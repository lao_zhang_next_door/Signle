package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

/**
 * 请求头鉴权码所需参数
 */
@Data
public class TokenParams {
    /**
     * 授权类型，固定值：password
     */
    private String grant_type;
    /**
     * 用户名
     */
    private String username;
    /**
     * 平台密码，需RSA加密密码传输,RSA公钥调获取公钥接口
     */
    private String password;
    /**
     * 凭证id，自定义，建议数字、字母
     */
    private String client_id;
    /**
     * 凭证密钥，访问凭证client_id与client_secret必须和在ICC平台上申请的client_id与client_secret保持一致，
     */
    private String client_secret;
    /**
     * RSA公钥
     */
    private String public_key;
}
