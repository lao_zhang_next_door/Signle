package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

@Data
public class InfoData {
    /**
     * 设备编码
     */
    private String deviceCode;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 通道序号
     */
    private Integer channelSeq;
    /**
     * 通道名称
     */
    private String channelName;
    /**
     * 单元类型
     */
    private Integer unitType;
    /**
     * 单元号
     */
    private Integer unitSeq;
    /**
     * 报警编号UUID
     */
    private String alarmCode;
    /**
     * 报警状态
     */
    private Integer alarmStat;
    /**
     * 报警类型
     */
    private Integer alarmType;
    /**
     * 报警等级
     */
    private Integer alarmGrade;
    /**
     * 报警时间
     */
    private String alarmDate;
    /**
     * 报警图片路径
     */
    private String alarmPicture;
    /**
     * 报警图片大小
     */
    private Long alarmPictureSize;
    /**
     * 备注
     */
    private String memo;
    /**
     * 节点类型
     */
    private Integer nodeType;
    /**
     * 设备编码或通道编码
     */
    private String nodeCode;
    /**
     * 节点所属组织编码
     */
    private String orgCode;
    /**
     * 节点所属组织名称
     */
    private String orgName;
    /**
     * 扩展对象
     */
    private Object extend;
}
