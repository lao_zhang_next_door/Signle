package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

@Data
public class Subsystem {
    /**
     * 系统类型,0代表子系统 固定值：0
     */
    private String subsystemType;
    /**
     * 订阅者名称,唯一,建议与magic一样，也可按照ip_port_业务模块，例如10.35.111.10_8010_alarm
     * name存在则覆盖之前的订阅内容；name不存在则新增
     */
    private String name;
    /**
     * 订阅者ip端口,子系统填ip_port， ip、port为字段monitor中的ip与端口
     */
    private String magic;
}
