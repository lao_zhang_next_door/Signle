package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

import java.util.List;

/**
 * 监听对象
 */
@Data
public class Events {
    /**
     * 事件大类,参考事件大类枚举
     */
    private String category;
    /**
     * 是否订阅所有,仅限客户端（web，client，app）使用，0代表不订阅，1代表订阅所有authorities，报警，设备状态会过滤权限，不传不处理
     */
    private Integer subscribeAll;
    /**
     * 级联订阅,不填，没有该字段，0,代表本级，1代表下级，2代表本级+下级 ,默认值0
     */
    private Integer domainSubscribe;
    /**
     * 权限组
     */
    private List<Authorities> authorities;
}
