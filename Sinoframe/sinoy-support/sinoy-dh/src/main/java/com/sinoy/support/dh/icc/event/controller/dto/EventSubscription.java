package com.sinoy.support.dh.icc.event.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class EventSubscription {
     /**
      * 监听组
      */
     private List<Monitors> monitors;
     /**
      * 订阅者信息
      */
     private Subsystem subsystem;
}
