package com.sinoy.support.intelligence.test;

import net.sourceforge.tess4j.Tesseract;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class TestImgV1 {
    /** tessdata 路径 */
    private static final String TESSDATA_PATH = "D:\\mywork\\work\\git\\myGit\\Signle\\Sinoframe\\sinoy-support\\sinoy-intelligence\\src\\main\\resources\\tesseract-main\\tessdata";

    public static void main(String[] args) throws Exception {

//        clean("D:\\新建文件夹\\fa51ce22-4ad4-4b99-ab98-735d47c5f599\\study-1.jpg","D:\\新建文件夹\\fa51ce22-4ad4-4b99-ab98-735d47c5f599\\study-2.jpg");

        BufferedImage codeImage = ImageIO.read(new File("D:\\新建文件夹\\fa51ce22-4ad4-4b99-ab98-735d47c5f599\\study-2.jpg"));
        BufferedImage codeImage1 = ImageIO.read(new File("D:\\test\\1677121947592.jpg"));
        BufferedImage codeImage2 = ImageIO.read(new File("D:\\新建文件夹\\1.png"));

//        // 本地做个备份，好对比
//        write2disk(codeImage);

        Tesseract tessreact = new Tesseract();
        tessreact.setDatapath(TESSDATA_PATH);
        tessreact.setLanguage("chi_sim");

        String result = tessreact.doOCR(codeImage2);
        System.out.println("测验结果：[" + result.replace(" ", "").trim() + "]");
    }


    /**
     * 图片预处理
     * @param fromPath
     * @param toPath
     * @throws IOException
     */
    public static void clean(String fromPath,String toPath) throws IOException{
        File file1 = new File(fromPath);
        BufferedImage image = ImageIO.read(file1);

        BufferedImage sourceImg =ImageIO.read(new FileInputStream(file1));  // 获取图片的长宽
        int width = sourceImg.getWidth();
        int height = sourceImg.getHeight();

        /**
         * 创建3维数组用于保存图片rgb数据
         */
        int[][][] array = new int[width][height][3];
        for(int i=0;i<width;i++){ // 获取图片中所有像素点的rgb
            for(int j=0;j<height;j++){
                int pixel = image.getRGB(i, j); //获得坐标(i,j)的像素
                int red = (pixel & 0xff0000) >> 16;
                int green = (pixel & 0xff00) >> 8;
                int blue = (pixel & 0xff);  //通过坐标(i,j)的像素值获得r,g,b的值
                array[i][j][0] = red;
                array[i][j][1] = green;
                array[i][j][2] = blue;
            }
        }

        /**
         * 清除表格线：
         * 竖线：绝大多数点的x值都为255
         */
        for(int i=0;i<width;i++){
            int nums = 0;
            for(int j=0;j<height;j++){
                if(array[i][j][0]<128 && array[i][j][1]<128 && array[i][j][2]<128){
                    nums += 1;
                }
            }
            if(nums > height * 0.8){
                for(int n=0;n<height;n++){
                    array[i][n][0] = 255;
                    array[i][n][1] = 255;
                    array[i][n][2] = 255;
                }
            }
        }
        /**
         * 清除表格线：
         *     横线：绝大多数点的y值都为255
         */
        for(int j=0;j<height;j++){
            int nums = 0;
            for(int i=0;i<width;i++){
                if(array[i][j][0]<128 && array[i][j][1]<128 && array[i][j][2]<128){
                    nums += 1;
                }
            }
            if(nums > height * 0.8){
                for(int n=0;n<width;n++){
                    array[n][j][0] = 255;
                    array[n][j][1] = 255;
                    array[n][j][2] = 255;
                }
            }
        }
        /**
         * 大点
         */
        for(int i=0;i<width;i++){
            for(int j=0;j<height;j++){
                int cover = new Color(array[i][j][0],array[i][j][1],array[i][j][2]).getRGB();
                image.setRGB(i,j,cover);
            }
        }
        File file2 = new File(toPath);
        ImageIO.write(image, "png", file2);
    }


}
