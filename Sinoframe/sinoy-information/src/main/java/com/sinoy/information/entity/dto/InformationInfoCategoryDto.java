package com.sinoy.information.entity.dto;

import com.sinoy.information.entity.InformationInfo;

/**
 * 栏目信息关联dto
 *
 * @author T470
 */
public class InformationInfoCategoryDto {

    /**
     * 信息栏目审核状态
     */
    private Integer auditStatus;

    /**
     * 信息Guid
     */
    private String infoGuid;

    /**
     * 栏目code
     */
    private String categoryCode;

    /**
     * 栏目Guid
     */
    private String categoryGuid;

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getInfoGuid() {
        return infoGuid;
    }

    public void setInfoGuid(String infoGuid) {
        this.infoGuid = infoGuid;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryGuid() {
        return categoryGuid;
    }

    public void setCategoryGuid(String categoryGuid) {
        this.categoryGuid = categoryGuid;
    }
}
