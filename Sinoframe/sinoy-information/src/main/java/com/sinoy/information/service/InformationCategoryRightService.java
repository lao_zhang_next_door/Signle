package com.sinoy.information.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.information.entity.InformationCategoryRight;
import com.sinoy.information.entity.dto.InformationCategoryDto;
import com.sinoy.information.entity.vo.InformationCategoryRightVo;

import java.util.List;

public interface InformationCategoryRightService extends BaseService<InformationCategoryRight> {

    /**
     * 批量设置角色栏目权限
     * @param informationCategoryRightVo
     */
    void batchSetRoleCategoryPermission(InformationCategoryRightVo informationCategoryRightVo);

    /**
     * 根据角色Guid联查栏目表
     *
     * @param allowTo
     * @return
     */
    List<InformationCategoryDto> getRoleCategoryPermission(String allowTo);
}
