package com.sinoy.information.entity;

import com.sinoy.information.basic.base.BaseEntity;

/**
 * 信息栏目关联表
 *
 * @author T470
 */
public class InformationInfoCategory extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public InformationInfoCategory(boolean init) {
        super(init);
    }

    public InformationInfoCategory(String rowGuid) {
        super(rowGuid);
    }

    public InformationInfoCategory() {
    }


    /**
     * 信息Guid
     **/
    private String infoGuid;

    /**
     * 栏目Guid
     **/
    private String categoryGuid;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    public String getCategoryGuid() {
        return categoryGuid;
    }

    public void setCategoryGuid(String categoryGuid) {
        this.categoryGuid = categoryGuid;
    }

    public String getInfoGuid() {
        return infoGuid;
    }

    public void setInfoGuid(String infoGuid) {
        this.infoGuid = infoGuid;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
}

