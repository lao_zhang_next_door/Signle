package com.sinoy.information.controller.admin;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.information.basic.enumeration.InformationStatus;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.vo.InformationInfoVo;
import com.sinoy.information.service.InformationInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/information/informationInfo")
public class InformationInfoController {

    @Autowired
    private InformationInfoService informationInfoService;


    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Page<InformationInfo> informationCategoryPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        QueryWrapper<InformationInfo> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        IPage<InformationInfo> iPage = informationInfoService.page(informationCategoryPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    /**
     * 关联信息联查
     *
     * @param search
     * @return
     */
    @PostMapping("/getInfoRelationList")
    private R getInfoRelationList(@RequestBody Search search) {
        Map<String, Object> params = new HashMap<>(2);
        params.putAll(search.getParams());
        params.put("offset", (search.getCurrentPage() - 1) * search.getPageSize());
        params.put("limit", search.getPageSize());
        String orderBy = StringUtils.join(search.getProp(), ",") + " " + StringUtils.join(search.getOrder(), ",");
        params.put("orderBy", orderBy);
        List<InformationInfoVo> informationInfoList = informationInfoService.getInfoRelationList(params, false);
        int count = informationInfoService.getInfoRelationCount(params, false);
        return R.list(count, informationInfoList);
    }

    /**
     * 查看信息详情
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<InformationInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        wrapper.eq("del_flag", DelFlag.Normal.getCode());
        return R.ok().put("data", informationInfoService.getOne(wrapper));
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<InformationInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            informationInfoService.remove(wrapper);
        }
        return R.ok();
    }


    /**
     * 信息发布
     *
     * @param informationInfo
     * @return
     */
    @PostMapping("/deliveryInfo")
    private R deliveryInfo(@RequestBody InformationInfoVo informationInfo) {
        informationInfoService.deliveryInfo(informationInfo);
        return R.ok();
    }

    /**
     * 信息发布修改
     *
     * @param informationInfo
     * @return
     */
    @PutMapping("/updateInfo")
    private R updateInfo(@RequestBody InformationInfoVo informationInfo) {
        informationInfoService.updateInfo(informationInfo);
        return R.ok();
    }

    /**
     * 信息停止发布
     *
     * @param rowGuids
     * @return
     */
    @PostMapping("/batchStopRelease")
    private R batchStopRelease(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<InformationInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            InformationInfo informationInfo = new InformationInfo();
            informationInfo.setInfoStatus(InformationStatus.STOP.getCode());
            informationInfoService.update(informationInfo, wrapper);
        }
        return R.ok();
    }

    /**
     * 信息批量发布
     *
     * @param rowGuids
     * @return
     */
    @PostMapping("/batchStartRelease")
    private R batchStartRelease(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<InformationInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            InformationInfo informationInfo = new InformationInfo();
            informationInfo.setInfoStatus(InformationStatus.YFB.getCode());
            informationInfoService.update(informationInfo, wrapper);
        }
        return R.ok();
    }

    /**
     * 更新信息点击次数
     *
     * @param rowGuid
     * @return
     */
    @PostMapping("/updateClickTime")
    private R updateClickTime(@RequestParam String rowGuid) {
        informationInfoService.updateClickTimes(rowGuid);
        return R.ok();
    }

    /**
     * 第三方平台新增推送信息
     *
     * @param informationInfo
     * @return
     */
    @PostMapping("/pushNewInformation")
    private R pushNewInformation(@RequestBody InformationInfoVo informationInfo) {
        String rowGuid = informationInfoService.deliveryInfo(informationInfo);
        JSONObject object = new JSONObject();
        object.put("rowGuid", rowGuid);
        return R.ok().put("data", object);
    }

    /**
     * 第三方平台修改推送信息
     *
     * @param informationInfo
     * @return
     */
    @PostMapping("/updatePushInformation")
    private R updatePushInformation(@RequestBody InformationInfoVo informationInfo) {
        informationInfoService.updateInfo(informationInfo);
        return R.ok();
    }

    /**
     * 第三方平台删除信息
     *
     * @param rowGuid
     * @return
     */
    @PostMapping("/deletePushInformation")
    private R deletePushInformation(@RequestParam String rowGuid) {
        UpdateWrapper<InformationInfo> wrapper = new UpdateWrapper<>();
        wrapper.set("del_flag", DelFlag.Del.getCode());
        wrapper.eq("row_guid", rowGuid);
        informationInfoService.update(wrapper);
        return R.ok();
    }

    /**
     * 我发布的关联信息联查
     *
     * @param search
     * @return
     */
    @PostMapping("/getMyInfoList")
    private R getMyInfoList(@RequestBody Search search) {
        Map<String, Object> params = new HashMap<>(2);
        params.putAll(search.getParams());
        params.put("offset", (search.getCurrentPage() - 1) * search.getPageSize());
        params.put("limit", search.getPageSize());
        String orderBy = StringUtils.join(search.getProp(), ",") + " " + StringUtils.join(search.getOrder(), ",");
        params.put("orderBy", orderBy);
        List<InformationInfoVo> informationInfoList = informationInfoService.getInfoRelationList(params, true);
        int count = informationInfoService.getInfoRelationCount(params, true);
        return R.list(count, informationInfoList);
    }
}