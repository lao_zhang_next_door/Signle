package com.sinoy.information.entity.dto;

import com.sinoy.information.entity.InformationInfo;

/**
 * @author: Ptar xin
 * @since: 2023/1/29 16:04
 */
public class InformationInfoDto extends InformationInfo {
    private int readStatus;

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }
}
