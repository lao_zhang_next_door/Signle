package com.sinoy.information.controller.app;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.information.basic.enumeration.InformationStatus;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.dto.InformationInfoDto;
import com.sinoy.information.service.InformationInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/information/informationInfo")
public class AppInformationInfoController {

    @Autowired
    private InformationInfoService informationInfoService;

    @Resource
    private CommonPropAndMethods commonPropAndMethods;

    /**
     * 关联信息联查
     *
     * @param search
     * @return
     */
    @PostMapping("/getNoticeInfoList")
    private R getNoticeInfoList(@RequestBody Search search) {
        Map<String, Object> params = new HashMap<>(2);
        params.putAll(search.getParams());
        params.put("offset", (search.getCurrentPage() - 1) * search.getPageSize());
        params.put("limit", search.getPageSize());
        String orderBy = StringUtils.join(search.getProp(), ",") + " " + StringUtils.join(search.getOrder(), ",");
        params.put("orderBy", orderBy);
        XnwUser user = commonPropAndMethods.getXnwUser();
        if (user != null) {
            params.put("userGuid", user.getRowGuid());
        }
        List<InformationInfoDto> informationInfoList = informationInfoService.getNoticeInfoList(params);
        int count = informationInfoService.getInfoRelationCount(params, false);
        return R.list(count, informationInfoList);
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<InformationInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        return R.ok().put("data", informationInfoService.getOne(wrapper));
    }

    /**
     * 未读消息
     *
     * @return
     */
    @PostMapping("/getNoticeInfoListUnReadCount")
    public R getNoticeInfoListUnReadCount(@RequestParam Map<String, Object> params) {
        XnwUser user = commonPropAndMethods.getXnwUser();
        if (user != null) {
            params.put("userGuid", user.getRowGuid());
        }
        int infoRelationCount = informationInfoService.getNoticeInfoListUnReadCount(params);

        InformationInfoDto lastNotice = informationInfoService.getLastNotice(params);

        return R.ok().put("count", infoRelationCount).put("data", lastNotice);
    }

}