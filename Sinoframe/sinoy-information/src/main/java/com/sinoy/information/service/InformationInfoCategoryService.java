package com.sinoy.information.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.information.entity.InformationInfoCategory;
import com.sinoy.information.entity.dto.InformationInfoCategoryDto;

import java.util.List;

/**
 * 信息栏目关联
 *
 * @author T470
 */
public interface InformationInfoCategoryService extends BaseService<InformationInfoCategory> {

    /**
     * 批量新增栏目信息关联
     *
     * @param infoGuid
     * @param categoryGuids
     * @return infoStatus
     */
    int batchInsertRelation(String infoGuid, List<String> categoryGuids);

    /**
     * 修改信息栏目审核状态
     *
     * @param informationInfoCategoryDto
     */
    void updateInfoCategoryStatus(InformationInfoCategoryDto informationInfoCategoryDto);

}
