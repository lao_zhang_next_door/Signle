package com.sinoy.information.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.IsOrNot;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.information.dao.InformationCategoryDao;
import com.sinoy.information.entity.InformationCategory;
import com.sinoy.information.entity.vo.InformationCategoryVo;
import com.sinoy.information.service.InformationCategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InformationCategoryServiceImpl extends BaseServiceImpl<InformationCategoryDao, InformationCategory> implements InformationCategoryService {

    @Resource
    private InformationCategoryDao informationCategoryDao;

    @Resource
    private CommonPropAndMethods commonPropAndMethods;

    @Override
    public void insertNewCategory(InformationCategory informationCategory) {
        informationCategory.initNull();
        if (informationCategory.getPcategoryCode() == null || "".equals(informationCategory.getPcategoryCode())) {
            //顶级目录
            informationCategory.setIsTop(IsOrNot.IS.getCode());
            informationCategory.setPcategoryCode("");
            informationCategory.setParentGuid("");
        } else {
            informationCategory.setIsTop(IsOrNot.NOT.getCode());
        }

        String categoryCode = this.getNextCateGoryCode(informationCategory.getPcategoryCode());
        if (StringUtil.isEmpty(categoryCode)) {
            categoryCode = informationCategory.getPcategoryCode() + "001";
        } else {
            Integer nextCateGoryCode = Integer.parseInt(categoryCode) + 1;
            categoryCode = String.format("%0" + categoryCode.length() + "d", nextCateGoryCode);
            //查询上级栏目Guid
            QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("category_code", informationCategory.getPcategoryCode());
            queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
            InformationCategory fatherCategory = informationCategoryDao.selectOne(queryWrapper);
            if (fatherCategory != null) {
                informationCategory.setParentGuid(fatherCategory.getRowGuid());
            }

        }

        informationCategory.setCategoryCode(categoryCode);
        informationCategoryDao.insert(informationCategory);
    }

    @Override
    public String getNextCateGoryCode(String pcategoryCode) {
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        if ("".equals(pcategoryCode) || pcategoryCode == null) {
            queryWrapper.eq("is_top", IsOrNot.IS.getCode());
        }
        queryWrapper.eq("pcategory_code", pcategoryCode);
        queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
        queryWrapper.orderBy(true, false, "category_code");
        List<InformationCategory> categoryList = informationCategoryDao.selectList(queryWrapper);
        if (categoryList.isEmpty()) {
            return null;
        }
        return categoryList.get(0).getCategoryCode();
    }

    @Override
    public List<InformationCategory> getChildCategory(String pcategoryCode) {
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pcategory_code", pcategoryCode);
        queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
        queryWrapper.groupBy("row_id");
        queryWrapper.orderBy(true, true, "create_time");
        return informationCategoryDao.selectList(queryWrapper);
    }

    @Override
    public JSONArray findCategorys(InformationCategoryVo informationCategoryVo) {
        QueryWrapper<InformationCategory> topQueryWrapper = new QueryWrapper<>();
        topQueryWrapper.eq("is_top", IsOrNot.IS.getCode());
        topQueryWrapper.eq("del_flag", DelFlag.Normal.getCode());
        if (informationCategoryVo.getSort() != null) {
            BatisPlusUtil.setOrders(topQueryWrapper, informationCategoryVo.getSort().getProp(), informationCategoryVo.getSort().getOrder());
        }
        List<InformationCategory> categoryList = informationCategoryDao.selectList(topQueryWrapper);
        return this.getChildCategories(categoryList);
    }

    @Override
    public JSONArray findCategoryTreesByRoles(InformationCategoryVo informationCategoryVo) {
        XnwUser user = commonPropAndMethods.getXnwUser();
        Optional.ofNullable(user).orElseThrow(() -> new BaseException("用户不存在"));
        Map<String, Object> params = new HashMap<>(1);
        if (!user.getRoleGuidList().isEmpty()) {
            String roleGuids = StringUtils.join(user.getRoleGuidList(), ",");
            params.put("roleGuids", roleGuids);
            params.put("isTop", String.valueOf(IsOrNot.IS.getCode()));
            if (informationCategoryVo.getSort() != null && !informationCategoryVo.getSort().getProp().isEmpty()) {
                String orderBy = StringUtils.join(informationCategoryVo.getSort().getProp(), ",") + " " + StringUtils.join(informationCategoryVo.getSort().getOrder(), ",");
                params.put("orderBy", orderBy);
            }
        } else {
            return null;
        }
        JSONArray jsonArray;
        //用户所有顶级栏目模块
        List<InformationCategory> allTopCategory = informationCategoryDao.findCategoryTreesByRoles(params);
        //先找顶级栏目
        params.remove("isTop");
        jsonArray = getChildCategoriesByRole(allTopCategory, params);
        return jsonArray;
    }

    @Override
    public void deleteByPcategoryCode(String pcategoryCode) {
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pcategory_code", pcategoryCode);
        InformationCategory informationCategory = new InformationCategory();
        informationCategory.setDelFlag(DelFlag.Del.getCode());
        informationCategoryDao.update(informationCategory, queryWrapper);
    }

    private JSONArray getChildCategories(List<InformationCategory> topTrees) {
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        JSONArray array = new JSONArray();
        for (InformationCategory topTree : topTrees) {
            queryWrapper.clear();
            JSONObject json = new JSONObject();
            json.put("label", topTree.getCategoryName());
            json.put("pCategoryCode", topTree.getPcategoryCode());
            json.put("parentGuid", topTree.getParentGuid());
            json.put("value", topTree.getCategoryCode());
            json.put("rowGuid", topTree.getRowGuid());
            queryWrapper.eq("pcategory_code", topTree.getCategoryCode());
            queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
            List<InformationCategory> childCategory = informationCategoryDao.selectList(queryWrapper);
            JSONArray childrenArray = this.getChildCategories(childCategory);
            if (!childrenArray.isEmpty()) {
                json.put("leaf", false);
                json.put("children", childrenArray);
            } else {
                json.put("leaf", true);
            }
            array.add(json);
        }

        return array;
    }

    /**
     * 查询用户权限下的栏目子类
     *
     * @param topTrees
     * @param params
     * @return
     */
    private JSONArray getChildCategoriesByRole(List<InformationCategory> topTrees, Map<String, Object> params) {
        JSONArray array = new JSONArray();
        params.put("isTop", String.valueOf(IsOrNot.NOT.getCode()));
        for (InformationCategory topTree : topTrees) {
            params.put("pcategoryCode", topTree.getCategoryCode());
            JSONObject json = new JSONObject();
            json.put("label", topTree.getCategoryName());
            json.put("pCategoryCode", topTree.getPcategoryCode());
            json.put("parentGuid", topTree.getParentGuid());
            json.put("rowGuid", topTree.getRowGuid());
            json.put("value", topTree.getCategoryCode());
            List<InformationCategory> childCategory = informationCategoryDao.findCategoryTreesByRoles(params);
            JSONArray childrenArray = this.getChildCategories(childCategory);
            if (!childrenArray.isEmpty()) {
                json.put("leaf", false);
                json.put("children", childrenArray);
            } else {
                json.put("leaf", true);
            }
            array.add(json);
        }
        return array;
    }
}

