package com.sinoy.information.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.information.basic.base.BaseEntity;
import com.sinoy.information.entity.InformationInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;


/**
 * @author T470
 */
public class InformationInfoVo extends InformationInfo {

    /**
     * @param init true则有默认值
     */
    public InformationInfoVo(boolean init) {
        super(init);
    }

    public InformationInfoVo(String rowGuid) {
        super(rowGuid);
    }

    public InformationInfoVo() {
    }

    /**
     * 栏目Guids
     */
    @Transient
    @TableField(exist = false)
    private List<String> categoryGuids;

    /**
     * 栏目Guid
     */
    @Transient
    @TableField(exist = false)
    private String categoryGuid;

    /**
     * 栏目名称
     */
    @Transient
    @TableField(exist = false)
    private String categoryName;

    /**
     * 栏目Code
     */
    @Transient
    @TableField(exist = false)
    private String categoryCode;

    /**
     * 栏目关联审核状态
     */
    @Transient
    @TableField(exist = false)
    private Integer auditStatus;


    public List<String> getCategoryGuids() {
        return categoryGuids;
    }

    public void setCategoryGuids(List<String> categoryGuids) {
        this.categoryGuids = categoryGuids;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getCategoryGuid() {
        return categoryGuid;
    }

    public void setCategoryGuid(String categoryGuid) {
        this.categoryGuid = categoryGuid;
    }
}

