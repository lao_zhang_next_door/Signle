package com.sinoy.information.controller.admin;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.information.entity.dto.InformationCategoryDto;
import com.sinoy.information.entity.vo.InformationCategoryRightVo;
import com.sinoy.information.service.InformationCategoryRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/information/informationCategoryRight")
public class InformationCategoryRightController {

    @Autowired
    private InformationCategoryRightService informationCategoryRightService;


    /**
     * 调整用户角色栏目权限
     *
     * @param informationCategoryRightVo
     * @return
     */
    @PostMapping("/addCategoryRight")
    private R addCategoryRight(@RequestBody InformationCategoryRightVo informationCategoryRightVo) {
        informationCategoryRightService.batchSetRoleCategoryPermission(informationCategoryRightVo);
        return R.ok();
    }

    /**
     * 根据角色Guid联查栏目表
     *
     * @param allowTo
     * @return
     */
    @GetMapping("/getRoleCategoryPermission")
    private R getRoleCategoryPermission(@RequestParam String allowTo) {
        List<InformationCategoryDto> informationCategoryDtos = informationCategoryRightService.getRoleCategoryPermission(allowTo);
        return R.ok().put("data", informationCategoryDtos);
    }


}