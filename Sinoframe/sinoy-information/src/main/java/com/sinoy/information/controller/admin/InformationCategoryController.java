package com.sinoy.information.controller.admin;

import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.information.entity.InformationCategory;
import com.sinoy.information.entity.vo.InformationCategoryVo;
import com.sinoy.information.service.InformationCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/information/informationCategory")
public class InformationCategoryController {

    @Autowired
    private InformationCategoryService informationCategoryService;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Page<InformationCategory> informationCategoryPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        IPage<InformationCategory> iPage = informationCategoryService.page(informationCategoryPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @PassToken
    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<InformationCategory> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        return R.ok().put("data", informationCategoryService.getOne(wrapper));
    }

    @PostMapping("/add")
    private R add(@RequestBody InformationCategory informationCategory) {
        informationCategoryService.insertNewCategory(informationCategory);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody InformationCategory informationCategory) {
        informationCategory.setUpdateTime(LocalDateTime.now());
        UpdateWrapper<InformationCategory> wrapper = new UpdateWrapper<>();
        //查询上级栏目Guid
        wrapper.eq("category_code", informationCategory.getPcategoryCode());
        wrapper.eq("del_flag", DelFlag.Normal.getCode());
        InformationCategory fatherCategory = informationCategoryService.getOne(wrapper);
        informationCategory.setParentGuid(fatherCategory.getRowGuid());
        wrapper.clear();
        wrapper.eq("row_guid", informationCategory.getRowGuid());
        informationCategoryService.update(informationCategory, wrapper);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<InformationCategory> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            InformationCategory category = informationCategoryService.selectByGuid(rowGuid);
            //删除子类栏目
            informationCategoryService.deleteByPcategoryCode(category.getCategoryCode());
            informationCategoryService.remove(wrapper);
        }
        return R.ok();
    }

    /**
     * 获取所有栏目信息
     *
     * @param search
     * @return
     */
    @PostMapping("/getCategoryList")
    private R getCategoryList(@RequestBody Search search) {
        QueryWrapper<InformationCategory> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        List<InformationCategory> informationCategoryList = informationCategoryService.list(queryWrapper);
        return R.ok().put("data", informationCategoryList);
    }

    /**
     * 根据用户角色查询栏目复选框模块树
     *
     * @return
     */
    @PostMapping("/getCategoryTreeByRole")
    public R getCategoryTreeByRole(@RequestBody InformationCategoryVo informationCategoryVo) {
        JSONArray trees = informationCategoryService.findCategoryTreesByRoles(informationCategoryVo);
        return R.ok().put("data", trees);
    }

    /**
     * 查询所有栏目树
     *
     * @return
     */
    @PostMapping("/getAllCategoryTree")
    public R getAllCategoryTree(@RequestBody InformationCategoryVo informationCategoryVo) {
        JSONArray trees = informationCategoryService.findCategorys(informationCategoryVo);
        return R.ok().put("data", trees);
    }
}