package com.sinoy.information.basic.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.sinoy.information.dao")
@Configuration
public class InformationMapperConfig {
}
