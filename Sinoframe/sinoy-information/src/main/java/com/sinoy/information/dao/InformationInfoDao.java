package com.sinoy.information.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.dto.InformationInfoDto;
import com.sinoy.information.entity.vo.InformationInfoVo;

import java.util.List;
import java.util.Map;

public interface InformationInfoDao extends ExtendMapper<InformationInfo> {

    /**
     * 联查信息栏目列表
     *
     * @param params
     * @return
     */
    List<InformationInfoVo> getInfoRelationList(Map<String, Object> params);

    /**
     * 联查信息栏目数量
     *
     * @param params
     * @return
     */
    int getInfoRelationCount(Map<String, Object> params);

    /**
     * 更新信息点击次数
     * @param rowGuid
     */
    void updateClickTimes(String rowGuid);

    /**
     * 联查信息栏目列表及阅读状态
     *
     * @param params
     * @return
     */
    List<InformationInfoDto> getNoticeInfoList(Map<String, Object> params);

    /**
     * 联查信息栏目列表未阅读状态数量
     *
     * @param params
     * @return
     */
    int getNoticeInfoListUnReadCount(Map<String, Object> params);

    /**
     * 最新一条数据
     * @return
     */
    InformationInfoDto getLastNotice(Map<String, Object> params);
}

