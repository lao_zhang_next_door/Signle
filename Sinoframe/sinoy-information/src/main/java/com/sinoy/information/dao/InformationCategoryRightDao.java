package com.sinoy.information.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.information.entity.InformationCategoryRight;
import com.sinoy.information.entity.dto.InformationCategoryDto;

import java.util.List;

public interface InformationCategoryRightDao extends ExtendMapper<InformationCategoryRight> {


    /**
     * 根据角色Guid联查栏目表
     *
     * @param allowTo
     * @return
     */
    List<InformationCategoryDto> getRoleCategoryPermission(String allowTo);
}

