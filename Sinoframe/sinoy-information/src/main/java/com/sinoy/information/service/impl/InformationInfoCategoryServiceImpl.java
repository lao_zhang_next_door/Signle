package com.sinoy.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.IsOrNot;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.information.basic.enumeration.InformationStatus;
import com.sinoy.information.dao.InformationCategoryDao;
import com.sinoy.information.dao.InformationCategoryRightDao;
import com.sinoy.information.dao.InformationInfoCategoryDao;
import com.sinoy.information.entity.InformationCategory;
import com.sinoy.information.entity.InformationCategoryRight;
import com.sinoy.information.entity.InformationInfoCategory;
import com.sinoy.information.entity.dto.InformationInfoCategoryDto;
import com.sinoy.information.service.InformationCategoryRightService;
import com.sinoy.information.service.InformationCategoryService;
import com.sinoy.information.service.InformationInfoCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class InformationInfoCategoryServiceImpl extends BaseServiceImpl<InformationInfoCategoryDao, InformationInfoCategory> implements InformationInfoCategoryService {

    @Resource
    private InformationInfoCategoryDao informationInfoCategoryDao;

    @Resource
    private InformationCategoryService informationCategoryService;

    @Override
    public int batchInsertRelation(String infoGuid, List<String> categoryGuids) {
        List<Integer> infoStatus = new ArrayList<>();
        //先删除之前关联的
        QueryWrapper<InformationInfoCategory> queryWrapper = new QueryWrapper<>();
        QueryWrapper<InformationCategory> categoryQueryWrapper = new QueryWrapper<>();
        queryWrapper.eq("info_guid", infoGuid);
        informationInfoCategoryDao.delete(queryWrapper);
        for (String categoryGuid : categoryGuids) {
            categoryQueryWrapper.clear();
            categoryQueryWrapper.eq("row_guid", categoryGuid);
            categoryQueryWrapper.eq("del_flag", DelFlag.Normal.getCode());
            InformationCategory informationCategory = informationCategoryService.getOne(categoryQueryWrapper);
            InformationInfoCategory informationInfoCategory = new InformationInfoCategory();
            //需要审核
            if (informationCategory.getIsNeedAudit() != null && informationCategory.getIsNeedAudit() == IsOrNot.IS.getCode()) {
                informationInfoCategory.setAuditStatus(InformationStatus.DSH.getCode());
                infoStatus.add(InformationStatus.DSH.getCode());
            } else {
                //不需要审核
                informationInfoCategory.setAuditStatus(InformationStatus.YFB.getCode());
                infoStatus.add(InformationStatus.YFB.getCode());
            }
            informationInfoCategory.initNull();
            informationInfoCategory.setCategoryGuid(categoryGuid);
            informationInfoCategory.setInfoGuid(infoGuid);
            informationInfoCategoryDao.insert(informationInfoCategory);
        }
        if (infoStatus.contains(InformationStatus.YFB.getCode())) {
            //若选择的栏目中有需要审核的，但包含不需要审核的，信息状态为发布
            return InformationStatus.YFB.getCode();
        } else {
            //若都为待审核，则为审核
            return InformationStatus.DSH.getCode();
        }

    }

    @Override
    public void updateInfoCategoryStatus(InformationInfoCategoryDto informationInfoCategoryDto) {
        QueryWrapper<InformationInfoCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("info_guid", informationInfoCategoryDto.getInfoGuid());
        queryWrapper.eq("category_guid", informationInfoCategoryDto.getCategoryGuid());
        InformationInfoCategory informationInfoCategory = informationInfoCategoryDao.selectOne(queryWrapper);
        informationInfoCategory.setAuditStatus(informationInfoCategoryDto.getAuditStatus());
        informationInfoCategory.setUpdateTime(LocalDateTime.now());
        informationInfoCategoryDao.updateById(informationInfoCategory);
    }
}

