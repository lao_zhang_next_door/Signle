package com.sinoy.information.entity.vo;

import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.information.entity.InformationCategory;
import com.sinoy.platform.system.component.entity.FrameDept;

/**
 * 栏目查询排序
 *
 * @author T470
 */
public class InformationCategoryVo {

    private InformationCategory informationCategory;

    private Sort sort;

    public InformationCategory getInformationCategory() {
        return informationCategory;
    }

    public void setInformationCategory(InformationCategory informationCategory) {
        this.informationCategory = informationCategory;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
