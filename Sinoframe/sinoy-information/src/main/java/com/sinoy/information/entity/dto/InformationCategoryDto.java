package com.sinoy.information.entity.dto;


import com.sinoy.information.entity.InformationCategory;

public class InformationCategoryDto extends InformationCategory {

    /**
     * 名称
     */
    private String label;

    /**
     * 值
     */
    private String value;

    /**
     * 父栏目code
     */
    private String pCategoryCode;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getpCategoryCode() {
        return pCategoryCode;
    }

    public void setpCategoryCode(String pCategoryCode) {
        this.pCategoryCode = pCategoryCode;
    }
}
