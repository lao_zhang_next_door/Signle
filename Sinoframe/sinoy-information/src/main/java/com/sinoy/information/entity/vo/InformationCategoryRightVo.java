package com.sinoy.information.entity.vo;

import java.util.List;

/**
 * 栏目权限Vo
 *
 * @author T470
 */
public class InformationCategoryRightVo {

    /**
     * 栏目Guids
     */
    private List<String> categoryGuids;

    /**
     * 角色类型
     */
    private String allowType;

    /**
     * 角色Guid
     */
    private String allowTo;

    public List<String> getCategoryGuids() {
        return categoryGuids;
    }

    public void setCategoryGuids(List<String> categoryGuids) {
        this.categoryGuids = categoryGuids;
    }

    public String getAllowType() {
        return allowType;
    }

    public void setAllowType(String allowType) {
        this.allowType = allowType;
    }

    public String getAllowTo() {
        return allowTo;
    }

    public void setAllowTo(String allowTo) {
        this.allowTo = allowTo;
    }
}
