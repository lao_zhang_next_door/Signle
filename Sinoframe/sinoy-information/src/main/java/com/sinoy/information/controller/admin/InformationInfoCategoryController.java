package com.sinoy.information.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.dto.InformationInfoCategoryDto;
import com.sinoy.information.service.InformationInfoCategoryService;
import com.sinoy.information.service.InformationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/information/informationInfoCategory")
public class InformationInfoCategoryController {

    @Autowired
    private InformationInfoCategoryService informationInfoCategoryService;

    @Autowired
    private InformationInfoService informationInfoService;


    /**
     * 修改信息栏目审核状态
     *
     * @param informationInfoCategoryDto
     * @return
     */
    @PostMapping("/updateInfoCategoryStatus")
    private R updateInfoCategoryStatus(@RequestBody InformationInfoCategoryDto informationInfoCategoryDto) {
        informationInfoCategoryService.updateInfoCategoryStatus(informationInfoCategoryDto);
        //改变信息状态
        QueryWrapper<InformationInfo> informationInfoQueryWrapper = new QueryWrapper<>();
        informationInfoQueryWrapper.eq("row_guid",informationInfoCategoryDto.getInfoGuid());
        InformationInfo informationInfo = informationInfoService.getOne(informationInfoQueryWrapper);
        informationInfo.setInfoStatus(informationInfoCategoryDto.getAuditStatus());
        informationInfoService.updateById(informationInfo);
        return R.ok();
    }


}