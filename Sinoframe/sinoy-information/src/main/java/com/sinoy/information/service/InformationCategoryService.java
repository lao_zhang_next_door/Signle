package com.sinoy.information.service;


import com.alibaba.fastjson2.JSONArray;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.information.entity.InformationCategory;
import com.sinoy.information.entity.vo.InformationCategoryVo;

import java.util.List;
import java.util.Map;

public interface InformationCategoryService extends BaseService<InformationCategory> {


    /**
     * 获取父栏目下的新子栏目编号
     *
     * @param pcategoryCode
     * @return
     */
    String getNextCateGoryCode(String pcategoryCode);

    /**
     * 根据父栏目Code获取子栏目信息
     *
     * @param pcategoryCode
     * @return
     */
    List<InformationCategory> getChildCategory(String pcategoryCode);

    /**
     * 获取复选框栏目树
     *
     * @return
     */
    JSONArray findCategorys(InformationCategoryVo informationCategoryVo);

    /**
     * 根据操作类型、用户角色查询可使用的信息模块
     *
     * @param informationCategoryVo
     * @return
     */
    JSONArray findCategoryTreesByRoles(InformationCategoryVo informationCategoryVo);

    /**
     * 根据父栏目Code删除子栏目
     *
     * @param pcategoryCode
     */
    void deleteByPcategoryCode(String pcategoryCode);

    /**
     * 新增栏目
     * @param informationCategory
     */
    void insertNewCategory(InformationCategory informationCategory);
}
