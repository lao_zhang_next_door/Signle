package com.sinoy.information.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.dto.InformationInfoDto;
import com.sinoy.information.entity.vo.InformationInfoVo;

import java.util.List;
import java.util.Map;

/**
 * @author T470
 */
public interface InformationInfoService extends BaseService<InformationInfo> {

    /**
     * 信息发布
     *
     * @param informationInfo
     */
    String deliveryInfo(InformationInfoVo informationInfo);

    /**
     * 信息发布修改
     *
     * @param informationInfo
     */
    void updateInfo(InformationInfoVo informationInfo);

    /**
     * 联查信息栏目列表
     *
     * @param params 查询参数
     * @param isSelf 是否查询自己发布的信息
     * @return
     */
    List<InformationInfoVo> getInfoRelationList(Map<String, Object> params, boolean isSelf);

    /**
     * 联查信息栏目数量
     *
     * @param params 查询参数
     * @param isSelf 是否查询自己发布的信息
     * @return
     */
    int getInfoRelationCount(Map<String, Object> params, boolean isSelf);

    /**
     * 联查信息栏目列表及阅读状态
     *
     * @param params
     * @return
     */
    List<InformationInfoDto> getNoticeInfoList(Map<String, Object> params);

    /**
     * 联查信息栏目列表及阅读状态
     *
     * @param params
     * @return
     */
    int getNoticeInfoListUnReadCount(Map<String, Object> params);

    /**
     * 最新一条数据
     *
     * @return
     */
    InformationInfoDto getLastNotice(Map<String, Object> params);

    /**
     * 更新信息点击次数
     *
     * @param rowGuid
     */
    void updateClickTimes(String rowGuid);
}
