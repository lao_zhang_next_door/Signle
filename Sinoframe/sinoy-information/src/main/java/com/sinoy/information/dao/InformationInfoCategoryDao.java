package com.sinoy.information.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.information.entity.InformationCategoryRight;
import com.sinoy.information.entity.InformationInfoCategory;

public interface InformationInfoCategoryDao extends ExtendMapper<InformationInfoCategory> {


}

