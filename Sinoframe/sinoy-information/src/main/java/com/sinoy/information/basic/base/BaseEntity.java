package com.sinoy.information.basic.base;


public class BaseEntity extends com.sinoy.core.database.mybatisplus.entity.BaseEntity {

	public BaseEntity(){}

	public BaseEntity(boolean init) {
		super(init);
	}

	public BaseEntity(String rowGuid) {
		super(rowGuid);
	}

}
