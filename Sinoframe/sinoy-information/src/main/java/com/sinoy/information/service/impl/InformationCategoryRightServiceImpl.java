package com.sinoy.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.information.dao.InformationCategoryRightDao;
import com.sinoy.information.entity.InformationCategoryRight;
import com.sinoy.information.entity.dto.InformationCategoryDto;
import com.sinoy.information.entity.vo.InformationCategoryRightVo;
import com.sinoy.information.service.InformationCategoryRightService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author T470
 */
@Service
public class InformationCategoryRightServiceImpl extends BaseServiceImpl<InformationCategoryRightDao, InformationCategoryRight> implements InformationCategoryRightService {

    @Resource
    private InformationCategoryRightDao informationCategoryRightDao;


    @Override
    public void batchSetRoleCategoryPermission(InformationCategoryRightVo informationCategoryRightVo) {
        QueryWrapper<InformationCategoryRight> wrapper = new QueryWrapper<>();
        wrapper.eq("allow_to", informationCategoryRightVo.getAllowTo());
        informationCategoryRightDao.delete(wrapper);
        //删除用户角色之前的栏目权限
        List<String> categoryGuids = informationCategoryRightVo.getCategoryGuids();
        for (String categoryGuid : categoryGuids) {
            InformationCategoryRight informationCategoryRight = new InformationCategoryRight();
            informationCategoryRight.initNull();
            informationCategoryRight.setCategoryGuid(categoryGuid);
            informationCategoryRight.setAllowTo(informationCategoryRightVo.getAllowTo());
            informationCategoryRight.setAllowType(informationCategoryRight.getAllowType());
            informationCategoryRightDao.insert(informationCategoryRight);
        }
    }

    @Override
    public List<InformationCategoryDto> getRoleCategoryPermission(String allowTo) {
        return informationCategoryRightDao.getRoleCategoryPermission(allowTo);
    }
}

