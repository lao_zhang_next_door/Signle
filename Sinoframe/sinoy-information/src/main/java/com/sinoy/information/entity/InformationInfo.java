package com.sinoy.information.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.information.basic.base.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户实体
 * 实体类的字段数量 >= 数据库表中需要操作的字段数量。
 * 默认情况下，实体类中的所有字段都会作为表中的字段来操作，如果有额外的字段，必须加上@Transient注解。
 * <p>
 * 1.表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info。
 * 2.表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
 * 3.字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
 * 4.可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
 * 5.使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
 * 6.建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
 * 7.如果是MySQL的自增字段，加上@GeneratedValue(generator = "JDBC")即可
 * <p>
 * Example方法
 * 方法：List<T> selectByExample(Object example);
 * 说明：根据Example条件进行查询
 * 重点：这个查询支持通过Example类指定查询列，通过selectProperties方法指定查询列
 * <p>
 * 方法：int selectCountByExample(Object example);
 * 说明：根据Example条件进行查询总数
 * <p>
 * 方法：int updateByExample(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的全部属性，null值会被更新
 * <p>
 * 方法：int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的不是null的属性值
 * <p>
 * 方法：int deleteByExample(Object example);
 * 说明：根据Example条件删除数据
 * <p>
 * 例子： // 创建Example
 * Example example = new Example(TestTableVO.class);
 * // 创建Criteria
 * Example.Criteria criteria = example.createCriteria();
 * // 添加条件
 * criteria.andEqualTo("isDelete", 0);
 * criteria.andLike("name", "%abc123%");
 * List<TestTableVO> list = testTableDao.selectByExample(example);
 */

/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 */
public class InformationInfo extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public InformationInfo(boolean init) {
        super(init);
    }

    public InformationInfo(String rowGuid) {
        super(rowGuid);
    }

    public InformationInfo() {
    }


    /**
     * 信息标题
     */
    private String infoTitle;


    /**
     * 信息内容
     */
    private String infoContent;


    /**
     * 发布时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime releaseTime;


    /**
     * 创建人
     */
    private String infoCreateUser;


    /**
     * 创建人guid
     */
    private String infoCreateUserGuid;


    /**
     * 信息附件
     */
    private String infoFile;


    /**
     * 点击(查看)次数
     */
    private Integer clickTimes;


    /**
     * 信息发布图片附件
     */
    private String infoPic;


    /**
     * 信息栏目guid
     */
    private String infoCategoryGuid;


    /**
     * 信息类型
     */
    private String infoType;


    /**
     * 链接地址
     */
    private String infoUrl;


    /**
     * 信息状态
     */
    private Integer infoStatus;

    /**
     * 是否显示信息
     */
    private String isShowInformation;

    /**
     * 来源
     */
    private String source;
    /**
     * 作者
     */
    private String author;

    /**
     * 是否置顶
     */
    private Integer isTop;


    /**
     * 设置：信息标题
     */
    public void setInfoTitle(String infoTitle) {
        this.infoTitle = infoTitle;
    }

    /**
     * 获取：信息标题
     */
    public String getInfoTitle() {
        return infoTitle;
    }

    /**
     * 设置：信息内容
     */
    public void setInfoContent(String infoContent) {
        this.infoContent = infoContent;
    }

    /**
     * 获取：信息内容
     */
    public String getInfoContent() {
        return infoContent;
    }

    /**
     * 设置：发布时间
     */
    public void setReleaseTime(LocalDateTime releaseTime) {
        this.releaseTime = releaseTime;
    }

    /**
     * 获取：发布时间
     */
    public LocalDateTime getReleaseTime() {
        return releaseTime;
    }

    /**
     * 设置：创建人
     */
    public void setInfoCreateUser(String infoCreateUser) {
        this.infoCreateUser = infoCreateUser;
    }

    /**
     * 获取：创建人
     */
    public String getInfoCreateUser() {
        return infoCreateUser;
    }

    /**
     * 设置：创建人guid
     */
    public void setInfoCreateUserGuid(String infoCreateUserGuid) {
        this.infoCreateUserGuid = infoCreateUserGuid;
    }

    /**
     * 获取：创建人guid
     */
    public String getInfoCreateUserGuid() {
        return infoCreateUserGuid;
    }

    /**
     * 设置：信息附件
     */
    public void setInfoFile(String infoFile) {
        this.infoFile = infoFile;
    }

    /**
     * 获取：信息附件
     */
    public String getInfoFile() {
        return infoFile;
    }

    /**
     * 设置：点击(查看)次数
     */
    public void setClickTimes(Integer clickTimes) {
        this.clickTimes = clickTimes;
    }

    /**
     * 获取：点击(查看)次数
     */
    public Integer getClickTimes() {
        return clickTimes;
    }

    /**
     * 设置：信息发布图片附件
     */
    public void setInfoPic(String infoPic) {
        this.infoPic = infoPic;
    }

    /**
     * 获取：信息发布图片附件
     */
    public String getInfoPic() {
        return infoPic;
    }

    /**
     * 设置：信息栏目guid
     */
    public void setInfoCategoryGuid(String infoCategoryGuid) {
        this.infoCategoryGuid = infoCategoryGuid;
    }

    /**
     * 获取：信息栏目guid
     */
    public String getInfoCategoryGuid() {
        return infoCategoryGuid;
    }

    /**
     * 设置：信息类型
     */
    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    /**
     * 获取：信息类型
     */
    public String getInfoType() {
        return infoType;
    }

    /**
     * 设置：链接地址
     */
    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }

    /**
     * 获取：链接地址
     */
    public String getInfoUrl() {
        return infoUrl;
    }

    /**
     * 设置：信息状态
     */
    public void setInfoStatus(Integer infoStatus) {
        this.infoStatus = infoStatus;
    }

    /**
     * 获取：信息状态
     */
    public Integer getInfoStatus() {
        return infoStatus;
    }

    public String getIsShowInformation() {
        return isShowInformation;
    }

    public void setIsShowInformation(String isShowInformation) {
        this.isShowInformation = isShowInformation;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

}

