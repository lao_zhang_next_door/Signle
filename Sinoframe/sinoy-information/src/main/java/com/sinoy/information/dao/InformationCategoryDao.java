package com.sinoy.information.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.information.entity.InformationCategory;

import java.util.List;
import java.util.Map;

public interface InformationCategoryDao extends ExtendMapper<InformationCategory> {

    /**
     * 根据当前角色获取栏目树
     *
     * @param params
     * @return
     */
    List<InformationCategory> findCategoryTreesByRoles(Map<String, Object> params);
}

