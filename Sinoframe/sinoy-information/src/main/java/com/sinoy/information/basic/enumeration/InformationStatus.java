package com.sinoy.information.basic.enumeration;

/**
 * 信息发布状态枚举
 *
 * @author wzl
 */
public enum InformationStatus {
    /**
     *
     */
    YFB(0, "已发布"),
    DSH(1, "待审核"),
    SHBTG(2, "审核不通过"),
    STOP(3, "停止发布");

    private final int code;
    private final String value;

    private InformationStatus(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
