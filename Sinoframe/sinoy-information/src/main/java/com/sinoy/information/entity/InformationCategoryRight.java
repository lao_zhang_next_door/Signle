package com.sinoy.information.entity;

import com.sinoy.information.basic.base.BaseEntity;

/**
 * 角色栏目权限表
 */
public class InformationCategoryRight extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public InformationCategoryRight(boolean init) {
        super(init);
    }

    public InformationCategoryRight(String rowGuid) {
        super(rowGuid);
    }

    public InformationCategoryRight() {
    }


    /**
     * 栏目Guid
     */
    private String categoryGuid;

    /**
     * 授权对象
     */
    private String allowTo;

    /**
     * 授权类型
     */
    private Integer allowType;

    public String getCategoryGuid() {
        return categoryGuid;
    }

    public void setCategoryGuid(String categoryGuid) {
        this.categoryGuid = categoryGuid;
    }

    public String getAllowTo() {
        return allowTo;
    }

    public void setAllowTo(String allowTo) {
        this.allowTo = allowTo;
    }

    public Integer getAllowType() {
        return allowType;
    }

    public void setAllowType(Integer allowType) {
        this.allowType = allowType;
    }
}

