package com.sinoy.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.information.dao.InformationInfoDao;
import com.sinoy.information.entity.InformationInfo;
import com.sinoy.information.entity.dto.InformationInfoDto;
import com.sinoy.information.entity.vo.InformationInfoVo;
import com.sinoy.information.service.InformationInfoCategoryService;
import com.sinoy.information.service.InformationInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class InformationInfoServiceImpl extends BaseServiceImpl<InformationInfoDao, InformationInfo> implements InformationInfoService {


    @Autowired
    private InformationInfoDao informationInfoDao;

    @Autowired
    protected HttpServletRequest request;

    @Resource
    private CommonPropAndMethods commonPropAndMethods;

    @Autowired
    private InformationInfoCategoryService informationInfoCategoryService;

    @Override
    public List<InformationInfoVo> getInfoRelationList(Map<String, Object> params, boolean isSelf) {
        XnwUser user = commonPropAndMethods.getXnwUser();
        Optional.ofNullable(user).orElseThrow(() -> new BaseException("用户不存在"));
        if (!user.getRoleGuidList().isEmpty()) {
            String roleGuids = StringUtils.join(user.getRoleGuidList(), ",");
            params.put("roleGuids", roleGuids);
        }
        //查询自己创建的
        if (isSelf) {
            params.put("infoCreateUserGuid_equal", user.getRowGuid());
        }
        return informationInfoDao.getInfoRelationList(params);
    }

    @Override
    public int getInfoRelationCount(Map<String, Object> params, boolean isSelf) {
        XnwUser user = commonPropAndMethods.getXnwUser();
        Optional.ofNullable(user).orElseThrow(() -> new BaseException("用户不存在"));
        //查询自己创建的
        if (isSelf) {
            params.put("infoCreateUserGuid_equal", user.getRowGuid());
        }
        return informationInfoDao.getInfoRelationCount(params);
    }

    @Override
    public List<InformationInfoDto> getNoticeInfoList(Map<String, Object> params) {
        return informationInfoDao.getNoticeInfoList(params);
    }

    @Override
    public int getNoticeInfoListUnReadCount(Map<String, Object> params) {
        return informationInfoDao.getNoticeInfoListUnReadCount(params);
    }

    @Override
    public InformationInfoDto getLastNotice(Map<String, Object> params) {
        return informationInfoDao.getLastNotice(params);
    }

    @Override
    public String deliveryInfo(InformationInfoVo informationInfo) {
        if (informationInfo.getInfoCreateUserGuid() == null || "".equals(informationInfo.getInfoCreateUserGuid())) {
            //获取创建人信息
            XnwUser user = commonPropAndMethods.getXnwUser();
            Optional.ofNullable(user).orElseThrow(() -> new BaseException("用户不存在"));
            informationInfo.setInfoCreateUser(user.getUsername());
            informationInfo.setInfoCreateUserGuid(user.getRowGuid());
        }
        if (informationInfo.getCategoryGuids().isEmpty()) {
            throw new BaseException("infoCategoryGuid(栏目guid) 值为null");
        }
        informationInfo.initNull();
        int infoStatus = informationInfoCategoryService.batchInsertRelation(informationInfo.getRowGuid(), informationInfo.getCategoryGuids());
        informationInfo.setInfoStatus(infoStatus);
        informationInfo.setClickTimes(0);
        informationInfoDao.insert(informationInfo);
        return informationInfo.getRowGuid();
    }

    @Override
    public void updateInfo(InformationInfoVo informationInfo) {
        if (informationInfo.getCategoryGuids().isEmpty()) {
            throw new BaseException("infoCategoryGuid(栏目guid) 值为null");
        }
        int infoStatus = informationInfoCategoryService.batchInsertRelation(informationInfo.getRowGuid(), informationInfo.getCategoryGuids());
        informationInfo.setInfoStatus(infoStatus);
        informationInfo.setUpdateTime(LocalDateTime.now());
        UpdateWrapper<InformationInfo> wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", informationInfo.getRowGuid());
        informationInfoDao.update(informationInfo, wrapper);
    }

    @Override
    public void updateClickTimes(String rowGuid) {
        informationInfoDao.updateClickTimes(rowGuid);
    }
}

