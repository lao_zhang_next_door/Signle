package com.sinoy.servicezjgsdacx.config;

import com.sinoy.core.common.constant.XnwConstant;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器去获取token并验证token
 * <p>Title: AuthenticationInterceptor</p>
 * <p>Description: </p>
 *
 * @author hero
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

    /**
     * 当请求达到服务A时，应用中的拦截器会从请求头中获取trace_id，如果为空，
     * 则生成一个trace_id，并设置到SLF4J的MDC中，如果服务A调用服务B/服务C，那么需要将trace_id设置到请求头中。
     * @param httpServletRequest
     * @param httpServletResponse
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,
                             HttpServletResponse httpServletResponse, Object handler) throws Exception {
        if(StringUtil.isNotBlank(httpServletRequest.getHeader(XnwConstant.XNW_TRACE_ID))){
            MDC.put(XnwConstant.LOG_TRACE_ID, httpServletRequest.getHeader(XnwConstant.XNW_TRACE_ID));
        }




        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
        MDC.remove(XnwConstant.LOG_TRACE_ID);
    }

    public static long getDatePoor(java.util.Date expireTime, java.util.Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = expireTime.getTime() - nowDate.getTime();
        // 计算差多少分钟
        long min = diff / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return min;
    }
}