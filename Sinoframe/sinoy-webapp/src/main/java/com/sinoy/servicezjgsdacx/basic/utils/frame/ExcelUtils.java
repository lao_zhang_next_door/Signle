package com.sinoy.servicezjgsdacx.basic.utils.frame;

import com.sinoy.core.common.utils.dataformat.ExcelUtil;
import com.sinoy.core.common.utils.dataformat.MergeCommand;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.jxls.builder.xls.XlsCommentAreaBuilder;
import org.jxls.common.Context;
import org.jxls.expression.JexlExpressionEvaluator;
import org.jxls.transform.Transformer;
import org.jxls.transform.poi.PoiTransformer;
import org.jxls.util.JxlsHelper;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Excel 工具类
 * @ClassName: ExcelUtil
 * @Description: Excel 工具类
 * @author keeny
 * @date 2018年9月28日 下午3:21:19
 *
 */
public class ExcelUtils {
    public static final String OFFICE_EXCEL_2003_POSTFIX = "xls";

    public static final String OFFICE_EXCEL_2010_POSTFIX = "xlsx";

    public static final String EMPTY = "";

    public static final String POINT = ".";

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * 使用批量导入方法时，请注意需要导入的Bean的字段和excel的列一一对应
     * @param clazz
     * @param file
     * @param beanPropertys
     * @return
     */
    public static <T> List<T> parserExcel(Class<T> clazz, File file, String[] beanPropertys) {
        // 得到workbook
        List<T> list = new ArrayList<T>();
        try {
            Workbook workbook = WorkbookFactory.create(file);
            Sheet sheet = workbook.getSheetAt(0);
            // 直接从第三行开始获取数据
            int rowSize = sheet.getPhysicalNumberOfRows();
            if(rowSize > 1){
                for (int i = 2; i < rowSize; i++) {
                    T t = clazz.newInstance();
                    Row row = sheet.getRow(i);
                    int cellSize = row.getPhysicalNumberOfCells();
                    for(int j=0; j<cellSize; j++){
                        Object cellValue = getCellValue(row.getCell(j));
                        BeanUtils.copyProperty(t, beanPropertys[j], cellValue);
                    }
                    list.add(t);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    /**
     * MultipartFile 转 File
     *
     * @param file
     * @throws Exception
     */
    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }

    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除本地临时文件
     * @param file
     */
    public static void delteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }
    }


    /**
     * 通用的读取excel单元格的处理方法
     * @param cell
     * @return
     */
    private static Object getCellValue(Cell cell) {
        Object result = null;
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                    result = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    //对日期进行判断和解析
                    if(HSSFDateUtil.isCellDateFormatted(cell)){
                        double cellValue = cell.getNumericCellValue();
                        result = HSSFDateUtil.getJavaDate(cellValue);
                    }else {
                        cell.setCellType(CellType.STRING);
                        result = cell.getStringCellValue();
                    }
                    break;
                case BOOLEAN:
                    result = cell.getBooleanCellValue();
                    break;
                case FORMULA:
                    result = cell.getCellFormula();
                    break;
                case ERROR:
                    result = cell.getErrorCellValue();
                    break;
                case BLANK:
                    break;
                default:
                    break;
            }
        }
        return result;
    }

    /**
     * 获得path的后缀名
     * @param path
     * @return
     */
    public static String getPostfix(String path){
        if(path==null || EMPTY.equals(path.trim())){
            return EMPTY;
        }
        if(path.contains(POINT)){
            return path.substring(path.lastIndexOf(POINT)+1,path.length());
        }
        return EMPTY;
    }
    /**
     * 单元格格式
     * @param hssfCell
     * @return
     */
    @SuppressWarnings({ "static-access", "deprecation" })
    public static String getHValue(HSSFCell hssfCell){
        if (hssfCell.getCellType() == CellType.BOOLEAN) {
            return String.valueOf(hssfCell.getBooleanCellValue());
        } else if (hssfCell.getCellType() == CellType.NUMERIC) {
            String cellValue = "";
            if(HSSFDateUtil.isCellDateFormatted(hssfCell)){
                Date date = HSSFDateUtil.getJavaDate(hssfCell.getNumericCellValue());
                cellValue = sdf.format(date);
            }else{
                DecimalFormat df = new DecimalFormat("#.##");
                cellValue = df.format(hssfCell.getNumericCellValue());
                String strArr = cellValue.substring(cellValue.lastIndexOf(POINT)+1,cellValue.length());
                if(strArr.equals("00")){
                    cellValue = cellValue.substring(0, cellValue.lastIndexOf(POINT));
                }
            }
            return cellValue;
        } else {
            return String.valueOf(hssfCell.getStringCellValue());
        }
    }
    /**
     * 单元格格式
     * @param xssfCell
     * @return
     */
    public static String getXValue(XSSFCell xssfCell){
        if (xssfCell.getCellType() == CellType.BOOLEAN) {
            return String.valueOf(xssfCell.getBooleanCellValue());
        } else if (xssfCell.getCellType() == CellType.NUMERIC) {
            String cellValue = "";
            if(DateUtil.isCellDateFormatted(xssfCell)){
                Date date = DateUtil.getJavaDate(xssfCell.getNumericCellValue());
                cellValue = sdf.format(date);
            }else{
                DecimalFormat df = new DecimalFormat("#.##");
                cellValue = df.format(xssfCell.getNumericCellValue());
                String strArr = cellValue.substring(cellValue.lastIndexOf(POINT)+1,cellValue.length());
                if(strArr.equals("00")){
                    cellValue = cellValue.substring(0, cellValue.lastIndexOf(POINT));
                }
            }
            return cellValue;
        } else {
            return String.valueOf(xssfCell.getStringCellValue());
        }
    }

    public static void exportExcel(String fileName, String templateName, Map<String, Object> model,HttpServletResponse response, HttpServletRequest request) throws IOException{
        Context context = PoiTransformer.createInitialContext();
        if (model != null) {
            for (String key : model.keySet()) {
                context.putVar(key, model.get(key));
            }
        }

        response.reset();
        response.setHeader("Accept-Ranges", "bytes");
        OutputStream os = null;
        // 解决各浏览器的中文乱码问题
        String userAgent = request.getHeader("User-Agent");

        // fileName.getBytes("UTF-8")处理safari的乱码问题
        byte[] bytes = userAgent.contains("MSIE") ? fileName.getBytes() : fileName.getBytes("UTF-8");

        // 各浏览器基本都支持ISO编码
        fileName = new String(bytes, "ISO-8859-1");
        response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", fileName+".xlsx"));
        response.setContentType("application/octet-stream;charset=UTF-8");

        os = response.getOutputStream();

//      启动新的jxls-api 加载自定义方法
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath()+"templates/excelTemplate/"+templateName+".xlsx";
        InputStream is = new FileInputStream(new File(path));
        System.out.println(is+" >>>"+os);

        JxlsHelper jxlsHelper = JxlsHelper.getInstance();
        Transformer transformer  = jxlsHelper.createTransformer(is, os);
        //获得配置
        JexlExpressionEvaluator evaluator = (JexlExpressionEvaluator)transformer.getTransformationConfig().getExpressionEvaluator();
        //设置静默模式，不报警告
        //evaluator.getJexlEngine().setSilent(true);
        //函数强制，自定义功能
        Map<String, Object> funcs = new HashMap<String, Object>();
        funcs.put("utils", new ExcelUtil());    //添加自定义功能
        evaluator.getJexlEngine().setFunctions(funcs);
        //必须要这个，否者表格函数统计会错乱
        jxlsHelper.setUseFastFormulaProcessor(false).processTemplate(context, transformer);
    }

//    public static byte[] exportExcelZip(String fileName, String templateName, Map<String, Object> model,HttpServletResponse response, HttpServletRequest request,String compressedFile) throws IOException{
//        Context context = PoiTransformer.createInitialContext();
//        if (model != null) {
//            for (String key : model.keySet()) {
//                context.putVar(key, model.get(key));
//            }
//        }
//
//        response.reset();
//        response.setHeader("Accept-Ranges", "bytes");
//        // 解决各浏览器的中文乱码问题
//        String userAgent = request.getHeader("User-Agent");
//
//        // fileName.getBytes("UTF-8")处理safari的乱码问题
//        byte[] bytes = userAgent.contains("MSIE") ? fileName.getBytes() : fileName.getBytes("UTF-8");
//
//        // 各浏览器基本都支持ISO编码
//        fileName = new String(bytes, "ISO-8859-1");
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////        ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
//
//
////      启动新的jxls-api 加载自定义方法
//        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath()+"templates/excelTemplate/"+templateName+".xlsx";
//        InputStream is = new FileInputStream(new File(path));
//
//        File file = null;
//        file = new File(compressedFile+".xlsx");//文件路径（路径+文件名）
//        zipOutputStream.putNextEntry(new ZipEntry(file.getPath()));
//
//        System.out.println(is+" >>>"+zipOutputStream);
//
//        JxlsHelper jxlsHelper = JxlsHelper.getInstance();
//        Transformer transformer  = jxlsHelper.createTransformer(is, zipOutputStream);
//        //获得配置
//        JexlExpressionEvaluator evaluator = (JexlExpressionEvaluator)transformer.getTransformationConfig().getExpressionEvaluator();
//        //设置静默模式，不报警告
//        //evaluator.getJexlEngine().setSilent(true);
//        //函数强制，自定义功能
//        Map<String, Object> funcs = new HashMap<String, Object>();
//        funcs.put("utils", new ExcelUtil());    //添加自定义功能
//        evaluator.getJexlEngine().setFunctions(funcs);
//        //必须要这个，否者表格函数统计会错乱
//        jxlsHelper.setUseFastFormulaProcessor(false).processTemplate(context, transformer);
//        return outputStream.toByteArray();
//    }

    //单元格合并
    static{
        //添加自定义指令（可覆盖jxls原指令）
        //合并单元格(模板已经做过合并单元格操作的单元格无法再次合并)
        XlsCommentAreaBuilder.addCommandMapping("merge", MergeCommand.class);
    }

    //获取jxls模版文件
    public static File getTemplate(String path){
        File template = new File(path);
        if(template.exists()){
            return template;
        }
        return null;
    }

    // 日期格式化
    public String dateFmt(Date date, String fmt) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat dateFmt = new SimpleDateFormat(fmt);
            return dateFmt.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    // if判断
    public Object ifelse(boolean b, Object o1, Object o2) {
        return b ? o1 : o2;
    }
}

