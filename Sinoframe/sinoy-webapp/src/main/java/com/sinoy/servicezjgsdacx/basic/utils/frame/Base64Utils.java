package com.sinoy.servicezjgsdacx.basic.utils.frame;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;

public class Base64Utils implements Serializable {

	public static String TransformPhotoToBase64Data(String LoadPath){
		Base64.Encoder encoder= Base64.getEncoder();  //获取Base64编码器
		byte [] ImgContainer = null ;    //数据集缓存器
		try {
			URLConnection openConnection = new URL(LoadPath).openConnection();
			InputStream inputStream = openConnection.getInputStream(); //到指定路径寻找文件
			ImgContainer = readInputStream(inputStream);          //设置图片字节数据缓冲区大小
			String Base64ImgData =encoder.encodeToString(ImgContainer);  //将图片编码转换成Base64格式的数据集
			inputStream.close();      //关闭数据流
			return Base64ImgData;  //将缓冲区数据转换成字符数据返回
		} catch (Exception e) {
			return "找不到指定文件!";
		}
	}

	/**
	 * 从输入流中获取数据
	 * @param inStream 输入流
	 * @return
	 * @throws Exception
	 */
	public static byte[] readInputStream(InputStream inStream) throws Exception{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while( (len=inStream.read(buffer)) != -1 ){
			outStream.write(buffer, 0, len);
		}
		inStream.close();
		return outStream.toByteArray();
	}


	public static void main(String[] args) {
		System.out.println(TransformPhotoToBase64Data("http://localhost:9528/frame/file/2022-05-12/4a54023f-87f0-45cc-b780-baf0c5ff3757.jpg"));
//		System.out.println(TransformPhotoToBase64Data("http://localhost:9528/frame/file/2022-05-12/4a54023f-87f0-45cc-b780-baf0c5ff3757.jpg").length());
	}

	public static String encode(String str){
		String encodeToString = Base64.getEncoder().encodeToString(str.getBytes());
		return encodeToString;
	}

	public static String encode(byte[] b){
		String encodeToString = Base64.getEncoder().encodeToString(b);
		return encodeToString;
	}

}
