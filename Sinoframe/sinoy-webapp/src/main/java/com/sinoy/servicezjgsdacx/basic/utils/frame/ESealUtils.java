package com.sinoy.servicezjgsdacx.basic.utils.frame;


import com.sinoy.core.common.utils.dataformat.StringUtil;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 *  张家港电子印章获取token
  *
 */
public class ESealUtils implements Serializable {

	public static String createToken(String appKey, String appSecret) {
		if (StringUtil.isBlank(appKey) || StringUtil.isBlank(appSecret)) {
			throw new RuntimeException("appKey|appSecret不能为空");
		}
		// 在调用该方法之后的5分钟内，生成的token有效
		Integer num = (int) ((System.currentTimeMillis() / 1000) + 60 * 5);
		String p1 = num.toString();
		try {
			// UrlEncode，并Base64编码
			String str = URLEncoder.encode(p1, "UTF-8");
			String p2 = Base64Utils.encode(str);
			// 对P2加密
			byte[] b = hmacSha1(p2, appSecret);
			String p4 = Base64Utils.encode(b).replace("+", "-").replace("/", "_");
			String result = appKey + "@" + p4 + "@" + p2;
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] hmacSha1(String P2, String AppSecret) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(AppSecret.getBytes("UTF-8"), "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(signingKey);
		byte[] P3= mac.doFinal(P2.getBytes("UTF-8"));
		return P3;
	}

	public static void main(String[] args) {
		System.out.println(createToken("ZJGDACX","20d1d5e3-af7a-44fe-bd1a-7681a6dcf5dc"));

	}
	
}
