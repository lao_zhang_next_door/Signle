package com.sinoy.servicezjgsdacx;



import com.sinoy.core.database.tk.mybatis.spring.annotation.MapperScan;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync
@SpringBootApplication(scanBasePackages={"com.sinoy"})
@EnableTransactionManagement
@EnableEncryptableProperties
@MapperScan("com.sinoy.servicezjgsdacx.dao")
public class StartApplication {
	
  public static void main(String[] args) {
    SpringApplicationBuilder app = new SpringApplicationBuilder(StartApplication.class);
    app.build().addListeners(new ApplicationPidFileWriter("./bin/shutdown.pid"));
    app.run();
  }

//  public static void main(String[] args) {
//    Long a = 3L;
//    String b = "3";
//    System.out.println(a == Long.valueOf(b));
//    System.out.println(a.equals(Long.valueOf(b)));
//
//    long c = 3L;
//    System.out.println(a == c);
//    System.out.println(a.equals(c));
//    System.out.println(c == Long.parseLong(b));
//  }

}
