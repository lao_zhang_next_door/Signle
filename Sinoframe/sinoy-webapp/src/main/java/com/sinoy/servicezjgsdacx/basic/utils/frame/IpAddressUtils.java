package com.sinoy.servicezjgsdacx.basic.utils.frame;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class IpAddressUtils implements Serializable {

    public static void main(String[] args) throws UnknownHostException, SocketException {
        //得到IP，输出PC-201309011313/122.206.73.83
        InetAddress ipAddress = InetAddress.getLocalHost();
        String ipAddressStr = "";
        if ((ipAddress != null) && (ipAddress.toString().length() > 15)) { //"***.***.***.***".length() = 15

            if (ipAddress.toString().indexOf("/") > 0) {
                ipAddressStr = ipAddress.toString().substring(ipAddress.toString().indexOf("/")+1);
            }
        }
        System.out.println(ipAddressStr);
        getAddressInfo();
    }

    public static Map getAddressInfo() throws SocketException, UnknownHostException {
        InetAddress ipAddress = InetAddress.getLocalHost();
        String ipAddressStr = "";
        if ((ipAddress != null) && (ipAddress.toString().length() > 15)) { //"***.***.***.***".length() = 15

            if (ipAddress.toString().indexOf("/") > 0) {
                ipAddressStr = ipAddress.toString().substring(ipAddress.toString().indexOf("/")+1);
            }
        }
        //获取网卡，获取地址
        byte[] mac = NetworkInterface.getByInetAddress(ipAddress).getHardwareAddress();
        System.out.println("mac数组长度：" + mac.length);
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < mac.length; i++) {
            if (i != 0) {
                sb.append("-");
            }
           //字节转换为整数
            int temp = mac[i] & 0xff;
            String str = Integer.toHexString(temp);
            System.out.println("每8位:" + str);
            if (str.length() == 1) {
                sb.append("0" + str);
            } else {
                sb.append(str);
            }
        }
        Map addressMap = new HashMap();
        addressMap.put("ipAddress",ipAddressStr);
        addressMap.put("macAddress",sb.toString().toUpperCase());
        return addressMap;
//        System.out.println("本机MAC地址:" + sb.toString().toUpperCase());
    }
}
