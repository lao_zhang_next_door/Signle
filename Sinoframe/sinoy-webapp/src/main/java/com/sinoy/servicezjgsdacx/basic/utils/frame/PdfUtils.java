package com.sinoy.servicezjgsdacx.basic.utils.frame;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.dom4j.DocumentException;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.util.List;

public class PdfUtils {

    /**
     * 获取当前系统的临时目录
     */
//    private static final String FILE_PATH = System.getProperty("java.io.tmpdir") + File.separator;
    private static final String FILE_PATH = "D:" + File.separator;
    public static void main(String[] args) throws IOException, DocumentException {

        String sjFileName = "省级主管部门初审意见.pdf";
//        String title = "省级主管部门初审意见";
//        String pdfText = "审批意见：插入审批意见！审批意见：插入审批意见！审批意见：插入审批意见！审批意见：插入审批意见！审批意见：插入审批意见！";
//        createSjFile(sjFileName, title, pdfText);

        String[] files = new String[2];
        files[0] = "a.pdf";
        files[1] = "a_signed.pdf";
        String newFile = "c_signed.pdf";
        mergePdfFiles(files, newFile);

    }

    /**
     * 新建pdf，保存省局审核信息
     * @param sjFileName
     * @param content
     * @throws IOException
     * @throws DocumentException
     */
    public static void createSjFile(String sjFileName, String title, String content) throws Exception {
        Document document = new Document(PageSize.A4, 100, 100, 50, 0);
        //指定字体库，并创建字体
        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        PdfWriter.getInstance(document, new FileOutputStream(FILE_PATH + sjFileName));
        document.open();

        //添加标题
        Font titleFont = new Font(baseFont, 18);
        Paragraph titleParagraph = new Paragraph(title,titleFont);
        titleParagraph.setAlignment(Element.ALIGN_CENTER); //设置对齐方式：居中、左对齐、右对齐
        titleParagraph.setSpacingAfter(50f);//设置段落下方的空白距离
        document.add(titleParagraph);

        //添加内容
        Font contentFont = new Font(baseFont, 12);
        Paragraph contentParagraph = new Paragraph(content,contentFont);
        contentParagraph.setFirstLineIndent(20f);//设置首行缩进
        document.add(contentParagraph);

        document.close();
    }

    /**
     * 合并pdf文件
     * @param files
     * @param newfile
     * @return
     */
    public static boolean mergePdfFiles(String[] files, String newfile) {
        boolean retValue = false;
        Document document = null;
        try {
            PdfReader pdfReader = new PdfReader(FILE_PATH + files[0]);
            document = new Document(pdfReader.getPageSize(1));
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(FILE_PATH + newfile));
            document.open();
            for (int i = 0; i < files.length; i++) {
                PdfReader reader = new PdfReader(FILE_PATH + files[i]);
                int n = reader.getNumberOfPages();
                for (int j = 1; j <= n; j++) {
                    document.newPage();
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);
                }
                reader.close();
            }
            retValue = true;
            copy.close();
            pdfReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document != null) document.close();
        }
        return retValue;
    }

    /**
     * 生成添加文字数据流
     *
     * @param text 输入信息
     * @param width 宽度
     * @param height 高度
     * @return 数据流
     */
    public static byte[] addText(String pdfPath, String text, int width, int height) throws DocumentException {

        ByteArrayOutputStream byteArrayOutputStream = null;
        PdfReader pdfReader = null;
        PdfStamper pdfStamper = null;
        PdfContentByte pdfContentByte;
        try {
            BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            InputStream inputStream = new FileInputStream(pdfPath); // 读取默认pdf文件
            pdfReader = new PdfReader(inputStream); // 加载文件到pdf引擎
            byteArrayOutputStream = new ByteArrayOutputStream();
            pdfStamper = new PdfStamper(pdfReader, byteArrayOutputStream); // 加载模板
            pdfContentByte = pdfStamper.getOverContent(1); // 获取顶部
            pdfContentByte.beginText(); // 插入文字信息
            pdfContentByte.setFontAndSize(baseFont, 12);
            BaseColor baseColor = new BaseColor(0, 0, 0);
            pdfContentByte.setColorFill(baseColor);
            pdfContentByte.setTextMatrix(width, height); // 设置文字在页面中的坐标
            pdfContentByte.showText(text);
            pdfContentByte.endText();
        } catch (Exception e) {
            System.err.println("失败了！" + e);
        } finally {
            try {
                if (pdfStamper != null) pdfStamper.close();
                if (pdfReader != null) pdfReader.close();
                if (byteArrayOutputStream != null) byteArrayOutputStream.close();
            } catch (Exception e) {
                System.err.println("失败了！" + e);
            }
        }
        if (byteArrayOutputStream != null) return byteArrayOutputStream.toByteArray();
        else return null;
    }

    public static void writeBytesToFile(String fileName, byte[] bytes) throws IOException {

        System.err.println("文件路径：" + FILE_PATH);
        OutputStream out = new FileOutputStream(FILE_PATH + fileName);
        InputStream is = new ByteArrayInputStream(bytes);
        byte[] buff = new byte[1024];
        int len;
        while((len=is.read(buff))!=-1){
            out.write(buff, 0, len);
        }
        is.close();
        out.close();
    }

    /**
     * 获取MultipartFile
     * @param fileName
     * @return
     * @throws IOException
     */
    public static MultipartFile getMultipartFile(String fileName) throws IOException {

        File file = new File(FILE_PATH + fileName);
        return new MockMultipartFile(file.getName(), file.getName(), "text/plain", new FileInputStream(file));

    }

    /**
     * 删除文件
     * @param fileNames
     * @throws IOException
     */
    public static void deleteFile(List<String> fileNames) throws IOException {
        for (String fileName : fileNames) {
            File file = new File(FILE_PATH + fileName);
            if (file.exists()) file.delete();
        }
    }

    /**
     * 添加水印
     * @param srcPdfPath
     * @param tarPdfPath
     * @param WaterMarkContent
     * @throws Exception
     */
    public static void addWaterMark(String srcPdfPath,String tarPdfPath,String WaterMarkContent)throws Exception {
        PdfReader reader = new PdfReader(srcPdfPath);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(tarPdfPath));
        PdfGState gs = new PdfGState();
        BaseFont font =  BaseFont.createFont("STSong-Light","UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        gs.setFillOpacity(0.4f);// 设置透明度

        int total = reader.getNumberOfPages() + 1;
        PdfContentByte content;
        for (int i = 1; i < total; i++) {
            content = stamper.getOverContent(i);
            content.beginText();
            content.setGState(gs);
            content.setColorFill(BaseColor.DARK_GRAY); //水印颜色
            content.setFontAndSize(font, 56); //水印字体样式和大小
            content.showTextAligned(Element.ALIGN_CENTER,WaterMarkContent, 300, 300, 30); //水印内容和水印位置
            content.endText();
        }
        stamper.close();
        System.out.println("PDF水印添加完成！");
    }

}