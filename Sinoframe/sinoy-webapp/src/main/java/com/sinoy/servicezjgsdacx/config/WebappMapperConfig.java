package com.sinoy.servicezjgsdacx.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan({"com.sinoy.*.module.*.dal.mysql.*"})
@Configuration
public class WebappMapperConfig {
}
