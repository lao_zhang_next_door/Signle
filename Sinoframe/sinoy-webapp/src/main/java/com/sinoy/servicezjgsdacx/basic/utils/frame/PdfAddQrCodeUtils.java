package com.sinoy.servicezjgsdacx.basic.utils.frame;


import com.spire.barcode.*;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.PdfPageSize;
import com.spire.pdf.graphics.PdfImage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class PdfAddQrCodeUtils {

    public static void main(String[] args) throws IOException {
        String showPdfPath = "http://192.168.2.178:9528/#/showPdf?formRowGuid=098ff85a-45b0-49d8-a31b-f0b652b6f3fe";
        addBarCode("D:\\pdfImg\\","dc74f722-ab76-4cb6-ad1c-c65aa603aa4b", "_waterMark.pdf", showPdfPath, "婚姻登记证明");
    }

    public static void addBarCode(String preFilePath, String fileName, String falgStr, String showPdfPath, String eLicenceName) throws IOException {

        PdfDocument pdf = new PdfDocument();
        pdf.loadFromFile(preFilePath + fileName + falgStr);
        //获取第一页
        PdfPageBase page = null;
        if("婚姻登记证明".equals(eLicenceName)){
             page = pdf.getPages().get(0);
        }else{
             page = pdf.getPages().get(0);
        }

        //生成二维码图片，绘制到PDF页面
        BarcodeSettings settings = new BarcodeSettings();//创建二维码图形
        settings.setType(BarCodeType.QR_Code);
        settings.setData(showPdfPath);
        settings.setData2D("张家港党政通");
        settings.setX(0.15f);
        settings.setShowTextOnBottom(true);
        settings.setQRCodeECL(QRCodeECL.Q);
        settings.setQRCodeDataMode(QRCodeDataMode.Numeric);
        BarCodeGenerator generator = new BarCodeGenerator(settings);
        Image image = generator.generateImage();
        PdfImage pdfImage = PdfImage.fromImage((BufferedImage) image);//绘制二维码图片到PDF
        page.getCanvas().drawImage(pdfImage, PdfPageSize.A4.getWidth() - 350, 5);
        //保存PDF文档
        pdf.saveToFile(preFilePath + fileName  + "_signed.pdf");
        pdf.dispose();
    }




}