//package com.sinoy.servicezjgsdacx.config;
//
//import com.sinoy.core.common.base.Base;
//import com.sinoy.core.security.utils.CommonPropAndMethods;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import java.io.File;
//import java.io.IOException;
//
///**
// * 配置拦截器
// * <p>Title: InterceptorConfig</p>
// * <p>Description: </p>
// *
// * @author hero
// */
//@Configuration
//public class InterceptorConfig extends Base implements WebMvcConfigurer {
//
//    @Autowired
//    private CommonPropAndMethods commonPropAndMethods;
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//
//        registry.addInterceptor(authenticationInterceptor())
//                .addPathPatterns("/**") //所有路径都被拦截
//                .excludePathPatterns("/ws/**", "/auth/**", "/app/**", "/file/**", "/logs/**", "/templates/**", "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");//添加不拦截路径
//    }
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//        //registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
//        //文件磁盘url 映射
//        File fileSave = new File(commonPropAndMethods.filePath);//文件路径（路径+文件名）
//        String path = "";
//        if (!fileSave.exists()) {
//            fileSave.mkdirs();
//        }
//        try {
//            path = fileSave.getCanonicalPath();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String remoteUrl = path.replace("\\", "/") + "/";
//        //windows:
////        registry.addResourceHandler("/file/**","/logs/**","/templates/**").addResourceLocations("file:/"+remoteUrl,"file:/"+remoteUrl+"../logs/","classpath:/templates/");
//        //linux
//        registry.addResourceHandler("/file/**", "/logs/**", "/templates/**").addResourceLocations("file:" + remoteUrl, "file:/" + remoteUrl + "../logs/", "classpath:/templates/");
//    }
//
//    @Bean
//    public AuthenticationInterceptor authenticationInterceptor() {
//        return new AuthenticationInterceptor();
//    }
//
//}
