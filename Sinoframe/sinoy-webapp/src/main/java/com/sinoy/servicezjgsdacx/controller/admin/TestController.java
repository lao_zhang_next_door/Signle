package com.sinoy.servicezjgsdacx.controller.admin;

import cn.afterturn.easypoi.word.WordExportUtil;
import cn.afterturn.easypoi.word.entity.MyXWPFDocument;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.common.utils.dataformat.ExcelUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.security.annotation.PassToken;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * 尊老金信息导入Excel
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PassToken
    @PostMapping(value = "/importExcelInfo")
    public R importExcelInfo(@RequestParam(value = "file") MultipartFile file) throws Exception {
        File f = ExcelUtil.multipartFileToFile(file);
        excelReadUtil ex = new excelReadUtil();
        int startRow = 1;
        JSONObject res = ex.importExcelByFile(f, Objects.requireNonNull(file.getOriginalFilename()), startRow);
        excelReadUtil.deleteTempFile(f);
        return R.ok();
    }

    @PassToken
    @GetMapping(value = "/testv2")
    public void test(HttpServletResponse response) throws Exception {
        Map<String,Object> params = new HashMap(){
            {
                put("test","111111");
            }
        };
        InputStream is = this.getClass().getResourceAsStream("/a.docx");
//        XWPFDocument xwpfDocument = new XWPFDocument(is);
        MyXWPFDocument xwpfD =new MyXWPFDocument(is);
        WordExportUtil.exportWord07(xwpfD,params);
        xwpfD.write(response.getOutputStream());
    }

}
