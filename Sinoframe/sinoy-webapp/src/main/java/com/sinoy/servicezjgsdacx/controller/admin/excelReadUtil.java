//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.sinoy.servicezjgsdacx.controller.admin;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excelReadUtil {
    public excelReadUtil() {
    }

    public JSONObject importExcel(InputStream inputStream, String fileName, String[] beanPropertys, int startRow) throws Exception {
        String message = "Import success";
        boolean isE2007 = false;
        if (fileName.endsWith("xlsx")) {
            isE2007 = true;
        }

        int rowIndex = 0;
        JSONArray array = new JSONArray();
        JSONObject res = new JSONObject();

        try {
            Object wb;
            if (isE2007) {
                wb = new XSSFWorkbook(inputStream);
            } else {
                wb = new HSSFWorkbook(inputStream);
            }

            Sheet sheet = ((Workbook)wb).getSheetAt(0);
            int rowCount = sheet.getLastRowNum() + 1;

            for(int i = startRow; i < rowCount; ++i) {
                JSONObject obj = new JSONObject();
                int a = 0;

                for(int j = a; j < beanPropertys.length; ++j) {
                    if (this.isMergedRegion(sheet, i, j)) {
                        if (this.getMergedRegionValue(sheet, i, j) == null) {
                            obj.put(beanPropertys[j], "");
                        } else {
                            obj.put(beanPropertys[j], this.getMergedRegionValue(sheet, i, j));
                        }
                    } else {
                        Row row = sheet.getRow(i);
                        if (getCellValueByCell(row.getCell(j)) == null) {
                            obj.put(beanPropertys[j], "");
                        } else {
                            obj.put(beanPropertys[j], getCellValueByCell(row.getCell(j)));
                        }
                    }
                }

                array.add(obj);
            }
        } catch (Exception var19) {
//            System.out.println(var19.getMessage());
            var19.printStackTrace();
            message = "Import failed, please check the data in " + rowIndex + " rows ";
            res.put("code", "500");
            res.put("msg", message);
            return res;
        }

        res.put("code", "0");
        res.put("data", array);
        return res;
    }

    public JSONObject importExcel_new(InputStream inputStream, String fileName,  int startRow) throws Exception {
        String message = "Import success";
        boolean isE2007 = false;
        if (fileName.endsWith("xlsx")) {
            isE2007 = true;
        }

        int rowIndex = 0;
        JSONArray array = new JSONArray();
        JSONObject res = new JSONObject();

        try {
            Object wb;
            if (isE2007) {
                wb = new XSSFWorkbook(inputStream);
            } else {
                wb = new HSSFWorkbook(inputStream);
            }

            Sheet sheet = ((Workbook)wb).getSheetAt(0);
            int rowCount = sheet.getLastRowNum() + 1;
            int columCount=sheet.getRow(0).getLastCellNum();

            for(int i = startRow; i < rowCount; ++i) {
                JSONObject obj = new JSONObject();
                int a = 0;

                for(int j = a; j < columCount; ++j) {
                    if (this.isMergedRegion(sheet, i, j)) {
                        if (this.getMergedRegionValue(sheet, i, j) == null) {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), "");
                        } else {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), this.getMergedRegionValue(sheet, i, j));
                        }
                    } else {
                        Row row = sheet.getRow(i);
                        if (getCellValueByCell(row.getCell(j)) == null) {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), "");
                        } else {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), getCellValueByCell(row.getCell(j)));
                        }
                    }
                }

                array.add(obj);
            }
        } catch (Exception var19) {
//            System.out.println(var19.getMessage());
            var19.printStackTrace();
            message = "Import failed, please check the data in " + rowIndex + " rows ";
            res.put("code", "500");
            res.put("msg", message);
            return res;
        }

        res.put("code", "0");
        res.put("data", array);
        return res;
    }

    public JSONObject importExcelByFile(File file, String fileName,  int startRow) throws Exception {
        String message = "Import success";
        boolean isE2007 = false;
        if (fileName.endsWith("xlsx")) {
            isE2007 = true;
        }

        int rowIndex = 0;
        JSONArray array = new JSONArray();
        JSONObject res = new JSONObject();

        try {

            Workbook workbook = WorkbookFactory.create(file);
            Sheet sheet = workbook.getSheetAt(0);

            /*Object wb;
            if (isE2007) {
                wb = new XSSFWorkbook(inputStream);
            } else {
                wb = new HSSFWorkbook(inputStream);
            }

            Sheet sheet = ((Workbook)wb).getSheetAt(0);*/
            int rowCount = sheet.getLastRowNum() + 1;
            int columCount=sheet.getRow(0).getLastCellNum();

            for(int i = startRow; i < rowCount; ++i) {
                JSONObject obj = new JSONObject();
                int a = 0;

                for(int j = a; j < columCount; ++j) {
                    if (this.isMergedRegion(sheet, i, j)) {
                        if (this.getMergedRegionValue(sheet, i, j) == null) {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), "");
                        } else {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), this.getMergedRegionValue(sheet, i, j));
                        }
                    } else {
                        Row row = sheet.getRow(i);
                        if (getCellValueByCell(row.getCell(j)) == null) {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), "");
                        } else {
                            obj.put(String.valueOf(getCellValueByCell(sheet.getRow(0).getCell(j))), getCellValueByCell(row.getCell(j)));
                        }
                    }
                }

                array.add(obj);
            }
        } catch (Exception var19) {
//            System.out.println(var19.getMessage());
            var19.printStackTrace();
            message = "Import failed, please check the data in " + rowIndex + " rows ";
            res.put("code", "500");
            res.put("msg", message);
            return res;
        }

        System.out.println(array.size());

        res.put("code", "0");
        res.put("data", array);
        return res;
    }

    public static void deleteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }

    }

    public String getCellValue(Cell cell) {
        return cell == null ? "" : cell.getStringCellValue();
    }

    private static Object getCellValueByCell(Cell cell) {
        Object result = null;
        if (cell != null) {
            switch(cell.getCellType()) {
            case STRING:
                result = cell.getStringCellValue();
                break;
            case NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    double cellValue = cell.getNumericCellValue();
                    result = HSSFDateUtil.getJavaDate(cellValue);
                } else {
                    cell.setCellType(CellType.STRING);
                    result = cell.getStringCellValue();
                }
                break;
            case BOOLEAN:
                result = cell.getBooleanCellValue();
                break;
            case FORMULA:
                result = cell.getCellFormula();
                break;
            case ERROR:
                result = cell.getErrorCellValue();
            case BLANK:
            }
        } else {
            result = "";
        }

        return result;
    }

    public List<CellRangeAddress> getCombineCell(Sheet sheet) {
        List<CellRangeAddress> list = new ArrayList();
        int sheetmergerCount = sheet.getNumMergedRegions();

        for(int i = 0; i < sheetmergerCount; ++i) {
            CellRangeAddress ca = sheet.getMergedRegion(i);
            list.add(ca);
        }

        return list;
    }

    private int getRowNum(List<CellRangeAddress> listCombineCell, Cell cell, Sheet sheet) {
        int xr = 0;
        Iterator var9 = listCombineCell.iterator();

        while(var9.hasNext()) {
            CellRangeAddress ca = (CellRangeAddress)var9.next();
            int firstC = ca.getFirstColumn();
            int lastC = ca.getLastColumn();
            int firstR = ca.getFirstRow();
            int lastR = ca.getLastRow();
            if (cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR && cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC) {
                xr = lastR;
            }
        }

        return xr;
    }

    public String isCombineCell(List<CellRangeAddress> listCombineCell, Cell cell, Sheet sheet) throws Exception {
        String cellValue = null;
        Iterator var9 = listCombineCell.iterator();

        while(var9.hasNext()) {
            CellRangeAddress ca = (CellRangeAddress)var9.next();
            int firstC = ca.getFirstColumn();
            int lastC = ca.getLastColumn();
            int firstR = ca.getFirstRow();
            int lastR = ca.getLastRow();
            if (cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR) {
                if (cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC) {
                    Row fRow = sheet.getRow(firstR);
                    Cell fCell = fRow.getCell(firstC);
                    cellValue = this.getCellValue(fCell);
                    break;
                }
            } else {
                cellValue = "";
            }
        }

        return cellValue;
    }

    public String getMergedRegionValue(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();

        for(int i = 0; i < sheetMergeCount; ++i) {
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();
            if (row >= firstRow && row <= lastRow && column >= firstColumn && column <= lastColumn) {
                Row fRow = sheet.getRow(firstRow);
                Cell fCell = fRow.getCell(firstColumn);
                if(fCell.getCellType().equals(CellType.NUMERIC)){
                    fCell.setCellType(CellType.STRING);
                }
                return this.getCellValue(fCell);
            }
        }

        return null;
    }

    private boolean isMergedRegion(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();

        for(int i = 0; i < sheetMergeCount; ++i) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (row >= firstRow && row <= lastRow && column >= firstColumn && column <= lastColumn) {
                return true;
            }
        }

        return false;
    }
}
