package com.sinoy.servicezjgsdacx.basic.enumeration;

public enum EventStatus {

	SHZ(0,"审核中"),
	BLZ(1,"办理中"),
	YWC(2,"已完成");

	private final int code;
    private final String value;
    private EventStatus(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
