package com.sinoy.platform.system.component.dao;

import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.base.query.QueryWrapperX;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.utils.Query;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

public interface FrameUserDao extends ExtendMapper<FrameUser> {

    /**
     * 查询用户信息列表带部门信息 (联查部门表)
     * @param query
     * @return
     */
    List<UserInfo> getList(Query query);

    /**
     * 查询用户信息列表带部门数量 (联查部门表)
     * @param query
     * @return
     */
    Integer getListCount(Query query);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息(联查部门表 附件表)
     * @param userGuid
     * @return
     */
    UserInfo selectDetailByUserGuid(String userGuid);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息(联查部门表 附件表)
     * @param loginId
     * @return
     */
    UserInfo selectDetailByLoginId(String loginId);

    /**
     * 查询guid用户 关联角色信息
     * @param userGuid
     * @return
     */
    List<FrameRole> selectRoleListByUserGuid(String userGuid);

    /**
     * 获得指定部门的用户集合
     * @param deptIds
     * @return
     */
    default List<FrameUser> selectListByDeptIds(Set<Long> deptIds){
        return selectList(FrameUser::getDeptId, deptIds);
    }

    /**
     * 获得指定状态的用户数据
     * @param status 用户状态
     * @return
     */
    default List<FrameUser> selectListByStatus(Integer status){
        return selectList(FrameUser::getStatus,status,FrameUser::getDelFlag, DelFlag.Normal.getCode());
    };

    /**
     * 获取指定单位下的拥有指定角色的人员
     * @param deptCode 指定单位code
     * @param roleFlag 指定角色
     * @return
     */
    @Select("select * from frame_user f \n" +
            "left join frame_dept d on d.row_id = f.dept_id " +
            "left join frame_user_role u on u.user_guid = f.row_guid \n" +
            "left join frame_role r on r.row_guid = u.role_guid\n" +
            "where (locate(#{deptCode},concat(d.dept_code,'.')) > 0 or d.dept_code = #{deptCode}) and r.role_flag = #{roleFlag} and f.del_flag = 0")
    List<FrameUser> getUnityLeaderRole(@Param("deptCode") String deptCode, @Param("roleFlag") String roleFlag);
}
