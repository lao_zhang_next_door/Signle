package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.core.database.mybatisplus.handler.JsonLongSetTypeHandler;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

/**
* 角色权限 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里
*/
@Data
public class RoleLimitBaseVO {

    //角色id
    private Long roleId;


    //权限范围内的角色id
    private String roleChildId;


    //角色名称
    private String roleName;


    //可配置角色名称
    private String roleChildName;


}
