package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.redis.core.RedisService;
import com.sinoy.platform.system.component.dao.FrameCodeDao;
import com.sinoy.platform.system.component.dao.FrameCodeValueDao;
import com.sinoy.platform.system.component.entity.FrameCode;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.entity.pojo.FrameCodeValuePojo;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wzl
 */
@Service
public class FrameCodeValueServiceImpl extends BaseServiceImpl<FrameCodeValueDao, FrameCodeValue> implements FrameCodeValueService {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RedisService redisService;

    public FrameCodeValueServiceImpl(RedisService redisService) {
        this.redisService = redisService;
    }

    @Autowired
    private FrameCodeDao frameCodeDao;

    @Autowired
    private FrameCodeValueDao frameCodeValueDao;

    @Value(value = "${spring.jmx.default-domain}")
    public String projectName;

    @Override
    public List<FrameCodeValuePojo> selectParentCode(String rowGuid) {
        return baseMapper.selectParentCode(rowGuid);
    }

    @Override
    public String getItemTextByCodeNameAndCodeValue(String codeName, String codeValue) {

        QueryWrapper<FrameCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("code_name", codeName);
        List<FrameCode> codeList = frameCodeDao.selectList(queryWrapper);
        //code = null;

        if (!codeList.isEmpty() && codeList.size() > 1) {
            throw new BaseException("有重复的代码项>>" + codeName);
        }

        QueryWrapper<FrameCodeValue> Wrapper = new QueryWrapper<>();
        Wrapper.eq("code_guid", codeList.get(0).getRowGuid());
        Wrapper.eq("item_value", String.valueOf(codeValue));
        List<FrameCodeValue> codeValueList = baseMapper.selectList(Wrapper);

        if (!codeValueList.isEmpty() && codeValueList.size() > 1) {
            throw new BaseException("有重复的代码项值>>" + codeName);
        }

        return codeValueList.get(0).getItemText();
    }

    @Override
    public void cacheAllCodeValue() {
        List<FrameCodeValuePojo> frameCodeValueList = getCodeValueToCache();
        Map<String, List<FrameCodeValue>> dataMap = Maps.newHashMap();

        if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
            //筛选二维代码
            List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                    .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                    .collect(Collectors.toList());
            //@代码项转为Hash存储
            frameCodeValueList.forEach(frameCodeValue -> {
                //这里确保名称唯一
                List<FrameCodeValue> list = dataMap.get(frameCodeValue.getCodeNames());
                if (list == null || list.isEmpty()) {
                    list = Lists.newLinkedList();
                }
                //若此代码为二维代码的父类
                if ("".equals(frameCodeValue.getParentGuid())) {
                    //获取他的子类
                    List<FrameCodeValuePojo> frameCodeValueChildrenList;
                    frameCodeValueChildrenList = multCodeList.stream()
                            .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                            .collect(Collectors.toList());
                    List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);
                    if (!frameCodeValueGrandSon.isEmpty() && frameCodeValueGrandSon.size() > 0) {
                        frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                        frameCodeValue.setHasChildren(true);
                    } else {
                        frameCodeValue.setFrameCodeValueList(null);
                        frameCodeValue.setHasChildren(false);
                    }
                }
                //若遍历到子类，忽略
                if (!StringUtils.isBlank(frameCodeValue.getParentGuid())) {

                } else {
                    list.add(frameCodeValue);
                    if (frameCodeValue.getCodeNames() != null) {
                        dataMap.put(frameCodeValue.getCodeNames(), list);
                    }
                }
            });

            //@存入redis
//            HashOperations<String, String, List<FrameCodeValue>> hashOperations = redisTemplate.opsForHash();
//            hashOperations.putAll(projectName + ":code_value", dataMap);
        }

//        redisService.del(projectName+":code_value");
        redisService.hmset(projectName + ":code_value", JSON.parseObject(JSON.toJSONString(dataMap)));
        logger.info("codeValue存入redis");

    }

    @Override
    public List<FrameCodeValuePojo> getCodeValueToCache() {
        return baseMapper.getCodeValueToCache();
    }

    /**
     * 获取子类的子类(递归)
     *
     * @param childrenList 子类
     * @param allMultiList 所有二维代码数据
     * @return list
     */
    private List<FrameCodeValuePojo> getChildrenToChildren(List<FrameCodeValuePojo> childrenList, List<FrameCodeValuePojo> allMultiList) {
        //遍历子类的子类
        for (FrameCodeValuePojo children : childrenList) {
            List<FrameCodeValuePojo> frameCodeValueChildrenList = new ArrayList<>();
            //筛选数据，获取所有数据中子类ParentGuid等于父类CodeLevel的
            frameCodeValueChildrenList = allMultiList.stream()
                    .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(children.getRowGuid()))
                    .collect(Collectors.toList());
            children.setFrameCodeValueList(frameCodeValueChildrenList);
            if (!frameCodeValueChildrenList.isEmpty() && frameCodeValueChildrenList.size() > 0) {
                children.setFrameCodeValueList(frameCodeValueChildrenList);
                children.setHasChildren(true);
            } else {
                children.setFrameCodeValueList(null);
                children.setHasChildren(false);
            }
            // 递归查询
            getChildrenToChildren(frameCodeValueChildrenList, allMultiList);
        }
        return childrenList;
    }

    @Override
    public void cacheCodesMapByValue(String rowGuid) {
        try {
            String codeName = getCodeNameByGuid(rowGuid);
            List<FrameCodeValuePojo> frameCodeValueList = getValueByCodeName(codeName);
            // 若还有代码项值存在
            if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
                // 是否为多维代码标识
                boolean isMultidimensional = false;
                List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                        .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                        .collect(Collectors.toList());
                if (multCodeList.size() > 0) {
                    isMultidimensional = true;
                }
                //新建多维列表
                List<FrameCodeValuePojo> dimensionaCodeList = new ArrayList<>();
                Map<String, List<FrameCodeValuePojo>> dataMap = Maps.newHashMap();
                //@:代码项转为Hash存储
                frameCodeValueList.forEach(frameCodeValue -> {
                    //这里确保名称唯一
                    //若此代码为二维代码的父类
                    if ("".equals(frameCodeValue.getParentGuid())) {
                        //获取他的子类
                        List<FrameCodeValuePojo> frameCodeValueChildrenList;
                        frameCodeValueChildrenList = multCodeList.stream()
                                .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                                .collect(Collectors.toList());
                        List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);

                        if (!frameCodeValueGrandSon.isEmpty() && frameCodeValueGrandSon.size() > 0) {
                            frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                            frameCodeValue.setHasChildren(true);
                        } else {
                            frameCodeValue.setFrameCodeValueList(null);
                            frameCodeValue.setHasChildren(false);
                        }
                        dimensionaCodeList.add(frameCodeValue);
                    }
                    //若遍历到子类，忽略

                });
                if (isMultidimensional) {
                    dataMap.put(codeName, dimensionaCodeList);
                } else {
                    dataMap.put(codeName, frameCodeValueList);
                }
                //@:存入redis
                redisService.hmset(projectName + ":code_value", JSON.parseObject(JSON.toJSONString(dataMap)));
                //HashOperations<String, String, List<FrameCodeValuePojo>> hashOperations = redisTemplate.opsForHash();
                //hashOperations.putAll(projectName + ":code_value", JSON.parseObject(JSON.toJSONString(dataMap)));
                logger.info("更新codeValue存入redis");
            } else {
                //删没了就全删了
                redisService.del(projectName + ":code_value" + ":" + codeName);
//                HashOperations<String, String, List<FrameCodeValuePojo>> hashOperations = redisTemplate.opsForHash();
//                hashOperations.delete(projectName + ":code_value" + ":" + codeName);
            }
        } catch (Exception e) {
            logger.error("根据详细代码项名称更新存储失败", e.fillInStackTrace());
        }
    }

    @Override
    public List<FrameCodeValuePojo> getValueByCodeName(String codeName) {
        return baseMapper.getValueByCodeName(codeName);
    }

    @Override
    public String getCodeNameByGuid(String rowGuid) {
        return baseMapper.getCodeNameByGuid(rowGuid);
    }

    @Override
    public List<FrameCodeValuePojo> getCacheByCodeName(String codeName) {
        List<FrameCodeValuePojo> temp = null;
        try {
            Object object = redisService.hget(projectName + ":code_value", codeName);
            if (object != null) {
                //防止类对应失败
                JSONArray jsonArray = JSON.parseArray(object.toString());
                temp = jsonArray.toJavaList(FrameCodeValuePojo.class);
            } else {
                logger.error("从缓存中获取codeValue失败,代码项名称：{}", codeName);
            }
        } catch (Exception e) {
            logger.error("从缓存中获取codeValue失败", e.fillInStackTrace());
        }
        return temp;
    }

    /**
     * 代码项遍历赋值
     *
     * @param codeName
     * @param value
     * @return
     */
    @Override
    public String getItemTextByValue(String codeName, Object value) {
        List<FrameCodeValuePojo> frameCodeValueList = getCacheByCodeName(codeName);
        if (frameCodeValueList != null) {
            for (FrameCodeValue frameCodeValue : frameCodeValueList) {
                if (frameCodeValue.getItemValue().equals(value)) {
                    return frameCodeValue.getItemText();
                }
            }
        }
        return null;
    }

    @Override
    public void deleteCodeValue(String rowGuid) {
        QueryWrapper<FrameCodeValue> wrapper = new QueryWrapper<>();
        wrapper.eq("code_guid", rowGuid);
        frameCodeValueDao.delete(wrapper);
    }

    @Override
    public void cacheCodesMapByName(String codeName) {
        try {
            List<FrameCodeValuePojo> frameCodeValueList = getValueByCodeName(codeName);
            // 若还有代码项值存在
            if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
                // 是否为多维代码标识
                boolean isMultidimensional = false;
                List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                        .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                        .collect(Collectors.toList());
                if (multCodeList.size() > 0) {
                    isMultidimensional = true;
                }
                //新建多维列表
                List<FrameCodeValuePojo> dimensionaCodeList = new ArrayList<>();
                Map<String, List<FrameCodeValuePojo>> dataMap = Maps.newHashMap();
                //@:代码项转为Hash存储
                frameCodeValueList.forEach(frameCodeValue -> {
                    //这里确保名称唯一
                    //若此代码为二维代码的父类
                    if ("".equals(frameCodeValue.getParentGuid())) {
                        //获取他的子类
                        List<FrameCodeValuePojo> frameCodeValueChildrenList;
                        frameCodeValueChildrenList = multCodeList.stream()
                                .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                                .collect(Collectors.toList());
                        List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);

                        if (!frameCodeValueGrandSon.isEmpty() && frameCodeValueGrandSon.size() > 0) {
                            frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                            frameCodeValue.setHasChildren(true);
                        } else {
                            frameCodeValue.setFrameCodeValueList(null);
                            frameCodeValue.setHasChildren(false);
                        }

                        dimensionaCodeList.add(frameCodeValue);
                    }
                    //若遍历到子类，忽略

                });
                if (isMultidimensional) {
                    dataMap.put(codeName, dimensionaCodeList);
                } else {
                    dataMap.put(codeName, frameCodeValueList);
                }
                //@:存入redis
                redisService.hmset(projectName + ":code_value", JSON.parseObject(JSON.toJSONString(dataMap)));
                logger.info("更新codeValue存入redis");
            } else {
                //删没了就全删了
                redisService.del(projectName + ":code_value" + ":" + codeName);
//
            }
        } catch (Exception e) {
            logger.error("根据详细代码项名称更新存储失败", e.fillInStackTrace());
        }
    }

    @Override
    public void deleteCodesMapByName(String codeName) {
        redisService.del(projectName + ":code_value" + ":" + codeName);
    }

    @Override
    public void validDictDatas(String dictType, Collection<String> values) {
        if (CollUtil.isEmpty(values)) {
            return;
        }
        Map<String, FrameCodeValue> dictDataMap = CollectionUtils.convertMap(
                baseMapper.selectList("dict_type", dictType), FrameCodeValue::getItemValue);
        // 校验
        values.forEach(value -> {
            FrameCodeValue dictData = dictDataMap.get(value);
            if (dictData == null) {
                throw new BaseException("该代码项不存在");
            }
//            if (!CommonStatusEnum.ENABLE.getStatus().equals(dictData.getStatus())) {
//                throw exception(DICT_DATA_NOT_ENABLE, dictData.getLabel());
//            }
        });
    }
}
