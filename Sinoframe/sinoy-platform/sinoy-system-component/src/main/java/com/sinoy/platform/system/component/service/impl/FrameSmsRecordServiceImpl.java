package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameSmsRecordDao;
import com.sinoy.platform.system.component.entity.FrameSmsRecord;
import com.sinoy.platform.system.component.service.FrameSmsRecordService;
import org.springframework.stereotype.Service;

@Service
public class FrameSmsRecordServiceImpl extends BaseServiceImpl<FrameSmsRecordDao, FrameSmsRecord> implements FrameSmsRecordService {

    @Override
    public int saveSmsRecord(FrameSmsRecord frameSmsRecord) {
       return this.baseMapper.insert(frameSmsRecord);
    }
}
