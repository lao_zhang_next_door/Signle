package com.sinoy.platform.system.component.entity;


import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 */
public class FrameSchedule extends BaseEntity {

	/**
	 * @param init true则有默认值
	 */
	public FrameSchedule(boolean init) {
		super(init);
	}

	public FrameSchedule(String rowGuid) {
		super(rowGuid);
	}

	public FrameSchedule() {}


	/**
	 *待办事项标题
	 */
	private String schTitle;


	/**
	 *待办事项发起人
	 */
	private String schOriginator;


	/**
	 *待办事项接收人
	 */
	private String schReceiver;


	/**
	 *是否已被查看
	 */
	private String schViewed;


	/**
	 *点击去处理的路由地址
	 */
	private String schTargetUrl;


	/**
	 *待办事项类型
	 */
	private String schType;

	/**
	 * 关联表guid
	 */
	private String schEventGuid;

	/**
	 * 接收人部门
	 */
	private Long schReceiverDept;

	public Long getSchReceiverDept() {
		return schReceiverDept;
	}

	public void setSchReceiverDept(Long schReceiverDept) {
		this.schReceiverDept = schReceiverDept;
	}

	public String getSchEventGuid() {
		return schEventGuid;
	}

	public FrameSchedule setSchEventGuid(String schEventGuid) {
		this.schEventGuid = schEventGuid;
		return this;
	}


	/**
	 * 设置：待办事项标题
	 */
	public FrameSchedule setSchTitle(String schTitle) {
		this.schTitle = schTitle;return this;
	}
	/**
	 * 获取：待办事项标题
	 */
	public String getSchTitle() {
		return schTitle;
	}
	/**
	 * 设置：待办事项发起人
	 */
	public FrameSchedule setSchOriginator(String schOriginator) {
		this.schOriginator = schOriginator;return this;
	}
	/**
	 * 获取：待办事项发起人
	 */
	public String getSchOriginator() {
		return schOriginator;
	}
	/**
	 * 设置：待办事项接收人
	 */
	public FrameSchedule setSchReceiver(String schReceiver) {
		this.schReceiver = schReceiver;return this;
	}
	/**
	 * 获取：待办事项接收人
	 */
	public String getSchReceiver() {
		return schReceiver;
	}
	/**
	 * 设置：是否已被查看
	 */
	public FrameSchedule setSchViewed(String schViewed) {
		this.schViewed = schViewed;return this;
	}
	/**
	 * 获取：是否已被查看
	 */
	public String getSchViewed() {
		return schViewed;
	}
	/**
	 * 设置：点击去处理的路由地址
	 */
	public FrameSchedule setSchTargetUrl(String schTargetUrl) {
		this.schTargetUrl = schTargetUrl;return this;
	}
	/**
	 * 获取：点击去处理的路由地址
	 */
	public String getSchTargetUrl() {
		return schTargetUrl;
	}
	/**
	 * 设置：待办事项类型
	 */
	public FrameSchedule setSchType(String schType) {
		this.schType = schType;return this;
	}
	/**
	 * 获取：待办事项类型
	 */
	public String getSchType() {
		return schType;
	}

}

