package com.sinoy.platform.system.component.module.system.service.rolelimit;

import java.util.*;
import javax.validation.*;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitPageReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitUpdateReqVO;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;

/**
 * 角色权限 Service 接口
 *
 * @author 管理员
 */
public interface RoleLimitService extends BaseService<RoleLimitDO>{

    /**
     * 创建角色权限
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createRoleLimit(@Valid RoleLimitCreateReqVO createReqVO);

    /**
     * 更新角色权限
     *
     * @param updateReqVO 更新信息
     */
    void updateRoleLimit(@Valid RoleLimitUpdateReqVO updateReqVO);

    /**
     * 删除角色权限
     *
     * @param id 编号
     */
    void deleteRoleLimit(Long id);

    /**
     * 获得角色权限
     *
     * @param id 编号
     * @return 角色权限
     */
    RoleLimitDO getRoleLimit(Long id);

    /**
     * 获得角色权限列表
     *
     * @param ids 编号
     * @return 角色权限列表
     */
    List<RoleLimitDO> getRoleLimitList(Collection<Long> ids);

    /**
     * 获得角色权限分页
     *
     * @param pageReqVO 分页查询
     * @return 角色权限分页
     */
    Page<RoleLimitDO> getRoleLimitPage(RoleLimitPageReqVO pageReqVO);


}
