package com.sinoy.platform.system.component.entity.dto;


import com.sinoy.platform.system.component.entity.FrameModule;

public class FrameModuleDto extends FrameModule {

	/**
	 * 父级名称
	 */
	private String parentModuleName;

	/**
	 * 父级guid
	 */
	private String parentModuleGuid;

	/**
	 * 父级code
	 */
	private String parentModuleCode;

	public String getParentModuleName() {
		return parentModuleName;
	}

	public FrameModuleDto setParentModuleName(String parentModuleName) {
		this.parentModuleName = parentModuleName;
		return this;
	}

	public String getParentModuleGuid() {
		return parentModuleGuid;
	}

	public FrameModuleDto setParentModuleGuid(String parentModuleGuid) {
		this.parentModuleGuid = parentModuleGuid;
		return this;
	}

	public String getParentModuleCode() {
		return parentModuleCode;
	}

	public FrameModuleDto setParentModuleCode(String parentModuleCode) {
		this.parentModuleCode = parentModuleCode;
		return this;
	}
}
