package com.sinoy.platform.system.component.base;


import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import com.sinoy.platform.system.component.utils.SpringContextUtils;

import javax.persistence.Transient;

public class BaseEntity extends com.sinoy.core.database.mybatisplus.entity.BaseEntity {

	public BaseEntity(){}

	public BaseEntity(boolean init) {
		super(init);
	}

	public BaseEntity(String rowGuid) {
		super(rowGuid);
	}

	@Transient
	@TableField(exist = false)
	protected FrameCodeValueService codeValueService = SpringContextUtils.getBean(FrameCodeValueService.class);

}
