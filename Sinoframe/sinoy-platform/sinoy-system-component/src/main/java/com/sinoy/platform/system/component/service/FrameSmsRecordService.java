package com.sinoy.platform.system.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameSmsRecord;

public interface FrameSmsRecordService extends BaseService<FrameSmsRecord> {

    /**
     * 新增短信
     * @param frameSmsRecord
     * @return FrameSms
     */
    int saveSmsRecord(FrameSmsRecord frameSmsRecord);
}
