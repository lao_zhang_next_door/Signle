package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.FrameModulePermission;
import com.sinoy.platform.system.component.entity.dto.FrameModulePermissionDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FrameModulePermissionDao extends ExtendMapper<FrameModulePermission> {

    /**
     * 联查role表 获取roleName,roleFlag
     *
     * @return
     */
    List<FrameModulePermissionDto> selectLJRole(String moduleGuid);

    /**
     * 根据角色Guid联查模块权限
     *
     * @param moduleType
     * @param roleGuids
     * @return
     */
    List<FrameModule> selectModulesFromRoleGuid(@Param("moduleType") Integer moduleType, @Param("roleGuids") String[] roleGuids);
}
