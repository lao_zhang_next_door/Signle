package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.file.FileUtil;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.oss.core.client.FileClient;
import com.sinoy.core.oss.core.utils.FileTypeUtils;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.platform.system.component.dao.FrameAttachDao;
import com.sinoy.platform.system.component.entity.FrameAttach;
import com.sinoy.platform.system.component.service.FrameAttachConfigService;
import com.sinoy.platform.system.component.service.FrameAttachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wzl
 */
@Service
public class FrameAttachServiceImpl extends BaseServiceImpl<FrameAttachDao, FrameAttach> implements FrameAttachService {

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

    @Autowired
    private FrameAttachConfigService frameAttachConfigService;

    @Override
    public FrameAttach createFile(String name, String path, byte[] content,String formRowGuid) {
        // 计算默认的 path 名
        String type = FileTypeUtils.getMineType(content, name);
        if (StrUtil.isEmpty(path)) {
            path = FileUtil.generatePath(content, name);
        }
        // 如果 name 为空，则使用 path 填充
        if (StrUtil.isEmpty(name)) {
            name = path;
        }

        //前面补全 /
        path = "/" + path;

        // 上传到文件存储器
        FileClient client = frameAttachConfigService.getMasterFileClient();
        Assert.notNull(client, "客户端(master) 不能为空");
        String url = null;
        try {
            url = client.upload(content, path);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException("上传文件异常");
        }

        // 保存到数据库
        FrameAttach frameAttach = new FrameAttach(true);
        frameAttach.setConfigId(client.getId());
        frameAttach.setAttachName(name);
        frameAttach.setContentType(type);
        frameAttach.setUrl(url);
        frameAttach.setContentUrl(path);
        frameAttach.setContentLength(content.length);
        frameAttach.setFormRowGuid(formRowGuid);
        frameAttach.setUploadUserGuid(commonPropAndMethods.getUserGuid());
        save(frameAttach);
        return frameAttach;
    }

    @Override
    public void deleteFile(Long id) throws Exception {
        // 校验存在
        FrameAttach file = this.validateFileExists(id);

        // 从文件存储器中删除
        FileClient client = frameAttachConfigService.getFileClient(file.getConfigId());
        Assert.notNull(client, "客户端({}) 不能为空", file.getConfigId());
        client.delete(file.getContentUrl());

        // 删除记录
        baseMapper.deleteById(id);
    }

    @Override
    public void deleteFileByFormRowGuid(String formRowGuid) {
        QueryWrapper<FrameAttach> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("form_row_guid",formRowGuid);
        List<FrameAttach> attachList = baseMapper.selectList(queryWrapper);

        attachList.forEach(file -> {
            // 从文件存储器中删除
            FileClient client = frameAttachConfigService.getFileClient(file.getConfigId());
            Assert.notNull(client, "客户端({}) 不能为空", file.getConfigId());
            try {
                client.delete(file.getContentUrl());
            } catch (Exception e) {
                e.printStackTrace();
            }

            // 删除记录
            baseMapper.deleteByGuid(file.getRowGuid());
        });
    }

    @Override
    public void deleteFileByRowGuids(String[] rowGuids) {

        for (String rowGuid : rowGuids) {
            FrameAttach file = baseMapper.selectByGuid(rowGuid);
            if (file == null) {
                continue;
            }
            // 从文件存储器中删除
            FileClient client = frameAttachConfigService.getFileClient(file.getConfigId());
            Assert.notNull(client, "客户端({}) 不能为空", file.getConfigId());
            try {
                client.delete(file.getContentUrl());
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        // 删除记录
        baseMapper.deleteBatchByGuids(rowGuids);
    }

    private FrameAttach validateFileExists(Long id) {
        FrameAttach fileDO = baseMapper.selectById(id);
        if (fileDO == null) {
            throw new BaseException("文件不存在");
        }
        return fileDO;
    }

    @Override
    public byte[] getFileContent(Long configId, String path) throws Exception {
        FileClient client = frameAttachConfigService.getFileClient(configId);
        Assert.notNull(client, "客户端({}) 不能为空", configId);
        return client.getContent(path);
    }
}
