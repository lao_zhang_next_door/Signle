//package com.sino.platform.system.component.strategy;
//
//import cn.hutool.core.convert.Convert;
//import org.springframework.web.context.request.RequestAttributes;
//import org.springframework.web.context.request.RequestContextHolder;
//
///**
// * 数据权限单例上下文
// */
//public class PermissionContextHolder {
//
//    private static final String PERMISSION_CONTEXT_ATTRIBUTES = "PERMISSION_CONTEXT";
//
//    public static void setContext(String permission)
//    {
//        RequestContextHolder.currentRequestAttributes().setAttribute(PERMISSION_CONTEXT_ATTRIBUTES, permission,
//                RequestAttributes.SCOPE_REQUEST);
//    }
//
//    public static String getContext()
//    {
//        return Convert.toStr(RequestContextHolder.currentRequestAttributes().getAttribute(PERMISSION_CONTEXT_ATTRIBUTES,
//                RequestAttributes.SCOPE_REQUEST));
//    }
//
//}
