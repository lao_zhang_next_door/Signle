package com.sinoy.platform.system.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.entity.pojo.FrameCodeValuePojo;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author wzl
 */
public interface FrameCodeValueService extends BaseService<FrameCodeValue> {

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    List<FrameCodeValuePojo> selectParentCode(String rowGuid);

    /**
     * 根据codeName 以及对应的value 值返回对应的文本
     *
     * @param codeName
     * @param codeValue
     * @return
     */
    String getItemTextByCodeNameAndCodeValue(String codeName, String codeValue);

    /**
     * 缓存所有代码项：方式HashList
     */
    void cacheAllCodeValue();

    /**
     * 获取所有代码项
     *
     * @return list
     */
    List<FrameCodeValuePojo> getCodeValueToCache();

    /**
     * 按rowGuid更新缓存代码
     *
     * @param rowGuid
     */
    void cacheCodesMapByValue(String rowGuid);

    /**
     * 根据codeName更新
     *
     * @param codeName
     */
    void cacheCodesMapByName(String codeName);

    /**
     * 根据codeName删除
     *
     * @param codeName
     */
    void deleteCodesMapByName(String codeName);

    /**
     * 根据代码项名称查询
     *
     * @param codeName
     * @return
     */
    List<FrameCodeValuePojo> getValueByCodeName(String codeName);

    /**
     * 按guid查询代码项名称
     *
     * @param rowGuid
     * @return
     */
    String getCodeNameByGuid(String rowGuid);

    /**
     * 根据codeName获取缓存
     *
     * @param codeName
     * @return list
     */
    List<FrameCodeValuePojo> getCacheByCodeName(String codeName);

    /**
     * 通过代码项值获取代码项文本
     *
     * @param codeName
     * @param value
     * @return
     */
    String getItemTextByValue(String codeName, Object value);

    /**
     * 根据rowGuid 删除
     * @param rowGuid
     */
    void deleteCodeValue(String rowGuid);

    /**
     * 校验字典数据们是否有效。如下情况，视为无效：
     * 1. 字典数据不存在
     * 2. 字典数据被禁用
     *
     * @param dictType 字典类型
     * @param values 字典数据值的数组
     */
    void validDictDatas(String dictType, Collection<String> values);
}
