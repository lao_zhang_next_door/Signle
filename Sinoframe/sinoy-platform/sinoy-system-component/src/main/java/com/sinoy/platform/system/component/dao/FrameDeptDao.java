package com.sinoy.platform.system.component.dao;

import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;

import java.util.List;

public interface FrameDeptDao extends ExtendMapper<FrameDept> {

    /**
     * 根据rowGuid 联查(left join frameDept) 查询上级deptName
     * @param rowGuid
     * @return
     */
    FrameDeptDto selectDeptAndParentDept(String rowGuid);

    /**
     * 查询修改前这个部门code下的所有子集部门
     * @param deptCode
     * @return
     */
    List<FrameDept> getChildDeptByPdeptCode(String deptCode);

    /**
     * 级联删除部门与用户
     */
    void deleteDeptAndUser(String deptCode);

    /**
     * 获得指定状态的部门列表
     * @param status
     * @return
     */
    default List<FrameDept> getSimpleDepts(Character status){
        return selectList(FrameDept::getDeptStatus,status,FrameDept::getDelFlag, DelFlag.Normal.getCode());
    };

    List<FrameDept> getDeptsByUserIdForTel(Long userId);


}
