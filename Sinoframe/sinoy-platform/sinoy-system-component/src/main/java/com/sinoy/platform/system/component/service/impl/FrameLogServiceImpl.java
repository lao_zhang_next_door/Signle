package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameLogDao;
import com.sinoy.platform.system.component.entity.FrameLog;
import com.sinoy.platform.system.component.service.FrameLogService;
import org.springframework.stereotype.Service;

@Service
public class FrameLogServiceImpl extends BaseServiceImpl<FrameLogDao, FrameLog> implements FrameLogService {


}

