package com.sinoy.platform.system.component.enumeration;

/**
 * 岗位枚举
 */
public enum PostEnum {

    Leader(1,"领导"),
    DISABLE(0,"文员");

    private final int code;
    private final String value;
    private PostEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }
    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
