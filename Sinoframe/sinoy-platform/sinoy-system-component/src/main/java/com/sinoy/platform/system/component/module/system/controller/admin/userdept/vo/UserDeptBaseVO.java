package com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

/**
* 多部门关联 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里
*/
@Data
public class UserDeptBaseVO {

    //编号
    @NotNull(message = "编号不能为空")
    private Long rowId;


    //创建者
    private String creator;


    //更新者
    private String updater;


    //关联部门id
    private Long deptId;


    //关联用户id
    private Long userId;


    //是否主部门
    private Integer mainDept;


}
