package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostPageReqVO extends PageParam {

    //岗位编码
    private String code;

    //岗位名称
    private String name;

    //展示状态
    private Integer status;

}
