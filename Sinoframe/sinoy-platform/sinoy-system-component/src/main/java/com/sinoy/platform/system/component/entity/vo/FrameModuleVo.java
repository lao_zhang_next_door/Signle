package com.sinoy.platform.system.component.entity.vo;

import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.platform.system.component.entity.FrameModule;

/**
 * 菜单排序
 */
public class FrameModuleVo {

    private FrameModule frameModule;

    private Sort sort;

    /**
     * 是否需要guids数组
     */
    private boolean needList;

    public FrameModule getFrameModule() {
        return frameModule;
    }

    public void setFrameModule(FrameModule frameModule) {
        this.frameModule = frameModule;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public boolean isNeedList() {
        return needList;
    }

    public void setNeedList(boolean needList) {
        this.needList = needList;
    }
}
