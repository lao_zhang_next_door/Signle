package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameModulePermissionDao;
import com.sinoy.platform.system.component.entity.FrameModulePermission;
import com.sinoy.platform.system.component.entity.dto.FrameModulePermissionDto;
import com.sinoy.platform.system.component.service.FrameModulePermissionService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FrameModulePermissionServiceImpl extends BaseServiceImpl<FrameModulePermissionDao, FrameModulePermission> implements FrameModulePermissionService {


    @Override
    public List<FrameModulePermissionDto> selectLJRole(String moduleGuid) {
        return baseMapper.selectLJRole(moduleGuid);
    }
}
