//package com.sino.platform.system.component.strategy;
//
//import com.sino.platform.system.component.service.FrameDeptService;
//import org.springframework.stereotype.Component;
//
///**
// * 自定义数据范围
// *
// */
//@Component("5")
//public class CustomizeDataScope implements AbstractDataScopeHandler {
//
//	private final FrameDeptService sysDepartService;
//
//	public CustomizeDataScope(FrameDeptService sysDepartService) {
//		this.sysDepartService = sysDepartService;
//	}
//
////	@Override
////	public List<String> getDeptIds(RoleDto roleDto, DataScopeTypeEnum dataScopeTypeEnum) {
////		List<Long> roleDeptIds = roleDto.getRoleDepts();
////		List<Long> ids = new ArrayList<>();
////		for (Long deptId : roleDeptIds) {
////			ids.addAll(sysDepartService.selectDeptIds(deptId));
////		}
////		Set<Long> set = new HashSet<>(ids);
////		ids.clear();
////		ids.addAll(set);
////		return ids;
////	}
//}
