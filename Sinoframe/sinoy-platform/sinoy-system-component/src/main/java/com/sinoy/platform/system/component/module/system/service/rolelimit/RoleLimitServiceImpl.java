package com.sinoy.platform.system.component.module.system.service.rolelimit;

import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitPageReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitUpdateReqVO;
import com.sinoy.platform.system.component.module.system.convert.rolelimit.RoleLimitConvert;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import com.sinoy.platform.system.component.module.system.dal.mysql.rolelimit.RoleLimitMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.sql.BatisUtil;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.*;


import com.sinoy.core.common.utils.exception.BaseException;

/**
 * 角色权限 Service 实现类
 *
 * @author 管理员
 */
@Service
@Validated
public class RoleLimitServiceImpl extends BaseServiceImpl<RoleLimitMapper,RoleLimitDO> implements RoleLimitService {

    @Resource
    private RoleLimitMapper roleLimitMapper;

    @Override
    public Long createRoleLimit(RoleLimitCreateReqVO createReqVO) {
        //首先要判断是否重复插入
        QueryWrapper<RoleLimitDO> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("role_id",createReqVO.getRoleId());
        List<RoleLimitDO> list=roleLimitMapper.selectList(queryWrapper);
        if(list.size()>0)
        {
            return (long)-1;
        }
        // 插入
        RoleLimitDO roleLimit = RoleLimitConvert.INSTANCE.convert(createReqVO);
        roleLimit.initNull();
        roleLimitMapper.insert(roleLimit);
        // 返回
        return roleLimit.getRowId();
    }

    @Override
    public void updateRoleLimit(RoleLimitUpdateReqVO updateReqVO) {
        // 校验存在
        this.validateRoleLimitExists(updateReqVO.getRowId());
        // 更新
        RoleLimitDO updateObj = RoleLimitConvert.INSTANCE.convert(updateReqVO);
        roleLimitMapper.updateById(updateObj);
    }

    @Override
    public void deleteRoleLimit(Long id) {
        // 校验存在
        this.validateRoleLimitExists(id);
        // 删除
        roleLimitMapper.deleteById(id);
    }

    private void validateRoleLimitExists(Long id) {
        if (roleLimitMapper.selectById(id) == null) {
                        throw new BaseException("该条记录不存在");
        }
    }

    @Override
    public RoleLimitDO getRoleLimit(Long id) {
        return roleLimitMapper.selectById(id);
    }

    @Override
    public List<RoleLimitDO> getRoleLimitList(Collection<Long> ids) {
        return roleLimitMapper.selectBatchIds(ids);
    }

    @Override
    public Page<RoleLimitDO> getRoleLimitPage(RoleLimitPageReqVO pageReqVO) {
        Page<RoleLimitDO> page = new Page(pageReqVO.getCurrentPage(),pageReqVO.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        BatisPlusUtil.setOrders(queryWrapper,null,null);
        BatisPlusUtil.checkParamsIncluded(queryWrapper,pageReqVO,pageReqVO.getParams());
        return roleLimitMapper.selectPage(page,queryWrapper);
    }


}
