package com.sinoy.platform.system.component.entity;

import com.sinoy.platform.system.component.base.BaseEntity;

public class FrameModulePermission extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public FrameModulePermission(boolean init) {
        super(init);
    }

    public FrameModulePermission() {}

    /**
     * 模块guid
     */
    private String moduleGuid;

    /**
     * 所属角色或其他guid
     */
    private String allowTo;

    /**
     * 所属类型
     */
    private String allowType;

    public String getModuleGuid() {
        return moduleGuid;
    }

    public void setModuleGuid(String moduleGuid) {
        this.moduleGuid = moduleGuid;
    }

    public String getAllowTo() {
        return allowTo;
    }

    public void setAllowTo(String allowTo) {
        this.allowTo = allowTo;
    }

    public String getAllowType() {
        return allowType;
    }

    public void setAllowType(String allowType) {
        this.allowType = allowType;
    }
}
