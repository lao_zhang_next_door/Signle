package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.oss.core.client.FileClient;
import com.sinoy.core.oss.core.client.FileClientFactory;
import com.sinoy.platform.system.component.dao.FrameAttachConfigDao;
import com.sinoy.platform.system.component.entity.FrameAttachConfig;
import com.sinoy.platform.system.component.service.FrameAttachConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Validated
public class FrameAttachConfigServiceImpl extends BaseServiceImpl<FrameAttachConfigDao, FrameAttachConfig> implements FrameAttachConfigService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 定时执行 {@link #schedulePeriodicRefresh()} 的周期
     * 因为已经通过 Redis Pub/Sub 机制，所以频率不需要高
     */
    private static final long SCHEDULER_PERIOD = 5 * 60 * 1000L;

    /**
     * 缓存菜单的最大更新时间，用于后续的增量轮询，判断是否有更新
     */
    private volatile LocalDateTime maxUpdateTime;

    @Resource
    private FileClientFactory fileClientFactory;
    /**
     * Master FileClient 对象，有且仅有一个
     */
    private FileClient masterFileClient;

    public LocalDateTime getMaxUpdateTime() {
        return maxUpdateTime;
    }

    public FileClientFactory getFileClientFactory() {
        return fileClientFactory;
    }

    /**
     * 初始化文件配置
     */
    @Override
    @PostConstruct
    public void initFileClients() {
        // 获取文件配置，如果有更新
        List<FrameAttachConfig> configs = loadFileConfigIfUpdate(maxUpdateTime);
        if (CollUtil.isEmpty(configs)) {
            return;
        }

        // 创建或更新 Client
        configs.forEach(config -> {
            fileClientFactory.createOrUpdateFileClient(config.getRowId(), config.getStorage(), config.getConfig());
            // 如果是 master，进行设置
            if (Boolean.TRUE.equals(config.getMaster())) {
                masterFileClient = fileClientFactory.getFileClient(config.getRowId());
            }
        });

        // 写入缓存
        maxUpdateTime = CollectionUtils.getMaxValue(configs, FrameAttachConfig::getUpdateTime);
        log.info("[initFileClients][初始化 FileConfig 数量为 {}]", configs.size());
    }

    @Scheduled(fixedDelay = SCHEDULER_PERIOD, initialDelay = SCHEDULER_PERIOD)
    public void schedulePeriodicRefresh() {
        this.initFileClients();
    }

    /**
     * 如果文件配置发生变化，从数据库中获取最新的全量文件配置。
     * 如果未发生变化，则返回空
     *
     * @param maxUpdateTime 当前文件配置的最大更新时间
     * @return 文件配置列表
     */
    private List<FrameAttachConfig> loadFileConfigIfUpdate(LocalDateTime maxUpdateTime) {
        // 第一步，判断是否要更新。
        if (maxUpdateTime == null) { // 如果更新时间为空，说明 DB 一定有新数据
            log.info("[loadFileConfigIfUpdate][首次加载全量文件配置]");
        } else { // 判断数据库中是否有更新的文件配置
            if (baseMapper.selectCountByUpdateTimeGt(maxUpdateTime) == 0) {
                return null;
            }
            log.info("[loadFileConfigIfUpdate][增量加载全量文件配置]");
        }
        // 第二步，如果有更新，则从数据库加载所有文件配置
        return this.list();
    }

    @Override
    public FileClient getMasterFileClient() {
        return this.masterFileClient;
    }

    @Override
    public FileClient getFileClient(Long id) {
        return fileClientFactory.getFileClient(id);
    }

    @Override
    public JSONObject getFileConfig() {
        FrameAttachConfig frameAttachConfig = baseMapper.selectById(masterFileClient.getId());
        if(frameAttachConfig.getConfig() == null){
            throw new BaseException("配置异常");
        }
        JSONObject obj = JSON.parseObject(JSON.toJSONString(frameAttachConfig.getConfig()));
        return obj;
    }

    /**
     * 更新文件配置
     * @param frameAttachConfig
     */
    @Override
    public void updateFileAttachConfig(FrameAttachConfig frameAttachConfig) {
        UpdateWrapper<FrameAttachConfig> wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", frameAttachConfig.getRowGuid());
        wrapper.set("update_time",LocalDateTime.now());
        update(frameAttachConfig, wrapper);

        // 初始化文件配置
        this.initFileClients();
    }

    /**
     * 更新为主配置
     * @param masterRowGuid
     */
    @Override
    public void updateMaster(String masterRowGuid) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("master",true);
        List<FrameAttachConfig> frameAttachConfigs = list(queryWrapper);
        if(frameAttachConfigs.size() > 1){
            throw new BaseException("主配置变更异常,只能存在至多一个主配置");
        }

        if(frameAttachConfigs.size() == 1 && masterRowGuid.equals(frameAttachConfigs.get(0).getRowGuid())){
            throw new BaseException("无需变更配置");
        }

        //更新老配置
        UpdateWrapper<FrameAttachConfig> wrapper = new UpdateWrapper<>();
        wrapper.eq("master", true);
        wrapper.set("master",false);
        wrapper.set("update_time",LocalDateTime.now());
        update(wrapper);

        //变更新配置
        wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", masterRowGuid);
        wrapper.set("master",true);
        wrapper.set("update_time",LocalDateTime.now());
        update(wrapper);

        // 初始化文件配置
        this.initFileClients();
    }
}
