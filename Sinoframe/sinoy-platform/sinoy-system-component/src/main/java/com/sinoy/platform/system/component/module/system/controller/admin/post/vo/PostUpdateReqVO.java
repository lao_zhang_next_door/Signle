package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostUpdateReqVO extends PostBaseVO {

    //岗位编号
    @NotNull(message = "岗位编号不能为空")
    private Long rowId;

    //排序
    private Integer sortSq;

}
