package com.sinoy.platform.system.component.module.system.controller.admin.userdept;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.platform.system.component.entity.FrameDept;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;



import com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo.*;
import com.sinoy.platform.system.component.module.system.dal.dataobject.userdept.UserDeptDO;
import com.sinoy.platform.system.component.module.system.convert.userdept.UserDeptConvert;
import com.sinoy.platform.system.component.module.system.service.userdept.UserDeptService;

//管理后台 - 多部门关联
@RestController
@RequestMapping("/system/user-dept")
@Validated
public class UserDeptController {

    @Resource
    private UserDeptService userDeptService;

        //创建多部门关联
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('system:user-dept:create')")
    public R createUserDept(@Valid @RequestBody UserDeptCreateReqVO createReqVO) {
        return R.ok().put("data",userDeptService.createUserDept(createReqVO));
    }

        //更新多部门关联
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('system:user-dept:update')")
    public R updateUserDept(@Valid @RequestBody UserDeptUpdateReqVO updateReqVO) {
        userDeptService.updateUserDept(updateReqVO);
        return R.ok();
    }

            //删除多部门关联
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('system:user-dept:delete')")
    public R deleteUserDept(@RequestParam("rowId") Long id) {
        userDeptService.deleteUserDept(id);
        return R.ok();
    }

    //批量删除多部门关联
    @DeleteMapping("/deleteBatch")
    @PreAuthorize("@ss.hasPermission('system:user-dept:delete')")
    public R deleteUserDeptBatch(@RequestBody Long[] ids) {
        userDeptService.deleteBatchByIds(ids);
        return R.ok();
    }

            //获得多部门关联
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('system:user-dept:query')")
    public R getUserDept(@RequestParam("id") Long id) {
        UserDeptDO userDept = userDeptService.getUserDept(id);
                return R.ok().put("data",UserDeptConvert.INSTANCE.convert(userDept));
    }

            //获得多部门关联列表
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermission('system:user-dept:query')")
    public R getUserDeptList(@RequestParam("ids") Collection<Long> ids) {
        List<UserDeptDO> list = userDeptService.getUserDeptList(ids);
                return R.ok().put("data",UserDeptConvert.INSTANCE.convertList(list));
    }


        //获得多部门关联分页
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:user-dept:query')")
    public R getUserDeptPage(@Valid UserDeptPageReqVO pageVO) {
        Page<UserDeptDO> pageResult = userDeptService.getUserDeptPage(pageVO);
                return R.ok().put("data",UserDeptConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/getDeptsByUserId")
    public R getDeptsByUserId(@RequestParam("userId") Long userId) {
        List<FrameDept> deptDOS = userDeptService.getDeptsByUserId(userId);
        return R.ok().put("data",deptDOS);
    }

    @PutMapping("/switch-dept")
    public void switchDept(@RequestParam("deptId") Long deptId) {
        userDeptService.switchDept(deptId);

    }

    /**
     * 获取兼职部门
     * @param userId
     * @return
     */
    @GetMapping("/getJZDeptsByUserId")
    public R getJZDeptsByUserId(@RequestParam("userId") Long userId) {
        List<FrameDept> deptDOS = userDeptService.getJZDeptsByUserId(userId);
        return R.ok().put("data",deptDOS);
    }


}
