package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.FrameRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FrameRoleDao extends ExtendMapper<FrameRole> {

    /**
     * 根据角色guid 查询该角色所拥有的菜单模块
     * @param roleGuid
     * @return
     */
    List<FrameModule> getModuleByRoleGuid(String roleGuid);

    /**
     * 根据角色guid 组 获取数据权限数组
     * @param roleGuids
     * @return
     */
    List<String> selectDataScopeByRoleGuids(@Param("roleGuids") List<String> roleGuids);

    /**
     * 根据角色guid 获取部门列表
     * @param roleGuid
     * @return
     */
    List<FrameDept> getDeptByRoleGuid(String roleGuid);
}
