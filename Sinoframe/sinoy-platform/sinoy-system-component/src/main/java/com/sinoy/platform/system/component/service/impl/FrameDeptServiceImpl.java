package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.platform.system.component.dao.FrameDeptDao;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;
import com.sinoy.platform.system.component.entity.vo.FrameDeptVo;
import com.sinoy.platform.system.component.service.FrameDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Service
public class FrameDeptServiceImpl extends BaseServiceImpl<FrameDeptDao, FrameDept> implements FrameDeptService {


    @Override
    public FrameDeptDto selectDeptAndParentDept(String rowGuid) {
        return baseMapper.selectDeptAndParentDept(rowGuid);
    }

    @Override
    public JSONArray getStreetAndCommunity(String qGuid) {
        QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_guid", qGuid);
        List<FrameDept> streetList = baseMapper.selectList(queryWrapper);
        JSONArray array = new JSONArray();
        streetList.forEach(frameDept -> {
            QueryWrapper<FrameDept> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("parent_guid", frameDept.getRowGuid());
            List<FrameDept> communityList = baseMapper.selectList(queryWrapper1);
            JSONObject object = JSON.parseObject(JSON.toJSONString(frameDept));
            object.put("children", communityList);
            array.add(object);
        });

        return array;
    }

    @Override
    public FrameDept saveDept(FrameDept frameDept) {
        baseMapper.insert(frameDept);
        if (StringUtil.isBlank(frameDept.getParentGuid())) {
            frameDept.setParentGuid("");
            //说明是根节点
            frameDept.setDeptCode(String.valueOf(frameDept.getRowId()));
            //单位层级
            frameDept.setDeptLevel(1);
        } else {
            QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("row_guid", frameDept.getParentGuid());
            //子节点
            FrameDept parentDept = baseMapper.selectOne(queryWrapper);
            String deptCode = parentDept.getDeptCode() + "." + frameDept.getRowId();
            frameDept.setDeptCode(deptCode);
            //单位层级
            String[] strArr = deptCode.split("\\.");
            frameDept.setDeptLevel(strArr.length);
        }
        QueryWrapper<FrameDept> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", frameDept.getRowGuid());
        baseMapper.update(frameDept, wrapper);
        return frameDept;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FrameDept update(FrameDept frameDept) {

        String oldDeptCode = frameDept.getDeptCode();
        /**
         * 初始待修改的单位
         */
        QueryWrapper<FrameDept> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", frameDept.getRowGuid());
        FrameDept originDept = baseMapper.selectOne(wrapper);
        if (originDept == null) {
            throw new BaseException("修改异常,未查询到该单位或者部门");
        }

        String oldParentGuid = originDept.getParentGuid();
        String newParentGuid = frameDept.getParentGuid();

        String newDeptCode = "";
        if (StringUtil.isBlank(newParentGuid)) {
            /**
             * 根目录
             */
            newDeptCode = frameDept.getRowId().toString();
            frameDept.setDeptLevel(1);
        } else {
            /**
             * 需要修改到这个父节点下
             */
            QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("row_guid", newParentGuid);
            FrameDept newDept = baseMapper.selectOne(queryWrapper);
            if (newDept == null || StringUtil.isBlank(newDept.getDeptCode())) {
                throw new BaseException("修改异常,该选择的父节点部门不存在或者其部门编码异常");
            }
            newDeptCode = newDept.getDeptCode() + "." + frameDept.getRowId().toString();
            frameDept.setDeptLevel(newDeptCode.split("\\.").length);
        }
        frameDept.setDeptCode(newDeptCode);
        /**
         * 修改部门
         */
        QueryWrapper<FrameDept> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("row_guid", frameDept.getRowGuid());
        baseMapper.update(frameDept, updateWrapper);
        if (!oldParentGuid.equals(newParentGuid)) {
            this.updateChildDept(oldDeptCode, newDeptCode);
        }
        return frameDept;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDeptAndUser(String deptGuid) {
        baseMapper.deleteDeptAndUser(deptGuid);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDeptAndUserAndMore(String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameDept> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            wrapper.eq("del_flag", DelFlag.Normal.getCode());
            FrameDept frameDept = getOne(wrapper);
            if (frameDept != null) {
                if (StringUtil.isBlank(frameDept.getDeptCode())) {
                    throw new BaseException(frameDept.getDeptName() + "部门code为空");
                }
                //级联删除部门与用户
                baseMapper.deleteDeptAndUser(frameDept.getDeptCode());
            }
        }
    }

    @Override
    public JSONObject allTreeData(FrameDeptVo frameDeptVo) {
        /**
         * 获取所有根目录数据
         */
        QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
        if (frameDeptVo.getSort() != null) {
            BatisPlusUtil.setOrders(queryWrapper, frameDeptVo.getSort().getProp(), frameDeptVo.getSort().getOrder());
        }
        queryWrapper.eq("parent_guid", "");
        List<FrameDept> pList = baseMapper.selectList(queryWrapper);
        List<String> strList = new ArrayList<>();
        JSONArray arr = getChildDepts(pList, strList, "");
        JSONObject obj = new JSONObject();
        obj.put("array", arr);
        if (frameDeptVo.isNeedList()) {
            obj.put("list", strList);
        }
        return obj;
    }

    public JSONArray getChildDepts(List<FrameDept> deptTopTrees, List<String> strList, String parentGuid) {
        JSONArray array = new JSONArray();
        for (FrameDept frameDept : deptTopTrees) {
            strList.add(frameDept.getRowGuid());
            JSONObject json = new JSONObject();
            json.put("deptName", frameDept.getDeptName());
            json.put("rowGuid", frameDept.getRowGuid());
            json.put("parentGuid", parentGuid);
            //获取子模块
            QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_guid", frameDept.getRowGuid());
            List<FrameDept> childDept = baseMapper.selectList(queryWrapper);
            json.put("children", getChildDepts(childDept, strList, frameDept.getRowGuid()));
            array.add(json);
        }
        return array;
    }

    /**
     * 更新(转移)子部门
     *
     * @param oldDeptCode
     * @param newDeptCode
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateChildDept(String oldDeptCode, String newDeptCode) {
        /**
         * 查询修改前这个部门code下的所有子集部门
         */
        List<FrameDept> depts = this.getChildDeptByPdeptCode(oldDeptCode);
        for (FrameDept childDept : depts) {
            String nowDeptCode = newDeptCode + childDept.getDeptCode().substring(oldDeptCode.length());
            childDept.setDeptCode(nowDeptCode);
            childDept.setDeptLevel(nowDeptCode.split("\\.").length);
            QueryWrapper<FrameDept> updateWrapper = new QueryWrapper<>();
            updateWrapper.eq("row_guid", childDept.getRowGuid());
            baseMapper.update(childDept, updateWrapper);
        }

    }

    /**
     * 查询部门code下的所有子集部门
     *
     * @param deptCode
     * @return
     */
    private List<FrameDept> getChildDeptByPdeptCode(String deptCode) {
        return baseMapper.getChildDeptByPdeptCode(deptCode);
    }

    /**
     * 获取部门树列表 全部加载
     *
     * @param sort
     * @return
     */
    @Override
    public List<FrameDept> getAllTreeData(Sort sort) {
        QueryWrapper<FrameDept> deptQueryWrapper = new QueryWrapper();
        deptQueryWrapper.eq("del_flag",0);
        if (sort != null) {
            BatisPlusUtil.setOrders(deptQueryWrapper, sort.getProp(), sort.getOrder());
        }
        List<FrameDept> deptList = baseMapper.selectList(deptQueryWrapper);
        return buildDeptTree(deptList);
    }

    @Override
    public List<FrameDept> getTreeDataByDeptId(String deptId,Boolean self,String[] excludeDeptIds) {
        FrameDept frameDept = baseMapper.selectById(deptId);
        Optional.ofNullable(frameDept).orElseThrow(() -> new BaseException("未找到该部门"));
        QueryWrapper<FrameDept> deptQueryWrapper = new QueryWrapper();
        /**
         * 是否包含自己层级
         */
        if(self){
            deptQueryWrapper.likeRight("dept_code",frameDept.getDeptCode()+".")
                    .or().eq("dept_code",frameDept.getDeptCode());
        }else
            deptQueryWrapper.likeRight("dept_code",frameDept.getDeptCode()+".");


        deptQueryWrapper.notIn(excludeDeptIds != null && excludeDeptIds.length > 0,"row_id",excludeDeptIds);
        deptQueryWrapper.orderByDesc("sort_sq").orderByDesc("row_id");
        List<FrameDept> deptList = baseMapper.selectList(deptQueryWrapper);
        return buildDeptTree(deptList);
    }

    /**
     * 懒加载 树 根据parentGuid加载
     * @param frameDeptVo
     * @return
     */
    @Override
    public JSONArray getTreeDataLazy(FrameDeptVo frameDeptVo) {
        List<FrameDept> deptList = selectByEntity(frameDeptVo.getFrameDept(),frameDeptVo.getSort());
        JSONArray array = JSON.parseArray(JSON.toJSONString(deptList));
        JSONObject object = null;
        FrameDept mod = null;
        for (int i = 0; i < array.size(); i++) {
            object = array.getJSONObject(i);
            mod = new FrameDept();
            mod.setParentGuid(object.getString("rowGuid"));

            QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_guid", object.getString("rowGuid"));
            Long count = count(queryWrapper);
            if (count > 0) {
                object.put("children", new ArrayList<>());
            }
        }
        return array;
    }

    /**
     * 查询补充父部门信息
     * @return
     */
    @Override
    public void addParentDeptInfo(List<FrameDeptDto> deptDtoList) {
        Set<String> deptParentGuids = deptDtoList.stream().map(FrameDeptDto::getParentGuid).collect(Collectors.toSet());
        if(deptParentGuids.size() == 0){
            return;
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("dept_name","row_guid");
        queryWrapper.in("row_guid",deptParentGuids);
        List<FrameDept> depts = baseMapper.selectList(queryWrapper);
        Map<String,String> map = depts.stream().collect(toMap(FrameDept::getRowGuid,FrameDept::getDeptName));
        deptDtoList.forEach(e->e.setParentDeptName(map.get(e.getRowGuid())));
    }

    @Override
    public void validDepts(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return;
        }
        // 获得科室信息
        List<FrameDept> depts = baseMapper.selectBatchIds(ids);
        Map<Long, FrameDept> deptMap = CollectionUtils.convertMap(depts, FrameDept::getRowId);
        // 校验
        ids.forEach(id -> {
            FrameDept dept = deptMap.get(id);
            if (dept == null) {
                throw new BaseException("未找到该部门");
            }
//            if (!CommonStatusEnum.ENABLE.getStatus().equals(dept.getStatus())) {
//                throw exception(DEPT_NOT_ENABLE, dept.getName());
//            }
        });
    }

    @Override
    public List<FrameDept> getSimpleDepts(Character status) {
        return baseMapper.getSimpleDepts(status);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<FrameDept> buildDeptTree(List<FrameDept> depts) {
        List<FrameDept> returnList = new ArrayList<FrameDept>();
        List<String> tempList = new ArrayList<String>();
        for (FrameDept dept : depts) {
            tempList.add(dept.getRowGuid());
        }
        for (FrameDept dept : depts) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentGuid())) {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty()) {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<FrameDept> list, FrameDept t) {
        // 得到子节点列表
        List<FrameDept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (FrameDept tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<FrameDept> getChildList(List<FrameDept> list, FrameDept t) {
        List<FrameDept> tlist = new ArrayList<FrameDept>();
        Iterator<FrameDept> it = list.iterator();
        while (it.hasNext()) {
            FrameDept n = (FrameDept) it.next();
            if (StringUtil.isNotBlank(n.getParentGuid()) && n.getParentGuid().equals(t.getRowGuid())) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<FrameDept> list, FrameDept t) {
        return getChildList(list, t).size() > 0;
    }

    /**
     * 根据人员编号获取所有部门--手机号码+验证码方式登录专用
     * @param userId
     * @return
     */
    @Override
    public List<FrameDept> getDeptsByUserIdForTel(Long userId) {
        return baseMapper.getDeptsByUserIdForTel(userId);
    }
}
