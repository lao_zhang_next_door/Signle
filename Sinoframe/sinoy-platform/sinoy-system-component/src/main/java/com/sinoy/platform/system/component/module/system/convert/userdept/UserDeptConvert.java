package com.sinoy.platform.system.component.module.system.convert.userdept;

import java.util.*;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo.*;
import com.sinoy.platform.system.component.module.system.dal.dataobject.userdept.UserDeptDO;

/**
 * 多部门关联 Convert
 *
 * @author 管理员
 */
@Mapper
public interface UserDeptConvert {

    UserDeptConvert INSTANCE = Mappers.getMapper(UserDeptConvert.class);

    UserDeptDO convert(UserDeptCreateReqVO bean);

    UserDeptDO convert(UserDeptUpdateReqVO bean);

    UserDeptRespVO convert(UserDeptDO bean);

    List<UserDeptRespVO> convertList(List<UserDeptDO> list);

    Page<UserDeptRespVO> convertPage(Page<UserDeptDO> page);


}
