package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostCreateReqVO extends PostBaseVO {

    //排序
    private Integer sortSq;

}
