package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

//管理后台 - 角色权限更新 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RoleLimitUpdateReqVO extends RoleLimitBaseVO {
    //行标
    @NotNull(message = "行标不能为空")
    private Long rowId;
}
