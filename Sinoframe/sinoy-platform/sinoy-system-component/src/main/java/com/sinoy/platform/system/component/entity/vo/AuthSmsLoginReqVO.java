package com.sinoy.platform.system.component.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthSmsLoginReqVO {

    @NotEmpty(message = "手机号不能为空")
    private String mobile;

    @NotEmpty(message = "验证码不能为空")
    private String code;

}
