package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo;

import lombok.*;
import java.util.*;

//管理后台 - 角色权限 Response VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RoleLimitRespVO extends RoleLimitBaseVO {

    //行标
    private Long rowId;

    //创建时间
    private Date createTime;

    private String rowGuid;

}
