package com.sinoy.platform.system.component.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan({"com.sinoy.platform.system.component.dao",
        "com.sinoy.platform.system.component.module.system.dal.mysql.*"})
@Configuration
public class MapperConfig {
}
