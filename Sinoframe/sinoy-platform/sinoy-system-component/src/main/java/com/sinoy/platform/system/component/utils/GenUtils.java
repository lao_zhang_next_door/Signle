//package com.sino.platform.system.component.utils;
//
//
//import com.sino.core.common.utils.application.SpringContextUtils;
//import com.sino.core.common.utils.exception.BaseException;
//import com.sino.platform.system.component.entity.Column;
//import com.sino.platform.system.component.entity.FormTableField;
//import com.sino.platform.system.component.entity.FormTableInfo;
//import com.sino.platform.system.component.entity.Table;
//import com.sino.platform.system.component.service.FormTableInfoService;
//import com.sino.platform.system.component.service.FormTablefieldService;
//import com.sino.platform.system.component.service.FrameCodeValueService;
//import org.apache.commons.configuration.ConfigurationException;
//import org.apache.commons.configuration.PropertiesConfiguration;
//import org.apache.commons.configuration.Configuration;
//import org.apache.commons.io.IOUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.velocity.Template;
//import org.apache.velocity.VelocityContext;
//import org.apache.velocity.app.Velocity;
//
//import java.io.*;
//import java.time.LocalDateTime;
//import java.util.*;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipOutputStream;
//
///**
// * 代码生成器   工具类
// *
// * @author jd
// * @email jd@qq.com
// * @date 2016年12月19日 下午11:40:24
// */
//public class GenUtils {
//	static FormTablefieldService tableService = SpringContextUtils.getBean(FormTablefieldService.class);
//	static FormTableInfoService tableInfoService =  SpringContextUtils.getBean(FormTableInfoService.class);
//	static FrameCodeValueService codeValueService = SpringContextUtils.getBean(FrameCodeValueService.class);
//
//	public static List<String> getTemplates(){
//		List<String> templates = new ArrayList<String>();
////		templates.add("generator/template/Mapper.java.vm");
//		//templates.add("generator/template/DaoImpl.java.vm");
//		templates.add("generator/template/Entity.java.vm");
//		templates.add("generator/template/Mapper.xml.vm");
//		templates.add("generator/template/Service.java.vm");
//		templates.add("generator/template/ServiceImpl.java.vm");
//		templates.add("generator/template/Dao.java.vm");
//		templates.add("generator/template/Controller.java.vm");
//
//		templates.add("generator/template/index.vue.vm");
//		templates.add("generator/template/api.js.vm");
//		templates.add("generator/template/list.vue.vm");
//		templates.add("generator/template/add.vue.vm");
//		templates.add("generator/template/edit.vue.vm");
//		templates.add("generator/template/detail.vue.vm");
//		templates.add("generator/template/formContent.vue.vm");
//
////		templates.add("generator/template/list.ftl.vm");
////		templates.add("generator/template/add.ftl.vm");
////		templates.add("generator/template/edit.ftl.vm");
////		templates.add("generator/template/info.ftl.vm");
//		return templates;
//	}
//
//	/**
//	 * 生成代码
//	 * @param rowGuid
//	 * @param params
//	 */
//	public static void generatorCode(Map<String, String> table,
//			List<Map<String, String>> columns, String rowGuid, Map<String, Object> params,ZipOutputStream output){
//		//配置信息
//		Configuration config = getConfig();
//
//		//表信息
//		Table tableEntity = new Table();
//		tableEntity.setTableName(table.get("tableName"));
//		tableEntity.setComments(table.get("tableComment"));
//		//表名转换成Java类名
//		String className = tableToJava(tableEntity.getTableName(), config.getString("tablePrefix"));
//		tableEntity.setClassName(className);
//		tableEntity.setClassname(StringUtils.uncapitalize(className));
//		//获取长度
//		//列信息
//		List<Column> columsList = new ArrayList<>();
//
//		//获取对应的属性
//		FormTableField f = new FormTableField();
//		f.setAllowTo(rowGuid);
//		List<FormTableField> fields = tableService.select(f);
//
//		FormTableInfo ft = new FormTableInfo();
//		ft.setRowGuid(rowGuid);
//		FormTableInfo form_tableInfo = tableInfoService.selectOne(ft);
//		if (fields == null || fields.size() == 0) {
//			throw new BaseException("不存在对应表，或者该表未添加任何字段");
//		}
//
//		for(Map<String, String> column : columns){
//			Column columnEntity = new Column();
//			columnEntity.setColumnName(column.get("columnName"));
//			columnEntity.setDataType(column.get("dataType"));
//			//设置字段长度
//			int start=column.get("maxLength").indexOf("(");
//			int end =column.get("maxLength").indexOf(")");
//			if(start!=-1){
//				columnEntity.setMaxLength(column.get("maxLength").substring(start+1,end));
//			}else{
//				columnEntity.setMaxLength("50");
//			}
//			columnEntity.setComments(column.get("columnComment"));
//			columnEntity.setExtra(column.get("extra"));
//
//			//列名转换成Java属性名
//			String attrName = columnToJava(columnEntity.getColumnName());
//			columnEntity.setAttrName(attrName);
//			columnEntity.setAttrname(StringUtils.uncapitalize(attrName));
//
//			//列的数据类型，转换成Java类型
//			String attrType = config.getString(columnEntity.getDataType(), "unknowType");
//			columnEntity.setAttrType(attrType);
//
//			//是否主键
//			if("PRI".equalsIgnoreCase(column.get("columnKey")) && tableEntity.getPk() == null){
//				tableEntity.setPk(columnEntity);
//			}
//			for (FormTableField field : fields) {
//				if (column.get("columnName").equals(field.getFieldName())) {
//
//					columnEntity.setCodeName(String.valueOf(field.getCodesGuid()));
//					columnEntity.setIsRequired(String.valueOf(field.getMustFill()));
//					columnEntity.setIsShow(String.valueOf(field.getShowInadd()));
//					columnEntity.setShowType(codeValueService.getItemTextByCodeNameAndCodeValue("表单字段展现形式",field.getFieldDisplayType()));
//					columnEntity.setIsSelected(String.valueOf(field.getIsQueryCondition()));
//				}
//			}
//			columsList.add(columnEntity);
//		}
//		tableEntity.setColumns(columsList);
//
//		//没主键，则第一个字段为主键
//		if(tableEntity.getPk() == null){
//			tableEntity.setPk(tableEntity.getColumns().get(0));
//		}
//
//		//设置velocity资源加载器
//		Properties prop = new Properties();
//		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//		Velocity.init(prop);
//
//		//封装模板数据
//		Map<String, Object> map = new HashMap<>();
//		map.put("tableName", tableEntity.getTableName());
//		map.put("comments", tableEntity.getComments());
//		map.put("pk", tableEntity.getPk());
//		map.put("className", tableEntity.getClassName());
//		map.put("classname", tableEntity.getClassname());
//
////		map.put("pathName", params.get("controllerName").toString().toLowerCase());
////		map.put("pathName", form_tableInfo.getControllerName().toLowerCase());
//		map.put("columns", tableEntity.getColumns());
//		map.put("package",params.get("projectName"));
////		map.put("xmlpackage", config.getString("xmlpackage") +"."+ params.get("projectName"));
////		map.put("package", config.getString("package") + "." + form_tableInfo.getProjectName());
//		map.put("author", config.getString("author"));
//		map.put("email", config.getString("email"));
////		map.put("projectName", params.get("projectName"));
////		map.put("projectName", form_tableInfo.getProjectName());
////		map.put("controllerName", params.get("controllerName"));
////		map.put("controllerName", form_tableInfo.getControllerName());
//		map.put("datetime", LocalDateTime.now());
//		System.out.println(params.get("formStyle"));
//		map.put("formStyle",params.get("formStyle"));
//        VelocityContext context = new VelocityContext(map);
//
//        //获取模板列表
//		List<String> templates = getTemplates();
//		for(String template : templates){
//			//渲染模板
////            OutputStream output = null;
//			try {
//				File file = null;
//
//				file = new File("code/" + getFileName(template, tableEntity.getClassName(),
//						params.get("projectName").toString(),
//						params.get("projectName").toString()));//文件路径（路径+文件名）
//
//				output.putNextEntry(new ZipEntry(file.getPath()));
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//
//			StringWriter sw = new StringWriter();
//			Template tpl = Velocity.getTemplate(template, "UTF-8");
//			tpl.merge(context, sw);
//			try {
//				byte[] sourceBytes = sw.toString().getBytes("UTF-8");
//		        if(null!=sourceBytes){
//		            IOUtils.write(sw.toString(), output, "UTF-8");
//					IOUtils.closeQuietly(sw);
//		        }
//			} catch (IOException e) {
//				throw new BaseException("渲染模板失败，表名：" + tableEntity.getTableName());
//			}
//		}
//	}
//
//
//
//	/**
//	 * 列名转换成Java属性名
//	 */
//	public static String columnToJava(String columnName) {
//		return capitalizeFullySelf(columnName, new char[]{'_'}).replace("_", "");
//	}
//
//
//	public static String capitalizeFullySelf(String str, char[] delimiters) {
//        int delimLen = (delimiters == null ? -1 : delimiters.length);
//        if (str == null || str.length() == 0 || delimLen == 0) {
//            return str;
//        }
//        return capitalize(str, delimiters);
//    }
//
//	 public static String capitalize(String str, char[] delimiters) {
//	        int delimLen = (delimiters == null ? -1 : delimiters.length);
//	        if (str == null || str.length() == 0 || delimLen == 0) {
//	            return str;
//	        }
//	        int strLen = str.length();
//	        StringBuffer buffer = new StringBuffer(strLen);
//	        boolean capitalizeNext = true;
//	        for (int i = 0; i < strLen; i++) {
//	            char ch = str.charAt(i);
//
//	            if (isDelimiter(ch, delimiters)) {
//	                buffer.append(ch);
//	                capitalizeNext = true;
//	            } else if (capitalizeNext) {
//	                buffer.append(Character.toTitleCase(ch));
//	                capitalizeNext = false;
//	            } else {
//	                buffer.append(ch);
//	            }
//	        }
//	        return buffer.toString();
//	    }
//	 private static boolean isDelimiter(char ch, char[] delimiters) {
//	        if (delimiters == null) {
//	            return Character.isWhitespace(ch);
//	        }
//	        for (int i = 0, isize = delimiters.length; i < isize; i++) {
//	            if (ch == delimiters[i]) {
//	                return true;
//	            }
//	        }
//	        return false;
//	    }
//
//	/**
//	 * 表名转换成Java类名
//	 */
//	public static String tableToJava(String tableName, String tablePrefix) {
//		if(StringUtils.isNotBlank(tablePrefix)){
//			tableName = tableName.replace(tablePrefix, "");
//		}
//		return columnToJava(tableName);
//	}
//
//	/**
//	 * 获取配置信息
//	 */
//	public static Configuration getConfig(){
//		try {
//			return new PropertiesConfiguration("generator/generator.properties");
//		} catch (ConfigurationException e) {
//			throw new BaseException("获取配置文件失败，");
//		}
//	}
//
//	/**
//	 * 获取文件名
//	 */
//	public static String getFileName(String template, String className, String packageName, String projectName){
//		String packagePath = "main" + File.separator + "java" + File.separator;
//		if(StringUtils.isNotBlank(packageName)){
//			packagePath += packageName.replace(".", File.separator) + File.separator;
//		}
//
//		if(template.contains("Entity.java.vm")){
//			return packagePath + "entity" + File.separator + className + ".java";
//		}
//
//		if(template.contains("Mapper.java.vm")){
//			return packagePath + "mapper" + File.separator + className + "Mapper.java";
//		}
//
//		if(template.contains("Mapper.xml.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "mapper"
//				 + File.separator + projectName	+ File.separator + className + "Mapper.xml";
//		}
//
//		if(template.contains("Dao.java.vm")){
//			return packagePath + "dao" + File.separator + className + "Dao.java";
//		}
//
//		if(template.contains("list.vue.vm")){
//			return className.toLowerCase() + File.separator + "list.vue";
//		}
//
//		if(template.contains("edit.vue.vm")){
//			return className.toLowerCase() + File.separator + "edit.vue";
//		}
//
//		if(template.contains("add.vue.vm")){
//			return className.toLowerCase() + File.separator + "add.vue";
//		}
//
//		if(template.contains("detail.vue.vm")){
//			return className.toLowerCase() + File.separator + "detail.vue";
//		}
//
//		if(template.contains("formContent.vue.vm")){
//			return className.toLowerCase() + File.separator + "components" +File.separator+ "formContent.vue";
//		}
//
//		if(template.contains("api.js.vm")){
//			return className.toLowerCase() + File.separator + className+ "Api" +".js";
//		}
//
//		if(template.contains("index.vue.vm")){
//			return className.toLowerCase() + File.separator + "index.vue";
//		}
//
//		if(template.contains("DaoImpl.java.vm")){
//			return packagePath + "dao" + File.separator + "impl" + File.separator + className + "DaoImpl.java";
//		}
//
//		if(template.contains("Service.java.vm")){
//			return packagePath + "service" + File.separator + className + "Service.java";
//		}
//
//		if(template.contains("ServiceImpl.java.vm")){
//			return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
//		}
//
//		if(template.contains("Controller.java.vm")){
//			return packagePath + "controller" + File.separator + className + "Controller.java";
//		}
//
//		if(template.contains("list.ftl.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName+ File.separator + className.toLowerCase() + File.separator + "list.html";
//		}
//		if(template.contains("list.js.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase() +File.separator+"js"+ File.separator + "list.js";
//		}
//		if(template.contains("add.ftl.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase() + File.separator + "add.html";
//		}
//		if(template.contains("edit.ftl.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase() + File.separator + "edit.html";
//		}
//		if(template.contains("info.ftl.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase() + File.separator + "detail.html";
//		}
//		if(template.contains("infoSearch.js.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase()  +File.separator + "js"+ File.separator + "infoSearch.js";
//		}
//		if(template.contains("common.ftl.vm")){
//			return "main" + File.separator + "resources" +  File.separator + "templates"
//					+ File.separator + projectName + File.separator + className.toLowerCase() + File.separator + "common.ftl";
//		}
//		if(template.contains("menu.sql.vm")){
//			return className.toLowerCase() + "_menu.sql";
//		}
//
//		return null;
//	}
//
//	public static void filetozip(String filepath,String zipname) throws IOException {
//		File file=new File(filepath);
//		OutputStream outputStream=new FileOutputStream(zipname);
//		ZipOutputStream zipout=new ZipOutputStream(outputStream);
//		//递归函数  三个参数分别代表 1:当前zipout流 2:当前文件/文件夹 3:在zip下的path
//		dozip(zipout,file,"");
//		zipout.finish();
//		zipout.close();
//		outputStream.close();
//	}
//
//	private static void dozip(ZipOutputStream zipout, File file, String addpath) throws IOException {
//		if(file.isDirectory())//如果是个文件夹
//		{
//			File f[]=file.listFiles();
//			for(int i=0;i<f.length;i++)
//			{
//				//如果是个文件夹 后面要加“/”因为它会遍历下层
//				if(f[i].isDirectory()) {
//					dozip(zipout, f[i], addpath+f[i].getName()+"/");
//				}//如果是文件，执行dozip下一次判断它不是文件夹就会进行压缩
//				else {
//					dozip(zipout, f[i], addpath+f[i].getName());
//				}
//			}
//		}
//		else//不是文件夹，是文件
//		{
//			InputStream input;
//			BufferedInputStream buff;
//			//初始为"",意味不找下级就是("xxx.doc")，如果不是顶级目录层就("xx/yy/x.doc")
//			zipout.putNextEntry(new ZipEntry(addpath+file.getName()));
//			input=new FileInputStream(file);
//			buff=new BufferedInputStream(input);
//			byte b[]=new byte[1024*5];
//			int a=0;
//			while((a=buff.read(b))!=-1)
//			{
//				zipout.write(b);
//			}
//			buff.close();
//			input.close();
//			System.out.println(file.getName());
//		}
//
//	}
//
//	public static void main(String[] args) throws IOException {
//		filetozip("D:\\cs","压缩包.zip");
//	}
//}
