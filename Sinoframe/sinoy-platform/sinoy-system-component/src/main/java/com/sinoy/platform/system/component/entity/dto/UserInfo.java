package com.sinoy.platform.system.component.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.database.mybatisplus.handler.JsonLongSetTypeHandler;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameUser;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 用户信息封装
 *
 * @author pangu
 */
@TableName(value = "frame_user", autoResultMap = true)
public class UserInfo extends FrameUser implements Serializable {

	private static final long serialVersionUID = -7657663783681647907L;

	/**
	 * 系统用户信息
	 */
	private FrameUser frameUser;

	/**
	 * 系统权限标识组
	 */
	private List<String> permissions;

	/**
	 * 数据权限
	 */
	private List<String> dataScope;

	/**
	 * 系统角色标识组
	 */
	private List<String> roleGuids;

	/**
	 * 系统角色名组
	 */
	private List<String> roleNameList;

	/**
	 * 角色组
	 */
	private List<FrameRole> roleList;

	/**
	 * 登录类型　1：用户名密码登录　2：手机号登录　3：社交登录
	 */
	private int type;

	/**
	 * 部门code
	 */
	private String deptCode;

	/**
	 * 部门名
	 */
	private String deptName;

	/**
	 * 父级部门guid
	 */
	private String pdeptGuid;

	/**
	 * 父级部门
	 */
	private String pdetpName;

	/**
	 * 租户ID
	 */
	private String tenantId;

	/**
	 * 是否管理员
	 */
	private Boolean isAdmin;

	/**
	 * 部门Id
	 */
	private Long deptId;

	/**
	 * 头像url
	 */
	private String headImgUrl;

	/**
	 * 岗位id 数组
	 */
	@TableField(typeHandler = JsonLongSetTypeHandler.class)
	private Set<Long> postIds;
	/**
	 * 部门id 数组
	 */
/*	@TableField(typeHandler = JsonLongSetTypeHandler.class)
	private Set<Long> deptIds;

	private Set<String> deptIds_jz;*/
	/**
	 * 兼职部门
	 */
/*	private String jzDeptName;

	public String getJzDeptName() {
		return jzDeptName;
	}

	public void setJzDeptName(String jzDeptName) {
		this.jzDeptName = jzDeptName;
	}*/

/*	public Set<String> getDeptIds_jz() {
		return deptIds_jz;
	}

	public void setDeptIds_jz(Set<String> deptIds_jz) {
		this.deptIds_jz = deptIds_jz;
	}*/

	public Set<Long> getPostIds() {
		return postIds;
	}

	public void setPostIds(Set<Long> postIds) {
		this.postIds = postIds;
	}

/*	public Set<Long> getDeptIds() {
		return deptIds;
	}

	public void setDeptIds(Set<Long> deptIds) {
		this.deptIds = deptIds;
	}*/

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public UserInfo setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
		return this;
	}

	public Long getDeptId() {
		return deptId;
	}

	public UserInfo setDeptId(Long deptId) {
		this.deptId = deptId;
		return this;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public List<String> getDataScope() {
		return dataScope;
	}

	public UserInfo setDataScope(List<String> dataScope) {
		this.dataScope = dataScope;
		return this;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public UserInfo setDeptCode(String deptCode) {
		this.deptCode = deptCode;
		return this;
	}

	public List<FrameRole> getRoleList() {
		return roleList;
	}

	public UserInfo setRoleList(List<FrameRole> roleList) {
		this.roleList = roleList;
		return this;
	}

	public List<String> getRoleNameList() {
		return roleNameList;
	}

	public UserInfo setRoleNameList(List<String> roleNameList) {
		this.roleNameList = roleNameList;
		return this;
	}

	public String getPdeptGuid() {
		return pdeptGuid;
	}

	public UserInfo setPdeptGuid(String pdeptGuid) {
		this.pdeptGuid = pdeptGuid;
		return this;
	}

	public String getPdetpName() {
		return pdetpName;
	}

	public UserInfo setPdetpName(String pdetpName) {
		this.pdetpName = pdetpName;
		return this;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public FrameUser getFrameUser() {
		return frameUser;
	}

	public UserInfo setFrameUser(FrameUser frameUser) {
		this.frameUser = frameUser;
		return this;
	}

	public String getDeptName() {
		return deptName;
	}

	public UserInfo setDeptName(String deptName) {
		this.deptName = deptName;
		return this;
	}

	public List<String> getPermissions() {
		return permissions;
	}

	public UserInfo setPermissions(List<String> permissions) {
		this.permissions = permissions;
		return this;
	}

	public List<String> getRoleGuids() {
		return roleGuids;
	}

	public UserInfo setRoleGuids(List<String> roleGuids) {
		this.roleGuids = roleGuids;
		return this;
	}

	public int getType() {
		return type;
	}

	public UserInfo setType(int type) {
		this.type = type;
		return this;
	}

	public String getTenantId() {
		return tenantId;
	}

	public UserInfo setTenantId(String tenantId) {
		this.tenantId = tenantId;
		return this;
	}
}
