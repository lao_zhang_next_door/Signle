package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.FrameUserRole;
import com.sinoy.platform.system.component.entity.pojo.FrameUserRolePojo;
import com.sinoy.platform.system.component.utils.Query;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface FrameUserRoleDao extends ExtendMapper<FrameUserRole> {

    /**
     * 通过用户guid 获取角色英文集合
     * @param userGuid
     * @return
     */
    List<String> selectRoleNamesByUserGuid(String userGuid);

    /**
     * 根据角色guid 获取 所属用户列表
     * @param query
     * @return
     */
    List<FrameUser> getLimitUserByRoleGuid(Query query);
    Integer getCountUserByRoleGuid(Query query);

    /**
     * 获取菜单（模块）权限 ['system:config:list','system:config:add']
     * @param roleGuids
     * @return
     */
    List<String> selectPermissionsByRoleGuids(@Param("roleGuids") List<String> roleGuids);

    @Select({"<script>",
            "SELECT e.*,u.row_id as userId,r.row_id as roleId FROM `frame_user_role` e ",
            " left join frame_user u on e.user_guid = u.row_guid ",
            " left join frame_role r on r.row_guid = e.role_guid",
            " where r.row_id in  ",
            "<foreach item='item' index='index' collection='items' open='(' separator=',' close=')'>",
            "#{item}",
            "</foreach>",
            "</script>"})
    List<FrameUserRolePojo> selectListByRoleIds(@Param("items") Collection<Long> roleIds);
}
