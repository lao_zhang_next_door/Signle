package com.sinoy.platform.system.component.entity;


/**
 * 设计原则:
 * 1.不产生垃圾文件，垃圾数据(待完善)
 * 2.formRowGuid 对应 一个 或者 多个 关联记录
 * 3.记录上传人 ip,若为平台用户上传 则记录上传用户guid
 * 4.存储方式:数据库二进制流 或者 本地文件形式
 */

import com.sinoy.platform.system.component.base.BaseEntity;

/**
 * <p>Title: FrameAttach</p>
 * <p>Description: 附件表</p>
 *
 * @author wzl
 */
public class FrameAttach extends BaseEntity {

    public FrameAttach(String rowGuid){super(rowGuid);};

    public FrameAttach(){};

    public FrameAttach(boolean init){super(init);};
    /**
     * 附件名
     */
    private String attachName;

    /**
     * 文件类型
     **/
    private String contentType;

    /**
     * 文件长度
     */
    private Integer contentLength;
    /**
     * 文件地址或内容
     */

    private String contentUrl;

    /**
     * 与记录关联唯一标识
     **/
    private String formRowGuid;
    
    /**
     * 上传人ip
     */
    private String uploadIp;

    /**
     * 字节流内容
     */
    private byte[] content;

    /**
     * 是否已经生成合成图片
     */
    private String isCreate;

    /**
     * 上传人Guid
     */
    private String uploadUserGuid;

    /**
     * 文件配置编号
     */
    private Long configId;

    /**
     * 文件访问地址
     */
    private String url;

    public String getUrl() {
        return url;
    }

    public FrameAttach setUrl(String url) {
        this.url = url;
        return this;
    }

    public Long getConfigId() {
        return configId;
    }

    public FrameAttach setConfigId(Long configId) {
        this.configId = configId;
        return this;
    }

    public String getUploadIp() {
		return uploadIp;
	}

	public void setUploadIp(String uploadIp) {
		this.uploadIp = uploadIp;
	}

	public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Integer getContentLength() {
        return contentLength;
    }

    public void setContentLength(Integer contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getFormRowGuid() {
        return formRowGuid;
    }

    public void setFormRowGuid(String formRowGuid) {
        this.formRowGuid = formRowGuid;
    }

    public String getUploadUserGuid() {
        return uploadUserGuid;
    }

    public void setUploadUserGuid(String userGuid) {
        this.uploadUserGuid = uploadUserGuid;
    }

    public String getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(String isCreate) {
        this.isCreate = isCreate;
    }
}
