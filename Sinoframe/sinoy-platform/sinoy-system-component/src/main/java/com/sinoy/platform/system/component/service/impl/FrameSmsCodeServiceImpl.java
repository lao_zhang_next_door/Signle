package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameSmsCodeDao;
import com.sinoy.platform.system.component.entity.FrameSmsCode;
import com.sinoy.platform.system.component.service.FrameSmsCodeService;
import org.springframework.stereotype.Service;

@Service
public class FrameSmsCodeServiceImpl extends BaseServiceImpl<FrameSmsCodeDao, FrameSmsCode> implements FrameSmsCodeService {


    @Override
    public int saveSms(FrameSmsCode frameSms) {
        return this.baseMapper.insert(frameSms);
    }
}
