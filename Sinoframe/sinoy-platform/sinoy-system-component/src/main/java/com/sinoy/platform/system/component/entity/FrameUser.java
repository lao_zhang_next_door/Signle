package com.sinoy.platform.system.component.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.core.database.mybatisplus.handler.JsonLongSetTypeHandler;
import com.sinoy.platform.system.component.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 * @author hero
 *
 */
@Builder
@AllArgsConstructor
@TableName(value = "frame_user", autoResultMap = true)
public class FrameUser extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public FrameUser(boolean init) {
        super(init);
    }

    public FrameUser(String rowGuid) {
        super(rowGuid);
    }

    public FrameUser() {}

    /**
     * 最近登录时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime lastLoginTime;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 登录名
     */
    private String loginId;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 工号
     */
    private String gongHao;

    /**
     * 部门关联字段
     */
    private String deptGuid;

    /**
     * 职位
     */
    private String duty;

    /**
     * 电话
     */
    private String tel;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 微信关联号
     */
    private String openId;

    /**
     * 状态(初始值0 >>> 正常)
     */
    private Integer status;
    /**
     * 头像
     */
    private String headimgGuid;
    /**
     * 头像地址
     */
    @Transient
    @TableField(exist = false)
    private String headimgUrl;
    /**
     * 主题
     */
    private String themeGuid;

    /**
     * 是否禁用
     */
    private Boolean isLimit;
    
    /**
     * email
     */
    private String email;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 岗位id 数组
     */
    @TableField(typeHandler = JsonLongSetTypeHandler.class)
    private Set<Long> postIds;

/*    @TableField(typeHandler = JsonLongSetTypeHandler.class)
    private Set<Long> deptIds;*/
    /**
     * 当前登录部门
     */
    private String mainDept;
    /**
     * 是否修改过初始密码
     */
    private Integer isUpdateInitialPassword;

    public Integer getIsUpdateInitialPassword() {
        return isUpdateInitialPassword;
    }

    public FrameUser setIsUpdateInitialPassword(Integer isUpdateInitialPassword) {
        this.isUpdateInitialPassword = isUpdateInitialPassword;
        return this;
    }


    public String getMainDept() {
        return mainDept;
    }

    public void setMainDept(String mainDept) {
        this.mainDept = mainDept;
    }

/*    public Set<Long> getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(Set<Long> deptIds) {
        this.deptIds = deptIds;
    }*/

    public String getHeadimgUrl() {
        return headimgUrl;
    }

    public void setHeadimgUrl(String headimgUrl) {
        this.headimgUrl = headimgUrl;
    }

    public Set<Long> getPostIds() {
        return postIds;
    }

    public void setPostIds(Set<Long> postIds) {
        this.postIds = postIds;
    }

    public Long getDeptId() {
        return deptId;
    }

    public FrameUser setDeptId(Long deptId) {
        this.deptId = deptId;
        return this;
    }

    public LocalDateTime getLastLoginTime() {
        return lastLoginTime;
    }

    public FrameUser setLastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public FrameUser setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getLoginId() {
        return loginId;
    }

    public FrameUser setLoginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public FrameUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public FrameUser setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public String getGongHao() {
        return gongHao;
    }

    public FrameUser setGongHao(String gongHao) {
        this.gongHao = gongHao;
        return this;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public FrameUser setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
        return this;
    }

    public String getDuty() {
        return duty;
    }

    public FrameUser setDuty(String duty) {
        this.duty = duty;
        return this;
    }

    public String getTel() {
        return tel;
    }

    public FrameUser setTel(String tel) {
        this.tel = tel;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public FrameUser setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public FrameUser setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public FrameUser setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getHeadimgGuid() {
        return headimgGuid;
    }

    public FrameUser setHeadimgGuid(String headimgGuid) {
        this.headimgGuid = headimgGuid;
        return this;
    }

    public String getThemeGuid() {
        return themeGuid;
    }

    public FrameUser setThemeGuid(String themeGuid) {
        this.themeGuid = themeGuid;
        return this;
    }

    public Boolean getIsLimit() {
        return isLimit;
    }

    public FrameUser setIsLimit(Boolean isLimit) {
        this.isLimit = isLimit;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public FrameUser setEmail(String email) {
        this.email = email;
        return this;
    }


}
