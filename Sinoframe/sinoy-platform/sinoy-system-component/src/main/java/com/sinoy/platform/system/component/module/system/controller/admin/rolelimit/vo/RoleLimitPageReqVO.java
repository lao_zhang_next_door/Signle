package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.core.database.mybatisplus.handler.JsonLongSetTypeHandler;
import lombok.*;
import java.util.*;
import com.sinoy.core.common.utils.request.PageParam;
import java.time.LocalDate;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

//管理后台 - 角色权限分页 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RoleLimitPageReqVO extends PageParam {
    //行标
    private Long rowId;

    //create_time
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime[] createTime;

    //角色id
    private Long roleId;

    //权限范围内的角色id
    private String roleChildId;


    //角色名称
    private String roleName;

    //可配置角色名称
    private String roleChildName;

}
