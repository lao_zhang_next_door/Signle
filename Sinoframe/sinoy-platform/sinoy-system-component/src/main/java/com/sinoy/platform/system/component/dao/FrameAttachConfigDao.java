package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameAttachConfig;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;

public interface FrameAttachConfigDao extends ExtendMapper<FrameAttachConfig> {

    @Select("SELECT COUNT(*) FROM frame_attach_config WHERE update_time > #{maxUpdateTime}")
    Long selectCountByUpdateTimeGt(LocalDateTime maxUpdateTime);
}
