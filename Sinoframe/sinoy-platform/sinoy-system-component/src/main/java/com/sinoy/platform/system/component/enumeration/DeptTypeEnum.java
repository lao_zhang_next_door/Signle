package com.sinoy.platform.system.component.enumeration;

import com.alibaba.druid.sql.visitor.functions.Char;

/**
 * 部门状态枚举
 * @author hero
 *
 */
public enum DeptTypeEnum {

	COMPANY('0',"公司"),
	DEPARTMENT('1',"部门"),
	FACTORY('2',"工厂"),
	WORKSHOP('3',"车间"),
	TEAM('4',"班组"),
	WAREHOUSE('5',"仓库"),
	STATION('6',"岗位"),
	OTHER('7',"其他");

	private final Character code;
    private final String value;

    private DeptTypeEnum(Character code, String value) {
        this.code = code;
        this.value = value;
    }
	public Character getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
