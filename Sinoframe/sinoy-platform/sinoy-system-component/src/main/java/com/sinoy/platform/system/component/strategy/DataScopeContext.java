//package com.sino.platform.system.component.strategy;
//
//import com.sino.platform.system.component.service.FrameRoleService;
//import com.sino.platform.system.component.utils.CommonPropAndMethods;
//import org.springframework.stereotype.Service;
//
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * 数据权限上下文
// *
// */
//@Service
//public class DataScopeContext {
//
//	private final Map<String, AbstractDataScopeHandler> strategyMap = new ConcurrentHashMap<>();
//
//	private final CommonPropAndMethods commonPropAndMethods;
//
//	private final FrameRoleService frameRoleService;
//
//	/**
//	 * Component里边的1是指定其名字，这个会作为key放到strategyMap里
//	 *
//	 * @param strategyMap
//	 */
//	public DataScopeContext(Map<String, AbstractDataScopeHandler> strategyMap,CommonPropAndMethods commonPropAndMethods,FrameRoleService frameRoleService) {
//		strategyMap.forEach(this.strategyMap::put);
//		this.commonPropAndMethods = commonPropAndMethods;
//		this.frameRoleService = frameRoleService;
//	}
//
////	public List<String> getDeptIdsForDataScope(RoleDto roleDto, Integer type) {
////		return strategyMap.get(String.valueOf(type)).getDeptIds(roleDto, DataScopeTypeEnum.valueOf(type));
////	}
//}
