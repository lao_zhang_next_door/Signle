package com.sinoy.platform.system.component.entity;

import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.platform.system.component.base.BaseEntity;

import java.util.List;

/**
 * 角色
 */
public class FrameModule extends BaseEntity {

    public FrameModule() {
    }

    public FrameModule(String rowGuid) {
        super(rowGuid);
    }

    public FrameModule(boolean init) {
        super(init);
    }

    /**
     * 模块名
     */
    private String moduleName;

    /**
     * 父级模块
     */
    private String parentGuid;

    /**
     * 子集数量
     */
    private Integer hasChild;

    /**
     * 模块地址
     */
    private String moduleAddress;

    /**
     * 小图标
     */
    private String smallIcon;

    /**
     * 大图标
     */
    private String bigIcon;

    /**
     * 目标框架
     */
    private Character target;

    /**
     * 转发路径
     */
    private String redirect;

    /**
     * 配置的访问路径
     */
    private String path;

    /**
     * 层级
     */
    private Integer moduleLevel;

    /**
     * 规则code
     */
    private String moduleCode;

    /**
     * 路由组件名
     */
    private String compName;

    /**
     * 模块类型 0:分类  1:页面  2:按钮
     */
    private Long moduleType;

    /**
     * 是否隐藏
     */
    private Character isHide;

    /**
     * 是否隐藏text
     */
    @TableField(exist = false)
    private String isHideVal;

    /**
     * 缓存
     */
    private Character keepAlive;

    /**
     * 是否缓存Text
     */
    @TableField(exist = false)
    private String keepAliveVal;

    /**
     * 菜单类型  应用::目录::菜单::按钮
     */
    private Character menuType;

    /**
     * 权限标识 ? system::user::list::add
     */
    private String perms;

    /**
     * 应用id
     */
    private String appId;

    /**
     * 打开方式 组件 无 内链 外链
     */
    private Character openType;

    /**
     * 路由参数
     */
    private String query;

    @TableField(exist = false)
    private String test;

    /**
     * 子模块
     */
    @TableField(exist = false)
    private JSONArray children;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public JSONArray getChildren() {
        return children;
    }

    public void setChildren(JSONArray children) {
        this.children = children;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getParentGuid() {
        return parentGuid;
    }

    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }

    public Integer getHasChild() {
        return hasChild;
    }

    public void setHasChild(Integer hasChild) {
        this.hasChild = hasChild;
    }

    public String getModuleAddress() {
        return moduleAddress;
    }

    public void setModuleAddress(String moduleAddress) {
        this.moduleAddress = moduleAddress;
    }

    public String getSmallIcon() {
        return smallIcon;
    }

    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }

    public String getBigIcon() {
        return bigIcon;
    }

    public void setBigIcon(String bigIcon) {
        this.bigIcon = bigIcon;
    }

    public Character getTarget() {
        return target;
    }

    public void setTarget(Character target) {
        this.target = target;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getModuleLevel() {
        return moduleLevel;
    }

    public void setModuleLevel(Integer moduleLevel) {
        this.moduleLevel = moduleLevel;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Long getModuleType() {
        return moduleType;
    }

    public void setModuleType(Long moduleType) {
        this.moduleType = moduleType;
    }

    public Character getIsHide() {
        return isHide;
    }

    public void setIsHide(Character isHide) {
        this.isHide = isHide;
    }

    public String getIsHideVal() {
        return isHideVal;
    }

    public void setIsHideVal(String isHideVal) {
        this.isHideVal = isHideVal;
    }

    public Character getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(Character keepAlive) {
        this.keepAlive = keepAlive;
    }

    public String getKeepAliveVal() {
        return keepAliveVal;
    }

    public void setKeepAliveVal(String keepAliveVal) {
        this.keepAliveVal = keepAliveVal;
    }

    public Character getMenuType() {
        return menuType;
    }

    public void setMenuType(Character menuType) {
        this.menuType = menuType;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Character getOpenType() {
        return openType;
    }

    public void setOpenType(Character openType) {
        this.openType = openType;
    }
}
