package com.sinoy.platform.system.component.module.system.convert.rolelimit;

import java.util.*;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitRespVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitUpdateReqVO;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * 角色权限 Convert
 *
 * @author 管理员
 */
@Mapper
public interface RoleLimitConvert {

    RoleLimitConvert INSTANCE = Mappers.getMapper(RoleLimitConvert.class);

    RoleLimitDO convert(RoleLimitCreateReqVO bean);

    RoleLimitDO convert(RoleLimitUpdateReqVO bean);

    RoleLimitRespVO convert(RoleLimitDO bean);

    List<RoleLimitRespVO> convertList(List<RoleLimitDO> list);

    Page<RoleLimitRespVO> convertPage(Page<RoleLimitDO> page);


}
