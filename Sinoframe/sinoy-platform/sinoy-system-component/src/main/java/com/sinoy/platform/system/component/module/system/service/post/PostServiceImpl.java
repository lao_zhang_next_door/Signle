package com.sinoy.platform.system.component.module.system.service.post;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostPageReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostUpdateReqVO;
import com.sinoy.platform.system.component.module.system.convert.post.PostConvert;
import com.sinoy.platform.system.component.module.system.dal.dataobject.post.PostDO;
import com.sinoy.platform.system.component.module.system.dal.mysql.post.PostMapper;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.sinoy.core.common.utils.dataformat.collection.CollectionUtils.convertMap;

/**
 * 岗位 Service 实现类
 *
 */
@Service
@Validated
public class PostServiceImpl implements PostService {

    @Resource
    private PostMapper postMapper;

    @Override
    public Long createPost(PostCreateReqVO reqVO) {
        // 校验正确性
        this.checkCreateOrUpdate(null, reqVO.getName(), reqVO.getCode());
        // 插入岗位
        PostDO post = PostConvert.INSTANCE.convert(reqVO);
        post.initNull();
        postMapper.insert(post);
        return post.getRowId();
    }

    @Override
    public void updatePost(PostUpdateReqVO reqVO) {
        // 校验正确性
        this.checkCreateOrUpdate(reqVO.getRowId(), reqVO.getName(), reqVO.getCode());
        // 更新岗位
        PostDO updateObj = PostConvert.INSTANCE.convert(reqVO);
        postMapper.updateById(updateObj);
    }

    @Override
    public void deletePost(Long id) {
        // 校验是否存在
        this.checkPostExists(id);
        // 删除部门
        postMapper.deleteById(id);
    }

    @Override
    public List<PostDO> getPosts(Collection<Long> ids, Collection<Integer> statuses) {
        return postMapper.selectList(ids, statuses);
    }

    @Override
    public Page<PostDO> getPostPage(PostPageReqVO reqVO) {
        return postMapper.selectPage(reqVO);
    }

    @Override
    public PostDO getPost(Long id) {
        return postMapper.selectById(id);
    }

    private void checkCreateOrUpdate(Long id, String name, String code) {
        // 校验自己存在
        checkPostExists(id);
        // 校验岗位名的唯一性
        checkPostNameUnique(id, name);
        // 校验岗位编码的唯一性
        checkPostCodeUnique(id, code);
    }

    private void checkPostNameUnique(Long id, String name) {
        PostDO post = postMapper.selectByName(name);
        if (post == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的岗位
        if (id == null) {
            throw new BaseException("岗位名称不能重复");
        }
        if (!post.getRowId().equals(id)) {
            throw new BaseException("岗位名称不能重复");
        }
    }

    private void checkPostCodeUnique(Long id, String code) {
        PostDO post = postMapper.selectByCode(code);
        if (post == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的岗位
        if (id == null) {
            throw new BaseException("岗位编码不能重复");
        }
        if (!post.getRowId().equals(id)) {
            throw new BaseException("岗位编码不能重复");
        }
    }

    private void checkPostExists(Long id) {
        if (id == null) {
            return;
        }
        PostDO post = postMapper.selectById(id);
        if (post == null) {
            throw new BaseException("未找到该岗位");
        }
    }

    @Override
    public void validPosts(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return;
        }
        // 获得岗位信息
        List<PostDO> posts = postMapper.selectBatchIds(ids);
        Map<Long, PostDO> postMap = convertMap(posts, PostDO::getRowId);
        // 校验
        ids.forEach(id -> {
            PostDO post = postMap.get(id);
            if (post == null) {
                throw new BaseException("未找到该岗位");
            }
            if (!CommonStatusEnum.ENABLE.getStatus().equals(post.getStatus())) {
                throw new BaseException("岗位未启用");
            }
        });
    }
}
