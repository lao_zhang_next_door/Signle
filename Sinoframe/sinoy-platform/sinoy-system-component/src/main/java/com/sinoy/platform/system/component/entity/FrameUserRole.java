package com.sinoy.platform.system.component.entity;


import com.sinoy.platform.system.component.base.BaseEntity;

/**
 * 用户与角色关系
 */
public class FrameUserRole extends BaseEntity {

    public FrameUserRole(){}

    /**
     * @param init true则有默认值
     */
    public FrameUserRole(Boolean init){
        super(init);
    }

    private String userGuid;

    private String roleGuid;

    public String getUserGuid() {
        return userGuid;
    }

    public FrameUserRole setUserGuid(String userGuid) {
        this.userGuid = userGuid;
        return this;
    }

    public String getRoleGuid() {
        return roleGuid;
    }

    public FrameUserRole setRoleGuid(String roleGuid) {
        this.roleGuid = roleGuid;
        return this;
    }

}
