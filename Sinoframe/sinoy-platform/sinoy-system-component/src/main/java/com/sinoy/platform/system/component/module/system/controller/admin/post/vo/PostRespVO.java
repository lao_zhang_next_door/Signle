package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostRespVO extends PostBaseVO {

    //岗位序号
    private Long rowId;

    //创建时间
    private Date createTime;

    //排序
    private Integer sortSq;

}
