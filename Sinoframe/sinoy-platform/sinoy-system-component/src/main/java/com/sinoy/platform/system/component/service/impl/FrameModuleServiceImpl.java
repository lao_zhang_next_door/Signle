package com.sinoy.platform.system.component.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.IsOrNot;
import com.sinoy.core.common.enums.ModuleTypeEnum;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.platform.system.component.dao.FrameModuleDao;
import com.sinoy.platform.system.component.dao.FrameModulePermissionDao;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.dto.FrameModuleDto;
import com.sinoy.platform.system.component.entity.dto.FrameModulePermissionDto;
import com.sinoy.platform.system.component.entity.vo.FrameModuleVo;
import com.sinoy.platform.system.component.enumeration.MenuType;
import com.sinoy.platform.system.component.service.FrameModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;


@Service
public class FrameModuleServiceImpl extends BaseServiceImpl<FrameModuleDao, FrameModule> implements FrameModuleService {

    @Autowired
    private FrameModulePermissionDao roleModuleDao;

    @Override
    public List<FrameModuleDto> selectParentCode(String rowGuid) {
        return baseMapper.selectModuleAndParentModule(rowGuid);
    }

    @Override
    public JSONObject allTreeData(FrameModuleVo frameModuleVo) {

        /**
         * 获取所有根目录数据
         */
        QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
        if (frameModuleVo.getSort() != null) {
            BatisPlusUtil.setOrders(queryWrapper, frameModuleVo.getSort().getProp(), frameModuleVo.getSort().getOrder());
        }
        queryWrapper.eq("parent_guid", "");
        if (frameModuleVo.getFrameModule() != null) {
            queryWrapper.eq("module_type", frameModuleVo.getFrameModule().getModuleType());
        }

        List<FrameModule> pList = baseMapper.selectList(queryWrapper);
        List<String> strList = new ArrayList<>();
        JSONArray arr = getChildModules(pList, strList, "", frameModuleVo.getSort());
        JSONObject obj = new JSONObject();
        obj.put("array", arr);
        if (frameModuleVo.isNeedList()) {
            obj.put("list", strList);
        }
        return obj;
    }

    public JSONArray getChildModules(List<FrameModule> deptTopTrees, List<String> strList, String parentGuid, Sort sort) {
        JSONArray array = new JSONArray();
        for (FrameModule frameModule : deptTopTrees) {
            strList.add(frameModule.getRowGuid());
            JSONObject json = new JSONObject();
            json.put("moduleName", frameModule.getModuleName());
            json.put("rowGuid", frameModule.getRowGuid());
            json.put("parentGuid", parentGuid);
            json.put("moduleCode", frameModule.getModuleCode());
            json.put("menuType", frameModule.getMenuType());
            //获取子模块
            QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_guid", frameModule.getRowGuid());
            if (sort != null) {
                BatisPlusUtil.setOrders(queryWrapper, sort.getProp(), sort.getOrder());
            }
            List<FrameModule> childDept = baseMapper.selectList(queryWrapper);
            json.put("children", getChildModules(childDept, strList, frameModule.getRowGuid(), sort));
            array.add(json);
        }
        return array;
    }

    @Override
    public FrameModule saveModule(FrameModule frameModule) {
        baseMapper.insert(frameModule);
        if (StringUtil.isBlank(frameModule.getParentGuid())) {
            frameModule.setParentGuid("");
            //说明是根节点
            frameModule.setModuleCode(String.valueOf(frameModule.getRowId()));
            //模块层级
            frameModule.setModuleLevel(1);
        } else {
            //子节点
            QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("row_guid", frameModule.getParentGuid());
            FrameModule parentModule = baseMapper.selectOne(queryWrapper);
            String deptCode = parentModule.getModuleCode() + "." + frameModule.getRowId();
            frameModule.setModuleCode(deptCode);
            //单位层级
            String[] strArr = deptCode.split("\\.");
            frameModule.setModuleLevel(strArr.length);
        }
        QueryWrapper<FrameModule> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("row_guid", frameModule.getRowGuid());
        baseMapper.update(frameModule, updateWrapper);
        return frameModule;
    }

    @Override
    @Transactional
    public FrameModule updateModule(FrameModule frameModule) {

        String oldModuleCode = frameModule.getModuleCode();
        /**
         * 初始待修改的单位
         */
        QueryWrapper<FrameModule> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", frameModule.getRowGuid());
        FrameModule originModule = baseMapper.selectOne(wrapper);
        if (originModule == null) {
            throw new BaseException("修改异常,未查询到该模块");
        }

        String oldParentGuid = originModule.getParentGuid();
        String newParentGuid = frameModule.getParentGuid();

        String newModuleCode = "";
        if (StringUtil.isBlank(newParentGuid)) {
            /**
             * 根目录
             */
            newModuleCode = frameModule.getRowId().toString();
            frameModule.setModuleLevel(1);
        } else {
            /**
             * 需要修改到这个父节点下
             */
            QueryWrapper<FrameModule> newWrapper = new QueryWrapper<>();
            newWrapper.eq("row_guid", newParentGuid);
            FrameModule newModule = baseMapper.selectOne(newWrapper);
            if (newModule == null || StringUtil.isBlank(newModule.getModuleCode())) {
                throw new BaseException("修改异常,该选择的父节点部门不存在或者其部门编码异常");
            }
            newModuleCode = newModule.getModuleCode() + "." + frameModule.getRowId().toString();
            frameModule.setModuleLevel(newModuleCode.split("\\.").length);
        }
        frameModule.setModuleCode(newModuleCode);

        /**
         * 修改部门
         */
        QueryWrapper<FrameModule> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("row_guid", frameModule.getRowGuid());

        baseMapper.update(frameModule, updateWrapper);

        if (!oldParentGuid.equals(newParentGuid)) {
            this.updateChildModule(oldModuleCode, newModuleCode);
        }

        return frameModule;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteModule(String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("row_guid", rowGuid);
            queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
            FrameModule frameModule = baseMapper.selectOne(queryWrapper);
            if (frameModule != null) {
                if (StringUtil.isBlank(frameModule.getModuleCode())) {
                    throw new BaseException(frameModule.getModuleName() + "模块code为空");
                }
                //级联删除模块
                baseMapper.deleteModule(frameModule.getModuleCode());
            }
        }

    }

    /**
     * 更新(转移)子模块
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateChildModule(String oldModuleCode, String newModuleCode) {
        /**
         * 查询修改前这个部门code下的所有子集部门
         */
        List<FrameModule> modules = this.getChildModuleByPModuleCode(oldModuleCode);

        for (FrameModule childModule : modules) {

            String nowDeptCode = newModuleCode + childModule.getModuleCode().substring(oldModuleCode.length());
//			String nowDeptCode = newDeptCode + "." + childDept.getRowId();
            childModule.setModuleCode(nowDeptCode);
            childModule.setModuleLevel(nowDeptCode.split("\\.").length);

            QueryWrapper<FrameModule> updateWrapper = new QueryWrapper<>();
            updateWrapper.eq("row_guid", childModule.getRowGuid());
            baseMapper.update(childModule, updateWrapper);
        }

    }

    /**
     * 查询部门code下的所有子集部门
     *
     * @return
     */
    private List<FrameModule> getChildModuleByPModuleCode(String moduleCode) {
        return baseMapper.getChildModuleByPModuleCode(moduleCode);
    }

    @Override
    public JSONArray allTreeDataRoute(){
        List<FrameModule> pList = list(new LambdaQueryWrapper<FrameModule>().ne(FrameModule::getMenuType,MenuType.AN.getCode())
                .orderByDesc(FrameModule::getSortSq).orderByAsc(FrameModule::getRowId));
        return buildModuleTree(pList);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public JSONArray buildModuleTree(List<FrameModule> depts) {
        JSONArray returnList = new JSONArray();
        List<String> tempList = new ArrayList<String>();
        for (FrameModule dept : depts) {
            tempList.add(dept.getRowGuid());
        }
        for (FrameModule dept : depts) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentGuid())) {
                JSONObject object = JSON.parseObject(JSON.toJSONString(dept));
                recursionFn(depts, object);
                returnList.add(buildModuleJson(object));
            }
        }
        if (returnList.isEmpty()) {
            returnList.addAll(depts);
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<FrameModule> list, JSONObject t) {
        // 得到子节点列表
        JSONArray childList = getChildList(list, t);
        t.put("children",childList);
        for (int i = 0; i < childList.size(); i++) {
            if (hasChild(list, childList.getJSONObject(i))) {
                recursionFn(list, childList.getJSONObject(i));
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private JSONArray getChildList(List<FrameModule> list, JSONObject t) {
        JSONArray tlist = new JSONArray();
        Iterator<FrameModule> it = list.iterator();
        while (it.hasNext()) {
            FrameModule n = (FrameModule) it.next();
            if (StringUtil.isNotBlank(n.getParentGuid()) && n.getParentGuid().equals(t.getString("rowGuid"))) {
                tlist.add(buildModuleJson(JSON.parseObject(JSON.toJSONString(n))));
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<FrameModule> list, JSONObject t) {
        return getChildList(list, t).size() > 0;
    }

    private JSONObject buildModuleJson(JSONObject frameModule){

        JSONObject json = new JSONObject();
        json.put("rowGuid",frameModule.getString("rowGuid"));
        json.put("children",frameModule.getJSONArray("children"));
        json.put("path", frameModule.getString("path") == null ? "" : frameModule.getString("path"));
        json.put("name", frameModule.getString("moduleName"));
        json.put("component", frameModule.getString("moduleAddress"));
        json.put("isHide", frameModule.getString("isHide"));
        json.put("visible", IsOrNot.NOT.getCode() == Integer.valueOf(frameModule.getString("isHide")));
        json.put("redirect", frameModule.getString("redirect"));
        json.put("icon", frameModule.getString("smallIcon"));
        json.put("parentModule", frameModule.getString("parentGuid"));
        json.put("menuType",frameModule.getString("menuType"));
        json.put("query",frameModule.getString("query"));
        JSONObject metaObj = new JSONObject();

        //根据moduleGuid获取对应角色
        List<FrameModulePermissionDto> moduleRight = roleModuleDao.selectLJRole(frameModule.getString("rowGuid"));

        JSONArray arr = new JSONArray();
        if (moduleRight != null && moduleRight.size() != 0) {
            for (FrameModulePermissionDto roleModulePojo : moduleRight) {
                arr.add(roleModulePojo.getRoleFlag());
            }
        }
        metaObj.put("roles", arr);
        metaObj.put("keepAlive", frameModule.getString("keepAlive"));
        metaObj.put("compName", frameModule.getString("compName"));
//        metaObj.put("parentCompName", nPcompNames);
        metaObj.put("level", frameModule.getString("moduleLevel"));
        json.put("meta", metaObj);
        return json;
    }


    /**
     * 方法过于沉重 所以再次废弃 通过内存来进行数据处理
     * @return
     */
    @Deprecated
    public JSONArray allTreeDataRouteBack() {
        /**
         * 获取所有根目录数据
         */
        FrameModule module = new FrameModule();
        module.setParentGuid("");

        QueryWrapper<FrameModule> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_guid", "");
        wrapper.orderByDesc("sort_sq").orderByAsc("row_id");
        List<FrameModule> pList = baseMapper.selectList(wrapper);
        module = null;
        JSONArray arr = getChildModulesRoute(pList, null);

        return arr;
    }


    public JSONArray getChildModulesRoute(List<FrameModule> deptTopTrees, List<String> PcompNames) {
        JSONArray array = new JSONArray();
        for (FrameModule frameModule : deptTopTrees) {
            //按钮不参与路由
            if(MenuType.AN.getCode().equals(frameModule.getMenuType())){
                continue;
            }
            List<String> nPcompNames = new ArrayList<>();
            if (PcompNames != null) {
                nPcompNames.addAll(PcompNames);
            }
            JSONObject json = new JSONObject();
            json.put("path", frameModule.getPath() == null ? "" : frameModule.getPath());
            json.put("name", frameModule.getModuleName());
            json.put("component", frameModule.getModuleAddress());
            json.put("isHide", frameModule.getIsHide());
            json.put("visible", IsOrNot.NOT.getCode() == Integer.valueOf(String.valueOf(frameModule.getIsHide().charValue())));
            json.put("redirect", frameModule.getRedirect());
            json.put("icon", frameModule.getSmallIcon());
            json.put("parentModule", frameModule.getParentGuid());
            json.put("menuType",frameModule.getMenuType());
            JSONObject metaObj = new JSONObject();

            //根据moduleGuid获取对应角色
            List<FrameModulePermissionDto> moduleRight = roleModuleDao.selectLJRole(frameModule.getRowGuid());

            JSONArray arr = new JSONArray();
            if (moduleRight != null && moduleRight.size() != 0) {
                for (FrameModulePermissionDto roleModulePojo : moduleRight) {
                    arr.add(roleModulePojo.getRoleFlag());
                }
            }
            metaObj.put("roles", arr);
            metaObj.put("keepAlive", frameModule.getKeepAlive());
            metaObj.put("compName", frameModule.getCompName());
            metaObj.put("parentCompName", nPcompNames);
            metaObj.put("level", frameModule.getModuleLevel());
            json.put("meta", metaObj);

            //获取子模块
            QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_guid", frameModule.getRowGuid());
            queryWrapper.orderByDesc("sort_sq").orderByAsc("row_id");
            List<FrameModule> childDept = baseMapper.selectList(queryWrapper);
            if (childDept.size() > 0) {
                String parentCompName = frameModule.getModuleName();
                nPcompNames.add(parentCompName);
                json.put("children", getChildModulesRoute(childDept, nPcompNames));
            }
            array.add(json);
        }
        return array;
    }

    @Override
    public JSONArray getVue3PermissionRoute(String[] roleGuids) {
        //获取用户目录所有vue3模块权限
        List<FrameModule> permissionModuleList = roleModuleDao.selectModulesFromRoleGuid(ModuleTypeEnum.VUE3.getCode(), roleGuids);
        //筛选父级节点
        List<FrameModule> parentModuleList = permissionModuleList.stream().filter(item -> MenuType.ML.getCode().equals(item.getMenuType())).collect(Collectors.toList());
        //筛选按钮权限节点
        List<FrameModule> buttonModuleList = permissionModuleList.stream().filter(item -> MenuType.AN.getCode().equals(item.getMenuType())).collect(Collectors.toList());
        return this.initVue3ChildModulesRoute(permissionModuleList, parentModuleList, buttonModuleList);
    }

    /**
     * 查询补充模块信息
     *
     * @param moduleDtos
     * @return
     */
    @Override
    public void addParentModuleInfo(List<FrameModuleDto> moduleDtos) {
        // 提取子模块parentGuid，方便批量查询
        Set<String> parentGuids = moduleDtos.stream().map(FrameModuleDto::getParentGuid).collect(toSet());
        if (parentGuids.size() == 0) {
            return;
        }
        // 根据提取子模块parentGuid查询moduleName（查询前，先做非空判断）
        List<FrameModule> modules = baseMapper.selectList(Wrappers.lambdaQuery(FrameModule.class).in(FrameModule::getRowGuid, parentGuids));
        // 构造映射关系，方便匹配
        Map<String, String> hashMap = modules.stream().collect(toMap(FrameModule::getRowGuid, FrameModule::getModuleName));
        // 封装Vo，并添加到集合中(关键内容)
        moduleDtos.forEach(e -> e.setParentModuleName(hashMap.get(e.getParentGuid())));
    }

    /**
     * 递归生成vue3路由参数
     *
     * @param permissionModuleList 用户权限下所有模块菜单
     * @param parentModuleList     上一级父级菜单
     * @param buttonModuleList     按钮模块
     * @return
     */
    public JSONArray initVue3ChildModulesRoute(List<FrameModule> permissionModuleList, List<FrameModule> parentModuleList, List<FrameModule> buttonModuleList) {
        JSONArray array = new JSONArray();
        for (FrameModule fatherModule : parentModuleList) {
            JSONObject json = new JSONObject();
            json.put("path", fatherModule.getPath());
            json.put("name", fatherModule.getCompName());
            json.put("component", fatherModule.getModuleAddress());
            if (fatherModule.getRedirect() != null && !"".equals(fatherModule.getRedirect())) {
                json.put("redirect", fatherModule.getRedirect());
            }
            JSONObject metaObj = new JSONObject();

            //如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
            metaObj.put("noCache", Integer.parseInt(fatherModule.getKeepAlive().toString()) != IsOrNot.IS.getCode());
            //设置该路由在侧边栏和面包屑中展示的名字
            metaObj.put("title", fatherModule.getModuleName());
            metaObj.put("icon", fatherModule.getSmallIcon());
            metaObj.put("hidden", Integer.parseInt(fatherModule.getIsHide().toString()) != IsOrNot.NOT.getCode());
            //获取他的子模块菜单(包含按钮)
            List<FrameModule> allChildrenModuleList = permissionModuleList.stream().filter(child -> child.getParentGuid().equals(fatherModule.getRowGuid())).collect(Collectors.toList());
            //若为父级菜单
            if (MenuType.ML.getCode().equals(fatherModule.getMenuType())) {
                metaObj.put("alwaysShow", false);
                //筛选菜单模块权限
                List<FrameModule> childrenModuleList = allChildrenModuleList.stream().filter(child -> child.getMenuType().equals(MenuType.CD.getCode())).collect(Collectors.toList());
                //递归
                json.put("children", initVue3ChildModulesRoute(permissionModuleList, childrenModuleList, buttonModuleList));
            } else {
                //子菜单不递归
                //查询此模块用户的按钮权限
                List<FrameModule> childrenButtonList = buttonModuleList.stream().filter(child -> child.getParentGuid().equals(fatherModule.getRowGuid())).collect(Collectors.toList());
                List<String> buttonPermissions = childrenButtonList.stream().map(FrameModule::getPerms).collect(Collectors.toList());
                metaObj.put("buttonPermission", buttonPermissions);
            }
            json.put("meta", metaObj);
            array.add(json);

        }
        return array;
    }
}
