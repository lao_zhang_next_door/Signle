package com.sinoy.platform.system.component.entity.maps;

import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 类型转换
 * @Mapper注解的componentModel属性
 * componentModel属性用于指定自动生成的接口实现类的组件类型。这个属性支持四个值：
 * default: 这是默认的情况，mapstruct不使用任何组件类型, 可以通过Mappers.getMapper(Class)方式获取自动生成的实例对象。
 * cdi: the generated mapper is an application-scoped CDI bean and can be retrieved via @Inject
 * spring: 生成的实现类上面会自动添加一个@Component注解，可以通过Spring的 @Autowired方式进行注入
 * jsr330: 生成的实现类上会添加@javax.inject.Named 和@Singleton注解，可以通过 @Inject注解获取。
 */
@Mapper(componentModel="spring")
public interface FrameDeptMapstruct {

    /**
     * 获取该类自动生成的实现类的实例
     * 接口中的属性都是 public static final 的 方法都是public abstract的
     */
    FrameDeptMapstruct INSTANCES = Mappers.getMapper(FrameDeptMapstruct.class);

    /**
     * 这个方法就是用于实现对象属性复制的方法
     *
     * @Mapping 用来定义属性复制规则 source 指定源对象属性 target指定目标对象属性
     *
     * @param dept 这个参数就是源对象，也就是需要被复制的对象
     * @return 返回的是目标对象，就是最终的结果对象
     */
    @Mappings({
            @Mapping(target = "children", ignore = true)
    })
    FrameDeptDto toDeptDto(FrameDept dept);

    /**
     * 转换list前置条件 需要先有转换实体
     * @param deptlist
     * @return
     */
    List<FrameDeptDto> toDeptDtoList(List<FrameDept> deptlist);


}
