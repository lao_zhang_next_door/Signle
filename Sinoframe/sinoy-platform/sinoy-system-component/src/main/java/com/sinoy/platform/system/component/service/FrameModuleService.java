package com.sinoy.platform.system.component.service;


import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.dto.FrameModuleDto;
import com.sinoy.platform.system.component.entity.vo.FrameModuleVo;

import java.util.List;

public interface FrameModuleService extends BaseService<FrameModule> {

    /**
     * 根据rowGuid 联查(left join frameModule)
     * @param rowGuid
     * @return
     */
    List<FrameModuleDto> selectParentCode(String rowGuid);

    /**
     * 一次获取全部树数据
     * @param frameModuleVo
     * @return
     */
    JSONObject allTreeData(FrameModuleVo frameModuleVo);

    /**
     * 一次获取全部路由
     * @return
     */
    JSONArray allTreeDataRoute();

    /**
     * 新建模块
     * @param frameModule
     * @return
     */
    FrameModule saveModule(FrameModule frameModule);

    /**
     * 修改模块
     * @param frameModule
     * @return
     */
    FrameModule updateModule(FrameModule frameModule);

    /**
     * 级联删除模块
     * @param rowGuids
     */
    void deleteModule(String[] rowGuids);

    /**
     * 获取vue3路由权限
     * @param roleGuids
     * @return
     */
    JSONArray getVue3PermissionRoute(String[] roleGuids);

    /**
     * 查询补充信息 添加 父部门信息
     * @return
     */
    void addParentModuleInfo(List<FrameModuleDto> moduleDtos);

}
