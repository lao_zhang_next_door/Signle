package com.sinoy.platform.system.component.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameModulePermission;
import com.sinoy.platform.system.component.entity.dto.FrameModulePermissionDto;

import java.util.List;

public interface FrameModulePermissionService extends BaseService<FrameModulePermission> {

    /**
     * 根据 模块guid 联查role表
     * @return
     */
    List<FrameModulePermissionDto> selectLJRole(String moduleGuid);

}
