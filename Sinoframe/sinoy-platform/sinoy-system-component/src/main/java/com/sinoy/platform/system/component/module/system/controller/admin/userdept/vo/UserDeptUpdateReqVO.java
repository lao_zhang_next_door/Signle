package com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

//管理后台 - 多部门关联更新 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserDeptUpdateReqVO extends UserDeptBaseVO {

}
