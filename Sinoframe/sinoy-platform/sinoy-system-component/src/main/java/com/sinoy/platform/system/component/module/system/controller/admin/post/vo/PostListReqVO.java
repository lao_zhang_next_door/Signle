package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PostListReqVO extends PostBaseVO {

    //岗位名称
    private String name;

    //展示状态 CommonStatusEnum
    private Integer status;

}
