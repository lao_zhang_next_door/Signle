package com.sinoy.platform.system.component.entity.pojo;

import com.sinoy.platform.system.component.entity.FrameUserRole;
import lombok.Data;

@Data
public class FrameUserRolePojo extends FrameUserRole {

    private Long userId;

    private Long roleId;
}
