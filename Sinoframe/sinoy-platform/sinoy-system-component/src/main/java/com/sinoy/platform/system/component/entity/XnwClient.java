package com.sinoy.platform.system.component.entity;

import com.sinoy.platform.system.component.base.BaseEntity;

public class XnwClient extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public XnwClient(boolean init) {
        super(init);
    }

    public XnwClient(String rowGuid) {
        super(rowGuid);
    }

    public XnwClient() {}


    /**
     *客户端Id
     */
    private String clientId;


    /**
     *客户端secret
     */
    private String clientSecret;


    /**
     *服务资源标识
     */
    private String resourceIds;


    /**
     *授予客户端特定权限
     */
    private String scope;


    /**
     *令牌生成方式
     */
    private String authorizedGrantTypes;


    /**
     *客户端的重定向URI,可为空, 当grant_type为
     */
    private String webServerRedirectUri;


    /**
     *指定客户端所拥有的Spring
     */
    private String authorities;


    /**
     *设定客户端的access_token的有效时间值(单位:秒
     */
    private String accessTokenValidity;


    /**
     *设定客户端的refresh_token的有效时间值(单位:秒
     */
    private String refreshTokenValidity;


    /**
     *这是一个预留的字段,在Oauth的流程中没有实际的使用
     */
    private String additionalInformation;


    /**
     *设置用户是否自动Approval操作
     */
    private String autoapprove;


    /**
     * 设置：客户端Id
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    /**
     * 获取：客户端Id
     */
    public String getClientId() {
        return clientId;
    }
    /**
     * 设置：客户端secret
     */
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
    /**
     * 获取：客户端secret
     */
    public String getClientSecret() {
        return clientSecret;
    }
    /**
     * 设置：服务资源标识
     */
    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }
    /**
     * 获取：服务资源标识
     */
    public String getResourceIds() {
        return resourceIds;
    }
    /**
     * 设置：授予客户端特定权限
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    /**
     * 获取：授予客户端特定权限
     */
    public String getScope() {
        return scope;
    }
    /**
     * 设置：令牌生成方式
     */
    public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }
    /**
     * 获取：令牌生成方式
     */
    public String getAuthorizedGrantTypes() {
        return authorizedGrantTypes;
    }
    /**
     * 设置：客户端的重定向URI,可为空, 当grant_type为
     */
    public void setWebServerRedirectUri(String webServerRedirectUri) {
        this.webServerRedirectUri = webServerRedirectUri;
    }
    /**
     * 获取：客户端的重定向URI,可为空, 当grant_type为
     */
    public String getWebServerRedirectUri() {
        return webServerRedirectUri;
    }
    /**
     * 设置：指定客户端所拥有的Spring
     */
    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }
    /**
     * 获取：指定客户端所拥有的Spring
     */
    public String getAuthorities() {
        return authorities;
    }
    /**
     * 设置：设定客户端的access_token的有效时间值(单位:秒
     */
    public void setAccessTokenValidity(String accessTokenValidity) {
        this.accessTokenValidity = accessTokenValidity;
    }
    /**
     * 获取：设定客户端的access_token的有效时间值(单位:秒
     */
    public String getAccessTokenValidity() {
        return accessTokenValidity;
    }
    /**
     * 设置：设定客户端的refresh_token的有效时间值(单位:秒
     */
    public void setRefreshTokenValidity(String refreshTokenValidity) {
        this.refreshTokenValidity = refreshTokenValidity;
    }
    /**
     * 获取：设定客户端的refresh_token的有效时间值(单位:秒
     */
    public String getRefreshTokenValidity() {
        return refreshTokenValidity;
    }
    /**
     * 设置：这是一个预留的字段,在Oauth的流程中没有实际的使用
     */
    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
    /**
     * 获取：这是一个预留的字段,在Oauth的流程中没有实际的使用
     */
    public String getAdditionalInformation() {
        return additionalInformation;
    }
    /**
     * 设置：设置用户是否自动Approval操作
     */
    public void setAutoapprove(String autoapprove) {
        this.autoapprove = autoapprove;
    }
    /**
     * 获取：设置用户是否自动Approval操作
     */
    public String getAutoapprove() {
        return autoapprove;
    }

}
