package com.sinoy.platform.system.component.enumeration;

/**
 * 部门状态枚举
 * @author hero
 *
 */
public enum DeptStatus {

	NORMAL('E',"正常"),
	DISABLE('D',"禁用");

	private final Character code;
    private final String value;

    private DeptStatus(Character code, String value) {
        this.code = code;
        this.value = value;
    }
	public Character getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
