package com.sinoy.platform.system.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameSmsCode;

public interface FrameSmsCodeService extends BaseService<FrameSmsCode> {
    /**
     * 新增验证码
     * @param frameSms
     * @return FrameSms
     */
    int saveSms(FrameSmsCode frameSms);
}
