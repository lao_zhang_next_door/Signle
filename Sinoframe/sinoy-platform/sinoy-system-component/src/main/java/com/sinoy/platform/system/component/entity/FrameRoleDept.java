package com.sinoy.platform.system.component.entity;

/**
 * 角色部门关联表
 */
public class FrameRoleDept {

    private String roleGuid;

    private String deptGuid;

    public String getRoleGuid() {
        return roleGuid;
    }

    public FrameRoleDept setRoleGuid(String roleGuid) {
        this.roleGuid = roleGuid;
        return this;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public FrameRoleDept setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
        return this;
    }
}
