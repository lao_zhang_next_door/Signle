package com.sinoy.platform.system.component.entity;


import com.sinoy.platform.system.component.base.BaseEntity;

/**
 * 设计规范:
 * 代码项每一项的名称都要唯一
 * 代码项的每一个子项的value值也是必须唯一
 *
 * @author hero
 */
public class FrameCodeValue extends BaseEntity {

    public FrameCodeValue(Boolean init) {
        super(init);
    }

    public FrameCodeValue() {
    }

    /**
     * 显示名称
     */
    private String itemText;

    /**
     * 显示值
     */
    private String itemValue;

    /**
     * 关联guid
     */
    private String codeGuid;

    /**
     * 父级Guid
     */
    private String parentGuid;

    /**
     * 代码项颜色
     */
    private String colorStyle;

    /**
     * 字典类型
     */
    private String dictType;

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public String getCodeGuid() {
        return codeGuid;
    }

    public void setCodeGuid(String codeGuid) {
        this.codeGuid = codeGuid;
    }

    public String getParentGuid() {
        return parentGuid;
    }

    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }

    public String getColorStyle() {
        return colorStyle;
    }

    public void setColorStyle(String colorStyle) {
        this.colorStyle = colorStyle;
    }
}
