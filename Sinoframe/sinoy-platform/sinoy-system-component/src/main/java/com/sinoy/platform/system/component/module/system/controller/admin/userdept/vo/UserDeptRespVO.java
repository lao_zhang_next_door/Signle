package com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo;

import lombok.*;
import java.util.*;

//管理后台 - 多部门关联 Response VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserDeptRespVO extends UserDeptBaseVO {

    //创建时间
    private Date createTime;

}
