//package com.sino.platform.system.component.strategy;
//
//
//import com.sino.platform.system.component.service.FrameDeptService;
//import com.sino.platform.system.component.utils.CommonPropAndMethods;
//import org.springframework.stereotype.Component;
//
///**
// * 本级及以下级别的数据权限
// *
// */
//@Component("4")
//public class ThisLevelChildrenDataScope implements AbstractDataScopeHandler {
//
//	private final FrameDeptService sysDepartService;
//
//	private final CommonPropAndMethods commonPropAndMethods;
//
//	public ThisLevelChildrenDataScope(FrameDeptService sysDepartService,CommonPropAndMethods commonPropAndMethods) {
//		this.sysDepartService = sysDepartService;
//		this.commonPropAndMethods = commonPropAndMethods;
//	}
//
////	@Override
////	public List<Long> getDeptIds(RoleDto roleDto, DataScopeTypeEnum dataScopeTypeEnum) {
////		Map<String,Object> userMap = commonPropAndMethods.getCurrentUser();
////		if (userMap == null || userMap.get("deptGuid") == null) {
////			throw new BaseException("部门信息为空！");
////		}
////		return sysDepartService.selectDeptIds(Long.valueOf(deptId));
////	}
//}
