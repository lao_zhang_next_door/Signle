package com.sinoy.platform.system.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameCode;

public interface FrameCodeService extends BaseService<FrameCode> {

}
