package com.sinoy.platform.system.component.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.core.oss.core.client.FileClientConfig;


@TableName(value = "frame_attach_config", autoResultMap = true)
public class FrameAttachConfig extends BaseEntity {

    public FrameAttachConfig() {}

    public FrameAttachConfig(boolean init) {
        super(init);
    }

    public FrameAttachConfig(String rowGuid) {
        super(rowGuid);
    }

    //配置名
    private String name;

    //存储器
    private Integer storage;

    //备注
    private String remark;

    //是否为主配置
    private Boolean master;

    /**
     * 文件配置
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private FileClientConfig config;

    public FileClientConfig getConfig() {
        return config;
    }

    public FrameAttachConfig setConfig(FileClientConfig config) {
        this.config = config;
        return this;
    }

    public String getName() {
        return name;
    }

    public FrameAttachConfig setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getStorage() {
        return storage;
    }

    public FrameAttachConfig setStorage(Integer storage) {
        this.storage = storage;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public FrameAttachConfig setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public Boolean getMaster() {
        return master;
    }

    public FrameAttachConfig setMaster(Boolean master) {
        this.master = master;
        return this;
    }
}
