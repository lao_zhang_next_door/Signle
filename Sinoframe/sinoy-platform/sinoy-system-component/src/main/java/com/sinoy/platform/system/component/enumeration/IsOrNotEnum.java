package com.sinoy.platform.system.component.enumeration;

/**
 * 是否枚举
 *
 * @author T470
 */
public enum IsOrNotEnum {
    /**
     *
     */
    NOT(0, "否", false),
    IS(1, "是", true);

    private final int code;
    private final String value;
    private final boolean flag;

    IsOrNotEnum(int code, String value, boolean flag) {
        this.code = code;
        this.value = value;
        this.flag = flag;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public boolean getFlag() {
        return flag;
    }

    public static boolean getFlagByCode(int code) {
        IsOrNotEnum[] isOrNotEnums = values();
        for (IsOrNotEnum isOrNotEnum : isOrNotEnums) {
            if (isOrNotEnum.getCode() == code) {
                return isOrNotEnum.getFlag();
            }
        }
        return false;
    }
}
