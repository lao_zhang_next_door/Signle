package com.sinoy.platform.system.component.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.platform.system.component.base.BaseEntity;

import javax.persistence.Transient;
import java.util.List;

/**
 * @author jtr
 * @creatTime 2021-04-20-10:49
 **/
public class FrameDept extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
    public FrameDept(boolean init) {
		super(init);
	}

	public FrameDept() {}

	public FrameDept(String rowGuid){
    	super(rowGuid);
	}
	
	/**
	 * 部门名称
	 */
	private String deptName;
	
	/**
	 * 部门简称
	 */
	private String shortName;
	
	/**
	 * 部门编码
	 */
	private String oucode;
	
	/**
	 * 部门电话
	 */
	private String tel;
	
	/**
	 * 传真
	 */
	private String fax;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 部门简介
	 */
	private String description;
	
	/**
	 * 父级guid
	 */
	private String parentGuid;

	/**
	 * 部门级别
	 */
	private String deptCode;

	/**
	 * 部门级别
	 */
	private Integer deptLevel;

	/**
	 * 部门状态
	 */
	private Character deptStatus;

	/**
	 * 部门类型
	 */
	private Character deptType;

	/**
	 * 部门负责人id
	 */
	private Long leaderUserId;

	/**
	 * 部门负责人
	 */
	private String leaderUser;
	/**
	 * 部门负责人guid
	 */
	private String leaderUserGuid;

	/**
	 * 子部门
	 */
	@Transient
	@TableField(exist = false)
	private List<FrameDept> children;

	public Long getLeaderUserId() {
		return leaderUserId;
	}

	public void setLeaderUserId(Long leaderUserId) {
		this.leaderUserId = leaderUserId;
	}

	public Character getDeptStatus() {
		return deptStatus;
	}

	public void setDeptStatus(Character deptStatus) {
		this.deptStatus = deptStatus;
	}

	public Character getDeptType() {
		return deptType;
	}

	public void setDeptType(Character deptType) {
		this.deptType = deptType;
	}

	public String getDeptName() {
		return deptName;
	}

	public FrameDept setDeptName(String deptName) {
		this.deptName = deptName;
		return this;
	}

	public String getShortName() {
		return shortName;
	}

	public FrameDept setShortName(String shortName) {
		this.shortName = shortName;
		return this;
	}

	public String getOucode() {
		return oucode;
	}

	public FrameDept setOucode(String oucode) {
		this.oucode = oucode;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public FrameDept setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public FrameDept setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public FrameDept setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public FrameDept setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getParentGuid() {
		return parentGuid;
	}

	public FrameDept setParentGuid(String parentGuid) {
		this.parentGuid = parentGuid;
		return this;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public FrameDept setDeptCode(String deptCode) {
		this.deptCode = deptCode;
		return this;
	}

	public Integer getDeptLevel() {
		return deptLevel;
	}

	public FrameDept setDeptLevel(Integer deptLevel) {
		this.deptLevel = deptLevel;
		return this;
	}

	public List<FrameDept> getChildren()
	{
		return children;
	}

	public void setChildren(List<FrameDept> children)
	{
		this.children = children;
	}

	public String getLeaderUser() {
		return leaderUser;
	}

	public void setLeaderUser(String leaderUser) {
		this.leaderUser = leaderUser;
	}
	public String getLeaderUserGuid() {
		return leaderUserGuid;
	}

	public void setLeaderUserGuid(String leaderUserGuid) {
		this.leaderUserGuid = leaderUserGuid;
	}
}
