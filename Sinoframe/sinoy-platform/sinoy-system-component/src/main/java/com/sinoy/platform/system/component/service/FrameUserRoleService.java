package com.sinoy.platform.system.component.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.FrameUserRole;
import com.sinoy.platform.system.component.utils.Query;

import java.util.List;
import java.util.Set;

public interface FrameUserRoleService extends BaseService<FrameUserRole> {

    /**
     * 查询某个用户拥有的角色标识列表
     * @param userGuid
     * @return
     */
    List<String> selectRolesGuidsByUserGuid(String userGuid);

    /**
     * 查询某个用户拥有的角色英文名列表
     * @param userGuid
     * @return
     */
    List<String> selectRoleNamesByUserGuid(String userGuid);

    /**
     * 根据角色guid 获取 所属用户列表
     * @param query
     * @return
     */
    List<FrameUser> getLimitUserByRoleGuid(Query query);
    Integer getCountUserByRoleGuid(Query query);

    /**
     * 获取菜单(模块)权限 ['system:config:list','system:config:add']
     * @param roleGuids
     * @return
     */
    List<String> selectPermissionsByRoleGuids(List<String> roleGuids);

    /**
     * 获得拥有多个角色的用户编号集合
     *
     * @param roleIds 角色编号集合
     * @return 用户编号集合
     */
    Set<Long> getUserRoleIdListByRoleIds(Set<Long> roleIds);
}
