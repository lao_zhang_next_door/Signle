package com.sinoy.platform.system.component.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.redis.core.RedisService;
import com.sinoy.platform.system.component.dao.FrameConfigDao;
import com.sinoy.platform.system.component.entity.FrameConfig;
import com.sinoy.platform.system.component.service.FrameConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FrameConfigServiceImpl extends BaseServiceImpl<FrameConfigDao, FrameConfig> implements FrameConfigService {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RedisService redisService;

    @Value(value = "${spring.jmx.default-domain}")
    public String projectName;

    @Override
    public void cacheFrameConfig() {
        try {
            QueryWrapper<FrameConfig> wrapper = new QueryWrapper<>();
            wrapper.eq("del_flag", DelFlag.Normal.getCode());
            List<FrameConfig> frameConfigList = this.list(wrapper);
            if (frameConfigList != null && !frameConfigList.isEmpty()) {
                Map<String, String> dataMap = Maps.newHashMap();
                //@代码项转为Hash存储
                frameConfigList.forEach(frameConfig -> {
                    //这里确保名称唯一
                    dataMap.put(frameConfig.getConfigName(), frameConfig.getConfigValue());
                });
                //@存入redis
                redisService.hmset(projectName + ":frame_config", dataMap);
                logger.info("frameConfig存入redis");
            }
        } catch (Exception e) {
            logger.error("获取系统参数缓存失败", e.fillInStackTrace());
            e.printStackTrace();
        }
    }

    @Override
    public String getCacheByConfigName(String configName) {
        String temp = null;
        try {
            //防止类对应失败
            temp = redisService.hget(projectName + ":frame_config", configName).toString();
        } catch (Exception e) {
            logger.error("从缓存中获取frameConfig失败", e.fillInStackTrace());
        }
        return temp;
    }

    @Override
    public void updateConfigCacheByName(String configName) {
        try {
            QueryWrapper<FrameConfig> wrapper = new QueryWrapper<>();
            wrapper.eq("config_name", configName);
            FrameConfig frameConfig = this.getOne(wrapper);
            // 若还有配置项值存在
            if (frameConfig != null) {
                Map<String, String> dataMap = Maps.newHashMap();
                //@:代码项转为Hash存储
                dataMap.put(configName, frameConfig.getConfigValue());
                //@:存入redis
                redisService.hmset(projectName + ":frame_config", dataMap);
                logger.info("更新frameConfig存入redis");
            }
        } catch (Exception e) {
            logger.error("根据系统配置项名称更新存储失败", e.fillInStackTrace());
        }
    }

    @Override
    public void deleteConfigCacheByName(String configName) {
        redisService.del(projectName + ":frame_config:" + configName);
    }
}
