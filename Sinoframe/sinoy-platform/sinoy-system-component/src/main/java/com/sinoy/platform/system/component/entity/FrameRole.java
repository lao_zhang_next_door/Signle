package com.sinoy.platform.system.component.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.platform.system.component.base.BaseEntity;

/**
 * 角色
 */
public class FrameRole extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public FrameRole(boolean init) {
        super(init);
    }

    public FrameRole(String rowGuid) {
        super(rowGuid);
    }

    public FrameRole() {
    }

    /**
     * 角色名
     */
    private String roleName;
    /**
     * 角色类型
     */
    private String roleType;

    /**
     * 角色类型Val
     */
    @TableField(exist = false)
    private String roleTypeVal;

    /**
     * 角色标识
     */
    private String roleFlag;

    /**
     * 数据范围（1：全部数据权限 2：仅查看本人数据权限 3：本部门数据权限 4：本部门及以下数据权限 5:自定义数据权限）
     */
    private Character dataScope;

//    /**
//     * 菜单树选择项是否关联显示
//     */
//    private Boolean menuCheckStrictly;

    /**
     * 部门树选择项是否关联显示
     */
    private Boolean deptCheckStrictly;

    public Character getDataScope() {
        return dataScope;
    }

    public void setDataScope(Character dataScope) {
        this.dataScope = dataScope;
    }

//    public Boolean getMenuCheckStrictly() {
//        return menuCheckStrictly;
//    }
//
//    public void setMenuCheckStrictly(Boolean menuCheckStrictly) {
//        this.menuCheckStrictly = menuCheckStrictly;
//    }

    public Boolean getDeptCheckStrictly() {
        return deptCheckStrictly;
    }

    public void setDeptCheckStrictly(Boolean deptCheckStrictly) {
        this.deptCheckStrictly = deptCheckStrictly;
    }

    public String getRoleFlag() {
        return roleFlag;
    }

    public void setRoleFlag(String roleFlag) {
        this.roleFlag = roleFlag;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleTypeVal() {
        if (getRoleType() != null) {
            return codeValueService.getItemTextByValue("角色类型", String.valueOf(getRoleType()));
        } else {
            return "";
        }
    }

    public void setRoleTypeVal(String roleTypeVal) {
        this.roleTypeVal = roleTypeVal;
    }
}
