package com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo;

import lombok.*;
import java.util.*;
import com.sinoy.core.common.utils.request.PageParam;
import java.time.LocalDate;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

//管理后台 - 多部门关联分页 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserDeptPageReqVO extends PageParam {

    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime[] createTime;

    //创建者
    private String creator;

    //更新者
    private String updater;

    //关联部门id
    private Long deptId;

    //关联用户id
    private Long userId;

    //是否主部门
    private Integer mainDept;

}
