package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.enums.DataScopeType;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.platform.system.component.dao.FrameRoleDao;
import com.sinoy.platform.system.component.entity.*;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import com.sinoy.platform.system.component.module.system.service.rolelimit.RoleLimitService;
import com.sinoy.platform.system.component.service.FrameConfigService;
import com.sinoy.platform.system.component.service.FrameRoleDeptService;
import com.sinoy.platform.system.component.service.FrameRoleService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service
public class FrameRoleServiceImpl extends BaseServiceImpl<FrameRoleDao, FrameRole> implements FrameRoleService {

    @Autowired
    private FrameRoleDeptService frameRoleDeptService;

    @Resource
    public FrameConfigService frameConfigService;
    @Resource
    public RoleLimitService roleLimitService;
    @Resource
    public CommonPropAndMethods commonPropAndMethods;
    @Resource
    public FrameUserService frameUserService;
    @Resource
    public  FrameRoleDao frameRoleDao;

    @Override
    public List<FrameModule> getModuleByRoleGuid(String roleGuid) {
        return baseMapper.getModuleByRoleGuid(roleGuid);
    }

    @Override
    public List<String> selectDataScopeByRoleGuids(List<String> roleGuids) {
        return baseMapper.selectDataScopeByRoleGuids(roleGuids);
    }

    @Override
    public List<FrameDept> getDeptByRoleGuid(String roleGuid) {
        return baseMapper.getDeptByRoleGuid(roleGuid);
    }

    /**
     * 保存数据权限
     * @param vo
     */
    @Override
    public void saveDataPermission(FrameRole vo,List<FrameRoleDept> frameRoleDeptList) {
        FrameRole role = new FrameRole(vo.getRowGuid());
        role.setDataScope(vo.getDataScope());
        updateByRowGuid(role);

        //清除其原本的数据权限
        QueryWrapper<FrameRoleDept> delWrapper = new QueryWrapper();
        delWrapper.eq("role_guid",vo.getRowGuid());
        frameRoleDeptService.remove(delWrapper);

        //若数据权限为自定义 则另外需要保存自定义数据
        if(vo.getDataScope() == DataScopeType.CUSTOMIZE_DEPT.getCode().charAt(0) && frameRoleDeptList != null && !frameRoleDeptList.isEmpty()){
            frameRoleDeptService.saveBatch(frameRoleDeptList);
        }
    }

    @Override
    public void validRoles(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return;
        }
        // 获得角色信息
        List<FrameRole> roles = baseMapper.selectBatchIds(ids);
        Map<Long, FrameRole> roleMap = CollectionUtils.convertMap(roles, FrameRole::getRowId);
        // 校验
        ids.forEach(id -> {
            FrameRole role = roleMap.get(id);
            if (role == null) {
                throw new BaseException("该角色不存在");
            }
//            if (!CommonStatusEnum.ENABLE.getStatus().equals(role.getStatus())) {
//                throw new BaseException(ROLE_IS_DISABLE, role.getName());
//            }
        });
    }

    @Override
    public List<FrameRole> getAllRole() {
        QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_name","is_user_role_limit");
        List<FrameConfig> list=frameConfigService.list(queryWrapper);
        if(list.size()>0)
        {
            //启用角色权限控制
            if("1".equals(list.get(0).getConfigValue()))
            {
                boolean isAdmin=false;
                List<FrameRole> roleList = frameUserService.selectRoleListByUserGuid(commonPropAndMethods.getUserGuid());
                for(int i=0;i<roleList.size();i++)
                {
                    //管理员不受限制
                    if("admin".equals(roleList.get(i).getRoleFlag()))
                    {
                        isAdmin=true;
                        break;
                    }
                }
                if(!isAdmin) {
                    List<String> roleGuids = commonPropAndMethods.getCurrentRoleGuid();

                    if(roleGuids.size()>0) {
                        String roleChildIds="";
                        for (int i = 0; i < roleGuids.size(); i++) {
                            QueryWrapper<RoleLimitDO> wrapper = new QueryWrapper<>();
                            wrapper.eq("role_id", baseMapper.selectByGuid(roleGuids.get(i)).getRowId());
                            List<RoleLimitDO> roleLimitList = roleLimitService.list(wrapper);
                            if(roleLimitList.size()>0)
                            {
                                roleChildIds+=roleLimitList.get(0).getRoleChildId()+",";
                            }

                        }
                        roleChildIds=roleChildIds.substring(0,roleChildIds.length()-1);
                        String [] ids=roleChildIds.split(",");
                        ArrayList<String> arr=new ArrayList<>(Arrays.asList(ids));
                        return baseMapper.selectBatchIds(arr);

                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return this.listAll();
                }
            }
            else
            {
                return this.listAll();
            }
        }
        else{
            return this.listAll();
        }
    }
}
