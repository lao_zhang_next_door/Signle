package com.sinoy.platform.system.component.module.system.dal.dataobject.userdept;

import lombok.*;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import lombok.experimental.Accessors;

/**
 * 多部门关联 DO
 *
 * @author 管理员
 */
@TableName("frame_user_dept")
@KeySequence("frame_user_dept_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDeptDO extends BaseEntity {


    /**
    * 创建者
    */
    private String creator;



    /**
    * 更新者
    */
    private String updater;



    /**
    * 关联部门id
    */
    private Long deptId;



    /**
    * 关联用户id
    */
    private Long userId;



    /**
    * 是否主部门
    */
    private Integer mainDept;



}
