package com.sinoy.platform.system.component.module.system.dal.dataobject.post;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.platform.system.component.base.BaseEntity;
import com.sinoy.platform.system.component.entity.FrameUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户和岗位关联
 *
 */
@TableName("frame_user_post")
@KeySequence("frame_user_post_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@Accessors(chain=true)
public class UserPostDO {

    /**
     * 用户 ID
     *
     * 关联 {@link FrameUser#getRowId()}
     */
    private Long userId;
    /**
     * 角色 ID
     *
     * 关联 {@link PostDO#getRowId()} ()}
     */
    private Long postId;

}
