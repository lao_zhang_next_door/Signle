package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ReUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.sinoy.core.common.constant.ErrorCodeConstants;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.dataformat.MD5Util;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.encode.SHA256;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.RequestUtil;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.platform.system.component.dao.FrameUserDao;
import com.sinoy.platform.system.component.entity.*;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.entity.vo.FrameUserPasswordVo;
import com.sinoy.platform.system.component.enumeration.UserStatus;
import com.sinoy.platform.system.component.module.system.dal.dataobject.post.UserPostDO;
import com.sinoy.platform.system.component.module.system.dal.mysql.post.UserPostMapper;
import com.sinoy.platform.system.component.service.*;
import com.sinoy.platform.system.component.utils.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.sinoy.core.common.utils.dataformat.collection.CollectionUtils.convertList;
import static com.sinoy.core.common.utils.dataformat.collection.CollectionUtils.convertSet;


@Service
public class FrameUserServiceImpl extends BaseServiceImpl<FrameUserDao, FrameUser> implements FrameUserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

//    @Autowired
//    protected HttpServletRequest request;
//
//    @Autowired
//    protected HttpServletResponse response;

    @Autowired
    private FrameUserRoleService frameUserRoleService;

    @Autowired
    private FrameDeptService frameDeptService;

    @Autowired
    private FrameConfigService frameConfigService;

    @Autowired
    private FrameRoleService frameRoleService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FrameAttachService attachService;

    @Autowired
    private UserPostMapper userPostMapper;


    @Override
    public List<UserInfo> getList(Query query) {
        return baseMapper.getList(query);
    }

    @Override
    public Integer getListCount(Query query) {
        return baseMapper.getListCount(query);
    }

    @Override
    public UserInfo selectDetailByUserGuid(String userGuid) {
        return baseMapper.selectDetailByUserGuid(userGuid);
    }

    @Override
    public UserInfo selectDetailByLoginId(String userGuid) {
        return baseMapper.selectDetailByLoginId(userGuid);
    }

    @Override
    public List<FrameRole> selectRoleListByUserGuid(String userGuid) {
        return baseMapper.selectRoleListByUserGuid(userGuid);
    }

    @Override
    public UserInfo addFrameUser(UserInfo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())) {
            throw new BaseException("登录名不可为空");
        }

        //查询登录名是否重复
        QueryWrapper<FrameUser> countWrapper = new QueryWrapper<>();
        countWrapper.eq("login_id", frameUserPojo.getLoginId());
        countWrapper.eq("del_flag", 0);
        Long count = baseMapper.selectCount(countWrapper);
        if (count > 0) {
            throw new BaseException("已存在该登录账户");
        }

        //初始化用户并设置初始密码
        frameUserPojo.init();
        //若不存在状态字段则给与默认状态
        if (frameUserPojo.getStatus() == null) {
            frameUserPojo.setStatus(UserStatus.NORMAL.getCode());
        }

        //设置初始密码 设置失败则禁用
        if (StringUtils.isBlank(this.getInitPassword())) {
            frameUserPojo.setStatus(UserStatus.DISABLE.getCode());
        } else {
            //前端MD5 后端sha256加密后存储
            frameUserPojo.setPassword(this.getInitPassword());
        }

        //插入deptId 目前前端只传deptGuid
        String deptGuid = frameUserPojo.getDeptGuid();
        if (StringUtil.isNotBlank(deptGuid)) {
            FrameDept dept = frameDeptService.selectByGuid(deptGuid);
            if (dept != null) {
                frameUserPojo.setDeptId(dept.getRowId());
            }
        }

        // 给dept_ids赋值
       /* Set<Long> deptIds=new HashSet<>();
        if (CollectionUtil.isNotEmpty(frameUserPojo.getDeptIds_jz())) {
            Iterator<String> it = frameUserPojo.getDeptIds_jz().iterator();
            while (it.hasNext()) {
                String dguid = it.next();
                FrameDept dept=frameDeptService.selectByGuid(dguid);
                deptIds.add(dept.getRowId());
            }
            frameUserPojo.setFrameUser(frameUserPojo.getFrameUser()).setDeptIds(deptIds);
        }*/

        baseMapper.insert(frameUserPojo);

        /**
         * vue2 改造成 新增用户时不会插入用户角色信息
         * 先生成用户 再分配角色  支持无角色用户生成
         * vue3 端插入用户所拥有的角色 vue3前端只传roleGuid
         */
        List<String> roleGuids = frameUserPojo.getRoleGuids();
        List<FrameUserRole> frameUserRoleList = new ArrayList<>();
        FrameUserRole fur;
        if (roleGuids != null) {
            for (String roleGuid : roleGuids) {
                fur = new FrameUserRole(true);
                fur.setUserGuid(frameUserPojo.getRowGuid());
                fur.setRoleGuid(roleGuid);
                frameUserRoleList.add(fur);
            }
        }
        if (!frameUserRoleList.isEmpty()) {
            frameUserRoleService.saveBatch(frameUserRoleList);
        }

        // 插入关联岗位
        if (CollectionUtil.isNotEmpty(frameUserPojo.getPostIds())) {
            userPostMapper.insertBatch(convertList(frameUserPojo.getPostIds(),
                    postId -> new UserPostDO().setUserId(frameUserPojo.getRowId()).setPostId(postId)));
        }

        // 插入关联部门
       /* if (CollectionUtil.isNotEmpty(frameUserPojo.getDeptIds_jz())) {
            for (Long id : deptIds) {
               UserDeptDO userDeptDO=new UserDeptDO();
               userDeptDO.initNull();
               userDeptDO.setDeptId(id);
               userDeptDO.setUserId(frameUserPojo.getRowId());
               userDeptDO.setMainDept(0);
               userDeptDO.setCreator(commonPropAndMethods.getUserName());
               userDeptDO.setUpdater(commonPropAndMethods.getUserName());
               userDeptMapper.insert(userDeptDO);
            }
            *//*userDeptMapper.insertBatch(convertList(deptIds,
                     deptId-> new UserDeptDO().setUserId(frameUserPojo.getRowId()).setDeptId(deptId)));*//*
        }*/

        return frameUserPojo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateFrameUser(UserInfo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())
                || StringUtil.isBlank(frameUserPojo.getRowGuid())) {
            throw new BaseException("参数不可为空");
        }

        QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("row_guid", frameUserPojo.getRowGuid());
        FrameUser originUser = baseMapper.selectOne(queryWrapper);

        if (StringUtil.isNotBlank(frameUserPojo.getPassword())) {
            throw new BaseException("参数不可更新");
        }

        if (!frameUserPojo.getLoginId().equals(originUser.getLoginId())) {
            throw new BaseException("登录名不可修改");
        }

        //若deptGuid需要更新 则同步更新deptId
        String originDeptGuid = originUser.getDeptGuid();
        if (StringUtil.isNotBlank(originDeptGuid) && !originDeptGuid.equals(frameUserPojo.getDeptGuid())) {
            FrameDept dept = frameDeptService.selectByGuid(frameUserPojo.getDeptGuid());
            if (dept != null) {
                frameUserPojo.setDeptId(dept.getRowId());
            }
        }

        QueryWrapper<FrameUser> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("row_guid", frameUserPojo.getRowGuid());

        // 给dept_ids赋值
       /* Set<Long> deptIds=new HashSet<>();
        if (CollectionUtil.isNotEmpty(frameUserPojo.getDeptIds_jz())) {
            Iterator<String> it = frameUserPojo.getDeptIds_jz().iterator();
            while (it.hasNext()) {
                String dguid = it.next();
                FrameDept dept=frameDeptService.selectByGuid(dguid);
                deptIds.add(dept.getRowId());
            }
            frameUserPojo.setFrameUser(frameUserPojo.getFrameUser()).setDeptIds(deptIds);
        }
        else{
            frameUserPojo.setFrameUser(frameUserPojo.getFrameUser()).setDeptIds(deptIds);
        }*/

        baseMapper.update(frameUserPojo, updateWrapper);

        /**
         * vue2 改造成 新增用户时不会插入用户角色信息
         * 先生成用户 再分配角色  支持无角色用户生成
         * vue3 端插入用户所拥有的角色 vue3前端只传roleGuid
         */
        List<String> roleGuids = frameUserPojo.getRoleGuids();
        List<FrameUserRole> userRoles = new ArrayList<>();
        if (roleGuids != null && !roleGuids.isEmpty()) {
            //删除
            QueryWrapper<FrameUserRole> deleteWrapper = new QueryWrapper<>();
            deleteWrapper.eq("user_guid", frameUserPojo.getRowGuid());
            frameUserRoleService.remove(deleteWrapper);
            //再新增
            for (String roleGuid : roleGuids) {
                FrameUserRole frameUserRole = new FrameUserRole(true)
                        .setRoleGuid(roleGuid)
                        .setUserGuid(frameUserPojo.getRowGuid());
                userRoles.add(frameUserRole);
            }
        }
        frameUserRoleService.saveBatch(userRoles);

        //更新岗位
        updateUserPost(frameUserPojo);

        // 插入关联部门
        /*if (CollectionUtil.isNotEmpty(frameUserPojo.getDeptIds_jz())) {
            //先删除老的，在新增新的
            QueryWrapper<UserDeptDO> wrapper=new QueryWrapper<>();
            wrapper.eq("user_id",frameUserPojo.getRowId());
            userDeptMapper.delete(wrapper);

            for (Long id : deptIds) {
                UserDeptDO userDeptDO=new UserDeptDO();
                userDeptDO.initNull();
                userDeptDO.setDeptId(id);
                userDeptDO.setUserId(frameUserPojo.getRowId());
                userDeptDO.setMainDept(0);
                userDeptDO.setCreator(commonPropAndMethods.getUserName());
                userDeptDO.setUpdater(commonPropAndMethods.getUserName());
                userDeptMapper.insert(userDeptDO);
            }
        }
        else
        {
            QueryWrapper<UserDeptDO> wrapper=new QueryWrapper<>();
            wrapper.eq("user_id",frameUserPojo.getRowId());
            userDeptMapper.delete(wrapper);
        }*/
    }

    private void updateUserPost(UserInfo frameUserPojo) {
        Long userId = frameUserPojo.getRowId();
        Set<Long> dbPostIds = convertSet(userPostMapper.selectListByUserId(userId), UserPostDO::getPostId);
        // 计算新增和删除的岗位编号
        Set<Long> postIds = frameUserPojo.getPostIds();
        if (postIds == null) {
            return;
        }
        Collection<Long> createPostIds = CollUtil.subtract(postIds, dbPostIds);
        Collection<Long> deletePostIds = CollUtil.subtract(dbPostIds, postIds);
        // 执行新增和删除。对于已经授权的菜单，不用做任何处理
        if (!CollectionUtil.isEmpty(createPostIds)) {
            userPostMapper.insertBatch(convertList(createPostIds,
                    postId -> new UserPostDO().setUserId(userId).setPostId(postId)));
        }
        if (!CollectionUtil.isEmpty(deletePostIds)) {
            userPostMapper.deleteByUserIdAndPostId(userId, deletePostIds);
        }
    }

//    @Override
//    public void updateCacheUser(String userGuid) {
//        QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("row_guid", userGuid);
//        FrameUser user = baseMapper.selectOne(queryWrapper);
//
//        QueryWrapper<FrameDept> deptWrapper = new QueryWrapper<>();
//        deptWrapper.eq("row_guid", user.getDeptGuid());
//        FrameDept dept = frameDeptService.getOne(deptWrapper);
//        String deptName = "";
//        if (dept != null) {
//            deptName = dept.getDeptName();
//        }

//        // 读取token
//        String headerToken = request.getHeader(Oauth2Constant.HEADER_TOKEN);
//        String token = TokenUtil.getToken(headerToken);
//        OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
//        // 读取认证信息
//        OAuth2Authentication authentication = tokenStore.readAuthentication(accessToken);
//        Object principal = authentication.getPrincipal();
//        if (principal instanceof XnwUser) {
//            ((XnwUser) principal).setDeptGuid(user.getDeptGuid());
//            ((XnwUser) principal).setDeptName(deptName);
//            ((XnwUser) principal).setMobile(user.getMobile());
//            ((XnwUser) principal).setNickName(user.getUserName());
//        }
//        tokenStore.storeAccessToken(accessToken, authentication);

//        // 构建登录用户
//        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(Convert.toStrArray(userInfo.getPermissions()));
//        XnwUser user = new XnwUser(user.getLoginId(), "{SHA-256}" + user.getPassword(), userInfo.getIsAdmin(),authorities,userInfo.getDataScope()
//                ,user.getRowId(), user.getRowGuid(), user.getDeptId(), user.getDeptGuid(), userInfo.getDeptName()
//                , userInfo.getPdeptGuid(), userInfo.getPdetpName(), user.getUserName(), user.getMobile(), userInfo.getHeadImgUrl(), null
//                , user.getEmail(), userInfo.getRoleNameList(), userInfo.getDeptCode(), userInfo.getRoleGuids());
//
//        //待调整
//        SecurityFrameworkUtils.setLoginUser(user,request);
//    }

    @Override
    public String getInitPassword() {
        QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_name", "initPassword");
        FrameConfig config = frameConfigService.getOne(queryWrapper);

        if (config == null) {
            logger.info("未配置初始密码,获取初始密码异常");
            return "";
        }

        //加密
        if (!StringUtils.isBlank(config.getConfigValue())) {
            return SHA256.getSHA256StrJava(MD5Util.md5Password(config.getConfigValue()).toLowerCase());
        } else {
            return "";
        }
    }

    @Override
    public void changePassword(FrameUserPasswordVo frameUserPasswordVo) {
        RequestUtil.isFieldBlank(frameUserPasswordVo, "userGuid", "originPassword", "newPassword");

        if (frameUserPasswordVo.getNewPassword().equals(frameUserPasswordVo.getOriginPassword())) {
            throw new BaseException(1000, "修改后的密码不可与原来的一致");
        }

        if (!frameUserPasswordVo.getNewPassword()
                .equals(frameUserPasswordVo.getVerifyPassword())) {
            throw new BaseException(1000, "两次输入密码不一致");
        }

        QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("row_guid", frameUserPasswordVo.getUserGuid());
        FrameUser frameUser = baseMapper.selectOne(queryWrapper);
        if (frameUser == null) {
            throw new BaseException(1000, "用户不存在,修改密码失败");
        }
        if (!frameUser.getPassword().equals(SHA256.getSHA256StrJava(MD5Util.md5Password(frameUserPasswordVo.getOriginPassword()).toLowerCase()))) {
            throw new BaseException(1000, "旧密码错误");
        }

        //判断密码强度 包含大小写字母，数字，特殊字符，大于8位，不能有连续的三位数字
        boolean match = isPasswordStrong(frameUserPasswordVo.getNewPassword());
        if (!match) {
            throw new BaseException(1000, "新密码强度不符合规则");
        }
        FrameUser user = new FrameUser(frameUser.getRowGuid())
                .setPassword(SHA256.getSHA256StrJava(MD5Util.md5Password(frameUserPasswordVo.getNewPassword()).toLowerCase()))
                .setIsUpdateInitialPassword(1);

        QueryWrapper<FrameUser> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("row_guid", frameUser.getRowGuid());
        baseMapper.update(user, updateWrapper);

    }


    /**
     * 判断是否是强密码(包含大小写字母，数字，特殊字符，)
     * @param password
     * @return
     */
    public static boolean isPasswordStrong(String password) {
        // 检查长度是否超过8位
        if (password.length() <= 8) {
            return false;
        }

        // 检查是否包含大小写字母
        boolean hasUpperCase = false;
        boolean hasLowerCase = false;
        // 检查是否包含数字
        boolean hasDigit = false;
        // 检查是否包含特殊字符
        boolean hasSpecialChar = false;



        Set<Character> specialChars = new HashSet<>(Arrays.asList('!', '@', '#', '$', '%', '^', '&', '*'));

        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            if (Character.isUpperCase(c)) {
                hasUpperCase = true;
            } else if (Character.isLowerCase(c)) {
                hasLowerCase = true;
            } else if (Character.isDigit(c)) {
                hasDigit = true;
            } else if(specialChars.contains(c)) {
                hasSpecialChar = true;
            }

        }

        //检查是否键盘上连续3位或者以上
        boolean rsThree=validateKey(password);

        //检查是否存在三个或者三个以上相同
        boolean check3=check3(password);

        // 检查是否所有条件都满足
        return hasUpperCase && hasLowerCase && hasDigit && hasSpecialChar&&rsThree&&check3;
    }
    /***
     * 描述: 三个或者三个以上相同
     *
     * @param pwd 密码
     * @return String
     */
    public static boolean check3(String pwd) {

        String regx = "^.*(.)\\1{2}.*$";
        Matcher m = null;
        Pattern p = null;
        p = Pattern.compile(regx);
        m = p.matcher(pwd);
        if (m.matches()) {
            return false;
        } else {
            return true;

        }

    }
    public static boolean validateKey(String str) {

        //定义横向穷举
        String[][] keyCode = {
                {"`~·", "1=", "2@@", "3#", "4$￥", "5%", "6^……", "7&", "8*", "9(（", "0）)", "-_", "=+"},
                {" ", "qQ", "wW", "eE", "rR", "tT", "yY", "uU", "iI", "oO", "pP", "[{【", "]}】", "\\|、"},
                {" ", "aA", "sS", "dD", "fF", "gG", "hH", "jJ", "kK", "lL", ";:", "\'\"’“"},
                {" ", "zZ", "xX", "cC", "vV", "bB", "nN", "mM", ",《<", ".>》", "/?？"}
        };

        //找出给出的字符串，每个字符，在坐标系中的位置。
        char[] c = str.toCharArray();
        List<Integer> x = new ArrayList<Integer>();
        List<Integer> y = new ArrayList<Integer>();
        for (int i = 0; i < c.length; i++) {
            char temp = c[i];
            toHere:
            for (int j = 0; j < keyCode.length; j++) {
                for (int k = 0; k < keyCode[j].length; k++) {
                    String jk = keyCode[j][k];
                    if (jk.contains(String.valueOf(temp))) {
                        x.add(j);
                        y.add(k);
                        break toHere;
                    }
                }
            }
        }
        boolean flag = false;
        for (int i = 0; i < x.size() - 2; i++) {
            // 如果X一致，那么就是在一排
            if (x.get(i) == x.get(i + 1) && x.get(i + 1) == x.get(i + 2)) {//四者在同一行上
                if (y.get(i) > y.get(i + 2)) {
                    if (y.get(i) - 1 == y.get(i + 1) && y.get(i) - 2 == y.get(i + 2)) {
                        flag = true;
                        break;
                    }
                } else {
                    if (y.get(i) + 1 == y.get(i + 1) && y.get(i) + 2 == y.get(i + 2)) {
                        flag = true;
                        break;
                    }
                }

            } else if (x.get(i) != x.get(i + 1)
                    && x.get(i + 1) != x.get(i + 2)
                    && x.get(i) != x.get(i + 2)
            ) {//四者均不在同一行上,但是如果y相同，说明是一列
                if (y.get(i) == y.get(i + 1) && y.get(i + 1) == y.get(i + 2)) {
                    flag = true;
                    break;
                }
            }

        }
        if (flag) {
            return false;

        } else {
            return true;
        }
    }

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     *
     * @return
     */
    @Override
    public List<FrameUser> getParentDeptUser() {
        Map<String, Object> userMap = commonPropAndMethods.getCurrentUser();
        QueryWrapper<FrameDept> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", String.valueOf(userMap.get("deptGuid")));
        FrameDept dept = frameDeptService.getOne(wrapper);
        QueryWrapper<FrameUser> userWrapper = new QueryWrapper<>();
        userWrapper.eq("dept_guid", dept.getParentGuid());
        return baseMapper.selectList(userWrapper);
    }

    @Override
    public void setRole(String userGuid, List<String> roleGuids) {

        List<FrameUserRole> userRoles = new ArrayList<>();
        //删除
        QueryWrapper<FrameUserRole> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("user_guid", userGuid);
        frameUserRoleService.remove(deleteWrapper);

        if (roleGuids != null && roleGuids.size() > 0) {
            //再新增
            for (String roleGuid : roleGuids) {
                FrameUserRole frameUserRole = new FrameUserRole(true)
                        .setRoleGuid(roleGuid)
                        .setUserGuid(userGuid);
                userRoles.add(frameUserRole);
            }
        }

        frameUserRoleService.saveBatch(userRoles);

    }

    @Override
    public boolean isPasswordMatch(String rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    @Override
    public String updateUserAvatar(String loginId, InputStream avatarFile) {
        checkUserExists(loginId);
        // 存储文件
        String formRowGuid = UUID.randomUUID().toString();
        FrameAttach attach = attachService.createFile(null, null, IoUtil.readBytes(avatarFile), formRowGuid);
        // 更新路径
        FrameUser user = new FrameUser();
        UpdateWrapper<FrameUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("headimg_guid", formRowGuid);
        updateWrapper.eq("login_id", loginId);
        update(updateWrapper);
        return attach.getUrl();
    }

    @Override
    public void validUsers(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return;
        }
        // 获得岗位信息
        List<FrameUser> users = baseMapper.selectBatchIds(ids);
        Map<Long, FrameUser> userMap = CollectionUtils.convertMap(users, FrameUser::getRowId);
        // 校验
        ids.forEach(id -> {
            FrameUser user = userMap.get(id);
            if (user == null) {
                throw new BaseException("该用户不存在");
            }
            if (!CommonStatusEnum.ENABLE.getStatus().equals(user.getStatus())) {
                throw new BaseException("{}用户已被禁用", user.getUserName());
            }
        });
    }

    @Override
    public List<FrameUser> getUsersByDeptIds(Set<Long> deptIds) {
        if (CollUtil.isEmpty(deptIds)) {
            return Collections.emptyList();
        }
        List<FrameUser> users = baseMapper.selectListByDeptIds(deptIds);
        return users;
    }

    @Override
    public Map<Long, FrameUser> getUserMap(Collection<Long> ids) {
        List<FrameUser> users = null;
        if (CollUtil.isEmpty(ids)) {
            users = new ArrayList();
        }
        users = getBaseMapper().selectBatchIds(ids);

        // 批量设置头像 为app端预留
        users.forEach((user) -> {
            if (user != null) {
                FrameAttach avatar = attachService.getOne(
                        new LambdaQueryWrapper<FrameAttach>().eq(FrameAttach::getFormRowGuid, user.getHeadimgGuid())
                                .orderByDesc(BaseEntity::getCreateTime).last(" limit 1"));
                if (avatar != null) {
                    user.setHeadimgUrl(avatar.getContentUrl());
                }
            }
        });

        return CollectionUtils.convertMap(users, FrameUser::getRowId);

    }

    @Override
    public List<FrameUser> getUsersByStatus(Integer status) {
        return baseMapper.selectListByStatus(status);
    }

    @Override
    public List<FrameUser> getUsersByPostIds(Collection<Long> postIds) {
        if (CollUtil.isEmpty(postIds)) {
            return Collections.emptyList();
        }
        Set<Long> userIds = convertSet(userPostMapper.selectListByPostIds(postIds), UserPostDO::getUserId);
        if (CollUtil.isEmpty(userIds)) {
            return Collections.emptyList();
        }
        return baseMapper.selectBatchIds(userIds);
    }

    @Override
    public List<FrameUser> getUnityLeaderRole(String deptCode, String roleFlag) {
        return baseMapper.getUnityLeaderRole(deptCode, roleFlag);
    }

    @Override
    public List<FrameUser> getUserByRoleFlag(String roleFlag, Long deptId, List<String> excludeUserGuids) {
        FrameRole role = frameRoleService.getOne(new LambdaQueryWrapper<FrameRole>().eq(FrameRole::getRoleFlag, roleFlag));
        Optional.ofNullable(role).orElseThrow(() -> new BaseException("未找到该角色"));
        List<FrameUserRole> userRoleList = frameUserRoleService.list(new LambdaQueryWrapper<FrameUserRole>()
                .select(FrameUserRole::getUserGuid).eq(FrameUserRole::getRoleGuid, role.getRowGuid()));

        Stream<FrameUserRole> stream = userRoleList.stream();
        if (excludeUserGuids != null && excludeUserGuids.size() > 0) {
            stream = stream.filter(i -> !excludeUserGuids.contains(i.getUserGuid()));
        }
        List<String> userGuids = stream.map((frameUserRole) -> {
            return frameUserRole.getUserGuid();
        }).collect(Collectors.toList());
        if (userGuids == null || userGuids.isEmpty()) {
            throw new BaseException("未找到该角色对应用户");
        }
        List<FrameUser> users = list(new LambdaQueryWrapper<FrameUser>()
                .eq(FrameUser::getDelFlag, DelFlag.Normal.getCode()).in(FrameUser::getRowGuid, userGuids)
                .eq(deptId != null, FrameUser::getDeptId, deptId));
        return users;
    }


    /**
     * 根据登录id查询用户信息
     *
     * @param loginId
     * @return
     */
    @Override
    public UserInfo loadUserByLoginId(String loginId) {
        QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
        wrapper.eq("login_id", loginId);
        wrapper.eq("del_flag", DelFlag.Normal.getCode());
        return getUserInfo(baseMapper.selectOne(wrapper));
    }

    @Override
    public UserInfo loadUserByUserId(Long userId) {
        return getUserInfo(baseMapper.selectById(userId));
    }

    /**
     * 检查用户是否存在
     */
    public void checkUserExists(String loginId) {
        if (StringUtil.isBlank(loginId)) {
            return;
        }
        FrameUser user = new FrameUser();
        user.setLoginId(loginId);
        user.setDelFlag(DelFlag.Normal.getCode());
        FrameUser resUser = selectOneByEntity(user);
        if (resUser == null) {
            throw new BaseException(ErrorCodeConstants.USER_NOT_EXISTS);
        }
    }

    public UserInfo getUserInfo(FrameUser frameUser) {
        if (frameUser == null) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setFrameUser(frameUser);
        /**
         * 权限模块暂时还未改造 目前依然是根据角色判断
         */
        //角色信息
        List<String> roleGuids = frameUserRoleService.selectRolesGuidsByUserGuid(frameUser.getRowGuid());
        List<String> roleNames = frameUserRoleService.selectRoleNamesByUserGuid(frameUser.getRowGuid());
        //菜单权限
        List<String> permissions = frameUserRoleService.selectPermissionsByRoleGuids(roleGuids);
        //数据权限
        List<String> dataScope = frameRoleService.selectDataScopeByRoleGuids(roleGuids);
        //部门信息
        FrameDeptDto deptPojo = frameDeptService.selectDeptAndParentDept(frameUser.getDeptGuid());

        //设置头像
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("form_row_guid", frameUser.getHeadimgGuid());
        queryWrapper.orderByDesc("row_id");
        FrameAttach avatar = attachService.getOne(queryWrapper);
        if (avatar != null) {
            userInfo.setHeadImgUrl(avatar.getUrl());
        }

        userInfo.setPermissions(permissions);
        if (deptPojo != null) {
            userInfo.setDeptName(deptPojo.getDeptName());
            userInfo.setDeptCode(deptPojo.getDeptCode());
            userInfo.setPdeptGuid(deptPojo.getParentDeptGuid());
            userInfo.setPdetpName(deptPojo.getParentDeptName());
            //存在部门信息再存数据权限
            userInfo.setDataScope(dataScope);
        }
        userInfo.setRoleGuids(roleGuids);
        userInfo.setRoleNameList(roleNames);
        userInfo.setIsAdmin(roleNames.contains("admin"));
        return userInfo;
    }

    @Override
    public FrameDept getCurrentDept(Long userId, Long deptId) {
        // 判断是否存在兼职部门
        /*UserDeptDO userDeptDO = userDeptMapper.selectMainDept(userId);
        if(userDeptDO == null){
            // 返回所在主部门
            return frameDeptService.getById(deptId);
        }

        // 返回兼职部门
        return frameDeptService.getById(userDeptDO.getDeptId());*/

        return frameDeptService.getById(deptId);
    }


}
