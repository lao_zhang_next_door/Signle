package com.sinoy.platform.system.component.service;

import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.core.oss.core.client.FileClient;
import com.sinoy.platform.system.component.entity.FrameAttachConfig;


public interface FrameAttachConfigService extends BaseService<FrameAttachConfig> {

    /**
     * 初始化文件客户端
     */
    void initFileClients();

    /**
     * 获得 Master 文件客户端
     *
     * @return 文件客户端
     */
    FileClient getMasterFileClient();

    /**
     * 获得指定编号的文件客户端
     *
     * @param id 配置编号
     * @return 文件客户端
     */
    FileClient getFileClient(Long id);

    /**
     * 获取当前主配置配置项
     * @param id
     * @return
     */
    JSONObject getFileConfig();

    /**
     * 更新文件配置
     * @param frameAttachConfig
     */
    void updateFileAttachConfig(FrameAttachConfig frameAttachConfig);

    /**
     * 更新为主配置
     * @param masterRowGuid
     */
    void updateMaster(String masterRowGuid);
}
