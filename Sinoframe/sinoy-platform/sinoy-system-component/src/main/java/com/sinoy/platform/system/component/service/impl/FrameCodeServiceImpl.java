package com.sinoy.platform.system.component.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameCodeDao;
import com.sinoy.platform.system.component.entity.FrameCode;
import com.sinoy.platform.system.component.service.FrameCodeService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

@Service
public class FrameCodeServiceImpl extends BaseServiceImpl<FrameCodeDao, FrameCode> implements FrameCodeService {

}
