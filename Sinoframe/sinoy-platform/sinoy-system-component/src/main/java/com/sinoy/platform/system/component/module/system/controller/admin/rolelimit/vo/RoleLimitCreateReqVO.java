package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo;

import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

//管理后台 - 角色权限创建 Request VO
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RoleLimitCreateReqVO extends RoleLimitBaseVO {


}
