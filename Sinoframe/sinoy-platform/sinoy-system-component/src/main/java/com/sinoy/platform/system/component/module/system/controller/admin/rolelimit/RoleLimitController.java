package com.sinoy.platform.system.component.module.system.controller.admin.rolelimit;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitPageReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.rolelimit.vo.RoleLimitUpdateReqVO;
import com.sinoy.platform.system.component.module.system.convert.rolelimit.RoleLimitConvert;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import com.sinoy.platform.system.component.module.system.service.rolelimit.RoleLimitService;
import com.sinoy.platform.system.component.service.FrameRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;


//管理后台 - 角色权限
@RestController
@RequestMapping("/system/role-limit")
@Validated
public class RoleLimitController {

    @Resource
    private RoleLimitService roleLimitService;
    @Resource
    private FrameRoleService frameRoleService;

    //创建角色权限
    @PostMapping("/create")
    public R createRoleLimit(@Valid @RequestBody RoleLimitCreateReqVO createReqVO) {

        String roleChildNames = "";
        for (int i = 0; i < createReqVO.getRoleChildId().split(",").length; i++) {
            Long id = Long.valueOf(createReqVO.getRoleChildId().split(",")[i]);
            FrameRole role = frameRoleService.getById(id);
            roleChildNames += role.getRoleName() + ",";
        }
        createReqVO.setRoleChildName(roleChildNames.substring(0, roleChildNames.length() - 1));

        Long rowId = roleLimitService.createRoleLimit(createReqVO);
        if (rowId < 0) {
            return R.error("该角色名称已经存在");
        }
        return R.ok().put("data", roleLimitService.createRoleLimit(createReqVO));
    }

    //更新角色权限
    @PutMapping("/update")
    public R updateRoleLimit(@Valid @RequestBody RoleLimitUpdateReqVO updateReqVO) {

        String roleChildNames = "";
        for (int i = 0; i < updateReqVO.getRoleChildId().split(",").length; i++) {
            Long id = Long.valueOf(updateReqVO.getRoleChildId().split(",")[i]);
            FrameRole role = frameRoleService.getById(id);
            roleChildNames += role.getRoleName() + ",";
        }
        updateReqVO.setRoleChildName(roleChildNames.substring(0, roleChildNames.length() - 1));

        roleLimitService.updateRoleLimit(updateReqVO);
        return R.ok();
    }

    //删除角色权限
    @DeleteMapping("/delete")
    public R deleteRoleLimit(@RequestParam("rowId") Long id) {
        roleLimitService.deleteRoleLimit(id);
        return R.ok();
    }

    //批量删除角色权限
    @DeleteMapping("/deleteBatch")
    public R deleteRoleLimitBatch(@RequestBody Long[] ids) {
        roleLimitService.deleteBatchByIds(ids);
        return R.ok();
    }

    //获得角色权限
    @GetMapping("/get")
    public R getRoleLimit(@RequestParam("id") Long id) {
        RoleLimitDO roleLimit = roleLimitService.getRoleLimit(id);
        return R.ok().put("data", RoleLimitConvert.INSTANCE.convert(roleLimit));
    }

    //获得角色权限列表
    @GetMapping("/list")
    public R getRoleLimitList(@RequestParam("ids") Collection<Long> ids) {
        List<RoleLimitDO> list = roleLimitService.getRoleLimitList(ids);
        return R.ok().put("data", RoleLimitConvert.INSTANCE.convertList(list));
    }


    //获得角色权限分页
    @GetMapping("/page")
    public R getRoleLimitPage(@Valid RoleLimitPageReqVO pageVO) {
        Page<RoleLimitDO> pageResult = roleLimitService.getRoleLimitPage(pageVO);
        return R.ok().put("data", RoleLimitConvert.INSTANCE.convertPage(pageResult));
    }


}
