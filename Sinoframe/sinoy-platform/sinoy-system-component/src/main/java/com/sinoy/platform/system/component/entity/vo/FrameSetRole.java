package com.sinoy.platform.system.component.entity.vo;

import java.util.List;

public class FrameSetRole {

    private String userGuid;

    private List<String> roleGuids;

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public List<String> getRoleGuids() {
        return roleGuids;
    }

    public void setRoleGuids(List<String> roleGuids) {
        this.roleGuids = roleGuids;
    }
}
