package com.sinoy.platform.system.component.enumeration;

/**
 * 用户状态枚举
 * @author hero
 *
 */
public enum UserStatus {

	NORMAL(1,"正常"),
	DISABLE(0,"禁用");
	
	private final int code;
    private final String value;
    private UserStatus(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
