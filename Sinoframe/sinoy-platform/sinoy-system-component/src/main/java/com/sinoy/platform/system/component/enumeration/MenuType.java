package com.sinoy.platform.system.component.enumeration;

/**
 * 菜单类型枚举
 */
public enum MenuType {

    ML('M',"目录"),
    CD('C',"菜单"),
    AN('A',"按钮");

    private final Character code;
    private final String value;
    private MenuType(Character code, String value) {
        this.code = code;
        this.value = value;
    }
    public Character getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
