package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameRoleDeptDao;
import com.sinoy.platform.system.component.entity.FrameRoleDept;
import com.sinoy.platform.system.component.service.FrameRoleDeptService;
import org.springframework.stereotype.Service;


@Service
public class FrameRoleDeptServiceImpl extends BaseServiceImpl<FrameRoleDeptDao, FrameRoleDept> implements FrameRoleDeptService {

 
}
