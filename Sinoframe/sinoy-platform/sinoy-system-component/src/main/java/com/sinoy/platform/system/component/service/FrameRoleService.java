package com.sinoy.platform.system.component.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameRoleDept;

import java.util.Collection;
import java.util.List;

public interface FrameRoleService extends BaseService<FrameRole> {

    /**
     * 根据角色guid 查询该角色所拥有的菜单模块
     * @param roleGuid
     * @return
     */
    List<FrameModule> getModuleByRoleGuid(String roleGuid);

    /**
     * 根据角色guid 组 获取数据权限
     * @param roleGuids
     * @return
     */
    List<String> selectDataScopeByRoleGuids(List<String> roleGuids);

    /**
     * 根据角色guid 获取部门列表
     * @param roleGuid
     * @return
     */
    List<FrameDept> getDeptByRoleGuid(String roleGuid);

    /**
     * 保存数据权限
     * @param vo
     */
    void saveDataPermission(FrameRole vo, List<FrameRoleDept> frameRoleDeptList);

    /**
     * 校验角色们是否有效。如下情况，视为无效：
     * 1. 角色编号不存在
     * 2. 角色被禁用
     *
     * @param ids 角色编号数组
     */
    void validRoles(Collection<Long> ids);

    /**
     * 获取角色列表
     * @return
     */
    List<FrameRole> getAllRole();
}
