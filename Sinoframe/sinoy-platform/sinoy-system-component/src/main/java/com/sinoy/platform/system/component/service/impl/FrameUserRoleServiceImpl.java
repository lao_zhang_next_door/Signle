package com.sinoy.platform.system.component.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameUserRoleDao;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.FrameUserRole;
import com.sinoy.platform.system.component.entity.pojo.FrameUserRolePojo;
import com.sinoy.platform.system.component.service.FrameUserRoleService;
import com.sinoy.platform.system.component.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.sinoy.core.common.utils.dataformat.collection.CollectionUtils.convertSet;


@Service
public class FrameUserRoleServiceImpl extends BaseServiceImpl<FrameUserRoleDao, FrameUserRole> implements FrameUserRoleService {

    @Override
    public List<String> selectRolesGuidsByUserGuid(String userGuid) {
        QueryWrapper<FrameUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_guid", userGuid);
        List<FrameUserRole> frameUserRoleList = this.baseMapper.selectList(queryWrapper);
        return frameUserRoleList.stream().map(FrameUserRole::getRoleGuid).collect(Collectors.toList());
    }

    @Override
    public List<String> selectRoleNamesByUserGuid(String userGuid) {
        return baseMapper.selectRoleNamesByUserGuid(userGuid);
    }

    @Override
    public List<FrameUser> getLimitUserByRoleGuid(Query query) {
        return baseMapper.getLimitUserByRoleGuid(query);
    }

    @Override
    public Integer getCountUserByRoleGuid(Query query) {
        return baseMapper.getCountUserByRoleGuid(query);
    }

    /**
     * 获取菜单（模块）权限 ['system:config:list','system:config:add']
     * @param roleGuids
     * @return
     */
    @Override
    public List<String> selectPermissionsByRoleGuids(List<String> roleGuids) {
        return baseMapper.selectPermissionsByRoleGuids(roleGuids);
    }

    @Override
    public Set<Long> getUserRoleIdListByRoleIds(Set<Long> roleIds) {
        return convertSet(baseMapper.selectListByRoleIds(roleIds),
                FrameUserRolePojo::getUserId);
    }
}
