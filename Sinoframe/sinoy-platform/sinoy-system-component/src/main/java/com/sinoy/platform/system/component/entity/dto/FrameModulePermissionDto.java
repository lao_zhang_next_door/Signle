package com.sinoy.platform.system.component.entity.dto;


import com.sinoy.platform.system.component.entity.FrameModulePermission;

public class FrameModulePermissionDto extends FrameModulePermission {
	
	private String roleName;

	private String roleFlag;

	public String getRoleFlag() {
		return roleFlag;
	}

	public FrameModulePermissionDto setRoleFlag(String roleFlag) {
		this.roleFlag = roleFlag;
		return this;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
