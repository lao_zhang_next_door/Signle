package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameCode;

public interface FrameCodeDao extends ExtendMapper<FrameCode> {

	
}
