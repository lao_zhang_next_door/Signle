package com.sinoy.platform.system.component.entity.pojo;

import com.sinoy.platform.system.component.entity.FrameCodeValue;

import javax.persistence.Transient;
import java.util.List;

/**
 * 代码项参数pojo
 *
 * @author wzl
 */
public class FrameCodeValuePojo extends FrameCodeValue {

    /**
     * 父级名称
     */
    private String parentGuidVal;

    /**
     * 代码项名称
     */
    private String codeNames;

    /**
     * 是否有子类
     */
    @Transient
    private boolean hasChildren;

    /**
     * 二维代码项子类
     */
    private List<FrameCodeValuePojo> frameCodeValueList;

    public String getParentGuidVal() {
        return parentGuidVal;
    }

    public void setParentGuidVal(String parentGuidVal) {
        this.parentGuidVal = parentGuidVal;
    }

    public String getCodeNames() {
        return codeNames;
    }

    public void setCodeNames(String codeNames) {
        this.codeNames = codeNames;
    }

    public List<FrameCodeValuePojo> getFrameCodeValueList() {
        return frameCodeValueList;
    }

    public void setFrameCodeValueList(List<FrameCodeValuePojo> frameCodeValueList) {
        this.frameCodeValueList = frameCodeValueList;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }
}
