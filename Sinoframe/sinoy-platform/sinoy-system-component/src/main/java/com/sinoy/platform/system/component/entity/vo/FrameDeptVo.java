package com.sinoy.platform.system.component.entity.vo;

import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.platform.system.component.entity.FrameDept;

/**
 * 部门排序
 */
public class FrameDeptVo {

    private FrameDept frameDept;

    private Sort sort;

    /**
     * 是否需要guids数组
     */
    private boolean needList;

    public FrameDept getFrameDept() {
        return frameDept;
    }

    public void setFrameDept(FrameDept frameDept) {
        this.frameDept = frameDept;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public boolean isNeedList() {
        return needList;
    }

    public void setNeedList(boolean needList) {
        this.needList = needList;
    }

}
