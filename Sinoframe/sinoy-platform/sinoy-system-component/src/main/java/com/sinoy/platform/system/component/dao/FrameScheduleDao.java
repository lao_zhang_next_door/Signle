package com.sinoy.platform.system.component.dao;


import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameSchedule;
import com.sinoy.platform.system.component.entity.dto.FrameScheduleDto;
import com.sinoy.platform.system.component.utils.Query;

import java.util.List;

public interface FrameScheduleDao extends ExtendMapper<FrameSchedule> {

    /**
     * 联查用户表
     * @param query
     * @return
     */
    List<FrameScheduleDto> getList(Query query);
    Integer getListCount(Query query);

    int insertBatchByXml(List<FrameScheduleDto> frameScheduleDtos);
}

