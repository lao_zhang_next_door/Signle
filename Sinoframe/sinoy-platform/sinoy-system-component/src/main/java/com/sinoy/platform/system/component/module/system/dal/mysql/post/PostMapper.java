package com.sinoy.platform.system.component.module.system.dal.mysql.post;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.base.query.QueryWrapperX;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostPageReqVO;
import com.sinoy.platform.system.component.module.system.dal.dataobject.post.PostDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.*;

public interface PostMapper extends ExtendMapper<PostDO> {

    default List<PostDO> selectList(Collection<Long> ids, Collection<Integer> statuses) {
        return selectList(new QueryWrapperX<PostDO>().inIfPresent("row_id", ids)
                .inIfPresent("status", statuses));
    }

    default Page<PostDO> selectPage(PostPageReqVO reqVO) {
        Map<String,Object> params = Optional.ofNullable(reqVO.getParams()).orElse(new HashMap());

        return selectPageFuck(reqVO, new QueryWrapperX<PostDO>()
                .likeIfPresent("code", params.get("code_cache") == null ? null : (String)params.get("code_cache"))
                .likeIfPresent("name", params.get("name_cache") == null ? null : (String)params.get("name_cache"))
                .eqIfPresent("status", "".equals(params.get("status_equal")) ?  null : params.get("status_equal"))
                .orderByDesc("sort_sq"));
    }

    default PostDO selectByName(String name) {
        return selectOne(new QueryWrapper<PostDO>().eq("name", name));
    }

    default PostDO selectByCode(String code) {
        return selectOne(new QueryWrapper<PostDO>().eq("code", code));
    }

}
