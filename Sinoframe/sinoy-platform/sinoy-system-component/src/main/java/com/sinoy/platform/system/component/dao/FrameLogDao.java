package com.sinoy.platform.system.component.dao;


import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameLog;

public interface FrameLogDao extends ExtendMapper<FrameLog> {
	
}

