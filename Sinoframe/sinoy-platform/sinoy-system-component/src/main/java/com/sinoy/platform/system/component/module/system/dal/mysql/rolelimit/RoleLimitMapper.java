package com.sinoy.platform.system.component.module.system.dal.mysql.rolelimit;


import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;

import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import org.apache.ibatis.annotations.Mapper;


/**
 * 角色权限 Mapper
 *
 * @author 管理员
 */
@Mapper
public interface RoleLimitMapper extends ExtendMapper<RoleLimitDO> {


}
