package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.XnwClientDao;
import com.sinoy.platform.system.component.entity.XnwClient;
import com.sinoy.platform.system.component.service.XnwClientService;
import org.springframework.stereotype.Service;

@Service
public class XnwClientServiceImpl extends BaseServiceImpl<XnwClientDao, XnwClient> implements XnwClientService {

}

