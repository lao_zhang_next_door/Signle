package com.sinoy.platform.system.component.entity.dto;


import com.sinoy.platform.system.component.entity.FrameSchedule;

public class FrameScheduleDto extends FrameSchedule {

    private String schReceiverVal;

    private String schOriginatorVal;

    private String tmp_leadDeptName;

    public String getTmp_leadDeptName() {
        return tmp_leadDeptName;
    }

    public void setTmp_leadDeptName(String tmp_leadDeptName) {
        this.tmp_leadDeptName = tmp_leadDeptName;
    }

    public String getSchReceiverVal() {
        return schReceiverVal;
    }

    public void setSchReceiverVal(String schReceiverVal) {
        this.schReceiverVal = schReceiverVal;
    }

    public String getSchOriginatorVal() {
        return schOriginatorVal;
    }

    public void setSchOriginatorVal(String schOriginatorVal) {
        this.schOriginatorVal = schOriginatorVal;
    }
}
