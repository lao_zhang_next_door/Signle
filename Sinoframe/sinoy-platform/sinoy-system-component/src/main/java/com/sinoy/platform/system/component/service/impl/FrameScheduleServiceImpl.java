package com.sinoy.platform.system.component.service.impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.platform.system.component.dao.FrameScheduleDao;
import com.sinoy.platform.system.component.entity.FrameSchedule;
import com.sinoy.platform.system.component.entity.dto.FrameScheduleDto;
import com.sinoy.platform.system.component.service.FrameScheduleService;
import com.sinoy.platform.system.component.utils.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FrameScheduleServiceImpl extends BaseServiceImpl<FrameScheduleDao, FrameSchedule> implements FrameScheduleService {


	@Resource
	private FrameScheduleDao frameScheduleDao;

	@Override
	public List<FrameScheduleDto> getList(Query query) {
		return baseMapper.getList(query);
	}

	@Override
	public Integer getListCount(Query query) {
		return baseMapper.getListCount(query);
	}

	@Override
	public int insertBatchByXml(List<FrameScheduleDto> frameScheduleDtos) {
		return frameScheduleDao.insertBatchByXml(frameScheduleDtos);
	}
}

