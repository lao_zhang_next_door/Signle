//package com.sino.platform.system.component.strategy.enums;
//
//
///**
// * 数据权限枚举类
// *
// */
//public enum DataScopeTypeEnum {
//	/**
//	 * 全部
//	 */
//	ALL(1, "全部"),
//	/**
//	 * 仅查看本人数据
//	 */
//	SELF_LEVEL(2,"仅查看本人数据"),
//	/**
//	 * 本级
//	 */
//	THIS_LEVEL(3, "本级"),
//	/**
//	 * 本级以及子级
//	 */
//	THIS_LEVEL_CHILDREN(4, "本级以及子级"),
//	/**
//	 * 自定义
//	 */
//	CUSTOMIZE(5, "自定义");
//
//
//	private final int type;
//	private final String description;
//
//
//	public static DataScopeTypeEnum valueOf(int type) {
//		for (DataScopeTypeEnum typeVar : DataScopeTypeEnum.values()) {
//			if (typeVar.getType() == type) {
//				return typeVar;
//			}
//		}
//		return ALL;
//	}
//
//	public int getType() {
//		return type;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	DataScopeTypeEnum(int type, String description) {
//		this.type = type;
//		this.description = description;
//	}
//}
