package com.sinoy.platform.system.component.entity.dto;


import com.sinoy.platform.system.component.entity.FrameDept;

public class FrameDeptDto extends FrameDept {
	
	/**
	 * 父级guid
	 */
	private String parentDeptGuid;

	/**
     * 父级名称
	 */
	private String parentDeptName;

	/**
     * 父级编号
	 */
	private String parentDeptCode;

	public String getParentDeptGuid() {
		return parentDeptGuid;
	}

	public FrameDeptDto setParentDeptGuid(String parentDeptGuid) {
		this.parentDeptGuid = parentDeptGuid;
		return this;
	}

	public String getParentDeptName() {
		return parentDeptName;
	}

	public FrameDeptDto setParentDeptName(String parentDeptName) {
		this.parentDeptName = parentDeptName;
		return this;
	}

	public String getParentDeptCode() {
		return parentDeptCode;
	}

	public FrameDeptDto setParentDeptCode(String parentDeptCode) {
		this.parentDeptCode = parentDeptCode;
		return this;
	}
}
