package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.dto.FrameModuleDto;

import java.util.List;

public interface FrameModuleDao extends ExtendMapper<FrameModule> {

    /**
     * 根据rowGuid 联查(left join frameModule)
     * @param rowGuid
     * @return
     */
    List<FrameModuleDto> selectModuleAndParentModule(String rowGuid);


    /**
     * 查询修改前这个模块code下的所有子集模块
     * @return
     */
    List<FrameModule> getChildModuleByPModuleCode(String moduleCode);

    /**
     * 级联删除模块
     */
    void deleteModule(String moduleCode);

}
