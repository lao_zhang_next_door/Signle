package com.sinoy.platform.system.component.entity.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * 附件传输DTO
 *
 * @author T470
 */
public class FrameAttachDto {

    /**
     * 附件关联Guid
     */
    private String formRowGuid;

    /**
     * 传输文件
     */
    private MultipartFile file;

    /**
     * 上传文件路径
     */
    private String path;

    public String getFormRowGuid() {
        return formRowGuid;
    }

    public void setFormRowGuid(String formRowGuid) {
        this.formRowGuid = formRowGuid;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
