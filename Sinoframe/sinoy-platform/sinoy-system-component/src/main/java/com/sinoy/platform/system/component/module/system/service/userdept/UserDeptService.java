package com.sinoy.platform.system.component.module.system.service.userdept;

import java.util.*;
import javax.validation.*;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo.*;
import com.sinoy.platform.system.component.module.system.dal.dataobject.userdept.UserDeptDO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 多部门关联 Service 接口
 *
 * @author 管理员
 */
public interface UserDeptService extends BaseService<UserDeptDO>{

    /**
     * 创建多部门关联
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createUserDept(@Valid UserDeptCreateReqVO createReqVO);

    /**
     * 更新多部门关联
     *
     * @param updateReqVO 更新信息
     */
    void updateUserDept(@Valid UserDeptUpdateReqVO updateReqVO);

    /**
     * 删除多部门关联
     *
     * @param id 编号
     */
    void deleteUserDept(Long id);

    /**
     * 获得多部门关联
     *
     * @param id 编号
     * @return 多部门关联
     */
    UserDeptDO getUserDept(Long id);

    /**
     * 获得多部门关联列表
     *
     * @param ids 编号
     * @return 多部门关联列表
     */
    List<UserDeptDO> getUserDeptList(Collection<Long> ids);

    /**
     * 获得多部门关联分页
     *
     * @param pageReqVO 分页查询
     * @return 多部门关联分页
     */
    Page<UserDeptDO> getUserDeptPage(UserDeptPageReqVO pageReqVO);

    /**
     * 获取用户关联的部门
     * @param userId 用户id
     * @return
     */
    List<FrameDept> getDeptsByUserId(Long userId);

    /**
     * 切换部门 兼职部门更新 mainDept 主职部门则清除所有mainDept
     * @param deptId
     */
    void switchDept(Long deptId);

    /**
     * 获取用户兼职部门
     * @param userId
     * @return
     */
    List<FrameDept> getJZDeptsByUserId(Long userId);
}
