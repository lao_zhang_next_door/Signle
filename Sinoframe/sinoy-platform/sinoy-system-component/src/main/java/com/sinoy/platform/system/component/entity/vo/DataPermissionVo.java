package com.sinoy.platform.system.component.entity.vo;

import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameRoleDept;

import java.util.List;

/**
 * 数据权限vo
 */
public class DataPermissionVo {

    private FrameRole frameRole;

    private List<FrameRoleDept> frameRoleDeptList;

    public FrameRole getFrameRole() {
        return frameRole;
    }

    public DataPermissionVo setFrameRole(FrameRole frameRole) {
        this.frameRole = frameRole;
        return this;
    }

    public List<FrameRoleDept> getFrameRoleDeptList() {
        return frameRoleDeptList;
    }

    public DataPermissionVo setFrameRoleDeptList(List<FrameRoleDept> frameRoleDeptList) {
        this.frameRoleDeptList = frameRoleDeptList;
        return this;
    }
}
