package com.sinoy.platform.system.component.module.system.service.userdept;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import com.sinoy.platform.system.component.dao.FrameDeptDao;
import com.sinoy.platform.system.component.dao.FrameUserDao;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.enumeration.IsOrNotEnum;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.sql.BatisUtil;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.*;
import com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo.*;
import com.sinoy.platform.system.component.module.system.dal.dataobject.userdept.UserDeptDO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.sinoy.platform.system.component.module.system.convert.userdept.UserDeptConvert;
import com.sinoy.platform.system.component.module.system.dal.mysql.userdept.UserDeptMapper;

import com.sinoy.core.common.utils.exception.BaseException;

/**
 * 多部门关联 Service 实现类
 *
 * @author 管理员
 */
@Service
@Validated
public class UserDeptServiceImpl extends BaseServiceImpl<UserDeptMapper,UserDeptDO> implements UserDeptService {

    @Resource
    private UserDeptMapper userDeptMapper;

    @Resource
    private FrameUserDao frameUserDao;
    @Resource
    private FrameDeptDao frameDeptDao;

    @Override
    public Long createUserDept(UserDeptCreateReqVO createReqVO) {
        // 插入
        UserDeptDO userDept = UserDeptConvert.INSTANCE.convert(createReqVO);
        userDept.initNull();
        userDeptMapper.insert(userDept);
        // 返回
        return userDept.getRowId();
    }

    @Override
    public void updateUserDept(UserDeptUpdateReqVO updateReqVO) {
        // 校验存在
        this.validateUserDeptExists(updateReqVO.getRowId());
        // 更新
        UserDeptDO updateObj = UserDeptConvert.INSTANCE.convert(updateReqVO);
        userDeptMapper.updateById(updateObj);
    }

    @Override
    public void deleteUserDept(Long id) {
        // 校验存在
        this.validateUserDeptExists(id);
        // 删除
        userDeptMapper.deleteById(id);
    }

    private void validateUserDeptExists(Long id) {
        if (userDeptMapper.selectById(id) == null) {
                        throw new BaseException("该条记录不存在");
        }
    }

    @Override
    public UserDeptDO getUserDept(Long id) {
        return userDeptMapper.selectById(id);
    }

    @Override
    public List<UserDeptDO> getUserDeptList(Collection<Long> ids) {
        return userDeptMapper.selectBatchIds(ids);
    }

    @Override
    public Page<UserDeptDO> getUserDeptPage(UserDeptPageReqVO pageReqVO) {
        Page<UserDeptDO> page = new Page(pageReqVO.getCurrentPage(),pageReqVO.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        BatisPlusUtil.setOrders(queryWrapper,null,null);
        BatisPlusUtil.checkParamsIncluded(queryWrapper,pageReqVO,pageReqVO.getParams());
        return userDeptMapper.selectPage(page,queryWrapper);
    }

    @Override
    public List<FrameDept> getDeptsByUserId(Long userId) {
        List<FrameDept> deptDTOS = new ArrayList<>();
        deptDTOS= userDeptMapper.getDeptsByUserId(userId);

        FrameUser user=frameUserDao.selectById(userId);
        FrameDept deptDO = frameDeptDao.selectById(user.getDeptId());
        // 添加到deptDTOS的最前面
        deptDTOS.add(0, deptDO);
        return deptDTOS;
    }

    @Override
    public void switchDept(Long deptId) {
        // 判断是否是主职部门
        Long userId = SecurityFrameworkUtils.getLoginUserId();
        FrameUser adminUserDO = frameUserDao.selectById(userId);
        // 清除所有兼职部门的 mainDept
        userDeptMapper.clearMainDept(userId);
        if(adminUserDO != null && adminUserDO.getDeptId().equals(deptId)){
            return;
        }

        // 设置mainDept
        UserDeptDO userDeptDO=new UserDeptDO();
        userDeptDO.setMainDept(IsOrNotEnum.IS.getCode());
        userDeptMapper.update(userDeptDO,
                new UpdateWrapper<UserDeptDO>().eq("user_id", userId).eq("dept_id", deptId));
    }

    @Override
    public List<FrameDept> getJZDeptsByUserId(Long userId) {
        List<FrameDept>  deptDTOS= userDeptMapper.getDeptsByUserId(userId);
        return deptDTOS;
    }
}
