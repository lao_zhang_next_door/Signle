package com.sinoy.platform.system.component.module.system.convert.post;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostRespVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostSimpleRespVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostUpdateReqVO;
import com.sinoy.platform.system.component.module.system.dal.dataobject.post.PostDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PostConvert {

    PostConvert INSTANCE = Mappers.getMapper(PostConvert.class);

    List<PostSimpleRespVO> convertList02(List<PostDO> list);

    Page<PostRespVO> convertPage(Page<PostDO> page);

    PostRespVO convert(PostDO id);

    PostDO convert(PostCreateReqVO bean);

    PostDO convert(PostUpdateReqVO reqVO);

}
