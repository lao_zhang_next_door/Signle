package com.sinoy.platform.system.component.service;


import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.core.datascope.anno.DataColumn;
import com.sinoy.core.datascope.anno.DataPermission;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.entity.vo.FrameUserPasswordVo;
import com.sinoy.platform.system.component.utils.Query;

import java.io.InputStream;
import java.util.*;

public interface FrameUserService extends BaseService<FrameUser> {

    /**
     * 根据登录id查询用户信息
     * @param loginId
     * @return
     */
    UserInfo loadUserByLoginId(String loginId);

    /**
     * 根据rowId 获取用户信息
     * @param userId
     * @return
     */
    UserInfo loadUserByUserId(Long userId);

    /**
     * 查询用户信息列表带部门信息 (联查部门表)
     * @param query
     * @return
     */
    @DataPermission({
            @DataColumn(key = "column", value = "t.dept_id"),
            @DataColumn(key = "userColumn", value = "t.row_guid")
    })
    List<UserInfo> getList(Query query);

    /**
     * 查询用户信息列表带部门数量 (联查部门表)
     * @param query
     * @return
     */
    Integer getListCount(Query query);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息 (联查部门表 附件表)
     * @param userGuid
     * @return
     */
    UserInfo selectDetailByUserGuid(String userGuid);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息 (联查部门表 附件表) 注意会返回加密过后的密码
     * @param loginId
     * @return
     */
    UserInfo selectDetailByLoginId(String loginId);

    /**
     * 查询guid用户 关联角色信息
     * @param userGuid
     * @return
     */
    List<FrameRole> selectRoleListByUserGuid(String userGuid);

    /**
     * 新增框架用户(设置密码 设置权限)
     * @param frameUserPojo
     * @return str
     */
    UserInfo addFrameUser(UserInfo frameUserPojo);

    /**
     * 更新框架用户
     * @param frameUserPojo
     */
    void updateFrameUser(UserInfo frameUserPojo);

    /**
     * 获取配置项设置的初始密码(md5 加密过后 再aes 加密)
     */
    String getInitPassword();

    /**
     * 修改密码
     * @param
     */
    void changePassword(FrameUserPasswordVo frameUserPasswordVo);

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     * @return
     */
    List<FrameUser> getParentDeptUser();

    /**
     * 给用户分配角色
     * @param userGuid
     */
    void setRole(String userGuid, List<String> roleGuids);

    /**
     * 判断密码是否匹配
     * @param rawPassword 未加密的密码
     * @param encodedPassword 加密后的密码
     * @return
     */
    boolean isPasswordMatch(String rawPassword, String encodedPassword);

    /**
     * 上传用户头像
     * @param loginId
     * @param inputStream
     * @return
     */
    String updateUserAvatar(String loginId, InputStream inputStream);

    /**
     * 校验用户们是否有效。如下情况，视为无效：
     * 1. 用户编号不存在
     * 2. 用户被禁用
     *
     * @param ids 用户编号数组
     */
    void validUsers(Collection<Long> ids);

    /**
     * 获得指定部门的用户数组
     * @param options
     * @return
     */
    List<FrameUser> getUsersByDeptIds(Set<Long> options);

    /**
     * 获得用户 Map
     *
     * @param ids 用户编号数组
     * @return 用户 Map
     */
    Map<Long, FrameUser> getUserMap(Collection<Long> ids);

    /**
     * 获得指定状态的用户们
     *
     * @param status 状态
     * @return 用户们
     */
    List<FrameUser> getUsersByStatus(Integer status);

    /**
     * 获得指定岗位的用户数组
     *
     * @param postIds 岗位数组
     * @return 用户数组
     */
    List<FrameUser> getUsersByPostIds(Collection<Long> postIds);

    /**
     * 获取指定单位下 （包含子单位） 的拥有指定角色的人员
     * @param deptCode 指定单位code
     * @param roleFlag 指定角色
     * @return
     */
    List<FrameUser> getUnityLeaderRole(String deptCode, String roleFlag);

    /**
     * 获取指定roleFlag 的所有用户
     * @param roleFlag
     * @param deptId 指定部门 不包含子部门
     * @param excludeUserGuids 排除的用户id集合
     * @return
     */
    List<FrameUser> getUserByRoleFlag(String roleFlag,Long deptId,List<String> excludeUserGuids);

    /**
     * 获取当前部门信息
     * 可能存在多个兼职部门以及所在主部门，需要根据情况选择
     * @param userId
     * @return
     */
    FrameDept getCurrentDept(Long userId, Long deptId);

    /**
     * 查询用户信息
     * @param frameUser
     * @return
     */
    UserInfo getUserInfo(FrameUser frameUser);
}
