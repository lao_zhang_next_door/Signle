package com.sinoy.platform.system.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.platform.system.component.entity.FrameConfig;


public interface FrameConfigService extends BaseService<FrameConfig> {

    /**
     * redis缓存系统参数配置
     */
    void cacheFrameConfig();

    /**
     * 获取系统配置项缓存
     *
     * @param configName
     * @return
     */
    String getCacheByConfigName(String configName);

    /**
     * 根据configName更新缓存内容
     *
     * @param configName
     */
    void updateConfigCacheByName(String configName);

    /**
     * 根据configName删除缓存内容
     *
     * @param configName
     */
    void deleteConfigCacheByName(String configName);
}
