package com.sinoy.platform.system.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.entity.pojo.FrameCodeValuePojo;

import java.util.List;

public interface FrameCodeValueDao extends ExtendMapper<FrameCodeValue> {

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    List<FrameCodeValuePojo> selectParentCode(String rowGuid);

    /**
     * 获取所有代码项
     *
     * @return list
     */
    List<FrameCodeValuePojo> getCodeValueToCache();

    /**
     * 根据代码项名称查询
     *
     * @param codeName
     * @return
     */
    List<FrameCodeValuePojo> getValueByCodeName(String codeName);

    /**
     * 按guid查询代码项名称
     *
     * @param rowGuid
     * @return
     */
    String getCodeNameByGuid(String rowGuid);
}
