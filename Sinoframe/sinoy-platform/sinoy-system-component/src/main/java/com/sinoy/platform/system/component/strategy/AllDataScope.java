//package com.sino.platform.system.component.strategy;
//
//import com.sino.platform.system.component.service.FrameDeptService;
//import org.springframework.stereotype.Component;
//
///**
// * 所有数据范围
// *
// */
//@Component("1")
//public class AllDataScope implements AbstractDataScopeHandler {
//
//	private final FrameDeptService deptService;
//
//	public AllDataScope(FrameDeptService deptService) {
//		this.deptService = deptService;
//	}
//
////	@Override
////	public List<Long> getDeptIds(DataScopeTypeEnum dataScopeTypeEnum) {
////		List<FrameDept> sysDeparts = deptService.list();
////		return sysDeparts.stream().map(FrameDept::getRowId).collect(Collectors.toList());
////	}
//}
