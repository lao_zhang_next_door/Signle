package com.sinoy.platform.system.component.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.sinoy.platform.system.component.base.BaseEntity;


/**
 * @author jtr
 * @creatTime 2021-04-20-10:49
 **/
public class FrameConfig extends BaseEntity {

	/**
	 * @param init true则有默认值
	 */
    public FrameConfig(boolean init) {
		super(init);
	}

    public FrameConfig() {}
	
	private Character categoryType;

    private String configName;

    private String configValue;

    private String description;

    @TableField(exist = false)
    private String categoryTypeVal;

    public String getCategoryTypeVal() {
        if(getCategoryType() != null){
            return  codeValueService.getItemTextByValue("系统参数分类", String.valueOf(getCategoryType()));
        }else{
            return "";
        }
    }

    public void setCategoryTypeVal(String categoryTypeVal) {
        this.categoryTypeVal = categoryTypeVal;
    }

    public Character getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Character categoryType) {
		this.categoryType = categoryType;
	}

	public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
