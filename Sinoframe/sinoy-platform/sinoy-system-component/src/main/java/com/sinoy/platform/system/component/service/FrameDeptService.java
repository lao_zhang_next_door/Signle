package com.sinoy.platform.system.component.service;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;
import com.sinoy.platform.system.component.entity.vo.FrameDeptVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface FrameDeptService extends BaseService<FrameDept> {

    /**
     * 根据rowGuid 联查(left join frameDept) 查询上级deptName
     * @param deptGuid
     * @return FrameDeptDto
     */
    FrameDeptDto selectDeptAndParentDept(String deptGuid);


    /**
     * 根据区guid获取街道以及社区的关联数据
     * @param qGuid
     * @return
     */
    JSONArray getStreetAndCommunity(String qGuid);

    /**
     * 新增部门
     * @param frameDept
     * @return FrameDept
     */
    FrameDept saveDept(FrameDept frameDept);

    /**
     * 修改部门
     * @param frameDept
     * @return FrameDept
     */
    FrameDept update(FrameDept frameDept);

    /**
     * 级联删除部门与用户
     * @param deptGuid
     */
    void deleteDeptAndUser(String deptGuid);

    /**
     * 级联删除部门子部门与相关用户 以及业务相关数据
     * @param rowGuids
     */
    void deleteDeptAndUserAndMore(String[] rowGuids);

    /**
     * 一次性获取全部树数据
     * @return
     */
    JSONObject allTreeData(FrameDeptVo frameDeptVo);

    /**
     * 部门树列表 全部加载
     * @param sort
     * @return
     */
    List<FrameDept> getAllTreeData(Sort sort);

    /**
     * 查询指定部门下的所有部门树数据
     *
     * self 包含自己
     * @param deptId 部门id
     * @return
     */
    List<FrameDept> getTreeDataByDeptId(String deptId,Boolean self,String[] excludeDeptIds);

    /**
     * 部门树数据 懒加载 根据父parentGuid查询
     * @return
     */
    JSONArray getTreeDataLazy(FrameDeptVo frameDeptVo);

    /**
     * 补充父级部门信息
     * @param deptDtoList
     */
    void addParentDeptInfo(List<FrameDeptDto> deptDtoList);

    /**
     * 校验部门们是否有效。如下情况，视为无效：
     * 1. 部门编号不存在
     * 2. 部门被禁用
     *
     * @param ids 部门编号数组
     */
    void validDepts(Collection<Long> ids);

    /**
     * 获得指定编号的部门 Map
     *
     * @param ids 部门编号数组
     * @return 部门 Map
     */
    default Map<Long, FrameDept> getDeptMap(Set<Long> ids){
        List<FrameDept> list = getBaseMapper().selectBatchIds(ids);
        return CollectionUtils.convertMap(list, FrameDept::getRowId);
    };

    /**
     * 获得指定状态的部门列表  只包含被开启的部门，主要用于前端的下拉选项
     * @param status
     * @return
     */
    List<FrameDept> getSimpleDepts(Character status);

    /**
     * 根据人员编号获取所有部门--手机号码+验证码方式登录专用
     * @param userId
     * @return
     */
    List<FrameDept> getDeptsByUserIdForTel(Long userId);
}
