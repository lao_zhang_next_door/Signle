package com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit;

import com.sinoy.core.database.mybatisplus.handler.JsonLongSetTypeHandler;
import lombok.*;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

/**
 * 角色权限 DO
 *
 * @author 管理员
 */
@TableName("frame_role_limit")
@KeySequence("frame_role_limit_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleLimitDO extends BaseEntity {


    /**
    * 角色id
    */
    private Long roleId;



    /**
    * 权限范围内的角色id
    */
    private String roleChildId;




    /**
    * 角色名称
    */
    private String roleName;



    /**
    * 可配置角色名称
    */
    private String roleChildName;



}
