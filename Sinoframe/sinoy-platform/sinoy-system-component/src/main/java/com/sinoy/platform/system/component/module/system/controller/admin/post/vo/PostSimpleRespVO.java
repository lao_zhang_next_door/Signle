package com.sinoy.platform.system.component.module.system.controller.admin.post.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostSimpleRespVO {

    //岗位编号
    private Long rowId;

    //岗位名称
    private String name;

}
