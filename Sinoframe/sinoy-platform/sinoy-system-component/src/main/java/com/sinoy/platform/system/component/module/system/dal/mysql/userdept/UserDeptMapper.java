package com.sinoy.platform.system.component.module.system.dal.mysql.userdept;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.enumeration.IsOrNotEnum;
import com.sinoy.platform.system.component.module.system.dal.dataobject.userdept.UserDeptDO;
import org.apache.ibatis.annotations.Mapper;
import com.sinoy.platform.system.component.module.system.controller.admin.userdept.vo.*;
import org.apache.ibatis.annotations.Param;

/**
 * 多部门关联 Mapper
 *
 * @author 管理员
 */
@Mapper
public interface UserDeptMapper extends ExtendMapper<UserDeptDO> {

    default  UserDeptDO selectMainDept(Long userId){
        return selectOne(new LambdaQueryWrapper<UserDeptDO>()
                .eq(UserDeptDO::getUserId, userId)
                .eq(UserDeptDO::getMainDept, true));
    };

    default void clearMainDept(Long userId){
        UserDeptDO userDeptDO = new UserDeptDO();
        userDeptDO.setMainDept(IsOrNotEnum.NOT.getCode());
        update(userDeptDO, new LambdaQueryWrapper<UserDeptDO>()
                .eq(UserDeptDO::getUserId, userId));
    };

    List<FrameDept> getDeptsByUserId(@Param("userId") Long userId) ;
}
