package com.sinoy.platform.system.component.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * OAuth2.0 访问令牌的信息 Response DTO
 *
 */
public class OAuth2AccessTokenRespDTO implements Serializable {

    /**
     * 访问令牌
     */
    private String accessToken;
    /**
     * 刷新令牌
     */
    private String refreshToken;
    /**
     * 用户编号
     */
    private Long userId;
    /**
     * 用户类型
     */
    private Integer userType;
    /**
     * 过期时间
     */
    private Date expiresTime;

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2AccessTokenRespDTO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2AccessTokenRespDTO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public OAuth2AccessTokenRespDTO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2AccessTokenRespDTO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2AccessTokenRespDTO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }
}
