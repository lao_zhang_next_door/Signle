package com.sinoy.platform.system.startmission;

import com.sinoy.platform.system.component.service.FrameAttachConfigService;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import com.sinoy.platform.system.component.service.FrameConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wzl
 */
@Component
@Order(2)
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @Autowired
    private FrameConfigService frameConfigService;

    @Override
    public void run(ApplicationArguments args) {
        //代码项缓存
        frameCodeValueService.cacheAllCodeValue();
        //缓存系统配置项
        frameConfigService.cacheFrameConfig();
        //初始化文件配置
//        frameAttachConfigService.initFileClients();
    }
}
