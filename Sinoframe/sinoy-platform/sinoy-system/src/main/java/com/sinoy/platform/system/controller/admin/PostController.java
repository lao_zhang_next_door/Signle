package com.sinoy.platform.system.controller.admin;


import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostCreateReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostPageReqVO;
import com.sinoy.platform.system.component.module.system.controller.admin.post.vo.PostUpdateReqVO;
import com.sinoy.platform.system.component.module.system.convert.post.PostConvert;
import com.sinoy.platform.system.component.module.system.dal.dataobject.post.PostDO;
import com.sinoy.platform.system.component.module.system.service.post.PostService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * 岗位
 */
@RestController
@RequestMapping("/frame/post")
@Validated
public class PostController {

    @Resource
    private PostService postService;

    /**
     * 创建岗位
     * @param reqVO
     * @return
     */
    @PostMapping("/create")
    public R createPost(@Valid @RequestBody PostCreateReqVO reqVO) {
        Long postId = postService.createPost(reqVO);
        return R.ok();
    }

    /**
     * 修改岗位
     * @param reqVO
     * @return
     */
    @PutMapping("/update")
    public R updatePost(@Valid @RequestBody PostUpdateReqVO reqVO) {
        postService.updatePost(reqVO);
        return R.ok();
    }

    /**
     * 删除岗位
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public R deletePost(@RequestParam("id") Long id) {
        postService.deletePost(id);
        return R.ok();
    }

    /**
     * 获得岗位信息
     * @param id
     * @return
     */
    @GetMapping(value = "/get")
    public R getPost(@RequestParam("id") Long id) {
        return R.ok().put("data",PostConvert.INSTANCE.convert(postService.getPost(id)));
    }

    /**
     * 获取岗位精简信息列表 只包含被开启的岗位 主要用于前端的下拉选项
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimplePosts() {
        // 获得岗位列表，只要开启状态的
        List<PostDO> list = postService.getPosts(null, Collections.singleton(CommonStatusEnum.ENABLE.getStatus()));
        // 排序后，返回给前端
        list.sort(Comparator.comparing(PostDO::getSortSq));
        return R.ok().put("data",PostConvert.INSTANCE.convertList02(list));
    }

    /**
     * 获得岗位分页列表
     * @param reqVO
     * @return
     */
    @GetMapping("/page")
    public R getPostPage(@Validated PostPageReqVO reqVO) {
        return R.ok().put("data",PostConvert.INSTANCE.convertPage(postService.getPostPage(reqVO)));
    }


}
