package com.sinoy.platform.system.controller.app;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameCode;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.service.FrameCodeService;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 代码项控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameCode")
public class AppCodeController extends BaseController {

    @Autowired
    private FrameCodeService frameCodeService;

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @GetMapping("/selectCacheByCodeName")
    private R selectCacheByCodeName(@RequestParam String codeName) {
        return R.ok().put("data", frameCodeValueService.getCacheByCodeName(codeName));
    }

    /**
     * 查询所有字典赋予前端缓存
     * 所有字典值 [{text:'男',val:'0'},{text:'女',val:'1'},...]
     * @return
     */
    @GetMapping("/listSimpleDictDatas")
    private R listSimpleDictDatas(){
        QueryWrapper queryWrapper = new QueryWrapper();
        BatisPlusUtil.setOrders(queryWrapper,null,null);
        return R.ok().put("data", frameCodeValueService.list(queryWrapper));
    }

    /**
     * 查询所有字典项  所有项 没有值  [{性别},{状态}...]
     * @return
     */
    @GetMapping("/listAllSimple")
    private R listAllSimple(){
        return R.ok().put("data", frameCodeService.listAll());
    }
}
 