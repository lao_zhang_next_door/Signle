package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.platform.system.component.entity.FrameLog;
import com.sinoy.platform.system.component.service.FrameLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/frame/FrameLog")
public class FrameLogController {

	@Autowired
	private FrameLogService frameLogService;

	@PostMapping("/listData")
    private R listData(@RequestBody Search search){
		QueryWrapper queryWrapper = new QueryWrapper();
		BatisPlusUtil.setParams(queryWrapper,search.getParams());
		BatisPlusUtil.setOrders(queryWrapper,search.getProp(),search.getOrder());
		Page<FrameLog> frameLogPage = new Page(search.getCurrentPage(), search.getPageSize());
		Page iPage = frameLogService.page(frameLogPage,queryWrapper);
		return R.list((int) iPage.getTotal(), iPage.getRecords());
    }
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		return R.ok().put("data", frameLogService.selectByGuid(rowGuid));
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameLog frameLog){
		frameLog.initNull();
		frameLogService.save(frameLog);
		return R.ok();
	}
	
	@PutMapping("/update")
	private R update(@RequestBody FrameLog frameLog){
	    frameLog.setUpdateTime(LocalDateTime.now());
		frameLogService.updateByRowGuid(frameLog);
		return R.ok();
	}
	
	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		frameLogService.deleteBatchByGuids(rowGuids);
		return R.ok();
	}
}