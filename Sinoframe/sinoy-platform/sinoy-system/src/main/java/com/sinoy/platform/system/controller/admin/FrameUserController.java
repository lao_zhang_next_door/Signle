package com.sinoy.platform.system.controller.admin;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.LoginLogTypeEnum;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.datascope.anno.DataColumn;
import com.sinoy.core.datascope.anno.DataPermission;
import com.sinoy.core.oauth2.maps.AuthConvert;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.login.AuthLoginRespVO;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameRole;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.FrameUserRole;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.entity.vo.FrameSetRole;
import com.sinoy.platform.system.component.entity.vo.FrameUserPasswordVo;
import com.sinoy.platform.system.component.enumeration.IsOrNotEnum;
import com.sinoy.platform.system.component.enumeration.UserStatus;
import com.sinoy.platform.system.component.module.system.service.userdept.UserDeptService;
import com.sinoy.platform.system.component.service.FrameDeptService;
import com.sinoy.platform.system.component.service.FrameRoleService;
import com.sinoy.platform.system.component.service.FrameUserRoleService;
import com.sinoy.platform.system.component.service.FrameUserService;
import com.sinoy.platform.system.component.utils.PageUtils;
import com.sinoy.platform.system.component.utils.Query;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 用户控制器
 *
 * @author hero
 */
@RestController
@RequestMapping("/frame/FrameUser")
public class FrameUserController extends BaseController {

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

    @Autowired
    public FrameUserService frameUserService;

    @Autowired
    public FrameUserRoleService frameUserRoleService;

    @Autowired
    public OAuth2TokenService oAuth2TokenService;

    @Resource
    public FrameDeptService frameDeptService;

    @Resource
    public UserDeptService userDeptService;

    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameUser> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameUser> iPage = frameUserService.page(frameDeptPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    public R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("row_guid", rowGuid);
        FrameUser user = frameUserService.getOne(queryWrapper);
        return R.ok().put("data", user);
    }

    /**
     * 逻辑删除
     *
     * @param rowGuids
     * @return
     */
    @DeleteMapping("/deleteLogic")
    public R deleteLogic(@RequestBody String[] rowGuids) {
        for (String userGuid : rowGuids) {
            UpdateWrapper<FrameUser> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("row_guid", userGuid);
            updateWrapper.set("del_flag", DelFlag.Del.getCode());
            frameUserService.update(updateWrapper);
        }
        return R.ok();
    }

    /**
     * 物理删除
     *
     * @param rowGuids
     * @return
     */
    @PassToken
    @DeleteMapping("/deletePhysics")
    public R deletePhysics(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            frameUserService.remove(wrapper);
        }
        return R.ok();
    }

    /**
     * 新增框架用户
     *
     * @param frameUserPojo
     * @return
     */
    @PostMapping("/addFrameUser")
    @Transactional
    public R add(@RequestBody UserInfo frameUserPojo) {
        UserInfo userInfo = frameUserService.addFrameUser(frameUserPojo);
        return R.ok().put("userGuid", userInfo.getRowGuid());
    }

    /**
     * 更新框架用户
     *
     * @param frameUserPojo
     * @return
     */
    @PutMapping("/updateFrameUser")
    @Transactional
    public R updateFrameUser(@RequestBody UserInfo frameUserPojo) {
        frameUserService.updateFrameUser(frameUserPojo);
        //更新缓存用户信息
        FrameUser user = frameUserPojo.getFrameUser();
        if (user != null) {
            oAuth2TokenService.updateChcheUser(user.getRowId());
        }

        return R.ok();
    }

    /**
     * 查询用户信息列表带部门信息 (联查部门表)
     *
     * @param params
     * @return
     */
   /* @DataPermission({
            @DataColumn(key = "column", value = "t.dept_id"),
            @DataColumn(key = "userColumn", value = "t.row_guid")
    })*/
    @GetMapping("/getList")
    public R getList(@RequestParam Map<String, Object> params) {
        params.put("delFlag", DelFlag.Normal.getCode());
        Query query = new Query(params);
        List<UserInfo> userList = frameUserService.getList(query);
       /* for (UserInfo model : userList) {
            if (model.getDeptIds() != null) {
                String jzDeptName = "";
                List<FrameDept> depts = userDeptService.getJZDeptsByUserId(model.getRowId());
                if (depts.size() > 0) {
                    for (FrameDept dept : depts) {
                        jzDeptName += dept.getDeptName() + ";";
                    }
                    model.setJzDeptName(jzDeptName.substring(0, jzDeptName.length() - 1));
                }
            }

        }*/
        Integer count = frameUserService.getListCount(query);
        PageUtils pageUtil = new PageUtils(userList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 验证登录名是否唯一
     *
     * @param loginId
     * @return
     */
    @GetMapping("/verifyUnique")
    public R verifyUnique(@RequestParam String loginId) {

        QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
        wrapper.eq("login_id", loginId);
        wrapper.eq("del_flag", DelFlag.Normal.getCode());
        Long count = frameUserService.count(wrapper);

        if (count == 0) {
            return R.ok().put("data", true);
        } else {
            return R.ok().put("data", false);
        }
    }

    /**
     * 查询guid用户详情信息 包括角色,头像,部门等信息
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/getUserDataByGuid")
    public R getUserDataByGuid(@RequestParam String rowGuid) {
        UserInfo user = frameUserService.selectDetailByUserGuid(rowGuid);
        List<FrameRole> roleList = frameUserService.selectRoleListByUserGuid(rowGuid);
        if (roleList.size() > 0) {
            user.setRoleList(roleList);
            user.setRoleGuids(roleList.stream().map(FrameRole::getRowGuid).collect(Collectors.toList()));
        }
        return R.ok().put("data", user);
    }

    /**
     * 启用或者禁用用户
     *
     * @param userGuid
     * @param type
     * @return
     */
    @PutMapping("/enableOrDisable")
    public R enableOrDisable(@RequestParam String userGuid, String type) {
        FrameUser user = new FrameUser();
        UpdateWrapper<FrameUser> wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", userGuid);
        //启用
        if ("enable".equals(type)) {
            user.setStatus(UserStatus.NORMAL.getCode());
        }

        //禁用
        if ("disable".equals(type)) {
            user.setStatus(UserStatus.DISABLE.getCode());
        }

        frameUserService.update(user, wrapper);
        return R.ok();
    }

    /**
     * 重置用户密码
     *
     * @return
     */
    @PutMapping("/resetPassword")
    public R resetPassword(@RequestParam String userGuid) {
        String initPass = frameUserService.getInitPassword();
        if (!StringUtils.isBlank(initPass)) {
            QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", userGuid);
            FrameUser user = new FrameUser();
            user.setPassword(initPass);
            user.setIsUpdateInitialPassword(0);//修改是否修改初始密码标志
            frameUserService.update(user, wrapper);
        }
        return R.ok();
    }

    /**
     * 修改用户密码
     *
     * @return
     */
    @Transactional
    @PutMapping("/changePassword")
    public R changePassword(@RequestBody FrameUserPasswordVo frameUserPasswordVo) {
        frameUserService.changePassword(frameUserPasswordVo);
        return R.ok("修改密码成功");
    }

    /**
     * 查询某个部门下的所有人员
     *
     * @param deptGuid
     * @return
     */
    @GetMapping("/getUserListByDeptGuid")
    public R getUserListByDeptGuid(@RequestParam String deptGuid) {
        Map params = new HashMap();
        params.put("dept_guid", deptGuid);
        params.put("del_flag", DelFlag.Normal.getCode());
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.allEq(params);
        wrapper.select("row_id", "user_name", "login_id", "row_guid", "create_time", "dept_guid", "mobile");
        return R.ok().put("data", frameUserService.list(wrapper));
    }

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     *
     * @return
     */
    @GetMapping("/getParentDeptUser")
    public R getParentDeptUser() {
        return R.ok().put("data", frameUserService.getParentDeptUser());
    }

    /**
     * 根据rowGuid 获取用户部分信息
     *
     * @param userGuid
     * @return
     */
    @GetMapping("/getUserInfoByGuid")
    public R getUserInfoByGuid(@RequestParam String userGuid) {
        Map params = new HashMap();
        params.put("row_guid", userGuid);
        params.put("del_flag", DelFlag.Normal.getCode());
        params.put("status", UserStatus.NORMAL.getCode());
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.allEq(params);
        wrapper.select("user_name", "login_id", "dept_guid");

        return R.ok().put("data", frameUserService.list(wrapper));
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    @GetMapping("/getUserInfo")
    public R getUserInfo() {
        return R.ok().put("data", commonPropAndMethods.getCurrentUser());
    }

    /**
     * 给用户分配角色
     *
     * @return
     */
    @Transactional
    @PostMapping("/setRole")
    public R setRole(@RequestBody FrameSetRole frameSetRole) {
        frameUserService.setRole(frameSetRole.getUserGuid(), frameSetRole.getRoleGuids());
        return R.ok("设置成功");
    }

    /**
     * 用户自己上传头像
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/update-avatar", method = {RequestMethod.POST, RequestMethod.PUT})
    public R updateUserAvatarProfile(@RequestParam("avatarFile") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return R.error("上传文件不能为空");
        }
        String avatar = frameUserService.updateUserAvatar(commonPropAndMethods.getLoginId(), file.getInputStream());
        return R.ok().put("data", avatar);
    }

    /**
     * 获取用户精简信息列表  只包含被开启的用户，主要用于前端的下拉选项
     *
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimpleUsers() {
        // 获用户门列表，只要开启状态的
        List<FrameUser> list = frameUserService.getUsersByStatus(CommonStatusEnum.ENABLE.getStatus());
        // 排序后，返回给前端
        return R.ok().put("data", list);
    }

    /**
     * 根据roleFlag 查询正常的用户列表
     *
     * @param roleFlag
     * @param deptId           指定部门下 不包含子部门
     * @param excludeUserGuids 排除的用户id
     * @return
     */
    @GetMapping("/getUserByRoleFlag")
    public R getUserByRoleFlag(@RequestParam String roleFlag, @RequestParam(required = false) Long deptId, @RequestParam(required = false) List<String> excludeUserGuids) {
        return R.ok().put("data", frameUserService.getUserByRoleFlag(roleFlag, deptId, excludeUserGuids));
    }

    /**
     * 多部门切换
     *
     * @param deptId
     */
    @PutMapping("/switch-dept")
    public R switchDept(@RequestParam("deptId") Long deptId) {
        //frameUserService.switchDept(deptId);
        Long userId = SecurityFrameworkUtils.getLoginUserId();
        FrameUser adminUserDO = frameUserService.getById(userId);

        //重新登录
        QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
        wrapper.eq("del_flag", "0");
        wrapper.eq("mobile", adminUserDO.getMobile());
        wrapper.eq("dept_id", deptId);
        List<FrameUser> user = frameUserService.list(wrapper);

        if (user.size() > 0) {
            UserInfo userInfo = frameUserService.getUserInfo(user.get(0));
            AuthLoginRespVO reqVO = createTokenAfterLoginSuccess(userInfo, LoginLogTypeEnum.LOGIN_USERNAME);

            //清空user表中的maidept
            QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("mobile", adminUserDO.getMobile());
            FrameUser updateUser = new FrameUser();
            updateUser.setMainDept(String.valueOf(IsOrNotEnum.NOT.getCode()));
            frameUserService.update(updateUser, queryWrapper);

            // 设置mainDept
            FrameUser userDO = new FrameUser();
            userDO.setMainDept(String.valueOf(IsOrNotEnum.IS.getCode()));
            frameUserService.update(userDO,
                    new UpdateWrapper<FrameUser>()
                            .eq("mobile", adminUserDO.getMobile())
                            .eq("dept_id", deptId)
                            .eq("del_flag", 0));

            return R.ok().put("data", reqVO);
        } else {
            return R.error("该部门下没有用户");
        }
    }

    private AuthLoginRespVO createTokenAfterLoginSuccess(UserInfo user, LoginLogTypeEnum logType) {
        // 插入登陆日志
//        createLoginLog(user.getRowId(), user.getUserName(), logType, LoginResultEnum.SUCCESS);
        // 创建访问令牌
        OAuth2AccessTokenDO accessTokenDO = oAuth2TokenService.createAccessToken(user, getUserType().getValue(),
                "default", null);
        // 构建返回结果
        return AuthConvert.INSTANCE.convert(accessTokenDO);
    }

    private UserTypeEnum getUserType() {
        return UserTypeEnum.ADMIN;
    }

    /**
     * 查询出所有正常的用户--用于部门里面选择负责人
     * @return
     */
    @GetMapping("/getAllUserList")
    public R getAllUserList(){
        List<FrameUser> list=frameUserService.getUsersByStatus(UserStatus.NORMAL.getCode());
        return R.ok().put("data",list);
    }

    /**
     * 检查该用户是否修改初始密码
     * @param userGuid
     * @return
     */
    @PostMapping("/checkInitialPassword")
    public R checkInitialPassword(@RequestParam String userGuid){
       UserInfo userInfo= frameUserService.selectDetailByUserGuid(userGuid);
       return R.ok().put("data",userInfo);
    }



}
