package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.base.BaseController;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.platform.system.component.entity.FrameModulePermission;
import com.sinoy.platform.system.component.service.FrameModulePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameRoleModule")
public class FrameModulePermissionController extends BaseController {

    @Autowired
    private FrameModulePermissionService frameModulePermissionService;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Page<FrameModulePermission> frameConfigPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        QueryWrapper<FrameModulePermission> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        IPage<FrameModulePermission> iPage = frameModulePermissionService.page(frameConfigPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<FrameModulePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        FrameModulePermission frameModulePermission = frameModulePermissionService.getOne(wrapper);
        return R.ok().put("data", frameModulePermission);
    }

    @GetMapping("/getListByAllowTo")
    private R getListByAllowTo(@RequestParam String allowTo) {
        QueryWrapper<FrameModulePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("allow_to", allowTo);
        List<FrameModulePermission> frameModulePermissionList = frameModulePermissionService.list(wrapper);
        return R.ok().put("data", frameModulePermissionList);
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameModulePermission frameModulePermission) {
        frameModulePermission.initNull();
        frameModulePermissionService.save(frameModulePermission);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameModulePermission frameModulePermission) {
        UpdateWrapper<FrameModulePermission> wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", frameModulePermission.getRowGuid());
        frameModulePermissionService.update(frameModulePermission, wrapper);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameModulePermission> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            frameModulePermissionService.remove(wrapper);
        }
        return R.ok();
    }

    /**
     * 设置权限树（批量新增）
     *
     * @param frameRoleModuleList
     * @return
     */
    @PostMapping("/addBatch")
    private R addBatch(@RequestBody List<FrameModulePermission> frameRoleModuleList, @RequestParam String roleGuid) {
        if(StringUtil.isBlank(roleGuid) || roleGuid.equals("undefined")){
            return R.error("操作失败,请先选择一个角色");
        }

        QueryWrapper<FrameModulePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("allow_to", roleGuid);
        List<FrameModulePermission> rmList = frameModulePermissionService.list(wrapper);

        /**
         * 先删除该角色之前设置的权限
         * 若长度为0 则只删除之前设置的权限
         */
        List<String> guidArr = rmList.stream()
                .map(FrameModulePermission::getRowGuid).collect(Collectors.toList());
        if (rmList.size() != 0) {
            for (String rowGuid : guidArr) {
                QueryWrapper<FrameModulePermission> wrap = new QueryWrapper<>();
                wrap.eq("row_guid", rowGuid);
                frameModulePermissionService.remove(wrap);
            }
        }

        if (frameRoleModuleList.size() == 0) {
            return R.ok("删除成功");
        }

        for (FrameModulePermission roleModule : frameRoleModuleList) {
            roleModule.initNull();
        }
        frameModulePermissionService.saveBatch(frameRoleModuleList);
        return R.ok("设置成功");
    }

}
 