package com.sinoy.platform.system.controller.admin;

import com.sinoy.core.database.mybatisplus.base.BaseController;
import com.sinoy.platform.system.component.service.FrameUserRoleService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录控制器
 * @author hero
 *
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameLogin")
public class FrameLoginController extends BaseController {
	
	@Autowired
	private FrameUserService userService;

	@Autowired
	private FrameUserRoleService frameUserRoleService;
	
//	/**
//	 * 登录接口 返回用户信息
//	 * @return
//	 */
//	@Log(value = "用户登陆", exception = "用户登陆请求异常")
//	@PostMapping("/login")
//	public R login(@RequestBody FrameUser user){
//
//		QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
//		wrapper.eq("login_id", user.getLoginId());
//		wrapper.eq("del_flag", DelFlag.Normal.getCode());
//		FrameUser userVo = new FrameUser();
//		List<FrameUser> userList = userService.list(wrapper);
//
//		if(userList == null || userList.isEmpty()){
//			return R.error("用户名或密码错误");
//		}
//
//		if(userList.size() > 1){
//			return R.error("重复用户,请联系管理员处理");
//		}
//
//		userVo = userList.get(0);
//
//		if(!user.getPassword().equals(userVo.getPassword())){
//			return R.error("用户名或密码错误");
//		}
//
//		//更新最近登录时间
//		UpdateWrapper<FrameUser> updateWrapper = new UpdateWrapper<>();
//		updateWrapper.eq("row_guid", userVo.getRowGuid());
//		userService.update(new FrameUser().setLastLoginTime(LocalDateTime.now()), updateWrapper);
//		return R.ok();
//	}

//	/**
//	 * 根据token获取用户信息
//	 * @return
//	 */
////	@Log(value = "获取用户信息", exception = "获取用户信息请求异常")
//	@GetMapping("/getUserInfo")
//	private R getUserInfo(HttpServletRequest request){
//		return R.ok().put("data",getCurrentUser());
//	}

//	/**
//	 *生成图片验证码
//	 *@return
//	 */
//	@PostMapping("/imgAuth")
//	public R prodImgAuth(@RequestParam(value = "isForget",required = false)String isForget,HttpSession session){
//		//校验是否已经存放
//		userService.checkIsSetImgCode(isForget,session);
//
//		ImagUtil iu = new ImagUtil();
//		String code = "";
//		try {
//			code = iu.createImageWithVerifyCode(isForget,150,37,4,session);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		code = "data:image/png;base64,"+code;
//		return  R.ok().put("data", code);
//	}


//	/**
//	 *注册成为APP使用者
//	 */
////	@PostMapping("/registerAppUser")
//	public R registerAppUser(@RequestBody FrameUserPojo user, HttpSession session){
//
//		userService.registerAppUser(user,session);
//		 return R.ok();
//	}
//
//	/**
//	 *忘记密码
//	 */
////	@PutMapping("/forgetPassword")
//	public R forgetPassword(@RequestBody FrameUserPojo user, HttpSession session){
//
//		userService.forgetPassword(user,session);
//		return R.ok();
//	}


}
