package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.XnwClient;
import com.sinoy.platform.system.component.service.XnwClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/frame/XnwClient")
public class XnwClientController extends BaseController {

	@Autowired
	private XnwClientService xnwClientService;

	@PostMapping("/listData")
    private R listData(@RequestBody Search search){
		QueryWrapper queryWrapper = new QueryWrapper();
		BatisPlusUtil.setParams(queryWrapper,search.getParams());
		BatisPlusUtil.setOrders(queryWrapper,search.getProp(),search.getOrder());
		Page<XnwClient> xnwClientPage = new Page(search.getCurrentPage(), search.getPageSize());
		Page iPage = xnwClientService.page(xnwClientPage,queryWrapper);
		return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		return R.ok().put("data", xnwClientService.selectByGuid(rowGuid));
	}

	@PostMapping("/add")
	private R add(@RequestBody XnwClient xnwClient){
		xnwClient.initNull();
		xnwClientService.save(xnwClient);
		return R.ok();
	}

	@PutMapping("/update")
	private R update(@RequestBody XnwClient xnwClient){
	    xnwClient.setUpdateTime(LocalDateTime.now());
		xnwClientService.updateByRowGuid(xnwClient);
		return R.ok();
	}

	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		xnwClientService.deleteBatchByGuids(rowGuids);
		return R.ok();
	}
}