package com.sinoy.platform.system.controller.admin;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.IsOrNot;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.sql.SQLFilter;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameModule;
import com.sinoy.platform.system.component.entity.dto.FrameModuleDto;
import com.sinoy.platform.system.component.entity.maps.FrameModuleMapstruct;
import com.sinoy.platform.system.component.entity.vo.FrameModuleVo;
import com.sinoy.platform.system.component.service.FrameModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 模块控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameModule")
public class FrameModuleController extends BaseController {

    @Autowired
    private FrameModuleService frameModuleService;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {

        Map<String, Object> params = search.getParams();

        /**
         * 查询本级 与 查询本级与子级  moduleCode 区分
         * moduleCodeCurrent:本级
         * moduleCodeCurrentAndChildren:本级与子级
         */
        String customParams = null;
        String moduleCodeCurrent = (String) params.get("moduleCodeCurrent");
        String moduleCodeCurrentAndChildren = (String) params.get("moduleCodeCurrentAndChildren");
        QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
        //本级
        if (StringUtil.isNotBlank(moduleCodeCurrent)) {
            //除部门模块以外数据使用下面的条件
            if (params.get("activeGuid") == null) {
                return R.error("activeGuid is null");
            }
            queryWrapper.eq("parent_guid", SQLFilter.sqlValidate((String) params.get("activeGuid")));
        }

        //本级与子级
        if (StringUtil.isNotBlank(moduleCodeCurrentAndChildren)) {
            queryWrapper.likeRight("module_code", SQLFilter.sqlValidate(moduleCodeCurrentAndChildren) + ".");
        }

        //根目录 查询本级
        if ("".equals(moduleCodeCurrent)) {
            queryWrapper.eq("parent_guid", "");
        }

        Page<FrameModule> frameModulePage = new Page<>(search.getCurrentPage(), search.getPageSize());
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameModule> iPage = frameModuleService.page(frameModulePage, queryWrapper);

        List<FrameModuleDto> moduleDtoList = FrameModuleMapstruct.INSTANCES.toModuleDtoList(iPage.getRecords());
        /**
         * 查询补充信息  父模块信息
         */
        frameModuleService.addParentModuleInfo(moduleDtoList);
        return R.list((int) iPage.getTotal(), moduleDtoList);
    }

    @GetMapping("/getDetailByGuid")
    @PassToken
    private R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameModuleService.selectByGuid(rowGuid));
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameModule frameModule) {
        frameModule.initNull();
        return R.ok().put("data", frameModuleService.saveModule(frameModule));
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameModule frameModule) {
        return R.ok().put("data", frameModuleService.updateModule(frameModule));
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        //级联删除模块 子模块
        frameModuleService.deleteModule(rowGuids);
        return R.ok();
    }

    /**
     * 异步获取树数据
     *
     * @param
     * @return
     */
    @PostMapping("/treeData")
    private R treeData(@RequestBody FrameModuleVo frameModuleVo) {
        List<FrameModule> moduleList = frameModuleService.selectByEntity(frameModuleVo.getFrameModule(), frameModuleVo.getSort());
        JSONArray array = new JSONArray();
        JSONObject object = null;
        FrameModule mod = null;
        for (FrameModule frameModule : moduleList) {
            object = JSON.parseObject(JSON.toJSONString(frameModule));
            QueryWrapper<FrameModule> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_guid", object.getString("rowGuid"));
            long count = frameModuleService.count(queryWrapper);
            if (count > 0) {
                object.put("children", new ArrayList<>());
            }
            array.add(object);
        }
        return R.ok().put("data", array);
    }


    /**
     * 一次获取全部树数据
     *
     * @return
     */
    @PostMapping("/allTreeData")
    private R allTreeData(@RequestBody FrameModuleVo frameModuleVo) {
        JSONObject obj = frameModuleService.allTreeData(frameModuleVo);
        return R.ok().put("data", obj);
    }

    /**
     * 一次获取全部路由
     *
     * @return
     */
    @GetMapping("/allTreeDataRoute")
    private R allTreeDataRoute() {
        JSONArray array = frameModuleService.allTreeDataRoute();
        return R.ok().put("data", array);
    }

    /**
     * 根据rowGuid 联查(left join frameModule)
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    private R selectParentCode(@RequestParam String rowGuid) {
        List<FrameModuleDto> codeValueList = frameModuleService.selectParentCode(rowGuid);
        return R.ok().put("data", codeValueList);
    }

    /**
     * 获取所有path 为空的模块
     *
     * @return
     */
    @GetMapping("/getAllBlankPathModule")
    public R getAllBlankPathModule() {
        LambdaQueryWrapper<FrameModule> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.isNull(FrameModule::getPath).or().eq(FrameModule::getPath, "");
        queryWrapper.eq(FrameModule::getDelFlag, DelFlag.Normal.getCode());
        List<FrameModule> modules = frameModuleService.list(queryWrapper);
        return R.ok().put("data", modules);
    }

    /**
     * vue3获取用户路由权限
     *
     * @param roleGuids
     * @return JSONArray
     */
    @PostMapping("/getVue3PermissionRoute")
    private R getVue3PermissionRoute(@RequestBody String[] roleGuids) {
        JSONArray array = frameModuleService.getVue3PermissionRoute(roleGuids);
        return R.ok().put("data", array);
    }

    /**
     * vue3获取用户按钮权限
     *
     * @param roleGuids
     * @return JSONArray
     */
    @PostMapping("/getVue3PermissionButton")
    private R getVue3PermissionButton(@RequestBody String[] roleGuids) {
        JSONArray array = frameModuleService.getVue3PermissionRoute(roleGuids);
        return R.ok().put("data", array);
    }

    /**
     * 获取所有正常显示菜单
     * @return
     */
    @GetMapping("/listAllSimple")
    private R listAllSimple(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("del_flag",DelFlag.Normal.getCode());
        queryWrapper.eq("is_hide", IsOrNot.NOT.getCode());
        return R.ok().put("data",frameModuleService.list());
    }
}
 