package com.sinoy.platform.system.controller.app;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.enums.LoginLogTypeEnum;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.oauth2.maps.AuthConvert;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.login.AuthLoginRespVO;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.enumeration.IsOrNotEnum;
import com.sinoy.platform.system.component.service.FrameDeptService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户控制器
 *
 * @author hero
 */
@RestController
@RequestMapping("/frame/FrameUser")
public class AppUserController {

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

    @Autowired
    public FrameUserService frameUserService;
    @Autowired
    public FrameDeptService frameDeptService;
    @Autowired
    public OAuth2TokenService oAuth2TokenService;

    /**
     * 获取用户信息
     * @return
     */
    @GetMapping("/getUserInfo")
    public R getUserInfo() {
        return R.ok().put("data", commonPropAndMethods.getCurrentUser());
    }

    /**
     * 获取用户精简信息列表  只包含被开启的用户，主要用于前端的下拉选项
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimpleUsers() {
        // 获用户门列表，只要开启状态的
        List<FrameUser> list = frameUserService.getUsersByStatus(CommonStatusEnum.ENABLE.getStatus());
        // 排序后，返回给前端
        return R.ok().put("data",list);
    }

    /**
     * 根据人员编号获取所有部门--手机号码+验证码方式登录专用
     * @param userId
     * @return
     */
    @GetMapping("/getDeptsByUserIdForTel")
    public R getDeptsByUserIdForTel(@RequestParam("userId") Long userId) {
        List<FrameDept> deptDOS = frameDeptService.getDeptsByUserIdForTel(userId);
        return R.ok().put("data", deptDOS);
    }

    /**
     * 多部门切换
     *
     * @param deptId
     */
    @PutMapping("/switch-dept")
    public R switchDept(@RequestParam("deptId") Long deptId) {
        //frameUserService.switchDept(deptId);
        Long userId = SecurityFrameworkUtils.getLoginUserId();
        FrameUser adminUserDO = frameUserService.getById(userId);

        //重新登录
        QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
        wrapper.eq("del_flag", "0");
        wrapper.eq("mobile", adminUserDO.getMobile());
        wrapper.eq("dept_id", deptId);
        List<FrameUser> user = frameUserService.list(wrapper);

        if (user.size() > 0) {
            UserInfo userInfo = frameUserService.getUserInfo(user.get(0));
            AuthLoginRespVO reqVO = createTokenAfterLoginSuccess(userInfo, LoginLogTypeEnum.LOGIN_USERNAME);

            //清空user表中的maidept
            QueryWrapper<FrameUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("mobile", adminUserDO.getMobile());
            FrameUser updateUser = new FrameUser();
            updateUser.setMainDept(String.valueOf(IsOrNotEnum.NOT.getCode()));
            frameUserService.update(updateUser, queryWrapper);

            // 设置mainDept
            FrameUser userDO = new FrameUser();
            userDO.setMainDept(String.valueOf(IsOrNotEnum.IS.getCode()));
            frameUserService.update(userDO,
                    new UpdateWrapper<FrameUser>()
                            .eq("mobile", adminUserDO.getMobile())
                            .eq("dept_id", deptId)
                            .eq("del_flag", 0));

            return R.ok().put("data", reqVO);
        } else {
            return R.error("该部门下没有用户");
        }
    }

    private AuthLoginRespVO createTokenAfterLoginSuccess(UserInfo user, LoginLogTypeEnum logType) {
        // 插入登陆日志
//        createLoginLog(user.getRowId(), user.getUserName(), logType, LoginResultEnum.SUCCESS);
        // 创建访问令牌
        OAuth2AccessTokenDO accessTokenDO = oAuth2TokenService.createAccessToken(user, getUserType().getValue(),
                "default", null);
        // 构建返回结果
        return AuthConvert.INSTANCE.convert(accessTokenDO);
    }

    private UserTypeEnum getUserType() {
        return UserTypeEnum.ADMIN;
    }

}
