package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.base.BaseController;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.platform.system.component.entity.FrameConfig;
import com.sinoy.platform.system.component.service.FrameConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.getLoginUserId;

/**
 * 系统参数控制器
 *
 * @author hero
 */
@RestController
@RequestMapping("/frame/FrameConfig")
public class FrameConfigController extends BaseController {

    @Autowired
    private FrameConfigService frameConfigService;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameConfig> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameConfig> iPage = frameConfigService.page(frameDeptPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<FrameConfig> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        FrameConfig frameConfig = frameConfigService.getOne(wrapper);
        return R.ok().put("data", frameConfig);
    }

//    @Autowired
//    private RepositoryService repositoryService;
//
//    @Autowired
//    private FormService formService;


    @PostMapping("/add")
    private R add(@RequestBody FrameConfig frameConfig) {
        //检查是否重复
        QueryWrapper<FrameConfig> wrapper = new QueryWrapper<>();
        wrapper.eq("config_name", frameConfig.getConfigName());
        boolean exist = frameConfigService.getOne(wrapper) != null;
        if (exist) {
            return R.error(frameConfig.getConfigName() + " 该参数名称在数据库中重复");
        }
        frameConfig.initNull();
        frameConfigService.save(frameConfig);
        //更新redis缓存
        frameConfigService.updateConfigCacheByName(frameConfig.getConfigName());

        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameConfig frameConfig) {
        UpdateWrapper<FrameConfig> wrapper = new UpdateWrapper<>();
        wrapper.eq("row_guid", frameConfig.getRowGuid());
        FrameConfig oldData = frameConfigService.getOne(wrapper);
        //检查是否重复
        boolean flag = oldData.getConfigName().equals(frameConfig.getConfigName());
        wrapper.clear();
        wrapper.eq("config_name", frameConfig.getConfigName());
        boolean exist = frameConfigService.getOne(wrapper) != null;
        if (!flag && exist) {
            return R.error(frameConfig.getConfigName() + " 该参数名称在数据库中重复!");
        }
        wrapper.clear();
        wrapper.eq("row_guid", frameConfig.getRowGuid());
        frameConfigService.update(frameConfig, wrapper);
        //更新redis缓存
        frameConfigService.updateConfigCacheByName(frameConfig.getConfigName());
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameConfig> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);
            frameConfigService.remove(wrapper);
        }
        return R.ok();
    }


    /**
     * 获取是否启用验证码信息
     *
     * @return
     */
    @PassToken
    @GetMapping("/checkCaptchaEnable")
    private R checkCaptchaEnable() {
        String value = frameConfigService.getCacheByConfigName("是否启用验证码");
        return R.ok().put("data", Boolean.valueOf(value));
    }
}
 