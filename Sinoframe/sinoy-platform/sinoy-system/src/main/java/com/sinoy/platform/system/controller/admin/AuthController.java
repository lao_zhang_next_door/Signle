package com.sinoy.platform.system.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.constant.Oauth2Constant;
import com.sinoy.core.common.enums.LoginLogTypeEnum;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.encryption.annotation.AsymmetryCrypto;
import com.sinoy.core.oauth2.pojo.login.AuthLoginReqVO;
import com.sinoy.core.oauth2.service.AuthService;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.platform.system.component.entity.vo.AuthSmsLoginReqVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.obtainAuthorization;

@RestController
@RequestMapping("/frame/auth")
@Validated
public class AuthController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private AuthService authService;

    /**
     * 使用cas ticket 登录
     *
     * @param ticket
     * @return
     */
    @PostMapping("/loginByTicket")
    @PassToken
    public R loginByTicket(@RequestParam String ticket) {
        return R.ok().put("data", authService.loginByTicket(ticket));
    }

    /**
     * 使用账号密码登录
     *
     * @param reqVO
     * @return
     */
    @AsymmetryCrypto
    @PostMapping("/login")
    @PassToken
    public R login(@RequestBody @Valid AuthLoginReqVO reqVO) {
        String captchaResult = authService.checkUserCaptcha(reqVO.getCaptchaVO());
        if (captchaResult == null) {
            return R.ok().put("data", authService.login(reqVO));
        }
        return R.error(captchaResult);
    }

    /**
     * 登出系统
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    @PassToken
    public R logout(HttpServletRequest request) {
        String token = obtainAuthorization(request, Oauth2Constant.HEADER_TOKEN);
        if (StrUtil.isNotBlank(token)) {
            authService.logout(token, LoginLogTypeEnum.LOGOUT_SELF.getType());
        }
        return R.ok();
    }

    /**
     * 刷新令牌
     *
     * @param refreshToken
     * @return
     */
    @PostMapping("/refresh-token")
    @PassToken
    public R refreshToken(@RequestParam("refreshToken") String refreshToken) {
        return R.ok().put("data", authService.refreshToken(refreshToken));
    }


    /**
     * 短信登录
     *
     * @param reqVO
     * @return
     */
    @PostMapping("/smsLogin")
    @PassToken
    public R smsLogin(@RequestBody @Valid AuthSmsLoginReqVO reqVO) {

        return R.ok().put("data", authService.smsLogin(reqVO));

    }

    /**
     * 发送短信
     *
     * @param mobile
     * @return
     */
    @PostMapping("/sendCode")
    @PassToken
    public R sendCode(@RequestParam("mobile") String mobile) {
        authService.sendCode(mobile);
        return R.ok();


    }


}
