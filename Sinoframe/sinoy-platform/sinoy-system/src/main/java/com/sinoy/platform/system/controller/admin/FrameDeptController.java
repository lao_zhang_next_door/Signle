package com.sinoy.platform.system.controller.admin;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.sql.SQLFilter;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.datascope.anno.DataColumn;
import com.sinoy.core.datascope.anno.DataPermission;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.FrameDeptDto;
import com.sinoy.platform.system.component.entity.maps.FrameDeptMapstruct;
import com.sinoy.platform.system.component.entity.vo.FrameDeptVo;
import com.sinoy.platform.system.component.enumeration.DeptStatus;
import com.sinoy.platform.system.component.service.FrameDeptService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * 部门控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameDept")
public class FrameDeptController extends BaseController {

    @Autowired
    public FrameDeptService frameDeptService;
    @Autowired
    private FrameUserService frameUserService;

    @DataPermission({
            @DataColumn(key = "column", value = "row_id")
    })
    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        /**
         * 查询本级 与 查询本级与子级  deptCode 区分
         * deptCodeCurrent:本级
         * deptCodeCurrentAndChildren:本级与子级
         */
        String deptCodeCurrent = (String) params.get("deptCodeCurrent");
        String deptCodeCurrentAndChildren = (String) params.get("deptCodeCurrentAndChildren");

        QueryWrapper<FrameDept> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("del_flag",0);
        //本级
        if (StringUtil.isNotBlank(deptCodeCurrent)) {
            //除部门模块以外数据使用下面的条件
            if (params.get("activeGuid") == null) {
                return R.error("activeGuid is null");
            }
            queryWrapper.eq("parent_guid", SQLFilter.sqlValidate((String) params.get("activeGuid")));
        }

        //本级与子级
        if (StringUtil.isNotBlank(deptCodeCurrentAndChildren)) {
            queryWrapper.likeRight("dept_code", SQLFilter.sqlValidate(deptCodeCurrentAndChildren) + ".");
        }

        //根目录 查询本级
        if ("".equals(deptCodeCurrent)) {
            queryWrapper.eq("parent_guid", "");
        }
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameDept> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameDept> iPage = frameDeptService.page(frameDeptPage, queryWrapper);
        List<FrameDeptDto> deptDtoList = FrameDeptMapstruct.INSTANCES.toDeptDtoList(iPage.getRecords());

        /**
         * 补充父级部门信息
         */
        frameDeptService.addParentDeptInfo(deptDtoList);
        return R.list((int) iPage.getTotal(), deptDtoList);
    }

    @GetMapping("/getDetailByGuid")
    public R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameDeptService.selectByGuid(rowGuid));
    }

    /**
     * 新增部门
     *
     * @param frameDept
     * @return
     */
    @PostMapping("/add")
    public R add(@RequestBody FrameDept frameDept) {
        frameDept.initNull();
        if (!StringUtil.isNullOrEmpty(frameDept.getLeaderUserGuid())) {
            frameDept.setLeaderUser(frameUserService.selectDetailByUserGuid(frameDept.getLeaderUserGuid()).getUserName());
        }
        return R.ok("创建成功").put("data", frameDeptService.saveDept(frameDept));
    }

    /**
     * 修改部门
     *
     * @param frameDept
     * @return
     */
    @PutMapping("/update")
    public R update(@RequestBody FrameDept frameDept) {
        if (!StringUtil.isNullOrEmpty(frameDept.getLeaderUserGuid())) {
            frameDept.setLeaderUser(frameUserService.selectDetailByUserGuid(frameDept.getLeaderUserGuid()).getUserName());
        }
        return R.ok().put("data", frameDeptService.update(frameDept));
    }

    /**
     * 删除部门以及用户
     *
     * @param rowGuids
     * @return
     */
    @DeleteMapping("/delete")
    @Transactional(rollbackFor = Exception.class)
    public R deleteBatch(@RequestBody String[] rowGuids) {
        //级联删除部门子部门与相关用户 以及业务相关数据
        frameDeptService.deleteDeptAndUserAndMore(rowGuids);
        return R.ok();
    }

    /**
     * 获取树数据 全部加载
     *
     * @param
     * @return
     */
    @DataPermission({
            @DataColumn(key = "column", value = "row_id")
    })
    @PostMapping("/treeData")
    public R treeData(@RequestBody FrameDeptVo frameDeptV) {
        return R.ok().put("data", frameDeptService.getAllTreeData(frameDeptV.getSort()));
    }

    /**
     * 获取指定（单位）部门下的所有层级树
     * <p>
     * self: 是否包含自己层级
     *
     * @param
     * @return
     */
    @PostMapping("/treeDataByDeptId")
    public R treeDataByDeptId(@RequestParam String deptId, @RequestParam Boolean self, @RequestParam(required = false) String[] excludeDeptIds) {
        return R.ok().put("data", frameDeptService.getTreeDataByDeptId(deptId, self, excludeDeptIds));
    }

    /**
     * 懒加载树 根据parentGuid 加载
     *
     * @param frameDeptV
     * @return
     */
    @PostMapping("/treeDataLazy")
    public R treeDataLazy(@RequestBody FrameDeptVo frameDeptV) {
        return R.ok().put("data", frameDeptService.getTreeDataLazy(frameDeptV));
    }

    /**
     * 根据rowGuid 联查(left join frameDept) 查询上级deptName
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    public R selectParentCode(@RequestParam String rowGuid) {
        FrameDeptDto frameDeptDto = frameDeptService.selectDeptAndParentDept(rowGuid);
        return R.ok().put("data", frameDeptDto);
    }

    /**
     * 以固定格式 获取当前部门的上级部门所有人员的树数据
     *
     * @param frameDept
     * @return
     */
    @PostMapping("/treeDataPerson")
    public R treeDataPerson(@RequestBody FrameDept frameDept) {
        List<FrameUser> frameUserList = null;
        FrameDept frameDeptEntity = frameDeptService.selectOneByEntity(frameDept);
        if (frameDeptEntity != null && StringUtil.isNotBlank(frameDeptEntity.getParentGuid())) {
            String parentGuid = frameDeptEntity.getParentGuid();
            if (StringUtil.isNotBlank(parentGuid)) {
                FrameUser frameUser = new FrameUser();
                frameUser.setDeptGuid(parentGuid);
                frameUserList = frameUserService.selectByEntity(frameUser);
            }
        }
        JSONArray array = JSON.parseArray(JSON.toJSONString(frameUserList));
        return R.ok().put("data", array);
    }

    /**
     * 一次获取全部树数据
     *
     * @return
     */
    @PostMapping("/allTreeData")
    @DataPermission({
            @DataColumn(key = "column", value = "row_id")
    })
    public R allTreeData(@RequestBody FrameDeptVo frameDeptVo) {
        JSONObject obj = frameDeptService.allTreeData(frameDeptVo);
        return R.ok().put("data", obj);
    }

    /**
     * 获取部门精简信息列表 只包含被开启的部门，主要用于前端的下拉选项
     *
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimpleDepts() {
        // 获得部门列表，只要开启状态的
        List<FrameDept> list = frameDeptService.getSimpleDepts(DeptStatus.NORMAL.getCode());
        // 排序后，返回给前端
        list.sort(Comparator.comparing(FrameDept::getSortSq));
        return R.ok().put("data", list);
    }

    /**
     * 根据人员编号获取所有部门--手机号码+验证码方式登录专用
     *
     * @param userId
     * @return
     */
    @GetMapping("/getDeptsByUserIdForTel")
    public R getDeptsByUserIdForTel(@RequestParam("userId") Long userId) {
        List<FrameDept> deptDOS = frameDeptService.getDeptsByUserIdForTel(userId);
        return R.ok().put("data", deptDOS);
    }

}
 