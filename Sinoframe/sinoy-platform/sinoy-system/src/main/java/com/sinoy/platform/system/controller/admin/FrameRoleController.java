package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.base.BaseController;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.platform.system.component.entity.*;
import com.sinoy.platform.system.component.entity.vo.DataPermissionVo;
import com.sinoy.platform.system.component.module.system.dal.dataobject.rolelimit.RoleLimitDO;
import com.sinoy.platform.system.component.module.system.service.rolelimit.RoleLimitService;
import com.sinoy.platform.system.component.service.FrameConfigService;
import com.sinoy.platform.system.component.service.FrameRoleDeptService;
import com.sinoy.platform.system.component.service.FrameRoleService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 角色控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameRole")
public class FrameRoleController extends BaseController {

    @Autowired
    public FrameRoleService frameRoleService;

    @Autowired
    public FrameRoleDeptService frameRoleDeptService;


    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        QueryWrapper<FrameRole> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameRole> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameRole> iPage = frameRoleService.page(frameDeptPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    public R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameRoleService.selectByGuid(rowGuid));
    }

    @PostMapping("/add")
    public R add(@RequestBody FrameRole frameRole) {
        frameRole.initNull();
        frameRoleService.save(frameRole);
        return R.ok();
    }

    @PutMapping("/update")
    public R update(@RequestBody FrameRole frameRole) {
        frameRoleService.updateByRowGuid(frameRole);
        return R.ok();
    }

    @DeleteMapping("/delete")
    @Transactional
    public R deleteBatch(@RequestBody String[] rowGuids) {
        frameRoleService.deleteBatchByGuids(rowGuids);
        //删除数据权限
        for (String roleGuid:rowGuids) {
            QueryWrapper<FrameRoleDept> delWrapper = new QueryWrapper();
            delWrapper.eq("role_guid",roleGuid);
            frameRoleDeptService.remove(delWrapper);
        }

        return R.ok();
    }

    @GetMapping("/getAllRole")
    public R getAllRole() {
        List<FrameRole> list=frameRoleService.getAllRole();
        return R.ok().put("data", list);
    }

    /**
     * 根据角色guid 查询该角色所拥有的菜单模块
     *
     * @return
     */
    @PostMapping("/getModuleByRoleGuid")
    public R getModuleByRoleGuid(@RequestParam String roleGuid) {
        List<FrameModule> moduleList = frameRoleService.getModuleByRoleGuid(roleGuid);
        return R.ok().put("data", moduleList);
    }

    /**
     * 根据角色guid 获取 部门列表
     *
     * @return
     */
    @PostMapping("/getDeptByRoleGuid")
    public R getDeptByRoleGuid(@RequestParam String roleGuid) {
        List<FrameDept> deptList = frameRoleService.getDeptByRoleGuid(roleGuid);
        return R.ok().put("data", deptList);
    }

    /**
     * 保存数据权限
     * @param dataPermissionVo
     * @return
     */
    @PostMapping("/saveDataPermission")
    @Transactional
    public R saveDataPermission(@RequestBody DataPermissionVo dataPermissionVo){
        //保存数据权限
        FrameRole vo = dataPermissionVo.getFrameRole();
        if(vo == null){
            return R.error("操作失败,请选择一个数据权限");
        }

        if(StringUtil.isBlank(vo.getRowGuid())){
            return R.error("操作失败,请先选择一个角色");
        }
        frameRoleService.saveDataPermission(vo,dataPermissionVo.getFrameRoleDeptList());
        return R.ok("设置成功");
    }
}
 