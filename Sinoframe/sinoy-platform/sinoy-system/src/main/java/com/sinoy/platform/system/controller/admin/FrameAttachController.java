package com.sinoy.platform.system.controller.admin;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.text.AntPathMatcher;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.request.ServletUtils;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameAttach;
import com.sinoy.platform.system.component.entity.dto.FrameAttachDto;
import com.sinoy.platform.system.component.service.FrameAttachService;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 附件控制器
 */
@RestController
@RequestMapping("/frame/FrameAttach")
public class FrameAttachController extends BaseController {

    @Autowired
    private FrameAttachService frameAttachService;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Autowired
    private FrameUserService frameUserService;

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Page<FrameAttach> frameUserPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        QueryWrapper<FrameAttach> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        IPage<FrameAttach> iPage = frameAttachService.page(frameUserPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @PassToken
    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameAttachService.selectByGuid(rowGuid));
    }


    /**
     * 根据formRowGuid查询附件列表
     *
     * @param formRowGuid
     * @return
     */
    @PassToken
    @GetMapping("/getListByFormRowGuid")
    private R getListByFormRowGuid(@RequestParam String formRowGuid) {
        FrameAttach frameAttach = new FrameAttach();
        frameAttach.setFormRowGuid(formRowGuid);
        List<FrameAttach> attachList = frameAttachService.selectByEntity(frameAttach);
        return R.ok().put("data", attachList);
    }

    /**
     * 根据formRowGuid删除附件
     *
     * @param formRowGuid
     * @return
     */
    @DeleteMapping("/deleteFileByFormRowGuid")
    private R deleteFileByFormRowGuid(@RequestParam String formRowGuid) throws Exception {
//        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
//        queryWrapper.eq("form_row_guid",formRowGuid);
//        List<FrameAttach> attachList = frameAttachService.list(queryWrapper);
//        attachList.forEach(frameAttach -> {
//            try {
//                frameAttachService.deleteFile(frameAttach.getRowId());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
        frameAttachService.deleteFileByFormRowGuid(formRowGuid);
        return R.ok();
    }

    /**
     * 根据rowGuid删除附件
     *
     * @param rowGuid
     * @return
     */
    @DeleteMapping("/deleteFileByRowGuid")
    public R deleteFileByRowGuid(@RequestParam String rowGuid) {
//        FrameAttach attach = frameAttachService.selectByGuid(rowGuid);
//        if (attach == null) {
//            return R.error("未找到该文件");
//        }
//        try {
//            frameAttachService.deleteFile(attach.getRowId());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        frameAttachService.deleteFileByRowGuids(new String[]{rowGuid});
        return R.ok();
    }

    /**
     * 上传文件
     *
     * @param file 文件附件
     * @param path 文件路径
     * @return
     * @throws Exception
     */
    @PostMapping("/uploadFile")
    public R uploadFile(@RequestParam("uploadify") MultipartFile file,
                        @RequestParam(value = "path", required = false) String path,
                        @RequestParam(value = "formRowGuid", required = false) String formRowGuid) throws Exception {

        FrameAttach attach = frameAttachService.createFile(file.getOriginalFilename(), path, IoUtil.readBytes(file.getInputStream()), formRowGuid);
        Map<String, String> map = new HashMap<>(3);
        map.put("src", attach.getUrl());
        map.put("name", attach.getAttachName());
        map.put("rowGuid", attach.getRowGuid());
        map.put("formRowGuid", attach.getFormRowGuid());
        return R.ok().put("data", map);
    }

    /**
     * 删除文件
     *
     * @return
     * @throws Exception
     */
    @DeleteMapping("/delete")
    public R deleteFile(@RequestBody String[] rowGuids) {
        try {
            frameAttachService.deleteFileByRowGuids(rowGuids);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.ok();
    }

    /**
     * 下载文件
     *
     * @param response
     * @param configId 配置编号
     * @param path     文件路径
     * @throws Exception
     */
    @GetMapping("/{configId}/get")
    @PassToken
    public void getFileContent(HttpServletResponse response,
                               @PathVariable("configId") Long configId,
                               @RequestParam("path") String path) throws Exception {
        byte[] content = frameAttachService.getFileContent(configId, path);
        if (content == null) {
            logger.warn("[getFileContent][configId({}) path({}) 文件不存在]", configId, path);
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }
        ServletUtils.writeAttachment(response, path, content);
    }

    /**
     * vue3上传文件
     *
     * @param frameAttachDto 文件附件
     * @return
     * @throws Exception
     */
    @PostMapping("/vue3uploadFile")
    public R vue3uploadFile(FrameAttachDto frameAttachDto) throws Exception {
        FrameAttach attach = frameAttachService.createFile(frameAttachDto.getFile().getOriginalFilename(), frameAttachDto.getPath(), IoUtil.readBytes(frameAttachDto.getFile().getInputStream()), frameAttachDto.getFormRowGuid());
        Map<String, String> map = new HashMap<>(2);
        map.put("src", attach.getUrl());
        map.put("name", attach.getAttachName());
        map.put("rowGuid", attach.getRowGuid());
        return R.ok().put("data", map);
    }

    /**
     * vue3移动端上传文件
     *
     * @param formRowGuid
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/vue3MobileUploadFile")
    public R vue3MobileUploadFile(@RequestParam(value = "formRowGuid") String formRowGuid, @RequestParam(value = "uploadify") MultipartFile file, @RequestParam(value = "fileName") String fileName) throws Exception {
        FrameAttach attach = frameAttachService.createFile(fileName, "", IoUtil.readBytes(file.getInputStream()), formRowGuid);
        Map<String, String> map = new HashMap<>(2);
        map.put("src", attach.getUrl());
        map.put("name", attach.getAttachName());
        map.put("rowGuid", attach.getRowGuid());
        return R.ok().put("data", map);
    }


}
 