package com.sinoy.platform.system.controller.app;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.platform.system.component.entity.FrameSchedule;
import com.sinoy.platform.system.component.entity.dto.FrameScheduleDto;
import com.sinoy.platform.system.component.service.FrameScheduleService;
import com.sinoy.platform.system.component.utils.PageUtils;
import com.sinoy.platform.system.component.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/frame/FrameSchedule")
public class AppScheduleController {

	@Autowired
	private FrameScheduleService frameScheduleService;

	@GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
		List<FrameScheduleDto> frameScheduleList = frameScheduleService.getList(query);
        Integer count = frameScheduleService.getListCount(query);
        PageUtils pageUtil = new PageUtils(frameScheduleList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		return R.ok().put("data", frameScheduleService.selectByGuid(rowGuid));
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameSchedule frameSchedule){
		frameSchedule.initNull();
		frameScheduleService.save(frameSchedule);
		return R.ok();
	}
	
	@PutMapping("/update")
	private R update(@RequestBody FrameSchedule frameSchedule){
	    frameSchedule.setUpdateTime(LocalDateTime.now());
		frameScheduleService.updateByRowGuid(frameSchedule);
		return R.ok();
	}
	
	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		frameScheduleService.deleteBatchByGuids(rowGuids);
		return R.ok();
	}

	/**
	 * 获取未做代办项数量
	 * @return
	 */
	@GetMapping("/getInComplete")
	private R getInComplete(){
		QueryWrapper<FrameSchedule> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
		queryWrapper.isNull("sch_viewed").or().eq("sch_viewed","");
		return R.ok().put("data",frameScheduleService.count(queryWrapper));
	}

	/**
	 * 处理事件 更新是否处理字段
	 * @return
	 */
	@PutMapping("/dealSchedule")
	private R dealSchedule(@RequestParam String rowGuid){
		FrameSchedule frameSchedule = new FrameSchedule(rowGuid);
		frameSchedule.setSchViewed("是");
		frameScheduleService.updateByRowGuid(frameSchedule);
		return R.ok();
	}
}