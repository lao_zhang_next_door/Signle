package com.sinoy.platform.system.controller.admin;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameCode;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.entity.pojo.FrameCodeValuePojo;
import com.sinoy.platform.system.component.service.FrameCodeService;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 代码项控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameCodeValue")
public class FrameCodeValueController extends BaseController {

    @Autowired
    private FrameCodeValueService frameCodeValueService;
    @Autowired
    private FrameCodeService frameCodeService;

    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        QueryWrapper<FrameCodeValue> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameCodeValue> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameCodeValue> iPage = frameCodeValueService.page(frameDeptPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameCodeValueService.selectByGuid(rowGuid));
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameCodeValue frameCodeValue) {
        frameCodeValue.initNull();
        FrameCode frameCode =frameCodeService.selectByGuid(frameCodeValue.getCodeGuid());
        if(frameCode!=null)
        {
            frameCodeValue.setDictType(frameCode.getDictType());
        }
        if (frameCodeValueService.save(frameCodeValue)) {
            frameCodeValueService.cacheCodesMapByValue(frameCodeValue.getRowGuid());
        }
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameCodeValue frameCodeValue) {
        int code = frameCodeValueService.updateByRowGuid(frameCodeValue);
        if (code > 0) {
            frameCodeValueService.cacheCodesMapByValue(frameCodeValue.getRowGuid());
        }
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids, @RequestParam("codeName") String codeName) {
        frameCodeValueService.deleteBatchByGuids(rowGuids);
        frameCodeValueService.cacheCodesMapByName(codeName);
        return R.ok();
    }

    /**
     * 获取树数据
     *
     * @param frameCodeValue
     * @return
     */
    @PostMapping("/treeData")
    private R treeData(@RequestBody FrameCodeValue frameCodeValue) {
        List<FrameCodeValue> codeValueList = frameCodeValueService.selectByEntity(frameCodeValue);
        JSONArray array = JSON.parseArray(JSON.toJSONString(codeValueList));
        JSONObject object = null;
        FrameCodeValue codev = null;
        for (int i = 0; i < array.size(); i++) {
            object = array.getJSONObject(i);
            codev = new FrameCodeValue();
            codev.setParentGuid(object.getString("rowGuid"));
            Long count = frameCodeValueService.selectCountByEntity(codev);
            if (count > 0) {
                object.put("children", new ArrayList<>());
            }
        }

        return R.ok().put("data", array);
    }

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    private R selectParentCode(@RequestParam String rowGuid) {
        List<FrameCodeValuePojo> codeValueList = frameCodeValueService.selectParentCode(rowGuid);
        return R.ok().put("data", codeValueList);
    }
}
 