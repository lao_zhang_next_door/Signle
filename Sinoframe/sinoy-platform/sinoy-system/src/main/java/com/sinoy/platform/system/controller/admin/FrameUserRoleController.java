package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.FrameUserRole;
import com.sinoy.platform.system.component.service.FrameUserRoleService;
import com.sinoy.platform.system.component.utils.PageUtils;
import com.sinoy.platform.system.component.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 权限控制器
 * @author hero
 *
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameUserRole")
public class FrameUserRoleController extends BaseController {

	@Autowired
	private FrameUserRoleService frameUserRoleService;
	
	@GetMapping("/listData")
	private R listData(@RequestParam Search search){
		Page<FrameUserRole> frameUserPage = new Page(search.getCurrentPage(), search.getPageSize());
		QueryWrapper queryWrapper = new QueryWrapper();
		BatisPlusUtil.setParams(queryWrapper,search.getParams());
		BatisPlusUtil.setOrders(queryWrapper,search.getProp(),search.getOrder());
		IPage iPage = frameUserRoleService.page(frameUserPage,queryWrapper);
		return R.list((int) iPage.getTotal(), iPage.getRecords());
	}
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		QueryWrapper<FrameUserRole> wrapper = new QueryWrapper<>();
		wrapper.eq("row_guid", rowGuid);
		List<FrameUserRole> UserRoleList = frameUserRoleService.list(wrapper);
		return R.ok().put("data", UserRoleList);
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameUserRole frameUserRole){
		frameUserRole.initNull();
		frameUserRoleService.save(frameUserRole);
		return R.ok();
	}
	
	@PutMapping("/update")
	private R update(@RequestBody FrameUserRole frameUserRole){
		UpdateWrapper<FrameUserRole> wrapper = new UpdateWrapper();
		wrapper.eq("row_guid", frameUserRole.getRoleGuid());
		frameUserRoleService.update(frameUserRole, wrapper);
		return R.ok();
	}
	
	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		for(String rowGuid: rowGuids){
			QueryWrapper<FrameUserRole> wrapper = new QueryWrapper<>();
			wrapper.eq("row_guid",rowGuid);
			frameUserRoleService.remove(wrapper);
		}
		return R.ok();
	}

	/**
	 * 根据角色guid 获取 所属用户列表  分页
	 * @return
	 */
	@GetMapping("/getUserByRoleGuid")
	private R getUserByRoleGuid(@RequestParam Map<String,Object> params){

		if(!params.containsKey("roleGuid") && StringUtil.isBlank((String)params.get("roleGuid"))){
			return R.error("缺少参数roleGuid");
		}

		Query query = new Query(params);
		List<FrameUser> userList = frameUserRoleService.getLimitUserByRoleGuid(query);
		Integer count = frameUserRoleService.getCountUserByRoleGuid(query);
		PageUtils pageUtil = new PageUtils(userList, count, query.getLimit(), query.getPage());
		return R.list(pageUtil.getTotalCount(), pageUtil.getList());
	}

}
 