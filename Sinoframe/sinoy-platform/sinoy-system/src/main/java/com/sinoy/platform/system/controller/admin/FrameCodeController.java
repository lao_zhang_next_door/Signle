package com.sinoy.platform.system.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameCode;
import com.sinoy.platform.system.component.entity.FrameCodeValue;
import com.sinoy.platform.system.component.service.FrameCodeService;
import com.sinoy.platform.system.component.service.FrameCodeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 代码项控制器
 *
 * @author hero
 */
/*@CrossOrigin*/
@RestController
@RequestMapping("/frame/FrameCode")
public class FrameCodeController extends BaseController {

    @Autowired
    private FrameCodeService frameCodeService;

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @PostMapping("/listData")
    private R listData(@RequestBody Search search) {
        Page<FrameCode> frameCodePage = new Page(search.getCurrentPage(), search.getPageSize());
        QueryWrapper<FrameCode> queryWrapper = new QueryWrapper();
        BatisPlusUtil.setParams(queryWrapper,search.getParams());
        BatisPlusUtil.setOrders(queryWrapper,search.getProp(),search.getOrder());
        IPage iPage = frameCodeService.page(frameCodePage,queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", frameCodeService.selectByGuid(rowGuid));
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameCode frameCode) {
        frameCode.initNull();
        frameCodeService.save(frameCode);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameCode frameCode) {
        frameCodeService.updateByRowGuid(frameCode);

        if(StringUtil.isNotBlank(frameCode.getDictType())){
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.set("dict_type",frameCode.getDictType());
            updateWrapper.eq("code_guid",frameCode.getRowGuid());
            frameCodeValueService.update(updateWrapper);
        }

        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            //删除缓存中的代码项
            FrameCode frameCode = frameCodeService.selectByGuid(rowGuid);
            frameCodeValueService.deleteCodesMapByName(frameCode.getCodeName());
            //删除frameCodeValue中的值
            frameCodeValueService.deleteCodeValue(rowGuid);
        }
        frameCodeService.deleteBatchByGuids(rowGuids);
        return R.ok();
    }

    @GetMapping("/selectByCodeName")
    private R selectByCodeName(@RequestParam String codeName) {
        FrameCode frameCode = new FrameCode();
        frameCode.setCodeName(codeName);
        List<FrameCode> codeList = frameCodeService.selectByEntity(frameCode);
        if (codeList.isEmpty()) {
            return R.error(codeName + "代码项为空");
        }
        if (codeList.size() > 1) {
            return R.error("系统异常,有重复代码项》》》" + codeName);
        }
        FrameCodeValue codeValue = new FrameCodeValue();
        codeValue.setCodeGuid(codeList.get(0).getRowGuid());
        return R.ok().put("data", frameCodeValueService.selectByEntity(codeValue));
    }

    @GetMapping("/selectCacheByCodeName")
    private R selectCacheByCodeName(@RequestParam String codeName) {
        return R.ok().put("data", frameCodeValueService.getCacheByCodeName(codeName));
    }

    /**
     * 查询所有字典赋予前端缓存
     * 所有字典值 [{text:'男',val:'0'},{text:'女',val:'1'},...]
     * @return
     */
    @GetMapping("/listSimpleDictDatas")
    private R listSimpleDictDatas(){
        QueryWrapper queryWrapper = new QueryWrapper();
        BatisPlusUtil.setOrders(queryWrapper,null,null);
        return R.ok().put("data", frameCodeValueService.list(queryWrapper));
    }

    /**
     * 查询所有字典项  所有项 没有值  [{性别},{状态}...]
     * @return
     */
    @GetMapping("/listAllSimple")
    private R listAllSimple(){
        return R.ok().put("data", frameCodeService.listAll());
    }
}
 