package com.sinoy.platform.system.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.sharding.base.BaseController;
import com.sinoy.platform.system.component.entity.FrameAttachConfig;
import com.sinoy.platform.system.component.service.FrameAttachConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 附件配置控制器
 *
 */
@RestController
@RequestMapping("/frame/FrameAttachConfig")
public class FrameAttachConfigController extends BaseController {

    @Autowired
    public FrameAttachConfigService frameAttachConfigService;

    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        Map<String, Object> params = search.getParams();
        QueryWrapper<FrameAttachConfig> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FrameAttachConfig> frameDeptPage = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FrameAttachConfig> iPage = frameAttachConfigService.page(frameDeptPage, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @GetMapping("/getDetailByGuid")
    public R getDetailByGuid(@RequestParam String rowGuid) {
        QueryWrapper<FrameAttachConfig> wrapper = new QueryWrapper<>();
        wrapper.eq("row_guid", rowGuid);
        FrameAttachConfig frameAttachConfig = frameAttachConfigService.getOne(wrapper);
        return R.ok().put("data", frameAttachConfig);
    }

    /**
     * 更新 文件 配置
     * @param frameAttachConfig
     * @return
     */
//    @PreAuthorize("hasRole('admin')")
    @PutMapping("/update")
    @Transactional
    public R update(@RequestBody FrameAttachConfig frameAttachConfig) {
        frameAttachConfigService.updateFileAttachConfig(frameAttachConfig);
        return R.ok("修改成功");
    }

    /**
     * 更新为主配置
     * @return
     */
//    @PreAuthorize("hasRole('admin')")
    @PutMapping("/updateMaster")
    @Transactional
    public R updateMaster(@RequestParam String masterRowGuid){
        frameAttachConfigService.updateMaster(masterRowGuid);
        return R.ok("修改成功");
    }

//    @PreAuthorize("hasRole('admin')")
    @DeleteMapping("/delete")
    @Transactional
    public R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            QueryWrapper<FrameAttachConfig> wrapper = new QueryWrapper<>();
            wrapper.eq("row_guid", rowGuid);

            FrameAttachConfig attachConfig = frameAttachConfigService.getOne(wrapper);
            if(attachConfig == null || attachConfig.getMaster()){
                throw new BaseException("主配置不可删除！");
            }
            frameAttachConfigService.remove(wrapper);
        }
        return R.ok("删除成功");
    }

}
 