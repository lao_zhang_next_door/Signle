package com.sinoy.core.encryption.annotation;

import com.sinoy.core.encryption.constants.CryptoType;
import com.sinoy.core.encryption.constants.EncodingType;
import java.lang.annotation.*;

/**
 * 编码注解
 *
 * @author wzl
 * @since 1.0.0.RELEASE
 */

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ApiCrypto(encryptType = CryptoType.ENCODING, decryptType = CryptoType.ENCODING)
public @interface EncodingCrypto {

    /**
     * 编码类型
     * <p>
     * 默认为配置文件配置的编码类型
     *
     * @return cn.hermesdi.crypto.constants.EncodingType 编码 类型枚举
     * @author wzl
     **/
    EncodingType encodingType() default EncodingType.DEFAULT;
}
