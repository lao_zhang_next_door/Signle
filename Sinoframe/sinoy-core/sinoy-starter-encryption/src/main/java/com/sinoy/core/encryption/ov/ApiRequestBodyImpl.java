package com.sinoy.core.encryption.ov;

import cn.hutool.json.JSONObject;
import com.sinoy.core.encryption.annotation.AsymmetryCrypto;
import com.sinoy.core.encryption.bean.ApiCryptoBody;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class ApiRequestBodyImpl implements IApiRequestBody{

    @Override
    public ApiCryptoBody requestBody(Annotation annotation, InputStream inputStream) {

        AsymmetryCrypto asymmetryCrypto = (AsymmetryCrypto) annotation;
        String frontKey = asymmetryCrypto.frontKey();
        ApiCryptoBody apiCryptoBody = new ApiCryptoBody();
        try {
            BufferedReader bR = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            StringBuilder responseStrBuilder = new StringBuilder();
            while((line =  bR.readLine()) != null){
                responseStrBuilder.append(line);
            }
            inputStream.close();
            JSONObject obj= new JSONObject(responseStrBuilder.toString());


            // 使用反射复制与ApiCryptoBody对象相同字段名的字段值
            Field[] objFields = obj.getClass().getDeclaredFields();
            for (Field objField : objFields) {
                String fieldName = objField.getName();
                if (fieldName.equals(frontKey)) {
                    // 复制frontKey字段的值到ApiCryptoBody的data字段
                    objField.setAccessible(true);
                    Object fieldValue = objField.get(obj);
                    Field dataField = ApiCryptoBody.class.getDeclaredField("data");
                    dataField.setAccessible(true);
                    dataField.set(apiCryptoBody, fieldValue.toString());
                } else {
                    // 复制其他字段的值到ApiCryptoBody对象中
                    Field apiCryptoBodyField = ApiCryptoBody.class.getDeclaredField(fieldName);
                    if(apiCryptoBodyField.getName().equals(fieldName)){
                        apiCryptoBodyField.setAccessible(true);
                        objField.setAccessible(true);
                        Object fieldValue = objField.get(obj);
                        apiCryptoBodyField.set(apiCryptoBody, fieldValue);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return apiCryptoBody;
    }
}
