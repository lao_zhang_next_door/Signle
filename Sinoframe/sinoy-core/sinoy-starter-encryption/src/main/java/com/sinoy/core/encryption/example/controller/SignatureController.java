//package com.sino.core.encryption.example.controller;
//
//import com.sino.core.encryption.annotation.SignatureCrypto;
//import com.sino.core.encryption.example.bean.TestBean;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Collections;
//
///**
// * 接口数据签名、验签
// *
// * @author T470
// */
//@RestController
//public class SignatureController {
//
//    //    @NotDecrypt // 不需要解密
////    @NotEncrypt // 不需要加密
//    @SignatureCrypto(timeout = 60)
//    @PostMapping("/signature")
//    public TestBean signature(@RequestBody TestBean req) {
//
//        System.out.println(req.toString());
//
//        TestBean testBean = new TestBean();
//        testBean.setAnInt(0);
//        testBean.setInteger(1);
//        testBean.setString("test string");
//        testBean.setStringList(Collections.singletonList("list"));
//        testBean.setObjectMap(Collections.singletonMap("test", "map"));
//
//        return testBean;
//    }
//
//}
