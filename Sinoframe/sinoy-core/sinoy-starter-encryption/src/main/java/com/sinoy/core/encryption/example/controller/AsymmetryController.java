//package com.sino.core.encryption.example.controller;
//
//import com.sino.core.common.utils.request.R;
//import com.sino.core.encryption.annotation.AsymmetryCrypto;
//import com.sino.core.encryption.annotation.NotDecrypt;
//import com.sino.core.encryption.constants.AsymmetryType;
//import com.sino.core.encryption.example.bean.TestBean;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Collections;
//
///**
// * 非对称性加密、解密
// *
// * @author T470
// */
//@RestController
//public class AsymmetryController {
//
//
//    @NotDecrypt // 不需要解密
////    @NotEncrypt // 不需要加密
//    @AsymmetryCrypto(
//            // 响应内容签名
//            signature = true,
//            // 请求内容验证签名
//            verifySignature = true,
//            // 加密、解密算法
//            type = AsymmetryType.RSA_ECB_PKCS1_PADDING
//    )
//    @PostMapping("/rsaTest")
//    public R rsaTest(@RequestBody TestBean req) throws Exception {
//
//        System.out.println(req.toString());
//
//        TestBean testBean = new TestBean();
//        testBean.setAnInt(0);
//        testBean.setInteger(1);
//        testBean.setString("test string");
//        testBean.setStringList(Collections.singletonList("list"));
//        testBean.setObjectMap(Collections.singletonMap("test", "map"));
//
//        return R.ok().put("data", testBean);
//    }
//
//    @PostMapping("/rsaTest2")
//    public R rsaTest2(@RequestBody TestBean req) throws Exception {
//
//        System.out.println(req.toString());
//
//        TestBean testBean = new TestBean();
//        testBean.setAnInt(0);
//        testBean.setInteger(1);
//        testBean.setString("test string");
//        testBean.setStringList(Collections.singletonList("list"));
//        testBean.setObjectMap(Collections.singletonMap("test", "map"));
//
//        return R.ok().put("data", testBean);
//    }
//}
