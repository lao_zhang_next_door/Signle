//package com.sino.core.encryption.annotation;
//
//import com.sino.core.encryption.advice.DecryptRequestBodyAdvice;
//import com.sino.core.encryption.advice.EncryptResponseBodyAdvice;
//import com.sino.core.encryption.config.ApiCryptoConfig;
//import org.springframework.context.annotation.Import;
//
//import java.lang.annotation.*;
//
///**
// * ApiCrypto 自动装配注解
// *
// * @author wzl
// * @since 1.0.0.RELEASE
// */
//@Target(ElementType.TYPE)
//@Retention(RetentionPolicy.RUNTIME)
//@Documented
//@Import({EncryptResponseBodyAdvice.class, DecryptRequestBodyAdvice.class, ApiCryptoConfig.class})
//public @interface EnableApiCrypto {
//}
