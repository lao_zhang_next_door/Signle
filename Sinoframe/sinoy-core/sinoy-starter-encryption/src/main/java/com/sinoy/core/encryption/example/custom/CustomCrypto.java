package com.sinoy.core.encryption.example.custom;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义加密解密注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomCrypto {
    /**
     * 是否请求体解密
     */
    boolean isDecryption();

    /**
     * 是否响应体加密
     */
    boolean isEncryption();

    /**
     * 加密的盐
     */
    String salt();
}
