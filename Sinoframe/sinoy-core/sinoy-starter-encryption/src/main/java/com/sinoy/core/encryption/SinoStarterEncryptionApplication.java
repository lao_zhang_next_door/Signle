//package com.sino.core.encryption;
//
//import com.sino.core.encryption.annotation.EnableApiCrypto;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
///**
// * @author T470
// */
//// 开启 ApiCrypto 注解
//@EnableApiCrypto
//@SpringBootApplication
//public class SinoStarterEncryptionApplication {
//
//    public static void main(String[] args) {
//        SpringApplication.run(SinoStarterEncryptionApplication.class, args);
//    }
//
//}
