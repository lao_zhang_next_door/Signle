//package com.sino.core.encryption.example.controller;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sino.core.encryption.annotation.NotDecrypt;
//import com.sino.core.encryption.annotation.SymmetricCrypto;
//import com.sino.core.encryption.constants.SymmetricType;
//import com.sino.core.encryption.example.bean.TestBean;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.util.Collections;
//
///**
// * 对称性加密、解密
// * @author T470
// */
//@RestController
//public class SymmetricController {
//
//    @Resource
//    ObjectMapper objectMapper;
//
//    @NotDecrypt
////    @NotEncrypt
//    @SymmetricCrypto(type = SymmetricType.AES_CFB_PKCS7_PADDING)
//    @PostMapping("/symmetric")
//    public String symmetric(@RequestBody TestBean req) throws JsonProcessingException {
//
//        System.out.println(req.toString());
//
//        TestBean testBean = new TestBean();
//        testBean.setAnInt(0);
//        testBean.setInteger(1);
//        testBean.setString("test string");
//        testBean.setStringList(Collections.singletonList("list"));
//        testBean.setObjectMap(Collections.singletonMap("test", "map"));
//
//        return objectMapper.writeValueAsString(testBean);
//    }
//}
