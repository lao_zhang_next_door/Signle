package com.sinoy.core.encryption.annotation;

import com.sinoy.core.encryption.constants.CryptoType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * 加密、解密 标记注解
 *
 * @author wzl
 * @since 1.0.0.RELEASE
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface ApiCrypto {

    /**
     * 加密 类型（枚举）
     *
     * @return cn.hermesdi.crypto.constants.CryptoType 加密解密 类型枚举（用于描述）
     * @author wzl
     **/
    CryptoType encryptType();


    /**
     * 解密 类型（枚举）
     *
     * @return cn.hermesdi.crypto.constants.CryptoType 加密解密 类型枚举（用于描述）
     * @author wzl
     **/
    CryptoType decryptType();
}
