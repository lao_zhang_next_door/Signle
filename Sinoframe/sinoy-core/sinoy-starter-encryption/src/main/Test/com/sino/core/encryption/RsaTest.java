package com.sinoy.core.encryption;


import com.alibaba.fastjson2.JSON;
import com.sinoy.core.encryption.constants.EncodingType;
import com.sinoy.core.encryption.constants.RSASignatureType;
import com.sinoy.core.encryption.example.bean.TestBean;
import com.sinoy.core.encryption.util.CryptoUtil;
import com.sinoy.core.encryption.util.EncodingUtil;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.util.Base64Utils;
import org.springframework.util.DigestUtils;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.time.Instant;
import java.util.Collections;

/**
 * RSA 测试
 */
public class RsaTest {

    String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALhigdVu4hQ50aYkqKo+hMckqVe8hrUa8KrLGiuAx10MHZzERZmHkrjwbO6u+J1r+DbZcy9PzdwdE4k9+4xhwjw8lsT4OTizxF425suslJ480DQrtsYRgNPb52rkDk/5R8tnl+JYMZx9o4784PG2S9qW8Fy0CPEKlVFr4fvAWPe9AgMBAAECgYARYmBBYlH4fDcTBhPM8VYNfJxmxESjIKYeREX0YhDp6fGEzVCOmCSyQSDUJDUWio59hWUByfFr1mX4I5rq8nCgmjG3n+qGSdinkO1oZGLY1/rv1I7xqAqqEl82hkDIswRGgsmbZ74gfYk+QDSnmcA0qjE/CPUWoPljURm9tR5mlQJBAOrzU1RkeKPt6Ozi81oSA/esx2v1kttehLyJ5xBb1Esjrvmb9tr61WkfYrAmoAlmjFbQIQJSQv3hwHR2XHFn8ssCQQDI53BpiC2MNwQTk4Nl48oWlFTIF3MjubvAj2bY/M/K77iJp6RWlBx7kLxArOdOW+eHBHcY6YhNvzI8W83CCAaXAkEAmU46gmeonHahtOiFl7EHq7WuzlAICmILRKbLAc6ZoInhI3hURCDF3fkfwQcDB/9E9WSLFWOt2NFUbenJeio0rwJBALw6as3VMqd+HCmjOabKxtpk2xIlNlEwgUImmPuP0beW5dTC5mvflNgIgPgvhv+Zh9CuVE9Y7cW57v0yHhM+pb0CQQDROjCCU09nNvmhxUCJdghBhrwzeADauhz2cFV3KoffPxdPMymOVuDk/Or1e165nthfwGZxINqOHR9thYqgtjOw";
    String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4YoHVbuIUOdGmJKiqPoTHJKlXvIa1GvCqyxorgMddDB2cxEWZh5K48Gzurvida/g22XMvT83cHROJPfuMYcI8PJbE+Dk4s8ReNubLrJSePNA0K7bGEYDT2+dq5A5P+UfLZ5fiWDGcfaOO/ODxtkvalvBctAjxCpVRa+H7wFj3vQIDAQAB";

    /**
     * 生成密钥
     */
    @Test
    public void generator() throws Exception {

        KeyPair keyPair = CryptoUtil.generatorRsaKeyPair(1024);

        String privateKey = new String(Base64Utils.encode(keyPair.getPrivate().getEncoded()));

        String publicKey = new String(Base64Utils.encode(keyPair.getPublic().getEncoded()));

        System.out.println("---------- privateKey ----------");
        System.out.println(privateKey);


        System.out.println("---------- publicKey ----------");
        System.out.println(publicKey);
    }

    /**
     * 加密、解密
     */
    @Test
    public void rsa() throws Exception {
        TestBean testBean = new TestBean();
        testBean.setAnInt(0);
        testBean.setInteger(1);
        testBean.setString("test string");
        testBean.setStringList(Collections.singletonList("list"));
        testBean.setObjectMap(Collections.singletonMap("test", "map"));
        String data = JSON.toJSONString(testBean);

        System.out.println("---------- privateKey ----------");
        System.out.println(privateKey);
        System.out.println("---------- publicKey ----------");
        System.out.println(publicKey);

        // 加密
        String encryptData = CryptoUtil.asymmetry(
                "RSA",
                "RSA/ECB/PKCS1Padding",
                Cipher.ENCRYPT_MODE,
                publicKey,
                EncodingType.BASE64,
                data,
                EncodingType.BASE64,
                StandardCharsets.UTF_8
        );

        // 签名
        byte[] bytes = CryptoUtil.resSignature(
                RSASignatureType.MD5withRSA,
                EncodingUtil.decode(EncodingType.BASE64, encryptData, StandardCharsets.UTF_8),
                EncodingUtil.decode(EncodingType.BASE64, privateKey, StandardCharsets.UTF_8)
        );

        String signature = Base64Utils.encodeToString(bytes);
        System.out.println("signature:" + signature);
        // 验证签名
        boolean verify = CryptoUtil.resSignatureVerify(
                RSASignatureType.MD5withRSA,
                EncodingUtil.decode(EncodingType.BASE64, encryptData, StandardCharsets.UTF_8),
                EncodingUtil.decode(EncodingType.BASE64, signature, StandardCharsets.UTF_8),
                EncodingUtil.decode(EncodingType.BASE64, publicKey, StandardCharsets.UTF_8)
        );


        // 解密
        String decryptData = CryptoUtil.asymmetry(
                "RSA",
                "RSA/ECB/PKCS1Padding",
                Cipher.DECRYPT_MODE,
                privateKey,
                EncodingType.BASE64,
                encryptData,
                EncodingType.BASE64,
                StandardCharsets.UTF_8
        );

        System.out.println("\n验签结果：" + verify);
        System.out.println("\n加密前数据：" + data);
        System.out.println("\n加密后数据：" + encryptData);
        System.out.println("\n解密后数据：" + decryptData);
        System.out.println("\n对比结果：" + data.equals(decryptData));

    }

    @Test
    public void initSignData() throws Exception {
        TestBean testBean = new TestBean();
        testBean.setAnInt(1);
        testBean.setInteger(2);
        testBean.setString("3");
        String data = JSON.toJSONString(testBean);
        System.out.println(data);
        // 加密
        Instant timestamp = Instant.now();
        System.out.println("timestamp:" + timestamp.toEpochMilli() / 1000);
        String str = "data=" + data +
                "&timestamp=" + timestamp.toEpochMilli() / 1000 +
                "&nonce=wzlwzlwzl123" +
                "&key=13245678";
        // 签名
        System.out.println("str:" + str);
        String signature2 = DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
        System.out.println("signature2:" + signature2);

        // 验证签名
        System.out.println("\n加密前数据：" + data);
    }
}
