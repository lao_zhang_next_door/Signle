//package com.sinoy.core.service.login;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.sinoy.core.convert.LoginLogConvert;
//import com.sinoy.core.dal.dataobject.LoginLogDO;
//import com.sinoy.core.dal.mysql.login.LoginLogMapper;
//import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
//import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
//import com.sinoy.core.pojo.login.LoginLogCreateReqDTO;
//import com.sinoy.core.pojo.login.LoginLogPageReqVO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import org.springframework.validation.annotation.Validated;
//
//import javax.annotation.Resource;
//
///**
// * 登录日志 实现
// */
//@Service
//@Validated
//@Slf4j
//public class LoginLogServiceImpl extends BaseServiceImpl<LoginLogMapper,LoginLogDO> implements LoginLogService{
//
//    @Resource
//    private LoginLogMapper loginLogMapper;
//
//    @Override
//    public Page<LoginLogDO> getLoginLogPage(LoginLogPageReqVO reqVO) {
//        Page<LoginLogDO> page = new Page(reqVO.getCurrentPage(),reqVO.getPageSize());
//        QueryWrapper queryWrapper = new QueryWrapper();
//        BatisPlusUtil.setOrders(queryWrapper,null,null);
//        BatisPlusUtil.checkParamsIncluded(queryWrapper,reqVO,reqVO.getParams());
//        return loginLogMapper.selectPage(page,queryWrapper);
//    }
//
//    @Override
//    public void createLoginLog(LoginLogCreateReqDTO reqDTO) {
//        LoginLogDO loginLogDO = LoginLogConvert.INSTANCE.convert(reqDTO);
//        loginLogMapper.insert(loginLogDO);
//    }
//}
