package com.sinoy.core.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.dal.dataobject.LoginLogDO;
import com.sinoy.core.pojo.login.LoginLogCreateReqDTO;
import com.sinoy.core.pojo.login.LoginLogRespVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoginLogConvert {

    LoginLogConvert INSTANCE = Mappers.getMapper(LoginLogConvert.class);

    Page<LoginLogRespVO> convertPage(Page<LoginLogDO> page);

    LoginLogDO convert(LoginLogCreateReqDTO bean);

}
