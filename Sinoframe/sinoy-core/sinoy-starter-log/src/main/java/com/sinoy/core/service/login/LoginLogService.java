package com.sinoy.core.service.login;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.dal.dataobject.LoginLogDO;
import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.core.pojo.login.LoginLogCreateReqDTO;
import com.sinoy.core.pojo.login.LoginLogPageReqVO;

import javax.validation.Valid;

public interface LoginLogService extends BaseService<LoginLogDO> {

    /**
     * 获得登录日志分页
     *
     * @param reqVO 分页条件
     * @return 登录日志分页
     */
    Page<LoginLogDO> getLoginLogPage(LoginLogPageReqVO reqVO);


    /**
     * 创建登录日志
     *
     * @param reqDTO 日志信息
     */
    void createLoginLog(@Valid LoginLogCreateReqDTO reqDTO);

}
