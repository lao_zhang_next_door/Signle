package com.sinoy.core.log.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 日志配置
 * @author hero
 * @date 2020-9-5
 */
@ConfigurationProperties(LogProperties.PREFIX)
public class LogProperties {
    /**
     * 前缀
     */
    public static final String PREFIX = "xnw.logs";

    /**
     * 是否启用
     */
    private Boolean enable = false;

    /**
     * 记录日志类型
     */
    private LogType logType = LogType.DB;

    public static String getPREFIX() {
        return PREFIX;
    }

    public Boolean getEnable() {
        return enable;
    }

    public LogProperties setEnable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public LogType getLogType() {
        return logType;
    }

    public LogProperties setLogType(LogType logType) {
        this.logType = logType;
        return this;
    }
}
