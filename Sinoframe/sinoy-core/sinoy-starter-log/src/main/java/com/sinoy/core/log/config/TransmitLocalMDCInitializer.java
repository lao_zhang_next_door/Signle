package com.sinoy.core.log.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.slf4j.TransmitLocalMDCAdapter;

///**
// * 初始化TransmitLocalMDCAdapter，并替换MDC中的adapter对象
// * @author pangu
// */
//public class TransmitLocalMDCInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
//
//    @Override
//    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
//        //加载TtlMDCAdapter实例
//        TransmitLocalMDCAdapter.getInstance();
//    }
//}
