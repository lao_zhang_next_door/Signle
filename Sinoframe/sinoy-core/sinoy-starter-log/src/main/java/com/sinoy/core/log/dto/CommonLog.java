package com.sinoy.core.log.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 日志对象
 *
 * @author xuzhanfu
 */
public class CommonLog {

    private Long rowId;

    private String rowGuid;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 日志类型
     */
    private String type;
    /**
     * 跟踪ID
     */
    private String traceId;
    /**
     * 日志标题
     */
    private String title;
    /**
     * 操作内容
     */
    private String operation;
    /**
     * 执行方法
     */
    private String method;

    /**
     * 请求路径
     */
    private String url;
    /**
     * 参数
     */
    private String params;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 耗时
     */
    private Long executeTime;
    /**
     * 地区
     */
    private String location;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 异常信息
     */
    private String exception;

    public Long getRowId() {
        return rowId;
    }

    public CommonLog setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public String getRowGuid() {
        return rowGuid;
    }

    public CommonLog setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public CommonLog setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getType() {
        return type;
    }

    public CommonLog setType(String type) {
        this.type = type;
        return this;
    }

    public String getTraceId() {
        return traceId;
    }

    public CommonLog setTraceId(String traceId) {
        this.traceId = traceId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CommonLog setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getOperation() {
        return operation;
    }

    public CommonLog setOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public CommonLog setMethod(String method) {
        this.method = method;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public CommonLog setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getParams() {
        return params;
    }

    public CommonLog setParams(String params) {
        this.params = params;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public CommonLog setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public Long getExecuteTime() {
        return executeTime;
    }

    public CommonLog setExecuteTime(Long executeTime) {
        this.executeTime = executeTime;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public CommonLog setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getCreateBy() {
        return createBy;
    }

    public CommonLog setCreateBy(String createBy) {
        this.createBy = createBy;
        return this;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public CommonLog setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public CommonLog setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public String getException() {
        return exception;
    }

    public CommonLog setException(String exception) {
        this.exception = exception;
        return this;
    }
}
