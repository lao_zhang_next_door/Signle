//package com.sino.core.log.feign;
//
//import com.sino.core.servicecommon.utils.dataformat.R;
//import com.sino.core.servicefeign.constant.FeignConstant;
//import com.sino.core.servicelog.dto.CommonLog;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.PostMapping;
//
//
///**
// * 普通日志生产消息调用
// * @author pangu
// */
//
//@FeignClient(value = FeignConstant.XNW_CLOUD_LOG_PRODUCER)
//public interface ICommonLogProvider {
//
//    /**
//     * 向消息中心发送消息
//     * @param commonLog 普通日志
//     * @return 状态
//     */
//    @PostMapping("/provider/common-log/send")
//    R sendCommonLog(CommonLog commonLog);
//}
