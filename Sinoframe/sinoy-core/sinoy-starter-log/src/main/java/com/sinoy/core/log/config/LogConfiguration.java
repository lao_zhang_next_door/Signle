package com.sinoy.core.log.config;

import com.sinoy.core.log.aspect.LogAspect;
import com.sinoy.core.log.props.LogProperties;
import com.sinoy.core.log.resources.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * 日志配置中心
 * @author pangu
 */
@Configuration
@ConditionalOnWebApplication
@EnableConfigurationProperties(value = LogProperties.class)
@ConditionalOnProperty(name = "xnw.logs.enable",havingValue = "true",matchIfMissing = false)
public class LogConfiguration {

    @Autowired
    private ISysLogService iSysLogService;

    private final LogProperties logProperties;

    public LogConfiguration(LogProperties logProperties) {
        this.logProperties = logProperties;
    }

//    @Bean
//    public LogListener sysLogListener() {
//        return new LogListener(iSysLogService, logProperties);
//    }

    @Bean
    public LogAspect logAspect(ApplicationContext applicationContext){
        return new LogAspect(applicationContext);
    }
}
