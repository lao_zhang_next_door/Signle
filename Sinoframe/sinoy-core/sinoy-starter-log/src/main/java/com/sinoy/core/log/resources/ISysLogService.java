package com.sinoy.core.log.resources;

import com.sinoy.core.common.base.Base;
import com.sinoy.core.common.utils.db.JdbcClient;
import com.sinoy.core.log.dto.CommonLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.SQLException;

@Service
public class ISysLogService extends Base {

    @Autowired
    private DataSource dataSource;

    /**
     * 存储日志  数据表frame_log
     */
    public void saveLog(CommonLog commonLog){
        try {
            JdbcClient.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        JdbcClient<CommonLog> jdbcClient = new JdbcClient();
        jdbcClient.add("frame_log",commonLog.getClass(),commonLog);
        jdbcClient.CloseAll();
    }

}
