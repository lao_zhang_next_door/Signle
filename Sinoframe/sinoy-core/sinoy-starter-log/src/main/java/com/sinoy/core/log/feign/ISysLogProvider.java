//package com.sino.core.log.feign;
//
//import com.sino.core.servicecommon.utils.dataformat.R;
//import com.sino.core.servicefeign.constant.FeignConstant;
//import com.sino.core.servicelog.dto.CommonLog;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//
//
///**
// * feign调用service-frame存储日志
// * @author pangu
// * @date 2020-7-1
// */
//@FeignClient(value = FeignConstant.XNW_SERVICE_FRAME)
//public interface ISysLogProvider {
//
//    /**
//     * 日志设置
//     * @param commonLog　CommonLog对象
//     * @return Result
//     */
//    @PostMapping("FG/FrameLog/set")
//    R set(@RequestBody CommonLog commonLog);
//
//}
