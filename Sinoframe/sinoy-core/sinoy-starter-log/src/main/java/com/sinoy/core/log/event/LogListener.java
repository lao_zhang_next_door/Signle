//package com.sino.core.log.event;
//
//import com.sino.core.common.base.Base;
//import com.sino.core.log.dto.CommonLog;
//import com.sino.core.log.props.LogProperties;
//import com.sino.core.log.props.LogType;
//import com.sino.core.log.resources.ISysLogService;
//import org.springframework.context.event.EventListener;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//
///**
// * 注解形式，异步监听事件
// * @author pangu 7333791@qq.com
// * @since 2020-7-15
// */
//@Component
//public class LogListener extends Base {
//
//    private ISysLogService iSysLogService;
//    private LogProperties logProperties;
//
//    public LogListener(ISysLogService iSysLogService, LogProperties logProperties) {
//        this.iSysLogService = iSysLogService;
//        this.logProperties = logProperties;
//    }
//
////    @Async
//    @Order
//    @EventListener(LogEvent.class)
//    public void saveSysLog(LogEvent event) {
//        CommonLog commonLog = (CommonLog) event.getSource();
//        // 发送日志到kafka
//        logger.info("发送日志:{}", commonLog);
//        if (logProperties.getLogType().equals(LogType.DB)) {
//            //存储日志
//            iSysLogService.saveLog(commonLog);
//        }
//    }
//
//}
