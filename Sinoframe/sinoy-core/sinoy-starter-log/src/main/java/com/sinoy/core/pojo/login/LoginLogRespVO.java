package com.sinoy.core.pojo.login;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LoginLogRespVO extends LoginLogBaseVO {

    private Long rowId;

    /**
     * 用户编号
     */
    private Long userId;

    @NotNull(message = "用户类型不能为空")
    private Integer userType;
}
