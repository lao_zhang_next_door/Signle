package com.sinoy.core.pojo.login;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginLogPageReqVO extends PageParam {

    /**
     * 用户ip
     */
    private String userIp;

    /**
     * 用户账户
     */
    private String username;

    /**
     * 操作状态
     */
    private Boolean status;

}
