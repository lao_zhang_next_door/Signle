package com.sinoy.core.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * cors 跨域配置
 */
@Configuration
public class CorsConfig {

    /**
     *  跨域访问配置
     * @return
     */
    private CorsConfiguration buildCrosConfig(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //设置属性
        //允许跨域的地址，*表示所有
//        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedOriginPattern("*");
        //配置跨域的请求头
        corsConfiguration.addAllowedHeader("*");
        //配置跨域的请求方法
        corsConfiguration.addAllowedMethod("*");
        //配置-跨域请求时是否使用同一个session
        corsConfiguration.setAllowCredentials(true);
        return corsConfiguration;
    }

/*    @Bean
    public CorsFilter corsFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",buildCrosConfig());
        return new CorsFilter(source);
    }*/
}
