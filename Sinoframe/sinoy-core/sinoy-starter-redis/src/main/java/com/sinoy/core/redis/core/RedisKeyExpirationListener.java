package com.sinoy.core.redis.core;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

@AllArgsConstructor
public class RedisKeyExpirationListener implements MessageListener {

    private final ApplicationEventPublisher eventPublisher;

    @Override
    public void onMessage(Message message, byte[] pattern)
    {
        // 处理键过期事件的逻辑
        String expiredKey = message.toString();
        RedisKeyExpiredEvent event = new RedisKeyExpiredEvent(expiredKey);
        eventPublisher.publishEvent(event);
    }
}
