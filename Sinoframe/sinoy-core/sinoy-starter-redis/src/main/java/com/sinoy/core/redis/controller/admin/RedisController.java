package com.sinoy.core.redis.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.redis.constants.CacheConstants;
import com.sinoy.core.redis.core.RedisService;
import com.sinoy.core.redis.vo.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.*;

@RestController
@RequestMapping("/monitor/redis")
public class RedisController {

    @Value(value = "${spring.jmx.default-domain}")
    public String projectName;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisService redisService;

    private final List<RedisCache> caches = new ArrayList<RedisCache>();

    @PostConstruct
    public void initCachesList(){
        caches.add(new RedisCache(projectName+":"+CacheConstants.CODE_VALUE, "代码项字典", RedisCache.KeyTypeEnum.HASH));
    }

    @GetMapping()
    public R getInfo() throws Exception
    {
        Properties info = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info());
        Properties commandStats = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info("commandstats"));
        Object dbSize = redisTemplate.execute((RedisCallback<Object>) connection -> connection.dbSize());

        Map<String, Object> result = new HashMap<>(3);
        result.put("info", info);
        result.put("dbSize", dbSize);

        List<Map<String, String>> pieList = new ArrayList<>();
        commandStats.stringPropertyNames().forEach(key -> {
            Map<String, String> data = new HashMap<>(2);
            String property = commandStats.getProperty(key);
            data.put("name", StrUtil.subAfter(key, "cmdstat_",true));
            data.put("value", StrUtil.subBetween(property, "calls=", ",usec"));
            pieList.add(data);
        });
        result.put("commandStats", pieList);
        return R.ok().put("data",result);
    }

    @GetMapping("/getNames")
    public R cache()
    {
        return R.ok().put("data",caches);
    }

    @GetMapping("/getKeys/{keyType}/{cacheName}")
    public R getCacheKeys(@PathVariable String cacheName,@PathVariable String keyType)
    {
        Set<Object> cacheKeys = null;
        if(keyType.equals("Hash")){
            Map<Object,Object> map = redisService.hmget(cacheName);
            cacheKeys = map.keySet();
        }else{
            cacheKeys = Collections.singleton((redisTemplate.keys(cacheName + "*")));
        }
        return  R.ok().put("data",cacheKeys);
    }

    @GetMapping("/getValue/{keyType}/{cacheName}/{cacheKey}")
    public R getCacheValue(@PathVariable String cacheName, @PathVariable String cacheKey,@PathVariable String keyType)
    {
        RedisCache RedisCache = null;
        if(keyType.equals("Hash")){
            Object object = redisService.hget(cacheName,cacheKey);
            RedisCache = new RedisCache(cacheName, cacheKey, JSON.toJSONString(object));
        }else{
            String cacheValue = redisTemplate.opsForValue().get(cacheKey);
            RedisCache = new RedisCache(cacheName, cacheKey, cacheValue);
        }

        return R.ok().put("data",RedisCache);
    }

    @DeleteMapping("/clearCacheName/{cacheName}")
    public R clearCacheName(@PathVariable String cacheName)
    {
        Collection<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        redisTemplate.delete(cacheKeys);
        return R.ok();
    }

    @DeleteMapping("/clearCacheKey/{keyType}/{cacheName}/{cacheKey}")
    public R clearCacheKey(@PathVariable String cacheName,@PathVariable String cacheKey,@PathVariable String keyType)
    {
        if(keyType.equals("Hash")){
            redisService.hdel(cacheName,cacheKey);
        }else{
            redisTemplate.delete(cacheKey);
        }

        return R.ok();
    }

    @DeleteMapping("/clearCacheAll")
    public R clearCacheAll()
    {
        Collection<String> cacheKeys = redisTemplate.keys("*");
        redisTemplate.delete(cacheKeys);
        return R.ok();
    }

}
