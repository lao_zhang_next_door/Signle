package com.sinoy.core.redis.core;

public class RedisKeyExpiredEvent {

    private String expiredKey;

    public RedisKeyExpiredEvent(String expiredKey) {
        this.expiredKey = expiredKey;
    }

    public String getExpiredKey() {
        return expiredKey;
    }

}
