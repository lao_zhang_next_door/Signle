package com.sinoy.core.oauth2.pojo.user;

public class OAuth2UserInfoRespVO {

    //用户编号
    private Long id;

    //用户昵称
    private String username;

    //用户昵称
    private String nickname;

    //用户邮箱
    private String email;

    //手机号码
    private String mobile;

    //用户性别
    private Integer sex;

    //用户头像
    private String avatar;

    /**
     * 所在部门
     */
    private Dept dept;


    public static class Dept {

        //部门编号
        private Long id;

        //部门名称
        private String name;

        public Long getId() {
            return id;
        }

        public Dept setId(Long id) {
            this.id = id;
            return this;
        }

        public String getName() {
            return name;
        }

        public Dept setName(String name) {
            this.name = name;
            return this;
        }
    }

    public Long getId() {
        return id;
    }

    public OAuth2UserInfoRespVO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public OAuth2UserInfoRespVO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public OAuth2UserInfoRespVO setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public OAuth2UserInfoRespVO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public OAuth2UserInfoRespVO setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public OAuth2UserInfoRespVO setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public OAuth2UserInfoRespVO setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public Dept getDept() {
        return dept;
    }

    public OAuth2UserInfoRespVO setDept(Dept dept) {
        this.dept = dept;
        return this;
    }
}
