package com.sinoy.core.oauth2.dao;


import com.alibaba.fastjson2.JSON;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * {@link OAuth2AccessTokenDO} 的 RedisDAO
 *
 */
@Repository
public class OAuth2AccessTokenRedisDAO {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public OAuth2AccessTokenDO get(String accessToken) {
        String redisKey = formatKey(accessToken);
        return JSON.parseObject(stringRedisTemplate.opsForValue().get(redisKey), OAuth2AccessTokenDO.class);
    }

    public void set(OAuth2AccessTokenDO accessTokenDO) {
        String redisKey = formatKey(accessTokenDO.getAccessToken());
//        // 清理多余字段，避免缓存
//        accessTokenDO.setUpdateTime(null).setCreateTime(null).setCreator(null).setDeleted(null);
        try {
            stringRedisTemplate.opsForValue().set(redisKey, JSON.toJSONString(accessTokenDO),
                    accessTokenDO.getExpiresTime().getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(String accessToken) {
        String redisKey = formatKey(accessToken);
        stringRedisTemplate.delete(redisKey);
    }

    public void deleteList(Collection<String> accessTokens) {
        List<String> redisKeys = CollectionUtils.convertList(accessTokens, OAuth2AccessTokenRedisDAO::formatKey);
        stringRedisTemplate.delete(redisKeys);
    }

    private static String formatKey(String accessToken) {
        return String.format("oauth2_access_token:%s", accessToken);
    }

}
