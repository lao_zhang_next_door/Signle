package com.sinoy.core.oauth2.service.impl;

import cn.hutool.core.util.IdUtil;
import com.sinoy.core.common.constant.ErrorCodeConstants;
import com.sinoy.core.common.utils.dateformat.DateUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.oauth2.dao.OAuth2CodeMapper;
import com.sinoy.core.oauth2.pojo.OAuth2CodeDO;
import com.sinoy.core.oauth2.service.OAuth2CodeService;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

/**
 * OAuth2.0 授权码 Service 实现类
 */
@Service
@Validated
public class OAuth2CodeServiceImpl implements OAuth2CodeService {

    /**
     * 授权码的过期时间，默认 5 分钟
     */
    private static final Integer TIMEOUT = 5 * 60;

    @Resource
    private OAuth2CodeMapper oauth2CodeMapper;

    @Override
    public OAuth2CodeDO createAuthorizationCode(Long userId, Integer userType, String clientId,
                                                List<String> scopes, String redirectUri, String state) {
        OAuth2CodeDO codeDO = new OAuth2CodeDO(true)
                .setCode(generateCode())
                .setUserId(userId).setUserType(userType)
                .setClientId(clientId).setScopes(scopes)
                .setExpiresTime(DateUtil.addDate(Calendar.SECOND, TIMEOUT))
                .setRedirectUri(redirectUri).setState(state);
        oauth2CodeMapper.insert(codeDO);
        return codeDO;
    }

    @Override
    public OAuth2CodeDO consumeAuthorizationCode(String code) {
        OAuth2CodeDO codeDO = oauth2CodeMapper.selectByCode(code);
        if (codeDO == null) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CODE_NOT_EXISTS);
        }
        if (DateUtil.isExpired(codeDO.getExpiresTime())) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CODE_EXPIRE);
        }
        oauth2CodeMapper.deleteById(codeDO.getRowId());
        return codeDO;
    }

    private static String generateCode() {
        return IdUtil.fastSimpleUUID();
    }

}
