package com.sinoy.core.oauth2.pojo.client;

import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.utils.dataformat.JsonUtils;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * OAuth2 客户端 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里
 */
public class OAuth2ClientBaseVO {

    @NotNull(message = "客户端编号不能为空")
    private String clientId;

    /**
     * 客户端Key
     */
    private String clientKey;

    @NotNull(message = "客户端密钥不能为空")
    private String secret;

    @NotNull(message = "应用名不能为空")
    private String name;

    //    @NotNull(message = "应用图标不能为空")
    @URL(message = "应用图标的地址不正确")
    private String logo;

    //应用描述
    private String description;

    @NotNull(message = "状态不能为空")
    private Integer status;

    @NotNull(message = "访问令牌的有效期不能为空")
    private Integer accessTokenValiditySeconds;

    @NotNull(message = "刷新令牌的有效期不能为空")
    private Integer refreshTokenValiditySeconds;

    //    @NotNull(message = "可重定向的 URI 地址不能为空")
//    @NotEmpty(message = "重定向的 URI 不能为空")
//    @URL(message = "重定向的 URI 格式不正确")
    private List<String> redirectUris;

    @NotNull(message = "授权类型不能为空")
    private List<String> authorizedGrantTypes;

    //授权范围
    private List<String> scopes;

    //自动通过的授权范围
    private List<String> autoApproveScopes;

    //权限  system:user:query
    private List<String> authorities;

    //资源
    private List<String> resourceIds;

    //附加信息
    private String additionalInformation;

    @AssertTrue(message = "附加信息必须是 JSON 格式")
    public boolean isAdditionalInformationJson() {
        return StrUtil.isEmpty(additionalInformation) || JsonUtils.isJson(additionalInformation);
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2ClientBaseVO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public OAuth2ClientBaseVO setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public String getName() {
        return name;
    }

    public OAuth2ClientBaseVO setName(String name) {
        this.name = name;
        return this;
    }

    public String getLogo() {
        return logo;
    }

    public OAuth2ClientBaseVO setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OAuth2ClientBaseVO setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public OAuth2ClientBaseVO setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    public OAuth2ClientBaseVO setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds;
        return this;
    }

    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    public OAuth2ClientBaseVO setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
        return this;
    }

    public List<String> getRedirectUris() {
        return redirectUris;
    }

    public OAuth2ClientBaseVO setRedirectUris(List<String> redirectUris) {
        this.redirectUris = redirectUris;
        return this;
    }

    public List<String> getAuthorizedGrantTypes() {
        return authorizedGrantTypes;
    }

    public OAuth2ClientBaseVO setAuthorizedGrantTypes(List<String> authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public OAuth2ClientBaseVO setScopes(List<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public List<String> getAutoApproveScopes() {
        return autoApproveScopes;
    }

    public OAuth2ClientBaseVO setAutoApproveScopes(List<String> autoApproveScopes) {
        this.autoApproveScopes = autoApproveScopes;
        return this;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public OAuth2ClientBaseVO setAuthorities(List<String> authorities) {
        this.authorities = authorities;
        return this;
    }

    public List<String> getResourceIds() {
        return resourceIds;
    }

    public OAuth2ClientBaseVO setResourceIds(List<String> resourceIds) {
        this.resourceIds = resourceIds;
        return this;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public OAuth2ClientBaseVO setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
        return this;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }
}
