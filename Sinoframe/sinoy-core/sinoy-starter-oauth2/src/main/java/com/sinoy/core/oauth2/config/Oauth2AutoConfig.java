package com.sinoy.core.oauth2.config;

import com.sinoy.core.common.utils.exception.GlobalExceptionHandler;
import com.sinoy.core.oauth2.filter.TokenAuthenticationFilter;
import com.sinoy.core.oauth2.handle.AccessDeniedHandlerImpl;
import com.sinoy.core.oauth2.handle.AuthenticationEntryPointImpl;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
public class Oauth2AutoConfig {

    /**
     * Spring Security 加密器
     *
     * @see <a href="http://stackabuse.com/password-encoding-with-spring-security/">Password Encoding with Spring Security</a>
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new MessageDigestPasswordEncoder("SHA-256");
    }

    /**
     * 认证失败处理类 Bean
     */
    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new AuthenticationEntryPointImpl();
    }

    /**
     * 权限不够处理器 Bean
     */
    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new AccessDeniedHandlerImpl();
    }

    /**
     * Token 认证过滤器 Bean
     */
    @Bean
    public TokenAuthenticationFilter authenticationTokenFilter(GlobalExceptionHandler globalExceptionHandler,
                                                               OAuth2TokenService oAuth2TokenService,
                                                               CommonPropAndMethods commonPropAndMethods,
                                                               FrameUserService frameUserService) {
        return new TokenAuthenticationFilter(globalExceptionHandler, oAuth2TokenService, commonPropAndMethods,frameUserService);
    }

}
