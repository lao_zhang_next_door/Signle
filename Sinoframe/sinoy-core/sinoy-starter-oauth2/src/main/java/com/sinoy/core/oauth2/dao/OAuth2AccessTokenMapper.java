package com.sinoy.core.oauth2.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;

import java.util.List;

public interface OAuth2AccessTokenMapper extends ExtendMapper<OAuth2AccessTokenDO> {

    default OAuth2AccessTokenDO selectByAccessToken(String accessToken) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("access_token",accessToken);
        return selectOne(queryWrapper);
    }

    default List<OAuth2AccessTokenDO> selectListByRefreshToken(String refreshToken) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("refresh_token",refreshToken);
        return selectList(queryWrapper);
    }

//    default Page<OAuth2AccessTokenDO> selectPage(Page<OAuth2AccessTokenDO> reqVO) {
//        return selectPage(reqVO, new LambdaQueryWrapper<OAuth2AccessTokenDO>()
//                .eqIfPresent(OAuth2AccessTokenDO::getUserId, reqVO. .getUserId())
//                .eqIfPresent(OAuth2AccessTokenDO::getUserType, reqVO.getUserType())
//                .likeIfPresent(OAuth2AccessTokenDO::getClientId, reqVO.getClientId())
//                .gt(OAuth2AccessTokenDO::getExpiresTime, new Date())
//                .orderByDesc(OAuth2AccessTokenDO::getId));
//    }

}
