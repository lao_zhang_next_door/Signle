package com.sinoy.core.oauth2.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.constant.ErrorCodeConstants;
import com.sinoy.core.common.enums.*;
import com.sinoy.core.common.utils.dateformat.DateUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.oauth2.maps.AuthConvert;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.login.AuthLoginReqVO;
import com.sinoy.core.oauth2.pojo.login.AuthLoginRespVO;
import com.sinoy.core.oauth2.service.AuthService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.redis.core.RedisKeyConstants;
import com.sinoy.core.redis.core.RedisKeyExpiredEvent;
import com.sinoy.core.redis.core.RedisService;
import com.sinoy.core.security.props.CasProperties;
import com.sinoy.platform.system.component.entity.FrameConfig;
import com.sinoy.platform.system.component.entity.FrameSmsCode;
import com.sinoy.platform.system.component.entity.FrameSmsRecord;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.entity.vo.AuthSmsLoginReqVO;
import com.sinoy.platform.system.component.service.FrameConfigService;
import com.sinoy.platform.system.component.service.FrameSmsCodeService;
import com.sinoy.platform.system.component.service.FrameSmsRecordService;
import com.sinoy.platform.system.component.service.FrameUserService;
import com.sinoy.support.captcha.model.common.RepCodeEnum;
import com.sinoy.support.captcha.model.common.ResponseModel;
import com.sinoy.support.captcha.model.vo.CaptchaVO;
import com.sinoy.support.captcha.service.CaptchaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Validator;
import javax.xml.xpath.XPathConstants;
import java.awt.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;


/**
 * Auth Service 实现类
 */
@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Resource
    private FrameUserService userService;
    @Resource
    private OAuth2TokenService oauth2TokenService;
    @Resource
    private Validator validator;
    @Resource
    private CasProperties casProperties;
    @Resource
    private FrameConfigService frameConfigService;

    @Resource
    private CaptchaService captchaService;

    @Resource
    private FrameSmsCodeService frameSmsCodeService;

    @Resource
    private HttpServletRequest request;
    @Resource
    private FrameSmsRecordService frameSmsRecordService;
    @Resource
    private RedisService redisService;

    @Override
    public AuthLoginRespVO login(AuthLoginReqVO reqVO) {
        String userName = URLDecoder.decode(reqVO.getUsername(), StandardCharsets.UTF_8);
        // 使用账号密码，进行登录
        UserInfo user = authenticate(userName, reqVO.getPassword());
        // 创建 Token 令牌，记录登录日志
        return createTokenAfterLoginSuccess(user, LoginLogTypeEnum.LOGIN_USERNAME);
    }

    @Override
    public AuthLoginRespVO refreshToken(String refreshToken) {
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.refreshAccessToken(refreshToken, "default");
        return AuthConvert.INSTANCE.convert(accessTokenDO);
    }

    private AuthLoginRespVO createTokenAfterLoginSuccess(UserInfo user, LoginLogTypeEnum logType) {
        // 插入登陆日志
//        createLoginLog(user.getRowId(), user.getUserName(), logType, LoginResultEnum.SUCCESS);
        // 创建访问令牌
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.createAccessToken(user, getUserType().getValue(),
                "default", null);
        // 构建返回结果
        return AuthConvert.INSTANCE.convert(accessTokenDO);
    }

    private AuthLoginRespVO createTokenAfterLoginSuccess(Long userId, String username, LoginLogTypeEnum logType) {
        // 插入登陆日志

        // 创建访问令牌
        OAuth2AccessTokenDO accessTokenDO = null;
        // 构建返回结果
        return AuthConvert.INSTANCE.convert(accessTokenDO);
    }

    @Override
    public void logout(String token, Integer logType) {
        OAuth2AccessTokenDO oAuth2AccessTokenDO = oauth2TokenService.getAccessToken(token);
        if (oAuth2AccessTokenDO != null) {
            Long userId = oAuth2AccessTokenDO.getUserId();
            QueryWrapper<OAuth2AccessTokenDO> queryWrapper = new QueryWrapper<>();
            queryWrapper.ge("expires_time", LocalDateTime.now());
            queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
            queryWrapper.eq("user_id", userId);
            List<OAuth2AccessTokenDO> oAuth2AccessTokenDOList = oauth2TokenService.list(queryWrapper);
            for (OAuth2AccessTokenDO removeAssessToken : oAuth2AccessTokenDOList) {
                // 删除访问令牌
                OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.removeAccessToken(removeAssessToken.getAccessToken());
                if (accessTokenDO == null) {

                }
            }
        }

        // 删除成功，则记录登出日志
        //createLogoutLog(accessTokenDO.getUserId(), accessTokenDO.getUserType(), logType);
    }

    private UserTypeEnum getUserType() {
        return UserTypeEnum.ADMIN;
    }

    @Override
    public UserInfo authenticate(String username, String password) {
        final LoginLogTypeEnum logTypeEnum = LoginLogTypeEnum.LOGIN_USERNAME;
        // 校验账号是否存在、
        UserInfo user = userService.loadUserByLoginId(username);
        if (user == null) {
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_BAD_CREDENTIALS);
        }

        String key = redisService.getRedisKey(String.format(RedisKeyConstants.SYSTEM_LOGINID_FIALACCOUNT, user.getFrameUser().getLoginId()));

        Boolean loginFailEnable = Boolean.parseBoolean(frameConfigService.getCacheByConfigName("sys.login-fail.enable"));
        // 校验是否禁用
        if (ObjectUtil.notEqual(user.getFrameUser().getStatus(), UserStatus.NORMAL.getCode() - '0')) {
            if (loginFailEnable) {
                Long times = redisService.getExpire(key);
                if (times < 0) {
                    times = 0L;
                }
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_USER_DISABLED, String.format("请等待%s秒后再登录", times));
            } else {
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_USER_DISABLED);
            }
        }

        // 校验登录失败次数是否超过限制 超过限制禁用该账户
        if (!userService.isPasswordMatch(password, user.getFrameUser().getPassword())) {
            if (loginFailEnable) {
                // 登陆失败次数 +1
                userLoginFail(user.getFrameUser().getLoginId());
                if (!checkUserLoginFailCount(user.getFrameUser().getLoginId())) {
                    FrameUser updateObj = new FrameUser();
                    updateObj.setRowId(user.getFrameUser().getRowId());
                    updateObj.setStatus(CommonStatusEnum.DISABLE.getStatus());
                    userService.updateById(updateObj);
                }
            }
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_BAD_CREDENTIALS);
        }

        //删除key
        if (redisService.hasKey(key)) {
            redisService.del(key);
        }

        return user;
    }

    @Override
    public AuthLoginRespVO loginByTicket(String ticket) {
        Optional.ofNullable(ticket).orElseThrow(() -> new BaseException("票据为空"));
        String res = getCasUserByTicket(ticket);
        Document docResult = XmlUtil.readXML(res);
        String loginId = (String) XmlUtil.getByXPath("//cas:authenticationSuccess/cas:user", docResult, XPathConstants.STRING);
        Optional.ofNullable(loginId).orElseThrow(() -> new BaseException("获取登录名失败"));

        UserInfo user = authenticateCasUser(loginId);
        // 创建 Token 令牌，记录登录日志
        return createTokenAfterLoginSuccess(user, LoginLogTypeEnum.LOGIN_USERNAME);
    }

    @Override
    public String getCasUserByTicket(String ticket) {
        Map<String, Object> paramMap = new HashMap() {
            {
                put("service", casProperties.getWebUrl());
                put("ticket", ticket);
            }
        };
        log.info("根据ticket 获取用户信息接口调用>>>>>> paramMap:" + JSON.toJSONString(paramMap));
        String res = HttpUtil.get(casProperties.getCasServerUrl() + "/serviceValidate", paramMap);
        log.info("根据ticket 获取用户信息接口返回  " + res);
        return res;
    }

    public UserInfo authenticateCasUser(String username) {
        final LoginLogTypeEnum logTypeEnum = LoginLogTypeEnum.LOGIN_USERNAME;
        // 校验账号是否存在、
        UserInfo user = userService.loadUserByLoginId(username);
        if (user == null) {
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_BAD_CREDENTIALS);
        }
        // 校验是否禁用
        if (ObjectUtil.notEqual(user.getFrameUser().getStatus(), UserStatus.NORMAL.getCode() - '0')) {
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_USER_DISABLED);
        }
        return user;
    }

    @Override
    public String checkUserCaptcha(CaptchaVO captchaVO) {
        String captchaEnable = frameConfigService.getCacheByConfigName("是否启用验证码");
        if (captchaEnable == null) {
            log.error("无法获取是否启用验证码参数信息");
            return null;
        }
        if (Boolean.parseBoolean(captchaEnable)) {
            //进行滑动验证检查
            ResponseModel responseModel = captchaService.verification(captchaVO);
            if (responseModel.getRepCode().equals(RepCodeEnum.SUCCESS.getCode())) {
                return null;
            } else {
                return responseModel.getRepMsg();
            }
        } else {
            return null;
        }
    }

    @Override
    public AuthLoginRespVO smsLogin(AuthSmsLoginReqVO reqVO) {
        final LoginLogTypeEnum logTypeEnum = LoginLogTypeEnum.LOGIN_SMS;

        List<FrameUser> allList = userService.lambdaQuery().eq(FrameUser::getMobile, reqVO.getMobile()).eq(FrameUser::getDelFlag, 0).list();
        String key = "";
        if (allList.size() > 0) {
            key = redisService.getRedisKey(String.format(RedisKeyConstants.SYSTEM_LOGINID_FIALACCOUNT, allList.get(0).getLoginId()));
            if (ObjectUtil.notEqual(allList.get(0).getStatus(), UserStatus.NORMAL.getCode() - '0')) {
                Long times = redisService.getExpire(key);
                if (times < 0) {
                    times = 0L;
                }
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_USER_DISABLED, String.format("请等待%s秒后再登录", times));
            }
        } else {
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
        }

        // 校验验证码
        List<FrameSmsCode> smsList = frameSmsCodeService.lambdaQuery()
                .eq(FrameSmsCode::getMobile, reqVO.getMobile())
                .orderByDesc(FrameSmsCode::getSendTime).list();
        if (smsList.size() == 0) {
            //没有验证码
            userLoginFail(allList.get(0).getLoginId());//记录登录失败次数
            checkIsOverTime(allList);
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
        }

        if (reqVO.getCode().equals(smsList.get(0).getCode())) {
            if (smsList.get(0).getUseTime() != null) {
                //验证码已经使用过
                userLoginFail(allList.get(0).getLoginId());//记录登录失败次数
                checkIsOverTime(allList);
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
            }
            // 获得两个时间之间的相差值
            long diff = LocalDateTimeUtil.between(smsList.get(0).getSendTime(), LocalDateTime.now()).getSeconds();
            QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("config_name", "sms_expire");
            FrameConfig config = frameConfigService.getOne(queryWrapper);
            if (config == null) {
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
            }
            //获取验证码有效期
            int limitSecond = Integer.parseInt(config.getConfigValue());
            if (diff > limitSecond) {
                //验证码过期
                userLoginFail(allList.get(0).getLoginId());//记录登录失败次数
                checkIsOverTime(allList);
                throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
            }

        } else {
            //验证码不正确
            userLoginFail(allList.get(0).getLoginId());//记录登录失败次数
            checkIsOverTime(allList);
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
        }

        // 获得用户信息
        List<FrameUser> userList = userService.lambdaQuery().eq(FrameUser::getMobile, reqVO.getMobile()).eq(FrameUser::getDelFlag, 0).eq(FrameUser::getStatus, 1).orderByAsc(FrameUser::getRowId).list();
        if (userList.size() == 0) {
            //手机号不存在
            throw new BaseException(ErrorCodeConstants.AUTH_LOGIN_FAILED);
        }
        UserInfo user;
        if (userList.size() == 1) {

            user = authenticateCasUser(userList.get(0).getLoginId());
        } else {
            //多部门
            List<FrameUser> m_userList = userService.lambdaQuery().eq(FrameUser::getMobile, reqVO.getMobile()).eq(FrameUser::getDelFlag, 0).eq(FrameUser::getMainDept, 1).list();
            if (m_userList.size() == 0) {
                user = authenticateCasUser(userList.get(0).getLoginId());
            } else {
                user = authenticateCasUser(m_userList.get(0).getLoginId());
            }

        }

        //删除key
        if (redisService.hasKey(key)) {
            redisService.del(key);
        }

        //更新验证码使用时间
        smsList.get(0).setUseTime(LocalDateTime.now());
        frameSmsCodeService.updateById(smsList.get(0));

        // 创建 Token 令牌，记录登录日志
        return createTokenAfterLoginSuccess(user,
                LoginLogTypeEnum.LOGIN_MOBILE);
    }

    @Override
    public void sendCode(String mobile) {
        log.info("获取到的手机号码为:" + mobile);

        //判断手机号码是否在用户表中
        QueryWrapper<FrameUser> wrapper = new QueryWrapper<>();
        wrapper.eq("del_flag", "0");
        wrapper.eq("mobile", mobile.trim());
        List<FrameUser> userList = userService.list(wrapper);
        if (userList.size() == 0) {
            throw new BaseException(ErrorCodeConstants.SMS_SEND_FAILED);
        }

        //首先判断当日短信发送次数是否超过限制
        QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("config_name", "smsLoginCount");
        FrameConfig config = frameConfigService.getOne(queryWrapper);
        if (config == null) {
            throw new BaseException(ErrorCodeConstants.SMS_SEND_FAILED);
        }
        //判断同一个手机号码，当日发送短信次数是否超过限制
        int limitCount = Integer.parseInt(config.getConfigValue());

        QueryWrapper<FrameSmsCode> smsWrapper = new QueryWrapper<>();
        smsWrapper.eq("mobile", mobile);
        smsWrapper.apply(DateUtil.getCurrentDate() != null,
                "date_format (send_time,'%Y-%m-%d') = date_format ({0},'%Y-%m-%d')", DateUtil.getCurrentDate());
        List<FrameSmsCode> list = frameSmsCodeService.list(smsWrapper);
        if (list.size() >= limitCount) {
            throw new BaseException(ErrorCodeConstants.SMS_SEND_FAILED);
        }

        //插入验证码
        String code = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
        FrameSmsCode model = new FrameSmsCode();
        model.initNull();
        model.setMobile(mobile);
        model.setCode(code);
        model.setSendTime(LocalDateTime.now());
        frameSmsCodeService.saveSms(model);

        //插入短信内容
        FrameSmsRecord record = new FrameSmsRecord();
        record.initNull();
        record.setSaveTime(LocalDateTime.now());
        record.setSendTime(LocalDateTime.now());
        record.setSendStatus("0");//待发送
        record.setReceiveMobile(mobile);
        record.setSmsContent("#code#=" + code);
        record.setSmsType("VerificationCode");
        frameSmsRecordService.saveSmsRecord(record);


    }

    /**
     * 检查用户登录失败次数是否超过限制
     *
     * @param loginId
     * @return
     */
    private boolean checkUserLoginFailCount(String loginId) {
        String key = redisService.getRedisKey(String.format(RedisKeyConstants.SYSTEM_LOGINID_FIALACCOUNT, loginId));
        try {
            boolean loginIdFlag = this.redisService.hasKey(key);
            if (!loginIdFlag) {
                return true;
            } else {
                int loginLimit = Integer.parseInt(frameConfigService.getCacheByConfigName("sys.login-fail.count"));
                JSONObject jsonObject = (JSONObject) this.redisService.get(key);
                int loginIdLimit = Integer.parseInt(jsonObject.getString("loginIdTimes"));
                return loginIdLimit < loginLimit;
            }
        } catch (Exception var11) {
            var11.printStackTrace();
            return true;
        }
    }

    /**
     * 用户登录失败的次数处理
     *
     * @param loginId
     */
    private synchronized void userLoginFail(String loginId) {
        String key = redisService.getRedisKey(String.format(RedisKeyConstants.SYSTEM_LOGINID_FIALACCOUNT, loginId));
        long timeLimit = Integer.parseInt(frameConfigService.getCacheByConfigName("sys.login-fail.time"));
        Object object = redisService.get(key);
        if (object != null) {
            int loginLimit = Integer.parseInt(frameConfigService.getCacheByConfigName("sys.login-fail.count"));
            //多次
            JSONObject jsonObject = JSONObject.parseObject(redisService.get(key).toString());
            int loginIdTimes = (int) jsonObject.get("loginIdTimes");
            //若小于限制次数
            if (loginIdTimes < loginLimit) {
                jsonObject.put("loginIdTimes", loginIdTimes + 1);
                redisService.set(key, jsonObject, timeLimit);
            }
        } else {
            //设置默认
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("loginId", loginId);
            jsonObject.put("loginIdTimes", 1);
            redisService.set(key, jsonObject, timeLimit);
        }
    }

    @EventListener
    public void handleRedisKeyExpiredEvent(RedisKeyExpiredEvent event) {
        // 处理Redis键过期事件的具体实现逻辑
        String expiredKey = event.getExpiredKey();
        //logger.info("Redis过期key：{}", expiredKey);
        String key = redisService.getRedisKey("system_loginid_failaccount");
        if (expiredKey.startsWith(key)) {
            String[] k = expiredKey.split(":");
            String loginId = k[k.length - 1];
            FrameUser userDO = new FrameUser();
            userDO.setStatus(CommonStatusEnum.ENABLE.getStatus());
            userService.update(userDO, new LambdaQueryWrapper<FrameUser>().eq(FrameUser::getLoginId, loginId));
        }
    }

    /**
     * 短信登录，实时判断是否超过登录失败限制次数
     *
     * @param allList
     */
    private void checkIsOverTime(List<FrameUser> allList) {
        Boolean loginFailEnable = Boolean.valueOf(frameConfigService.getCacheByConfigName("sys.login-fail.enable"));
        if (loginFailEnable) {
            if (!checkUserLoginFailCount(allList.get(0).getLoginId())) {
                for (FrameUser user : allList) {
                    FrameUser updateObj = new FrameUser();
                    updateObj.setRowId(user.getRowId());
                    updateObj.setStatus(CommonStatusEnum.DISABLE.getStatus());
                    userService.updateById(updateObj);
                }
            }
        }
    }
}
