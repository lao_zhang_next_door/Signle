package com.sinoy.core.oauth2.maps;

import com.sinoy.core.common.entity.KeyValue;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.OAuth2ApproveDO;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.pojo.open.OAuth2OpenAccessTokenRespVO;
import com.sinoy.core.oauth2.pojo.open.OAuth2OpenAuthorizeInfoRespVO;
import com.sinoy.core.oauth2.pojo.open.OAuth2OpenCheckTokenRespVO;
import com.sinoy.core.oauth2.pojo.open.OAuth2OpenCodeAccessTokenRespVO;
import com.sinoy.core.oauth2.util.OAuth2Utils;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import com.sinoy.platform.system.component.entity.FrameUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
public interface OAuth2OpenConvert {

    OAuth2OpenConvert INSTANCE = Mappers.getMapper(OAuth2OpenConvert.class);

    default OAuth2OpenAccessTokenRespVO convert(OAuth2AccessTokenDO bean) {
        OAuth2OpenAccessTokenRespVO respVO = convert0(bean);
        respVO.setTokenType(SecurityFrameworkUtils.AUTHORIZATION_BEARER.toLowerCase());
        respVO.setExpiresIn(OAuth2Utils.getExpiresIn(bean.getExpiresTime()));
        respVO.setScope(OAuth2Utils.buildScopeStr(bean.getScopes()));
        return respVO;
    }

    /**
     * 目前只有授权码模式涉及到返回 用户信息 暂时先这样处理
     * @param bean
     * @return
     */
    default OAuth2OpenCodeAccessTokenRespVO convertCodeToken(OAuth2AccessTokenDO bean) {
        FrameUser user = bean.getUser().getFrameUser();
        OAuth2OpenCodeAccessTokenRespVO respVO = convertCode(bean);
        respVO.setTokenType(SecurityFrameworkUtils.AUTHORIZATION_BEARER.toLowerCase());
        respVO.setExpiresIn(OAuth2Utils.getExpiresIn(bean.getExpiresTime()));
        respVO.setScope(OAuth2Utils.buildScopeStr(bean.getScopes()));

        if(bean.getScopes().contains("loginId") && user != null){
            respVO.setLoginId(user.getLoginId());
        }

        return respVO;
    }

    OAuth2OpenAccessTokenRespVO convert0(OAuth2AccessTokenDO bean);

    OAuth2OpenCodeAccessTokenRespVO convertCode(OAuth2AccessTokenDO bean);

    default OAuth2OpenCheckTokenRespVO convert2(OAuth2AccessTokenDO bean) {
        OAuth2OpenCheckTokenRespVO respVO = convert3(bean);
        respVO.setExp(bean.getExpiresTime().getTime() / 1000L);
        respVO.setUserType(UserTypeEnum.ADMIN.getValue());
        return respVO;
    }

    OAuth2OpenCheckTokenRespVO convert3(OAuth2AccessTokenDO bean);

    default OAuth2OpenAuthorizeInfoRespVO convert(OAuth2ClientDO client, List<OAuth2ApproveDO> approves) {
        // 构建 scopes
        List<KeyValue<String, Boolean>> scopes = new ArrayList<>(client.getScopes().size());
        Map<String, OAuth2ApproveDO> approveMap = CollectionUtils.convertMap(approves, OAuth2ApproveDO::getScope);
        client.getScopes().forEach(scope -> {
            OAuth2ApproveDO approve = approveMap.get(scope);
            scopes.add(new KeyValue<>(scope, approve != null ? approve.getApproved() : false));
        });
        // 拼接返回
        return new OAuth2OpenAuthorizeInfoRespVO(
                new OAuth2OpenAuthorizeInfoRespVO.Client(client.getName(), client.getLogo()), scopes);
    }

}
