package com.sinoy.core.oauth2.filter;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.constant.Oauth2Constant;
import com.sinoy.core.common.utils.exception.GlobalExceptionHandler;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.request.ServletUtils;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Token 过滤器，验证 token 的有效性
 * 验证通过后，获得 用户 信息，并加入到 Spring Security 上下文
 *
 */
public class TokenAuthenticationFilter extends OncePerRequestFilter {


    private final FrameUserService frameUserService;
    private final GlobalExceptionHandler globalExceptionHandler;

    private final OAuth2TokenService oauth2TokenApi;

    private final CommonPropAndMethods commonPropAndMethods;

    public TokenAuthenticationFilter(GlobalExceptionHandler globalExceptionHandler, OAuth2TokenService oauth2TokenApi,
                                     CommonPropAndMethods commonPropAndMethods,FrameUserService frameUserService) {
        this.globalExceptionHandler = globalExceptionHandler;
        this.oauth2TokenApi = oauth2TokenApi;
        this.commonPropAndMethods = commonPropAndMethods;
        this.frameUserService=frameUserService;
    }

    @Override
    @SuppressWarnings("NullableProblems")
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        String token = SecurityFrameworkUtils.obtainAuthorization(request, Oauth2Constant.HEADER_TOKEN);
        if (StrUtil.isNotEmpty(token)) {
            Integer userType = commonPropAndMethods.getLoginUserType(request);
            try {
                // 1 基于 token 构建登录用户
                XnwUser loginUser = buildLoginUserByToken(token, userType);

                // 2. 设置当前用户
                if (loginUser != null) {
                    SecurityFrameworkUtils.setLoginUser(loginUser, request);
                }

            } catch (Throwable ex) {
                R result = globalExceptionHandler.allExceptionHandler(request, ex);
                ServletUtils.writeJSON(response, result);
                return;
            }
        }

        // 继续过滤链
        chain.doFilter(request, response);
    }

    /**
     * 根据token 获取 对应用户信息
     * @param token
     * @param userType
     * @return
     */
    private XnwUser buildLoginUserByToken(String token, Integer userType) {
        try {
            OAuth2AccessTokenDO accessToken = oauth2TokenApi.checkAccessToken(token);
            if (accessToken == null) {
                return null;
            }

            /**
             * 暂时先注释掉
             */
//            // 用户类型不匹配，无权限
//            if (ObjectUtil.notEqual(accessToken.getUserType(), userType)) {
//                throw new AccessDeniedException("错误的用户类型");
//            }

            /**
             * 用户密码模式 可以获取大部分用户信息
             * code 模式 只可以取得userId  即后续接口获取用户信息 只能获取会话中的 用户id信息
             * 其余模式 待开发
             */

            //用户信息不存在
            UserInfo userInfo = accessToken.getUser() == null ? new UserInfo() : accessToken.getUser();
            FrameUser user = userInfo.getFrameUser() == null ? new FrameUser(false) : userInfo.getFrameUser();
           // FrameDept dept= frameUserService.getCurrentDept(user.getRowId(),user.getDeptId());
            FrameDept dept= frameUserService.getCurrentDept(user.getRowId(),user.getDeptId());

            // 构建登录用户权限
            Collection<? extends GrantedAuthority> authorities = new ArrayList();
            if(userInfo.getPermissions() != null){
                authorities = AuthorityUtils.createAuthorityList(Convert.toStrArray(userInfo.getPermissions()));
            }

           /* return new XnwUser(user.getLoginId(), "{SHA-256}" + user.getPassword(), userInfo.getIsAdmin(),authorities,userInfo.getDataScope()
                    ,accessToken.getUserId(), user.getRowGuid(), user.getDeptId(), user.getDeptGuid(), userInfo.getDeptName()
                    , userInfo.getPdeptGuid(), userInfo.getPdetpName(), user.getUserName(), user.getMobile(), userInfo.getHeadImgUrl(), null
                    , user.getEmail(), userInfo.getRoleNameList(), userInfo.getDeptCode(), userInfo.getRoleGuids());*/

            return new XnwUser(user.getLoginId(), "{SHA-256}" + user.getPassword(), userInfo.getIsAdmin(),authorities,userInfo.getDataScope()
                    ,accessToken.getUserId(), user.getRowGuid(), dept.getRowId(), dept.getRowGuid(), dept.getDeptName()
                    , dept.getParentGuid(),"", user.getUserName(), user.getMobile(), userInfo.getHeadImgUrl(), null
                    , user.getEmail(), userInfo.getRoleNameList(), dept.getDeptCode(), userInfo.getRoleGuids());
        } catch (Exception serviceException) {
            serviceException.printStackTrace();
            // 校验 Token 不通过时，考虑到一些接口是无需登录的，所以直接返回 null 即可
            return null;
        }
    }

}
