package com.sinoy.core.oauth2.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.LoginLogTypeEnum;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.service.AuthService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Map;


@RestController
@RequestMapping("/frame/oauth2-token")
public class OAuth2TokenController {

    @Resource
    private OAuth2TokenService oauth2TokenService;
    @Resource
    private AuthService authService;

    /**
     * 获得访问令牌分页  只返回有效期内的
     * @return
     */
    @PostMapping("/page")
    public R getAccessTokenPage(@RequestBody Search search) {

        Map<String, Object> params = search.getParams();
        QueryWrapper<OAuth2AccessTokenDO> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, params);
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<OAuth2AccessTokenDO> page = new Page<>(search.getCurrentPage(), search.getPageSize());

        /**
         * 过期token不展示
         */
        queryWrapper.ge("expires_time", LocalDateTime.now()).eq("del_flag",DelFlag.Normal.getCode());
        Page<OAuth2AccessTokenDO> iPage = oauth2TokenService.page(page, queryWrapper);
        return R.ok().put("data",iPage);
    }

    /**
     * 删除访问令牌
     * @param accessToken
     * @return
     */
    @DeleteMapping("/delete")
    public R deleteAccessToken(@RequestParam("accessToken") String accessToken) {
        authService.logout(accessToken, LoginLogTypeEnum.LOGOUT_DELETE.getType());
        return R.ok();
    }

}
