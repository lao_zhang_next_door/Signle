package com.sinoy.core.oauth2.maps;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.token.OAuth2AccessTokenRespVO;
import com.sinoy.platform.system.component.entity.dto.OAuth2AccessTokenCheckRespDTO;
import com.sinoy.platform.system.component.entity.dto.OAuth2AccessTokenRespDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OAuth2TokenConvert {

    OAuth2TokenConvert INSTANCE = Mappers.getMapper(OAuth2TokenConvert.class);

    OAuth2AccessTokenRespVO convertRes(OAuth2AccessTokenDO oAuth2AccessTokenDO);

    OAuth2AccessTokenCheckRespDTO convert(OAuth2AccessTokenDO bean);

    Page<OAuth2AccessTokenRespVO> convertPage(Page<OAuth2AccessTokenDO> page);

    OAuth2AccessTokenRespDTO convert2(OAuth2AccessTokenDO bean);

}
