package com.sinoy.core.oauth2.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

import java.util.Date;
import java.util.Objects;

/**
 * OAuth2 批准 DO
 *
 * 用户在 sso.vue 界面时，记录接受的 scope 列表
 *
 */
@TableName(value = "frame_oauth2_approve", autoResultMap = true)
//@KeySequence("system_oauth2_approve_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class OAuth2ApproveDO extends BaseEntity {
    public OAuth2ApproveDO() {
    }

    public OAuth2ApproveDO(boolean init) {
        super(init);
    }

    public OAuth2ApproveDO(String rowGuid) {
        super(rowGuid);
    }
    /**
     * 用户编号
     */
    private Long userId;
    /**
     * 用户类型
     * <p>
     * 枚举 {@link UserTypeEnum}
     */
    private Integer userType;
    /**
     * 客户端编号
     * <p>
     * 关联
     */
    private String clientId;
    /**
     * 授权范围
     */
    private String scope;
    /**
     * 是否接受
     * <p>
     * true - 接受
     * false - 拒绝
     */
    private Boolean approved;
    /**
     * 过期时间
     */
    private Date expiresTime;

    public Long getUserId() {
        return userId;
    }

    public OAuth2ApproveDO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2ApproveDO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2ApproveDO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getScope() {
        return scope;
    }

    public OAuth2ApproveDO setScope(String scope) {
        this.scope = scope;
        return this;
    }

    public Boolean getApproved() {
        return approved;
    }

    public OAuth2ApproveDO setApproved(Boolean approved) {
        this.approved = approved;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2ApproveDO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2ApproveDO that = (OAuth2ApproveDO) o;
        return Objects.equals(userId, that.userId) && Objects.equals(userType, that.userType) && Objects.equals(clientId, that.clientId) && Objects.equals(scope, that.scope) && Objects.equals(approved, that.approved) && Objects.equals(expiresTime, that.expiresTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userType, clientId, scope, approved, expiresTime);
    }

}
