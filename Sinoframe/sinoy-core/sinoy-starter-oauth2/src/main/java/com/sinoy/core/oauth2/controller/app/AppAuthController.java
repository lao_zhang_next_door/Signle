package com.sinoy.core.oauth2.controller.app;

import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.constant.Oauth2Constant;
import com.sinoy.core.common.enums.LoginLogTypeEnum;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.encryption.annotation.AsymmetryCrypto;
import com.sinoy.core.oauth2.maps.OAuth2OpenConvert;
import com.sinoy.core.oauth2.pojo.login.AuthLoginReqVO;
import com.sinoy.core.oauth2.service.AuthService;
import com.sinoy.core.oauth2.service.OAuth2GrantService;
import com.sinoy.core.security.annotation.PassToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.obtainAuthorization;

@RestController
@RequestMapping("/frame/auth")
@Validated
public class AppAuthController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private AuthService authService;

    @Resource
    private OAuth2GrantService oauth2GrantService;

    /**
     * 登出系统
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    @PassToken
    public R logout(HttpServletRequest request) {
        String token = obtainAuthorization(request, Oauth2Constant.HEADER_TOKEN);
        if (StrUtil.isNotBlank(token)) {
            authService.logout(token, LoginLogTypeEnum.LOGOUT_SELF.getType());
        }
        return R.ok();
    }

    /**
     * 刷新令牌
     *
     * @param refreshToken
     * @return
     */
    @PostMapping("/refresh-token")
    @PassToken
    public R refreshToken(@RequestParam("refreshToken") String refreshToken,@RequestParam("clientId") String clientId) {
        return R.ok().put("data", OAuth2OpenConvert.INSTANCE.convert(oauth2GrantService.grantRefreshToken(refreshToken, clientId)));
    }

}
