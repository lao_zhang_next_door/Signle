package com.sinoy.core.oauth2.pojo.client;

import java.util.Date;
import java.util.Objects;

public class OAuth2ClientRespVO extends OAuth2ClientBaseVO {

    //编号
    private Long id;

    //创建时间
    private Date createTime;

    public Long getId() {
        return id;
    }

    public OAuth2ClientRespVO setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public OAuth2ClientRespVO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2ClientRespVO that = (OAuth2ClientRespVO) o;
        return Objects.equals(id, that.id) && Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createTime);
    }

    @Override
    public String toString() {
        return "OAuth2ClientRespVO{" +
                "id=" + id +
                ", createTime=" + createTime +
                '}';
    }
}
