package com.sinoy.core.oauth2.service;

import com.sinoy.core.oauth2.pojo.login.AuthLoginReqVO;
import com.sinoy.core.oauth2.pojo.login.AuthLoginRespVO;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.entity.vo.AuthSmsLoginReqVO;
import com.sinoy.support.captcha.model.vo.CaptchaVO;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 管理后台的认证 Service 接口
 * <p>
 * 提供用户的登录、登出的能力
 */
public interface AuthService {

    /**
     * 账号登录
     *
     * @param reqVO 登录信息
     * @return 登录结果
     */
    AuthLoginRespVO login(@Valid AuthLoginReqVO reqVO);

    /**
     * 基于 token 退出登录
     *
     * @param token   token
     * @param logType 登出类型
     */
    void logout(String token, Integer logType);

    /**
     * 刷新访问令牌
     *
     * @param refreshToken 刷新令牌
     * @return 登录结果
     */
    AuthLoginRespVO refreshToken(String refreshToken);

    /**
     * 验证账号 + 密码。如果通过，则返回用户
     *
     * @param username 账号
     * @param password 密码
     * @return 用户
     */
    UserInfo authenticate(String username, String password);

    /**
     * 使用cas ticket 登录
     *
     * @param ticket
     * @return
     */
    AuthLoginRespVO loginByTicket(String ticket);

    /**
     * 根据ticket 获取用户信息
     *
     * @param ticket
     * @return
     */
    String getCasUserByTicket(String ticket);



    /*待开发*/

    /**
     * 短信验证码发送
     *
     * @param reqVO 发送请求
     */
//    void sendSmsCode(AuthSmsSendReqVO reqVO);

    /**
     * 短信登录
     *
     * @param reqVO 登录信息
     * @return 登录结果
     */
    AuthLoginRespVO smsLogin(AuthSmsLoginReqVO reqVO) ;

    /**
     * 社交快捷登录，使用 code 授权码
     *
     * @param reqVO 登录信息
     * @return 登录结果
     */
//    AuthLoginRespVO socialLogin(@Valid AuthSocialLoginReqVO reqVO);

    /**
     * 验证用户滑动验证
     *
     * @param captchaVO
     * @return
     */
    String checkUserCaptcha(CaptchaVO captchaVO);

    /**
     * 发送短信验证码
     * @param mobile
     */
    void sendCode(String mobile);
}
