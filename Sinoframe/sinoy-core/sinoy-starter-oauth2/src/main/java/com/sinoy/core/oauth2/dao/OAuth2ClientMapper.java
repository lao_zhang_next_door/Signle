package com.sinoy.core.oauth2.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * OAuth2 客户端 Mapper
 */
public interface OAuth2ClientMapper extends ExtendMapper<OAuth2ClientDO> {

//    default Page<OAuth2ClientDO> selectPage(OAuth2ClientPageReqVO reqVO) {
//
//        QueryWrapper<FrameConfig> queryWrapper = new QueryWrapper<>();
//        BatisPlusUtil.setParams(queryWrapper, null);
//        Page<OAuth2ClientDO> pageP = new Page(reqVO.getPageNo(), reqVO.getPageSize());
//        Page<OAuth2ClientDO> page = selectPage(pageP,queryWrapper);
//
//         return null;
//    }

    default OAuth2ClientDO selectByClientId(String clientId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_id",clientId);
        return selectOne(queryWrapper);
    }

    @Select("SELECT COUNT(*) FROM frame_oauth2_client WHERE update_time > #{maxUpdateTime}")
    int selectCountByUpdateTimeGt(Date maxUpdateTime);

}
