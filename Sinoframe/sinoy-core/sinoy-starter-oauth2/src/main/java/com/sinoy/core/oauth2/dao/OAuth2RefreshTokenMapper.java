package com.sinoy.core.oauth2.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.oauth2.pojo.OAuth2RefreshTokenDO;

public interface OAuth2RefreshTokenMapper extends ExtendMapper<OAuth2RefreshTokenDO> {

    default int deleteByRefreshToken(String refreshToken) {
        return delete(new LambdaQueryWrapper<OAuth2RefreshTokenDO>()
                .eq(OAuth2RefreshTokenDO::getRefreshToken, refreshToken));
    }

    default OAuth2RefreshTokenDO selectByRefreshToken(String refreshToken) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("refresh_token",refreshToken);
        return selectOne(queryWrapper);
    }

}
