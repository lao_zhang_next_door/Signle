package com.sinoy.core.oauth2.maps;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientCreateReqVO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientRespVO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientUpdateReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * OAuth2 客户端 Convert
 *
 */
@Mapper
public interface OAuth2ClientConvert {

    OAuth2ClientConvert INSTANCE = Mappers.getMapper(OAuth2ClientConvert.class);

    OAuth2ClientDO convert(OAuth2ClientCreateReqVO bean);

    @Mappings({
            @Mapping(target = "rowId",source = "rowId")
    })
    OAuth2ClientDO convert(OAuth2ClientUpdateReqVO bean);

    OAuth2ClientRespVO convert(OAuth2ClientDO bean);

    List<OAuth2ClientRespVO> convertList(List<OAuth2ClientDO> list);

    Page<OAuth2ClientRespVO> convertPage(Page<OAuth2ClientDO> page);

}
