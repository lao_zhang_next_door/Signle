package com.sinoy.core.oauth2.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * OAuth2 访问令牌 DO
 *
 * 如下字段，暂时未使用，暂时不支持：
 * user_name、authentication（用户信息）
 *
 */
@TableName(value = "frame_oauth2_access_token", autoResultMap = true)
//@KeySequence("system_oauth2_access_token_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class OAuth2AccessTokenDO extends BaseEntity {

    public OAuth2AccessTokenDO() {
    }

    public OAuth2AccessTokenDO(boolean init) {
        super(init);
    }

    public OAuth2AccessTokenDO(String rowGuid) {
        super(rowGuid);
    }

    /**
     * 访问令牌
     */
    private String accessToken;
    /**
     * 刷新令牌
     */
    private String refreshToken;
    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 用户行标
     */
    private String userGuid;
    /**
     * 用户类型
     *
     * 枚举 {@link UserTypeEnum}
     */
    private Integer userType;
    /**
     * 客户端编号
     *
     * 关联
     */
    private String clientId;
    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;
    /**
     * 过期时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expiresTime;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    private UserInfo user;

//    /**
//     * 用户登录id
//     */
//    @Transient
//    @TableField(exist=false)
//    private String loginId;
//
//    public String getLoginId() {
//        return loginId;
//    }
//
//    public void setLoginId(String loginId) {
//        this.loginId = loginId;
//    }

    public UserInfo getUser() {
        return user;
    }

    public OAuth2AccessTokenDO setUser(UserInfo user) {
        this.user = user;
        return this;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public OAuth2AccessTokenDO setUserGuid(String userGuid) {
        this.userGuid = userGuid;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2AccessTokenDO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2AccessTokenDO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public OAuth2AccessTokenDO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2AccessTokenDO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2AccessTokenDO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public OAuth2AccessTokenDO setScopes(List<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2AccessTokenDO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2AccessTokenDO that = (OAuth2AccessTokenDO) o;
        return Objects.equals(accessToken, that.accessToken) && Objects.equals(refreshToken, that.refreshToken) && Objects.equals(userId, that.userId) && Objects.equals(userType, that.userType) && Objects.equals(clientId, that.clientId) && Objects.equals(scopes, that.scopes) && Objects.equals(expiresTime, that.expiresTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessToken, refreshToken, userId, userType, clientId, scopes, expiresTime);
    }
}
