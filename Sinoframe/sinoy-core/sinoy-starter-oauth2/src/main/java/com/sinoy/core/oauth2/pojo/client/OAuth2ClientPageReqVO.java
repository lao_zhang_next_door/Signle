package com.sinoy.core.oauth2.pojo.client;

import com.sinoy.core.common.utils.request.PageParam;

import java.util.Objects;

public class OAuth2ClientPageReqVO  extends PageParam {

    //应用名
    private String name;

    //状态
    private Integer status;

    public String getName() {
        return name;
    }

    public OAuth2ClientPageReqVO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public OAuth2ClientPageReqVO setStatus(Integer status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2ClientPageReqVO that = (OAuth2ClientPageReqVO) o;
        return Objects.equals(name, that.name) && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, status);
    }

    @Override
    public String toString() {
        return "OAuth2ClientPageReqVO{" +
                "name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
