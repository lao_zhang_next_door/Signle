package com.sinoy.core.oauth2.controller.admin;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.sinoy.core.common.enums.OAuth2GrantTypeEnum;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.common.exception.GlobalErrorCodeConstants;
import com.sinoy.core.common.utils.dataformat.JsonUtils;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.oauth2.maps.AuthConvert;
import com.sinoy.core.oauth2.maps.OAuth2OpenConvert;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.OAuth2ApproveDO;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.service.OAuth2ApproveService;
import com.sinoy.core.oauth2.service.OAuth2ClientService;
import com.sinoy.core.oauth2.service.OAuth2GrantService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.oauth2.util.OAuth2Utils;
import com.sinoy.core.security.annotation.PassToken;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * 提供给外部应用调用为主
 *
 * 一般来说，管理后台的 /system-api/* 是不直接提供给外部应用使用，主要是外部应用能够访问的数据与接口是有限的，而管理后台的 RBAC 无法很好的控制。
 * 参考大量的开放平台，都是独立的一套 OpenAPI，对应到【本系统】就是在 Controller 下新建 open 包，实现 /open-api/* 接口，然后通过 scope 进行控制。
 * 另外，一个公司如果有多个管理后台，它们 client_id 产生的 access token 相互之间是无法互通的，即无法访问它们系统的 API 接口，直到两个 client_id 产生信任授权。
 *
 * 默认只有获取到 access token 之后，可以访问【本系统】管理后台的 /system-api/* 所有接口，除非手动添加 scope 控制。
 *
 */
@RestController
@RequestMapping("/frame/oauth2")
@Validated
public class OAuth2OpenController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private OAuth2GrantService oauth2GrantService;
    @Resource
    private OAuth2ClientService oauth2ClientService;
    @Resource
    private OAuth2ApproveService oauth2ApproveService;
    @Resource
    private OAuth2TokenService oauth2TokenService;

    /**
     * 对应 Spring Security OAuth 的 TokenEndpoint 类的 postAccessToken 方法
     *
     * 授权码 authorization_code 模式时：code + redirectUri + state 参数
     * 密码 password 模式时：username + password + scope 参数
     * 刷新 refresh_token 模式时：refreshToken 参数
     * 客户端 client_credentials 模式：scope 参数
     * 简化 implicit 模式时：不支持
     *
     * 注意，默认需要传递 client_id + client_secret 参数
     *
     * 获得访问令牌
     * 适合 code 授权码模式，或者 implicit 简化模式；在 sso.vue 单点登录界面被【获取】调用
     *
     * @param grantType 授权类型
     * @param code 授权范围  userinfo.read
     * @param redirectUri 重定向 URI
     * @param state 状态
     * @param username 多个使用空格分隔
     * @param password 多个使用空格分隔
     * @param scope
     * @param refreshToken
     * @return
     *
     */
    @PostMapping("/token")
    @PassToken
    public R postAccessToken(HttpServletRequest request,
                             @RequestParam("grant_type") String grantType,
                             @RequestParam(value = "code", required = false) String code, // 授权码模式
                             @RequestParam(value = "redirect_uri", required = false) String redirectUri, // 授权码模式
                             @RequestParam(value = "state", required = false) String state, // 授权码模式
                             @RequestParam(value = "username", required = false) String username, // 密码模式
                             @RequestParam(value = "password", required = false) String password, // 密码模式
                             @RequestParam(value = "scope", required = false) String scope, // 密码模式
                             @RequestParam(value = "refresh_token", required = false) String refreshToken) { // 刷新模式
        List<String> scopes = OAuth2Utils.buildScopes(scope);
        // 授权类型
        OAuth2GrantTypeEnum grantTypeEnum = OAuth2GrantTypeEnum.getByGranType(grantType);
        if (grantTypeEnum == null) {
            throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), StrUtil.format("未知授权类型({})", grantType));
        }
        if (grantTypeEnum == OAuth2GrantTypeEnum.IMPLICIT) {
            throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), "Token 接口不支持 implicit 授权模式");
        }

        // 校验客户端
        String[] clientIdAndSecret = obtainBasicAuthorization(request);
        OAuth2ClientDO client = oauth2ClientService.validOAuthClientFromCache(clientIdAndSecret[0], clientIdAndSecret[1],
                grantType, scopes, redirectUri);

        // 根据授权模式，获取访问令牌
        OAuth2AccessTokenDO accessTokenDO;
        switch (grantTypeEnum) {
            case AUTHORIZATION_CODE:

                accessTokenDO = oauth2GrantService.grantAuthorizationCodeForAccessToken(client.getClientId(), code, redirectUri, state);
                Assert.notNull(accessTokenDO, "访问令牌不能为空"); // 防御性检查
                return R.ok().put("data", OAuth2OpenConvert.INSTANCE.convertCodeToken(accessTokenDO));

            case PASSWORD:

                accessTokenDO = oauth2GrantService.grantPassword(username, password, client.getClientId(), scopes);
                return buildRespToken(accessTokenDO);

            case CLIENT_CREDENTIALS:

                accessTokenDO = oauth2GrantService.grantClientCredentials(client.getClientId(), scopes);
                return buildRespToken(accessTokenDO);

            case REFRESH_TOKEN:

                accessTokenDO = oauth2GrantService.grantRefreshToken(refreshToken, client.getClientId());
                return buildRespToken(accessTokenDO);

            default:
                throw new IllegalArgumentException("未知授权类型：" + grantType);
        }
    }

    /**
     * 刷新令牌
     *
     * @param refreshToken
     * @return
     */
    @PostMapping("/refresh-token")
    @PassToken
    public R refreshToken(@RequestParam("refreshToken") String refreshToken,@RequestParam("clientId") String clientId) {
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.refreshAccessToken(refreshToken, clientId);
        return R.ok().put("data",buildRespToken(accessTokenDO));
    }

    public R buildRespToken(OAuth2AccessTokenDO accessTokenDO){
        Assert.notNull(accessTokenDO, "访问令牌不能为空"); // 防御性检查
        return R.ok().put("data", OAuth2OpenConvert.INSTANCE.convert(accessTokenDO));
    }


    /**
     * 删除访问令牌
     * @param request
     * @param token
     * @return
     */
    @DeleteMapping("/token")
    @PermitAll
    public R revokeToken(HttpServletRequest request,@RequestParam("token") String token) {
        // 校验客户端
        String[] clientIdAndSecret = obtainBasicAuthorization(request);
        OAuth2ClientDO client = oauth2ClientService.validOAuthClientFromCache(clientIdAndSecret[0], clientIdAndSecret[1],
                null, null, null);

        // 删除访问令牌
        return R.ok().put("data",oauth2GrantService.revokeToken(client.getClientId(), token));
    }

    /**
     * 对应 Spring Security OAuth 的 CheckTokenEndpoint 类的 checkToken 方法
     * 校验访问令牌
     */
    @PostMapping("/check-token")
    @PassToken
    public R checkToken(HttpServletRequest request,@RequestParam("token") String token) {
        // 校验客户端
        String[] clientIdAndSecret = obtainBasicAuthorization(request);
        oauth2ClientService.validOAuthClientFromCache(clientIdAndSecret[0], clientIdAndSecret[1],
                null, null, null);

        // 校验令牌
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.checkAccessToken(token);
        Assert.notNull(accessTokenDO, "访问令牌不能为空"); // 防御性检查
        return R.ok().put("data",OAuth2OpenConvert.INSTANCE.convert2(accessTokenDO));
    }

    /**
     * 对应 Spring Security OAuth 的 AuthorizationEndpoint 类的 authorize 方法
     * 获得授权信息 适合 code 授权码模式，或者 implicit 简化模式；在 sso.vue 单点登录界面被【获取】调用
     */
    @GetMapping("/authorize")
    public R authorize(@RequestParam("clientId") String clientId) {
        // 0. 校验用户已经登录。通过 Spring Security 实现

        // 1. 获得 Client 客户端的信息
        OAuth2ClientDO client = oauth2ClientService.validOAuthClientFromCache(clientId);
        // 2. 获得用户已经授权的信息
        List<OAuth2ApproveDO> approves = oauth2ApproveService.getApproveList(SecurityFrameworkUtils.getLoginUserId(), getUserType(), clientId);
        // 拼接返回
        return R.ok().put("data",OAuth2OpenConvert.INSTANCE.convert(client, approves));
    }

    /**
     * 对应 Spring Security OAuth 的 AuthorizationEndpoint 类的 approveOrDeny 方法
     *
     * 场景一：【自动授权 autoApprove = true】
     *      刚进入 sso.vue 界面，调用该接口，用户历史已经给该应用做过对应的授权，或者 OAuth2Client 支持该 scope 的自动授权
     * 场景二：【手动授权 autoApprove = false】
     *      在 sso.vue 界面，用户选择好 scope 授权范围，调用该接口，进行授权。此时，approved 为 true 或者 false
     *
     * 因为前后端分离，Axios 无法很好的处理 302 重定向，所以和 Spring Security OAuth 略有不同，返回结果是重定向的 URL，剩余交给前端处理
     * 申请授权  适合 code 授权码模式，或者 implicit 简化模式；在 sso.vue 单点登录界面被【提交】调用
     */

    /**
     *
     * @param responseType 响应类型
     * @param clientId 客户端编号
     * @param scope 授权范围
     * @param redirectUri 重定向 URI
     * @param autoApprove 用户是否接受
     * @param state
     * @return
     */
    @PostMapping("/authorize")
    @GetMapping("/authorize")
    public R approveOrDeny(@RequestParam("response_type") String responseType,
                                              @RequestParam("client_id") String clientId,
                                              @RequestParam(value = "scope", required = false) String scope,
                                              @RequestParam("redirect_uri") String redirectUri,
                                              @RequestParam(value = "auto_approve") Boolean autoApprove,
                                              @RequestParam(value = "state", required = false) String state) {
        @SuppressWarnings("unchecked")
        Map<String, Boolean> scopes = JsonUtils.parseObject(scope, Map.class);
        scopes = ObjectUtil.defaultIfNull(scopes, Collections.emptyMap());
        // 0. 校验用户已经登录。通过 Spring Security 实现

        // 1.1 校验 responseType 是否满足 code 或者 token 值
        OAuth2GrantTypeEnum grantTypeEnum = getGrantTypeEnum(responseType);
        // 1.2 校验 redirectUri 重定向域名是否合法 + 校验 scope 是否在 Client 授权范围内
        OAuth2ClientDO client = oauth2ClientService.validOAuthClientFromCache(clientId, null,
                grantTypeEnum.getGrantType(), scopes.keySet(), redirectUri);

        // 2.1 假设 approved 为 null，说明是场景一
        if (Boolean.TRUE.equals(autoApprove)) {
            // 如果无法自动授权通过，则返回空 url，前端不进行跳转
            if (!oauth2ApproveService.checkForPreApproval(SecurityFrameworkUtils.getLoginUserId(), getUserType(), clientId, scopes.keySet())) {
                return R.ok();
            }
        } else { // 2.2 假设 approved 非 null，说明是场景二
            // 如果计算后不通过，则跳转一个错误链接
            if (!oauth2ApproveService.updateAfterApproval(SecurityFrameworkUtils.getLoginUserId(), getUserType(), clientId, scopes)) {
                return R.ok().put("data",OAuth2Utils.buildUnsuccessfulRedirect(redirectUri, responseType, state,
                        "access_denied", "User denied access"));
            }
        }

        // 3.1 如果是 code 授权码模式，则发放 code 授权码，并重定向
        List<String> approveScopes = CollectionUtils.convertList(scopes.entrySet(), Map.Entry::getKey, Map.Entry::getValue);
        if (grantTypeEnum == OAuth2GrantTypeEnum.AUTHORIZATION_CODE) {
            return R.ok().put("data",getAuthorizationCodeRedirect(SecurityFrameworkUtils.getLoginUserId(), client, approveScopes, redirectUri, state));
        }
        // 3.2 如果是 token 则是 implicit 简化模式，则发送 accessToken 访问令牌，并重定向
        return R.ok().put("data",getImplicitGrantRedirect(SecurityFrameworkUtils.getLoginUserId(),SecurityFrameworkUtils.getLoginUserGuid(),client, approveScopes, redirectUri, state));
    }

    private static OAuth2GrantTypeEnum getGrantTypeEnum(String responseType) {
        if (StrUtil.equals(responseType, "code")) {
            return OAuth2GrantTypeEnum.AUTHORIZATION_CODE;
        }
        if (StrUtil.equalsAny(responseType, "token")) {
            return OAuth2GrantTypeEnum.IMPLICIT;
        }
        throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), "response_type 参数值只允许 code 和 token");
    }

    private String getImplicitGrantRedirect(Long userId,String userGuid,OAuth2ClientDO client,
                                            List<String> scopes, String redirectUri, String state) {
        // 1. 创建 access token 访问令牌
        OAuth2AccessTokenDO accessTokenDO = oauth2GrantService.grantImplicit(userId,userGuid, getUserType(), client.getClientId(), scopes);
        Assert.notNull(accessTokenDO, "访问令牌不能为空"); // 防御性检查
        // 2. 拼接重定向的 URL
        // noinspection unchecked
        return OAuth2Utils.buildImplicitRedirectUri(redirectUri, accessTokenDO.getAccessToken(), state, accessTokenDO.getExpiresTime(),
                scopes, JsonUtils.parseObject(client.getAdditionalInformation(), Map.class));
    }

    private String getAuthorizationCodeRedirect(Long userId, OAuth2ClientDO client,
                                                List<String> scopes, String redirectUri, String state) {
        // 1. 创建 code 授权码
        String authorizationCode = oauth2GrantService.grantAuthorizationCodeForCode(userId, getUserType(), client.getClientId(), scopes,
                redirectUri, state);
        // 2. 拼接重定向的 URL
        return OAuth2Utils.buildAuthorizationCodeRedirectUri(redirectUri, authorizationCode, state);
    }

    private Integer getUserType() {
        return UserTypeEnum.ADMIN.getValue();
    }

    private String[] obtainBasicAuthorization(HttpServletRequest request) {
        String[] clientIdAndSecret = OAuth2Utils.obtainBasicAuthorization(request);
        if (ArrayUtil.isEmpty(clientIdAndSecret) || clientIdAndSecret.length != 2) {
            throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), "client_id 或 client_secret 未正确传递");
        }
        return clientIdAndSecret;
    }

}
