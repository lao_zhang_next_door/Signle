package com.sinoy.core.oauth2.pojo.client;

import javax.validation.constraints.NotNull;


public class OAuth2ClientUpdateReqVO extends OAuth2ClientBaseVO {

    @NotNull(message = "编号不能为空")
    private Long rowId;

    public Long getRowId() {
        return rowId;
    }

    public OAuth2ClientUpdateReqVO setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }
}
