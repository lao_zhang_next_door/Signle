package com.sinoy.core.oauth2.config;

import com.sinoy.core.security.config.AuthorizeRequestsCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * oahth2 模块的 Security 配置 保留空配置 保证 引入不报错
 */
@Configuration("Ouath2SecurityConfig")
public class SecurityConfiguration {

    @Bean("Ouath2RequestsCustomizer")
    public AuthorizeRequestsCustomizer authorizeRequestsCustomizer() {
        return new AuthorizeRequestsCustomizer() {

            @Override
            public void customize(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
//                // Swagger 接口文档
//                registry.antMatchers("/swagger-ui.html").anonymous()
//                        .antMatchers("/swagger-resources/**").anonymous()
//                        .antMatchers("/webjars/**").anonymous()
//                        .antMatchers("/*/api-docs").anonymous();
            }

        };
    }

}
