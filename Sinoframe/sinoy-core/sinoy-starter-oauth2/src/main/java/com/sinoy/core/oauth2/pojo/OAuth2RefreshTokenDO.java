package com.sinoy.core.oauth2.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.platform.system.component.entity.dto.UserInfo;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * OAuth2 刷新令牌
 */
@TableName(value = "frame_oauth2_refresh_token", autoResultMap = true)
// 由于 Oracle 的 SEQ 的名字长度有限制，所以就先用 system_oauth2_access_token_seq 吧，反正也没啥问题
//@KeySequence("system_oauth2_access_token_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class OAuth2RefreshTokenDO extends BaseEntity {

    public OAuth2RefreshTokenDO() {
    }

    public OAuth2RefreshTokenDO(boolean init) {
        super(init);
    }

    public OAuth2RefreshTokenDO(String rowGuid) {
        super(rowGuid);
    }

    /**
     * 刷新令牌
     */
    private String refreshToken;
    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 用户行标
     */
    private String userGuid;
    /**
     * 用户类型
     *
     * 枚举 {@link UserTypeEnum}
     */
    private Integer userType;
    /**
     * 客户端编号
     *
     */
    private String clientId;
    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;
    /**
     * 过期时间
     */
    private Date expiresTime;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    private UserInfo user;

    public UserInfo getUser() {
        return user;
    }

    public OAuth2RefreshTokenDO setUser(UserInfo user) {
        this.user = user;
        return this;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public OAuth2RefreshTokenDO setUserGuid(String userGuid) {
        this.userGuid = userGuid;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2RefreshTokenDO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public OAuth2RefreshTokenDO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2RefreshTokenDO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2RefreshTokenDO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public OAuth2RefreshTokenDO setScopes(List<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2RefreshTokenDO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2RefreshTokenDO that = (OAuth2RefreshTokenDO) o;
        return Objects.equals(refreshToken, that.refreshToken) && Objects.equals(userId, that.userId) && Objects.equals(userType, that.userType) && Objects.equals(clientId, that.clientId) && Objects.equals(scopes, that.scopes) && Objects.equals(expiresTime, that.expiresTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(refreshToken, userId, userType, clientId, scopes, expiresTime);
    }
}
