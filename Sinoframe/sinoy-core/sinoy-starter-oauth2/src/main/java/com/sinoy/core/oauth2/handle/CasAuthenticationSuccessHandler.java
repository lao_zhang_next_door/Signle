package com.sinoy.core.oauth2.handle;

import com.alibaba.fastjson2.JSON;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.service.AuthService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.core.security.props.CasProperties;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@Service
public class CasAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	protected final Log logger = LogFactory.getLog(this.getClass());

	private RequestCache requestCache = new HttpSessionRequestCache();

	@Value("${xnw.AccessTokenKey}")
	private String tokenKey;

	@Autowired
	private CasProperties casProperties;

	@Autowired
	private OAuth2TokenService oauth2TokenService;


	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
										Authentication authentication) throws ServletException, IOException {
		String targetUrlParameter = getTargetUrlParameter();
		if (isAlwaysUseDefaultTargetUrl()
				|| (targetUrlParameter != null && StringUtils.hasText(request.getParameter(targetUrlParameter)))) {
			requestCache.removeRequest(request, response);
			super.onAuthenticationSuccess(request, response, authentication);
			return;
		}
		clearAuthenticationAttributes(request);
		XnwUser userDetails = (XnwUser) authentication.getPrincipal();
		System.out.println(JSON.toJSONString("获取的用户信息为:  >>>>>>>" + userDetails));
		UserInfo user = new UserInfo();
		FrameUser frameUser = (FrameUser) new FrameUser(userDetails.getRowGuid()).setRowId(userDetails.getId());
		user.setFrameUser(frameUser);

		OAuth2AccessTokenDO token = oauth2TokenService.createAccessToken(user, UserTypeEnum.ADMIN.getValue(), "default", null);
		//往Cookie中设置token
		Cookie casCookie = new Cookie(tokenKey, token.getAccessToken());
//		casCookie.setMaxAge(expireTime * 60);
		response.addCookie(casCookie);
//		//设置后端认证成功标识
//		HttpSession httpSession = request.getSession();
//		httpSession.setAttribute(Constants.CAS_TOKEN, token);
		//登录成功后跳转到前端登录页面
		getRedirectStrategy().sendRedirect(request, response, casProperties.getWebUrl());
	}
}