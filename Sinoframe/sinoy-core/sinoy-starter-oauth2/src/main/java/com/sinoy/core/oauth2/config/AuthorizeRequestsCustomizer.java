//package com.sinoy.core.oauth2.config;
//
//
//import com.sinoy.core.common.props.WebProperties;
//import org.springframework.security.config.Customizer;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
//
//
///**
// * 自定义的 URL 的安全配置
// * 目的：每个 Maven Module 可以自定义规则！
// *
// */
//public abstract class AuthorizeRequestsCustomizer
//        implements Customizer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> {
//
//    /**
//     * 构造B端访问地址
//     * @param url
//     * @return
//     */
//    protected String buildAdminApi(String url) {
//        return WebProperties.AdminApi + url;
//    }
//
//    /**
//     * 构造C端访问地址
//     * @param url
//     * @return
//     */
//    protected String buildAppApi(String url) {
//        return WebProperties.AppApi + url;
//    }
//
//}
