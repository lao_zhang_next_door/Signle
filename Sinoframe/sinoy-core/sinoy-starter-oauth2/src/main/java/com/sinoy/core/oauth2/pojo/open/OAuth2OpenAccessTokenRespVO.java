package com.sinoy.core.oauth2.pojo.open;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OAuth2OpenAccessTokenRespVO {

    //访问令牌
    private String accessToken;

    //刷新令牌
    private String refreshToken;

    //令牌类型
    private String tokenType;

    //过期时间 单位：秒
    private Long expiresIn;

    //授权范围 如果多个授权范围，使用空格分隔
    private String scope;

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2OpenAccessTokenRespVO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2OpenAccessTokenRespVO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public String getTokenType() {
        return tokenType;
    }

    public OAuth2OpenAccessTokenRespVO setTokenType(String tokenType) {
        this.tokenType = tokenType;
        return this;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public OAuth2OpenAccessTokenRespVO setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
        return this;
    }

    public String getScope() {
        return scope;
    }

    public OAuth2OpenAccessTokenRespVO setScope(String scope) {
        this.scope = scope;
        return this;
    }

    public OAuth2OpenAccessTokenRespVO() {}

    public OAuth2OpenAccessTokenRespVO(String accessToken, String refreshToken, String tokenType, Long expiresIn, String scope) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.scope = scope;
    }
}
