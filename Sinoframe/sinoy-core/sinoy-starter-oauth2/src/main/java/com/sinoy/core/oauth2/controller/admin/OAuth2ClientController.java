package com.sinoy.core.oauth2.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.oauth2.maps.OAuth2ClientConvert;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientCreateReqVO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientUpdateReqVO;
import com.sinoy.core.oauth2.service.OAuth2ClientService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


@RestController
@RequestMapping("/frame/oauth2-client")
@Validated
public class OAuth2ClientController {

    @Resource
    private OAuth2ClientService oAuth2ClientService;

    //创建 OAuth2 客户端
    @PostMapping("/create")
    public R createOAuth2Client(@Valid @RequestBody OAuth2ClientCreateReqVO createReqVO) {
        return R.ok().put("data",oAuth2ClientService.createOAuth2Client(createReqVO));
    }

    //更新 OAuth2 客户端
    @PutMapping("/update")
    public R updateOAuth2Client(@Valid @RequestBody OAuth2ClientUpdateReqVO updateReqVO) {
        oAuth2ClientService.updateOAuth2Client(updateReqVO);
        return R.ok();
    }

    //删除 OAuth2 客户端
    @DeleteMapping("/delete")
    public R deleteOAuth2Client(@RequestParam("id") Long id) {
        oAuth2ClientService.deleteOAuth2Client(id);
        return R.ok();
    }

    //获得 OAuth2 客户端
    @GetMapping("/get")
    public R getOAuth2Client(@RequestParam("id") Long id) {
        OAuth2ClientDO oAuth2Client = oAuth2ClientService.getOAuth2Client(id);
        return R.ok().put("data", OAuth2ClientConvert.INSTANCE.convert(oAuth2Client));
    }

    //获得OAuth2 客户端分页
    @PostMapping("/page")
    public R getOAuth2ClientPage(@RequestBody Search search) {
        QueryWrapper<OAuth2ClientDO> queryWrapper = new QueryWrapper();
        BatisPlusUtil.setParams(queryWrapper,search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<OAuth2ClientDO> pageParam = new Page<>(search.getCurrentPage(),search.getPageSize());
        Page<OAuth2ClientDO> pageResult = oAuth2ClientService.page(pageParam,queryWrapper);
        return R.ok().put("data",pageResult);
    }

}
