package com.sinoy.core.oauth2.pojo.login;

import cn.hutool.core.util.StrUtil;
import com.sinoy.support.captcha.model.vo.CaptchaVO;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 如果登录并绑定社交用户，需要传递 social 开头的参数
 */
public class AuthLoginReqVO {

    //账号
    @NotEmpty(message = "登录账号不能为空")
//    @Length(min = 4, max = 16, message = "账号长度为 4-16 位")
//    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "账号格式为数字以及字母")
    private String username;

    //密码
    @NotEmpty(message = "密码不能为空")
    @Length(min = 4, max = 32, message = "密码长度为 4-32 位")
    private String password;

    // ========== 图片验证码相关 ==========

    //验证码  验证码开启时，需要传递
//    @NotEmpty(message = "验证码不能为空", groups = CodeEnableGroup.class)
    private String captchaVerification;

    // ========== 绑定社交登录时，需要传递如下参数 ==========

    //社交平台的类型
    private Integer socialType;

    //授权码
    private String socialCode;

    private String socialState;

    /**
     * 图形验证码信息
     */

    private CaptchaVO captchaVO;

    /**
     * 开启验证码的 Group
     */
    public interface CodeEnableGroup {
    }

    @AssertTrue(message = "授权码不能为空")
    public boolean isSocialCodeValid() {
        return socialType == null || StrUtil.isNotEmpty(socialCode);
    }

    @AssertTrue(message = "授权 state 不能为空")
    public boolean isSocialState() {
        return socialType == null || StrUtil.isNotEmpty(socialState);
    }

    public AuthLoginReqVO() {
    }

    public AuthLoginReqVO(String username, String password, String captchaVerification, Integer socialType, String socialCode, String socialState) {
        this.username = username;
        this.password = password;
        this.captchaVerification = captchaVerification;
        this.socialType = socialType;
        this.socialCode = socialCode;
        this.socialState = socialState;
    }

    public String getUsername() {
        return username;
    }

    public AuthLoginReqVO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AuthLoginReqVO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getCaptchaVerification() {
        return captchaVerification;
    }

    public AuthLoginReqVO setCaptchaVerification(String captchaVerification) {
        this.captchaVerification = captchaVerification;
        return this;
    }

    public Integer getSocialType() {
        return socialType;
    }

    public AuthLoginReqVO setSocialType(Integer socialType) {
        this.socialType = socialType;
        return this;
    }

    public String getSocialCode() {
        return socialCode;
    }

    public AuthLoginReqVO setSocialCode(String socialCode) {
        this.socialCode = socialCode;
        return this;
    }

    public String getSocialState() {
        return socialState;
    }

    public AuthLoginReqVO setSocialState(String socialState) {
        this.socialState = socialState;
        return this;
    }


    public static final class AuthLoginReqVOBuilder {
        private @NotEmpty(message = "登录账号不能为空") @Pattern(regexp = "^[A-Za-z0-9]+$", message = "账号格式为数字以及字母") String username;
        private @NotEmpty(message = "密码不能为空") String password;
        private @NotEmpty(message = "验证码不能为空", groups = CodeEnableGroup.class) String captchaVerification;
        private Integer socialType;
        private String socialCode;
        private String socialState;

        private AuthLoginReqVOBuilder() {
        }

        public static AuthLoginReqVOBuilder anAuthLoginReqVO() {
            return new AuthLoginReqVOBuilder();
        }

        public AuthLoginReqVOBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public AuthLoginReqVOBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public AuthLoginReqVOBuilder withCaptchaVerification(String captchaVerification) {
            this.captchaVerification = captchaVerification;
            return this;
        }

        public AuthLoginReqVOBuilder withSocialType(Integer socialType) {
            this.socialType = socialType;
            return this;
        }

        public AuthLoginReqVOBuilder withSocialCode(String socialCode) {
            this.socialCode = socialCode;
            return this;
        }

        public AuthLoginReqVOBuilder withSocialState(String socialState) {
            this.socialState = socialState;
            return this;
        }

        public AuthLoginReqVO build() {
            return new AuthLoginReqVO(username, password, captchaVerification, socialType, socialCode, socialState);
        }
    }

    public CaptchaVO getCaptchaVO() {
        return captchaVO;
    }

    public void setCaptchaVO(CaptchaVO captchaVO) {
        this.captchaVO = captchaVO;
    }
}
