package com.sinoy.core.oauth2.pojo.open;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OAuth2OpenCheckTokenRespVO {

    //用户编号
    @JsonProperty("user_id")
    private Long userId;

    //用户类型
    @JsonProperty("user_type")
    private Integer userType;

    //租户编号
    @JsonProperty("tenant_id")
    private Long tenantId;

    //客户端编号
    private String clientId;

    //授权范围
    private List<String> scopes;

    //访问令牌
    @JsonProperty("access_token")
    private String accessToken;

    //过期时间  时间戳 / 1000，即单位：秒
    private Long exp;

    public Long getUserId() {
        return userId;
    }

    public OAuth2OpenCheckTokenRespVO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2OpenCheckTokenRespVO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public OAuth2OpenCheckTokenRespVO setTenantId(Long tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2OpenCheckTokenRespVO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public OAuth2OpenCheckTokenRespVO setScopes(List<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2OpenCheckTokenRespVO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public Long getExp() {
        return exp;
    }

    public OAuth2OpenCheckTokenRespVO setExp(Long exp) {
        this.exp = exp;
        return this;
    }

    public OAuth2OpenCheckTokenRespVO(Long userId, Integer userType, Long tenantId, String clientId, List<String> scopes, String accessToken, Long exp) {
        this.userId = userId;
        this.userType = userType;
        this.tenantId = tenantId;
        this.clientId = clientId;
        this.scopes = scopes;
        this.accessToken = accessToken;
        this.exp = exp;
    }

    public OAuth2OpenCheckTokenRespVO() {
    }
}
