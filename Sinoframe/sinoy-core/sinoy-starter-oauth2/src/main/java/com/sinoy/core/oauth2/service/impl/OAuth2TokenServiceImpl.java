package com.sinoy.core.oauth2.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.exception.GlobalErrorCodeConstants;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.dateformat.DateUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.oauth2.dao.OAuth2AccessTokenMapper;
import com.sinoy.core.oauth2.dao.OAuth2AccessTokenRedisDAO;
import com.sinoy.core.oauth2.dao.OAuth2RefreshTokenMapper;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.pojo.OAuth2RefreshTokenDO;
import com.sinoy.core.oauth2.service.OAuth2ClientService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

/**
 * OAuth2.0 Token Service 实现类
 */
@Service
public class OAuth2TokenServiceImpl extends BaseServiceImpl<OAuth2AccessTokenMapper, OAuth2AccessTokenDO> implements OAuth2TokenService {

    @Resource
    private OAuth2AccessTokenMapper oauth2AccessTokenMapper;
    @Resource
    private OAuth2RefreshTokenMapper oauth2RefreshTokenMapper;

    @Resource
    private OAuth2AccessTokenRedisDAO oauth2AccessTokenRedisDAO;

    @Resource
    private OAuth2ClientService oauth2ClientService;

    @Resource
    private FrameUserService frameUserService;

    @Override
    @Transactional
    public OAuth2AccessTokenDO createAccessToken(UserInfo user, Integer userType, String clientId, List<String> scopes) {
        OAuth2ClientDO clientDO = oauth2ClientService.validOAuthClientFromCache(clientId);
        // 创建刷新令牌
        FrameUser fu = user.getFrameUser();
        OAuth2RefreshTokenDO refreshTokenDO = createOAuth2RefreshToken(fu.getRowId(),fu.getRowGuid(),user,userType, clientDO, scopes);
        // 创建访问令牌
        return createOAuth2AccessToken(refreshTokenDO, clientDO);
    }

    @Override
    public OAuth2AccessTokenDO refreshAccessToken(String refreshToken, String clientId) {
        // 查询访问令牌
        OAuth2RefreshTokenDO refreshTokenDO = oauth2RefreshTokenMapper.selectByRefreshToken(refreshToken);
        if (refreshTokenDO == null) {
            throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), "无效的刷新令牌");
        }

        // 校验 Client 匹配
        OAuth2ClientDO clientDO = oauth2ClientService.validOAuthClientFromCache(clientId);
        if (ObjectUtil.notEqual(clientId, refreshTokenDO.getClientId())) {
            throw new BaseException(GlobalErrorCodeConstants.BAD_REQUEST.getCode(), "刷新令牌的客户端编号不正确");
        }

        // 移除相关的访问令牌
        List<OAuth2AccessTokenDO> accessTokenDOs = oauth2AccessTokenMapper.selectListByRefreshToken(refreshToken);
        if (CollUtil.isNotEmpty(accessTokenDOs)) {
            oauth2AccessTokenMapper.deleteBatchIds(CollectionUtils.convertSet(accessTokenDOs, OAuth2AccessTokenDO::getRowId));
            oauth2AccessTokenRedisDAO.deleteList(CollectionUtils.convertSet(accessTokenDOs, OAuth2AccessTokenDO::getAccessToken));
        }

        // 已过期的情况下，删除刷新令牌
        if (DateUtil.isExpired(refreshTokenDO.getExpiresTime())) {
            oauth2RefreshTokenMapper.deleteById(refreshTokenDO.getRowId());
            throw new BaseException(GlobalErrorCodeConstants.UNAUTHORIZED.getCode(), "刷新令牌已过期");
        }

        // 创建访问令牌
        return createOAuth2AccessToken(refreshTokenDO, clientDO);
    }

    @Override
    public OAuth2AccessTokenDO getAccessToken(String accessToken) {
        // 优先从 Redis 中获取
        OAuth2AccessTokenDO accessTokenDO = oauth2AccessTokenRedisDAO.get(accessToken);
        if (accessTokenDO != null) {
            return accessTokenDO;
        }

        // 获取不到，从 MySQL 中获取
        accessTokenDO = oauth2AccessTokenMapper.selectByAccessToken(accessToken);
        // 如果在 MySQL 存在，则往 Redis 中写入
        if (accessTokenDO != null && !DateUtil.isExpired(accessTokenDO.getExpiresTime())) {
            oauth2AccessTokenRedisDAO.set(accessTokenDO);
        }
        return accessTokenDO;
    }

    @Override
    public OAuth2AccessTokenDO checkAccessToken(String accessToken) {
        OAuth2AccessTokenDO accessTokenDO = getAccessToken(accessToken);
        if (accessTokenDO == null) {
            throw new BaseException(GlobalErrorCodeConstants.UNAUTHORIZED.getCode(), "访问令牌不存在");
        }
        if (DateUtil.isExpired(accessTokenDO.getExpiresTime())) {
            throw new BaseException(GlobalErrorCodeConstants.UNAUTHORIZED.getCode(), "访问令牌已过期");
        }
        return accessTokenDO;
    }

    @Override
    public OAuth2AccessTokenDO removeAccessToken(String accessToken) {
        // 删除访问令牌
        OAuth2AccessTokenDO accessTokenDO = oauth2AccessTokenMapper.selectByAccessToken(accessToken);
        if (accessTokenDO == null) {
            return null;
        }
        oauth2AccessTokenMapper.deleteById(accessTokenDO.getRowId());
        oauth2AccessTokenRedisDAO.delete(accessToken);
        // 删除刷新令牌
        oauth2RefreshTokenMapper.deleteByRefreshToken(accessTokenDO.getRefreshToken());
        return accessTokenDO;
    }

    @Override
    public void updateChcheUser(Long userId) {
        UserInfo user = frameUserService.loadUserByUserId(userId);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userId);
        queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
        OAuth2AccessTokenDO oAuth2AccessTokenDO = new OAuth2AccessTokenDO();
        oAuth2AccessTokenDO.setUser(user);
        oauth2AccessTokenMapper.update(oAuth2AccessTokenDO,queryWrapper);
    }

//    @Override
//    public Page<OAuth2AccessTokenDO> getAccessTokenPage(Page<OAuth2AccessTokenDO> reqVO) {
//
//        oauth2AccessTokenMapper.selectPage(reqVO,null);
//
//        return oauth2AccessTokenMapper.selectPage(reqVO);
//    }

    private OAuth2AccessTokenDO createOAuth2AccessToken(OAuth2RefreshTokenDO refreshTokenDO, OAuth2ClientDO clientDO) {
        OAuth2AccessTokenDO accessTokenDO = new OAuth2AccessTokenDO(true)
                .setUser(refreshTokenDO.getUser())
                .setUserGuid(refreshTokenDO.getUserGuid())
                .setAccessToken(generateAccessToken())
                .setUserId(refreshTokenDO.getUserId())
                .setUserType(refreshTokenDO.getUserType())
                .setClientId(clientDO.getClientId())
                .setScopes(refreshTokenDO.getScopes())
                .setRefreshToken(refreshTokenDO.getRefreshToken())
                .setExpiresTime(DateUtil.addDate(Calendar.SECOND, clientDO.getAccessTokenValiditySeconds()));
//        accessTokenDO.setTenantId(TenantContextHolder.getTenantId()); // 手动设置租户编号，避免缓存到 Redis 的时候，无对应的租户编号
        oauth2AccessTokenMapper.insert(accessTokenDO);
        // 记录到 Redis 中
        oauth2AccessTokenRedisDAO.set(accessTokenDO);
        return accessTokenDO;
    }

    private OAuth2RefreshTokenDO createOAuth2RefreshToken(Long userId,String userGuid,UserInfo user,Integer userType, OAuth2ClientDO clientDO, List<String> scopes) {
        OAuth2RefreshTokenDO refreshToken = new OAuth2RefreshTokenDO(true)
                .setUser(user)
                .setRefreshToken(generateRefreshToken())
                .setUserGuid(userGuid)
                .setUserId(userId)
                .setUserType(userType)
                .setClientId(clientDO.getClientId())
                .setScopes(scopes)
                .setExpiresTime(DateUtil.addDate(Calendar.SECOND, clientDO.getRefreshTokenValiditySeconds()));
        oauth2RefreshTokenMapper.insert(refreshToken);
        return refreshToken;
    }

    private static String generateAccessToken() {
        return IdUtil.fastSimpleUUID();
    }

    private static String generateRefreshToken() {
        return IdUtil.fastSimpleUUID();
    }

}
