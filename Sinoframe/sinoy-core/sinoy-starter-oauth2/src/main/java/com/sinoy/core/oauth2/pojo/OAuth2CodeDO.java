package com.sinoy.core.oauth2.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * OAuth2 授权码 DO
 */
@TableName(value = "frame_oauth2_code", autoResultMap = true)
//@KeySequence("system_oauth2_code_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class OAuth2CodeDO extends BaseEntity {
    public OAuth2CodeDO(){};

    public OAuth2CodeDO(boolean init){
        super(init);
    }
    /**
     * 授权码
     */
    private String code;
    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 用户行标
     */
    private String userGuid;
    /**
     * 用户类型
     *
     * 枚举 {@link UserTypeEnum}
     */
    private Integer userType;
    /**
     * 客户端编号
     *
     */
    private String clientId;
    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;
    /**
     * 重定向地址
     */
    private String redirectUri;
    /**
     * 状态
     */
    private String state;
    /**
     * 过期时间
     */
    private Date expiresTime;

    public String getUserGuid() {
        return userGuid;
    }

    public OAuth2CodeDO setUserGuid(String userGuid) {
        this.userGuid = userGuid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public OAuth2CodeDO setCode(String code) {
        this.code = code;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public OAuth2CodeDO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2CodeDO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2CodeDO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public OAuth2CodeDO setScopes(List<String> scopes) {
        this.scopes = scopes;
        return this;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public OAuth2CodeDO setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
        return this;
    }

    public String getState() {
        return state;
    }

    public OAuth2CodeDO setState(String state) {
        this.state = state;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2CodeDO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2CodeDO that = (OAuth2CodeDO) o;
        return Objects.equals(code, that.code) && Objects.equals(userId, that.userId) && Objects.equals(userType, that.userType) && Objects.equals(clientId, that.clientId) && Objects.equals(scopes, that.scopes) && Objects.equals(redirectUri, that.redirectUri) && Objects.equals(state, that.state) && Objects.equals(expiresTime, that.expiresTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, userId, userType, clientId, scopes, redirectUri, state, expiresTime);
    }
}
