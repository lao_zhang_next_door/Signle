package com.sinoy.core.oauth2.pojo.open;

/**
 * 授权码模式返回实体
 */
public class OAuth2OpenCodeAccessTokenRespVO {

    //访问令牌
    private String accessToken;

    //刷新令牌
    private String refreshToken;

    //令牌类型
    private String tokenType;

    //过期时间 单位：秒
    private Long expiresIn;

    //授权范围 如果多个授权范围，使用空格分隔
    private String scope;

    //用户行标
    private String userGuid;

    //用户行标
    private Long userId;

    //用户登录名
    private String loginId;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2OpenCodeAccessTokenRespVO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2OpenCodeAccessTokenRespVO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public String getTokenType() {
        return tokenType;
    }

    public OAuth2OpenCodeAccessTokenRespVO setTokenType(String tokenType) {
        this.tokenType = tokenType;
        return this;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public OAuth2OpenCodeAccessTokenRespVO setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
        return this;
    }

    public String getScope() {
        return scope;
    }

    public OAuth2OpenCodeAccessTokenRespVO setScope(String scope) {
        this.scope = scope;
        return this;
    }

    public OAuth2OpenCodeAccessTokenRespVO() {}

    public OAuth2OpenCodeAccessTokenRespVO(String accessToken, String refreshToken, String tokenType, Long expiresIn, String scope) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.scope = scope;
    }
}
