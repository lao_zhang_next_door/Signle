package com.sinoy.core.oauth2.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.oauth2.pojo.OAuth2CodeDO;

public interface OAuth2CodeMapper extends ExtendMapper<OAuth2CodeDO> {

    default OAuth2CodeDO selectByCode(String code) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("code",code);
        return selectOne(queryWrapper);
    }

}
