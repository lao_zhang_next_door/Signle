package com.sinoy.core.oauth2.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.core.common.constant.ErrorCodeConstants;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.oauth2.pojo.OAuth2AccessTokenDO;
import com.sinoy.core.oauth2.pojo.OAuth2CodeDO;
import com.sinoy.core.oauth2.service.OAuth2CodeService;
import com.sinoy.core.oauth2.service.OAuth2GrantService;
import com.sinoy.core.oauth2.service.OAuth2TokenService;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.entity.dto.UserInfo;
import com.sinoy.platform.system.component.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * OAuth2 授予 Service 实现类
 */
@Service
public class OAuth2GrantServiceImpl implements OAuth2GrantService {

    @Resource
    private OAuth2TokenService oauth2TokenService;
    @Resource
    private OAuth2CodeService oauth2CodeService;

    @Autowired
    private FrameUserService frameUserService;

    @Override
    public OAuth2AccessTokenDO grantImplicit(Long userId, String userGuid, Integer userType,
                                             String clientId, List<String> scopes) {
        UserInfo user = new UserInfo();
        FrameUser frameUser = (FrameUser) new FrameUser(userGuid).setRowId(userId);
        user.setFrameUser(frameUser);
        return oauth2TokenService.createAccessToken(user, userType, clientId, scopes);
    }

    @Override
    public String grantAuthorizationCodeForCode(Long userId, Integer userType,
                                                String clientId, List<String> scopes,
                                                String redirectUri, String state) {
        return oauth2CodeService.createAuthorizationCode(userId, userType, clientId, scopes,
                redirectUri, state).getCode();
    }

    @Override
    public OAuth2AccessTokenDO grantAuthorizationCodeForAccessToken(String clientId, String code,
                                                                    String redirectUri, String state) {
        OAuth2CodeDO codeDO = oauth2CodeService.consumeAuthorizationCode(code);
        Assert.notNull(codeDO, "授权码不能为空"); // 防御性编程
        // 校验 clientId 是否匹配
        if (!StrUtil.equals(clientId, codeDO.getClientId())) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_GRANT_CLIENT_ID_MISMATCH);
        }
        // 校验 redirectUri 是否匹配
        if (!StrUtil.equals(redirectUri, codeDO.getRedirectUri())) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_GRANT_REDIRECT_URI_MISMATCH);
        }
        // 校验 state 是否匹配
        state = StrUtil.nullToDefault(state, ""); // 数据库 state 为 null 时，会设置为 "" 空串
        if (!StrUtil.equals(state, codeDO.getState())) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_GRANT_STATE_MISMATCH);
        }

        // 创建访问令牌
        if(codeDO.getUserId() == null){
            throw new BaseException("获取用户信息异常");
        }
        UserInfo user = new UserInfo();
        FrameUser frameUser = frameUserService.getOne(new LambdaQueryWrapper<FrameUser>().eq(FrameUser::getDelFlag, DelFlag.Normal.getCode()).eq(FrameUser::getRowId,codeDO.getUserId()));
        user.setFrameUser(frameUser);
        return oauth2TokenService.createAccessToken(user,codeDO.getUserType(),codeDO.getClientId(), codeDO.getScopes());
    }

//    /**
//     * 根据配置 scope 获取对应用户信息
//     * 对应规则   先暂时手动填写
//     * @return
//     */
//    private UserInfo getUserByScope(OAuth2CodeDO codeDO){
//
//
//        return user;
//
//    }

    @Override
    public OAuth2AccessTokenDO grantPassword(String loginId, String password, String clientId, List<String> scopes) {
        // 使用账号 + 密码进行登录
        UserInfo userInfo = frameUserService.loadUserByLoginId(loginId);
        Assert.notNull(userInfo, "用户不能为空！"); // 防御性编程

        // 创建访问令牌
        return oauth2TokenService.createAccessToken(userInfo, UserTypeEnum.ADMIN.getValue(), clientId, scopes);
    }

    @Override
    public OAuth2AccessTokenDO grantRefreshToken(String refreshToken, String clientId) {
        return oauth2TokenService.refreshAccessToken(refreshToken, clientId);
    }

    @Override
    public OAuth2AccessTokenDO grantClientCredentials(String clientId, List<String> scopes) {
        // TODO 芋艿：项目中使用 OAuth2 解决的是三方应用的授权，内部的 SSO 等问题，所以暂时不考虑 client_credentials 这个场景
        throw new UnsupportedOperationException("暂时不支持 client_credentials 授权模式");
    }

    @Override
    public boolean revokeToken(String clientId, String accessToken) {
        // 先查询，保证 clientId 时匹配的
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenService.getAccessToken(accessToken);
        if (accessTokenDO == null || ObjectUtil.notEqual(clientId, accessTokenDO.getClientId())) {
            return false;
        }
        // 再删除
        return oauth2TokenService.removeAccessToken(accessToken) != null;
    }

}
