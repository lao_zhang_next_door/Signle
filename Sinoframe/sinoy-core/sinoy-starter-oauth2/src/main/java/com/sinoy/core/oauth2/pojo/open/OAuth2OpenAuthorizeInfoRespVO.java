package com.sinoy.core.oauth2.pojo.open;

import com.sinoy.core.common.entity.KeyValue;

import java.util.List;

public class OAuth2OpenAuthorizeInfoRespVO {

    /**
     * 客户端
     */
    private Client client;

    //scope 的选中信息   使用 List 保证有序性，Key 是 scope，Value 为是否选中
    private List<KeyValue<String, Boolean>> scopes;

    public static class Client {

        //应用名
        private String name;

        //应用图标
        private String logo;

        public Client(String name, String logo) {
            this.name = name;
            this.logo = logo;
        }

        public Client(){}

        public String getName() {
            return name;
        }

        public Client setName(String name) {
            this.name = name;
            return this;
        }

        public String getLogo() {
            return logo;
        }

        public Client setLogo(String logo) {
            this.logo = logo;
            return this;
        }
    }

    public Client getClient() {
        return client;
    }

    public OAuth2OpenAuthorizeInfoRespVO setClient(Client client) {
        this.client = client;
        return this;
    }

    public List<KeyValue<String, Boolean>> getScopes() {
        return scopes;
    }

    public OAuth2OpenAuthorizeInfoRespVO setScopes(List<KeyValue<String, Boolean>> scopes) {
        this.scopes = scopes;
        return this;
    }

    public OAuth2OpenAuthorizeInfoRespVO(Client client, List<KeyValue<String, Boolean>> scopes) {
        this.client = client;
        this.scopes = scopes;
    }
}
