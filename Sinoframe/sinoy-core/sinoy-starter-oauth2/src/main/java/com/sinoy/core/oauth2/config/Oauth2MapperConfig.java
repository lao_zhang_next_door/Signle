package com.sinoy.core.oauth2.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.sinoy.core.oauth2.dao")
@Configuration
public class Oauth2MapperConfig {
}
