package com.sinoy.core.oauth2.pojo.login;

import java.util.Date;

public class AuthLoginRespVO {

    //用户编号
    private Long userId;

    //访问令牌
    private String accessToken;

    //刷新令牌
    private String refreshToken;

    //过期时间
    private Date expiresTime;

    public AuthLoginRespVO(Long userId, String accessToken, String refreshToken, Date expiresTime) {
        this.userId = userId;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresTime = expiresTime;
    }

    public AuthLoginRespVO() {
    }

    public Long getUserId() {
        return userId;
    }

    public AuthLoginRespVO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public AuthLoginRespVO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public AuthLoginRespVO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public AuthLoginRespVO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }


    public static final class AuthLoginRespVOBuilder {
        private Long userId;
        private String accessToken;
        private String refreshToken;
        private Date expiresTime;

        private AuthLoginRespVOBuilder() {
        }

        public static AuthLoginRespVOBuilder anAuthLoginRespVO() {
            return new AuthLoginRespVOBuilder();
        }

        public AuthLoginRespVOBuilder withUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public AuthLoginRespVOBuilder withAccessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public AuthLoginRespVOBuilder withRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        public AuthLoginRespVOBuilder withExpiresTime(Date expiresTime) {
            this.expiresTime = expiresTime;
            return this;
        }

        public AuthLoginRespVO build() {
            return new AuthLoginRespVO(userId, accessToken, refreshToken, expiresTime);
        }
    }
}
