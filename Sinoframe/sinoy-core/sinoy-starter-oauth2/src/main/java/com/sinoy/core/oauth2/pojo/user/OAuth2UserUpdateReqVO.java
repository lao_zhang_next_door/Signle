package com.sinoy.core.oauth2.pojo.user;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class OAuth2UserUpdateReqVO {

    //用户昵称
    @Size(max = 30, message = "用户昵称长度不能超过 30 个字符")
    private String nickname;

    //用户邮箱
    @Email(message = "邮箱格式不正确")
    @Size(max = 50, message = "邮箱长度不能超过 50 个字符")
    private String email;

    //手机号码
    @Length(min = 11, max = 11, message = "手机号长度必须 11 位")
    private String mobile;

    //用户性别
    private Integer sex;

    public String getNickname() {
        return nickname;
    }

    public OAuth2UserUpdateReqVO setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public OAuth2UserUpdateReqVO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public OAuth2UserUpdateReqVO setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public Integer getSex() {
        return sex;
    }

    public OAuth2UserUpdateReqVO setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public OAuth2UserUpdateReqVO(String nickname, String email, String mobile, Integer sex) {
        this.nickname = nickname;
        this.email = email;
        this.mobile = mobile;
        this.sex = sex;
    }
}
