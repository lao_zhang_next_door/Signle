package com.sinoy.core.oauth2.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.annotations.VisibleForTesting;
import com.sinoy.core.common.constant.ErrorCodeConstants;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.dateformat.DateUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.core.oauth2.dao.OAuth2ClientMapper;
import com.sinoy.core.oauth2.maps.OAuth2ClientConvert;
import com.sinoy.core.oauth2.mq.producer.OAuth2ClientProducer;
import com.sinoy.core.oauth2.pojo.OAuth2ClientDO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientCreateReqVO;
import com.sinoy.core.oauth2.pojo.client.OAuth2ClientUpdateReqVO;
import com.sinoy.core.oauth2.service.OAuth2ClientService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * OAuth2.0 Client Service 实现类
 */
@Service
@Validated
public class OAuth2ClientServiceImpl extends BaseServiceImpl<OAuth2ClientMapper, OAuth2ClientDO> implements OAuth2ClientService {

    @Resource
    private OAuth2ClientProducer oauth2ClientProducer;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 定时执行 {@link #schedulePeriodicRefresh()} 的周期
     * 因为已经通过 Redis Pub/Sub 机制，所以频率不需要高
     */
    private static final long SCHEDULER_PERIOD = 5 * 60 * 1000L;

    /**
     * 客户端缓存
     * key：客户端编号 {@link OAuth2ClientDO#getClientId()} ()}
     * <p>
     * 这里声明 volatile 修饰的原因是，每次刷新时，直接修改指向
     */
    private volatile Map<String, OAuth2ClientDO> clientCache;
    /**
     * 缓存角色的最大更新时间，用于后续的增量轮询，判断是否有更新
     */
    private volatile Date maxUpdateTime;

    @Resource
    private OAuth2ClientMapper oauth2ClientMapper;

    /**
     * 初始化 {@link #clientCache} 缓存
     */
    @Override
    @PostConstruct
    public void initLocalCache() {
        // 获取客户端列表，如果有更新
        List<OAuth2ClientDO> tenantList = loadOAuth2ClientIfUpdate(maxUpdateTime);
        if (CollUtil.isEmpty(tenantList)) {
            return;
        }

        // 写入缓存
        clientCache = CollectionUtils.convertMap(tenantList, OAuth2ClientDO::getClientId);
        maxUpdateTime = DateUtil.toDate(CollectionUtils.getMaxValue(tenantList, OAuth2ClientDO::getUpdateTime));
        log.info("[initLocalCache][初始化 OAuth2Client 数量为 {}]", tenantList.size());
    }

    @Scheduled(fixedDelay = SCHEDULER_PERIOD, initialDelay = SCHEDULER_PERIOD)
    public void schedulePeriodicRefresh() {
        initLocalCache();
    }

    /**
     * 如果客户端发生变化，从数据库中获取最新的全量客户端。
     * 如果未发生变化，则返回空
     *
     * @param maxUpdateTime 当前客户端的最大更新时间
     * @return 客户端列表
     */
    private List<OAuth2ClientDO> loadOAuth2ClientIfUpdate(Date maxUpdateTime) {
        // 第一步，判断是否要更新。
        if (maxUpdateTime == null) { // 如果更新时间为空，说明 DB 一定有新数据
            log.info("[loadOAuth2ClientIfUpdate][首次加载全量客户端]");
        } else { // 判断数据库中是否有更新的客户端
            if (oauth2ClientMapper.selectCountByUpdateTimeGt(maxUpdateTime) == 0) {
                return null;
            }
            log.info("[loadOAuth2ClientIfUpdate][增量加载全量客户端]");
        }
        // 第二步，如果有更新，则从数据库加载所有客户端
        return oauth2ClientMapper.selectList(new QueryWrapper());
    }

    @Override
    public Long createOAuth2Client(OAuth2ClientCreateReqVO createReqVO) {
        validateClientIdExists(null, createReqVO.getClientId());
        // 插入
        OAuth2ClientDO oauth2Client = OAuth2ClientConvert.INSTANCE.convert(createReqVO);
        oauth2Client.initNull();
        oauth2ClientMapper.insert(oauth2Client);
        // 发送刷新消息
        oauth2ClientProducer.sendOAuth2ClientRefreshMessage();
        return oauth2Client.getRowId();
    }

    @Override
    public void updateOAuth2Client(OAuth2ClientUpdateReqVO updateReqVO) {
        // 校验存在
        validateOAuth2ClientExists(updateReqVO.getRowId());
        // 校验 Client 未被占用
        validateClientIdExists(updateReqVO.getRowId(), updateReqVO.getClientId());

        // 更新
        OAuth2ClientDO updateObj = OAuth2ClientConvert.INSTANCE.convert(updateReqVO);
        updateObj.setUpdateTime(LocalDateTime.now());
        oauth2ClientMapper.updateById(updateObj);
        // 发送刷新消息
        oauth2ClientProducer.sendOAuth2ClientRefreshMessage();
    }

    @Override
    public void deleteOAuth2Client(Long id) {
        // 校验存在
        validateOAuth2ClientExists(id);
        // 删除
        oauth2ClientMapper.deleteById(id);
        // 发送刷新消息
        oauth2ClientProducer.sendOAuth2ClientRefreshMessage();
    }

    private void validateOAuth2ClientExists(Long id) {
        if (oauth2ClientMapper.selectById(id) == null) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_NOT_EXISTS);
        }
    }

    @VisibleForTesting
    void validateClientIdExists(Long id, String clientId) {
        OAuth2ClientDO client = oauth2ClientMapper.selectByClientId(clientId);
        if (client == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的客户端
        if (id == null) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_EXISTS);
        }
        if (!client.getRowId().equals(id)) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_EXISTS);
        }
    }

    @Override
    public OAuth2ClientDO getOAuth2Client(Long id) {
        return oauth2ClientMapper.selectById(id);
    }

    @Override
    public OAuth2ClientDO validOAuthClientFromCache(String clientId, String clientSecret,
                                                    String authorizedGrantType, Collection<String> scopes, String redirectUri) {
        // 校验客户端存在、且开启
        OAuth2ClientDO client = clientCache.get(clientId);
        if (client == null) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_NOT_EXISTS);
        }
        if (ObjectUtil.notEqual(client.getStatus(), CommonStatusEnum.ENABLE.getStatus())) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_DISABLE);
        }
        // 校验客户端密钥
        if (StrUtil.isNotEmpty(clientSecret) && ObjectUtil.notEqual(client.getSecret(), clientSecret)) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_CLIENT_SECRET_ERROR);
        }
        // 校验授权方式
        if (StrUtil.isNotEmpty(authorizedGrantType) && !CollUtil.contains(client.getAuthorizedGrantTypes(), authorizedGrantType)) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_AUTHORIZED_GRANT_TYPE_NOT_EXISTS);
        }
        // 校验授权范围
        if (CollUtil.isNotEmpty(scopes) && !CollUtil.containsAll(client.getScopes(), scopes)) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_SCOPE_OVER);
        }
        // 校验回调地址
        if (StrUtil.isNotEmpty(redirectUri) && !StringUtils.startsWithAny(redirectUri, client.getRedirectUris().toArray(new String[client.getRedirectUris().size()]))) {
            throw new BaseException(ErrorCodeConstants.OAUTH2_CLIENT_REDIRECT_URI_NOT_MATCH, redirectUri);
        }
        return client;
    }

    public Map<String, OAuth2ClientDO> getClientCache() {
        return clientCache;
    }

    public OAuth2ClientServiceImpl setClientCache(Map<String, OAuth2ClientDO> clientCache) {
        this.clientCache = clientCache;
        return this;
    }

    public Date getMaxUpdateTime() {
        return maxUpdateTime;
    }

    public OAuth2ClientServiceImpl setMaxUpdateTime(Date maxUpdateTime) {
        this.maxUpdateTime = maxUpdateTime;
        return this;
    }
}
