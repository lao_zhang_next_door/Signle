package com.sinoy.core.oauth2.pojo.token;

import java.util.Date;

public class OAuth2AccessTokenRespVO {

    //编号
    private Long id;

    //访问令牌
    private String accessToken;

    //刷新令牌
    private String refreshToken;

    //用户编号
    private Long userId;

    //用户类型
    private Integer userType;

    //客户端编号
    private String clientId;

    //创建时间
    private Date createTime;

    //过期时间
    private Date expiresTime;

    public Long getId() {
        return id;
    }

    public OAuth2AccessTokenRespVO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public OAuth2AccessTokenRespVO setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2AccessTokenRespVO setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public OAuth2AccessTokenRespVO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2AccessTokenRespVO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2AccessTokenRespVO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public OAuth2AccessTokenRespVO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getExpiresTime() {
        return expiresTime;
    }

    public OAuth2AccessTokenRespVO setExpiresTime(Date expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }

    public OAuth2AccessTokenRespVO() {
    }

    public OAuth2AccessTokenRespVO(Long id, String accessToken, String refreshToken, Long userId, Integer userType, String clientId, Date createTime, Date expiresTime) {
        this.id = id;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.userId = userId;
        this.userType = userType;
        this.clientId = clientId;
        this.createTime = createTime;
        this.expiresTime = expiresTime;
    }
}
