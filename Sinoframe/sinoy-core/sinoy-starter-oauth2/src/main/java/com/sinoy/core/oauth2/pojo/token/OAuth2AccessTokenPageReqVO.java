package com.sinoy.core.oauth2.pojo.token;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Objects;

public class OAuth2AccessTokenPageReqVO extends Page {

    //用户编号
    private Long userId;

    //用户类型   参见 UserTypeEnum 枚举
    private Integer userType;

    //客户端编号
    private String clientId;

    public Long getUserId() {
        return userId;
    }

    public OAuth2AccessTokenPageReqVO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public OAuth2AccessTokenPageReqVO setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2AccessTokenPageReqVO setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OAuth2AccessTokenPageReqVO that = (OAuth2AccessTokenPageReqVO) o;
        return Objects.equals(userId, that.userId) && Objects.equals(userType, that.userType) && Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userType, clientId);
    }
}
