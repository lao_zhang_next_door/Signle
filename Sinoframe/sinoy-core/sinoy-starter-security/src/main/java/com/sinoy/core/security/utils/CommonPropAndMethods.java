package com.sinoy.core.security.utils;

import com.sinoy.core.common.enums.UserTypeEnum;
import com.sinoy.core.security.userdetails.XnwUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通用属性和方法
 */
@Component
public class CommonPropAndMethods {

    @Autowired
    public HttpSession session;
    @Autowired
    public HttpServletRequest request;
    @Autowired
    public HttpServletResponse response;
    @Value("${xnw.upload.path}")
    public String filePath;
    @Value("${xnw.upload.url}")
    public String localFileUrl;
//    @Autowired
//    private TokenStore tokenStore;

    public static final String AUTHORIZATION_BEARER = "Bearer";

    public XnwUser getXnwUser(){
        //勿删除  后续使用auth 可能还需要用到
//        String headerToken = request.getHeader(Oauth2Constant.HEADER_TOKEN);
//        String token = TokenUtil.getToken(headerToken);
//        OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
//        OAuth2Authentication authentication = tokenStore.readAuthentication(accessToken);
//        Object principal = authentication.getPrincipal();
//        XnwUser u = (XnwUser) principal;
//        return u;
        return SecurityFrameworkUtils.getLoginUser();
    }

    public Map getCurrentUser(){
        XnwUser u = getXnwUser();
        Map<String,String> res = new HashMap(){
            {
                put("userId",u.getId());
                put("loginId",u.getUsername());
                put("userName",u.getNickName());
                put("userGuid",u.getRowGuid());
                put("deptGuid",u.getDeptGuid());
                put("deptId",u.getDeptId());
                put("deptName",u.getDeptName());
                put("mobile",u.getMobile());
                put("roleNameList",u.getRoleNameList());
                put("roleGuidList",u.getRoleGuidList());
                put("pdeptGuid",u.getParentDeptGuid());
                put("pdeptName",u.getParentDeptName());
                put("deptCode",u.getDeptCode());
                put("permissions",u.getAuthorities());
                put("avatar",u.getAvatar());
            }
        };
        return res;
    }

    /**
     * 获取当前用户名昵称
     * @return
     */
    public String getUserName(){
        XnwUser u = getXnwUser();
        return u == null ? "" : u.getNickName();
    }

    /**
     * 获取当前用户账户名
     * @return
     */
    public String getLoginId(){
        XnwUser u = getXnwUser();
        return u == null ? "" : u.getUsername();
    }

    public String getUserGuid(){
        XnwUser u = getXnwUser();
        return u == null ? "" : u.getRowGuid();
    }

    public Long getUserId(){
        XnwUser u = getXnwUser();
        return u == null ? 0L : u.getId();
    }

    /**
     * 获取当前用户角色列
     */
    public List<String> getCurrentRoleGuid() {
        XnwUser u = getXnwUser();
        return u == null ? new ArrayList() : u.getRoleGuidList();
    }

    /**
     * 获取当前用户数据权限
     */
    public List<String> getCurrentDataScope(){
        XnwUser u = getXnwUser();
        return u == null ? new ArrayList() : u.getDataScope();
    }

    /**
     * 获取当前用户部门guid
     * @return
     */
    public String getCurrentDeptGuid(){
        XnwUser u = getXnwUser();
        return u == null ? "" : u.getDeptGuid();
    }

    /**
     * 获得当前用户的类型
     * 注意：该方法仅限于 web 相关的 framework 组件使用！！！
     *
     * @param request 请求
     * @return 用户编号
     */
    public Integer getLoginUserType(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        // 1. 优先，从 Attribute 中获取
        Integer userType = (Integer) request.getAttribute("login_user_type");
        if (userType != null) {
            return userType;
        }
        // 2. 其次，基于 URL 前缀的约定
        if (request.getRequestURI().startsWith("/sino-api")) {
            return UserTypeEnum.ADMIN.getValue();
        }
        if (request.getRequestURI().startsWith("/app-api")) {
            return UserTypeEnum.MEMBER.getValue();
        }
        if (request.getRequestURI().startsWith("/external-api")) {
            return UserTypeEnum.EXTERNAL.getValue();
        }
        return null;
    }
}
