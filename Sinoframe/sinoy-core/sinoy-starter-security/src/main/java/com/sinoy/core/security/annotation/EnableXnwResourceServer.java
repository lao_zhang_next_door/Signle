//package com.sino.core.security.annotation;
//
//import com.sino.core.security.config.XnwResourceServerConfig;
//import org.springframework.context.annotation.Import;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
///**
// * 资源服务注解
// * @Import :1.导入配置类, 2.导入普通类(4.2之前只支持导入配置类)
// * 可以同时导入多个配置类，比如：@Import({A.class,B.class}
// * @EnableXxx 注解是用在 @Configuration 标记的类上，用来自动配置和加载一些 @Configuration 类或者 bean 的。 所以，@EnableXxx 一定是与 @Configuration 一起使用的。
// * @EnableXxx 一定会与 @Import 一起使用，通过 @Import 来达到自动配置的目的。
// * @author pangu
// */
//@Target({ElementType.TYPE})
//@Retention(RetentionPolicy.RUNTIME)
//@Import(XnwResourceServerConfig.class)
//public @interface EnableXnwResourceServer {
//}
