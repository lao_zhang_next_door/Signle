package com.sinoy.core.security.config;

import cn.hutool.core.util.ReUtil;
import com.sinoy.core.security.annotation.PassToken;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 忽略URL属性配置
 *
 * @author hero
 */
@RefreshScope
@ConfigurationProperties(prefix = "xnw.security.ignore")
@Component
@Order
public class IgnoreUrlPropsConfiguration implements InitializingBean{

	@Autowired
	private WebApplicationContext applicationContext;

	/**
	 * 认证中心默认忽略验证地址
	 */
	private static final String[] SECURITY_ENDPOINTS = {
//			"/auth/**",
//			"/oauth/token",
//			"/login/*",
//			"/actuator/**",
//			"/v2/api-docs",
//			"/doc.html",
//			"/webjars/**",
//			"**/favicon.ico",
//			"/swagger-resources/**"
	};


	private List<String> urls = new ArrayList<>();

	private List<String> ignoreSecurity = new ArrayList<>();

	private List<String> client = new ArrayList<>();

	/**
	 * 首次加载合并ENDPOINTS
	 */
	@PostConstruct
	public void initIgnoreSecurity() {
		Collections.addAll(ignoreSecurity, SECURITY_ENDPOINTS);
	}

	public static String[] getSecurityEndpoints() {
		return SECURITY_ENDPOINTS;
	}

	public List<String> getUrls() {
		return urls;
	}

	public IgnoreUrlPropsConfiguration setUrls(List<String> urls) {
		this.urls = urls;
		return this;
	}

	public List<String> getClient() {
		return client;
	}

	public IgnoreUrlPropsConfiguration setClient(List<String> client) {
		this.client = client;
		return this;
	}

	public List<String> getIgnoreSecurity() {
		return ignoreSecurity;
	}

	public IgnoreUrlPropsConfiguration setIgnoreSecurity(List<String> ignoreSecurity) {
		this.ignoreSecurity = ignoreSecurity;
		return this;
	}

	private static final Pattern PATTERN = Pattern.compile("\\{(.*?)\\}");
	private static final String ASTERISK = "*";

	@Override
	public void afterPropertiesSet() throws Exception {
		RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
		Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();

		map.keySet().forEach(mappingInfo -> {
			HandlerMethod handlerMethod = map.get(mappingInfo);
			PassToken method = AnnotationUtils.findAnnotation(handlerMethod.getMethod(), PassToken.class);
			Optional.ofNullable(method)
					.ifPresent(authIgnore -> mappingInfo
							.getPatternsCondition()
							.getPatterns()
							.forEach(url -> ignoreSecurity.add(ReUtil.replaceAll(url, PATTERN, ASTERISK))));
			PassToken controller = AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), PassToken.class);
			Optional.ofNullable(controller)
					.ifPresent(authIgnore -> mappingInfo
							.getPatternsCondition()
							.getPatterns()
							.forEach(url -> ignoreSecurity.add(ReUtil.replaceAll(url, PATTERN, ASTERISK))));
		});
	}
}
