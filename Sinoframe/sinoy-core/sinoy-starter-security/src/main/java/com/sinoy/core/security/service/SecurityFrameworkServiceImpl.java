package com.sinoy.core.security.service;

import cn.hutool.core.collection.CollUtil;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public class SecurityFrameworkServiceImpl implements SecurityFrameworkService {

    private final PermissionApi permissionApi;

    private final CommonPropAndMethods commonPropAndMethods;

    @Override
    public boolean hasPermission(String permission) {
        return hasAnyPermissions(permission);
    }

    @Override
    public boolean hasAnyPermissions(String... permissions) {
        return permissionApi.hasAnyPermissions(permissions);
    }

    @Override
    public boolean hasRole(String role) {
        return hasAnyRoles(role);
    }

    @Override
    public boolean hasAnyRoles(String... roles) {
        return permissionApi.hasAnyRoles(roles);
    }

    @Override
    public boolean hasScope(String scope) {
        return hasAnyScopes(scope);
    }

    @Override
    public boolean hasAnyScopes(String... scope) {
        XnwUser user = SecurityFrameworkUtils.getLoginUser();
        if (user == null) {
            return false;
        }
        return CollUtil.containsAny(user.getScopes(), Arrays.asList(scope));
    }

}
