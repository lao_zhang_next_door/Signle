package com.sinoy.core.security.handle;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.request.ResponseUtil;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class XnwAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
		int status = HttpServletResponse.SC_UNAUTHORIZED;
		ResponseUtil.responseWriter(response, MediaType.APPLICATION_JSON_VALUE, status, R.error(status, "访问令牌不合法"));
	}
}
