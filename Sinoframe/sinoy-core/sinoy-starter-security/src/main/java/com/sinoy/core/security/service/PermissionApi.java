package com.sinoy.core.security.service;

import java.util.Collection;
import java.util.Set;

/**
 * 权限 API 接口
 *
 */
public interface PermissionApi {

    /**
     * 判断是否有权限，任一一个即可
     *
     * @param permissions 权限
     * @return 是否
     */
    boolean hasAnyPermissions(String... permissions);

    /**
     * 判断是否有角色，任一一个即可
     *
     * @param roles 角色数组
     * @return 是否
     */
    boolean hasAnyRoles(String... roles);
}
