//package com.sino.core.security.service;
//
//import com.alibaba.fastjson2.JSONObject;
//import com.sino.core.common.constant.Oauth2Constant;
//import com.sino.core.common.utils.auth.TokenUtil;
//import com.sino.core.common.utils.request.R;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.stereotype.Service;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Service
//public class CheckTokenService  {
//
//    @Autowired
//    private TokenStore tokenStore;
//
//    /**
//     * 验证token是否合法  可以不加前缀 Bearer
//     * @param headerToken
//     * @return
//     */
//    public boolean validToken(String headerToken){
//        try{
//            String token = TokenUtil.getToken(headerToken);
//            OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
//            // 读取认证信息
//            OAuth2Authentication authentication = tokenStore.readAuthentication(accessToken);
//            return true;
//        }catch (Exception exception){
//            return false;
//        }
//    }
//
//
//    public boolean validToken(HttpServletRequest request, HttpServletResponse response){
//        String token = request.getHeader(Oauth2Constant.HEADER_TOKEN);
//        boolean isValid = validToken(token);
//        if(!isValid){
//            try {
//                response.setCharacterEncoding("UTF-8");
//                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//                response.getWriter().print(JSONObject.toJSONString(R.error(401, "用户token验证失败！")));
//            }
//            catch (Exception ignored){}
//        }
//        return isValid;
//    }
//}
