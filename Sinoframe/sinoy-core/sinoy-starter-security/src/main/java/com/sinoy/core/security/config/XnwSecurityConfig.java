package com.sinoy.core.security.config;//package com.sino.core.security.config;
//
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
//import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;
//
///**
// * 安全配置
// * 当我们想要开启spring方法级安全时，只需要在任何 @Configuration实例上使用 @EnableGlobalMethodSecurity 注解就能达到此目的。
// * 同时这个注解为我们提供了prePostEnabled 、securedEnabled 和 jsr250Enabled 三种不同的机制来实现同一种功能：
// * prePostEnabled = true 会解锁 @PreAuthorize 和 @PostAuthorize 两个注解。
// * 从名字就可以看出@PreAuthorize 注解会在方法执行前进行验证，而 @PostAuthorize 注解会在方法执行后进行验证。
// *
// * example:
// * 限制只能查询自己的信息
// * @PreAuthorize("principal.username.equals(#username)")
// * 限制只能新增用户名称为abc的用户
// * @PreAuthorize("#user.name.equals('abc')")
// * 限制权限访问 若是角色 请前缀+
// * 注意 hasRole 这里会自动给传入的字符串加上 ROLE_ 前缀，所以在数据库中的权限字符串需要加上 ROLE_ 前缀。
// * 即数据库中存储的用户角色如果是 ROLE_admin，这里就是 admin。
// * 我们在调用 hasAuthority 方法时，如果数据是从数据库中查询出来的，这里的权限和数据库中保存一致即可，可以不加 ROLE_ 前缀。
// * 即数据库中存储的用户角色如果是 admin，这里就是 admin。
// * @PreAuthorize("hasRole('ROLE_USER') or hasAuthority('ADMIN')")
// * 限制只能查询Id小于10的用户
// * @PreAuthorize("#id<10")
// *
// *
// * 上面这一段代码表示将在方法find()调用完成后进行权限检查，如果返回值的id是偶数则表示校验通过，否则表示校验失败，将抛出AccessDeniedException。
// * 需要注意的是@PostAuthorize是在方法调用完成后进行权限检查，它不能控制方法是否能被调用，只能在方法调用完成后检查权限决定是否要抛出AccessDeniedException。
// * @PostAuthorize("returnObject.id%2==0")
// *
// * 使用@PreFilter和@PostFilter可以对集合类型的参数或返回值进行过滤。使用@PreFilter和@PostFilter时，
// * Spring Security将移除使对应表达式的结果为false的元素。
// * 将对返回结果中id不为偶数的user进行移除
// * @PostFilter("filterObject.id%2==0"）
// *
// * 当@PreFilter标注的方法拥有多个集合类型的参数时，需要通过@PreFilter的filterTarget属性指定当前@PreFilter是针对哪个参数进行过滤的。
// * @PreFilter(filterTarget="ids", value="filterObject%2==0")
// * public void delete(List<Integer> ids, List<String> usernames) {}
// *
// * @ConditionalOnProperty
// * 匹配规则：属性文件中查找key（prefix+"."+name）
// * 属性文件中设置了key的值value：
// * ConditionalOnProperty设置了havingValue：value=havingvalue则匹配，若不等则不匹配
// * ConditionalOnproperty没有设置havingValue：value不等于false则匹配，若为false，则不匹配
// * 属性文件中没有设置key：
// * ConditionalOnProperty中matchingIfMissing：true匹配，false不匹配
// *
// */
//@Order(6)
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//@ConditionalOnProperty(value = "xnw.security.enable", havingValue = "true", matchIfMissing = true)
//public class XnwSecurityConfig extends GlobalMethodSecurityConfiguration {
//
//	@Override
//	protected MethodSecurityExpressionHandler createExpressionHandler() {
//		return new OAuth2MethodSecurityExpressionHandler();
//	}
//}
