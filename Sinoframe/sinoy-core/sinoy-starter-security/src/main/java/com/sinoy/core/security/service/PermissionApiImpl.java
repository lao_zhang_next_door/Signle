package com.sinoy.core.security.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Set;

/**
 * 权限 API 实现类
 */
@Service
public class PermissionApiImpl implements PermissionApi {

    @Resource
    private PermissionService permissionService;

    @Override
    public boolean hasAnyPermissions(String... permissions) {
        return permissionService.hasAnyPermissions(permissions);
    }

    @Override
    public boolean hasAnyRoles(String... roles) {
        return permissionService.hasAnyRoles(roles);
    }
}
