package com.sinoy.core.security.annotation;

import java.lang.annotation.*;

/**
 * 是否跳过token验证
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PassToken {
    boolean required() default true;
}
