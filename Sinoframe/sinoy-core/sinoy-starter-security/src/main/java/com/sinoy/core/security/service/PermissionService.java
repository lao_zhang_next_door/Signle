package com.sinoy.core.security.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.SecurityFrameworkUtils;
import org.apache.commons.compress.utils.Sets;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.PatternMatchUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Set;

import static com.sinoy.core.common.utils.dataformat.collection.CollectionUtils.convertSet;

/**
 * 权限判断工具类
 *
 * @author hero
 */
@Service(value = "mt")
public class PermissionService{

	/**
	 * 判断是否拥有某个权限
	 * @param permissions
	 * @return
	 */
	public boolean hasPerm(String... permissions) {
		if (permissions.length == 0) {
			return false;
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return false;
		}
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		return authorities.stream()
				.map(GrantedAuthority::getAuthority)
				.filter(StringUtils::hasText)
				.anyMatch(x -> PatternMatchUtils.simpleMatch(permissions, x));
	}

	/**
	 * 是否有某个权限  若是管理员 则直接通过
	 * @param permissions
	 * @return
	 */
	public boolean hasAnyPermissions(String... permissions) {
		// 如果为空，说明已经有权限
		if (ArrayUtil.isEmpty(permissions)) {
			return false;
		}

		//如果是管理员则通过
		XnwUser user = SecurityFrameworkUtils.getLoginUser();
		if(user == null){
			return false;
		}
		if(user.getIsAdmin()){
			return true;
		}

		//遍历 比对
		Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
		return authorities.stream()
				.map(GrantedAuthority::getAuthority)
				.filter(StringUtils::hasText)
				.anyMatch(x -> PatternMatchUtils.simpleMatch(permissions, x));
	}

	/**
	 * 是否具有某个角色  若是管理员 则直接通过
	 * @param roles
	 * @return
	 */
	public boolean hasAnyRoles(String... roles) {
		// 如果为空，说明已经有权限
		if (ArrayUtil.isEmpty(roles)) {
			return true;
		}

		//如果是管理员则通过 当前用户无角色则不通过
		XnwUser user = SecurityFrameworkUtils.getLoginUser();
		if(user == null){
			return false;
		}
		if(user.getRoleNameList().isEmpty()){
			return false;
		}
		if(user.getIsAdmin()){
			return true;
		}
		return CollUtil.containsAny(user.getRoleNameList(), Sets.newHashSet(roles));
	}
}
