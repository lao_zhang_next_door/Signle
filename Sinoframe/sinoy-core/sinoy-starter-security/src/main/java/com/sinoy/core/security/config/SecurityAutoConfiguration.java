package com.sinoy.core.security.config;

import com.sinoy.core.security.service.PermissionApi;
import com.sinoy.core.security.service.SecurityFrameworkService;
import com.sinoy.core.security.service.SecurityFrameworkServiceImpl;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置类
 */
@Configuration(proxyBeanMethods = false)
public class SecurityAutoConfiguration {

    @Autowired
    private CommonPropAndMethods commonPropAndMethods;

    @Bean("ss") // 使用 Spring Security 的缩写，方便使用
    public SecurityFrameworkService securityFrameworkService(PermissionApi permissionApi) {
        return new SecurityFrameworkServiceImpl(permissionApi,commonPropAndMethods);
    }

}
