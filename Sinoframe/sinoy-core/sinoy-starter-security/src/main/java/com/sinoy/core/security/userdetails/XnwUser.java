package com.sinoy.core.security.userdetails;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.List;

/**
 * 自定义用户
 *
 * @author hero
 */
public class XnwUser {

    private static final long serialVersionUID = -5768257947433986L;

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户guid
     */
    private String rowGuid;

    /**
     * 部门ID
     */
    private Long deptId;

    /**
     * 部门guid
     */
    private String deptGuid;

    /**
     * 部门名
     */
    private String deptName;

    /**
     * 系统用户昵称
     */
    private String nickName;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * email
     */
    private String email;

    /**
     * 角色名列表
     */
    private List<String> roleNameList;

    /**
     * 角色Guid列表
     */
    private List<String> roleGuidList;

    /**
     * 父级部门guid
     */
    private String parentDeptGuid;

    /**
     * 父级部门名称
     */
    private String parentDeptName;

    /**
     * 部门code
     */
    private String deptCode;

    /**
     * 数据权限
     */
    private List<String> dataScope;

    /**
     * 是否管理员
     */
    private Boolean isAdmin;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 授权范围
     */
    private List<String> scopes;

    private String username;

    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public XnwUser(String username,String password, Collection<? extends GrantedAuthority> authorities,Long id, String rowGuid, Integer userType,List<String> scopes){
//        super(username,password,authorities);
        this.id = id;
        this.rowGuid = rowGuid;
        this.userType = userType;
        this.scopes = scopes;

        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public XnwUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Long id, String rowGuid, Long deptId, String deptGuid, String deptName, String nickName, String mobile, String avatar, String tenantId, String email, List<String> roleNameList, List<String> roleGuidList) {
//        super(username, password, authorities);
        this.id = id;
        this.rowGuid = rowGuid;
        this.deptId = deptId;
        this.deptGuid = deptGuid;
        this.deptName = deptName;
        this.nickName = nickName;
        this.mobile = mobile;
        this.avatar = avatar;
        this.tenantId = tenantId;
        this.email = email;
        this.roleNameList = roleNameList;
        this.roleGuidList = roleGuidList;

        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public XnwUser(String username, String password,Boolean isAdmin, Collection<? extends GrantedAuthority> authorities,List<String> dataScope, Long id, String rowGuid, Long deptId, String deptGuid, String deptName, String parentDeptGuid, String parentDeptName, String nickName, String mobile, String avatar, String tenantId, String email, List<String> roleNameList, String deptCode, List<String> roleGuidList) {
//        super(username, password, authorities);
        this.id = id;
        this.rowGuid = rowGuid;
        this.deptId = deptId;
        this.deptGuid = deptGuid;
        this.deptName = deptName;
        this.nickName = nickName;
        this.mobile = mobile;
        this.avatar = avatar;
        this.tenantId = tenantId;
        this.email = email;
        this.roleNameList = roleNameList;
        this.roleGuidList = roleGuidList;
        this.parentDeptGuid = parentDeptGuid;
        this.parentDeptName = parentDeptName;
        this.deptCode = deptCode;
        this.dataScope = dataScope;
        this.isAdmin = isAdmin;

        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public XnwUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, Long id, String rowGuid, Long deptId, String deptGuid, String deptName, String nickName, String mobile, String avatar, String tenantId, String email, List<String> roleNameList, List<String> roleGuidList) {
//        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.id = id;
        this.rowGuid = rowGuid;
        this.deptId = deptId;
        this.deptGuid = deptGuid;
        this.deptName = deptName;
        this.nickName = nickName;
        this.mobile = mobile;
        this.avatar = avatar;
        this.tenantId = tenantId;
        this.email = email;
        this.roleNameList = roleNameList;
        this.roleGuidList = roleGuidList;
    }

    public List<String> getRoleNameList() {
        return roleNameList;
    }

    public String getDeptName() {
        return deptName;
    }

    public String getNickName() {
        return nickName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public String getRowGuid() {
        return rowGuid;
    }

    public Long getDeptId() {
        return deptId;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getEmail() {
        return email;
    }

    public String getParentDeptGuid() {
        return parentDeptGuid;
    }

    public String getParentDeptName() {
        return parentDeptName;
    }

    public XnwUser setId(Long id) {
        this.id = id;
        return this;
    }

    public XnwUser setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
        return this;
    }

    public XnwUser setDeptId(Long deptId) {
        this.deptId = deptId;
        return this;
    }

    public XnwUser setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
        return this;
    }

    public XnwUser setDeptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public XnwUser setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public XnwUser setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public XnwUser setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public XnwUser setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public XnwUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public XnwUser setRoleNameList(List<String> roleNameList) {
        this.roleNameList = roleNameList;
        return this;
    }

    public XnwUser setParentDeptGuid(String parentDeptGuid) {
        this.parentDeptGuid = parentDeptGuid;
        return this;
    }

    public XnwUser setParentDeptName(String parentDeptName) {
        this.parentDeptName = parentDeptName;
        return this;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public XnwUser setDeptCode(String deptCode) {
        this.deptCode = deptCode;
        return this;
    }

    public List<String> getRoleGuidList() {
        return roleGuidList;
    }

    public XnwUser setRoleGuidList(List<String> roleGuidList) {
        this.roleGuidList = roleGuidList;
        return this;
    }

    public List<String> getDataScope() {
        return dataScope;
    }

    public XnwUser setDataScope(List<String> dataScope) {
        this.dataScope = dataScope;
        return this;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean IsAdmin) {
        this.isAdmin = IsAdmin;
    }

    public Integer getUserType() {
        return userType;
    }

    public XnwUser setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
