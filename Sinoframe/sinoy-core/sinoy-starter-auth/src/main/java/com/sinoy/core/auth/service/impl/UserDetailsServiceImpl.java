//package com.sinoy.core.auth.service.impl;
//
//
//import cn.hutool.core.convert.Convert;
//import com.sinoy.core.common.constant.Oauth2Constant;
//import com.sinoy.core.security.userdetails.XnwUser;
//import com.sinoy.core.security.userdetails.XnwUserDetailsService;
//import com.sinoy.core.common.utils.exception.BaseException;
//import com.sinoy.platform.system.component.entity.FrameUser;
//import com.sinoy.platform.system.component.entity.dto.UserInfo;
//import com.sinoy.platform.system.component.service.FrameUserService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.util.ObjectUtils;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
///**
// * 用户详情实现类
// *
// * @author pangu
// */
//@Service
//public class UserDetailsServiceImpl implements XnwUserDetailsService {
//
//    private Logger log = LoggerFactory.getLogger(this.getClass());
//
//    public static final String ENABLE = "1";
//    public static final String DISABLE = "0";
//
//    @Autowired
//    private Environment env;
//
//    @Autowired
//    private FrameUserService frameUserService;
//
//    @Override
//    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
//
//        UserInfo userInfo = frameUserService.loadUserByLoginId(userName);
//        if (userInfo == null) {
//            throw new BaseException("该用户：" + userName + "不存在");
//        }
//        userInfo.setType(Oauth2Constant.LOGIN_USERNAME_TYPE);
//        userInfo.setUserName(userName);
//        return getUserDetails(userInfo);
//
//    }
//
//    @Override
//    public UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException {
//        return null;
//    }
//
//    @Override
//    public UserDetails loadUserBySocial(String openId) throws UsernameNotFoundException {
//        return null;
//    }
//
//
//    private UserDetails getUserDetails(UserInfo userInfo) {
//
//        if (ObjectUtils.isEmpty(userInfo)) {
//            log.info("该用户：{} 不存在！", userInfo.getUserName());
//            throw new BaseException("该用户：" + userInfo.getUserName() + "不存在");
//        } else if (DISABLE.equals(userInfo.getFrameUser().getStatus().toString())) {
//            log.info("该用户：{} 已被停用!", userInfo.getUserName());
//            throw new BaseException("对不起，您的账号：" + userInfo.getUserName() + " 已停用");
//        }
//        FrameUser user = userInfo.getFrameUser();
//        log.info("用户名：{}", user.getUserName());
//        if (userInfo.getPermissions() == null) {
//            userInfo.setPermissions(new ArrayList<>());
//        }
//
//        /**
//         * 角色+前缀 与 权限合并
//         */
//        List<String> roleNameList = userInfo.getRoleNameList();
//        List<String> tempList = new ArrayList();
//        if(roleNameList != null){
//            for (int i = 0; i < roleNameList.size(); i++) {
//                tempList.add("ROLE_" + roleNameList.get(i));
//            }
//            userInfo.getPermissions().addAll(tempList);
//        }
//
//        Collection<? extends GrantedAuthority> authorities
//                = AuthorityUtils.createAuthorityList(Convert.toStrArray(userInfo.getPermissions()));
//        log.info("authorities: {}", authorities);
//        return new XnwUser(user.getLoginId(), "{SHA-256}" + user.getPassword(), userInfo.getIsAdmin(),authorities,userInfo.getDataScope(),user.getRowId(), user.getRowGuid(), user.getDeptId(), user.getDeptGuid(), userInfo.getDeptName()
//                , userInfo.getPdeptGuid(), userInfo.getPdetpName(), user.getUserName(), user.getMobile(), user.getHeadimgGuid(), null, user.getEmail(), userInfo.getRoleNameList(), userInfo.getDeptCode(), userInfo.getRoleGuids());
//
//    }
//}
