/*
 * Copyright 2019-2028 Beijing Daotiandi Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Author: xuzhanfu (7333791@qq.com)
 */
package com.sinoy.core.auth.config;

//import com.sinoy.core.auth.service.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 安全配置中心
 * WebSecurityConfigurerAdapter与ResourceServerConfigurerAdapter区别：
 *
 * 1.ResourceServerConfigurerAdapter用于保护oauth要开放的资源(哪些需要token验证后才能访问)，
 * 主要作用于client端以及token的认证(Bearer auth)。
 *
 * 2.WebSecurityConfigurerAdapter主要作用于用户的登录(form login,Basic auth)，
 * 不用设置拦截oauth要开放的资源，如果同时设置了对某一资源的访问控制，会以ResourceServerConfigurerAdapter设置的为准，
 * 因为ResourceServerConfigurerAdapter优先级更高，他会优先处理，而WebSecurityConfigurerAdapter会失效；
 *
 **/
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

//	@Resource
//	private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;
//
//	@Resource
//	private SocialAuthenticationSecurityConfig socialAuthenticationSecurityConfig;

	/**
	 * 必须要定义，否则不支持grant_type=password模式
	 * 注册一个认证管理器对象到容器
	 * @return AuthenticationManager
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() {
        try {
            return super.authenticationManagerBean();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//	@Bean
//	public AuthenticationSuccessHandler XnwAuthenticationSuccessHandler() {
//		return new XnwAuthenticationSuccessHandler();
//	}
//
//	@Bean
//	public AuthenticationFailureHandler XnwAuthenticationFailureHandler() {
//		return new XnwAuthenticationFailureHandler();
//	}

//	@Override
//	@Bean
//	public UserDetailsService userDetailsService() {
//		return new UserDetailsServiceImpl();
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config
				= http.requestMatchers().anyRequest().and().authorizeRequests();
//				.formLogin()
//				.and()
//				.apply(smsCodeAuthenticationSecurityConfig)
//				.and()
//				.apply(socialAuthenticationSecurityConfig)
//				.and()
				;
		config
//				.antMatchers("/test2/**")
//				.permitAll()
				//任何请求

				.anyRequest()
				//都需要身份认证
				.authenticated()
				.and()
				.headers().frameOptions().disable()
				//csrf跨站请求
				.and()
				//开启跨域请求
				.cors().and()
				.csrf().disable();

	}

	/**
	 * 处理用户名和密码验证事宜
	 * 1）客户端传递username和password参数到认证服务器
	 * 2）一般来说，username和password会存储在数据库中的用户表中
	 * 3）根据用户表中数据，验证当前传递过来的用户信息的合法性
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// 在这个方法中就可以去关联数据库了，当前我们先把用户信息配置在内存中
		// 实例化一个用户对象(相当于数据表中的一条用户记录)
		auth.userDetailsService(userDetailsService())
				.passwordEncoder(passwordEncoder());
	}
}
