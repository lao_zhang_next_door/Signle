package com.sinoy.core.auth.service.impl;

import com.sinoy.core.auth.service.ValidateService;
import com.sinoy.core.common.constant.Oauth2Constant;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.redis.core.RedisService;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;

/**
 * 验证码业务类
 * @author pangu
 */
@Service
public class ValidateServiceImpl implements ValidateService {

    private final RedisService redisService;

    public ValidateServiceImpl(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public R getCode() {
//        Map<String, String> data = new HashMap<>(2);
//        String uuid = UUID.randomUUID().toString().replace("-","");
//        //SpecCaptcha captcha = new SpecCaptcha(120, 40);
//        //String text = captcha.text();// 获取运算的结果：5
//        ArithmeticCaptcha captcha = new ArithmeticCaptcha(120, 40);
//        captcha.getArithmeticString();  // 获取运算的公式：3+2=?
//        // 获取运算的结果：5
//        String text = captcha.text();
//        redisService.set(Oauth2Constant.CAPTCHA_KEY + uuid, text, Duration.ofMinutes(30));
//        data.put("key", uuid);
//        data.put("codeUrl", captcha.toBase64());
//        return R.ok(data);

        return R.ok();

    }

    @Override
    public R getSmsCode(String mobile) {

//        String code = "1188";
//        redisService.set(Oauth2Constant.SMS_CODE_KEY + mobile, code, Duration.ofMinutes(5));
//        return R.ok("发送成功");

        return R.ok();
    }

    @Override
    public void check(String key, String code) throws Exception {
        Object codeFromRedis = redisService.get(Oauth2Constant.CAPTCHA_KEY + key);

        if (StringUtils.isBlank(code)) {
            throw new RuntimeException("请输入验证码");
        }
        if (codeFromRedis == null) {
            throw new RuntimeException("验证码已过期");
        }
        if (!StringUtils.equalsIgnoreCase(code, codeFromRedis.toString())) {
            throw new RuntimeException("验证码不正确");
        }

        redisService.del(Oauth2Constant.CAPTCHA_KEY + key);
    }
}
