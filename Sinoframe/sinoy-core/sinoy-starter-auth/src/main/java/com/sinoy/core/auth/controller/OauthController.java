package com.sinoy.core.auth.controller;


import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.encryption.annotation.AsymmetryCrypto;
import com.sinoy.core.log.annotation.Log;
import com.sinoy.core.security.annotation.PassToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 自定义Oauth2自定义返回格式
 *
 * @author hero
 * @link https://segmentfault.com/a/1190000020317220?utm_source=tag-newest
 */
@RestController
@RequestMapping("/oauth")
public class OauthController {

	@Autowired
	public TokenEndpoint tokenEndpoint;

	/**
	 *
	 * @param parameters  grant_type:授权类型   username:用户名  password:密码  scope:使用范围
	 * @return
	 * @throws HttpRequestMethodNotSupportedException
	 */
	@PassToken
	@PostMapping("/login")
	@AsymmetryCrypto
	@Log("用户登录")
	public R postAccessToken(@RequestBody Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
		User principle = new User(parameters.get("client_id"),parameters.get("client_secret"),true,true,true,true,new ArrayList());
		UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken(principle,null,principle.getAuthorities());
		return custom(tokenEndpoint.postAccessToken(principal, parameters).getBody());
	}

	/**
	 * 仅仅只获取token
	 * @param parameters
	 * @return
	 * @throws HttpRequestMethodNotSupportedException
	 */
	@PassToken
	@PostMapping("/getToken")
	public R getToken(@RequestBody Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
		parameters.put("grant_type","password");
		User principle = new User(parameters.get("client_id"),parameters.get("client_secret"),true,true,true,true,new ArrayList());
		UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken(principle,null,principle.getAuthorities());
		return customToken(tokenEndpoint.postAccessToken(principal, parameters).getBody());
	}

	/**
	 * 自定义返回格式
	 *
	 * @param accessToken 　Token
	 * @return Result
	 */
	public R custom(OAuth2AccessToken accessToken) {
		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
		Map<String, Object> data = new LinkedHashMap<>(token.getAdditionalInformation());
		data.put("accessToken", token.getValue());
		if (token.getRefreshToken() != null) {
			data.put("refreshToken", token.getRefreshToken().getValue());
		}
		return R.ok(data);
	}

	/**
	 * 自定义返回格式
	 *
	 * @param accessToken 　Token
	 * @return Result
	 */
	public R customToken(OAuth2AccessToken accessToken) {
		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("accessToken", token.getValue());
		return R.ok(data);
	}

}
