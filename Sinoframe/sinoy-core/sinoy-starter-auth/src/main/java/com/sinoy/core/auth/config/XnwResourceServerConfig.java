package com.sinoy.core.auth.config;

import com.sinoy.core.security.config.IgnoreUrlPropsConfiguration;
import com.sinoy.core.security.handle.XnwAccessDeniedHandler;
import com.sinoy.core.security.handle.XnwAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;


/**
 * 资源服务配置
 *
 * @EnableXxx 注解是用在 @Configuration 标记的类上，用来自动配置和加载一些 @Configuration 类或者 bean 的。
 * 所以，@EnableXxx 一定是与 @Configuration 一起使用的。
 * @EnableXxx 一定会与 @Import 一起使用，通过 @Import 来达到自动配置的目的。
 * @author hero
 */
@EnableWebSecurity
@EnableResourceServer
public class XnwResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private Environment env;

	private final IgnoreUrlPropsConfiguration ignoreUrlPropsConfig;

	private final RedisConnectionFactory redisConnectionFactory;

	public XnwResourceServerConfig(IgnoreUrlPropsConfiguration ignoreUrlPropsConfig,RedisConnectionFactory redisConnectionFactory) {
		this.ignoreUrlPropsConfig = ignoreUrlPropsConfig;
		this.redisConnectionFactory = redisConnectionFactory;
	}

	/**
	 * 配置token存储到redis中
	 */
	@Bean
	public RedisTokenStore redisTokenStore() {
		return new RedisTokenStore(redisConnectionFactory);
	}

	/**
	 * 场景：一个服务中可能有很多资源（API接口）
	 * 某一些API接口，需要先认证，才能访问
	 * 某一些API接口，压根就不需要认证，本来就是对外开放的接口
	 * 我们就需要对不同特点的接口区分对待（在当前configure方法中完成），设置是否需要经过认证
	 *
	 * @param http
	 * @throws Exception
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {

		// 设置session的创建策略（根据需要创建即可）
//		http.authorizeRequests()
//				.antMatchers("/test/**").authenticated() // autodeliver为前缀的请求需要认证
////                .antMatchers("/test2/**").authenticated()  // demo为前缀的请求需要认证
//				.anyRequest().permitAll();  //  其他请求不认证

		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config
				= http.requestMatchers().anyRequest()
				.and()
				.authorizeRequests();
		ignoreUrlPropsConfig.getUrls().forEach(url -> {
			config.antMatchers(url).permitAll();
		});
		ignoreUrlPropsConfig.getIgnoreSecurity().forEach(url -> {
			config.antMatchers(url).permitAll();
		});
		config
//				.antMatchers("/test/**")
//				.authenticated()
//				.antMatchers("/test2/**")
//				.permitAll();
				//任何请求
				.anyRequest()
				//都需要身份认证
				.authenticated()
				.and()
				.headers().frameOptions().disable()
//				csrf跨站请求
				.and()
				//开启跨域请求
				.cors().and()
				.csrf().disable();
//
//		http.formLogin().successHandler(new XnwAuthenticationSuccessHandler()).failureHandler(new XnwAuthenticationFailureHandler());


//		String[] urls = authIgnoreConfig.getIgnoreUrls().stream().distinct().toArray(String[]::new);
//		http
//				.csrf().disable()
//				.authorizeRequests().antMatchers("/file/**").permitAll()
//				.and().authorizeRequests().antMatchers(urls).permitAll()
//				.and()
//				.exceptionHandling()
//				.authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
//				.and()
//				.authorizeRequests()
//				.anyRequest().authenticated();




	}

	/**
	 * 该方法用于定义资源服务器向远程认证服务器发起请求，进行token校验等事宜
	 * @param resources
	 * @throws Exception
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// 设置当前资源服务的资源id
		resources.resourceId(env.getProperty("xnw.oauth.resourceId"));
		// 定义token服务对象（token校验就应该靠token服务对象）
		RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
		// 校验端点/接口设置
		remoteTokenServices.setCheckTokenEndpointUrl("http://localhost:"+env.getProperty("server.port")+"/sino-api/oauth/check_token");
//		remoteTokenServices.setCheckTokenEndpointUrl("http://localhost:"+env.getProperty("server.port")+"/sino-api/frame/oauth2/check_token");
		// 携带客户端id和客户端安全码
		remoteTokenServices.setClientId(env.getProperty("xnw.oauth.clientId"));
		remoteTokenServices.setClientSecret(env.getProperty("xnw.oauth.clientSecret"));
		resources.tokenServices(remoteTokenServices);
		resources.authenticationEntryPoint(new XnwAuthenticationEntryPoint())
				.accessDeniedHandler(new XnwAccessDeniedHandler());
	}

}
