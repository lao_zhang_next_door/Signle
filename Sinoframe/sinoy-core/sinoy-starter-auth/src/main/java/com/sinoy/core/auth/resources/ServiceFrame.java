package com.sinoy.core.auth.resources;

import com.sinoy.core.common.utils.exception.BaseException;

import java.sql.*;
import java.util.List;

public class ServiceFrame {

    private static Connection con = null;// 连接对象
    private static PreparedStatement psmt = null;// 预编译对象
    private static ResultSet rs = null;// 结果集对象
    private static CallableStatement csmt = null;// 过程对象

    public static void setConnection(String qd,String jdbcUrl,String sqlName,String sqlPassword){
        try {
            Class.forName(qd);
            con = DriverManager
                    .getConnection(jdbcUrl,sqlName,sqlPassword);
        } catch (ClassNotFoundException e) {
            throw new BaseException("找不到驱动");
        } catch (Exception e) {
            throw new BaseException("数据库连接失败");
        }
    }

    /**
     *
     * @param sql
     * @return
     */
    public static PreparedStatement getPreparedStatement(String sql) {
        try {
            psmt = con.prepareStatement(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return psmt;
    }

    /**
     * 查询  带参数
     * @param sql
     * @param params
     * @return  结果集
     */
    public static ResultSet executeQuery(String sql, List<Object> params) {
        getPreparedStatement(sql);
        if (params != null) {
            bindPramas(psmt, params);
        }
        try {
            rs = psmt.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rs;
    }

    public static void bindPramas(PreparedStatement psmt, List<Object> params) {
        int index = 0;
        if (params != null) {
            for (Object p : params) {
                try {
                    psmt.setObject(index + 1, p);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                index++;

            }

        }
    }

    public static void CloseAll() {
        try {
            if (con != null) {

                con.close();

            }
            if (psmt != null) {
                psmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }





}
