/*
 * Copyright 2019-2028 Beijing Daotiandi Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Author: xuzhanfu (7333791@qq.com)
 */
package com.sinoy.core.auth.config;

import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.auth.service.impl.ClientDetailsServiceImpl;
import com.sinoy.core.auth.service.impl.SingleLoginTokenServices;
import com.sinoy.core.common.constant.Oauth2Constant;
import com.sinoy.core.redis.core.RedisService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import java.util.*;

/**
 * 认证服务器配置中心
 * spring security oauth2 中的 endpoint
 *
 * /oauth/authorize(授权端，授权码模式使用)
 * /oauth/token(令牌端，获取 token)
 * /oauth/check_token(资源服务器用来校验token)
 * /oauth/confirm_access(用户发送确认授权)
 * /oauth/error(认证失败)
 * /oauth/token_key(如果使用JWT，可以获的公钥用于 token 的验签)
 *
 * @author hero
 **/

@Order(2)
@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

	private final ClientDetailsServiceImpl clientService;

	private final RedisConnectionFactory redisConnectionFactory;

	private final AuthenticationManager authenticationManager;

	private final UserDetailsService userDetailsService;

	private final RedisService redisService;

//	private final AuthRequestFactory factory;

    public AuthServerConfig(ClientDetailsServiceImpl clientService,RedisConnectionFactory redisConnectionFactory, AuthenticationManager authenticationManager, UserDetailsService userDetailsService, RedisService redisService) {
        this.clientService = clientService;
        this.redisConnectionFactory = redisConnectionFactory;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.redisService = redisService;
//        this.factory = factory;
    }

    @Value("${mate.uaa.isSingleLogin:false}")
	private final boolean isSingleLogin = false;


	/**
	 * 配置token存储到redis中
	 */
//	@Bean
	public RedisTokenStore redisTokenStore() {
		return new RedisTokenStore(redisConnectionFactory);
	}

    /**
     * 认证服务器是玩转token的，那么这里配置token令牌管理相关
	 * （token此时就是一个字符串，当下的token需要在服务器端存储，
     * 那么存储在哪里呢？都是在这里配置
     * @param endpoints
     * @throws Exception
     */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

//        endpoints
//                .tokenStore(tokenStore())  // 指定token的存储方法
//                .tokenServices(authorizationServerTokenServices())   // token服务的一个描述，可以认为是token生成细节的描述，比如有效时间多少等
//                .authenticationManager(authenticationManager) // 指定认证管理器，随后注入一个到当前类使用即可
//                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);

		endpoints
                .tokenStore(redisTokenStore())
				.tokenGranter(tokenGranter(endpoints, TokenServices()))
				.tokenServices(TokenServices())
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
				.accessTokenConverter(jwtAccessTokenConverter());

	}

    /**
     * 该方法用户获取一个token服务对象（该对象描述了token有效期等信息）
     */
    public DefaultTokenServices TokenServices() {
//        // 使用默认实现
//        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
//        defaultTokenServices.setSupportRefreshToken(true); // 是否开启令牌刷新
//        defaultTokenServices.setTokenStore(tokenStore());
//
//        // 设置令牌有效时间（一般设置为2个小时）
//        defaultTokenServices.setAccessTokenValiditySeconds(20); // access_token就是我们请求资源需要携带的令牌
//        // 设置刷新令牌的有效时间
//        defaultTokenServices.setRefreshTokenValiditySeconds(259200); // 3天

        DefaultTokenServices tokenServices = createDefaultTokenServices();
        // token增强链
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        // 把jwt增强，与额外信息增强加入到增强链
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), jwtAccessTokenConverter()));
        tokenServices.setTokenEnhancer(tokenEnhancerChain);
        // 配置tokenServices参数
        addUserDetailsService(tokenServices);

        return tokenServices;
    }


	private void addUserDetailsService(DefaultTokenServices tokenServices) {
		if (userDetailsService != null) {
			PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
			provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<>(userDetailsService));
			tokenServices.setAuthenticationManager(new ProviderManager(Collections.singletonList(provider)));
		}
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
//        // 相当于打开endpoints 访问接口的开关，这样的话后期我们能够访问该接口
//        security
//                // 允许客户端表单认证
//                .allowFormAuthenticationForClients()
//                // 开启端口/oauth/token_key的访问权限（允许）
//                .tokenKeyAccess("permitAll()")
//                // 开启端口/oauth/check_token的访问权限（允许）
//                .checkTokenAccess("permitAll()");

		security
				// 允许表单认证请求
				.allowFormAuthenticationForClients()
				// spel表达式 访问公钥端点（/oauth/token_key）需要认证
				.tokenKeyAccess("isAuthenticated()")
				// spel表达式 访问令牌解析端点（/oauth/check_token）需要认证
				.checkTokenAccess("permitAll()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
//        // 从内存中加载客户端详情
//        clients.inMemory()// 客户端信息存储在什么地方，可以在内存中，可以在数据库里
//                .withClient("client_lagou")  // 添加一个client配置,指定其client_id
//                .secret("abcxyz")                   // 指定客户端的密码/安全码
//                .resourceIds("autodeliver")         // 指定客户端所能访问资源id清单，此处的资源id是需要在具体的资源服务器上也配置一样
//                // 认证类型/令牌颁发模式，可以配置多个在这里，但是不一定都用，具体使用哪种方式颁发token，需要客户端调用的时候传递参数指定
//                .authorizedGrantTypes("password", "refresh_token")
//                // 客户端的权限范围，此处配置为all全部即可
//                .scopes("all");

		clientService.setSelectClientDetailsSql(Oauth2Constant.SELECT_CLIENT_DETAIL_SQL);
		clientService.setFindClientDetailsSql(Oauth2Constant.FIND_CLIENT_DETAIL_SQL);
		clients.withClientDetails(clientService);
	}

	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
		jwtAccessTokenConverter.setSigningKey(Oauth2Constant.SIGN_KEY);
//		jwtAccessTokenConverter.setVerifierKey(Oauth2Constant.VERIFIER_Key);
		return jwtAccessTokenConverter;
	}

	/**
	 * 重点
	 * 先获取已经有的四种授权，然后添加我们自己的进去
	 * 默认的 几种模式
	 * 1.authorization_code
	 * 2.refresh_token
	 * 3.implicit
	 * 4.client_credentials
	 * @param endpoints AuthorizationServerEndpointsConfigurer
	 * @return TokenGranter
	 */
	private TokenGranter tokenGranter(final AuthorizationServerEndpointsConfigurer endpoints, DefaultTokenServices tokenServices) {
		List<TokenGranter> granters = new ArrayList<>(Collections.singletonList(endpoints.getTokenGranter()));
//		// 短信验证码模式
//		granters.add(new SmsCodeTokenGranter(authenticationManager, tokenServices, endpoints.getClientDetailsService(),
//				endpoints.getOAuth2RequestFactory(), redisService));
//		// 验证码模式
//		granters.add(new CaptchaTokenGranter(authenticationManager, tokenServices, endpoints.getClientDetailsService(),
//				endpoints.getOAuth2RequestFactory(), redisService));
//		// 社交登录模式
//		granters.add(new SocialTokenGranter(authenticationManager, tokenServices, endpoints.getClientDetailsService(),
//				endpoints.getOAuth2RequestFactory(), redisService, factory));
//		// 增加密码模式
		granters.add(new ResourceOwnerPasswordTokenGranter(authenticationManager, tokenServices, endpoints.getClientDetailsService(), endpoints.getOAuth2RequestFactory()));
		return new CompositeTokenGranter(granters);
	}

	/**
	 * 创建默认的tokenServices
	 *
	 * @return DefaultTokenServices
	 */
	private DefaultTokenServices createDefaultTokenServices() {
		DefaultTokenServices tokenServices = new SingleLoginTokenServices(isSingleLogin);
		tokenServices.setTokenStore(redisTokenStore());
		// 支持刷新Token
		tokenServices.setSupportRefreshToken(Boolean.TRUE);
		tokenServices.setReuseRefreshToken(Boolean.FALSE);
		tokenServices.setClientDetailsService(clientService);
		addUserDetailsService(tokenServices);
		return tokenServices;
	}

	/**
	 * jwt token增强，添加额外信息
	 *
	 * @return TokenEnhancer
	 */
	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new TokenEnhancer() {
			@Override
			public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {

				// 添加额外信息的map
				final Map<String, Object> additionMessage = new HashMap<>(2);
				// 对于客户端鉴权模式，直接返回token
				if (oAuth2Authentication.getUserAuthentication() == null) {
					return oAuth2AccessToken;
				}
				// 获取当前登录的用户
				XnwUser user = (XnwUser) oAuth2Authentication.getUserAuthentication().getPrincipal();

				// 如果用户不为空 则把id放入jwt token中
				if (user != null) {

					JSONObject obj = new JSONObject();
					obj.put("loginId",user.getUsername());
					obj.put("userName",user.getNickName());
					obj.put("userGuid",user.getRowGuid());
					obj.put("deptGuid",user.getDeptGuid());
					obj.put("deptCode",user.getDeptCode());
					obj.put("deptName",user.getDeptName());
					obj.put("mobile",user.getMobile());
					obj.put("roleNameList",user.getRoleNameList());
					obj.put("roleGuidList",user.getRoleGuidList());
					obj.put("pdeptGuid",user.getParentDeptGuid());
					obj.put("pdeptName",user.getParentDeptName());
					additionMessage.put("userInfo", obj);
//					additionMessage.put(Oauth2Constant.XNW_USER_NAME, user.getUsername());
//					additionMessage.put(.XNW_DEPT_GUID, user.getDeptGuid());
//					additionMessage.put(Oauth2COauth2Constantonstant.XNW_DEPT_NAME, user);
//					additionMessage.put(Oauth2Constant.XNW_AVATAR, user.getAvatar());
//					additionMessage.put(Oauth2Constant.XNW_ROLE_ID, String.valueOf(user.getRoleId()));
//					additionMessage.put(Oauth2Constant.XNW_TYPE, user.getType());
//					additionMessage.put(Oauth2Constant.XNW_TENANT_ID, user.getTenantId());
				}
				((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionMessage);
				return oAuth2AccessToken;
			}
		};
	}
}
