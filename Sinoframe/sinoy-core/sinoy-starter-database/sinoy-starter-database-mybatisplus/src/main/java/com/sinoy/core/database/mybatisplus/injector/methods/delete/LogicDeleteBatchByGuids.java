package com.sinoy.core.database.mybatisplus.injector.methods.delete;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.sinoy.core.common.enums.DelFlag;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 根据rowGuids 数组物理删除
 */
public class LogicDeleteBatchByGuids extends AbstractMethod {

    private static final long serialVersionUID = 1271086946123968982L;
    private String methodName = "logicDeleteBatchByGuids";

    public LogicDeleteBatchByGuids(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        // 自定义sql tableInfo.getTableName() 获取表名
        String where = "<foreach item=\"rowGuid\" collection=\"array\" open=\"(\" separator=\",\" close=\")\">#{rowGuid}</foreach>";
        String sql = String.format("<script>\nUPDATE %s SET del_flag = " + DelFlag.Del.getCode() + " WHERE row_guid in %s\n</script>", tableInfo.getTableName(), where);
        // mapper 接口方法名
        String method = "logicDeleteBatchByGuids";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addDeleteMappedStatement(mapperClass, method, sqlSource);
    }
}
