/*
 * Copyright (c) 2019-2029, Dreamlu 卢春梦 (596392912@qq.com & www.dreamlu.net).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinoy.core.database.mybatisplus.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.sinoy.core.database.mybatisplus.injector.methods.*;
import com.sinoy.core.database.mybatisplus.injector.methods.delete.*;
import com.sinoy.core.database.mybatisplus.injector.methods.select.SelectByGuid;
import com.sinoy.core.database.mybatisplus.injector.methods.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 自定义的 sql 注入
 *
 * @author L.cm
 */
public class XnwSqlInjector extends DefaultSqlInjector {

	@Override
	public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
		/*List<AbstractMethod> methodList = new ArrayList<>();
		methodList.add(new InsertBatch());
		methodList.add(new InsertIgnore());
		methodList.add(new InsertIgnoreBatch());
		methodList.add(new Replace());
		methodList.add(new ReplaceBatch());
		methodList.add(new DeleteByGuid());
		methodList.add(new DeleteBatchByGuids());
		methodList.add(new SelectByGuid());*/

		List<AbstractMethod> methodList = new ArrayList<>();
		methodList.add(new InsertBatch());
		methodList.add(new InsertIgnore());
		methodList.add(new InsertIgnoreBatch());
		methodList.add(new Replace());
		methodList.add(new ReplaceBatch());
		methodList.add(new DeleteByGuid("deleteByGuid"));
		methodList.add(new DeleteBatchByGuids("deleteBatchByGuids"));
		methodList.add(new SelectByGuid("selectByGuid"));
		methodList.add(new LogicDeleteBatchByGuids("logicDeleteBatchByGuids"));
		methodList.add(new LogicDeleteBatchByIds("logicDeleteBatchByIds"));
		methodList.add(new LogicDeleteBatchById("logicDeleteBatchById"));

		methodList.addAll(super.getMethodList(mapperClass,tableInfo));
		return Collections.unmodifiableList(methodList);
	}
}
