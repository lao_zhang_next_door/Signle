package com.sinoy.core.database.mybatisplus.entity;


import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 搜索封装类
 *
 */
public class Search implements Serializable {

//	/**
//	 * 关键词
//	 */
//	private String keyword;
//
//	/**
//	 * 开始日期
//	 */
//	private String startDate;
//
//	/**
//	 * 结束日期
//	 */
//	private String endDate;

	/**
	 * 排序属性
	 */
	private List<String> prop;

	/**
	 * 排序方式：asc,desc
	 */
	private List<String> order;

	/**
	 * 当前页
	 */
	private Integer currentPage = 1;

	/**
	 * 每页的数量
	 */
	private Integer pageSize = 10;

	/**
	 * 分页列表参数
	 */
	private Map<String,Object> params;

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

//	public String getKeyword() {
//		return keyword;
//	}
//
//	public void setKeyword(String keyword) {
//		this.keyword = keyword;
//	}
//
//	public String getStartDate() {
//		return startDate;
//	}
//
//	public void setStartDate(String startDate) {
//		this.startDate = startDate;
//	}
//
//	public String getEndDate() {
//		return endDate;
//	}
//
//	public void setEndDate(String endDate) {
//		this.endDate = endDate;
//	}

	public List<String> getProp() {
		return prop;
	}

	public void setProp(List<String> prop) {
		this.prop = prop;
	}

	public List<String> getOrder() {
		return order;
	}

	public void setOrder(List<String> order) {
		this.order = order;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
