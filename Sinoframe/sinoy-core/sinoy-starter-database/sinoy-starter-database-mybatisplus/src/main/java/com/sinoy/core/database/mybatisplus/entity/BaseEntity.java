package com.sinoy.core.database.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.core.common.enums.DelFlag;
import groovy.transform.builder.Builder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 实体类的基类
 */
@Builder
public class BaseEntity {

    public BaseEntity() {
    }

    public BaseEntity(boolean init) {
        if (init) {
            this.init();
        }
    }

    public BaseEntity(String rowGuid) {
        if (!StringUtils.isEmpty(rowGuid)) {
            this.setRowGuid(rowGuid);
        }
    }

    public BaseEntity(Long rowId) {
        if (rowId != null) {
            this.setRowId(rowId);
        }
    }

    public void init() {
        this.rowGuid = UUID.randomUUID().toString();
        this.createTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
        this.delFlag = DelFlag.Normal.getCode();
        this.sortSq = 0L;
    }

    //初始化默认值为null的字段 若有值则不进行初始化
    public void initNull() {
        if (this.rowGuid == null) {
            this.rowGuid = UUID.randomUUID().toString();
        }
        if (this.createTime == null) {
            this.createTime = LocalDateTime.now();
        }
        if (this.updateTime == null) {
            this.updateTime = LocalDateTime.now();
        }
        if (this.delFlag == null) {
            this.delFlag = DelFlag.Normal.getCode();
        }
        if (this.sortSq == null) {
            this.sortSq = 0L;
        }
    }

    /**
     * 主键id
     */
    @TableId(value = "row_id", type = IdType.AUTO)
    private Long rowId;

    /**
     * rowGuid 从不更新策略
     */
    @TableField(value = "row_guid", updateStrategy = FieldStrategy.NEVER)
    private String rowGuid;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    /**
     * 删除标识
     */
    private Integer delFlag;

    /**
     * 排序
     */
    private Long sortSq;

    public Long getRowId() {
        return rowId;
    }

    public BaseEntity setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Long getSortSq() {
        return sortSq;
    }

    public void setSortSq(Long sortSq) {
        this.sortSq = sortSq;
    }
}
