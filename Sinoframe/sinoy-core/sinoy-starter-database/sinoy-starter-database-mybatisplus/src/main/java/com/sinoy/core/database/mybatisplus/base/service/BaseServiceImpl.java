package com.sinoy.core.database.mybatisplus.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sinoy.core.common.utils.dataformat.ObjectUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.entity.Sort;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.security.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class BaseServiceImpl<M extends ExtendMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {

    @Autowired
    protected PermissionService permissionService;

    //查询===============================================================================================================

    @Override
    public T selectByGuid(String rowGuid) {
        return baseMapper.selectByGuid(rowGuid);
    }

    @Override
    public List<T> selectByEntity(T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.notNullField(t, queryWrapper);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<T> selectByEntity(T t, Sort sort) {
        QueryWrapper<T> queryWrapper = new QueryWrapper();
        if(t != null){
            BatisPlusUtil.notNullField(t,queryWrapper);
        }
        if(sort != null){
            BatisPlusUtil.setOrders(queryWrapper,sort.getProp(),sort.getOrder());
        }
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public T selectOneByEntity(T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.notNullField(t, queryWrapper);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public Long selectCountByEntity(T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.notNullField(t, queryWrapper);
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public List<T> listAll() {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        //默认排序
        BatisPlusUtil.setOrders(queryWrapper,null,null);
        return baseMapper.selectList(queryWrapper);
    }

    //更新===============================================================================================================

    @Override
    public int updateByRowGuid(T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        Field field = ObjectUtil.getFieldByClasss("rowGuid", t);
        if (field == null) {
            throw new BaseException("更新时未获取到相关rowGuid值");
        }
        field.setAccessible(true);
        try {
            queryWrapper.eq("row_guid", (String) field.get(t));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return baseMapper.update(t, queryWrapper);
    }

    //删除===============================================================================================================

    @Override
    public int deleteByGuid(String rowGuid) {
        return baseMapper.deleteByGuid(rowGuid);
    }

    @Override
    public int deleteBatchByGuids(String[] rowGuids) {
        return baseMapper.deleteBatchByGuids(rowGuids);
    }

    @Override
    public int deleteBatchByIds(Long[] ids) {
        return baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public int logicDeleteBatchByGuids(String[] rowGuids) {
        return baseMapper.logicDeleteBatchByGuids(rowGuids);
    }

    @Override
    public int logicDeleteBatchByIds(Long[] ids) {
        return baseMapper.logicDeleteBatchByIds(ids);
    }

    @Override
    public int logicDeleteBatchById(Long id) {
        return baseMapper.logicDeleteBatchById(id);
    }
}
