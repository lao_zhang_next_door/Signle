//package com.sino.core.database.mybatisplus.datascope2.context;
//
//import com.sino.core.database.mybatisplus.datascope2.UserPermissionInfo;
//
///**
// * <p> 用户数据权限信息上下文 </p>
// *
// */
//public class DataPermissionThreadLocal {
//
//    private static final ThreadLocal<UserPermissionInfo> THREAD_LOCAL = new ThreadLocal<>();
//
//    public static void set(UserPermissionInfo userPermissionInfo) {
//        THREAD_LOCAL.set(userPermissionInfo);
//    }
//
//    public static UserPermissionInfo get() {
//        return THREAD_LOCAL.get();
//    }
//
//    public static void remove() {
//        THREAD_LOCAL.remove();
//    }
//
//}
