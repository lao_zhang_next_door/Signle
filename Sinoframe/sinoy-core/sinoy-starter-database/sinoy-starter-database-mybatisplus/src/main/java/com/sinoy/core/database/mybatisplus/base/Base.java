package com.sinoy.core.database.mybatisplus.base;

import com.sinoy.core.security.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Base {
	
	@Autowired
	protected HttpSession session;
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpServletResponse response;
	//定义一个全局的记录器，通过LoggerFactory获取
	
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value(value= "${SystemFrame.passwordKey}")
	protected String passwordKey;

	@Autowired
	protected PermissionService permissionService;



}
