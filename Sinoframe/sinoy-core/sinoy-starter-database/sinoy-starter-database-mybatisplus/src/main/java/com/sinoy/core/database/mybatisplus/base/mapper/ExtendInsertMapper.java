package com.sinoy.core.database.mybatisplus.base.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Collection;

public interface ExtendInsertMapper<T> extends BaseMapper<T> {


    /**
     * 逐条插入，适合少量数据插入，或者对性能要求不高的场景
     *
     * 如果大量，请使用 {@link com.baomidou.mybatisplus.extension.service.impl.ServiceImpl#saveBatch(Collection)} 方法
     * 使用示例，可见 RoleMenuBatchInsertMapper、UserRoleBatchInsertMapper 类
     *
     * @param entities 实体们
     */
    default void insertBatch(Collection<T> entities) {
        entities.forEach(this::insert);
    }
}
