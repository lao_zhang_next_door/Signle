//package com.sino.core.database.mybatisplus.datascope2;
//
//import java.util.Set;
//
///**
// * <p> 用户权限信息 </p>
// *
// * @author zhengqingya
// * @description
// * @date 2022/1/10 17:51
// */
//public class UserPermissionInfo {
//
//    //用户ID
//    private Long userId;
//
//    //部门Id
//    private String deptId;
//
//    //部门guid
//    private String deptGuid;
//
//    //部门code
//    private String deptCode;
//
//    //角色id
//    private String roleId;
//
//    //角色ids
//    private Set<String> roleIdList;
//
//    //自定义权限过滤sql
//    private String sql;
//
//    //数据权限类型
//    private DataPermissionTypeEnum dataPermissionTypeEnum;
//
//    public Long getUserId() {
//        return userId;
//    }
//
//    public UserPermissionInfo setUserId(Long userId) {
//        this.userId = userId;
//        return this;
//    }
//
//    public String getDeptId() {
//        return deptId;
//    }
//
//    public UserPermissionInfo setDeptId(String deptId) {
//        this.deptId = deptId;
//        return this;
//    }
//
//    public String getDeptGuid() {
//        return deptGuid;
//    }
//
//    public UserPermissionInfo setDeptGuid(String deptGuid) {
//        this.deptGuid = deptGuid;
//        return this;
//    }
//
//    public String getDeptCode() {
//        return deptCode;
//    }
//
//    public UserPermissionInfo setDeptCode(String deptCode) {
//        this.deptCode = deptCode;
//        return this;
//    }
//
//    public String getRoleId() {
//        return roleId;
//    }
//
//    public UserPermissionInfo setRoleId(String roleId) {
//        this.roleId = roleId;
//        return this;
//    }
//
//    public Set<String> getRoleIdList() {
//        return roleIdList;
//    }
//
//    public UserPermissionInfo setRoleIdList(Set<String> roleIdList) {
//        this.roleIdList = roleIdList;
//        return this;
//    }
//
//    public String getSql() {
//        return sql;
//    }
//
//    public UserPermissionInfo setSql(String sql) {
//        this.sql = sql;
//        return this;
//    }
//
//    public DataPermissionTypeEnum getDataPermissionTypeEnum() {
//        return dataPermissionTypeEnum;
//    }
//
//    public UserPermissionInfo setDataPermissionTypeEnum(DataPermissionTypeEnum dataPermissionTypeEnum) {
//        this.dataPermissionTypeEnum = dataPermissionTypeEnum;
//        return this;
//    }
//}
