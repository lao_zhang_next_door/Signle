package com.sinoy.core.database.mybatisplus.gen;

import com.baomidou.mybatisplus.annotation.DbType;

/**
 * 代码生成配置实体类
 */
public class CodeConfig {

    private String packageName;
    private String prefix;
    private String modelName;
    private DbType dbType;
    private String url;
    private String username;
    private String password;
    private String driverName;
    private String outputDir;
    private String tableName;

    public String getPackageName() {
        return packageName;
    }

    public CodeConfig setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getPrefix() {
        return prefix;
    }

    public CodeConfig setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public String getModelName() {
        return modelName;
    }

    public CodeConfig setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public DbType getDbType() {
        return dbType;
    }

    public CodeConfig setDbType(DbType dbType) {
        this.dbType = dbType;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public CodeConfig setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public CodeConfig setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CodeConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getDriverName() {
        return driverName;
    }

    public CodeConfig setDriverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public CodeConfig setOutputDir(String outputDir) {
        this.outputDir = outputDir;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public CodeConfig setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }
}
