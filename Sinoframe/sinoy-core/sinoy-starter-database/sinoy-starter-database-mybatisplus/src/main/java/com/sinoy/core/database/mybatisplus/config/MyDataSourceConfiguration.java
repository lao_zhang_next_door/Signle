package com.sinoy.core.database.mybatisplus.config;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.provider.AbstractDataSourceProvider;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.baomidou.mybatisplus.autoconfigure.SpringBootVFS;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.sinoy.core.common.utils.factory.YamlPropertySourceFactory;
import com.sinoy.core.database.mybatisplus.injector.XnwSqlInjector;
import com.sinoy.core.datascope.MyDataPermissionHandler;
import com.sinoy.core.datascope.MyDataPermissionInterceptor;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.core.sharding.interceptor.SqlLogInterceptor;
import com.sinoy.core.sharding.props.XnwMybatisProperties;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.boot.autoconfigure.MybatisProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Map;

/**
 * 动态数据源配置,集成sharding-jdbc
 *
 */
@Configuration
@AutoConfigureBefore({DynamicDataSourceAutoConfiguration.class, SpringBootConfiguration.class})
@EnableConfigurationProperties(XnwMybatisProperties.class)
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:xnw-sharding.yml")
public class MyDataSourceConfiguration {

	@Resource
	private DynamicDataSourceProperties properties;

    /**
     * shardingjdbc有四种数据源，需要根据业务注入不同的数据源
     *
     * <p>1. 未使用分片, 脱敏的名称(默认): shardingDataSource;
     * <p>2. 主从数据源: masterSlaveDataSource;
     * <p>3. 脱敏数据源：encryptDataSource;
     * <p>4. 影子数据源：shadowDataSource
     */
    @Lazy
    @Resource(name = "shardingDataSource")
    private DataSource shardingSphereDataSource;

    @Autowired
    public DataSource dataSource;

    @Autowired
    public MybatisProperties mybatisProperties;

    @Autowired
    public ResourceLoader resourceLoader = new DefaultResourceLoader();

    @Autowired(required = false)
    private Interceptor[] interceptors;

    @Autowired(required = false)
    public DatabaseIdProvider databaseIdProvider;

    @Autowired
    public CommonPropAndMethods commonPropAndMethods;

    /**
     * 单页分页条数限制(默认无限制,参见 插件#handlerLimit 方法)
     */
    public static final Long MAX_LIMIT = 1000L;



    @Bean
    public DynamicDataSourceProvider dynamicDataSourceProvider() {
        Map<String, DataSourceProperty> datasourceMap = properties.getDatasource();
        return new AbstractDataSourceProvider() {
            @Override
            public Map<String, DataSource> loadDataSources() {
                Map<String, DataSource> dataSourceMap = createDataSourceMap(datasourceMap);
                if(shardingSphereDataSource != null){
                    dataSourceMap.put("sharding", shardingSphereDataSource);
                }
                //打开下面的代码可以把 shardingjdbc 管理的数据源也交给动态数据源管理 (根据自己需要选择开启)
//                 dataSourceMap.putAll(((MasterSlaveDataSource) masterSlaveDataSource).getDataSourceMap());
                return dataSourceMap;
            }
        };
    }

    /**
     * 将动态数据源设置为首选的
     * 当spring存在多个数据源时, 自动注入的是首选的对象
     * 设置为主要的数据源之后，就可以支持shardingjdbc原生的配置方式了
     */
    @Primary
    @Bean
    public DataSource dataSource(DynamicDataSourceProvider dynamicDataSourceProvider) {
        DynamicRoutingDataSource dataSource = new DynamicRoutingDataSource();
        dataSource.setPrimary(properties.getPrimary());
        dataSource.setStrict(properties.getStrict());
        dataSource.setStrategy(properties.getStrategy());
//        dataSource.setProvider(dynamicDataSourceProvider);
        dataSource.setP6spy(properties.getP6spy());
        dataSource.setSeata(properties.getSeata());
        return dataSource;
    }

//    /**
//     * sql 日志
//     */
//    @Bean
////	@Profile({"local", "dev", "test"})
//    @ConditionalOnProperty(value = "mybatis-plus.sql-log.enable", matchIfMissing = true)
//    public SqlLogInterceptor sqlLogInterceptor() {
//        return new SqlLogInterceptor();
//    }

    /**
     * 这里全部使用mybatis-autoconfigure 已经自动加载的资源。不手动指定
     * 配置文件和mybatis-boot的配置文件同步
     * @return
     */
    @Bean
    public MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean() {

        MybatisSqlSessionFactoryBean mybatisPlus = new MybatisSqlSessionFactoryBean();
        mybatisPlus.setDataSource(dataSource);
        mybatisPlus.setVfs(SpringBootVFS.class);
        if (StringUtils.hasText(this.mybatisProperties.getConfigLocation())) {
            mybatisPlus.setConfigLocation(this.resourceLoader.getResource(this.mybatisProperties.getConfigLocation()));
        }
        //设置插件
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //分页插件: PaginationInnerInterceptor
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setMaxLimit(MAX_LIMIT);
        //防止全表更新与删除插件: BlockAttackInnerInterceptor
        BlockAttackInnerInterceptor blockAttackInnerInterceptor = new BlockAttackInnerInterceptor();
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        interceptor.addInnerInterceptor(blockAttackInnerInterceptor);
        //日志插件
        mybatisPlus.setPlugins(new SqlLogInterceptor(),interceptor);

        /**
         * 添加数据权限插件
         */
        MyDataPermissionInterceptor dataPermissionInterceptor = new MyDataPermissionInterceptor(new MyDataPermissionHandler(commonPropAndMethods));
        // 添加自定义的数据权限处理器
        interceptor.addInnerInterceptor(dataPermissionInterceptor);



        MybatisConfiguration mc = new MybatisConfiguration();
        mc.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
        //数据库字段设计为驼峰命名，默认开启的驼峰转下划线会报错字段找不到
//        mc.setMapUnderscoreToCamelCase(true);
        mybatisPlus.setConfiguration(mc);
        if (this.databaseIdProvider != null) {
            mybatisPlus.setDatabaseIdProvider(this.databaseIdProvider);
        }
        if (StringUtils.hasLength(this.mybatisProperties.getTypeAliasesPackage())) {
            mybatisPlus.setTypeAliasesPackage(this.mybatisProperties.getTypeAliasesPackage());
        }
        if (StringUtils.hasLength(this.mybatisProperties.getTypeHandlersPackage())) {
            mybatisPlus.setTypeHandlersPackage(this.mybatisProperties.getTypeHandlersPackage());
        }
        if (!ObjectUtils.isEmpty(this.mybatisProperties.resolveMapperLocations())) {
            mybatisPlus.setMapperLocations(this.mybatisProperties.resolveMapperLocations());
        }

//        // TODO 此处必为非 NULL
//        GlobalConfig globalConfig = new GlobalConfig();//这里是初始化全局缓存的最初开始
//        // TODO 注入填充器
//        this.getBeanThen(MetaObjectHandler.class, globalConfig::setMetaObjectHandler);
//        // TODO 注入主键生成器
//        this.getBeanThen(IKeyGenerator.class, i -> globalConfig.getDbConfig().setKey setKeyGenerator(i));//这里注入自定义的主键生成器
//        // TODO 注入sql注入器
//        this.getBeanThen(ISqlInjector.class, globalConfig::setSqlInjector);//这里设置注入器属性,注入自定义注入器就在这里
//        // TODO 注入ID生成器
//        this.getBeanThen(IdentifierGenerator.class, globalConfig::setIdentifierGenerator);
//        // TODO 设置 GlobalConfig 到 MybatisSqlSessionFactoryBean
//        mybatisPlus.setGlobalConfig(globalConfig);


        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setSqlInjector(new XnwSqlInjector());
        mybatisPlus.setGlobalConfig(globalConfig);

        return mybatisPlus;
    }

}
