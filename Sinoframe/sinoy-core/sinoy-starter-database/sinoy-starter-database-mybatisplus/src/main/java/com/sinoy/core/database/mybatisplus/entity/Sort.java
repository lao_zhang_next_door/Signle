package com.sinoy.core.database.mybatisplus.entity;

import java.util.List;

/**
 * 排序类
 */
public class Sort {

    /**
     * 排序属性
     */
    private List<String> prop;

    /**
     * 排序方式：asc,desc
     */
    private List<String> order;

    public List<String> getProp() {
        return prop;
    }

    public void setProp(List<String> prop) {
        this.prop = prop;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }
}
