package com.sinoy.core.database.mybatisplus.injector.methods.select;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 根据guid查询
 */
public class SelectByGuid extends AbstractMethod {

    private String methodName = "selectByGuid";

    public SelectByGuid(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {

        // 自定义sql tableInfo.getTableName() 获取表名
        String sql = "select * from " + tableInfo.getTableName()+" where row_guid = #{rowGuid}";
        // mapper 接口方法名
        String method = "selectByGuid";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addSelectMappedStatementForTable(mapperClass, method, sqlSource,tableInfo);
    }
}
