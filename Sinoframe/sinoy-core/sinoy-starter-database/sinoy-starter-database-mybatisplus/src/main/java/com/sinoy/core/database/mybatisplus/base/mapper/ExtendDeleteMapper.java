package com.sinoy.core.database.mybatisplus.base.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.web.bind.annotation.RequestParam;

public interface ExtendDeleteMapper<T> extends BaseMapper<T> {

	/**
	 * 根据guid删除
	 * @return
	 */
	int deleteByGuid(@RequestParam("rowGuid") String rowGuid);

	/**
	 * 根据guid数组批量删除
	 * @param rowGuids
	 * @return
	 */
	int deleteBatchByGuids(String[] rowGuids);

	/**
	 * 根据guid数组批量逻辑删除
	 * @param rowGuids
	 * @return
	 */
	int logicDeleteBatchByGuids(String[] rowGuids);

	/**
	 * 根据id数组批量逻辑删除
	 * @param ids
	 * @return
	 */
	int logicDeleteBatchByIds(Long[] ids);

	/**
	 * 根据id逻辑删除
	 * @param id
	 * @return
	 */
	int logicDeleteBatchById(Long id);
}
