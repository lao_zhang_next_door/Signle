package com.sinoy.core.database.mybatisplus.base.mapper;



public interface ExtendMapper<T> extends ExtendInsertMapper<T>, ExtendSelectMapper<T>, ExtendUpdateMapper<T>, ExtendDeleteMapper<T> {


}
