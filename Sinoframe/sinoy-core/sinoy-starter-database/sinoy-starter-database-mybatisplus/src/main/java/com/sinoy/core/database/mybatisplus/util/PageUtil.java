package com.sinoy.core.database.mybatisplus.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.database.mybatisplus.entity.Search;


/**
 * 分页工具类
 *
 * @author pangu
 */
public class PageUtil {

	public static <T> IPage<T> getPage(Search search) {
		return new Page<T>(search.getCurrentPage(), search.getPageSize());
	}
}
