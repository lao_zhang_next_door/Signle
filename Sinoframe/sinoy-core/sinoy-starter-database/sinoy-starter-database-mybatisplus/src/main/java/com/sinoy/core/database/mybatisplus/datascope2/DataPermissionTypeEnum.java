//package com.sino.core.database.mybatisplus.datascope2;
//
//
///**
// * 数据权限枚举类
// *
// */
//public enum DataPermissionTypeEnum {
//	/**
//	 * 全部
//	 */
//	ALL(1, "全部数据权限",""),
//	/**
//	 * 仅本人数据
//	 */
//	SELF(2,"仅本人数据权限"," #{#userName} = #{#user.userId} "),
//	/**
//	 * 本部门数据权限
//	 */
//	THIS_DEPT_LEVEL(3, "本部门数据权限"," #{#deptName} = #{#user.deptId} "),
//	/**
//	 * 本部门及以下数据权限
//	 */
//	THIS_DEPT_LEVEL_CHILDREN(4, "本部门及以下数据权限"," #{#deptName} IN ( #{@sdss.getDeptAndChild( #user.deptId )} )"),
//	/**
//	 * 自定义部门数据权限
//	 */
//	CUSTOMIZE_DEPT(5, "自定义部门数据权限"," #{#deptName} IN ( #{@sdss.getRoleCustom( #user.roleId )} ) ");
//
//
//	private final int type;
//	private final String description;
//	/**
//	 * spel 模板表达式
//	 */
//	private String sqlTemplate;
//
//
//	public static DataPermissionTypeEnum valueOf(int type) {
//		for (DataPermissionTypeEnum typeVar : DataPermissionTypeEnum.values()) {
//			if (typeVar.getType() == type) {
//				return typeVar;
//			}
//		}
//		return ALL;
//	}
//
//	public int getType() {
//		return type;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	DataPermissionTypeEnum(int type, String description,String sqlTemplate) {
//		this.type = type;
//		this.description = description;
//		this.sqlTemplate= sqlTemplate;
//	}
//}
