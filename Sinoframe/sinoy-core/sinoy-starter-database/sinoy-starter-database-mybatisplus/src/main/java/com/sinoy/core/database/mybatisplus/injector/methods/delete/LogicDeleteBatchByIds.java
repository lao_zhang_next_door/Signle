package com.sinoy.core.database.mybatisplus.injector.methods.delete;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.sinoy.core.common.enums.DelFlag;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 根据ids 数组逻辑删除
 */
public class LogicDeleteBatchByIds extends AbstractMethod {

    private String methodName = "logicDeleteBatchByIds";

    public LogicDeleteBatchByIds(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        // 自定义sql tableInfo.getTableName() 获取表名
        String where = "<foreach item=\"rowId\" collection=\"array\" open=\"(\" separator=\",\" close=\")\">#{rowId}</foreach>";
        String sql = String.format("<script>\nUPDATE %s SET del_flag = " + DelFlag.Del.getCode() + " WHERE row_id in %s\n</script>", tableInfo.getTableName(), where);
        // mapper 接口方法名
        String method = "logicDeleteBatchByIds";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addDeleteMappedStatement(mapperClass, method, sqlSource);
    }
}
