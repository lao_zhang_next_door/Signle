//package com.sino.core.database.mybatisplus.datascope2;
//
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Objects;
//
///**
// * 数据权限查询参数
// *
// */
//public class DataScope extends HashMap {
//
//	/**
//	 * 限制范围的字段名称
//	 */
//	private String scopeName = "depart_id";
//
//	/**
//	 * 具体的数据范围
//	 */
//	private List<Integer> departIds = new ArrayList<>();
//
//	public String getScopeName() {
//		return scopeName;
//	}
//
//	public void setScopeName(String scopeName) {
//		this.scopeName = scopeName;
//	}
//
//	public List<Integer> getDepartIds() {
//		return departIds;
//	}
//
//	public void setDepartIds(List<Integer> departIds) {
//		this.departIds = departIds;
//	}
//
//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//		if (!super.equals(o)) return false;
//		DataScope dataScope = (DataScope) o;
//		return Objects.equals(scopeName, dataScope.scopeName) && Objects.equals(departIds, dataScope.departIds);
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(super.hashCode(), scopeName, departIds);
//	}
//}
