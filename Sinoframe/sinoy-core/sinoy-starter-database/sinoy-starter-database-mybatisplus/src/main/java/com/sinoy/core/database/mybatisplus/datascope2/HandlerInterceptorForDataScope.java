//package com.sino.core.database.mybatisplus.datascope;
//
//import com.sino.core.database.mybatisplus.datascope.context.DataPermissionThreadLocal;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * <p> 拦截器 -- 数据权限 </p>
// *
// */
//@Component
//public class HandlerInterceptorForDataScope implements HandlerInterceptor {
//
//
//    /**
//     * 在业务处理器处理请求之前被调用
//     */
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        return true;
//    }
//
//    /**
//     * 在业务处理器处理请求执行完成后，生成视图之前执行。
//     * 后处理（调用了Service并返回ModelAndView，但未进行页面渲染），有机会修改ModelAndView
//     */
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
//    }
//
//    /**
//     * 在DispatcherServlet完全处理完请求后被调用，可用于清理资源等。返回处理（已经渲染了页面）
//     */
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
//        DataPermissionThreadLocal.remove();
//    }
//
//}
