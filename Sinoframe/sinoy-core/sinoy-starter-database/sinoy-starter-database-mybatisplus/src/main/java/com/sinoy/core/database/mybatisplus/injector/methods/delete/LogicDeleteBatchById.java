package com.sinoy.core.database.mybatisplus.injector.methods.delete;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.sinoy.core.common.enums.DelFlag;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

public class LogicDeleteBatchById extends AbstractMethod {

    private String methodName = "logicDeleteBatchById";

    public LogicDeleteBatchById(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }
    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {

        // 自定义sql tableInfo.getTableName() 获取表名
        String sql = "update " + tableInfo.getTableName()+" set del_flag="+ DelFlag.Del.getCode()+" where row_id = #{id}";
        // mapper 接口方法名
        String method = "logicDeleteBatchById";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addDeleteMappedStatement(mapperClass, method, sqlSource);
    }
}
