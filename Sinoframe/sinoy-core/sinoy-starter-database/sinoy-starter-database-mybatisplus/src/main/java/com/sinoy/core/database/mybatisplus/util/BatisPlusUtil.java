package com.sinoy.core.database.mybatisplus.util;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.utils.dataformat.ObjectUtil;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.sql.BatisUtil;
import com.sinoy.core.database.mybatisplus.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

public class BatisPlusUtil {

    private static final Logger logger = LoggerFactory.getLogger(BatisPlusUtil.class);

    /**
     * mybatis pluus 查询实体类非空字段
     *
     * @param t
     * @param <T>
     */
    public static <T> void notNullField(T t, QueryWrapper<T> wrapper) {
        for (Field field : ObjectUtil.getAllFields(t.getClass())) {
            field.setAccessible(true);
            try {
                //序列化 字段不需要查询
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                //属性为空，不用查询
                if (field.get(t) == null) {
                    continue;
                }
                //主键 注解TableId
                TableId tableId = field.getAnnotation(TableId.class);
                if (tableId != null) {
                    //主键
                    wrapper.eq(tableId.value(), field.get(t));
                    continue;
                }
                //数据库中字段名和实体类属性不一致 注解TableField
                TableField tableField = field.getAnnotation(TableField.class);
                if (tableField != null) {
                    if (tableField.exist()) {
                        wrapper.eq(tableField.value(), field.get(t));
                    }// @TableField(exist = false) 不是表中内容 不形成查询条件
                    continue;
                }
                //数据库中字段名和实体类属性一致,默认转驼峰
                wrapper.eq(BatisUtil.toDbField(field.getName()), field.get(t));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Title: 设置参数
     * @params: queryWrapper 条件   params 通过 search.getParams()获取
     * @author: 郑小康
     * @Date: 2022/8/26/17:20
     * 若使用这个方法 请保证参数一定符合要求 否则禁止查询
     */
    public static <T> QueryWrapper<T> setParams(QueryWrapper<T> queryWrapper, Map<String, Object> params) {
        if (params == null) {
            return queryWrapper;
        }
        for (String key : params.keySet()) {
            if (params.get(key) == null) {
                continue;
            }
            if (key.contains("_")) {
                String preKey = key.substring(0, key.indexOf("_"));
                String paramsKey = key.substring(key.indexOf("_") + 1);
                switch (paramsKey) {
                    case "in":
                        queryWrapper.in(Arrays.asList(params.get(key)).size() > 0,BatisUtil.toDbField(preKey), params.get(key));
                        break;
                    //json_contains
                    case "jsonContains":
                        queryWrapper.apply(!"".equals(params.get(key)),"JSON_CONTAINS("+BatisUtil.toDbField(preKey)+",JSON_QUOTE({0}))", params.get(key));
                        break;
                    case "jsonNotContains":
                        queryWrapper.apply(!"".equals(params.get(key)),"NOT JSON_CONTAINS("+BatisUtil.toDbField(preKey)+",JSON_QUOTE({0}))", params.get(key));
                        break;
                    case "cache":
                        queryWrapper.like(BatisUtil.toDbField(preKey), params.get(key));
                        break;
                    case "endWith":
                        queryWrapper.likeLeft(BatisUtil.toDbField(preKey), params.get(key));
                    case "between":
                        String[] strings = String.valueOf(params.get(key)).split(",");
                        if(strings.length > 1){
                            queryWrapper.between(BatisUtil.toDbField(preKey),strings[0],strings[1]);
                        }
                        break;
                    case "startWith":
                        queryWrapper.likeRight(BatisUtil.toDbField(preKey), params.get(key));
                        break;
                    case "isNull":
                        queryWrapper.isNull(BatisUtil.toDbField(preKey));
                        break;
                    case "isNotNull":
                        queryWrapper.isNotNull(BatisUtil.toDbField(preKey));
                        break;
                    case "notEqual":
                        queryWrapper.ne(BatisUtil.toDbField(preKey), params.get(key));
                        break;
                    case "isNotBlank":
                        queryWrapper.and(i -> i.isNotNull(BatisUtil.toDbField(preKey)).ne(BatisUtil.toDbField(preKey), ""));
                        break;
                    case "isBlank":
                        queryWrapper.and(i -> i.isNull(BatisUtil.toDbField(preKey)).or().eq(BatisUtil.toDbField(preKey), ""));
                        break;
                    case "equal":
                        if (StringUtil.isNotBlank(String.valueOf(params.get(key)))) {
                            queryWrapper.eq(BatisUtil.toDbField(preKey), params.get(key));
                        }
                        break;
                    default:
                        if (StringUtil.isNotBlank(String.valueOf(params.get(key)))) {
                            queryWrapper.eq(BatisUtil.toDbField(preKey), params.get(key));
                        }
                        break;
                }
            } else {
                //自定义参数
//                throw new BaseException("请检查参数名称");
            }
        }
        return queryWrapper;
    }

    /**
     * @Title: 校验参数
     * @Description: 校验params中的参数在传过来的reqVO中是否存在
     * @return: void
     * @author: 郑小康
     * @Date: 2022/11/9/8:49
     */
    public static <T, M> void checkParamsIncluded(QueryWrapper<T> queryWrapper, M reqVO, Map<String, Object> params) {
        //获取reqVO中的属性
        Field[] fields = reqVO.getClass().getDeclaredFields();
        //用于存放属性
        ArrayList<String> classPropsList = new ArrayList<>();
        //遍历类中所有属性
        for (Field field : fields) {
            //获取属性名并添加进入
            classPropsList.add(field.getName());
        }
        try {
            if (params != null) {
                Set<String> paramsSet = params.keySet();
                Iterator iterator = paramsSet.iterator();

                while (iterator.hasNext()){
                    String prop = String.valueOf(iterator.next());
                    String keyProp = "";
                    if (prop.contains("_")) {
                        keyProp = prop.substring(0, prop.indexOf("_"));
                        if (!classPropsList.contains(keyProp)) {
                            throw new BaseException("参数不完整,请检查对应pageReqVo中是否包含您所传入字段!");
                        }else{
                            Field f = reqVO.getClass().getDeclaredField(keyProp);

                            // 前端传String 后端类型为int  此时则需要一次转换
                            if((f.getType() == Integer.class || f.getType() == int.class)
                                    &&  params.get(prop) instanceof String){
                                if(StringUtil.isNotBlank((String)params.get(prop))){
                                    params.put(prop,Integer.valueOf((String)params.get(prop)));
                                }else {
                                    iterator.remove();
                                }
                            };
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.getMessage());
        }
        //设置参数
        if (queryWrapper != null) {
            setParams(queryWrapper, params);
        }
    }

    public static void main(String[] args) {
//        Search s = new Search();
        BaseController s = new BaseController() {
        };
        Map<String, Object> map = new HashMap<>();
        map.put("prop_cache", "aaaa");
        map.put("prqwe_cache", "aaaa");

        checkParamsIncluded(null, s, map);
    }

    /**
     * 批量设置排序字段
     *
     * @param queryWrapper<T>
     * @param props           排序字段
     * @param orders          排序类型
     * @return
     */
    public static <T> QueryWrapper<T> setOrders(QueryWrapper<T> queryWrapper, List<String> props, List<String> orders) {

        if (props == null || orders == null || props.size() == 0 || props.size() != orders.size()) {
            logger.error("排序异常,使用默认排序");
            queryWrapper.orderByDesc("sort_sq").orderByDesc("row_id");
            return queryWrapper;
        }

        for (int i = 0; i < props.size(); i++) {
            queryWrapper.orderBy(true, "ASC".equals(orders.get(i).toUpperCase()), BatisUtil.toDbField(props.get(i)));
        }

        return queryWrapper;
    }
}
