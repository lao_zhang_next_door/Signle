package com.sinoy.core.database.mybatisplus.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sinoy.core.database.mybatisplus.entity.Sort;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface BaseService<T> extends IService<T> {

    //查询===============================================================================================================

    /**
     * 根据rowGuid 查询
     * @param rowGuid
     * @return
     */
    T selectByGuid(@RequestParam String rowGuid);

    /**
     * 根据非空实体查询
     * @param t
     * @return
     */
    List<T> selectByEntity(T t);

    /**
     * 根据非空实体查询并排序
     * @param t
     * @param sort
     * @return
     */
    List<T> selectByEntity(T t, Sort sort);

    /**
     * 根据非空实体查询单个
     * @param t
     * @return
     */
    T selectOneByEntity(T t);

    /**
     * 根据非空实体查询数量
     * @param t
     * @return
     */
    Long selectCountByEntity(T t);

    /**
     * 默认排序查询所有
     * @return
     */
    List<T> listAll();

    //更新===============================================================================================================

    /**
     * 根据guid 更新不为null的键值
     * @param t
     * @return
     */
    int updateByRowGuid(T t);

    //删除===============================================================================================================

    /**
     * 根据guid删除
     * @return
     */
    int deleteByGuid(@RequestParam("rowGuid") String rowGuid);

    /**
     * 根据guid数组批量删除
     * @param rowGuids
     * @return
     */
    int deleteBatchByGuids(String[] rowGuids);

    /**
     * 根据主键id 批量删除
     * @param ids
     * @return
     */
    int deleteBatchByIds(Long[] ids);

    /**
     * 根据guid数组批量逻辑删除
     * @param rowGuids
     * @return
     */
    int logicDeleteBatchByGuids(String[] rowGuids);

    /**
     * 根据id数组批量逻辑删除
     * @param ids
     * @return
     */
    int logicDeleteBatchByIds(Long[] ids);

    /**
     * 根据id逻辑删除
     * @param id
     * @return
     */
    int logicDeleteBatchById(Long id);


}
