//package com.sino.core.database.mybatisplus.props;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
///**
// * 租户属性
// */
//@Component
//@RefreshScope
//@ConfigurationProperties(prefix = "xnw.tenant")
//public class TenantProperties {
//
//    /**
//     * 是否开启租户模式
//     */
//    private Boolean enable = false;
//
//    /**
//     * 需要排除的多租户的表
//     */
//    private List<String> ignoreTables = Arrays.asList();
//
//    /**
//     * 多租户字段名称
//     */
//    private String column = "tenant_id";
//
//    /**
//     * 排除不进行租户隔离的sql
//     * 样例全路径：vip.mate.system.mapper.UserMapper.findList
//     */
//    private List<String> ignoreSqls = new ArrayList<>();
//
//    public Boolean getEnable() {
//        return enable;
//    }
//
//    public void setEnable(Boolean enable) {
//        this.enable = enable;
//    }
//
//    public List<String> getIgnoreTables() {
//        return ignoreTables;
//    }
//
//    public void setIgnoreTables(List<String> ignoreTables) {
//        this.ignoreTables = ignoreTables;
//    }
//
//    public String getColumn() {
//        return column;
//    }
//
//    public void setColumn(String column) {
//        this.column = column;
//    }
//
//    public List<String> getIgnoreSqls() {
//        return ignoreSqls;
//    }
//
//    public void setIgnoreSqls(List<String> ignoreSqls) {
//        this.ignoreSqls = ignoreSqls;
//    }
//}
