package com.sinoy.core.database.mybatisplus.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.sinoy.core.common.utils.factory.YamlPropertySourceFactory;
import com.sinoy.core.database.mybatisplus.handler.XnwMetaObjectHandler;
//import com.sino.core.database.mybatisplus.props.TenantProperties;
import org.apache.ibatis.type.EnumTypeHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * mybatis plus配置中心
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Configuration
@EnableTransactionManagement
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:xnw-db.yml")
//@MapperScan("com.sino.dao")
public class MybatisPlusConfiguration {

//	private final TenantProperties tenantProperties;
//
//	private final TenantLineInnerInterceptor tenantLineInnerInterceptor;
//
//	public MybatisPlusConfiguration(TenantProperties tenantProperties, TenantLineInnerInterceptor tenantLineInnerInterceptor) {
//		this.tenantProperties = tenantProperties;
//		this.tenantLineInnerInterceptor = tenantLineInnerInterceptor;
//	}

//	/**
//	 * 新的分页插件,一缓和二缓遵循mybatis的规则,
//	 * 需要设置 MybatisConfiguration#useDeprecatedExecutor = false
//	 * 避免缓存出现问题(该属性会在旧插件移除后一同移除)
//	 */
//	@Bean
//	public MybatisPlusInterceptor paginationInterceptor() {
////		boolean enableTenant = tenantProperties.getEnable();
//		MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
////		if (enableTenant) {
////			interceptor.addInnerInterceptor(tenantLineInnerInterceptor);
////		}
//		//分页插件: PaginationInnerInterceptor
//		PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
//		paginationInnerInterceptor.setMaxLimit(MAX_LIMIT);
//		//防止全表更新与删除插件: BlockAttackInnerInterceptor
//		BlockAttackInnerInterceptor blockAttackInnerInterceptor = new BlockAttackInnerInterceptor();
//		interceptor.addInnerInterceptor(paginationInnerInterceptor);
//		interceptor.addInnerInterceptor(blockAttackInnerInterceptor);
//		return interceptor;
//	}

//	/**
//	 * sql 注入
//	 */
//	@Bean
//	public ISqlInjector sqlInjector() {
//		return new XnwSqlInjector();
//	}

	/**
	 * 自动填充数据
	 */
	@Bean
	@ConditionalOnMissingBean(XnwMetaObjectHandler.class)
	public XnwMetaObjectHandler mateMetaObjectHandler() {
		return new XnwMetaObjectHandler();
	}

	/**
	 * IEnum 枚举配置
	 */
	@Bean
	public ConfigurationCustomizer configurationCustomizer() {
		return configuration -> {
			configuration.setDefaultEnumTypeHandler(EnumTypeHandler.class);
			// 关闭 mybatis 默认的日志
			configuration.setLogPrefix("log.mybatis");
		};
	}

	/**
	 * mybatis-plus 乐观锁拦截器
	 */
	@Bean
	public OptimisticLockerInnerInterceptor optimisticLockerInterceptor() {
		return new OptimisticLockerInnerInterceptor();
	}
}
