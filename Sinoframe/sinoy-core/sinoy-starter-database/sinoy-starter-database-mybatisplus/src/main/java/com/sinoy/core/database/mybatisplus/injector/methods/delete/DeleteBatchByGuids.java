package com.sinoy.core.database.mybatisplus.injector.methods.delete;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 根据rowGuids 数组物理删除
 */
public class DeleteBatchByGuids extends AbstractMethod {

    private String methodName = "deleteBatchByGuids";

    public DeleteBatchByGuids(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        // 自定义sql tableInfo.getTableName() 获取表名
        String where = "<foreach item=\"rowGuid\" collection=\"array\" open=\"(\" separator=\",\" close=\")\">#{rowGuid}</foreach>";
        String sql = String.format("<script>\nDELETE FROM %s WHERE row_guid in %s\n</script>", tableInfo.getTableName(), where);
        // mapper 接口方法名
        String method = "deleteBatchByGuids";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addDeleteMappedStatement(mapperClass, method, sqlSource);
    }
}
