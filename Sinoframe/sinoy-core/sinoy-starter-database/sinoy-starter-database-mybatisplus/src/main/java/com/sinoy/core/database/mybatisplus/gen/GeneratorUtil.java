//package com.sino.core.database.mybatisplus.gen;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.InjectionConfig;
//import com.baomidou.mybatisplus.generator.config.*;
//import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
//import com.baomidou.mybatisplus.generator.config.po.TableInfo;
//import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
//import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
//import com.sino.core.common.constant.StringPool;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 代码生成工具类
// */
//public class GeneratorUtil {
//
//    public static void execute(CodeConfig config) {
//        AutoGenerator mpg = new AutoGenerator();
//        // 全局配置
//        GlobalConfig gc = new GlobalConfig();
//        gc.setOutputDir(config.getOutputDir());
//
//        gc.setFileOverride(true);
//        //ActiveRecord特性
//        gc.setActiveRecord(false);
//        // XML ResultMap
//        gc.setBaseResultMap(true);
//        // XML columList
//        gc.setBaseColumnList(true);
//        gc.setEnableCache(false);
//        // 自动打开输出目录
//        gc.setOpen(false);
////        gc.setAuthor("");
//        gc.setSwagger2(true);
//        //主键策略
//        gc.setIdType(IdType.AUTO);
//        // 自定义文件命名，注意 %s 会自动填充表实体属性！
//        gc.setServiceName("%sService");
//        gc.setServiceImplName("%sServiceImpl");
//        gc.setMapperName("%sDao");
//        gc.setXmlName("%sMapper");
//
//        mpg.setGlobalConfig(gc);
//
//        // 数据源配置
//        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setDbType(config.getDbType());
//        dsc.setDriverName(config.getDriverName());
//        dsc.setUrl(config.getUrl());
//        dsc.setUsername(config.getUsername());
//        dsc.setPassword(config.getPassword());
//        dsc.setTypeConvert(new MySqlTypeConvert() {
//            // 自定义数据库表字段类型转换【可选】
//            @Override
//            public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
//                //将数据库中datetime转换成date
//                if (fieldType.toLowerCase().contains("datetime")) {
//                    return DbColumnType.LOCAL_DATE_TIME;
//                }
//                return (DbColumnType) super.processTypeConvert(globalConfig, fieldType);
//            }
//        });
//        mpg.setDataSource(dsc);
//
//        // 包配置
//        PackageConfig pc = new PackageConfig();
//        // 此处设置包名，需要自定义
//        pc.setParent(config.getPackageName());
//        pc.setXml("mapper");
//        mpg.setPackageInfo(pc);
//
//        // 自定义配置
//        InjectionConfig cfg = new InjectionConfig() {
//            @Override
//            public void initMap() {
//            }
//        };
//        //修改此生成路径
////        String path="/src/main/java/com/example/demo/system";
////        String oPath = System.getProperty("user.dir");
//        List<FileOutConfig> focList = new ArrayList<>();
//        focList.add(new FileOutConfig("/temp/mapper.xml.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名及地址 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return config.getOutputDir() + "/mapper/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//        focList.add(new FileOutConfig("/temp/controller.java.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                return config.getOutputDir()+ "/controller/" + tableInfo.getControllerName() + StringPool.DOT_JAVA;
//            }
//        });
//        focList.add(new FileOutConfig("/temp/service.java.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                return config.getOutputDir()+  "/service/" + tableInfo.getServiceName() + StringPool.DOT_JAVA;
//            }
//        });
//        focList.add(new FileOutConfig("/temp/serviceImpl.java.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                return config.getOutputDir()+ "/service/impl/" + tableInfo.getServiceImplName() + StringPool.DOT_JAVA;
//            }
//        });
//        focList.add(new FileOutConfig("/temp/entity.java.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                return config.getOutputDir()+ "/entity/" + tableInfo.getEntityName() + StringPool.DOT_JAVA;
//            }
//        });
//        focList.add(new FileOutConfig("/temp/mapper.java.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                return config.getOutputDir()+ "/mapper/" + tableInfo.getMapperName() + StringPool.DOT_JAVA;
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        mpg.setCfg(cfg);
//
//
//        // 配置模板
//        TemplateConfig templateConfig = new TemplateConfig();
//
//        // 配置自定义输出模板
//        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
//        templateConfig.setEntity(null);
//        templateConfig.setService(null);
//        templateConfig.setController(null);
//        templateConfig.setServiceImpl(null);
//        templateConfig.setMapper(null);
//        templateConfig.setXml(null);
//        mpg.setTemplate(templateConfig);
//
//        // 策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//        strategy.setSuperEntityClass("com.sino.core.database.mybatisplus.entity.BaseEntity");
//        strategy.setSuperServiceClass("com.sino.core.database.mybatisplus.base.service.BaseService");
//        strategy.setSuperServiceImplClass("com.sino.core.database.mybatisplus.base.service.BaseServiceImpl");
//        strategy.setSuperMapperClass("com.sino.core.database.mybatisplus.base.mapper.ExtendMapper");
//        strategy.setEntityLombokModel(false);
//        strategy.setRestControllerStyle(true);
//        // 公共父类
////        strategy.setSuperControllerClass("vip.mate.core.web.controller.BaseController");
//        // 写于父类中的公共字段
////        strategy.setSuperEntityColumns("id");
//        strategy.setInclude(config.getTableName().split(","));
//        strategy.setControllerMappingHyphenStyle(true);
//        if(config.getPrefix() != null){
//            strategy.setTablePrefix(config.getPrefix());
//        }
//        // strategy.setTablePrefix(pc.getModuleName() + "_");
//        mpg.setStrategy(strategy);
//        mpg.setTemplateEngine(new VelocityTemplateEngine());
//        mpg.execute();
//    }
//}
