package com.sinoy.core.database.mybatisplus.enums;


/**
 * 排序类型
 */
public enum OrderTypeEnum {

	/**
	 * 排序规则
	 * ASC 正序
	 * DESC 倒序
	 */
	ASC("asc"),
	DESC("desc");

	private final String value;

	public String getValue() {
		return value;
	}

	OrderTypeEnum(String value) {
		this.value = value;
	}
}
