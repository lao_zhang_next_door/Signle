//package com.sino.core.database.mybatisplus.datascope2;
//
//import cn.hutool.core.annotation.AnnotationUtil;
//import cn.hutool.core.collection.ConcurrentHashSet;
//import cn.hutool.core.util.ArrayUtil;
//import cn.hutool.core.util.ClassUtil;
//import cn.hutool.core.util.ObjectUtil;
//import cn.hutool.core.util.StrUtil;
//import com.sino.core.database.mybatisplus.datascope2.anno.DataColumn;
//import com.sino.core.database.mybatisplus.datascope2.anno.DataPermission;
//import com.sino.core.database.mybatisplus.datascope2.context.DataPermissionThreadLocal;
//import net.sf.jsqlparser.expression.*;
//import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
//import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
//import net.sf.jsqlparser.schema.Column;
//import net.sf.jsqlparser.schema.Table;
//import net.sf.jsqlparser.statement.select.PlainSelect;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.lang.reflect.Method;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.stream.Collectors;
//
//
///**
// * <p> mybatis-plus 数据权限处理器 </p>
// *
// * @author zhengqingya
// * @description {@link MyDataPermissionInterceptor}
// * @date 2022/1/10 17:37
// */
//public class MyDataPermissionHandler {
//
//    /**
//     * 方法或类(名称) 与 注解的映射关系缓存
//     */
//    private final Map<String, DataPermission> dataPermissionCacheMap = new ConcurrentHashMap<>();
//
//    /**
//     * 无效注解方法缓存用于快速返回
//     */
//    private final Set<String> inavlidCacheSet = new ConcurrentHashSet<>();
//
//    private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    /**
//     * 获取数据权限 SQL 片段
//     *
//     * @param plainSelect  查询对象
//     * @param whereSegment 查询条件片段
//     * @return JSqlParser 条件表达式
//     */
//    public Expression getSqlSegment(PlainSelect plainSelect, String whereSegment) {
//        // 待执行 SQL Where 条件表达式
//        Expression where = plainSelect.getWhere();
//        DataColumn[] dataColumns = findAnnotation(whereSegment);
//        if (ArrayUtil.isEmpty(dataColumns)) {
//            inavlidCacheSet.add(whereSegment);
//            return where;
//        }
//
//        // 获取权限过滤相关信息
//        UserPermissionInfo userPermissionInfo = DataPermissionThreadLocal.get();
//        if (userPermissionInfo == null) {
//            return where;
//        }
//        Table fromItem = (Table) plainSelect.getFromItem();
//        // 有别名用别名，无别名用表名，防止字段冲突报错
//        Alias fromItemAlias = fromItem.getAlias();
//        String mainTableName = fromItemAlias == null ? fromItem.getName() : fromItemAlias.getName();
//
//        // 获取mapper层信息
//        List<String> split = StrUtil.split(whereSegment, '.');
//        int index = split.size();
//        String method = split.get(index - 1);
//        String mapper = split.get(index - 2);
//
//
//        try {
//            DataPermissionTypeEnum dataPermissionTypeEnum = userPermissionInfo.getDataPermissionTypeEnum();
//            logger.info("[数据权限过滤] dataPermissionType:[{}]  where:[{}]  whereSegment:[{}]", dataPermissionTypeEnum, where, whereSegment);
//            Expression expression = new HexValue(" 1 = 1 ");
//            if (where == null) {
//                where = expression;
//            }
//
//            // 根据不同类型进行权限处理
//            switch (dataPermissionTypeEnum) {
//                // 查看全部
//                case ALL:
//                    return where;
//                // 查看自己的数据
//                case SELF:
//                    // create_by = userId
//                    EqualsTo selfEqualsTo = new EqualsTo();
//                    selfEqualsTo.setLeftExpression(new Column(mainTableName + ".create_by"));
//                    selfEqualsTo.setRightExpression(new LongValue(userPermissionInfo.getUserId()));
//                    AndExpression selfAndExpression = new AndExpression(where, selfEqualsTo);
//                    logger.info(" where {}", selfAndExpression);
//                    return selfAndExpression;
//                //本部门数据权限
//                case THIS_DEPT_LEVEL:
//                    EqualsTo selfDeptEqualsTo = new EqualsTo();
//                    selfDeptEqualsTo.setLeftExpression(new Column(mainTableName + ".dept_guid"));
//                    selfDeptEqualsTo.setRightExpression(new StringValue(userPermissionInfo.getDeptGuid()));
//                    AndExpression selfDeptAndExpression = new AndExpression(where, selfDeptEqualsTo);
//                    logger.info(" where {}", selfDeptAndExpression);
//                    return selfDeptAndExpression;
//
//
////                // 查看本人所在角色以及下属机构
////                case ROLE_AUTO:
////                    // 创建IN 表达式
////                    // 创建IN范围的元素集合
////                    Set<String> roleIdList = userPermissionInfo.getRoleIdList();
////                    // 把集合转变为JSQLParser需要的元素列表
////                    ItemsList itemsList = new ExpressionList(roleIdList.stream().map(LongValue::new).collect(Collectors.toList()));
////                    InExpression inExpression = new InExpression(new Column(mainTableName + ".create_role_id"), itemsList);
////                    AndExpression andExpression = new AndExpression(where, inExpression);
////                    logger.info(" where {}", andExpression);
////                    return andExpression;
////                // 查看当前角色的数据
////                case ROLE:
////                    //  = 表达式
////                    // role_id = roleId
////                    EqualsTo equalsTo = new EqualsTo();
////                    equalsTo.setLeftExpression(new Column(mainTableName + ".create_role_id"));
////                    equalsTo.setRightExpression(new LongValue(userPermissionInfo.getRoleId()));
////                    // 创建 AND 表达式 拼接Where 和 = 表达式
////                    // WHERE xxx AND role_id = 3
////                    AndExpression deptAndExpression = new AndExpression(where, equalsTo);
////                    logger.info(" where {}", deptAndExpression);
////                    return deptAndExpression;
////                case AUTO:
////                    return new AndExpression(where, new StringValue(userPermissionInfo.getSql()));
//                default:
//                    break;
//            }
//        } catch (Exception e) {
//            logger.error("MyDataPermissionHandler 数据权限处理异常：", e);
//        } finally {
//
//        }
//        return where;
//    }
//
//    private DataColumn[] findAnnotation(String mappedStatementId) {
//        StringBuilder sb = new StringBuilder(mappedStatementId);
//        int index = sb.lastIndexOf(".");
//        String clazzName = sb.substring(0, index);
//        String methodName = sb.substring(index + 1, sb.length());
//        Class<?> clazz = ClassUtil.loadClass(clazzName);
//        List<Method> methods = Arrays.stream(ClassUtil.getDeclaredMethods(clazz))
//                .filter(method -> method.getName().equals(methodName)).collect(Collectors.toList());
//        DataPermission dataPermission;
//        // 获取方法注解
//        for (Method method : methods) {
//            dataPermission = dataPermissionCacheMap.get(mappedStatementId);
//            if (ObjectUtil.isNotNull(dataPermission)) {
//                return dataPermission.value();
//            }
//            if (AnnotationUtil.hasAnnotation(method, DataPermission.class)) {
//                dataPermission = AnnotationUtil.getAnnotation(method, DataPermission.class);
//                dataPermissionCacheMap.put(mappedStatementId, dataPermission);
//                return dataPermission.value();
//            }
//        }
//        dataPermission = dataPermissionCacheMap.get(clazz.getName());
//        if (ObjectUtil.isNotNull(dataPermission)) {
//            return dataPermission.value();
//        }
//        // 获取类注解
//        if (AnnotationUtil.hasAnnotation(clazz, DataPermission.class)) {
//            dataPermission = AnnotationUtil.getAnnotation(clazz, DataPermission.class);
//            dataPermissionCacheMap.put(clazz.getName(), dataPermission);
//            return dataPermission.value();
//        }
//        return null;
//    }
//
//    /**
//     * 是否为无效方法 无数据权限
//     */
//    public boolean isInvalid(String mappedStatementId) {
//        return inavlidCacheSet.contains(mappedStatementId);
//    }
//
//
//}
