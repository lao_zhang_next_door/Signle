package com.sinoy.core.database.mybatisplus.injector.methods.delete;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 根据guid删除
 */
public class DeleteByGuid extends AbstractMethod {

    private String methodName = "deleteByGuid";

    public DeleteByGuid(String methodName) {
        super(methodName);
        this.methodName = methodName;
    }
    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {

        // 自定义sql tableInfo.getTableName() 获取表名
        String sql = "delete from " + tableInfo.getTableName()+" where row_guid = #{rowGuid}";
        // mapper 接口方法名
        String method = "deleteByGuid";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addDeleteMappedStatement(mapperClass, method, sqlSource);
    }
}
