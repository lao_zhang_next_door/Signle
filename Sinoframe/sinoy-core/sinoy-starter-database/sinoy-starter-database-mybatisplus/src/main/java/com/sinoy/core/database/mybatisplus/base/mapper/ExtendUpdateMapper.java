package com.sinoy.core.database.mybatisplus.base.mapper;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ExtendUpdateMapper<T> extends BaseMapper<T> {

    default void updateBatch(T update) {
        update(update, new QueryWrapper<>());
    }

}
