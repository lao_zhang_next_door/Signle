package com.sinoy.core.database.tk.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sinoy.core.common.enums.DelFlag;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


public class BaseEntity  implements Serializable{
	
	public BaseEntity(){}
	
	public BaseEntity(boolean init) {
		if(init){
			this.init();
		}
	}

	public BaseEntity(String rowGuid) {
		if(!StringUtils.isEmpty(rowGuid)){
			this.setRowGuid(rowGuid);
		}
	}
	
	public void init(){
		this.rowGuid = UUID.randomUUID().toString();
		this.createTime = LocalDateTime.now();
		this.updateTime = LocalDateTime.now();
		this.delFlag = DelFlag.Normal.getCode();
		this.sortSq = 0;
	}
	
	//初始化默认值为null的字段 若有值则不进行初始化
	public void initNull(){
		if(this.rowGuid == null){
			this.rowGuid = UUID.randomUUID().toString();
		}
		if(this.createTime == null){
			this.createTime = LocalDateTime.now();
		}
		if(this.updateTime == null){
			this.updateTime = LocalDateTime.now();
		}
		if(this.delFlag == null){
			this.delFlag = DelFlag.Normal.getCode();
		}
		if(this.sortSq == null){
			this.sortSq = 0;
		}
	}


	//主键
	@Id
	@GeneratedValue(generator = "JDBC")
	protected Integer rowId;
	//rowGuid
	protected String rowGuid;
	//创建时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	protected LocalDateTime createTime;
	//更新时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	protected LocalDateTime updateTime;
	//删除标识(默认0)
	protected Integer delFlag;
	//排序号(默认0)
	@OrderBy(value="DESC")
	protected Integer sortSq;
	
	
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public String getRowGuid() {
		return rowGuid;
	}
	public void setRowGuid(String rowGuid) {
		this.rowGuid = rowGuid;
	}
	public LocalDateTime getCreateTime() {
		return createTime;
	}
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}
	public LocalDateTime getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	public Integer getSortSq() {
		return sortSq;
	}
	public void setSortSq(Integer sortSq) {
		this.sortSq = sortSq;
	}
	

}
