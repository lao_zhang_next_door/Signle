package com.sinoy.core.database.tk.base.service;

import java.util.List;

public interface BaseInsertService<T> {
	
	/**
     * 保存一个实体，null的属性也会保存，不会使用数据库默认值
     *
     * @param record
     * @return
     */
    int insert(T record);
    
    /**
     * 保存一个实体，null的属性不会保存，会使用数据库默认值
     *
     * @param record
     * @return
     */
    int insertSelective(T record);
    
    /**
     * 批量保存
     * @param list
     * @return
     */
    int insertList(List<T> list);
    
    

}
