package com.sinoy.core.database.tk.base.mapper;

import com.sinoy.core.database.tk.mybatis.mapper.annotation.RegisterMapper;
import com.sinoy.core.database.tk.mybatis.mapper.provider.base.BaseDeleteProvider;
import org.apache.ibatis.annotations.DeleteProvider;


@RegisterMapper
public interface ExtendDeleteMapper<T> {

	/**
	 * 根据rowGuids 数组批量删除
	 * @param rowGuids
	 * @return
	 */
	@DeleteProvider(type = BaseDeleteProvider.class, method = "dynamicSQL")
	int deleteBatch(String[] rowGuids);
}
