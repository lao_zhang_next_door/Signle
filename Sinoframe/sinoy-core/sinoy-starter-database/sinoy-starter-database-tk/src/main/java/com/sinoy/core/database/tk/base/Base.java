package com.sinoy.core.database.tk.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

public class Base extends com.sinoy.core.common.base.Base {
	
	@Autowired
	protected HttpSession session;
	
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

}
