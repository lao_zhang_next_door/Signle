package com.sinoy.core.database.tk.base;


import com.sinoy.core.database.tk.base.mapper.ExtendMapper;
import com.sinoy.core.database.tk.mybatis.mapper.common.Mapper;

public interface BaseDao<T> extends Mapper<T>, ExtendMapper<T> {



}
