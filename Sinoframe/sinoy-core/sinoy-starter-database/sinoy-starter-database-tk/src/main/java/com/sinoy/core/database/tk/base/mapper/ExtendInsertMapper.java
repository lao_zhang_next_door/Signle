package com.sinoy.core.database.tk.base.mapper;

import com.sinoy.core.database.tk.mybatis.mapper.annotation.RegisterMapper;
import com.sinoy.core.database.tk.mybatis.mapper.provider.base.BaseInsertProvider;
import org.apache.ibatis.annotations.InsertProvider;

import java.util.List;


@RegisterMapper
public interface ExtendInsertMapper<T> {
	
	/**
     * 保存一个实体，null的属性不会保存，会使用数据库默认值
     *
     * @return
     */
    @InsertProvider(type = BaseInsertProvider.class, method = "dynamicSQL")
    int insertList(List<T> list);

}
