package com.sinoy.core.database.tk.sql;

import com.sinoy.core.common.utils.dataformat.Underline2Camel;
import org.springframework.util.StringUtils;


/**
 * SQL过滤
 * @ClassName: SQLFilter
 * @Description: SQL过滤
 * @author keeny
 * @date 2019年1月8日 下午1:37:55
 *
 */
public class SQLFilter {

    /**
     * SQL注入过滤
     * @param str  待验证的字符串
     */
    public static String sqlInject(String str,boolean flag){
        if(StringUtils.isEmpty(str)){
            return null;
        }
        //去掉'|"|;|\字符
        str = StringUtils.replace(str, "'", "");
        str = StringUtils.replace(str, "\"", "");
        str = StringUtils.replace(str, ";", "");
        str = StringUtils.replace(str, "\\", "");
        //转换成小写
        str=flag? Underline2Camel.camel2Underline(str):str.toLowerCase();
        //非法字符
        String[] keywords = {"master", "truncate", "insert", "select", "delete", "update", "declare", "alert", "drop"};

        //判断是否包含非法字符
        for(String keyword : keywords){
            if(str.contains(keyword)){
                throw new RuntimeException("包含非法字符");
            }
        }
        return str;
    }

    /**
     * 关键词校验
     * @param str
     * @return
     */
    public static String sqlValidate(String str) {
        // 统一转为小写
        String testStr = str.toLowerCase();
        // 过滤掉的sql关键字，可以手动添加
        String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|*|%|chr|mid|master|truncate|" +
                "char|declare|sitename|net user|xp_cmdshell|;|or|-|+|,|like'|and|exec|execute|insert|create|drop|" +
                "table|from|grant|use|group_concat|column_name|" +
                "information_schema.columns|table_schema|union|where|select|delete|update|order|by|count|*|" +
                "chr|mid|master|truncate|char|declare|or|;|-|--|+|,|like|//|/|%|#";
        String[] badStrs = badStr.split("\\|");
        for (int i = 0; i < badStrs.length; i++) {
            if (testStr.indexOf(badStrs[i]) >= 0) {
                throw new RuntimeException("包含非法字符");
            }
        }
        return str;
    }


}
