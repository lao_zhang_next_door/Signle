package com.sinoy.core.database.tk.base.mapper;


import com.sinoy.core.database.tk.mybatis.mapper.annotation.RegisterMapper;

@RegisterMapper
public interface ExtendMapper<T> extends ExtendInsertMapper<T>, ExtendSelectMapper<T>, ExtendUpdateMapper<T>, ExtendDeleteMapper<T> {

    
    
	
	
	
}
