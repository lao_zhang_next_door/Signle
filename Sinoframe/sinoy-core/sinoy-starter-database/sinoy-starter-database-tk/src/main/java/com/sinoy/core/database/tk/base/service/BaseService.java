package com.sinoy.core.database.tk.base.service;

public interface BaseService<T> extends
        BaseSelectService<T>, BaseInsertService<T>, BaseDeleteService<T>,
        BaseUpdateService<T> {



}
