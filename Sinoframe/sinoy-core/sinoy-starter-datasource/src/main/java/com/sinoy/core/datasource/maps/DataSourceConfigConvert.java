package com.sinoy.core.datasource.maps;

import com.sinoy.core.datasource.entity.DataSourceConfigDO;
import com.sinoy.core.datasource.entity.vo.DataSourceConfigCreateReqVO;
import com.sinoy.core.datasource.entity.vo.DataSourceConfigRespVO;
import com.sinoy.core.datasource.entity.vo.DataSourceConfigUpdateReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 数据源配置 Convert
 *
 */
@Mapper
public interface DataSourceConfigConvert {

    DataSourceConfigConvert INSTANCE = Mappers.getMapper(DataSourceConfigConvert.class);

    DataSourceConfigDO convert(DataSourceConfigCreateReqVO bean);

    DataSourceConfigDO convert(DataSourceConfigUpdateReqVO bean);

    DataSourceConfigRespVO convert(DataSourceConfigDO bean);

    List<DataSourceConfigRespVO> convertList(List<DataSourceConfigDO> list);

}
