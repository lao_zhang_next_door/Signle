package com.sinoy.core.datasource.controller.admin;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.datasource.entity.DataSourceConfigDO;
import com.sinoy.core.datasource.entity.vo.DataSourceConfigCreateReqVO;
import com.sinoy.core.datasource.entity.vo.DataSourceConfigUpdateReqVO;
import com.sinoy.core.datasource.maps.DataSourceConfigConvert;
import com.sinoy.core.datasource.service.DataSourceConfigService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 数据源配置
 */
@RestController
@RequestMapping("/frame/data-source-config")
@Validated
public class DataSourceConfigController {

    @Resource
    private DataSourceConfigService dataSourceConfigService;

    /**
     * 创建数据源配置
     * @param createReqVO
     * @return
     */
    @PostMapping("/create")
    public R createDataSourceConfig(@Valid @RequestBody DataSourceConfigCreateReqVO createReqVO) {
        return R.ok().put("data",dataSourceConfigService.createDataSourceConfig(createReqVO));
    }

    /**
     * 更新数据源配置
     * @param updateReqVO
     * @return
     */
    @PutMapping("/update")
    public R updateDataSourceConfig(@Valid @RequestBody DataSourceConfigUpdateReqVO updateReqVO) {
        dataSourceConfigService.updateDataSourceConfig(updateReqVO);
        return R.ok();
    }

    /**
     * 删除数据源配置
     * @param rowId 编号
     * @return
     */
    @DeleteMapping("/delete")
    public R deleteDataSourceConfig(@RequestParam("rowId") Long rowId) {
        dataSourceConfigService.deleteDataSourceConfig(rowId);
        return R.ok();
    }

    /**
     * 获得数据源配置
     * @param rowId
     * @return
     */
    @GetMapping("/get")
    public R getDataSourceConfig(@RequestParam("rowId") Long rowId) {
        DataSourceConfigDO dataSourceConfig = dataSourceConfigService.getDataSourceConfig(rowId);
        return R.ok().put("data", DataSourceConfigConvert.INSTANCE.convert(dataSourceConfig));
    }

    /**
     * 获得数据源配置列表
     * @return
     */
    @GetMapping("/list")
    public R getDataSourceConfigList() {
        List<DataSourceConfigDO> list = dataSourceConfigService.getDataSourceConfigList();
        return R.ok().put("data",DataSourceConfigConvert.INSTANCE.convertList(list));
    }

}
