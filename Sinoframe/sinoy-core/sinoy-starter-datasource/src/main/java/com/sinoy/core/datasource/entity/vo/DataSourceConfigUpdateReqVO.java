package com.sinoy.core.datasource.entity.vo;

import javax.validation.constraints.NotNull;

public class DataSourceConfigUpdateReqVO extends DataSourceConfigBaseVO {

    //主键编号
    @NotNull(message = "主键编号不能为空")
    private Long rowId;

    //密码
    @NotNull(message = "密码不能为空")
    private String password;

    public Long getRowId() {
        return rowId;
    }

    public DataSourceConfigUpdateReqVO setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public DataSourceConfigUpdateReqVO setPassword(String password) {
        this.password = password;
        return this;
    }
}
