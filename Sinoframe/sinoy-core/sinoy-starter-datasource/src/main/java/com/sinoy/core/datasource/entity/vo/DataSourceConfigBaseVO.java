package com.sinoy.core.datasource.entity.vo;

import javax.validation.constraints.NotNull;

/**
* 数据源配置 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
public class DataSourceConfigBaseVO {

    //数据源名称
    @NotNull(message = "数据源名称不能为空")
    private String name;

    //数据源连接
    @NotNull(message = "数据源连接不能为空")
    private String url;

    //用户名
    @NotNull(message = "用户名不能为空")
    private String username;

    public String getName() {
        return name;
    }

    public DataSourceConfigBaseVO setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public DataSourceConfigBaseVO setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public DataSourceConfigBaseVO setUsername(String username) {
        this.username = username;
        return this;
    }
}
