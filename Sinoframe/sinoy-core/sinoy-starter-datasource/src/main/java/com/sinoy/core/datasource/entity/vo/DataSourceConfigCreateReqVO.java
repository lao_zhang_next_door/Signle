package com.sinoy.core.datasource.entity.vo;

import javax.validation.constraints.NotNull;
import java.util.Objects;


public class DataSourceConfigCreateReqVO extends DataSourceConfigBaseVO {

    //密码
    @NotNull(message = "密码不能为空")
    private String password;

    public String getPassword() {
        return password;
    }

    public DataSourceConfigCreateReqVO setPassword(String password) {
        this.password = password;
        return this;
    }

    public DataSourceConfigCreateReqVO(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataSourceConfigCreateReqVO that = (DataSourceConfigCreateReqVO) o;
        return Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password);
    }
}
