package com.sinoy.core.datasource.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.sinoy.core.datasource.dao")
@Configuration
public class DataSourceMapperConfig {
}
