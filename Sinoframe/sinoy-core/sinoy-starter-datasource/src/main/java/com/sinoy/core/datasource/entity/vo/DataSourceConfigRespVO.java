package com.sinoy.core.datasource.entity.vo;

import java.util.Date;


public class DataSourceConfigRespVO extends DataSourceConfigBaseVO {

    //主键编号
    private Long rowId;

    //创建时间
    private Date createTime;

    public Long getRowId() {
        return rowId;
    }

    public DataSourceConfigRespVO setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public DataSourceConfigRespVO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public DataSourceConfigRespVO(Long rowId, Date createTime) {
        this.rowId = rowId;
        this.createTime = createTime;
    }

    public DataSourceConfigRespVO() {
    }
}
