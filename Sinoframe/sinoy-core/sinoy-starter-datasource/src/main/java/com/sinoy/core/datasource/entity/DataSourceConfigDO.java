package com.sinoy.core.datasource.entity;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

/**
 * 数据源配置
 */
@TableName(value = "frame_data_source_config", autoResultMap = true)
@KeySequence("frame_data_source_config_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class DataSourceConfigDO extends BaseEntity {

    /**
     * 主键编号 - Master 数据源
     */
    public static final Long ID_MASTER = 0L;

    /**
     * 连接名
     */
    private String name;

    /**
     * 数据源连接
     */
    private String url;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
//    @TableField(typeHandler = EncryptTypeHandler.class)
    private String password;

    public static Long getIdMaster() {
        return ID_MASTER;
    }

    public String getName() {
        return name;
    }

    public DataSourceConfigDO setName(String name) {
        this.name = name;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public DataSourceConfigDO setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public DataSourceConfigDO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public DataSourceConfigDO setPassword(String password) {
        this.password = password;
        return this;
    }

    public DataSourceConfigDO(Long rowId) {
        super(rowId);
    }

    public DataSourceConfigDO(String name, String url, String username, String password) {
        this.name = name;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public DataSourceConfigDO() {
    }
}
