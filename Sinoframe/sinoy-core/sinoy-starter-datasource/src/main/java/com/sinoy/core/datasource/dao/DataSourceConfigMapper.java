package com.sinoy.core.datasource.dao;


import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.datasource.entity.DataSourceConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据源配置 Mapper
 *
 */
@Mapper
public interface DataSourceConfigMapper extends ExtendMapper<DataSourceConfigDO> {
}
