function evalObj(expression) {
    var ret = eval(expression);
    var obj = {
        value: ret,
        type: isArray(ret)? "array" : typeof ret
    };
    return stringify(obj);
}
function isArray(a) {
    return a.constructor === Array;
}
function escFunc(m) {
    var escMap = { '"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t' };
    return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1);
}
function stringify(value) {
    var escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
    if (value == null) {
        return 'null';
    }
    else if (typeof value === 'number') {
        return isFinite(value) ? value.toString() : 'null';
    }
    else if (typeof value === 'boolean') {
        return value.toString();
    }
    else if (typeof value === 'object') {
        if (isArray(value)) {
            var res = '[';
            for (var i = 0; i < value.length; i++) {
                res += (i ? ', ' : '') + stringify(value[i]);
            }
            return res + ']';
        }
        else if (value.toString() === '[object Object]') {
            var tmp = [];
            for (var k in value) {
                if (value.hasOwnProperty(k)) {
                    tmp.push(stringify(k) + ': ' + stringify(value[k]));
                }
            }
            return '{' + tmp.join(', ') + '}';
        }
    }
    return '"' + value.toString().replace(escRE, escFunc) + '"';
}

function dateFormat(strdate, fmt, isDate) { 
    if (strdate == null) {
        return "";
    }
    var date;
    if (typeof strdate == 'object') {
        date = strdate;
    }
    else if (isDate === undefined) {
        date = new Date(strdate.replace(/-/g, "/"));
        if (date !== "Invalid Date") {
        } else {
            return strdate;
        }
    } else {
        date = strdate;
    }

    if (fmt === undefined) {
        fmt = "yyyy-MM-dd";
    }
    var verify = function (Rex) {
        var arr = new RegExp(Rex).exec(fmt);
        if (!arr)
            return "";
        return arr[0];
    };
    var common = function (r, rex) {
        len === 2 ? fmt = fmt.replace(rex, o[r].length === 1 ? "0" + o[r] : o[r]) : fmt = fmt.replace(rex, o[r]);
    };
    var o = {
        "y+": date.getFullYear() + "",
        "q+": Math.floor((date.getMonth() + 3) / 3),
        "M+": date.getMonth() + 1 + "",
        "d+": date.getDate() + "",
        "H+": date.getHours() + "",
        "h+": date.getHours() + "",
        "m+": date.getMinutes() + "",
        "s+": date.getSeconds() + "",
        "S+": date.getMilliseconds()
    };
    for (var r in o) {
        var rex, len, temp;
        rex = new RegExp(r);
        temp = verify(rex);
        len = temp.length;
        if (!len || len === 0)
            continue;
        if (r === "y+") {
            len === 2 ? fmt = fmt.replace(rex, o[r].substr(2, 3)) : fmt = fmt.replace(rex, o[r]);
        } else if (r === "q+") {
            fmt = fmt.replace(rex, o[r]);
        } else if (r === "h+") {
            var h = (o[r] > 12 ? o[r] - 12 : o[r]) + "";
            len === 2 ? fmt = fmt.replace(rex, h.length === 1 ? "0" + h : h) : fmt = fmt.replace(rex, h);
        } else if (r === "S+") {
            fmt = fmt.replace(rex, o[r]);
        } else {
            common(r, rex);
        }
    }
    return fmt;
}

var decimal = function () {

    function isInteger(obj) {
        return Math.floor(obj) === obj;
    }

    function toInteger(floatNum) {
        var ret = { times: 1, num: 0 };
        if (isInteger(floatNum)) {
            ret.num = floatNum;
            return ret;
        }
        var strfi = floatNum + '';
        var dotPos = strfi.indexOf('.');
        var len = strfi.substr(dotPos + 1).length;
        if (dotPos === -1) {
            len = 0;
        }
        var times = Math.pow(10, len);
        var intNum = parseInt(strfi.replace('.', ''));
        ret.times = times;
        ret.num = intNum;
        return ret;
    }

    function operation(a, b, op) {
        var o1 = toInteger(a);
        var o2 = toInteger(b);
        var n1 = o1.num;
        var n2 = o2.num;
        var t1 = o1.times;
        var t2 = o2.times;
        var max = t1 > t2 ? t1 : t2;
        var result = null;
        switch (op) {
            case 'add':
                if (t1 === t2) {
                    result = n1 + n2;
                } else if (t1 > t2) {
                    result = n1 + n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) + n2;
                }
                return result / max;
            case 'subtract':
                if (t1 === t2) {
                    result = n1 - n2;
                } else if (t1 > t2) {
                    result = n1 - n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) - n2;
                }
                return result / max;
            case 'multiply':
                result = (n1 * n2) / (t1 * t2);
                return result;
            case 'divide':
                result = (n1 / n2) * (t2 / t1);
                return result;
        }
    }

    function add(a, b) {
        return operation(a, b, 'add');
    }
    function subtract(a, b) {
        return operation(a, b, 'subtract');
    }
    function multiply(a, b) {
        return operation(a, b, 'multiply');
    }
    function divide(a, b) {
        return operation(a, b, 'divide');
    }
    function toFixed(num, digits) {
        var times = Math.pow(10, digits);
        var des = num * times;
        return parseInt(des, 10) / times;
    }

    return {
        add: add,
        subtract: subtract,
        multiply: multiply,
        divide: divide,
        toFixed: toFixed
    }
}();
function plus(a, b) {
    return decimal.add(a, b);
}
function subtract(a, b) {
    return decimal.subtract(a, b);
}
function multiply(a, b) {
    return decimal.multiply(a, b);
}
function divide(a, b) {
    return decimal.divide(a, b);
}
function toFixed(num, digits) {
    return decimal.toFixed(num, digits);
}