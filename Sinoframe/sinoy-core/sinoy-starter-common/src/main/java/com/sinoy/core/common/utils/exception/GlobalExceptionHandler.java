package com.sinoy.core.common.utils.exception;

import com.sinoy.core.common.utils.request.R;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
   *  统一异常处理类
  * @ClassName: GlobalExceptionHandler 
  * @Description: 统一异常处理类
  * @date 2018年11月8日 上午10:31:14 
  *
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(BaseException.class)
    public R handleMyException(BaseException e){
        R r = new R();
        r.put("code", e.getStatus());
        r.put("msg", e.getMsg());
        r.put("detail",e.getMessage());
        return r;
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R globalBindException(HttpServletRequest request, MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        if (bindingResult.hasErrors()) {
            return R.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        return R.error();
    }

    public R allExceptionHandler(HttpServletRequest request, Throwable ex) {
        return R.error(ex.getMessage());
    }
}
