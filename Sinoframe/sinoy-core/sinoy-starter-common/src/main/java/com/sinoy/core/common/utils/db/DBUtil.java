package com.sinoy.core.common.utils.db;

import java.sql.*;
import java.util.*;

/**
 * 数据库操作工具类
 *
 * @author Administrator
 *
 */
public class DBUtil {
    private static Connection con = null;// 连接对象
    private static PreparedStatement psmt = null;// 预编译对象
    private static ResultSet rs = null;// 结果集对象
    private static CallableStatement csmt = null;// 过程对象
    /**
     *  获得连接对象
     * @return
     */
    public static Connection getConnetion(String qd,String jdbcUrl,String sqlName,String sqlPassword) {
        try {

            Class.forName(qd);
            con = DriverManager
                    .getConnection(jdbcUrl,sqlName,sqlPassword);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("找不到驱动");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.println("数据库连接失败");
        }
        return con;
    }

    /**
     *
     * @param sql
     * @return
     */
    public static PreparedStatement getPreparedStatement(String qd,String jdbcUrl,String sqlName,String sqlPasswor,String sql) {
        con = getConnetion(qd,jdbcUrl,sqlName,sqlPasswor);
        try {
            psmt = con.prepareStatement(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return psmt;
    }
//    /**
//     * 查询获得所有
//     * @param sql
//     * @return 结果集
//     */
//    public static ResultSet getResultSet(String sql) {
//        psmt = getPreparedStatement(sql);
//        try {
//            rs = psmt.executeQuery();
//        } catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return rs;
//    }

    /**
     * 更新操作
     * @param sql
     * @param params
     * @return   受影响的行数
     */
    public static int executeUpdate(String qd,String jdbcUrl,String sqlName,String sqlPasswor,String sql, List<Object> params) {
        int count = 0;

        psmt = getPreparedStatement(qd,jdbcUrl,sqlName,sqlPasswor,sql);
        bindPramas(psmt, params);
        try {
            count = psmt.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            CloseAll();
        }

        return count;
    }

    /**
     * 查询  带参数
     * @param sql
     * @param params
     * @return  结果集
     */
    public static ResultSet executeQuery(String qd,String jdbcUrl,String sqlName,String sqlPasswor,String sql, List<Object> params) {
        psmt = getPreparedStatement(qd,jdbcUrl,sqlName,sqlPasswor,sql);
        if (params != null) {
            bindPramas(psmt, params);
        }
        try {
            rs = psmt.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(psmt);
        return rs;
    }

    public static CallableStatement getCallableStatement(String qd,String jdbcUrl,String sqlName,String sqlPasswor,String sql) {
        try {
            csmt = getConnetion(qd,jdbcUrl,sqlName,sqlPasswor).prepareCall(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return csmt;

    }


    public static Object executeCallObject(String qd,String jdbcUrl,String sqlName,String sqlPasswor,String sql,
                                           Map<Integer, Object> params) {
        Object obj = null;
        try {
            int index = 0;// 输入参数下标
            csmt = getCallableStatement(qd,jdbcUrl,sqlName,sqlPasswor,sql);
            for (int i = 1; i <= params.size(); i++) {
                if (params.get(i).toString().equals("Integer")) {
                    csmt.registerOutParameter(i, Types.INTEGER);
                    index = i;
                } else {
                    csmt.setObject(i, params.get(i));
                }
            }
            csmt.execute();
            obj = csmt.getObject(index);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj;
    }
    public static void bindPramas(PreparedStatement psmt, List<Object> params) {
        int index = 0;
        if (params != null) {
            for (Object p : params) {
                try {
                    psmt.setObject(index + 1, p);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                index++;

            }

        }
    }

    public static void CloseAll() {
        try {
            if (con != null) {

                con.close();

            }
            if (psmt != null) {
                psmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
