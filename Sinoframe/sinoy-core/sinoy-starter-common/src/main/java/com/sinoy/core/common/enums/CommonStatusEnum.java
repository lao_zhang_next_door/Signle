package com.sinoy.core.common.enums;

import java.util.Arrays;

/**
 * 通用状态枚举
 *
 */
public enum CommonStatusEnum  {

    ENABLE(1, "开启"),
    DISABLE(0, "关闭");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(CommonStatusEnum::getStatus).toArray();


    /**
     * 状态值
     */
    private final Integer status;
    /**
     * 状态名
     */
    private final String name;

    public int[] array() {
        return ARRAYS;
    }

    public static int[] getARRAYS() {
        return ARRAYS;
    }

    public Integer getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    CommonStatusEnum(Integer status, String name) {
        this.status = status;
        this.name = name;
    }
}
