package com.sinoy.core.common.utils.request;

import com.sinoy.core.common.exception.ErrorCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**  
* <p>Title: R</p>  
* <p>Description: 返回值工具类</p>  
* @author hero    
*/ 
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public R() {
		put("code", 0);

	}
	
	public static R error() {
		return error(500, "未知异常，请联系管理员");
	}
	
	public static R error(String msg) {
		return error(500, msg);
	}
	
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}
	
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}
	
	public static R list(Integer count,List<?> data){
		R r = new R();
		Map<String,Object> res = new HashMap<>();
		res.put("count", count);
		res.put("list", data);
		return r.put("data",res);
	}
	
	public static R ok() {
		return new R();
	}

	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}

	public static R error(ResultCode res) {
		return R.error(res.getCode(),res.getMsg());
	}

	public static R error(ErrorCode res) {
		return R.error(res.getCode(),res.getMsg());
	}
}
