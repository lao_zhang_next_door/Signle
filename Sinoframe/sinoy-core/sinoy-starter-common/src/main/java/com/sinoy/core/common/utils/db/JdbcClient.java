package com.sinoy.core.common.utils.db;


import com.sinoy.core.common.utils.exception.BaseException;
import org.apache.poi.util.StringUtil;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class JdbcClient<T> {

    private static Connection con = null;// 连接对象
    private static PreparedStatement psmt = null;// 预编译对象
    private static ResultSet rs = null;// 结果集对象
    private static CallableStatement csmt = null;// 过程对象

    public static void setConnection(String qd,String jdbcUrl,String sqlName,String sqlPassword){
        try {
            Class.forName(qd);
            con = DriverManager
                    .getConnection(jdbcUrl,sqlName,sqlPassword);
        } catch (ClassNotFoundException e) {
            throw new BaseException("找不到驱动");
        } catch (Exception e) {
            throw new BaseException("数据库连接失败");
        }
    }

    public static void setConnection(Connection connection){
        try {
            con = connection;
        } catch (Exception e) {
            throw new BaseException("数据库连接失败");
        }
    }

    /**
     *
     * @param sql
     * @return
     */
    public static PreparedStatement getPreparedStatement(String sql) {
        try {
            psmt = con.prepareStatement(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return psmt;
    }

    /**
     * 查询  带参数
     * @param sql
     * @param params
     * @return  结果集
     */
    public static ResultSet executeQuery(String sql, List<Object> params) {
        getPreparedStatement(sql);
        if (params != null) {
            bindPramas(psmt, params);
        }
        try {
            rs = psmt.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rs;
    }

    public boolean add(String tableName, Class<?> clazz,T t) {
        if(clazz == null) return false;
        //先构建添加功能的sql语句
        String buildSql = buildSql(tableName,clazz);
        System.out.println("=> Preparing: "+buildSql.toString());
        try {
            PreparedStatement prepareStatement = con.prepareStatement(buildSql);
            Field[] fields = clazz.getDeclaredFields();
            for(int i=1; i < fields.length; i++) {
                //设置每个字段的值可访问
                fields[i].setAccessible(true);
                //get方法获取每个字段的值
                prepareStatement.setObject(i, fields[i].get(t));
            }
            return prepareStatement.executeUpdate() > 0;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    public static void bindPramas(PreparedStatement psmt, List<Object> params) {
        int index = 0;
        if (params != null) {
            for (Object p : params) {
                try {
                    psmt.setObject(index + 1, p);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                index++;

            }

        }
    }

    public void CloseAll() {
        try {
            if (con != null) {

                con.close();

            }
            if (psmt != null) {
                psmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static String buildSql(String tableName,Class<?> t) {
        // TODO Auto-generated method stub
        String sql = "";

        //动态构建添加功能的sql语句
        //getSimpleName获取传入对象的名字
        //getDeclaredFields来获取对象t中的字段属性
        //sql1代表要插入的字段
        String sql1 = "insert into " + tableName + "(";
        Field[] declaredFields = t.getDeclaredFields();
        //将获得的字段属性加入到sql语句中
        for(Field field : declaredFields) {
            sql1 += toDbField(field.getName()) + ",";
        }
        //将多出来的一个，除去
        sql1 = sql1.substring(0, sql1.length()-1) + ")";
        //sql2代表要插入的values
        String sql2 = " values(null,";
        String[] params = new String[declaredFields.length-1];
        Arrays.fill(params, "?");
        //将params使用，分隔开
        sql2 += StringUtil.join(params, ",") + ")";
        sql = sql1 + sql2;
        return sql;
    }

    /**
     * 实体字段转换为数据库字段
     *
     * @param entityField 实体字段
     * @return
     */
    public static String toDbField(String entityField) {
        String dbField = "";
        StringBuilder sb = new StringBuilder();
        sb.append(entityField);
        //获得每个排序字段的所有字母
        char[] chars = entityField.toCharArray();
        int num = 0, index = 0;
        //循环每个单词的字母数组，如果是大写的就进行转换
        for (char aChar : chars) {
            if (Character.isUpperCase(aChar)) {
                int i1 = index + num;// 因为每次循环变更后都多了一个下划线
                sb.replace(i1, i1 + 1, "_" + String.valueOf(Character.toLowerCase(aChar)));
                num++;
            }
            index++;
        }
        dbField = sb.toString();
        return dbField;
    }












}
