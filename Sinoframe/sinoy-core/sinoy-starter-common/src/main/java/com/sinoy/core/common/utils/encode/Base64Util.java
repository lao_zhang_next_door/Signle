package com.sinoy.core.common.utils.encode;

import java.util.Base64;

public class Base64Util {
    /**
     * Base64
     *
     */
    public static void base64(String str) {

    }

    /**
     * BASE64加密解密
     */
    public static void enAndDeCode(String str) {
        String data = encryptBASE64(str.getBytes());
        System.out.println("sun.misc.BASE64 加密后：" + data);

        byte[] byteArray = decryptBASE64(data);
        System.out.println("sun.misc.BASE64 解密后：" + new String(byteArray));
    }

    /**
     * BASE64解密
     * @throws Exception
     */
    public static byte[] decryptBASE64(String key){
    	Base64.Decoder encoder = Base64.getDecoder();
        return encoder.decode(key);  
    }

    /**
     * BASE64加密
     */
    public static String encryptBASE64(byte[] key) {
    	Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(key);
    }
    /**
     * BASE64加密
     */
    public static String encryptBASE64(String key) {
        String data = encryptBASE64(key.getBytes());
        return data;
    }
    

}
