package com.sinoy.core.common.utils.dataformat;

import com.sinoy.core.common.utils.application.ApplicationUtils;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class JSCaller {

    private static final String jsCodes = ApplicationUtils.loadApplicationResource("/values/jsCall.js");

    private static ThreadLocal<ScriptEngine> engine = new ThreadLocal<>();

    public static String doJavaScript(String value){
        getJSEngin();
        return getJavaScriptString(jsCodes, value);
    }

    private static String getJavaScriptString(String jsCodes, String value) {
        try {
            engine.get().eval(jsCodes);

            Invocable invocable = (Invocable) engine.get();
            //调用js中的方法
            Object res = invocable.invokeFunction("evalObj", "(" + value + ")");
            return res.toString();
        } catch (ScriptException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static void getJSEngin(){
        if(engine.get() == null){
            ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("javascript");
            engine.set(scriptEngine);
        }
    }
}
