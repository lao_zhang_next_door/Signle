package com.sinoy.core.common.utils.file;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * pdf
 */
public class PdfUtil {

    /**
     * 转pdf 根据传入文件后缀
     */
    public static void  toPdf(String fileNameSuffix,String inputFile,String outputFile){

        String[] docArr = new String[]{".doc",".docx",".xls",".xlsx"};
        String[] imgArr = new String[]{".jpg",".png"};
        if(Arrays.asList(docArr).contains(fileNameSuffix)){
            if(".doc".equals(fileNameSuffix)){
                docTopdf(DocumentType.DOC,inputFile,outputFile);
            }

            if(".docx".equals(fileNameSuffix)){
                docTopdf(DocumentType.DOCX,inputFile,outputFile);
            }

            if(".xls".equals(fileNameSuffix)){
                docTopdf(DocumentType.XLS,inputFile,outputFile);
            }

            if(".xlsx".equals(fileNameSuffix)){
                docTopdf(DocumentType.XLSX,inputFile,outputFile);
            }

        }

        else if(Arrays.asList(imgArr).contains(fileNameSuffix)){
            imgToPdf(inputFile,outputFile);
        }

        else{}

    }

    /**
     * 转pdf doc docx xls xlsx
     * @param documentType
     * @param inputWord 输入doc文件
     * @param outputFile 输出pdf文件
     */
    public static void docTopdf(DocumentType documentType, String inputWord,String outputFile) {
        try  {
            InputStream docxInputStream = new FileInputStream(inputWord);
            OutputStream outputStream = new FileOutputStream(outputFile);
            IConverter converter = LocalConverter.builder().build();
            converter.convert(docxInputStream).as(documentType).to(outputStream).as(DocumentType.PDF).execute();
            outputStream.close();
            System.out.println("pdf转换成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 图片转pdf
     * @param imagesPath
     * @param outputFile 输出pdf文件
     */
    public static void imgToPdf(String imagesPath,String outputFile){
        try {
            // 第一步：创建一个document对象。
            Document document = new Document();
            document.setMargins(0, 0, 0, 0);
            // 第二步：
            // 创建一个PdfWriter实例，
            PdfWriter.getInstance(document, new FileOutputStream(outputFile));
            // 第三步：打开文档。
            document.open();
            // 第四步：在文档中增加图片。
            if (true) {
                Image img = Image.getInstance(imagesPath);
                img.setAlignment(Image.ALIGN_CENTER);
                // 根据图片大小设置页面，一定要先设置页面，再newPage（），否则无效
                document.setPageSize(new Rectangle(img.getWidth(), img.getHeight()));
                document.newPage();
                document.add(img);
                // 第五步：关闭文档。
                document.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * 合并pdf文件
     * @param files  文件路径 列表
     * @param outputFile 输出pdf文件
     * @return
     */
    public static boolean mergePdfFiles(String[] files, File outputFile) {
        boolean retValue = false;
        Document document = null;
        try {
            PdfReader pdfReader = new PdfReader(files[0]);
            document = new Document(pdfReader.getPageSize(1));
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            PdfCopy copy = new PdfCopy(document, fileOutputStream);
            document.open();
            for (int i = 0; i < files.length; i++) {
                PdfReader reader = new PdfReader(files[i]);
                int n = reader.getNumberOfPages();
                for (int j = 1; j <= n; j++) {
                    document.newPage();
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);
                }
                reader.close();
            }
            retValue = true;
            copy.close();
            pdfReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document != null) document.close();
        }
        return retValue;
    }

    /**
     * 读取整个pdf
     * @Param filePath 文件路径
     */
    public static String readPdfByPage(String filePath){
        String result = "";
        File file = new File(filePath);
        FileInputStream in = null;
        try {
            in = new FileInputStream(filePath);
            // 新建一个PDF解析器对象
            PdfReader reader = new PdfReader(filePath);
            reader.setAppendable(true);
            // 对PDF文件进行解析，获取PDF文档页码
            int size = reader.getNumberOfPages();
            for(int i = 1 ; i < size + 1; ){
                //一页页读取PDF文本
                String pageStr = PdfTextExtractor.getTextFromPage(reader,i);
                result = result + pageStr + "\n" + "PDF解析第"+ (i)+ "页\n";
                i= i+1;
            }
            reader.close();
        } catch (Exception e) {
            System.out.println("读取PDF文件" + file.getAbsolutePath() + "生失败！" + e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                }
            }
        }
        return result;
    }

    /**
     * 读取某几页pdf文本
     * @param filePath 文件路径
     * @param from 开始页
     * @param end 结束页
     * @return
     */
    public static String readPdfByPage(String filePath, int from, int end) {
        String result = "";
        File file = new File(filePath);
        FileInputStream in = null;
        try {
            in = new FileInputStream(filePath);
            // 新建一个PDF解析器对象
            PdfReader reader = new PdfReader(filePath);
            reader.setAppendable(false);
            // 对PDF文件进行解析，获取PDF文档页码
            int size = reader.getNumberOfPages();
            for(int i = from ; i <= end && i< size;i++ ){
                //一页页读取PDF文本
                String pageStr = PdfTextExtractor.getTextFromPage(reader,i);
                result = result + pageStr + "\n" + "PDF解析第"+ i+ "页\n";
            }
            reader.close();
        } catch (Exception e) {
            System.out.println("读取PDF文件" + file.getAbsolutePath() + "生失败！" + e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                }
            }
        }
        return result;
    }

    /**
     * pdfBox 读取pdf
     * @return
     */
    public static String readPdfByBox(String filePath){
        try {
            PDDocument pdDocument = PDDocument.load(new File(filePath));
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            return pdfTextStripper.getText(pdDocument);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * PDF文件转PNG图片，全部页数
     *
     * @param PdfFilePath  pdf完整路径
     * @param dstImgFolder 图片存放的文件夹
     * @param dpi dpi越大转换后越清晰，相对转换速度越慢  72.0F
     * @return 返回转换后图片集合list
     */
    public static java.util.List<File> pdfToImage(String PdfFilePath, String dstImgFolder, float dpi) {
        UUID uuid = UUID.randomUUID();
        String uuId = uuid.toString();
        System.out.println(uuId);
        File file = new File(PdfFilePath);
        //定义集合保存返回图片数据
        java.util.List<File> fileList = new ArrayList<File>();
        @SuppressWarnings("resource")//抑制警告
        PDDocument pdDocument = new PDDocument();
        try {
            //String imagePDFName = file.getName().substring(0, dot); // 获取图片文件名
            String imgFolderPath = null;
            if (dstImgFolder.equals("")) {
                imgFolderPath = dstImgFolder + File.separator + uuId;// 获取图片存放的文件夹路径
            } else {
                imgFolderPath = dstImgFolder + File.separator + uuId;
            }
            if (createDirectory(imgFolderPath)) {
                pdDocument = PDDocument.load(file);
                PDFRenderer renderer = new PDFRenderer(pdDocument);
                /* dpi越大转换后越清晰，相对转换速度越慢 */
                PdfReader reader = new PdfReader(PdfFilePath);
                int pages = reader.getNumberOfPages();
                //System.out.println("pdf总共多少页-----" + pages);
                StringBuffer imgFilePath = null;
                for (int i = 0; i < pages; i++) {
                    String imgFilePathPrefix = imgFolderPath + File.separator + "study";
                    //System.out.println("文件夹地址imgFilePathPrefix=====" + imgFilePathPrefix);
                    imgFilePath = new StringBuffer();
                    imgFilePath.append(imgFilePathPrefix);
                    imgFilePath.append("-");
                    imgFilePath.append(String.valueOf(i+1));
                    imgFilePath.append(".jpg");
                    File dstFile = new File(imgFilePath.toString());
                    BufferedImage image = renderer.renderImageWithDPI(i, dpi);
                    ImageIO.write(image, "png", dstFile);
                    fileList.add(dstFile);
                }
                System.out.println("PDF文档转PNG图片成功！");
                return fileList;
            } else {
                System.out.println("PDF文档转PNG图片失败：" + "创建" + imgFolderPath + "失败");
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //创建文件夹
    private static boolean createDirectory(String folder) {
        File dir = new File(folder);
        if (dir.exists()) {
            return true;
        } else {
            return dir.mkdirs();
        }
    }

    public static void main(String[] args) {

        pdfToImage("D:\\新建文件夹\\_mnt_zm_zjgzhdag_406婚姻登记处_13_2020_406_406-离婚登记类-2020-01795_406-离婚登记类-2020-01795.pdf","D:\\新建文件夹",72.0F);

//        String content = readPdfByPage("D:\\新建文件夹\\1004551.pdf");
//        String content1 = readPdfByBox("D:\\新建文件夹\\_mnt_zm_zjgzhdag_406婚姻登记处_13_2020_406_406-离婚登记类-2020-01795_406-离婚登记类-2020-01795.pdf");
//
//
//        System.out.println(content);
//        System.out.println(content1);
    }

}
