package com.sinoy.core.common.enums;

import cn.hutool.core.util.ArrayUtil;

import java.util.Arrays;

/**
 * 全局用户类型枚举
 */
public enum UserTypeEnum {

    MEMBER(1, "普通用户"), // 面向 c 端，普通用户
    ADMIN(2, "管理员"), // 面向 b 端，管理后台
    EXTERNAL(3,"外部接口"); // 外部接口

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(UserTypeEnum::getValue).toArray();

    /**
     * 类型
     */
    private final Integer value;
    /**
     * 类型名
     */
    private final String name;

    public static UserTypeEnum valueOf(Integer value) {
        return ArrayUtil.firstMatch(userType -> userType.getValue().equals(value), UserTypeEnum.values());
    }

    public int[] array() {
        return ARRAYS;
    }

    public static int[] getARRAYS() {
        return ARRAYS;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    UserTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }
}
