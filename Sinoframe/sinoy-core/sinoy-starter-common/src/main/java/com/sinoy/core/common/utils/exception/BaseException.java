package com.sinoy.core.common.utils.exception;

import com.sinoy.core.common.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * 通用异常
 *
 * @author hero
 */
public class BaseException extends RuntimeException {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private static final long serialVersionUID = 5782968730281544562L;

	private int status = INTERNAL_SERVER_ERROR.value();

	private String msg;

	public BaseException(String message) {
		super(message);
		this.msg = message;
	}

	public BaseException(HttpStatus status, String message) {
		super(message);
		this.status = status.value();
		this.msg = message;
	}

	public BaseException(HttpStatus status) {
		super(status.getReasonPhrase());
		this.status = status.value();
		this.msg = status.getReasonPhrase();
	}

	public BaseException(ErrorCode status) {
		super(status.getMsg());
		this.status = status.getCode();
		this.msg = status.getMsg();
	}

	public BaseException(int code,String msg) {
		super(msg);
		this.status = code;
		this.msg = msg;
	}

//	public BaseException(ErrorCode status,String msg) {
//		String formatMsg = doFormat(status.getCode(),status.getMsg(),msg);
////		super(formatMsg);
//		this.status = status.getCode();
//		this.msg = formatMsg;
//	}

	public BaseException(String message,Object msg) {
		String formatMsg = doFormat(500,message,msg);
		this.status = 500;
		this.msg = formatMsg;
	}

	public BaseException(ErrorCode status,Object msg) {
		String formatMsg = doFormat(status.getCode(),status.getMsg(),msg);
//		super(formatMsg);
		this.status = status.getCode();
		this.msg = formatMsg;
	}

	public BaseException(ErrorCode status,Object... msg) {
		String formatMsg = doFormat(status.getCode(),status.getMsg(),msg);
//		super(formatMsg);
		this.status = status.getCode();
		this.msg = formatMsg;
	}

	public int getStatus() {
		return status;
	}

	public String getMsg() {
		return msg;
	}

	/**
	 * 将错误编号对应的消息使用 params 进行格式化。
	 *
	 * @param code           错误编号
	 * @param messagePattern 消息模版
	 * @param params         参数
	 * @return 格式化后的提示
	 */
	public String doFormat(int code, String messagePattern, Object... params) {
		StringBuilder sbuf = new StringBuilder(messagePattern.length() + 50);
		int i = 0;
		int j;
		int l;
		for (l = 0; l < params.length; l++) {
			j = messagePattern.indexOf("{}", i);
			if (j == -1) {
				log.error("[doFormat][参数过多：错误码({})|错误内容({})|参数({})", code, messagePattern, params);
				if (i == 0) {
					return messagePattern;
				} else {
					sbuf.append(messagePattern.substring(i));
					return sbuf.toString();
				}
			} else {
				sbuf.append(messagePattern, i, j);
				sbuf.append(params[l]);
				i = j + 2;
			}
		}
		if (messagePattern.indexOf("{}", i) != -1) {
			log.error("[doFormat][参数过少：错误码({})|错误内容({})|参数({})", code, messagePattern, params);
		}
		sbuf.append(messagePattern.substring(i));
		return sbuf.toString();
	}
}
