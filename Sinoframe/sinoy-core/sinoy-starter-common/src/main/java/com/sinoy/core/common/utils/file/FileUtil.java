package com.sinoy.core.common.utils.file;


import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.sinoy.core.common.utils.dataformat.JsonFormatTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 文件工具类
 */
public class FileUtil {

    private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 生成.json格式文件
     */
    public static boolean createJsonFile(String jsonString, String filePath, String fileName) {
        // 标记文件生成是否成功
        boolean flag = true;

        // 拼接文件完整路径
        String fullPath = filePath + File.separator + fileName + ".json";

        // 生成json格式文件
        try {
            // 保证创建一个新文件
            File file = new File(fullPath);
            if (!file.getParentFile().exists()) { // 如果父目录不存在，创建父目录
                file.getParentFile().mkdirs();
            }
            if (file.exists()) { // 如果已存在,删除旧文件
                file.delete();
            }
            file.createNewFile();

            // 格式化json字符串
            jsonString = JsonFormatTool.formatJson(jsonString);

            // 将格式化后的字符串写入文件
            Writer write = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
            write.write(jsonString);
            write.flush();
            write.close();
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }

        // 返回是否成功的标记
        return flag;
    }
    public static String readFile(String filePath){
        return readFile(filePath, true);
    }
    public static String readFile(String filePath, boolean showError){
    	StringBuilder result = new StringBuilder();
        try{
        	Reader r = new FileReader(filePath);
            BufferedReader br = new BufferedReader(r);//构造一个BufferedReader类来读取文件
            String s = "";
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();    
        }catch(Exception e){
            if(showError){
                e.printStackTrace();
            }
        }
        return result.toString().trim();
    }

	public static byte[] fileToByte(String filePath) {

        InputStream in = null;//将该文件加入到输入流之中
        try {
            in = new FileInputStream(new File(filePath));
            byte[] body = new byte[in.available()];// 返回下一次对此输入流调用的方法可以不受阻塞地从此输入流读取（或跳过）的估计剩余字节数
            in.read(body);//读入到输入流里面
            return body;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getContentType(String fileName) {
        String FilenameExtension = fileName.substring(fileName.lastIndexOf("."));
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "application/x-bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ) {
            return "image/jpeg";
        }
        if ( FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/png";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xla") ||
                FilenameExtension.equalsIgnoreCase(".xlc")||
                FilenameExtension.equalsIgnoreCase(".xlm")||
                FilenameExtension.equalsIgnoreCase(".xls")||
                FilenameExtension.equalsIgnoreCase(".xlt")||
                FilenameExtension.equalsIgnoreCase(".xlw")) {
            return "application/vnd.ms-excel";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        if (FilenameExtension.equalsIgnoreCase(".pdf")) {
            return "application/pdf";
        }
        if (FilenameExtension.equalsIgnoreCase(".zip")) {
            return "application/zip";
        }
        if (FilenameExtension.equalsIgnoreCase(".tar")) {
            return "application/x-tar";
        }
        if (FilenameExtension.equalsIgnoreCase(".avi")) {
            return "video/avi";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp4")) {
            return "video/mpeg4";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp3")) {
            return "audio/mp3";
        }
        if (FilenameExtension.equalsIgnoreCase(".mp2")) {
            return "audio/mp2";
        }
        return "application/octet-stream";
    }

    public static byte[] margeFileByBytes(List<byte[]> fileList){
        int length_byte = 0;
        for(byte[] data : fileList){
            length_byte += data.length;
        }
        byte[] all_byte = new byte[length_byte];
        int countLength = 0;
        for (byte[] data : fileList) {
            System.arraycopy(data, 0, all_byte, countLength, data.length);
            countLength += data.length;
        }
        return all_byte;
    }

    public static void mergeFile(List<File> fileList, String FileName){
        FileOutputStream fos=null;
        FileInputStream fis=null;
        logger.info("合并文件开始");
        try
        {
            fos = new FileOutputStream(new File(FileName));
            byte[] buff=new byte[1024];
            int length=0;
            for(File file:fileList)
            {
                fis= new FileInputStream(file);
                while((length=fis.read(buff)) != -1)
                {
                    fos.write(buff,0,length);
                    fos.flush();
                }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                fos.close();
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        logger.info("合并文件结束");

    }

    public static MultipartFile transformMultipartFile(File file, String name){
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            return new MockMultipartFile(name, file.getName(), getContentType(file.getName()), fileInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static MultipartFile transformMultipartFile(byte[] data, String fileName, String name){
        FileInputStream fileInputStream = null;
        try {
            InputStream inputStream = new ByteArrayInputStream(data);
            return new MockMultipartFile(name, fileName, getContentType(fileName), inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * MultipartFile 转 File
     *
     * @param file
     * @throws Exception
     */
    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }

    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建临时文件
     * 该文件会在 JVM 退出时，进行删除
     *
     * @param data 文件内容
     * @return 文件
     */
    public static File createTempFile(String data) {
        File file = createTempFile();
        // 写入内容
        cn.hutool.core.io.FileUtil.writeUtf8String(data, file);
        return file;
    }

    /**
     * 创建临时文件
     * 该文件会在 JVM 退出时，进行删除
     *
     * @param data 文件内容
     * @return 文件
     */
    public static File createTempFile(byte[] data) {
        File file = createTempFile();
        // 写入内容
        cn.hutool.core.io.FileUtil.writeBytes(data, file);
        return file;
    }

    /**
     * 创建临时文件，无内容
     * 该文件会在 JVM 退出时，进行删除
     *
     * @return 文件
     */
    public static File createTempFile() {
        // 创建文件，通过 UUID 保证唯一
        File file = null;
        try {
            file = File.createTempFile(IdUtil.simpleUUID(), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 标记 JVM 退出时，自动删除
        file.deleteOnExit();
        return file;
    }

    /**
     * 生成文件路径   请保证 content 一定要传  否则容易生成 一模一样的path
     *
     * @param content      文件内容
     * @param originalName 原始文件名
     * @return path，唯一不可重复
     */
    public static String generatePath(byte[] content, String originalName) {
        String sha256Hex = DigestUtil.sha256Hex(content);
        // 情况一：如果存在 name，则优先使用 name 的后缀
        if (StrUtil.isNotBlank(originalName)) {
            String extName = FileNameUtil.extName(originalName);
            return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/" +(StrUtil.isBlank(extName) ? sha256Hex : sha256Hex + "." + extName);
        }
        // 情况二：基于 content 计算
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/" + sha256Hex + '.' + FileTypeUtil.getType(new ByteArrayInputStream(content));
    }
}
