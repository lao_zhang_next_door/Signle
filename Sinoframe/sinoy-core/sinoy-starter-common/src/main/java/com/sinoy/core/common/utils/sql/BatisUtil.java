package com.sinoy.core.common.utils.sql;

public class BatisUtil {

    /**
     * 实体字段转换为数据库字段
     *
     * @param entityField 实体字段
     * @return
     */
    public static String toDbField(String entityField) {
        String dbField = "";
        StringBuilder sb = new StringBuilder();
        sb.append(entityField);
        //获得每个排序字段的所有字母
        char[] chars = entityField.toCharArray();
        int num = 0, index = 0;
        //循环每个单词的字母数组，如果是大写的就进行转换
        for (char aChar : chars) {
            if (Character.isUpperCase(aChar)) {
                int i1 = index + num;// 因为每次循环变更后都多了一个下划线
                sb.replace(i1, i1 + 1, "_" + String.valueOf(Character.toLowerCase(aChar)));
                num++;
            }
            index++;
        }
        dbField = sb.toString();
        return dbField;
    }


    /**
     * 将数据库字段转换为实体字段
     *
     * @param dbField
     * @return
     */
    public static String toEntityField(String dbField) {
        StringBuilder sb = new StringBuilder();
        sb.append(dbField);
        int index = 0, num = 0;
        char[] chars = dbField.toCharArray();
        for (char aChar : chars) {
            //找到_位置并且该位置不是最后一位
            if (aChar == '_' && index != chars.length - 1) {
                int i1 = index - num;
                sb.replace(i1, i1 + 2, String.valueOf(Character.toUpperCase(chars[index + 1])));
                num++;
            }
            index++;
        }
        return sb.toString();
    }



}
