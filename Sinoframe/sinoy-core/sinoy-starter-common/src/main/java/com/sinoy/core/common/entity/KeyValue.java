package com.sinoy.core.common.entity;

/**
 * Key Value 的键值对
 *
 */
public class KeyValue<K, V> {

    private K key;
    private V value;

    public KeyValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public KeyValue<K, V> setKey(K key) {
        this.key = key;
        return this;
    }

    public V getValue() {
        return value;
    }

    public KeyValue<K, V> setValue(V value) {
        this.value = value;
        return this;
    }
}
