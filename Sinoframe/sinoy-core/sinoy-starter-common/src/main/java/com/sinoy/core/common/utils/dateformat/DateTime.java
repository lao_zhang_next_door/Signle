package com.sinoy.core.common.utils.dateformat;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTime {

    private Date value;
    private boolean isCompare = false;
    private int totalMonth;

    public DateTime(Object date, int totalMonth){
        value = toDate(date);
        this.totalMonth = totalMonth;
        this.isCompare = true;
    }

    public DateTime(Object date){
        value = toDate(date);
    }

    public DateTime date(){
        return new DateTime(toString("yyyy-MM-dd"));
    }

    public String getWeekday(){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        return sdf.format(value);
    }
    /**
     * 增加年份
     * @param years
     * @return
     */
    public DateTime addYears(int years){
        Calendar c = getCalender(value);
        c.add(Calendar.YEAR, years);
        return new DateTime(c.getTime());
    }

    /**
     * 增加月份
     * @param months
     * @return
     */
    public DateTime addMonths(int months){
        Calendar c = getCalender(value);
        c.add(Calendar.MONTH, months);
        return new DateTime(c.getTime());
    }

    /**
     * 增加天数
     * @param days
     * @return
     */
    public DateTime addDays(int days){
        Calendar c = getCalender(value);
        c.add(Calendar.DATE, days);
        return new DateTime(c.getTime());
    }

    /**
     * 增加小时
     * @param hours
     * @return
     */
    public DateTime addHours(int hours){
        Calendar c = getCalender(value);
        c.add(Calendar.HOUR, hours);
        return new DateTime(c.getTime());
    }

    /**
     * 增加小时
     * @param minutes
     * @return
     */
    public DateTime addMinutes(int minutes){
        Calendar c = getCalender(value);
        c.add(Calendar.MINUTE, minutes);
        return new DateTime(c.getTime());
    }

    /**
     * 增加秒
     * @param seconds
     * @return
     */
    public DateTime addSeconds(int seconds){
        Calendar c = Calendar.getInstance();
        c.setTime(value);
        c.add(Calendar.SECOND, seconds);
        return new DateTime(c.getTime());
    }

    /**
     * 增加毫秒
     * @param milliseconds
     * @return
     */
    public DateTime addMilliseconds(int milliseconds){
        Calendar c = Calendar.getInstance();
        c.setTime(value);
        c.add(Calendar.MILLISECOND, milliseconds);
        return new DateTime(c.getTime());
    }


    public Calendar getCalender(Date date) {
        Calendar cd = Calendar.getInstance();
        if (date != null) {
            cd.setTime(date);
        }
        return cd;
    }

    public Calendar getCalender() {
        Calendar cd = Calendar.getInstance();
        cd.setTime(value);
        return cd;
    }

    public int getYear(){
        return getCalender().get(Calendar.YEAR);
    }

    public int getMonth(){
        return getCalender().get(Calendar.MONTH) + 1;
    }

    public int getDate(){
        return getCalender().get(Calendar.DAY_OF_MONTH);
    }

    public int getHour(){
        return getCalender().get(Calendar.HOUR_OF_DAY);
    }

    public int getMinute(){
        return getCalender().get(Calendar.MINUTE);
    }

    public int getSecond(){
        return getCalender().get(Calendar.SECOND);
    }

    public int getDay(){
        return getCalender().get(Calendar.DAY_OF_WEEK);
    }



    /**
     * 获取某天0：00：00
     *
     * @return Date
     */
    public DateTime getDayStart(Date dt) {
        return new DateTime(dt).date();
    }
    public DateTime getDayEnd(Date dt) {
        return new DateTime(dt).getDayEnd();
    }

    /**
     * 获取某天23：59：59
     *
     * @return Date
     */
    public DateTime getDayEnd() {
        Calendar calendar = getCalender(value);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.SECOND, -1);
        return new DateTime(calendar.getTime());
    }


    /**
     * 获取某周第一天0：00：00
     *
     * @return Date
     */
    public DateTime getWeekStart() {
        Calendar cDate = getCalender(value);
        cDate.setFirstDayOfWeek(Calendar.SUNDAY);
        Calendar calendar = getCalender(value);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        if (cDate.get(Calendar.WEEK_OF_YEAR) == Calendar.SUNDAY && cDate.get(Calendar.MONTH) == Calendar.DECEMBER) {
            calendar.set(Calendar.YEAR, cDate.get(Calendar.YEAR) + 1);
        }
        int typeNum = cDate.get(Calendar.WEEK_OF_YEAR);
        calendar.set(Calendar.WEEK_OF_YEAR, typeNum);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return getDayStart(calendar.getTime());
    }

    /**
     * 获取某周最后一天23：59：59
     *
     * @return Date
     */
    public DateTime getWeekEnd() {
        Calendar cDate = getCalender(value);
        cDate.setFirstDayOfWeek(Calendar.SUNDAY);
        Calendar calendar = getCalender(value);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        if (cDate.get(Calendar.WEEK_OF_YEAR) == Calendar.SUNDAY && cDate.get(Calendar.MONTH) == Calendar.DECEMBER) {
            calendar.set(Calendar.YEAR, cDate.get(Calendar.YEAR) + 1);
        }
        int typeNum = cDate.get(Calendar.WEEK_OF_YEAR);//当前周数
        calendar.set(Calendar.WEEK_OF_YEAR, typeNum);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        return getDayEnd(calendar.getTime());
    }

    /**
     * 获取某月第一天0：00：00
     *
     * @return Date
     */
    public DateTime getMonthStart() {
        Calendar calendar = getCalender(value);
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        return getDayStart(calendar.getTime());
    }

    /**
     * 获取某月最后一天23：59：59
     *
     * @return Date
     */
    public DateTime getMonthEnd() {
        Calendar calendar = getCalender(value);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        return getDayEnd(calendar.getTime());
    }

    /**
     * 获取某季度第一天0：00：00
     *
     * @return Date
     */
    public DateTime getSeasonStart() {
        Calendar calendar = getCalender(value);
        int curMonth = calendar.get(Calendar.MONTH);
        if (curMonth >= Calendar.JANUARY && curMonth <= Calendar.MARCH) {
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
        }
        if (curMonth >= Calendar.APRIL && curMonth <= Calendar.JUNE) {
            calendar.set(Calendar.MONTH, Calendar.APRIL);
        }
        if (curMonth >= Calendar.JULY && curMonth <= Calendar.SEPTEMBER) {
            calendar.set(Calendar.MONTH, Calendar.JULY);
        }
        if (curMonth >= Calendar.OCTOBER && curMonth <= Calendar.DECEMBER) {
            calendar.set(Calendar.MONTH, Calendar.OCTOBER);
        }
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        return getDayStart(calendar.getTime());
    }

    /**
     * 获取某季度最后一天23：59：59
     *
     * @return Date
     */
    public DateTime getSeasonEnd() {
        Calendar calendar = getCalender(value);
        int curMonth = calendar.get(Calendar.MONTH);
        if (curMonth >= Calendar.JANUARY && curMonth <= Calendar.MARCH) {
            calendar.set(Calendar.MONTH, Calendar.MARCH);
        }
        if (curMonth >= Calendar.APRIL && curMonth <= Calendar.JUNE) {
            calendar.set(Calendar.MONTH, Calendar.JUNE);
        }
        if (curMonth >= Calendar.JULY && curMonth <= Calendar.SEPTEMBER) {
            calendar.set(Calendar.MONTH, Calendar.SEPTEMBER);
        }
        if (curMonth >= Calendar.OCTOBER && curMonth <= Calendar.DECEMBER) {
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        }
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return getDayEnd(calendar.getTime());
    }

    /**
     * 获取某年第一天0：00：00
     *
     * @return Date
     */
    public DateTime getYearStart() {
        Calendar calendar = getCalender(value);
        int year = calendar.get(Calendar.YEAR);
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        return getDayStart(calendar.getTime());
    }

    /**
     * 获取某年最后一天23：59：59
     *
     * @return Date
     */
    public DateTime getYearEnd() {
        Calendar calendar = getCalender(value);
        int year = calendar.get(Calendar.YEAR);
        calendar.clear();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        return getDayEnd(calendar.getTime());
    }

    public long getZoneTimeStamp(){
        return value.getTime() - new DateTime("1970-01-01").getTimeStamp();
    }

    public long getTimeStamp(){
        return value.getTime();
    }


    public static DateTime now(){
        return new DateTime(new Date());
    }

    public static DateTime minusDate(Date dt1, Date dt2){
        long zStamp = new DateTime("1970-01-01").getTimeStamp();
        long minus = dt1.getTime() - dt2.getTime();
        DateTime dtF = new DateTime(dt1);
        DateTime dtT = new DateTime(dt2);
        long d1Start = dtF.getMonthStart().getTimeStamp();
        long d1End = dtF.getMonthEnd().getTimeStamp();
        long d2Start = dtT.getMonthStart().getTimeStamp();
        long d2End = dtT.getMonthEnd().getTimeStamp();
        double d1=  Double.parseDouble(String.valueOf(dtF.getTimeStamp() -   d1Start))/
                Double.parseDouble(String.valueOf(d1End - d1Start));
        double d2= Double.parseDouble(String.valueOf(dtT.getTimeStamp() -   d2Start))/
                Double.parseDouble(String.valueOf(d2End - d2Start));
        int month = new Double((dtF.getTotalMonths() + d1) - (dtT.getTotalMonths() + d2)).intValue();
        return new DateTime(minus + zStamp, month);
    }

    public Date toDate(){
        return value;
    }

    public int getTotalYears(){
        return getTotalMonths() / 12;
    }

    public int getTotalMonths(){
        if(!isCompare){
            return Integer.parseInt(toString("yyyy")) * 12 + Integer.parseInt(toString("M"));
        }
        return totalMonth;
    }

    public int getTotalDays(){
        return (int)(getZoneTimeStamp() / (3600 * 1000 * 24));
    }

    public int getTotalHours(){
        return (int)(getZoneTimeStamp() / (3600 * 1000));
    }

    public int getTotalMinutes(){
        return (int)(getZoneTimeStamp() / (60 * 1000));
    }

    public int getTotalSeconds(){
        return (int)(getZoneTimeStamp() / 1000);
    }

    private static Map<String, String> getDateFormatReplaceChar(){
        Map<String, String> ret = new HashMap<>();
        ret.put("/" , "-");
        return  ret;
    }

    private static String getDateFormat(String time){
        String[] timeArr = time.split(" ");
        List<String> retFormat= new LinkedList<>();
        if(timeArr[0].length() < 10){  //标准日期格式
            retFormat.add("yyyy-M-d");
        }
        else {
            retFormat.add("yyyy-MM-dd");
        }
        for(int idx = 1; idx < timeArr.length; idx ++){
            String nextStr = timeArr[idx];
            if(Character.isDigit(nextStr.charAt(0))){ //说明是时间格式
                List<String> tArr = new LinkedList<>();
                String[] timeSplit = nextStr.split(":");
                if(timeSplit[0].length() == 2){
                    tArr.add("HH");
                }
                else {
                    tArr.add("H");
                }
                if(timeSplit.length > 1){
                    if(timeSplit[1].length() == 2){
                        tArr.add("mm");
                    }
                    else {
                        tArr.add("mm");
                    }
                    if(timeSplit.length > 2){
                        String[] seconds = timeSplit[2].split("\\.");
                        String secondStr = seconds[0].replaceAll("\\d", "s");
                        if(seconds.length > 1){
                            secondStr += "." + seconds[1].replaceAll("\\d", "S");
                        }
                        secondStr = secondStr.replace("+", "@").replace("-", "@");
                        if(secondStr.contains("@")){
                            secondStr = secondStr.substring(0, secondStr.indexOf("@")) + "Z";
                        }
                        tArr.add(secondStr);
                    }
                }
                retFormat.add(String.join(":", tArr));
            }
            else {
                retFormat.add("Z");
            }
        }
        return String.join(" ", retFormat);
    }

    private static String getNormalDateString(String time){
        Map<String, String> replaceChars = getDateFormatReplaceChar();
        for(String key : replaceChars.keySet()){
            time = time.replace(key, replaceChars.get(key));
        }
        Pattern pattern = Pattern.compile("[0-9]T[0-9]");
        Matcher matcher = pattern.matcher(time);
        if(matcher.find()){
            String regValue = matcher.group();
            time = time.replaceFirst(regValue, regValue.replace("T", " "));
        }
        if(time.endsWith("Z")){
            time = time.substring(0, time.length()- 1) + "+0000";
        }
        pattern = Pattern.compile("[a-zA-Z]");
        matcher = pattern.matcher(time);
        if (matcher.find()) {
            int idx = time.indexOf(matcher.group());

            time = time.substring(0, idx) + " " + time.substring(idx);
            time = time.replace("  ", " ");
        }
        return time;
    }

    public static Date toDate(Object object){
        if(object == null){
            return null;
        }
        try {
            if(object instanceof Date){
                return (Date) object;
            }
            if(object.getClass() == Long.class){
                return convertByMilliseconds((Long)object).toDate();
            }

            String dateString = object.toString();
            dateString = getNormalDateString(dateString);
            String formatStr = getDateFormat(dateString);
            SimpleDateFormat df = new SimpleDateFormat(formatStr);
            return df.parse(dateString);
        }
        catch (Exception ex){
            return  null;
        }
    }

    public static DateTime convertByMilliseconds(long milli){
        return new DateTime(new Date(milli));
    }

    public String toString(String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(value);
    }
}
