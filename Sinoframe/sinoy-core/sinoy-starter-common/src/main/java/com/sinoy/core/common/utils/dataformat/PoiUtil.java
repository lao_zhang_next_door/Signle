package com.sinoy.core.common.utils.dataformat;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;

/**
 * poi处理类
 */
public class PoiUtil {

    /**
     * excel读取
     * @param inputStream 文件输入留
     * @param fileName 文件名
     * @param beanPropertys 字段名
     * @param startRow 开始读取行数
     * @return
     * @throws Exception
     */
    public static JSONObject readExcel(InputStream inputStream, String fileName, String[] beanPropertys, int startRow) throws Exception {
        String message = "Import success";
        boolean isE2007 = false;
        //判断是否是excel2007格式
        if (fileName.endsWith("xlsx")) {
            isE2007 = true;
        }
        int rowIndex = 0;
        JSONArray array = new JSONArray();
        JSONObject res = new JSONObject();
        try {
            InputStream input = inputStream;  //建立输入流
            Workbook wb;
            //根据文件格式(2003或者2007)来初始化
            if (isE2007) {
                wb = new XSSFWorkbook(input);
            } else {
                wb = new HSSFWorkbook(input);
            }
            Sheet sheet = wb.getSheetAt(0);    //获得第一个表单
            int rowCount = sheet.getLastRowNum() + 1;

            for (int i = startRow; i < rowCount; i++) {
                rowIndex = i;
                Row row;
                JSONObject obj = new JSONObject();
                int a = 0;
                for (int j = a; j < beanPropertys.length; j++) {
//                    if (isMergedRegion(sheet, i, j)) {
//                        if (getMergedRegionValue(sheet, i, j) == null) {
//                            obj.put(beanPropertys[j], "");
//                        } else {
//                            obj.put(beanPropertys[j], getMergedRegionValue(sheet, i, j));
//                        }
//                    } else {
//
//                    }
                    row = sheet.getRow(i);
                    if (getCellValueByCell(row.getCell(j)) == null) {
                        obj.put(beanPropertys[j], "");
                    } else {
                        obj.put(beanPropertys[j], getCellValueByCell(row.getCell(j)));
                    }
                }
                array.add(obj);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            message = "Import failed, please check the data in " + rowIndex + " rows ";
            res.put("code", 500);
            res.put("msg", message);
            return res;
        }

        res.put("code", 0);
        res.put("data", array);
        return res;
    }

    /**
     * 通用的读取excel单元格的处理方法
     *
     * @param cell
     * @return
     */
    private static Object getCellValueByCell(Cell cell) {
        Object result = null;
        if (cell != null) {
            switch (cell.getCellType()) {
                case STRING:
                    result = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    //对日期进行判断和解析
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        double cellValue = cell.getNumericCellValue();
                        result = HSSFDateUtil.getJavaDate(cellValue);
                    } else {
                        cell.setCellType(CellType.STRING);
                        result = cell.getStringCellValue();
                    }
                    break;
                case BOOLEAN:
                    result = cell.getBooleanCellValue();
                    break;
                case FORMULA:
                    result = cell.getCellFormula();
                    break;
                case ERROR:
                    result = cell.getErrorCellValue();
                    break;
                case BLANK:
                    break;
                default:
                    break;
            }
        } else {
            result = "";
        }
        return result;
    }

}
