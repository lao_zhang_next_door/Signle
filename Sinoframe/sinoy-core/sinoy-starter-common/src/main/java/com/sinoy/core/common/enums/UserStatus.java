package com.sinoy.core.common.enums;

/**
 * 用户状态枚举
 * @author hero
 *
 */
public enum UserStatus {

	NORMAL('1',"正常"),
	DISABLE('0',"禁用");
	
	private final Character code;
    private final String value;
    private UserStatus(Character code, String value) {
        this.code = code;
        this.value = value;
    }
	public Character getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
