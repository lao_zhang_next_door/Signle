package com.sinoy.core.common.utils.request;

import com.sinoy.core.common.utils.exception.BaseException;

import java.lang.reflect.Field;

/**
 * 请求工具类
 */
public class RequestUtil {

    /**
     * 判断实体中字段某字段是否存在 (不包括父类继承下来的字段)
     * @param obj
     * @param fieldNames
     * @return
     */
    public static boolean isFieldExist(Object obj,String... fieldNames)  {
        if(obj == null || fieldNames == null || fieldNames.length == 0){
            throw new BaseException("参数错误");
        }
        Class<?> aClass = obj.getClass();

        for (int i = 0; i < fieldNames.length; i++) {
            try {
                aClass.getDeclaredField(fieldNames[i]);
            } catch (NoSuchFieldException e) {
                //为空
                return true;
            }
        }
        return false;
    }

    /**
     * 判断当前实体中 字段是否为空 不支持父类继承字段
     * @param obj
     * @param fieldNames
     * @return
     */
    public static boolean isFieldBlank(Object obj, String... fieldNames) {

        if(obj == null || fieldNames == null || fieldNames.length == 0){
            throw new BaseException("参数错误");
        }
        Class<?> aClass = obj.getClass();

        for (int i = 0; i < fieldNames.length; i++) {
            try {
                Field f = aClass.getDeclaredField(fieldNames[i]);
                f.setAccessible(true);
                if (f.get(obj) == null || f.get(obj).equals("")) {
                    // 判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
                    return true;
                }
            } catch (Exception e) {
                //为空
                return true;
            }
        }
        return false;
    }

}
