package com.sinoy.core.common.utils.dataformat;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 数字计算工具类
 */
public class CalculatorUtils {

    private static String isExpr = "[+\\-*/]";
    private static String isMDExpr = "[*/]";


    //没有括号的
    //计算string  1+2*3+4  计算后得到结果
    public static BigDecimal CalculatorString(String str) {
        str = str.replace(" ", "");
        Stack<BigDecimal> number = new Stack<>();
        Stack<String> expr = new Stack<>();
        String reg = "-?\\d+(\\.\\d+)?|[+\\-*/]";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(str);
        int index = 0;
        while (m.find()) {
            String temp = m.group();
            if (temp.matches(isExpr)) {// 遇到运算符，将最后两个数字取出，进行该运算，将结果再放入容器
                if (expr.size() > 0 && expr.get(expr.size() - 1).matches(isMDExpr)) {
                    BigDecimal a1 = number.pop();
                    BigDecimal a2 = number.pop();
                    BigDecimal res = DecCalculator(a2, a1, expr.pop());
                    number.push(res);
                }
                expr.push(temp.trim());
            } else {// 遇到数字直接放入容器
                if (index % 2 == 1) {
                    expr.add("+");
                    index++;
                }
                number.push(new BigDecimal(temp));
            }
            index++;
        }
        if (expr.size() > 0) {
            if (expr.get(expr.size() - 1).matches(isMDExpr)) {
                BigDecimal a1 = number.pop();
                BigDecimal a2 = number.pop();
                BigDecimal res = DecCalculator(a2, a1, expr.pop());
                number.push(res);
            }
        }
        if (number.size() > 0) {
            for (int i = 0; i < expr.size(); i++) {
                if (expr.get(i).equals("-")) {
                    number.set(i + 1, BigDecimal.valueOf(-1).multiply(number.get(i + 1)));
                }
            }

            return add(number.toArray(new BigDecimal[0]));
        }
        return BigDecimal.ZERO;
    }

    public static BigDecimal CalculatorExpress(String express) {
        while (express.contains("(")) {
            String calExpress = express.substring(0, express.indexOf(")"));
            calExpress = calExpress.substring(calExpress.lastIndexOf("(") + 1);
            BigDecimal cal = CalculatorString(calExpress);
            express = express.replace("(" + calExpress + ")", cal.toPlainString());
        }
        return CalculatorString(express);
    }

    public static BigDecimal DecCalculator(BigDecimal a, BigDecimal b, String expr) {
        if (expr.equals("+")) {
            return a.add(b);
        } else if (expr.equals("-")) {
            return a.subtract(b);
        } else if (expr.equals("*")) {
            return a.multiply(b);
        } else if (expr.equals("/")) {
            return endZero(a.divide(b, 10, RoundingMode.DOWN));
        }
        return BigDecimal.ZERO;
    }

    private static String endZeroReg = "0+?$";

    public static BigDecimal multiply(BigDecimal... decimals){
        BigDecimal ret = BigDecimal.ONE;
        for(BigDecimal decimal : decimals){
            ret = ret.multiply(decimal);
        }
        return ret;
    }

    public static BigDecimal add(BigDecimal... decimals){
        BigDecimal ret = BigDecimal.ZERO;
        for(BigDecimal decimal : decimals){
            ret = ret.add(decimal);
        }
        return ret;
    }

    public static BigDecimal strains(BigDecimal decimal){
        return BigDecimal.valueOf(-1).multiply(decimal);
    }

    public static BigDecimal endZero(BigDecimal decimal){
        String data = decimal.toPlainString();
        if(data.contains(".")){
            data = data.replaceAll(endZeroReg, "");
            return new BigDecimal(data);
        }
        return decimal;
    }
}
