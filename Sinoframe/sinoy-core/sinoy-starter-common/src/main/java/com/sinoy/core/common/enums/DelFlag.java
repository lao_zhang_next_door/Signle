package com.sinoy.core.common.enums;

public enum DelFlag {

	Normal(0,"正常"),
	Del(1,"已删除");
	
	private final int code;
    private final String value;
    private DelFlag(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
