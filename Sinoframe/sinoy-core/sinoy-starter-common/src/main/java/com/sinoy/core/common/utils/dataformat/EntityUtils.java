package com.sinoy.core.common.utils.dataformat;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.common.utils.request.PropertyResult;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class EntityUtils {
    public static Object getPropertyValue(Object entity, String propertyName){
        if(entity instanceof Map){
            return ((Map<String, Object>)entity).get(propertyName);
        }
        PropertyResult propertyResult = new PropertyResult();
        return getPropertyValue(entity, propertyName, propertyResult);
    }
    public static Object getPropertyValue(Object entity, String propertyName, PropertyResult propertyResult){
        try {
            PropertyDescriptor pd = new PropertyDescriptor(propertyName, entity.getClass());
            Method getMethod = pd.getReadMethod();// 获得get方法
            Object result = getMethod.invoke(entity);
            propertyResult.setResultData(result);
            return result;// 执行get方法返回值
        } catch (Exception e) {
            propertyResult.setSuccess(false);
            return null;
        }
    }

    public static void setPropertyValue(Object entity, String propertyName, Object value){
        try {
            PropertyDescriptor pd = new PropertyDescriptor(propertyName, entity.getClass());
            Method setMethod = pd.getWriteMethod();// 获得get方法
            setMethod.invoke(entity, value);// 执行get方法返回值
        } catch (Exception e) {
        }
    }

    public static void concatEntites(Object before, Object end){
        concatEntites(before, end, false);
    }

    public static void concatEntites(Object before, Object end, boolean isIgnoreNull){
        Class<?> clazz = before.getClass();
        List<Field> fieldList = new ArrayList<>();
        while (clazz != null){
            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        for (Field field: fieldList){

            PropertyResult propertyResult = new PropertyResult();
            Object propertyValue = getPropertyValue(end, field.getName(), propertyResult);
            if(propertyResult.getSuccess()){
                if(!isIgnoreNull){
                    setPropertyValue(before, field.getName(), propertyValue);
                }
                else if(propertyValue != null){
                    setPropertyValue(before, field.getName(), propertyValue);
                }
            }
        }
    }

    public static boolean isExistField(Object obj, String field) {
        if (obj == null || StringUtil.isEmpty(field)) {
            return false;
        }
        Object o = JSON.toJSON(obj);
        JSONObject jsonObj = new JSONObject();
        if (o instanceof JSONObject) {
            jsonObj = (JSONObject) o;
        }
        return jsonObj.containsKey(field);
    }
}
