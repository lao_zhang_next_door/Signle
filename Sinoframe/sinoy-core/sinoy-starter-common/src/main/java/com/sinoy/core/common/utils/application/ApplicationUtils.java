package com.sinoy.core.common.utils.application;


import com.sinoy.core.common.utils.dataformat.StringUtil;
import org.springframework.util.ClassUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

/**
 * 读取静态资源工具类
 */
public class ApplicationUtils {
    public final static Properties systemProperties = loadAllApplicationProperties();


    public static Properties loadConfProperties(String name) {

        Properties properties = new Properties();

        InputStream in;
        BufferedReader bf;
        String path = name + ".yml";
        ClassLoader cl = ClassUtils.getDefaultClassLoader();
        URL url = cl != null ? cl.getResource(path) : ClassLoader.getSystemResource(path);

        try {
            if (url != null) {
                in = url.openStream();
                bf = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                Yaml props = new Yaml();
                Object obj = props.loadAs(bf, Map.class);
                Map<String, Object> param = (Map<String, Object>) obj;
                forEachYaml(properties, param, "");
            }
            path = name + ".properties";
            url = cl != null ? cl.getResource(path) : ClassLoader.getSystemResource(path);
            if (url != null) {
                in = url.openStream();
                bf = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                properties.load(bf);
            }
            else {
                return properties;
            }
        } catch (Exception ignored) {
        }
        return properties;
    }

    public static String loadApplicationResource(String path) {
        return loadResource(ApplicationUtils.class, path);
    }

    public static String loadResource(Class<?> cls, String path) {
        StringBuilder sb = new StringBuilder();
        InputStream in = loadResourceInputStream(cls, path);
        if (in != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                String contentLine = br.readLine();
                while (contentLine != null) {
                    sb.append(contentLine);
                    contentLine = br.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static InputStream loadResourceInputStream(Class<?> cls, String path) {
        return cls.getResourceAsStream(path);
    }

    public static Properties loadAllApplicationProperties() {
        Properties properties = loadConfProperties("application");
        String active = properties.getProperty("spring.profiles.active");

        // 读取配置文件获取定制数据源，也可以通过数据库获取数据源
        if (!StringUtil.isEmpty(active)) {
            String[] actives = active.split(",");
            for (String ac : actives) {
                if (!StringUtil.isEmpty(ac.trim())) {
                    Properties propertiesNew = loadConfProperties("application-" + ac.trim());
                    properties.putAll(propertiesNew);
                }
            }
        }
        return properties;
    }

    public static Properties loadApplicationProperties() {
        return systemProperties;
    }

    public static String getApplicationProperties(String key) {
        Properties properties = loadApplicationProperties();
        return properties.containsKey(key) ? properties.getProperty(key) : "";
    }


    /**
     * 遍历yml文件，获取map集合
     *
     * @param key_str
     * @param obj
     * @return
     */
    private static void forEachYaml(Properties result, Map<String, Object> obj, String key_str) {
        for (Map.Entry<String, Object> entry : obj.entrySet()) {
            String key = entry.getKey();
            Object val = entry.getValue();
            String str_new = "";
            if (!StringUtil.isEmpty(key_str)) {
                str_new = key_str + "." + key;
            } else {
                str_new = key;
            }
            if (val instanceof Map) {
                forEachYaml(result, (Map<String, Object>) val, str_new);
            } else {
                result.put(str_new, val.toString());
            }
        }
    }

}
