package com.sinoy.core.common.utils.request;

import com.alibaba.fastjson2.JSONObject;
import com.sinoy.core.common.base.Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * http请求操作类
 *
 * @author hero
 */
public class HttpUtil extends Base {

	protected static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	/**
	 * 发送GET请求
	 *
	 * @param requestUrl
	 * @return
	 */
	public static Object getRequest(String requestUrl, String charSetName) {
		String res = "";
		StringBuffer buffer = new StringBuffer();
		try {
			URL url = new URL(requestUrl);
			HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
			if (200 == urlCon.getResponseCode()) {
				InputStream is = urlCon.getInputStream();
				InputStreamReader isr = new InputStreamReader(is, charSetName);
				BufferedReader br = new BufferedReader(isr);
				String str = null;
				while ((str = br.readLine()) != null) {
					buffer.append(str);
				}
				br.close();
				isr.close();
				is.close();
				res = buffer.toString();
				return res;
			} else {
				throw new Exception("连接失败");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * 发送POST请求
	 *
	 * @param path
	 * @param post
	 * @return
	 */
	public static Object postRequest(String path, String post) {
		URL url = null;
		try {
			url = new URL(path);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			// 提交模式
			httpURLConnection.setRequestMethod("POST");
			//连接超时 单位毫秒
			httpURLConnection.setConnectTimeout(10000);
			//读取超时 单位毫秒
			httpURLConnection.setReadTimeout(2000);
			// 发送POST请求必须设置如下两行
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
			// 发送请求参数
			//post的参数 xx=xx&yy=yy
			printWriter.write(post);
			// flush输出流的缓冲
			printWriter.flush();
			//开始获取数据
			BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int len;
			byte[] arr = new byte[1024];
			while ((len = bis.read(arr)) != -1) {
				bos.write(arr, 0, len);
				bos.flush();
			}
			bos.close();

			return bos.toString("utf-8");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * 发送POST请求
	 *
	 * @param url
	 *            目的地址
	 * @param parameters
	 *            请求参数，Map类型。
	 * @return 远程响应结果
	 */
	public static String sendPost(String url, Map<String, String> parameters) {
		System.out.println(JSONObject.toJSONString(parameters));
		String result = "";// 返回的结果
		BufferedReader in = null;// 读取响应输入流
		PrintWriter out = null;
		StringBuffer sb = new StringBuffer();// 处理请求参数
		String params = "";// 编码之后的参数
		try {
			// 编码请求参数
			if (parameters.size() == 1) {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(parameters.get(name),
									"UTF-8"));
				}
				params = sb.toString();
			} else {
				for (String name : parameters.keySet()) {
					System.out.println(name);
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(parameters.get(name),
									"UTF-8")).append("&");
				}
				String temp_params = sb.toString();
				if (temp_params.length() != 0) {
					params = temp_params.substring(0, temp_params.length() - 1);
				}
			}
			// 创建URL对象
			URL connURL = new URL(url);
			// 打开URL连接
			HttpURLConnection httpConn = (HttpURLConnection) connURL
					.openConnection();
			// 设置通用属性
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			//httpConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			httpConn.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			// 设置POST方式
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);
			// 获取HttpURLConnection对象对应的输出流
			out = new PrintWriter(httpConn.getOutputStream());
			// 发送请求参数
			out.write(params);
			// flush输出流的缓冲
			out.flush();
			if(httpConn.getResponseCode() == 200) {
				// 定义BufferedReader输入流来读取URL的响应，设置编码方式
				in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));

			}else {
				in = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(), "UTF-8"));
			}

			String line;
			// 读取返回的内容
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static String sendGet(String url, Map<String, String> parameters) {
		String result = "";
		BufferedReader in = null;
		StringBuffer sb = new StringBuffer();
		String params = "";

		try {
			Iterator var6;
			String name;
			String full_url;
			if (parameters.size() == 1) {
				var6 = parameters.keySet().iterator();

				while(var6.hasNext()) {
					name = (String)var6.next();
					sb.append(name).append("=").append(URLEncoder.encode((String)parameters.get(name), "UTF-8"));
				}

				params = sb.toString();
			} else {
				var6 = parameters.keySet().iterator();

				while(var6.hasNext()) {
					name = (String)var6.next();
					sb.append(name).append("=").append(URLEncoder.encode((String)parameters.get(name), "UTF-8")).append("&");
				}

				full_url = sb.toString();
				params = full_url.substring(0, full_url.length() - 1);
			}

			full_url = url + "?" + params;
			System.out.println(full_url);
			URL connURL = new URL(full_url);
			HttpURLConnection httpConn = (HttpURLConnection)connURL.openConnection();
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			httpConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			httpConn.connect();
			Map<String, List<String>> headers = httpConn.getHeaderFields();
			Iterator var10 = headers.keySet().iterator();

			while(var10.hasNext()) {
				String key = (String)var10.next();
				System.out.println(key + "\t：\t" + headers.get(key));
			}

			String line;
			for(in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8")); (line = in.readLine()) != null; result = result + line) {
			}
		} catch (Exception var20) {
			var20.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException var19) {
				var19.printStackTrace();
			}

		}

		return result;
	}

	/**
	 * 发送postJSON
	 * @param param
	 * @param urls
	 * @return
	 */
	public static String sendPostJSON(String urls,String param){
		StringBuffer sb=new StringBuffer();
		try{
			//创建url
			// 创建url资源
			URL url = new URL(urls);
			// 建立http连接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// 设置允许输出
			conn.setDoOutput(true);
			// 设置允许输入
			conn.setDoInput(true);
			// 设置不用缓存
			conn.setUseCaches(false);
			// 设置传递方式
			conn.setRequestMethod("POST");
			// 设置维持长连接
			conn.setRequestProperty("Connection", "Keep-Alive");
			// 设置文件字符集:
			conn.setRequestProperty("Charset", "UTF-8");
			// 转换为字节数组
			byte[] data = param.getBytes("UTF-8");
			// 设置文件长度
			conn.setRequestProperty("Content-Length", String.valueOf(data.length));
			// 设置文件类型:
			conn.setRequestProperty("Content-Type", "application/json");
			// 开始连接请求
			conn.connect();
			OutputStream out = new DataOutputStream(conn.getOutputStream()) ;
			// 写入请求的字符串
			out.write(data);
			out.flush();
			out.close();
			// 请求返回的状态
			System.out.println(String.valueOf(conn.getResponseCode()));
			InputStream in;
			if(HttpURLConnection.HTTP_OK == conn.getResponseCode()){
				// 请求返回的数据
				in = conn.getInputStream();
			}else{
				// 请求返回的错误信息
				in = conn.getErrorStream();
			}
			try{
				String readLine = new String();
				BufferedReader responseReader=new BufferedReader(new InputStreamReader(in,"UTF-8"));
				while((readLine=responseReader.readLine())!=null){
					sb.append(readLine).append("\n");
				}
				responseReader.close();
			}catch (Exception e1) {
				throw new RuntimeException(e1.getMessage());
			}
			return sb.toString();
		}catch (Exception e){
			throw new RuntimeException(e.getMessage());
		}
	}

	public static String sendPostJSON(String urls,String param,String charset){
		StringBuffer sb=new StringBuffer();
		try{
			//创建url
			// 创建url资源
			URL url = new URL(urls);
			// 建立http连接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// 设置允许输出
			conn.setDoOutput(true);
			// 设置允许输入
			conn.setDoInput(true);
			// 设置不用缓存
			conn.setUseCaches(false);
			// 设置传递方式
			conn.setRequestMethod("POST");
			// 设置维持长连接
			conn.setRequestProperty("Connection", "Keep-Alive");
			// 设置文件字符集:
			conn.setRequestProperty("Charset", charset);
			// 转换为字节数组
			byte[] data = param.getBytes(charset);
			// 设置文件长度
			conn.setRequestProperty("Content-Length", String.valueOf(data.length));
			// 设置文件类型:
			conn.setRequestProperty("Content-Type", "application/json");
			// 开始连接请求
			conn.connect();
			OutputStream out = new DataOutputStream(conn.getOutputStream()) ;
			// 写入请求的字符串
			out.write(data);
			out.flush();
			out.close();
			// 请求返回的状态
			System.out.println(String.valueOf(conn.getResponseCode()));
			InputStream in;
			if(HttpURLConnection.HTTP_OK == conn.getResponseCode()){
				// 请求返回的数据
				in = conn.getInputStream();
			}else{
				// 请求返回的错误信息
				in = conn.getErrorStream();
			}
			try{
				String readLine = new String();
				BufferedReader responseReader=new BufferedReader(new InputStreamReader(in,"UTF-8"));
				while((readLine=responseReader.readLine())!=null){
					sb.append(readLine).append("\n");
				}
				responseReader.close();
			}catch (Exception e1) {
				throw new RuntimeException(e1.getMessage());
			}
			return sb.toString();
		}catch (Exception e){
			throw new RuntimeException(e.getMessage());
		}
	}

//	/**
//	 * 发送POST请求 (对象入参形式)
//	 *
//	 * @param url
//	 *            目的地址
//	 * @param parameters
//	 *            请求参数，Map类型。
//	 * @return 远程响应结果
//	 */
//	public static String sendPostwithObj(String url, Map<String, Object> parameters) {
//		String result = "";// 返回的结果
//		BufferedReader in = null;// 读取响应输入流
//		PrintWriter out = null;
//		StringBuffer sb = new StringBuffer();// 处理请求参数
//		String params = "";// 编码之后的参数
//		try {
//			// 编码请求参数
//			if (parameters.size() == 1) {
//				for (String name : parameters.keySet()) {
//					sb.append(name).append("=").append(
//							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),
//									"UTF-8"));
//				}
//				params = sb.toString();
//			} else {
//				for (String name : parameters.keySet()) {
//					sb.append(name).append("=").append(
//							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),
//									"UTF-8")).append("&");
//				}
//				String temp_params = sb.toString();
//				params = temp_params.substring(0, temp_params.length() - 1);
//			}
//			// 创建URL对象
//			java.net.URL connURL = new java.net.URL(url);
//			// 打开URL连接
//			java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) connURL
//					.openConnection();
//			// 设置通用属性
//			httpConn.setRequestProperty("Accept", "*/*");
//			httpConn.setRequestProperty("Connection", "Keep-Alive");
//			httpConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
//			httpConn.setRequestProperty("User-Agent",
//					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
//			// 设置POST方式
//			httpConn.setDoInput(true);
//			httpConn.setDoOutput(true);
//			// 获取HttpURLConnection对象对应的输出流
//			out = new PrintWriter(httpConn.getOutputStream());
//			// 发送请求参数
//			System.out.println(params);
//			out.write(params);
//			// flush输出流的缓冲
//			out.flush();
//			if(httpConn.getResponseCode() == 200) {
//				// 定义BufferedReader输入流来读取URL的响应，设置编码方式
//				in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
//
//			}else {
//				in = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(), "UTF-8"));
//			}
//
//			String line;
//			// 读取返回的内容
//			while ((line = in.readLine()) != null) {
//				result += line;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (out != null) {
//					out.close();
//				}
//				if (in != null) {
//					in.close();
//				}
//			} catch (IOException ex) {
//				ex.printStackTrace();
//			}
//		}
//		return result;
//	}


	/**
	 * 发送POST请求
	 *
	 * @param url
	 *            目的地址
	 * @param parameters
	 *            请求参数，Map类型。
	 * @return 远程响应结果
	 */
	public static String sendPostWithXWwwForm(String url, Map<String, String> parameters) {
		String result = "";// 返回的结果
		BufferedReader in = null;// 读取响应输入流
		PrintWriter out = null;
		StringBuffer sb = new StringBuffer();// 处理请求参数
		String params = "";// 编码之后的参数
		try {
			// 编码请求参数
			if (parameters.size() == 1) {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),
									"UTF-8"));
				}
				params = sb.toString();
			} else {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),"UTF-8")).append("&");
				}
				String temp_params = sb.toString();
				params = temp_params.substring(0, temp_params.length() - 1);
			}
			// 创建URL对象
			URL connURL = new URL(url);
			// 打开URL连接
			HttpURLConnection httpConn = (HttpURLConnection) connURL
					.openConnection();
			// 设置通用属性
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			httpConn.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			// 设置POST方式
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);
			// 获取HttpURLConnection对象对应的输出流
			out = new PrintWriter(httpConn.getOutputStream());
			// 发送请求参数
			out.write(params);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应，设置编码方式
			in = new BufferedReader(new InputStreamReader(httpConn
					.getInputStream(), "UTF-8"));
			String line;
			// 读取返回的内容
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 发送POST请求
	 *
	 * @param url
	 *            目的地址
	 * @param parameters
	 *            请求参数，Map类型。
	 * @return 远程响应结果
	 */
	public static String sendPostWithXWwwFormContain(String url, Map<String, String> parameters, String token) {
		String result = "";// 返回的结果
		BufferedReader in = null;// 读取响应输入流
		PrintWriter out = null;
		StringBuffer sb = new StringBuffer();// 处理请求参数
		String params = "";// 编码之后的参数
		try {
			// 编码请求参数
			if (parameters.size() == 1) {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),
									"UTF-8"));
				}
				params = sb.toString();
			} else {
				for (String name : parameters.keySet()) {
					sb.append(name).append("=").append(
							java.net.URLEncoder.encode(String.valueOf(parameters.get(name)),"UTF-8")).append("&");
				}
				String temp_params = sb.toString();
				params = temp_params.substring(0, temp_params.length() - 1);
			}
			// 创建URL对象
			URL connURL = new URL(url);
			// 打开URL连接
			HttpURLConnection httpConn = (HttpURLConnection) connURL
					.openConnection();
			// 设置通用属性
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Connection", "Keep-Alive");
			httpConn.setRequestProperty("Authorization", "Bearer "+token);
			httpConn.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			// 设置POST方式
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);
			// 获取HttpURLConnection对象对应的输出流
			out = new PrintWriter(httpConn.getOutputStream());
			// 发送请求参数
			out.write(params);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应，设置编码方式
			in = new BufferedReader(new InputStreamReader(httpConn
					.getInputStream(), "UTF-8"));
			String line;
			// 读取返回的内容
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 发送POST请求
	 *
	 * @param url
	 *            目的地址
	 * @param parameters
	 *            请求参数，Map类型。
	 * @return 远程响应结果
	 * @throws Exception
	 */
	public static String sendPostWithFile(String url, Map<String, String> parameters, MultipartFile file) throws Exception {
		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = "UTF-8";

		URL uri = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setReadTimeout(30 * 1000); // 缓存的最长时间
		conn.setDoInput(true);// 允许输入
		conn.setDoOutput(true);// 允许输出
		conn.setUseCaches(false); // 不允许使用缓存
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
				+ ";boundary=" + BOUNDARY);

		StringBuilder sb = new StringBuilder();

		if (parameters!=null) {
			// 首先组拼文本类型的参数
			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				sb.append(PREFIX);
				sb.append(BOUNDARY);
				sb.append(LINEND);
				sb.append("Content-Disposition: form-data; name=\""
						+ entry.getKey() + "\"" + LINEND);
				sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
				sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
				sb.append(LINEND);
				sb.append(entry.getValue());
				sb.append(LINEND);
			}

		}

		DataOutputStream outStream = new DataOutputStream(
				conn.getOutputStream());
		if (!StringUtils.isEmpty(sb.toString())) {
			outStream.write(sb.toString().getBytes());
		}


		// 发送文件数据
		if (file != null) {
			StringBuilder sb1 = new StringBuilder();
			sb1.append(PREFIX);
			sb1.append(BOUNDARY);
			sb1.append(LINEND);
			sb1.append("Content-Disposition: form-data; name=\"file\"; filename=\""
					+  file.getOriginalFilename() + "\"" + LINEND);
			sb1.append("Content-Type: application/octet-stream; charset="+ CHARSET + LINEND);
			sb1.append(LINEND);
			outStream.write(sb1.toString().getBytes());

			InputStream is = file.getInputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}

			is.close();
			outStream.write(LINEND.getBytes());
		}

		// 请求结束标志
		byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
		outStream.write(end_data);
		outStream.flush();

		// 得到响应码
		int res = conn.getResponseCode();
		InputStream in = conn.getInputStream();
		if (res == 200) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = bufferedReader.readLine()) != null){
				buffer.append(line);
			}

//          int ch;
//          StringBuilder sb2 = new StringBuilder();
//          while ((ch = in.read()) != -1) {
//              sb2.append((char) ch);
//          }
			return buffer.toString();
		}
		outStream.close();
		conn.disconnect();
		return in.toString();

	}


	/**
	 * 发送POST请求
	 *
	 * @param url
	 *            目的地址
	 * @param parameters
	 *            请求参数，Map类型。
	 * @return 远程响应结果
	 * @throws Exception
	 */
	public static String sendPostWithFiles(String url, Map<String, String> parameters,MultipartFile file) throws Exception {
		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = "UTF-8";

		URL uri = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setReadTimeout(30 * 1000); // 缓存的最长时间
		conn.setDoInput(true);// 允许输入
		conn.setDoOutput(true);// 允许输出
		conn.setUseCaches(false); // 不允许使用缓存
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
				+ ";boundary=" + BOUNDARY);

		StringBuilder sb = new StringBuilder();

		if (parameters!=null) {
			// 首先组拼文本类型的参数
			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				sb.append(PREFIX);
				sb.append(BOUNDARY);
				sb.append(LINEND);
				sb.append("Content-Disposition: form-data; name=\""
						+ entry.getKey() + "\"" + LINEND);
				sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
				sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
				sb.append(LINEND);
				sb.append(entry.getValue());
				sb.append(LINEND);
			}

		}

		DataOutputStream outStream = new DataOutputStream(
				conn.getOutputStream());
		if (!StringUtils.isEmpty(sb.toString())) {
			outStream.write(sb.toString().getBytes());
		}


		// 发送文件数据
		if (file != null) {
			StringBuilder sb1 = new StringBuilder();
			sb1.append(PREFIX);
			sb1.append(BOUNDARY);
			sb1.append(LINEND);
			sb1.append("Content-Disposition: form-data; name=\"files\"; filename=\""
					+  file.getOriginalFilename() + "\"" + LINEND);
			sb1.append("Content-Type: application/octet-stream; charset="+ CHARSET + LINEND);
			sb1.append(LINEND);
			outStream.write(sb1.toString().getBytes());

			InputStream is = file.getInputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}

			is.close();
			outStream.write(LINEND.getBytes());
		}

		// 请求结束标志
		byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
		outStream.write(end_data);
		outStream.flush();

		// 得到响应码
		int res = conn.getResponseCode();
		InputStream in = conn.getInputStream();
		if (res == 200) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = bufferedReader.readLine()) != null){
				buffer.append(line);
			}

//          int ch;
//          StringBuilder sb2 = new StringBuilder();
//          while ((ch = in.read()) != -1) {
//              sb2.append((char) ch);
//          }
			return buffer.toString();
		}
		outStream.close();
		conn.disconnect();
		return in.toString();

	}
}
