package com.sinoy.core.common.enums;

import com.sinoy.core.common.utils.dataformat.StringUtil;

/**
 * 数据权限类型
 * <p>
 * 语法支持 spel 模板表达式
 * <p>
 * 内置数据 user 当前用户 内容参考 XnwUser
 * 内置服务 sdss 系统数据权限服务 内容参考 DataScopeService
 * 如需扩展更多自定义服务 可以参考 dss 自行编写
 * 注意 扩展字段请尽量使用dept_id 作为 关联键
 */
public enum DataScopeType {

    /**
     * 全部
     */
    ALL("1", "全部数据权限","","String"),
    /**
     * 仅本人数据
     */
    SELF("2","仅本人数据权限"," #{#userColumn} = '#{#user.rowGuid}' ","String"),
    /**
     * 本部门数据权限
     */
    THIS_DEPT_LEVEL("3", "本部门数据权限"," #{#column} = '#{#user.deptId}' ","String"),
    /**
     * 本部门及以下数据权限
     */
    THIS_DEPT_LEVEL_CHILDREN("4", "本部门及以下数据权限"," #{#column} IN ( #{@dss.getDeptAndChild( #user.deptId,#user.deptCode )} ) ", "String"),
    /**
     * 自定义部门数据权限
     */
    CUSTOMIZE_DEPT("5", "自定义部门数据权限"," #{#column} IN ( #{@dss.getRoleDeptCustom( #user.roleGuidList )} ) ","String");

    private final String code;

    private final String desc;

    /**
     * 语法 采用 spel 模板表达式
     */
    private final String sqlTemplate;

    private final String className;

    public static DataScopeType findCode(String code) {
        if (StringUtil.isBlank(code)) {
            return null;
        }
        for (DataScopeType type : values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return null;
    }

    DataScopeType(String code, String desc, String sqlTemplate, String className) {
        this.code = code;
        this.desc = desc;
        this.sqlTemplate = sqlTemplate;
        this.className = className;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getSqlTemplate() {
        return sqlTemplate;
    }

    public String getClassName() {
        return className;
    }
}
