package com.sinoy.core.common.utils.request;

public class PropertyResult {
    private Object resultData = null;
    private boolean isSuccess = true;

    public Object getResultData() {
        return resultData;
    }

    public void setResultData(Object resultData) {
        this.resultData = resultData;
    }

    public boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
