package com.sinoy.core.common.utils.dataformat;

import com.alibaba.fastjson2.JSONArray;
import com.aspose.words.License;

import java.io.File;
import java.io.InputStream;

/**
 * asposeword 文件处理类
 */
public class AsposeWordUtil {

    /**
     * 破解
     * @return
     */
    public static boolean getLicense() {
        boolean result = false;
        try {
            InputStream is = AsposeWordUtil.class.getClassLoader().getResourceAsStream("license.xml");
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 读取excel
     * @param file
     * @return
     */
    public static JSONArray readExcel(File file){

        /**
         * 待开发
         */

        return null;
    }

}
