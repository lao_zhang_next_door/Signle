package com.sinoy.core.common.props;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

public class WebProperties {

    /**
     * C 端访问地址前缀
     */
    public static Api AppApi = new Api("/app-api", "**.controller.app.**");

    /**
     * B 端访问地址前缀
     */
    public static Api AdminApi = new Api("/sino-api", "**.controller.admin.**");

    /**
     * 对外接口访问前缀
     */
    public static Api ExternalApi = new Api("/external-api", "**.controller.external.**");

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Valid
    public static class Api {

        /**
         * API 前缀，实现所有 Controller 提供的 RESTFul API 的统一前缀
         *
         *
         * 意义：通过该前缀，避免 Swagger、Actuator 意外通过 Nginx 暴露出来给外部，带来安全性问题
         *      这样，Nginx 只需要配置转发到 /api/* 的所有接口即可。
         *
         *
         */
        @NotEmpty(message = "API 前缀不能为空")
        public String prefix;

        /**
         * Controller 所在包的 Ant 路径规则
         *
         * 主要目的是，给该 Controller 设置指定的 {@link #prefix}
         */
        @NotEmpty(message = "Controller 所在包不能为空")
        public String controller;

    }

//    /**
//     * B 端访问地址前缀
//     */
//    public final static String AdminApi = "/sino-api";
//
//    /**
//     * C 端访问地址前缀
//     */
//    public final static String AppApi = "/app-api";

    /**
     * 构造B端访问地址
     * @param url
     * @return
     */
    public static String buildAdminApi(String url) {
        return AdminApi.prefix + url;
    }

    /**
     * 构造C端访问地址
     * @param url
     * @return
     */
    public static String buildAppApi(String url) {
        return AppApi.prefix + url;
    }

}
