package com.sinoy.core.common.enums;

/**
 * 是否枚举
 *
 * @author T470
 */
public enum IsOrNot {
    /**
     *
     */
    NOT(0, "否", false),
    IS(1, "是", true);

    private final int code;
    private final String value;
    private final boolean flag;

    IsOrNot(int code, String value, boolean flag) {
        this.code = code;
        this.value = value;
        this.flag = flag;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public boolean getFlag() {
        return flag;
    }

    public static boolean getFlagByCode(int code) {
        IsOrNot[] isOrNots = values();
        for (IsOrNot isOrNot : isOrNots) {
            if (isOrNot.getCode() == code) {
                return isOrNot.getFlag();
            }
        }
        return false;
    }
}
