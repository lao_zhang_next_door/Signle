package com.sinoy.core.common.utils.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

//分页参数
public class PageParam implements Serializable {

    private static final Integer CURRENT_PAGE = 1;
    private static final Integer PAGE_SIZE = 10;

    //页码，从 1 开始
    @NotNull(message = "页码不能为空")
    @Min(value = 1, message = "页码最小值为 1")
    private Integer currentPage = CURRENT_PAGE;

    //每页条数，最大值为 200
    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页条数最小值为 1")
    @Max(value = 2000, message = "每页条数最大值为 2000")
    private Integer pageSize = PAGE_SIZE;

    /**
     * 排序属性
     */
    private List<String> prop;

    /**
     * 排序方式：asc,desc
     */
    private List<String> order;

    /**
     * 分页列表参数
     */
    private Map<String,Object> params;

    public int getStart() {
        return (this.getCurrentPage() - 1) * this.getPageSize();
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public PageParam setParams(Map<String, Object> params) {
        this.params = params;
        return this;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public List<String> getProp() {
        return prop;
    }

    public void setProp(List<String> prop) {
        this.prop = prop;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }

    public PageParam setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }
}
