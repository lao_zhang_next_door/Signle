package com.sinoy.core.common.enums;

/**
 * 模块类型枚举
 *
 * @author wzl
 */
public enum ModuleTypeEnum {

    /**
     *
     */
    VUE3(1, "VUE3"),
    VUE2(0, "VUE2");

    private final int code;
    private final String value;

    private ModuleTypeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
