package com.sinoy.core.sharding.config;

import com.alibaba.druid.spring.boot.autoconfigure.properties.DruidStatProperties;
import com.sinoy.core.common.utils.factory.YamlPropertySourceFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Druid配置
 *
 */
@Configuration
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:xnw-druid.yml")
// 打开和关闭druid模式加密
//@PropertySource(value = "classpath:mate-decrypt.properties")
public class DruidConfiguration {
	/**
	 * 创建 DruidAdRemoveFilter 过滤器，过滤 common.js 的广告
	 */
	@Bean
	@ConditionalOnProperty(name = "spring.datasource.druid.stat-view-servlet.enabled", havingValue = "true")
	public FilterRegistrationBean<DruidAdRemoveFilter> druidAdRemoveFilterFilter(DruidStatProperties properties) {
		// 获取 druid web 监控页面的参数
		DruidStatProperties.StatViewServlet config = properties.getStatViewServlet();
		// 提取 common.js 的配置路径
		String pattern = config.getUrlPattern() != null ? config.getUrlPattern() : "/druid/*";
		String commonJsPattern = pattern.replaceAll("\\*", "js/common.js");
		// 创建 DruidAdRemoveFilter Bean
		FilterRegistrationBean<DruidAdRemoveFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new DruidAdRemoveFilter());
		registrationBean.addUrlPatterns(commonJsPattern);

		registrationBean.addInitParameter("loginUsername",config.getLoginUsername());
		registrationBean.addInitParameter("loginPassword",config.getLoginPassword());
		registrationBean.addInitParameter("deny",config.getDeny());
		registrationBean.addInitParameter("allow",config.getAllow());
		return registrationBean;
	}

//	/**
//	 * 注册一个：filterRegistrationBean
//	 * 采集web-jdbc关联监控的数据
//	 * @return
//	 */
//	@Bean
//	public FilterRegistrationBean druidStatFilter() {
//		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
//
//		//添加过滤规则.
//		filterRegistrationBean.addUrlPatterns("/*");
//
//		//添加不需要忽略的格式信息.
//		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
//
//		return filterRegistrationBean;
//	}
//
//	/**
//	 * 注册一个StatViewServlet
//	 * 展示Druid的统计信息
//	 *
//	 * @return
//	 */
//	@Bean
//	public ServletRegistrationBean DruidStatViewServle() {
//
//		//org.springframework.boot.context.embedded.ServletRegistrationBean提供类的进行注册.
//		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
//
//		//添加初始化参数：initParams
//
//		//白名单：
//		servletRegistrationBean.addInitParameter("allow", "127.0.0.1");
//
//		//IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
////		servletRegistrationBean.addInitParameter("deny", "192.168.0.114");
//
////		//登录查看信息的账号密码.
////		servletRegistrationBean.addInitParameter("loginUsername", "admin");
////		servletRegistrationBean.addInitParameter("loginPassword", "matecloud");
//
//		//是否能够重置数据.
//		servletRegistrationBean.addInitParameter("resetEnable", "false");
//		return servletRegistrationBean;
//	}
}
