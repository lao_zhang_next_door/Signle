package com.sinoy.core.datascope.dao;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sinoy.core.datascope.entity.FrameDeptScope;

/**
 * 注意要加忽略 拦截 注解，防止死循环
 */
@InterceptorIgnore
public interface FrameDeptDaoScope extends BaseMapper<FrameDeptScope> {

}
