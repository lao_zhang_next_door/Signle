package com.sinoy.core.datascope.service;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;

import java.util.List;

/**
 * 通用数据权限
 */
public interface DataScopeService {

    /**
     * 获取部门及以下权限
     *
     * @param deptId 部门id
     * @return 部门id组
     */
    String getDeptAndChild(Long deptId,String deptCode);

    /**
     * 获取自定义部门数据权限
     * @param roleGuid 多个角色会以,号隔开
     * @return
     */
    String getRoleDeptCustom(String roleGuid);

}
