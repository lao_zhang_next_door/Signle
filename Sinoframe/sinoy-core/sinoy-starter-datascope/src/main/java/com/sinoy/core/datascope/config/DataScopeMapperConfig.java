package com.sinoy.core.datascope.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.sinoy.core.datascope.dao")
@Configuration
public class DataScopeMapperConfig {
}
