package com.sinoy.core.datascope.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.dataformat.stream.StreamUtils;
import com.sinoy.core.datascope.dao.FrameDeptDaoScope;
import com.sinoy.core.datascope.dao.FrameRoleDeptDaoScope;
import com.sinoy.core.datascope.entity.FrameDeptScope;
import com.sinoy.core.datascope.entity.FrameRoleDeptScope;
import com.sinoy.core.datascope.service.DataScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("dss")
public class DataScopeServiceImpl implements DataScopeService {

    @Autowired
    private FrameDeptDaoScope frameDeptDao;

    @Autowired
    private FrameRoleDeptDaoScope frameRoleDeptDaoScope;

    @Override
    public String getDeptAndChild(Long deptId,String deptCode) {
        /**
         * 查询 该deptId 子集
         */
        QueryWrapper<FrameDeptScope> queryWrapper = new QueryWrapper();
        queryWrapper.select("row_id");
        queryWrapper.likeRight("dept_code",deptCode+".");
        queryWrapper.eq("del_flag", DelFlag.Normal.getCode());
        List<FrameDeptScope> deptList = frameDeptDao.selectList(queryWrapper);

        List<Long> ids = StreamUtils.toList(deptList, FrameDeptScope::getRowId);
        ids.add(deptId);

        if (CollUtil.isNotEmpty(ids)) {
            return StreamUtils.join(ids, d -> Convert.toStr(d));
        }
        return null;
    }

    /**
     * @param roleGuid 多个角色会以,号隔开
     * @return
     */
    @Override
    public String getRoleDeptCustom(String roleGuid) {

        List<String> roleGuids = new ArrayList();
        if(roleGuid.contains(",")){
            roleGuids = Arrays.asList(roleGuid.split(","));
        }else{
            roleGuids.add(roleGuid);
        }

//        QueryWrapper<FrameRoleDeptScope> queryWrapper = new QueryWrapper();
//        queryWrapper.select("DISTINCT dept_guid");
//        queryWrapper.eq("role_guid",roleGuid);
//        List<FrameRoleDeptScope> frameRoleDeptScopeList = frameRoleDeptDaoScope.selectList(queryWrapper);

        List<FrameRoleDeptScope> frameRoleDeptScopeList = frameRoleDeptDaoScope.getRoleDeptCustom(roleGuids);
        List<Long> ids = StreamUtils.toList(frameRoleDeptScopeList,FrameRoleDeptScope::getDeptId);

        if (CollUtil.isNotEmpty(ids)) {
            return StreamUtils.join(ids, d -> Convert.toStr(d));
        }
        return null;
    }
}
