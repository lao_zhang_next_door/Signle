package com.sinoy.core.datascope;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ConcurrentHashSet;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.JSON;
import com.sinoy.core.common.enums.DataScopeType;
import com.sinoy.core.common.utils.dataformat.StringUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.datascope.anno.DataColumn;
import com.sinoy.core.datascope.anno.DataPermission;
import com.sinoy.core.security.userdetails.XnwUser;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * <p> mybatis-plus 数据权限处理器 </p>
 *
 */
public class MyDataPermissionHandler {

    /**
     * 方法或类(名称) 与 注解的映射关系缓存
     */
    private final Map<String, DataPermission> dataPermissionCacheMap = new ConcurrentHashMap<>();

    /**
     * 无效注解方法缓存用于快速返回
     */
    private final Set<String> inavlidCacheSet = new ConcurrentHashSet<>();

    /**
     * spel 解析器
     */
    private final ExpressionParser parser = new SpelExpressionParser();
    private final ParserContext parserContext = new TemplateParserContext();
    /**
     * bean解析器 用于处理 spel 表达式中对 bean 的调用
     */
    private final BeanResolver beanResolver = new BeanFactoryResolver(SpringUtil.getBeanFactory());

    private final CommonPropAndMethods commonPropAndMethods;

    public MyDataPermissionHandler(CommonPropAndMethods commonPropAndMethods) {
        this.commonPropAndMethods = commonPropAndMethods;
    }

    public Expression getSqlSegment(Expression where, String mappedStatementId, boolean isSelect) {
        DataPermission dataPermission = DataPermissionContextHolder.get();
        //无该注解则跳过
        if(dataPermission == null){
//            inavlidCacheSet.add(mappedStatementId);
            return where;
        }
        DataColumn[] dataColumns = dataPermission.value();
//        DataColumn[] dataColumns = findAnnotation(mappedStatementId);
        //判断是否是无权限注解
        if (ArrayUtil.isEmpty(dataColumns)) {
            inavlidCacheSet.add(mappedStatementId);
            return where;
        }
        XnwUser currentUser = commonPropAndMethods.getXnwUser();

        // 如果是超级管理员，则不过滤数据
        if (currentUser.getIsAdmin()) {
            return where;
        }
        String dataFilterSql = buildDataFilter(dataColumns, isSelect);
        if (StringUtil.isBlank(dataFilterSql)) {
            return where;
        }
        try {
            Expression expression = CCJSqlParserUtil.parseExpression(dataFilterSql);
            // 数据权限使用单独的括号 防止与其他条件冲突
            Parenthesis parenthesis = new Parenthesis(expression);
            if (ObjectUtil.isNotNull(where)) {
                return new AndExpression(where, parenthesis);
            } else {
                return parenthesis;
            }
        } catch (JSQLParserException e) {
            throw new BaseException("数据权限解析异常 => " + e.getMessage());
        }
    }

    /**
     * 构造数据过滤sql
     */
    private String buildDataFilter(DataColumn[] dataColumns, boolean isSelect) {
        // 更新或删除需满足所有条件
        String joinStr = isSelect ? " OR " : " AND ";
        XnwUser user = commonPropAndMethods.getXnwUser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver(beanResolver);
        context.setVariable("user",user);
        Set<String> conditions = new HashSet<>();
        List<String> dataScopeList = user.getDataScope();
        for (int i = 0; i < dataScopeList.size(); i++) {
            // 获取角色权限泛型
            DataScopeType type = DataScopeType.findCode(dataScopeList.get(i));
            if (ObjectUtil.isNull(type)) {
                throw new BaseException("角色数据范围异常 => " + dataScopeList.get(i));
            }
            // 全部数据权限直接返回
            if (type == DataScopeType.ALL) {
                return "";
            }
            leg:
            for (DataColumn dataColumn : dataColumns) {
                if (dataColumn.key().length != dataColumn.value().length) {
                    throw new BaseException("角色数据范围异常 => key与value长度不匹配");
                }
                // 不包含 key 变量 则不处理
                String[] strArr = Arrays.stream(dataColumn.key()).map(key -> "#" + key).toArray(String[]::new);
                for (String s : strArr) {
                    if(!type.getSqlTemplate().contains(s)){
                        continue leg;
                    }
                }
                // 设置注解变量 key 为表达式变量 value 为变量值
                for (int j = 0; j < dataColumn.key().length; j++) {
                    context.setVariable(dataColumn.key()[j], dataColumn.value()[j]);
                }

                // 解析sql模板并填充
                String sql = parser.parseExpression(type.getSqlTemplate(), parserContext).getValue(context,String.class);
                conditions.add(joinStr + sql);
            }
//            // 未处理成功则填充兜底方案
//            if (!isSuccess && StringUtils.isNotBlank(type.getElseSql())) {
//                conditions.add(joinStr + type.getElseSql());
//            }
        }

        if (CollUtil.isNotEmpty(conditions)) {
            String sql = conditions.stream().map(Function.identity()).filter(Objects::nonNull).collect(Collectors.joining(""));
            return sql.substring(joinStr.length());
        }
        return "";
    }

//    public static void main(String[] args) {
//        String[] arr = new String[]{"#column"};
//        System.out.println(StringUtils.contains("",arr));
//    }

//    public static void main(String[] args) {
//        ParserContext parserContext = new TemplateParserContext();
//        ExpressionParser parser = new SpelExpressionParser();
//        StandardEvaluationContext context = new StandardEvaluationContext();
////        BeanResolver beanResolver = new BeanFactoryResolver(SpringUtil.getBeanFactory());
////        context.setBeanResolver(beanResolver);
//        List<String> deptString = new ArrayList();
//        deptString.add("1");
//        deptString.add("2");
//        deptString.add("3");
//        context.setVariable("column",deptString);
////        context.setVariable("column","12345");
//        List<String> res = parser.parseExpression("#{#column} = 1", parserContext).getValue(context,List.class);
//        System.out.println(JSON.toJSONString(res));
//    }

//    private DataColumn[] findAnnotation(String mappedStatementId) {
//        StringBuilder sb = new StringBuilder(mappedStatementId);
//        int index = sb.lastIndexOf(".");
//        String clazzName = sb.substring(0, index);
//        String methodName = sb.substring(index + 1, sb.length());
//        Class<?> clazz = ClassUtil.loadClass(clazzName);
//        List<Method> methods = Arrays.stream(ClassUtil.getDeclaredMethods(clazz))
//                .filter(method -> method.getName().equals(methodName)).collect(Collectors.toList());
//        DataPermission dataPermission;
//        // 获取方法注解
//        for (Method method : methods) {
//            dataPermission = dataPermissionCacheMap.get(mappedStatementId);
//            if (ObjectUtil.isNotNull(dataPermission)) {
//                return dataPermission.value();
//            }
//            if (AnnotationUtil.hasAnnotation(method, DataPermission.class)) {
//                dataPermission = AnnotationUtil.getAnnotation(method, DataPermission.class);
//                dataPermissionCacheMap.put(mappedStatementId, dataPermission);
//                return dataPermission.value();
//            }
//        }
//        dataPermission = dataPermissionCacheMap.get(clazz.getName());
//        if (ObjectUtil.isNotNull(dataPermission)) {
//            return dataPermission.value();
//        }
//        // 获取类注解
//        if (AnnotationUtil.hasAnnotation(clazz, DataPermission.class)) {
//            dataPermission = AnnotationUtil.getAnnotation(clazz, DataPermission.class);
//            dataPermissionCacheMap.put(clazz.getName(), dataPermission);
//            return dataPermission.value();
//        }
//        return null;
//    }

    /**
     * 是否为无效方法 无数据权限
     */
    public boolean isInvalid(String mappedStatementId) {
        return inavlidCacheSet.contains(mappedStatementId);
    }


    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("[^0-9](\\d{15})$");
        System.out.println(pattern.matcher("010600007-孙保明-320521191919196603125117610-卫月芳-320521670516512").find());


    }


}
