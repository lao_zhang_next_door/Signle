package com.sinoy.core.datascope.dao;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sinoy.core.datascope.entity.FrameRoleDeptScope;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 注意要加忽略 拦截 注解，防止死循环
 */
@InterceptorIgnore
public interface FrameRoleDeptDaoScope extends BaseMapper<FrameRoleDeptScope> {

    /**
     * 根据 角色Guid 查询自定义部门权限  若多个角色存在 取并集
     * @param roleGuids
     * @return
     */
    List<FrameRoleDeptScope> getRoleDeptCustom(@Param("roleGuids") List<String> roleGuids);
}
