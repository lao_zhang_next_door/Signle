package com.sinoy.core.datascope.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.common.base.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-20-10:49
 **/
@TableName("frame_dept")
public class FrameDeptScope extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
    public FrameDeptScope(boolean init) {
		super(init);
	}

	public FrameDeptScope() {}

	public FrameDeptScope(String rowGuid){
    	super(rowGuid);
	}
	
	/**
	 * 部门名称
	 */
	private String deptName;
	
	/**
	 * 部门简称
	 */
	private String shortName;
	
	/**
	 * 部门编码
	 */
	private String oucode;
	
	/**
	 * 部门电话
	 */
	private String tel;
	
	/**
	 * 传真
	 */
	private String fax;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 部门简介
	 */
	private String description;
	
	/**
	 * 父级guid
	 */
	private String parentGuid;

	/**
	 * 部门级别
	 */
	private String deptCode;

	/**
	 * 部门级别
	 */
	private Integer deptLevel;

	/**
	 * 部门状态
	 */
	private Character deptStatus;

	/**
	 * 部门类型
	 */
	private Character deptType;

	public Character getDeptStatus() {
		return deptStatus;
	}

	public void setDeptStatus(Character deptStatus) {
		this.deptStatus = deptStatus;
	}

	public Character getDeptType() {
		return deptType;
	}

	public void setDeptType(Character deptType) {
		this.deptType = deptType;
	}

	public String getDeptName() {
		return deptName;
	}

	public FrameDeptScope setDeptName(String deptName) {
		this.deptName = deptName;
		return this;
	}

	public String getShortName() {
		return shortName;
	}

	public FrameDeptScope setShortName(String shortName) {
		this.shortName = shortName;
		return this;
	}

	public String getOucode() {
		return oucode;
	}

	public FrameDeptScope setOucode(String oucode) {
		this.oucode = oucode;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public FrameDeptScope setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public FrameDeptScope setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public FrameDeptScope setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public FrameDeptScope setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getParentGuid() {
		return parentGuid;
	}

	public FrameDeptScope setParentGuid(String parentGuid) {
		this.parentGuid = parentGuid;
		return this;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public FrameDeptScope setDeptCode(String deptCode) {
		this.deptCode = deptCode;
		return this;
	}

	public Integer getDeptLevel() {
		return deptLevel;
	}

	public FrameDeptScope setDeptLevel(Integer deptLevel) {
		this.deptLevel = deptLevel;
		return this;
	}
}
