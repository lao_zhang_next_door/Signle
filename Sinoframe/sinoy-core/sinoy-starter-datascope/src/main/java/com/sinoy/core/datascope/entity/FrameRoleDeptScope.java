package com.sinoy.core.datascope.entity;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("frame_role_dept")
public class FrameRoleDeptScope {

    //角色
    private String roleGuid;

    //部门
    private String deptGuid;

    //部门名
    private String deptName;

    //部门id
    private Long deptId;

    public String getRoleGuid() {
        return roleGuid;
    }

    public FrameRoleDeptScope setRoleGuid(String roleGuid) {
        this.roleGuid = roleGuid;
        return this;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public FrameRoleDeptScope setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
        return this;
    }

    public String getDeptName() {
        return deptName;
    }

    public FrameRoleDeptScope setDeptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public Long getDeptId() {
        return deptId;
    }

    public FrameRoleDeptScope setDeptId(Long deptId) {
        this.deptId = deptId;
        return this;
    }
}
