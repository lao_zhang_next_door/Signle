package com.sinoy.core.oss.core.client.local;

import com.sinoy.core.oss.core.client.FileClientConfig;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;

/**
 * 本地文件客户端的配置类
 *
 */
public class LocalFileClientConfig implements FileClientConfig {

    /**
     * 基础路径
     */
    @NotEmpty(message = "基础路径不能为空")
    private String basePath;

    /**
     * 自定义域名
     */
    @NotEmpty(message = "domain 不能为空")
    @URL(message = "domain 必须是 URL 格式")
    private String domain;

    public String getBasePath() {
        return basePath;
    }

    public LocalFileClientConfig setBasePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public LocalFileClientConfig setDomain(String domain) {
        this.domain = domain;
        return this;
    }
}
