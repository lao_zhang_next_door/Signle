package com.sinoy.core.oss.core.enums;

import cn.hutool.core.util.ArrayUtil;
import com.sinoy.core.oss.core.client.FileClient;
import com.sinoy.core.oss.core.client.FileClientConfig;
import com.sinoy.core.oss.core.client.db.DBFileClient;
import com.sinoy.core.oss.core.client.db.DBFileClientConfig;
import com.sinoy.core.oss.core.client.ftp.FtpFileClient;
import com.sinoy.core.oss.core.client.ftp.FtpFileClientConfig;
import com.sinoy.core.oss.core.client.local.LocalFileClient;
import com.sinoy.core.oss.core.client.local.LocalFileClientConfig;
import com.sinoy.core.oss.core.client.s3.S3FileClient;
import com.sinoy.core.oss.core.client.s3.S3FileClientConfig;
import com.sinoy.core.oss.core.client.sftp.SftpFileClient;
import com.sinoy.core.oss.core.client.sftp.SftpFileClientConfig;

/**
 * 文件存储器枚举
 *
 */
public enum FileStorageEnum {

    DB(1, DBFileClientConfig.class, DBFileClient.class),

    LOCAL(10, LocalFileClientConfig.class, LocalFileClient.class),
    FTP(11, FtpFileClientConfig.class, FtpFileClient.class),
    SFTP(12, SftpFileClientConfig.class, SftpFileClient.class),

    S3(20, S3FileClientConfig.class, S3FileClient.class),
    ;

    FileStorageEnum(Integer storage, Class<? extends FileClientConfig> configClass, Class<? extends FileClient> clientClass) {
        this.storage = storage;
        this.configClass = configClass;
        this.clientClass = clientClass;
    }

    /**
     * 存储器
     */
    private final Integer storage;

    /**
     * 配置类
     */
    private final Class<? extends FileClientConfig> configClass;
    /**
     * 客户端类
     */
    private final Class<? extends FileClient> clientClass;

    public static FileStorageEnum getByStorage(Integer storage) {
        return ArrayUtil.firstMatch(o -> o.getStorage().equals(storage), values());
    }

    public Integer getStorage() {
        return storage;
    }

    public Class<? extends FileClientConfig> getConfigClass() {
        return configClass;
    }

    public Class<? extends FileClient> getClientClass() {
        return clientClass;
    }
}
