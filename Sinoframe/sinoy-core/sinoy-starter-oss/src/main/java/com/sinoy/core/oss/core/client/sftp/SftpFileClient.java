package com.sinoy.core.oss.core.client.sftp;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.extra.ssh.JschUtil;
import cn.hutool.extra.ssh.Sftp;
import com.alibaba.fastjson2.JSON;
import com.jcraft.jsch.*;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.oss.core.client.AbstractFileClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Sftp 文件客户端
 *
 */
public class SftpFileClient extends AbstractFileClient<SftpFileClientConfig> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Sftp sftp;

    public SftpFileClient(Long id, SftpFileClientConfig config) {
        super(id, config);
    }

    // 开启或刷新sftp
    private void openFreshSftp(){
        ChannelSftp channelSftp = sftp.getClient();
        //若sftp已关闭 则初始化
        if(channelSftp == null || channelSftp.isClosed()){
            logger.info("sftp 已关闭,尝试初始化");
            doInit();
            return;
        }

        //若sftp 断开连接 则尝试重新连接
        if(!channelSftp.isConnected()){
            logger.info("sftp 已断开连接,尝试重连");
            try {
                channelSftp.connect(60000);
            } catch (JSchException e) {
                e.printStackTrace();
                throw new BaseException(e.getMessage());
            }
            return;
        }
    }

    // 关闭sftp
    private void closeSftp(){
        ChannelSftp channelSftp = sftp.getClient();
        Session session = null;
        try {
            session = channelSftp.getSession();
        } catch (JSchException e) {
            e.printStackTrace();
        } finally {
            //若sftp 在连接状态中 则关闭连接
            if(channelSftp.isConnected()){
                logger.info("sftp 即将断开连接");
                channelSftp.disconnect();
            }

            if(session != null){
                logger.info("session 即将断开连接");
                session.disconnect();
            }
        }

    }

    @Override
    protected void doInit() {
        // 补全风格。例如说 Linux 是 /，Windows 是 \
//        if (!config.getBasePath().endsWith(File.separator)) {
//            config.setBasePath(config.getBasePath() + File.separator);
//        }
        logger.info("sftp 初始化参数 host:"+config.getHost()+" port:"+config.getPort()+" password:"+config.getPassword()+" username:"+config.getUsername());
        // 初始化 Ftp 对象
        this.sftp = new Sftp(JschUtil.openSession(config.getHost(), config.getPort(), config.getUsername(), config.getPassword()));
    }

    @Override
    public synchronized String upload(byte[] content, String path) {
        logger.info("上传文件地址>>>>>"+ path);
        logger.info("content 长度>>>>>>"+ content.length);
        // 执行写入
        String filePath = getFilePath(path);
        // 创建文件，通过 UUID 保证唯一
        File file = null;
        try {
            file = File.createTempFile(IdUtil.simpleUUID(), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 标记 JVM 退出时，自动删除
        file.deleteOnExit();
        // 写入内容
        FileUtil.writeBytes(content, file);

        openFreshSftp();

        try {
            // 修改gbk编码 下载linux中 带有中文目录的 文件
            ChannelSftp channelSftp = sftp.getClient();
            Class cl = channelSftp.getClass();
            Field f1 =cl.getDeclaredField("server_version");
            f1.setAccessible(true);
            f1.set(channelSftp, 2);
            channelSftp.setFilenameEncoding("UTF-8");
        } catch (SftpException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        //获取目录
        String parentPath = filePath.substring(0,filePath.lastIndexOf("/"));
        //判断是否已存在当前目录
        if(!isDirExist(parentPath,sftp.getClient())){
            createDir(parentPath,sftp.getClient());
        }
        logger.info("上传文件最终地址>>>>>"+ filePath);
        logger.info("file>>>>"+ JSON.toJSONString(file));
        sftp.upload(filePath, file);

        closeSftp();
        // 拼接返回路径
        return super.formatFileUrl(config.getDomain(), path);
    }

    @Override
    public void delete(String path) {
        String filePath = getFilePath(path);
        openFreshSftp();
        sftp.delFile(filePath);
        closeSftp();
    }

    @Override
    public synchronized byte[] getContent(String path) {
        String filePath = getFilePath(path);
        // 创建文件，通过 UUID 保证唯一
        File destFile = null;
        try {
            destFile = File.createTempFile(IdUtil.simpleUUID(), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 标记 JVM 退出时，自动删除
        destFile.deleteOnExit();
//        try {
//            // 修改gbk编码 下载linux中 带有中文目录的 文件
//            ChannelSftp channelSftp = sftp.getClient();
//            Class cl = channelSftp.getClass();
//            Field f1 =cl.getDeclaredField("server_version");
//            f1.setAccessible(true);
//            f1.set(channelSftp, 2);
//            channelSftp.setFilenameEncoding("GBK");
//        } catch (SftpException | NoSuchFieldException | IllegalAccessException e) {
//            e.printStackTrace();
//        }
        openFreshSftp();
        try{
            sftp.download(filePath, destFile);
        }catch (Exception e){
            closeSftp();
            e.printStackTrace();
            throw new BaseException(e.getMessage());
        }finally{
            closeSftp();
        }

        return FileUtil.readBytes(destFile);
    }

    @Override
    public String getFilePath(String path) {
        return config.getBasePath() + path;
    }

    @Override
    public String getDomain() {
        return config.getDomain();
    }

    @Override
    public String formatFileUrl(String path){
        return super.formatFileUrl(config.getDomain(), path);
    }

    /**
     * 判断目录是否存在
     * @param directory
     * @return
     */
    public boolean isDirExist(String directory, ChannelSftp sftp)
    {
        boolean isDirExistFlag = false;
        try
        {
            SftpATTRS sftpATTRS = sftp.lstat(directory);
            isDirExistFlag = true;
            return sftpATTRS.isDir();
        }
        catch (Exception e)
        {
            if (e.getMessage().toLowerCase().equals("no such file"))
            {
                isDirExistFlag = false;
            }
        }
        return isDirExistFlag;
    }

    /**
     * 创建目录
     * @param createpath
     * @param sftp
     * @return
     */
    public boolean createDir(String createpath, ChannelSftp sftp) {
        try
        {
            if (isDirExist(createpath,sftp)) {
                sftp.cd(createpath);
                return true;
            }
            String pathArry[] = createpath.split("/");
            StringBuffer filePath = new StringBuffer("/");
            for (String path : pathArry) {
                if (path.equals("")) {
                    continue;
                }
                filePath.append(path + "/");
                if (isDirExist(filePath.toString(),sftp)) {
                    sftp.cd(filePath.toString());
                } else {
                    // 建立目录
                    sftp.mkdir(filePath.toString());
                    // 进入并设置为当前目录
                    sftp.cd(filePath.toString());
                }
            }
            sftp.cd(createpath);
            return true;
        }
        catch (SftpException e) {
            e.printStackTrace();
        }
        return false;
    }



}
