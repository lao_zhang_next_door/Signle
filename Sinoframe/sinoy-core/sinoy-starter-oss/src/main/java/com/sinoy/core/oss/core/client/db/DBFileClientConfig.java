package com.sinoy.core.oss.core.client.db;

import com.sinoy.core.oss.core.client.FileClientConfig;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;

/**
 * 基于 DB 存储的文件客户端的配置类
 *
 */
public class DBFileClientConfig implements FileClientConfig {

    /**
     * 自定义域名
     */
    @NotEmpty(message = "domain 不能为空")
    @URL(message = "domain 必须是 URL 格式")
    private String domain;

    public String getDomain() {
        return domain;
    }

    public DBFileClientConfig setDomain(String domain) {
        this.domain = domain;
        return this;
    }
}
