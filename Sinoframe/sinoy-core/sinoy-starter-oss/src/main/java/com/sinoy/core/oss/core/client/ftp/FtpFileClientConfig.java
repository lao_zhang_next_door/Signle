package com.sinoy.core.oss.core.client.ftp;

import com.sinoy.core.oss.core.client.FileClientConfig;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Ftp 文件客户端的配置类
 *
 */
public class FtpFileClientConfig implements FileClientConfig {

    /**
     * 基础路径
     */
    @NotEmpty(message = "基础路径不能为空")
    private String basePath;

    /**
     * 自定义域名
     */
    @NotEmpty(message = "domain 不能为空")
    @URL(message = "domain 必须是 URL 格式")
    private String domain;

    /**
     * 主机地址
     */
    @NotEmpty(message = "host 不能为空")
    private String host;
    /**
     * 主机端口
     */
    @NotNull(message = "port 不能为空")
    private Integer port;
    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    private String username;
    /**
     * 密码
     */
    @NotEmpty(message = "密码不能为空")
    private String password;
    /**
     * 连接模式
     *
     * 使用 {@link  cn.hutool.extra.ftp.FtpMode} 对应的字符串
     */
    @NotEmpty(message = "连接模式不能为空")
    private String mode;

    public String getBasePath() {
        return basePath;
    }

    public FtpFileClientConfig setBasePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public FtpFileClientConfig setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public String getHost() {
        return host;
    }

    public FtpFileClientConfig setHost(String host) {
        this.host = host;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public FtpFileClientConfig setPort(Integer port) {
        this.port = port;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public FtpFileClientConfig setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public FtpFileClientConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getMode() {
        return mode;
    }

    public FtpFileClientConfig setMode(String mode) {
        this.mode = mode;
        return this;
    }
}
