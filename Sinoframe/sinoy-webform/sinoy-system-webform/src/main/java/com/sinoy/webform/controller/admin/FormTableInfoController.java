package com.sinoy.webform.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.request.RequestUtil;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.webform.component.entity.FormTableInfo;
import com.sinoy.webform.component.entity.vo.FormTableVo;
import com.sinoy.webform.component.service.FormTableInfoService;
import com.sinoy.webform.component.service.FormTablefieldService;
import com.sinoy.webform.component.service.GenerateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author jtr
 * 表信息控制器
 * @creatTime 2021-04-23-13:14
 **/
/*@CrossOrigin*/
@RestController
@RequestMapping("/webform/FormTableInfo")
public class FormTableInfoController {

    @Autowired
    private FormTablefieldService formTablefieldService;

    @Autowired
    private FormTableInfoService formTableInfoService;

    @Autowired
    private GenerateCodeService generateCodeService;

    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        QueryWrapper<FormTableInfo> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FormTableInfo> page = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FormTableInfo> iPage = formTableInfoService.page(page, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @PostMapping("/add")
    private R add(@RequestBody FormTableInfo tableInfo) {

        try {
            formTablefieldService.addTable(tableInfo.getPhysicalName(),"表");
        } catch (Exception e) {
            e.printStackTrace();
        }
        tableInfo.initNull();
        formTableInfoService.save(tableInfo);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FormTableInfo tableInfo) {
        try {
            FormTableInfo f = formTableInfoService.selectByGuid(tableInfo.getRowGuid());
            formTablefieldService.updateTable(f.getPhysicalName(), tableInfo.getPhysicalName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        int code = formTableInfoService.updateByRowGuid(tableInfo);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            FormTableInfo del = formTableInfoService.selectByGuid(rowGuid);
            if (del != null) {
                try {
                    formTablefieldService.delTable(del.getPhysicalName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        formTableInfoService.deleteBatchByGuids(rowGuids);
        return R.ok();
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        return R.ok().put("data", formTableInfoService.selectByGuid(rowGuid));
    }

    /**
     * 代码生成
     *
     * @return
     */
    @PostMapping("/generateCode")
    private R generateCode(@RequestBody FormTableVo formTableVo) {

        if (RequestUtil.isFieldBlank(formTableVo, "packageName", "outPutDir", "tableName")) {
            return R.error("入参不可为空");
        }

        generateCodeService.genCode(formTableVo.getOutPutDir(), formTableVo.getPackageName(), formTableVo.getTableName());
        return R.ok("成功生成代码");
    }
}
