package com.sinoy.webform.controller.admin;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ZipUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.common.utils.request.ServletUtils;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.core.datasource.entity.DataSourceConfigDO;
import com.sinoy.core.security.utils.CommonPropAndMethods;
import com.sinoy.webform.component.entity.CodegenColumnDO;
import com.sinoy.webform.component.entity.CodegenTableDO;
import com.sinoy.webform.component.entity.FormTableField;
import com.sinoy.webform.component.entity.maps.CodegenConvert;
import com.sinoy.webform.component.entity.vo.CodegenCreateListReqVO;
import com.sinoy.webform.component.entity.vo.CodegenCreateReqVO;
import com.sinoy.webform.component.entity.vo.CodegenUpdateReqVO;
import com.sinoy.webform.component.service.CodegenColumnService;
import com.sinoy.webform.component.service.CodegenService;
import com.sinoy.webform.component.service.FormTableInfoService;
import com.sinoy.webform.component.service.FormTablefieldService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/form/codegen")
@Validated
public class CodegenController {

    @Resource
    private CodegenService codegenService;

    @Resource
    private CodegenColumnService codegenColumnService;

    @Resource
    private CommonPropAndMethods commonPropAndMethods;

    @Resource
    private FormTableInfoService formTableInfoService;

    @Resource
    private FormTablefieldService formTablefieldService;

    /**
     * 获得数据库自带的表定义列表
     *
     * @param dataSourceConfigId 数据源配置的编号
     * @param name               表名，模糊匹配
     * @param comment            描述，模糊匹配
     * @return
     */
    @GetMapping("/db/table/list")
    public R getDatabaseTableList(
            @RequestParam(value = "dataSourceConfigId") Long dataSourceConfigId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "comment", required = false) String comment) {
        return R.ok().put("data", codegenService.getDatabaseTableList(dataSourceConfigId, name, comment));
    }

    /**
     * 获得表定义分页
     *
     * @return
     */
    @GetMapping("/table/page")
    public R getCodeGenTablePage(@Valid Search search) {
        QueryWrapper<CodegenTableDO> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<CodegenTableDO> pageParam = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<CodegenTableDO> pageResult = codegenService.page(pageParam, queryWrapper);
        return R.ok().put("data", pageResult);
    }

    /**
     * 获得表和字段的明细
     *
     * @param tableId 表编号
     * @return
     */
    @GetMapping("/detail")
    public R getCodegenDetail(@RequestParam("tableId") Long tableId) {
        CodegenTableDO table = codegenService.getCodegenTablePage(tableId);
        List<CodegenColumnDO> columns = codegenService.getCodegenColumnListByTableId(tableId);
        // 拼装返回
        return R.ok().put("data", CodegenConvert.INSTANCE.convert(table, columns));
    }

    /**
     * 获得字段分页的明细
     *
     * @param search
     * @return
     */
    @GetMapping("/getColumnsList")
    public R getColumnsList(@Valid Search search) {
        QueryWrapper<CodegenColumnDO> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<CodegenColumnDO> pageParam = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<CodegenColumnDO> pageResult = codegenColumnService.page(pageParam, queryWrapper);
        return R.ok().put("data", pageResult);
    }

    /**
     * 基于数据库的表结构，创建代码生成器的表和字段定义
     *
     * @param reqVO
     * @return
     */
    @PostMapping("/create-list")
    public R createCodegenList(@Valid @RequestBody CodegenCreateListReqVO reqVO) {
        return R.ok().put("data", codegenService.createCodegenList(commonPropAndMethods.getUserId(), reqVO));
    }

    /**
     * 创建数据库表 以及 代码生成器的表的定义
     *
     * @param table
     * @return
     */
    @Transactional
    @PostMapping("/create")
    public R createCodegen(@Valid @RequestBody CodegenCreateReqVO table) {
        CodegenCreateReqVO.Table t = table.getTable();
        // 生成创建 表和列
        try {
            formTablefieldService.addTable(t.getTableName(), t.getTableComment());
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.getMessage());
        }
        codegenService.createCodegen(commonPropAndMethods.getUserId(), DataSourceConfigDO.ID_MASTER, table.getTable().getTableName());
        return R.ok();
    }

    /**
     * 更新数据库的表和字段定义
     *
     * @param updateReqVO
     * @return
     */
    @PutMapping("/update")
    public R updateCodegen(@Valid @RequestBody CodegenUpdateReqVO updateReqVO) {
        codegenService.updateCodegen(updateReqVO);
        return R.ok();
    }

    /**
     * 添加数据库字段 并同步至字段表
     *
     * @param formTableFieldList 字段集合
     * @param tableId            表id
     * @return
     */
    @PostMapping("/addCodegenFields")
    public R addCodegenFields(@RequestBody List<FormTableField> formTableFieldList, @RequestParam Long tableId) {
        //查询tableId
        CodegenTableDO table = codegenService.getCodegenTablePage(tableId);
        if (table == null) {
            return R.error("库中不存在该表定义");
        }

        //添加字段
        formTableFieldList.forEach((field) -> {
            try {
                formTablefieldService.insertFormField(field, table.getTableName());
            } catch (Exception e) {
                e.printStackTrace();
                throw new BaseException(e.getMessage());
            }
        });

        //同步字段库
        codegenService.syncCodegenFromDB(tableId);
        return R.ok();
    }

    /**
     * 基于数据库的表结构，同步数据库的表和字段定义
     *
     * @param tableId 表编号
     * @return
     */
    @PutMapping("/sync-from-db")
    public R syncCodegenFromDB(@RequestParam("tableId") Long tableId) {
        codegenService.syncCodegenFromDB(tableId);
        return R.ok();
    }

    /**
     * 删除数据库的表和字段定义
     *
     * @param tableId 表编号
     * @return
     */
    @DeleteMapping("/delete")
    public R deleteCodegen(@RequestParam("tableId") Long tableId) {
        codegenService.deleteCodegen(tableId);
        return R.ok();
    }

    /**
     * 预览生成代码
     *
     * @param tableId 表编号
     * @return
     */
    @GetMapping("/preview")
    public R previewCodegen(@RequestParam("tableId") Long tableId) {
        Map<String, String> codes = codegenService.generationCodes(tableId);
        return R.ok().put("data", CodegenConvert.INSTANCE.convert(codes));
    }

    /**
     * 下载生成代码
     *
     * @param tableId  表编号
     * @param response
     * @throws IOException
     */
    @GetMapping("/download")
    public void downloadCodegen(@RequestParam("tableId") Long tableId,
                                HttpServletResponse response) throws IOException {
        // 生成代码
        Map<String, String> codes = codegenService.generationCodes(tableId);
        // 构建 zip 包
        String[] paths = codes.keySet().toArray(new String[0]);
        ByteArrayInputStream[] ins = codes.values().stream().map(IoUtil::toUtf8Stream).toArray(ByteArrayInputStream[]::new);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipUtil.zip(outputStream, paths, ins);
        // 输出
        ServletUtils.writeAttachment(response, "codegen.zip", outputStream.toByteArray());
    }

}
