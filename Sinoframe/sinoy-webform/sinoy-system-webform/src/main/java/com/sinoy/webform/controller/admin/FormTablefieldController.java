package com.sinoy.webform.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.core.database.mybatisplus.entity.Search;
import com.sinoy.core.database.mybatisplus.util.BatisPlusUtil;
import com.sinoy.webform.component.entity.FormTableField;
import com.sinoy.webform.component.service.FormTablefieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/*@CrossOrigin*/
@RestController
@RequestMapping("/webform/FormTablefield")
public class FormTablefieldController {

    @Autowired
    private FormTablefieldService tableFieldService;

    @PostMapping("/listData")
    public R listData(@RequestBody Search search) {
        QueryWrapper<FormTableField> queryWrapper = new QueryWrapper<>();
        BatisPlusUtil.setParams(queryWrapper, search.getParams());
        BatisPlusUtil.setOrders(queryWrapper, search.getProp(), search.getOrder());
        Page<FormTableField> page = new Page<>(search.getCurrentPage(), search.getPageSize());
        Page<FormTableField> iPage = tableFieldService.page(page, queryWrapper);
        return R.list((int) iPage.getTotal(), iPage.getRecords());
    }

    @PostMapping("/add")
    public R addTableField(@RequestBody Map<String, Object> params) throws Exception {
        //插入表字段方法
        Boolean flag = tableFieldService.insertFormField(params);
        return flag ? R.ok() : R.error("表不存在");
    }

    @PutMapping("/updateTableField")
    public R updateTableField(@RequestBody Map<String, Object> params) throws Exception {
        //插入表字段方法
        Boolean flag = tableFieldService.updateFormField(params);
        return flag ? R.ok() : R.error("表不存在");
    }

    @PostMapping("/deleteTableField")
    public R deleteTableField(@RequestBody Map<String, Object> params) throws Exception {
        tableFieldService.deleteFormField(params);
        return R.ok();
    }
}
