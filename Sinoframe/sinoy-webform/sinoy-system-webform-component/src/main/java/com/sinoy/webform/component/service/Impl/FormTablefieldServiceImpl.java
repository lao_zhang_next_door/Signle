package com.sinoy.webform.component.service.Impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.webform.component.dao.FormTablefieldDao;
import com.sinoy.webform.component.entity.FormTableField;
import com.sinoy.webform.component.entity.FormTableInfo;
import com.sinoy.webform.component.service.FormTablefieldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:00
 **/
@Transactional
@Service("tableFieldService")
public class FormTablefieldServiceImpl extends BaseServiceImpl<FormTablefieldDao, FormTableField> implements FormTablefieldService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DynamicDataSourceProperties dynamicDataSourceProperties;

    @Override
    public List<FormTableField> selectFormTableFieldList(Map<String, Object> map) {
        return null;
    }

    public DataSourceProperty getDataSourceProperty() {
        Map<String, DataSourceProperty> propertyMap = dynamicDataSourceProperties.getDatasource();
        DataSourceProperty dataSourceProperty = null;
        if (propertyMap != null) {
            dataSourceProperty = propertyMap.get(dynamicDataSourceProperties.getPrimary());
        }

        if (dataSourceProperty == null) {
            logger.error("获取数据源异常");
            throw new BaseException("获取数据源异常");
        }
        return dataSourceProperty;
    }

    @Override
    public Boolean insertFormField(Map<String, Object> params) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        FormTableField tablefield = JSON.parseObject(JSON.toJSONString(params.get("field")),
                FormTableField.class);

        FormTableInfo tableInfo = new FormTableInfo();
        tableInfo.setPhysicalName(params.get("tableInfo").toString());
        String mustfill;
        if ("是".equals(tablefield.getMustFill())) {
            mustfill = "not null";
        } else {
            mustfill = "null";
        }
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        DatabaseMetaData mdata = conn.getMetaData();
        ResultSet rs = mdata.getTables(null, null, tableInfo.getPhysicalName(), null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            String SQL = "alter  table " + params.get("tableInfo").toString();
            SQL += " add " + tablefield.getFieldName();
            SQL += " " + tablefield.getFieldType();
            if ("varchar".equals(tablefield.getFieldType().toLowerCase())) {
                SQL += "(" + tablefield.getFieldLength() + ")";
            }
            if ("decimal".equals(tablefield.getFieldType().toLowerCase())) {
                SQL += " (" + (Integer.parseInt(tablefield.getFieldLength()) > 38 ? 38 : tablefield.getFieldLength()) + "," + tablefield.getDecimalLength() + ") ";
            }
            SQL += " " + mustfill + " comment '" + tablefield.getFieldDisplayName() + "'";
            stat.executeUpdate(SQL);

        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();
        //生成uuid作为rowguid
        tablefield.initNull();
        tablefield.setAllowTo(params.get("rowGuid").toString());
        //改为通用方法
        baseMapper.insert(tablefield);
        return true;
    }

    @Override
    public void insertFormField(FormTableField tablefield, String tableName) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        String mustfill;
        if ("是".equals(tablefield.getMustFill())) {
            mustfill = "not null";
        } else {
            mustfill = "null";
        }
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        DatabaseMetaData mdata = conn.getMetaData();
        ResultSet rs = mdata.getTables(null, null, tableName, null);
        // 判断表是否存在，如果表存在 则新增
        if (rs.next()) {
            String SQL = "alter  table " + tableName;
            SQL += " add " + tablefield.getFieldName();
            SQL += " " + tablefield.getFieldType();
            if ("varchar".equalsIgnoreCase(tablefield.getFieldType())) {
                SQL += "(" + tablefield.getFieldLength() + ")";
            }
            if ("decimal".equalsIgnoreCase(tablefield.getFieldType())) {
                SQL += " (" + (Integer.parseInt(tablefield.getFieldLength()) > 38 ? 38 : tablefield.getFieldLength()) + "," + tablefield.getDecimalLength() + ") ";
            }
            SQL += " " + mustfill + " comment '" + tablefield.getFieldDisplayName() + "'";
            stat.executeUpdate(SQL);

        } else {
            throw new BaseException("表不存在,请先建表");
        }
        // 释放资源
        stat.close();
        conn.close();
    }


    @Override
    public Boolean updateFormField(Map<String, Object> params) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        FormTableField formTableField = JSON.parseObject(JSON.toJSONString(params.get("field")), FormTableField.class);
        String tableName = params.get("tableName").toString();
        String originName = params.get("originalName").toString();
        this.updateByRowGuid(formTableField);

        String mustfill;
        if ("是".equals(formTableField.getMustFill())) {
            mustfill = "not null";
        } else {
            mustfill = "null";
        }
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            //创建行政区划表
            System.out.println("alter table " + tableName + "  change  " + originName + "  " + formTableField.getFieldName() + "  " + formTableField.getFieldType() + " (" + formTableField.getFieldLength() + ")   " + mustfill + " comment  '" + formTableField.getFieldDisplayName() + "';");
            stat.executeUpdate("alter table " + tableName + "  change  " + originName + "  " + formTableField.getFieldName() + "  " + formTableField.getFieldType() + " (" + formTableField.getFieldLength() + ")   " + mustfill + " comment  '" + formTableField.getFieldDisplayName() + "';");
        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();
        return true;
    }

    @Override
    public Boolean deleteFormField(Map<String, Object> params) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        String tableName = params.get("tableName").toString();
        List<String> fieldNames = (List<String>) params.get("fieldNames");

        List<String> rgList = (List) params.get("rowGuids");
        this.deleteBatchByGuids(rgList.toArray(new String[0]));

        FormTableInfo tableInfo = new FormTableInfo();
        tableInfo.setPhysicalName(params.get("tableName").toString());
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableInfo.getPhysicalName(), null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            //遍历字段名
            for (String fieldName : fieldNames) {
                stat.executeUpdate("alter table " + tableName + " drop " + fieldName + ";");
            }
        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();

        return true;
    }

    /**
     * 删除表
     *
     * @param tableName
     * @return
     */
    @Override
    public void delTable(String tableName) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则删除表
        if (rs.next()) {
            String sql = "drop table " + tableName;
            stat.execute(sql);
        } else {
        }
        // 释放资源
        stat.close();
        conn.close();
    }

    @Override
    public void addTable(String tableName, String tableComment) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            throw new BaseException("表已存在");
        } else {
            //创建行政区划表
            stat.execute("CREATE TABLE " + tableName + "("
                    + "row_id bigint(0) NOT NULL AUTO_INCREMENT comment '行标',"
                    + "row_guid varchar(50) NOT NULL comment '行标',"
                    + "create_time datetime NOT NULL comment '创建时间',"
                    + "update_time datetime NOT NULL comment '更新时间',"
                    + "del_flag bit(1) default 0 NOT NULL comment '删除标识',"
                    + "sort_sq bigInt(0) default 0 NOT NULL comment '排序号',"
                    + "PRIMARY KEY (row_id)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci comment='" + tableComment + "';"
            );
        }
        // 释放资源
        stat.close();
        conn.close();
    }

    @Override
    public void updateTable(String tableName, String newTableName) throws Exception {
        DataSourceProperty dataSourceProperty = getDataSourceProperty();
        String url = dataSourceProperty.getUrl();
        //连接数据库
        Class.forName(dataSourceProperty.getDriverClassName());
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, dataSourceProperty.getUsername(), dataSourceProperty.getPassword());
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);

        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (!rs.next()) {
            throw new BaseException("表不存在");
        } else {
            //创建行政区划表
            stat.executeUpdate("alter table " + tableName + " rename " + newTableName);
        }
        // 释放资源
        stat.close();
        conn.close();
    }


}
