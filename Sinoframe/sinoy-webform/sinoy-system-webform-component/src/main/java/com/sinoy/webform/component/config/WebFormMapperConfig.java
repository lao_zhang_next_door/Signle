package com.sinoy.webform.component.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.sinoy.webform.component.dao")
@Configuration
public class WebFormMapperConfig {
}
