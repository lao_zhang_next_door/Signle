package com.sinoy.webform.component.entity.vo.table;

import java.util.Date;

public class CodegenTableRespVO extends CodegenTableBaseVO {

    //编号
    private Long rowId;

    //主键编号
    private Integer dataSourceConfigId;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    public Long getRowId() {
        return rowId;
    }

    public CodegenTableRespVO setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public Integer getDataSourceConfigId() {
        return dataSourceConfigId;
    }

    public CodegenTableRespVO setDataSourceConfigId(Integer dataSourceConfigId) {
        this.dataSourceConfigId = dataSourceConfigId;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public CodegenTableRespVO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public CodegenTableRespVO setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public CodegenTableRespVO(Long rowId, Integer dataSourceConfigId, Date createTime, Date updateTime) {
        this.rowId = rowId;
        this.dataSourceConfigId = dataSourceConfigId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public CodegenTableRespVO() {
    }
}
