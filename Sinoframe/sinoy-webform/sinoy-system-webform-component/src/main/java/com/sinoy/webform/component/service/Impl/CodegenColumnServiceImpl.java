package com.sinoy.webform.component.service.Impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.webform.component.dao.CodegenColumnMapper;
import com.sinoy.webform.component.entity.CodegenColumnDO;
import com.sinoy.webform.component.service.CodegenColumnService;
import org.springframework.stereotype.Service;

/**
 * 代码生成 Service 实现类
 *
 * @author T470
 */
@Service
public class CodegenColumnServiceImpl extends BaseServiceImpl<CodegenColumnMapper, CodegenColumnDO> implements CodegenColumnService {

}
