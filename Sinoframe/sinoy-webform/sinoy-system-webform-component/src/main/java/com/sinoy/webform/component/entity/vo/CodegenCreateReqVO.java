package com.sinoy.webform.component.entity.vo;

import cn.hutool.core.util.ObjectUtil;
import com.sinoy.webform.component.entity.vo.column.CodegenColumnBaseVO;
import com.sinoy.webform.component.entity.vo.table.CodegenTableBaseVO;
import com.sinoy.webform.component.enums.CodegenSceneEnum;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CodegenCreateReqVO {

    @Valid // 校验内嵌的字段
    @NotNull(message = "表定义不能为空")
    private Table table;

    @Valid // 校验内嵌的字段
    @NotNull(message = "字段定义不能为空")
    private List<Column> columns;


    //更新表定义
    @Valid
    public static class Table extends CodegenTableBaseVO {

        //编号
        private Long rowId;

        @AssertTrue(message = "上级菜单不能为空")
        public boolean isParentMenuIdValid() {
            // 生成场景为管理后台时，必须设置上级菜单，不然生成的菜单 SQL 是无父级菜单的
            return ObjectUtil.notEqual(getScene(), CodegenSceneEnum.ADMIN.getScene())
                    || getParentMenuId() != null;
        }

        public Long getRowId() {
            return rowId;
        }

        public Table setRowId(Long rowId) {
            this.rowId = rowId;
            return this;
        }
    }

    //更新表定义
    public static class Column extends CodegenColumnBaseVO {

        //编号
        private Long rowId;

        public Long getRowId() {
            return rowId;
        }

        public Column setRowId(Long rowId) {
            this.rowId = rowId;
            return this;
        }
    }

    public Table getTable() {
        return table;
    }

    public CodegenCreateReqVO setTable(Table table) {
        this.table = table;
        return this;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public CodegenCreateReqVO setColumns(List<Column> columns) {
        this.columns = columns;
        return this;
    }

}
