package com.sinoy.webform.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.webform.component.entity.FormTableInfo;

import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:05
 **/
public interface FormTableInfoDao extends ExtendMapper<FormTableInfo> {

	/**
	 * 根据表名查询表信息
	 * @param tableName
	 * @return
	 */
	Map<String, String> queryTable(String tableName);

	/**
	 * 根据表名查询列信息
	 * @param tableName
	 * @return
	 */
	List<Map<String, String>> queryColumns(String tableName);

    
}
