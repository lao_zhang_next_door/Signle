package com.sinoy.webform.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.webform.component.entity.CodegenColumnDO;
import com.sinoy.webform.component.entity.CodegenTableDO;
import com.sinoy.webform.component.entity.vo.CodegenCreateListReqVO;
import com.sinoy.webform.component.entity.vo.CodegenUpdateReqVO;
import com.sinoy.webform.component.entity.vo.table.DatabaseTableRespVO;

import java.util.List;
import java.util.Map;

/**
 * 代码生成 Service 接口
 *
 */
public interface CodegenColumnService extends BaseService<CodegenColumnDO> {

}
