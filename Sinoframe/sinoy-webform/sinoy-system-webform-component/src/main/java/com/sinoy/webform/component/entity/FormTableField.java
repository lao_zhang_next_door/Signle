package com.sinoy.webform.component.entity;


import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-21-10:04
 **/
public class FormTableField extends BaseEntity {
	
    public FormTableField() {
		super();
	}

	public FormTableField(boolean init) {
		super(init);
	}

	//字段名称
    private String fieldName;
    //字段类型
    private String fieldType;
    //字段长度
    private String fieldLength;
    //字段精度
    private String decimalLength;
    //字段显示名称
    private String fieldDisplayName;
    //是否作为查询条件
    private String isQueryCondition;
    //是否必填
    private String mustFill;
    //字段展现方式
    private String fieldDisplayType;
    //是否显示在新增页面
    private String showInadd;
    //对应表的行标
    private String allowTo;
    //对应代码项行标
    private String codesGuid;


    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(String fieldLength) {
        this.fieldLength = fieldLength;
    }

    public String getDecimalLength() {
        return decimalLength;
    }

    public void setDecimalLength(String decimalLength) {
        this.decimalLength = decimalLength;
    }

    public String getFieldDisplayName() {
        return fieldDisplayName;
    }

    public void setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
    }

    public String getFieldDisplayType() {
        return fieldDisplayType;
    }

    public void setFieldDisplayType(String fieldDisplayType) {
        this.fieldDisplayType = fieldDisplayType;
    }

    public String getAllowTo() {
        return allowTo;
    }

    public void setAllowTo(String allowTo) {
        this.allowTo = allowTo;
    }

    public String getCodesGuid() {
        return codesGuid;
    }

    public void setCodesGuid(String codesGuid) {
        this.codesGuid = codesGuid;
    }

    public String getIsQueryCondition() {
        return isQueryCondition;
    }

    public void setIsQueryCondition(String isQueryCondition) {
        this.isQueryCondition = isQueryCondition;
    }

    public String getMustFill() {
        return mustFill;
    }

    public void setMustFill(String mustFill) {
        this.mustFill = mustFill;
    }

    public String getShowInadd() {
        return showInadd;
    }

    public void setShowInadd(String showInadd) {
        this.showInadd = showInadd;
    }
}
