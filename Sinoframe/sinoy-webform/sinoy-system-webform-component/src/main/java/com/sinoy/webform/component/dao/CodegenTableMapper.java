package com.sinoy.webform.component.dao;

import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.webform.component.entity.CodegenTableDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CodegenTableMapper extends ExtendMapper<CodegenTableDO> {

    default CodegenTableDO selectByTableNameAndDataSourceConfigId(String tableName, Long dataSourceConfigId) {
        return selectOne(CodegenTableDO::getTableName, tableName,
                CodegenTableDO::getDataSourceConfigId, dataSourceConfigId);
    }

//    default PageResult<CodegenTableDO> selectPage(CodegenTablePageReqVO pageReqVO) {
//        return selectPage(pageReqVO, new LambdaQueryWrapperX<CodegenTableDO>()
//                .likeIfPresent(CodegenTableDO::getTableName, pageReqVO.getTableName())
//                .likeIfPresent(CodegenTableDO::getTableComment, pageReqVO.getTableComment())
//                .betweenIfPresent(CodegenTableDO::getCreateTime, pageReqVO.getCreateTime()));
//    }

    default List<CodegenTableDO> selectListByDataSourceConfigId(Long dataSourceConfigId) {
        return selectList(CodegenTableDO::getDataSourceConfigId, dataSourceConfigId);
    }

}
