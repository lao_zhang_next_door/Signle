package com.sinoy.webform.component.dao;


import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.webform.component.entity.FormTableField;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:05
 **/
public interface FormTablefieldDao extends ExtendMapper<FormTableField> {


}
