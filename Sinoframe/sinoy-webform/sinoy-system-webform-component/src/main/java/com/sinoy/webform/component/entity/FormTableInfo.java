package com.sinoy.webform.component.entity;


import com.sinoy.core.database.mybatisplus.entity.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:05
 **/
public class FormTableInfo extends BaseEntity {

    public FormTableInfo(String rowGuid) {
        super(rowGuid);
    }

    public FormTableInfo() {}

    //数据表表单名称
    private String tableName;
    //数据表物理名称
    private String physicalName;
    //项目名称
    private String projectName;
    //控制器名称
    private String controllerName;
    //表单样式
    private String formStyle;
    //文件输出路径
    private String outPutDir;

    public String getOutPutDir() {
        return outPutDir;
    }

    public void setOutPutDir(String outPutDir) {
        this.outPutDir = outPutDir;
    }

    public String getFormStyle() { return formStyle; }

    public void setFormStyle(String formStyle) { this.formStyle = formStyle; }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }


}
