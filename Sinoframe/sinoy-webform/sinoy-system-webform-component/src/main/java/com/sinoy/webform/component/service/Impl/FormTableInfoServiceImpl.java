package com.sinoy.webform.component.service.Impl;

import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.webform.component.dao.FormTableInfoDao;
import com.sinoy.webform.component.entity.FormTableInfo;
import com.sinoy.webform.component.service.FormTableInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:20
 **/

@Service
public class FormTableInfoServiceImpl extends BaseServiceImpl<FormTableInfoDao, FormTableInfo> implements FormTableInfoService {
	
	public Map<String, String> queryTable(String tableName){
		return baseMapper.queryTable(tableName);
	}
	
	public List<Map<String, String>> queryColumns(String tableName){
		return baseMapper.queryColumns(tableName);
	}

}
