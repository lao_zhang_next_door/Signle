package com.sinoy.webform.component.entity.vo.column;


import javax.validation.constraints.NotNull;

/**
* 代码生成字段定义 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
public class CodegenColumnBaseVO {

    //表编号
    @NotNull(message = "表编号不能为空")
    private Long tableId;

    //字段名
    @NotNull(message = "字段名不能为空")
    private String columnName;

    //字段类型
    @NotNull(message = "字段类型不能为空")
    private String dataType;

    //字段描述
    @NotNull(message = "字段描述不能为空")
    private String columnComment;

    //是否允许为空
    @NotNull(message = "是否允许为空不能为空")
    private Boolean nullable;

    //是否主键
    @NotNull(message = "是否主键不能为空")
    private Boolean primaryKey;

    //是否自增
    @NotNull(message = "是否自增不能为空")
    private String autoIncrement;

    //排序
    @NotNull(message = "排序不能为空")
    private Integer ordinalPosition;

    //Java 属性类型
    @NotNull(message = "Java 属性类型不能为空")
    private String javaType;

    //Java 属性名
    @NotNull(message = "Java 属性名不能为空")
    private String javaField;

    //字典类型
    private String dictType;

    //数据示例
    private String example;

    //是否为 Create 创建操作的字段
    @NotNull(message = "是否为 Create 创建操作的字段不能为空")
    private Boolean createOperation;

    //是否为 Update 更新操作的字段
    @NotNull(message = "是否为 Update 更新操作的字段不能为空")
    private Boolean updateOperation;

    //是否为 List 查询操作的字段
    @NotNull(message = "是否为 List 查询操作的字段不能为空")
    private Boolean listOperation;

    //List 查询操作的条件类型
    @NotNull(message = "List 查询操作的条件类型不能为空")
    private String listOperationCondition;

    //是否为 List 查询操作的返回字段
    @NotNull(message = "是否为 List 查询操作的返回字段不能为空")
    private Boolean listOperationResult;

    //显示类型
    @NotNull(message = "显示类型不能为空")
    private String htmlType;

    public Long getTableId() {
        return tableId;
    }

    public CodegenColumnBaseVO setTableId(Long tableId) {
        this.tableId = tableId;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public CodegenColumnBaseVO setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public CodegenColumnBaseVO setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public CodegenColumnBaseVO setColumnComment(String columnComment) {
        this.columnComment = columnComment;
        return this;
    }

    public Boolean getNullable() {
        return nullable;
    }

    public CodegenColumnBaseVO setNullable(Boolean nullable) {
        this.nullable = nullable;
        return this;
    }

    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public CodegenColumnBaseVO setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }

    public String getAutoIncrement() {
        return autoIncrement;
    }

    public CodegenColumnBaseVO setAutoIncrement(String autoIncrement) {
        this.autoIncrement = autoIncrement;
        return this;
    }

    public Integer getOrdinalPosition() {
        return ordinalPosition;
    }

    public CodegenColumnBaseVO setOrdinalPosition(Integer ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
        return this;
    }

    public String getJavaType() {
        return javaType;
    }

    public CodegenColumnBaseVO setJavaType(String javaType) {
        this.javaType = javaType;
        return this;
    }

    public String getJavaField() {
        return javaField;
    }

    public CodegenColumnBaseVO setJavaField(String javaField) {
        this.javaField = javaField;
        return this;
    }

    public String getDictType() {
        return dictType;
    }

    public CodegenColumnBaseVO setDictType(String dictType) {
        this.dictType = dictType;
        return this;
    }

    public String getExample() {
        return example;
    }

    public CodegenColumnBaseVO setExample(String example) {
        this.example = example;
        return this;
    }

    public Boolean getCreateOperation() {
        return createOperation;
    }

    public CodegenColumnBaseVO setCreateOperation(Boolean createOperation) {
        this.createOperation = createOperation;
        return this;
    }

    public Boolean getUpdateOperation() {
        return updateOperation;
    }

    public CodegenColumnBaseVO setUpdateOperation(Boolean updateOperation) {
        this.updateOperation = updateOperation;
        return this;
    }

    public Boolean getListOperation() {
        return listOperation;
    }

    public CodegenColumnBaseVO setListOperation(Boolean listOperation) {
        this.listOperation = listOperation;
        return this;
    }

    public String getListOperationCondition() {
        return listOperationCondition;
    }

    public CodegenColumnBaseVO setListOperationCondition(String listOperationCondition) {
        this.listOperationCondition = listOperationCondition;
        return this;
    }

    public Boolean getListOperationResult() {
        return listOperationResult;
    }

    public CodegenColumnBaseVO setListOperationResult(Boolean listOperationResult) {
        this.listOperationResult = listOperationResult;
        return this;
    }

    public String getHtmlType() {
        return htmlType;
    }

    public CodegenColumnBaseVO setHtmlType(String htmlType) {
        this.htmlType = htmlType;
        return this;
    }
}
