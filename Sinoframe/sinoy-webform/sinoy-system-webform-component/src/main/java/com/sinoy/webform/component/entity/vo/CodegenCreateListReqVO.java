package com.sinoy.webform.component.entity.vo;

import javax.validation.constraints.NotNull;
import java.util.List;


public class CodegenCreateListReqVO {

    //数据源配置的编号
    @NotNull(message = "数据源配置的编号不能为空")
    private Long dataSourceConfigId;

    //表名数组
    @NotNull(message = "表名数组不能为空")
    private List<String> tableNames;

    public Long getDataSourceConfigId() {
        return dataSourceConfigId;
    }

    public CodegenCreateListReqVO setDataSourceConfigId(Long dataSourceConfigId) {
        this.dataSourceConfigId = dataSourceConfigId;
        return this;
    }

    public List<String> getTableNames() {
        return tableNames;
    }

    public CodegenCreateListReqVO setTableNames(List<String> tableNames) {
        this.tableNames = tableNames;
        return this;
    }
}
