package com.sinoy.webform.component.entity;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.webform.component.enums.CodegenSceneEnum;
import com.sinoy.webform.component.enums.CodegenTemplateTypeEnum;

/**
 * 代码生成 table 表定义
 *
 */
@TableName(value = "form_codegen_table", autoResultMap = true)
@KeySequence("infra_codegen_table_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class CodegenTableDO extends BaseEntity {

    /**
     * 数据源编号
     *
     */
    private Long dataSourceConfigId;
    /**
     * 生成场景
     *
     * 枚举 {@link CodegenSceneEnum}
     */
    private Integer scene;

    // ========== 表相关字段 ==========

    /**
     * 表名称
     */
    private String tableName;
    /**
     * 表描述
     */
    private String tableComment;
    /**
     * 备注
     */
    private String remark;

    // ========== 类相关字段 ==========

    /**
     * 模块名，即一级目录
     *
     * 例如说，system、infra、tool 等等
     */
    private String moduleName;
    /**
     * 业务名，即二级目录
     *
     * 例如说，user、permission、dict 等等
     */
    private String businessName;
    /**
     * 类名称（首字母大写）
     *
     * 例如说，SysUser、SysMenu、SysDictData 等等
     */
    private String className;
    /**
     * 类描述
     */
    private String classComment;
    /**
     * 作者
     */
    private String author;

    // ========== 生成相关字段 ==========

    /**
     * 模板类型
     *
     * 枚举 {@link CodegenTemplateTypeEnum}
     */
    private Integer templateType;

    // ========== 菜单相关字段 ==========

    /**
     * 父菜单编号
     *
     * 关联 MenuDO 的 id 属性
     */
    private Long parentMenuId;

    /**
     * 父菜单guid
     */
    private String parentMenuGuid;

    public String getParentMenuGuid() {
        return parentMenuGuid;
    }

    public void setParentMenuGuid(String parentMenuGuid) {
        this.parentMenuGuid = parentMenuGuid;
    }

    public Long getDataSourceConfigId() {
        return dataSourceConfigId;
    }

    public CodegenTableDO setDataSourceConfigId(Long dataSourceConfigId) {
        this.dataSourceConfigId = dataSourceConfigId;
        return this;
    }

    public Integer getScene() {
        return scene;
    }

    public CodegenTableDO setScene(Integer scene) {
        this.scene = scene;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public CodegenTableDO setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getTableComment() {
        return tableComment;
    }

    public CodegenTableDO setTableComment(String tableComment) {
        this.tableComment = tableComment;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public CodegenTableDO setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public String getModuleName() {
        return moduleName;
    }

    public CodegenTableDO setModuleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public String getBusinessName() {
        return businessName;
    }

    public CodegenTableDO setBusinessName(String businessName) {
        this.businessName = businessName;
        return this;
    }

    public String getClassName() {
        return className;
    }

    public CodegenTableDO setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getClassComment() {
        return classComment;
    }

    public CodegenTableDO setClassComment(String classComment) {
        this.classComment = classComment;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public CodegenTableDO setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Integer getTemplateType() {
        return templateType;
    }

    public CodegenTableDO setTemplateType(Integer templateType) {
        this.templateType = templateType;
        return this;
    }

    public Long getParentMenuId() {
        return parentMenuId;
    }

    public CodegenTableDO setParentMenuId(Long parentMenuId) {
        this.parentMenuId = parentMenuId;
        return this;
    }

    public CodegenTableDO(Long dataSourceConfigId, Integer scene, String tableName, String tableComment, String remark, String moduleName, String businessName, String className, String classComment, String author, Integer templateType, Long parentMenuId) {
        this.dataSourceConfigId = dataSourceConfigId;
        this.scene = scene;
        this.tableName = tableName;
        this.tableComment = tableComment;
        this.remark = remark;
        this.moduleName = moduleName;
        this.businessName = businessName;
        this.className = className;
        this.classComment = classComment;
        this.author = author;
        this.templateType = templateType;
        this.parentMenuId = parentMenuId;
    }

    public CodegenTableDO() {
    }
}
