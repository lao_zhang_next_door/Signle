package com.sinoy.webform.component.entity.vo;

public class CodegenPreviewRespVO {

    //文件路径
    private String filePath;

    //代码
    private String code;

    public String getFilePath() {
        return filePath;
    }

    public CodegenPreviewRespVO setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public String getCode() {
        return code;
    }

    public CodegenPreviewRespVO setCode(String code) {
        this.code = code;
        return this;
    }
}
