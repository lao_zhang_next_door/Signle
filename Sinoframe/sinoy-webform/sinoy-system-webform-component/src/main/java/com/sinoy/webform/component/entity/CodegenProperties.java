package com.sinoy.webform.component.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Component
@ConfigurationProperties(prefix = "xnw.codegen")
@Validated
public class CodegenProperties {

    /**
     * 生成的 Java 代码的基础包
     */
    @NotNull(message = "Java 代码的基础包不能为空")
    private String basePackage;

    /**
     * 数据库名数组
     */
    @NotEmpty(message = "数据库不能为空")
    private Collection<String> dbSchemas;

    public String getBasePackage() {
        return basePackage;
    }

    public CodegenProperties setBasePackage(String basePackage) {
        this.basePackage = basePackage;
        return this;
    }

    public Collection<String> getDbSchemas() {
        return dbSchemas;
    }

    public CodegenProperties setDbSchemas(Collection<String> dbSchemas) {
        this.dbSchemas = dbSchemas;
        return this;
    }
}
