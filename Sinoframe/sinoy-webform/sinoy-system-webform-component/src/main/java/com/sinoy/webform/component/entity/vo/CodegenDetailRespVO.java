package com.sinoy.webform.component.entity.vo;

import com.sinoy.webform.component.entity.vo.column.CodegenColumnRespVO;
import com.sinoy.webform.component.entity.vo.table.CodegenTableRespVO;

import java.util.List;

public class CodegenDetailRespVO {

    //表定义
    private CodegenTableRespVO table;

    //字段定义
    private List<CodegenColumnRespVO> columns;

    public CodegenTableRespVO getTable() {
        return table;
    }

    public CodegenDetailRespVO setTable(CodegenTableRespVO table) {
        this.table = table;
        return this;
    }

    public List<CodegenColumnRespVO> getColumns() {
        return columns;
    }

    public CodegenDetailRespVO setColumns(List<CodegenColumnRespVO> columns) {
        this.columns = columns;
        return this;
    }
}
