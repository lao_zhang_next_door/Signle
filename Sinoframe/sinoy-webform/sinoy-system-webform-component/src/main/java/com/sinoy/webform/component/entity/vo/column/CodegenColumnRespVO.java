package com.sinoy.webform.component.entity.vo.column;

import java.util.Date;

public class CodegenColumnRespVO extends CodegenColumnBaseVO {

    //编号
    private Long rowId;

    //创建时间
    private Date createTime;

    public Long getRowId() {
        return rowId;
    }

    public CodegenColumnRespVO setRowId(Long rowId) {
        this.rowId = rowId;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public CodegenColumnRespVO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public CodegenColumnRespVO(Long rowId, Date createTime) {
        this.rowId = rowId;
        this.createTime = createTime;
    }

    public CodegenColumnRespVO() {
    }
}
