package com.sinoy.webform.component.entity.vo.table;


import javax.validation.constraints.NotNull;

/**
* 代码生成 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
public class CodegenTableBaseVO {

    //生成场景 参见 CodegenSceneEnum 枚举
    @NotNull(message = "导入类型不能为空")
    private Integer scene;

    //表名称
    @NotNull(message = "表名称不能为空")
    private String tableName;

    //表描述
    @NotNull(message = "表描述不能为空")
    private String tableComment;

    //备注
    private String remark;

    //模块名
    @NotNull(message = "模块名不能为空")
    private String moduleName;

    //业务名
    @NotNull(message = "业务名不能为空")
    private String businessName;

    //类名称
    @NotNull(message = "类名称不能为空")
    private String className;

    //类描述
    @NotNull(message = "类描述不能为空")
    private String classComment;

    //作者
    @NotNull(message = "作者不能为空")
    private String author;

    //模板类型 参见 CodegenTemplateTypeEnum 枚举
    @NotNull(message = "模板类型不能为空")
    private Integer templateType;

    //父菜单编号
    private Long parentMenuId;

    //父菜单guid
    private String parentMenuGuid;

    public String getParentMenuGuid() {
        return parentMenuGuid;
    }

    public void setParentMenuGuid(String parentMenuGuid) {
        this.parentMenuGuid = parentMenuGuid;
    }

    public Integer getScene() {
        return scene;
    }

    public CodegenTableBaseVO setScene(Integer scene) {
        this.scene = scene;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public CodegenTableBaseVO setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getTableComment() {
        return tableComment;
    }

    public CodegenTableBaseVO setTableComment(String tableComment) {
        this.tableComment = tableComment;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public CodegenTableBaseVO setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public String getModuleName() {
        return moduleName;
    }

    public CodegenTableBaseVO setModuleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public String getBusinessName() {
        return businessName;
    }

    public CodegenTableBaseVO setBusinessName(String businessName) {
        this.businessName = businessName;
        return this;
    }

    public String getClassName() {
        return className;
    }

    public CodegenTableBaseVO setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getClassComment() {
        return classComment;
    }

    public CodegenTableBaseVO setClassComment(String classComment) {
        this.classComment = classComment;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public CodegenTableBaseVO setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Integer getTemplateType() {
        return templateType;
    }

    public CodegenTableBaseVO setTemplateType(Integer templateType) {
        this.templateType = templateType;
        return this;
    }

    public Long getParentMenuId() {
        return parentMenuId;
    }

    public CodegenTableBaseVO setParentMenuId(Long parentMenuId) {
        this.parentMenuId = parentMenuId;
        return this;
    }
}
