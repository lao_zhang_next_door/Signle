package com.sinoy.webform.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.webform.component.entity.FormTableInfo;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:16
 **/
public interface FormTableInfoService extends BaseService<FormTableInfo> {

	

}
