package com.sinoy.webform.component.entity;

import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.webform.component.enums.CodegenColumnHtmlTypeEnum;
import com.sinoy.webform.component.enums.CodegenColumnListConditionEnum;

/**
 * 代码生成 column 字段定义
 */
@TableName(value = "form_codegen_column", autoResultMap = true)
@KeySequence("infra_codegen_column_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
public class CodegenColumnDO extends BaseEntity {

    /**
     * 表编号
     *
     */
    private Long tableId;

    // ========== 表相关字段 ==========

    /**
     * 字段名
     */
    private String columnName;
    /**
     * 字段类型
     */
    private String dataType;
    /**
     * 字段描述
     */
    private String columnComment;
    /**
     * 是否允许为空
     */
    private Boolean nullable;
    /**
     * 是否主键
     */
    private Boolean primaryKey;
    /**
     * 是否自增
     */
    private Boolean autoIncrement;
    /**
     * 排序
     */
    private Integer ordinalPosition;

    // ========== Java 相关字段 ==========

    /**
     * Java 属性类型
     *
     * 例如说 String、Boolean 等等
     */
    private String javaType;
    /**
     * Java 属性名
     */
    private String javaField;
    /**
     * 字典类型
     *
     * 关联 DictTypeDO 的 type 属性
     */
    private String dictType;
    /**
     * 数据示例，主要用于生成 Swagger 注解的 example 字段
     */
    private String example;

    // ========== CRUD 相关字段 ==========

    /**
     * 是否为 Create 创建操作的字段
     */
    private Boolean createOperation;
    /**
     * 是否为 Update 更新操作的字段
     */
    private Boolean updateOperation;
    /**
     * 是否为 List 查询操作的字段
     */
    private Boolean listOperation;
    /**
     * List 查询操作的条件类型
     *
     * 枚举 {@link CodegenColumnListConditionEnum}
     */
    private String listOperationCondition;
    /**
     * 是否为 List 查询操作的返回字段
     */
    private Boolean listOperationResult;

    // ========== UI 相关字段 ==========

    /**
     * 显示类型
     *
     * 枚举 {@link CodegenColumnHtmlTypeEnum}
     */
    private String htmlType;

    public Long getTableId() {
        return tableId;
    }

    public CodegenColumnDO setTableId(Long tableId) {
        this.tableId = tableId;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public CodegenColumnDO setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public CodegenColumnDO setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public CodegenColumnDO setColumnComment(String columnComment) {
        this.columnComment = columnComment;
        return this;
    }

    public Boolean getNullable() {
        return nullable;
    }

    public CodegenColumnDO setNullable(Boolean nullable) {
        this.nullable = nullable;
        return this;
    }

    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public CodegenColumnDO setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }

    public Boolean getAutoIncrement() {
        return autoIncrement;
    }

    public CodegenColumnDO setAutoIncrement(Boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
        return this;
    }

    public Integer getOrdinalPosition() {
        return ordinalPosition;
    }

    public CodegenColumnDO setOrdinalPosition(Integer ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
        return this;
    }

    public String getJavaType() {
        return javaType;
    }

    public CodegenColumnDO setJavaType(String javaType) {
        this.javaType = javaType;
        return this;
    }

    public String getJavaField() {
        return javaField;
    }

    public CodegenColumnDO setJavaField(String javaField) {
        this.javaField = javaField;
        return this;
    }

    public String getDictType() {
        return dictType;
    }

    public CodegenColumnDO setDictType(String dictType) {
        this.dictType = dictType;
        return this;
    }

    public String getExample() {
        return example;
    }

    public CodegenColumnDO setExample(String example) {
        this.example = example;
        return this;
    }

    public Boolean getCreateOperation() {
        return createOperation;
    }

    public CodegenColumnDO setCreateOperation(Boolean createOperation) {
        this.createOperation = createOperation;
        return this;
    }

    public Boolean getUpdateOperation() {
        return updateOperation;
    }

    public CodegenColumnDO setUpdateOperation(Boolean updateOperation) {
        this.updateOperation = updateOperation;
        return this;
    }

    public Boolean getListOperation() {
        return listOperation;
    }

    public CodegenColumnDO setListOperation(Boolean listOperation) {
        this.listOperation = listOperation;
        return this;
    }

    public String getListOperationCondition() {
        return listOperationCondition;
    }

    public CodegenColumnDO setListOperationCondition(String listOperationCondition) {
        this.listOperationCondition = listOperationCondition;
        return this;
    }

    public Boolean getListOperationResult() {
        return listOperationResult;
    }

    public CodegenColumnDO setListOperationResult(Boolean listOperationResult) {
        this.listOperationResult = listOperationResult;
        return this;
    }

    public String getHtmlType() {
        return htmlType;
    }

    public CodegenColumnDO setHtmlType(String htmlType) {
        this.htmlType = htmlType;
        return this;
    }

    public CodegenColumnDO(Long tableId, String columnName, String dataType, String columnComment, Boolean nullable, Boolean primaryKey, Boolean autoIncrement, Integer ordinalPosition, String javaType, String javaField, String dictType, String example, Boolean createOperation, Boolean updateOperation, Boolean listOperation, String listOperationCondition, Boolean listOperationResult, String htmlType) {
        this.tableId = tableId;
        this.columnName = columnName;
        this.dataType = dataType;
        this.columnComment = columnComment;
        this.nullable = nullable;
        this.primaryKey = primaryKey;
        this.autoIncrement = autoIncrement;
        this.ordinalPosition = ordinalPosition;
        this.javaType = javaType;
        this.javaField = javaField;
        this.dictType = dictType;
        this.example = example;
        this.createOperation = createOperation;
        this.updateOperation = updateOperation;
        this.listOperation = listOperation;
        this.listOperationCondition = listOperationCondition;
        this.listOperationResult = listOperationResult;
        this.htmlType = htmlType;
    }

    public CodegenColumnDO() {
    }
}
