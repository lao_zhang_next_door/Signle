package com.sinoy.webform.component.entity.vo;

public class FormTableVo {

    /**
     * 包名
     */
    private String packageName;

    /**
     * 输出文件位置
     */
    private String outPutDir;

    /**
     * 数据表名
     */
    private String tableName;

    public String getPackageName() {
        return packageName;
    }

    public FormTableVo setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getOutPutDir() {
        return outPutDir;
    }

    public FormTableVo setOutPutDir(String outPutDir) {
        this.outPutDir = outPutDir;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public FormTableVo setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }
}
