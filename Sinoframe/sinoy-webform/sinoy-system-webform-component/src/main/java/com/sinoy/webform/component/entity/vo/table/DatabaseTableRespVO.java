package com.sinoy.webform.component.entity.vo.table;

public class DatabaseTableRespVO {

    //表名称
    private String name;

    //表描述
    private String comment;

    public String getName() {
        return name;
    }

    public DatabaseTableRespVO setName(String name) {
        this.name = name;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public DatabaseTableRespVO setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public DatabaseTableRespVO(String name, String comment) {
        this.name = name;
        this.comment = comment;
    }

    public DatabaseTableRespVO() {
    }
}
