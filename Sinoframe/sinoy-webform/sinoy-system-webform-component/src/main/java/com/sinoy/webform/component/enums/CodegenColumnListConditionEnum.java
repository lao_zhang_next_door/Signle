package com.sinoy.webform.component.enums;

/**
 * 代码生成器的字段过滤条件枚举
 */
public enum CodegenColumnListConditionEnum {

    EQ("="),
    NE("!="),
    GT(">"),
    GTE(">="),
    LT("<"),
    LTE("<="),
    LIKE("LIKE"),
    BETWEEN("BETWEEN");

    /**
     * 条件
     */
    private final String condition;

    CodegenColumnListConditionEnum(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }
}
