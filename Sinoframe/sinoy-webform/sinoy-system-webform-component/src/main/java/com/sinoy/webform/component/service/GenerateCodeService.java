package com.sinoy.webform.component.service;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.baomidou.mybatisplus.annotation.DbType;
import com.sinoy.core.database.mybatisplus.gen.CodeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * 代码生成服务
 */
@Service
public class GenerateCodeService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DynamicDataSourceProperties dynamicDataSourceProperties;

    /**
     * 生成代码
     * @param outPutDir  输出文件位置 全路径
     * @param packageName 包名
     * @param tableName 表名
     */
    public void genCode(String outPutDir,String packageName,String tableName){
        Map<String, DataSourceProperty> propertyMap = dynamicDataSourceProperties.getDatasource();
        DataSourceProperty dataSourceProperty = null;
        if(propertyMap != null){
            dataSourceProperty = propertyMap.get(dynamicDataSourceProperties.getPrimary());
        }
        if(dataSourceProperty == null){
            logger.error("获取数据源异常");
            return;
        }

        CodeConfig config = new CodeConfig();
        config.setDbType(DbType.MYSQL)
                .setDriverName(dataSourceProperty.getDriverClassName())
                .setUrl(dataSourceProperty.getUrl())
                .setUsername(dataSourceProperty.getUsername())
                .setPassword(dataSourceProperty.getPassword())
                .setOutputDir(outPutDir)
                .setPackageName(packageName)
                .setTableName(tableName)
                .setPrefix(null);

//        GeneratorUtil.execute(config);
    }


}
