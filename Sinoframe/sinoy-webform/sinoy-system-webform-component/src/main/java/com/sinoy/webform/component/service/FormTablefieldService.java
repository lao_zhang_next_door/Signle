package com.sinoy.webform.component.service;

import com.sinoy.core.database.mybatisplus.base.service.BaseService;
import com.sinoy.webform.component.entity.FormTableField;

import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-21-10:59
 **/
public interface FormTablefieldService extends BaseService<FormTableField> {

    /**
     *
     * @param map
     * @return
     */
    List<FormTableField> selectFormTableFieldList(Map<String, Object> map);

    Boolean insertFormField(Map<String, Object> params) throws Exception;

    /**
     * 修改表单
     * @param params
     * @return
     */
    Boolean updateFormField(Map<String, Object> params) throws Exception;

    /**
     * 删除表单
     * @param params
     * @return
     */
    Boolean deleteFormField(Map<String, Object> params) throws Exception;

    /**
     * 删除数据表
     * @param tableName
     */
    void delTable(String tableName) throws Exception;

    /**
     * 新增数据表
     * @param tableName
     * @throws Exception
     */
    void addTable(String tableName,String tableComment) throws Exception;

    /**
     * 更新数据表
     * @param tableName
     * @throws Exception
     */
    void updateTable(String tableName,String newTableName) throws Exception;

//    以上部门为老代码 且用且珍惜    新框架使用下面方法
//    =======================================================================代码分割==================================================================

    /**
     * 新增 字段
     * @param tablefield 字段
     * @param tableName  表名
     * @throws Exception
     */
    void insertFormField(FormTableField tablefield,String tableName) throws Exception;
}
