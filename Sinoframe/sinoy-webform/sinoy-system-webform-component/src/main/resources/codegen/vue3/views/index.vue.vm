<script setup lang="ts">
  import {ContentWrap} from '@/components/ContentWrap'
  import {Search} from '@/components/Search'
  import {useI18n} from '@/hooks/web/useI18n'
  import {Table} from '@/components/Table'
  import {ref, reactive, unref, onMounted} from 'vue'
  import {ElButton, ElRow, ElCol, ElMessage, ElTag} from 'element-plus'
  import {
    get${simpleClassName}Page,
    get${simpleClassName}Detail,
    create${simpleClassName},
    update${simpleClassName},
    delete${simpleClassName},
    export${simpleClassName}
  } from './api/${simpleClassName}Api'
  import {useTable} from '@/hooks/web/useTable'
  import {Descriptions} from '@/components/Descriptions'
  import {CrudSchema, useCrudSchemas} from '@/hooks/web/useCrudSchemas'
  import {Dialog} from '@/components/Dialog'
  import ${simpleClassName}Form from './components/${simpleClassName}Form.vue'
  import {useWindowSize} from '@vueuse/core'
  import {useValidator} from '@/hooks/web/useValidator'
  import {useDictStoreWithOut} from '@/store/modules/dict'
  import {FrameSearch} from '@/types/frame-system/frame-search'
  import {allSchemas} from './${classNameVar}Data.ts'

  defineOptions({
    name: '${simpleClassName}'
  })
  const dictStore = useDictStoreWithOut()
  const {required} = useValidator()
  const {t} = useI18n()
  //这里是schemas数据源
  const crudSchemas = reactive<CrudSchema[]>(allSchemas)

  // ========== 列表相关 ==========
  const {register, tableObject, methods} = useTable<${simpleClassName}VO>({
    getListApi: get${simpleClassName}Page,
    delListApi: delete${simpleClassName},
    exportListApi: export${simpleClassName},
    response: {
      list: 'list',
      total: 'total'
    }
  })

  const loading = ref(false)
  const delLoading = ref(false)
  const dialogTitle = ref('')
  const dialogVisible = ref(false)
  const actionType = ref('')
  // const tableDataList = ref<FrameUser[]>([])
  const {allSchemas} = useCrudSchemas(crudSchemas)
  const {getList, setSearchParams} = methods
  const {height} = useWindowSize()
  const actionTabRef: any = ref(null)

  onMounted(() => {
    tableObject.maxHeight = height.value - 350
    setSearchParams(tableSearchaData)
  })

  let tableSearchaData: FrameSearch = reactive({
    prop: ['sort_sq'],
    order: ['desc'],
    params: {}
  })

  const writeRef = ref<ComponentRef<typeof ${simpleClassName}Form>>()

  const acitonFn = (data: ${simpleClassName}VO) => {
    actionType.value = 'update'
    dialogTitle.value = t('formDemo.change')
    tableObject.currentRow = data
    dialogVisible.value = true
  }

  const addNew = () => {
    actionType.value = 'create'
    dialogTitle.value = t('exampleDemo.add')
    tableObject.currentRow = null
    dialogVisible.value = true
  }

  const addData = async () => {
    const write = unref(writeRef)
    await write?.elFormRef?.validate(async (isValid) => {
      if (isValid) {
        loading.value = true
        const data = (await write?.getFormData()) as ${simpleClassName}VO
        const res = await create${simpleClassName}(data)
            .catch(() => {
            })
            .finally(() => {
              loading.value = false
            })
        if (res) {
          ElMessage({
            message: '新增成功',
            type: 'success'
          })
          dialogVisible.value = false
          tableObject.currentPage = 1
          setSearchParams(tableSearchaData)
        }
      }
    })
  }

  const updateData = async () => {
    const write = unref(writeRef)
    await write?.elFormRef?.validate(async (isValid) => {
      if (isValid) {
        loading.value = true
        const data = (await write?.getFormData()) as ${simpleClassName}VO
        const res = await update${simpleClassName}VO(data)
            .catch(() => {
            })
            .finally(() => {
              loading.value = false
            })
        if (res) {
          ElMessage({
            message: '更新成功',
            type: 'success'
          })
          dialogVisible.value = false
          tableObject.currentPage = 1
          setSearchParams(tableSearchaData)
        }
      }
    })
  }

  const deleteData = async (row: ${simpleClassName}VO | null, multiple: boolean) => {
    tableObject.currentRow = row
    const {delList, getSelections} = methods
    const selections = await getSelections()
    delLoading.value = true
    await delList(
        multiple ? selections.map((v) => v.rowGuid as string) : [tableObject.currentRow?.rowGuid as string],
        multiple
    ).finally(() => {
      delLoading.value = false
    })
  }
</script>

<template>
  <ContentWrap title="操作">
    <Search :schema="allSchemas.searchSchema" @search="setSearchParams" @reset="setSearchParams"/>
  </ContentWrap>
  <ContentWrap>
    <div class="mb-10px">
      <ElButton type="primary" v-hasPermi="['${permissionPrefix}:create']" @click="addData">
        {{ t('exampleDemo.add') }}
      </ElButton>
      <ElButton :loading="delLoading" v-hasPermi="['${permissionPrefix}:delete']" type="danger"
                @click="deleteData(null, true)">
        {{ t('exampleDemo.del') }}
      </ElButton>
      <el-button
          type="warning"
          v-hasPermi="['${permissionPrefix}:export']"
          :loading="tableObject.exportLoading"
          @click="exportList('数据.xls')"
      >
        <Icon icon="ep:download" class="mr-5px"/>
        {{ t('action.export') }}
      </el-button>
    </div>
    <Table
        v-model:pageSize="tableObject.pageSize"
        v-model:currentPage="tableObject.currentPage"
        :max-height="tableObject.maxHeight"
        :columns="allSchemas.tableColumns"
        :data="tableObject.tableList"
        :loading="tableObject.loading"
        :pagination="{
            total: tableObject.total
          }"
        @register="register"
    >
      <template #action="{ row }">
        <el-button
            link
            type="primary"
            v-hasPermi="['${permissionPrefix}:update']"
            @click="updateData(row)"
        >
          <Icon icon="ep:edit" class="mr-1px"/>
          {{ t('formDemo.change') }}
        </el-button>
        <el-button
            link
            type="primary"
            v-hasPermi="['${permissionPrefix}:detail']"
            @click="showData(row)"
        >
          <Icon icon="ep:view" class="mr-1px"/>
          {{ t('formDemo.detail') }}
        </el-button>
        <el-button
            link
            type="primary"
            v-hasPermi="['${permissionPrefix}:delete']"
            @click="deleteData(row.rowGuid, false)"
        >
          <Icon icon="ep:delete" class="mr-1px"/>
          {{ t('formDemo.delete') }}
        </el-button>
      </template>
    </Table>
  </ContentWrap>

  <Dialog v-model="dialogVisible" :title="dialogTitle">
    <${simpleClassName}Form
        v-if="['create', 'update'].includes(actionType)"
        ref="writeRef"
        :form-schema="allSchemas.formSchema"
        :current-row="tableObject.currentRow"
    />
    <Descriptions
        v-if="actionType === 'detail'"
        :schema="allSchemas.detail"
        :data="tableObject.currentRow"
        title="detail"
        message="提示"
    />
    <template #footer>
      <ElButton
          v-if="actionType === 'create'"
          type="primary"
          :loading="loading"
          @click="addData"
      >
        {{ t('exampleDemo.save') }}
      </ElButton>
      <ElButton
          v-if="actionType === 'update'"
          type="primary"
          :loading="loading"
          @click="updateDate"
      >
        {{ t('formDemo.change') }}
      </ElButton>
      <el-button @click="dialogVisible = false">{{ t('dialog.close') }}</el-button>
    </template>
  </Dialog>
</template>
