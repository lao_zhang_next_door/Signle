package com.sinoy.workflow.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@MapperScan({"com.sinoy.workflow.dal.mysql.*"})
@Configuration
public class WorkflowMapperConfig {
}
