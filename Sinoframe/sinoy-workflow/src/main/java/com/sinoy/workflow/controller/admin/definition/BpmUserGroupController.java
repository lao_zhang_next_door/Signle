package com.sinoy.workflow.controller.admin.definition;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.enums.CommonStatusEnum;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.definition.vo.group.BpmUserGroupCreateReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.group.BpmUserGroupPageReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.group.BpmUserGroupUpdateReqVO;
import com.sinoy.workflow.convert.definition.BpmUserGroupConvert;
import com.sinoy.workflow.dal.dataobject.definition.BpmUserGroupDO;
import com.sinoy.workflow.service.definition.BpmUserGroupService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 用户组
 */
@RestController
@RequestMapping("/bpm/user-group")
@Validated
public class BpmUserGroupController {

    @Resource
    private BpmUserGroupService userGroupService;

    /**
     * 创建用户组
     * @param createReqVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:user-group:create')")
    public R createUserGroup(@Valid @RequestBody BpmUserGroupCreateReqVO createReqVO) {
        return R.ok().put("data",userGroupService.createUserGroup(createReqVO));
    }

    /**
     * 更新用户组
     * @param updateReqVO
     * @return
     */
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('bpm:user-group:update')")
    public R updateUserGroup(@Valid @RequestBody BpmUserGroupUpdateReqVO updateReqVO) {
        userGroupService.updateUserGroup(updateReqVO);
        return R.ok();
    }

    /**
     * 删除用户组
     * @param id 编号
     * @return
     */
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('bpm:user-group:delete')")
    public R deleteUserGroup(@RequestParam("id") Long id) {
        userGroupService.deleteUserGroup(id);
        return R.ok();
    }

    /**
     * 获得用户组
     * @param id 编号
     * @return
     */
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('bpm:user-group:query')")
    public R getUserGroup(@RequestParam("id") Long id) {
        BpmUserGroupDO userGroup = userGroupService.getUserGroup(id);
        return R.ok().put("data", BpmUserGroupConvert.INSTANCE.convert(userGroup));
    }

    /**
     * 获得用户组分页
     * @param pageVO
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('bpm:user-group:query')")
    public R getUserGroupPage(@Valid BpmUserGroupPageReqVO pageVO) {
        Page<BpmUserGroupDO> pageResult = userGroupService.getUserGroupPage(pageVO);
        return R.ok().put("data",BpmUserGroupConvert.INSTANCE.convertPage(pageResult));
    }

    /**
     * 获取用户组精简信息列表 只包含被开启的用户组，主要用于前端的下拉选项
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimpleUserGroups() {
        // 获用户门列表，只要开启状态的
        List<BpmUserGroupDO> list = userGroupService.getUserGroupListByStatus(CommonStatusEnum.ENABLE.getStatus());
        // 排序后，返回给前端
        return R.ok().put("data",BpmUserGroupConvert.INSTANCE.convertList2(list));
    }

}
