package com.sinoy.workflow.controller.admin.oa.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmOALeaveRespVO extends BpmOALeaveBaseVO {

    //请假表单主键
    private Long id;

    //状态  bpm_process_instance_result
    private Integer result;

    //申请时间
    @NotNull(message = "申请时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //流程id
    private String processInstanceId;

}
