package com.sinoy.workflow.framework.flowable.core.behavior.script.impl;

import com.sinoy.workflow.enums.definition.BpmTaskRuleScriptEnum;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 分配给发起人所属(单位或者公司)并且拥有律所领导角色 审批的 Script 实现类
 *
 */
@Component
public class BpmTaskAssignUnityLeaderRoleScript extends BpmTaskAssignUnityRoleAbstractScript {

    @Override
    public Set<Long> calculateTaskCandidateUsers(DelegateExecution execution) {
        return calculateTaskCandidateUsers(execution, "firmLeader");
    }

    @Override
    public BpmTaskRuleScriptEnum getEnum() {
        return BpmTaskRuleScriptEnum.UNITY_LEADER;
    }

}
