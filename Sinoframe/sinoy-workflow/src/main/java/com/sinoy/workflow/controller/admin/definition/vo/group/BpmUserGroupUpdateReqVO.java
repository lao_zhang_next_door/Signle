package com.sinoy.workflow.controller.admin.definition.vo.group;

import lombok.*;
import javax.validation.constraints.*;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmUserGroupUpdateReqVO extends BpmUserGroupBaseVO {

    //编号
    @NotNull(message = "编号不能为空")
    private Long rowId;

}
