package com.sinoy.workflow.controller.admin.oa.vo;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.*;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmOALeavePageReqVO extends PageParam {

    //状态  bpm_process_instance_result
    private Integer result;

    //请假类型 bpm_oa_type
    private Integer type;

    //原因
    private String reason;

    //申请时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date[] createTime;

}
