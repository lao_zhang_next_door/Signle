package com.sinoy.workflow.controller.admin.definition.vo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmModelRespVO extends BpmModelBaseVO {

    //编号
    private String id;

    //BPMN XML
    private String bpmnXml;

    //创建时间
    private Date createTime;

}
