package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmTaskDonePageItemRespVO extends BpmTaskTodoPageItemRespVO {

    //结束时间
    private Date endTime;

    //持续时间
    private Long durationInMillis;

    //任务结果  bpm_process_instance_result
    private Integer result;

    //审批建议
    private String reason;

}
