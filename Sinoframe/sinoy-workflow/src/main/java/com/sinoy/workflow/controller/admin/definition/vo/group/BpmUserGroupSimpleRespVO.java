package com.sinoy.workflow.controller.admin.definition.vo.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BpmUserGroupSimpleRespVO {

    //用户组编号
    private Long rowId;

    //用户组名字
    private String name;

}
