package com.sinoy.workflow.service.definition.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Bpm 表单的 Field 表单项 Response DTO
 * 字段的定义，可见 https://github.com/JakHuang/form-generator/issues/46 文档
 *
 *
 * 忽略 序列化中 不存在的 实体字段
 * @JsonIgnoreProperties(ignoreUnknown = true)
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpmFormFieldRespDTO {

    /**
     * 表单标题
     */
    private String label;
    /**
     * 表单字段的属性名，可自定义
     */
    @JsonProperty(value = "vModel")
    private String vModel;

}
