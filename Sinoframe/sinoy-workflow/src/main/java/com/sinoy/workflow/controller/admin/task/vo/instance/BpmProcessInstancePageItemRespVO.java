package com.sinoy.workflow.controller.admin.task.vo.instance;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 流程实例的分页 Item Response VO
 */
@Data
public class BpmProcessInstancePageItemRespVO {

    //流程实例的编号
    private String id;

    //流程名称
    private String name;

    //流程定义的编号
    private String processDefinitionId;

    //流程分类 bpm_model_category
    private String category;

    //流程实例的状态 bpm_process_instance_status
    private Integer status;

    //流程实例的结果 bpm_process_instance_result
    private Integer result;

    //提交时间
    private Date createTime;

    //结束时间
    private Date endTime;

    /**
     * 当前任务
     */
    private List<Task> tasks;

    /**
     * 流程任务
     */
    @Data
    public static class Task {

        //流程任务的编号
        private String id;

        //任务名称
        private String name;

    }

}
