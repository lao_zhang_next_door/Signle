package com.sinoy.workflow.controller.app.task;


import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.task.vo.instance.BpmProcessInstanceCancelReqVO;
import com.sinoy.workflow.controller.admin.task.vo.instance.BpmProcessInstanceCreateReqVO;
import com.sinoy.workflow.controller.admin.task.vo.instance.BpmProcessInstanceMyPageReqVO;
import com.sinoy.workflow.service.task.BpmProcessInstanceService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.getLoginUserId;


/**
 * 流程实例 通过流程定义创建的一次“申请”
 */
@RestController
@RequestMapping("/bpm/process-instance")
@Validated
public class AppBpmProcessInstanceController {

    @Resource
    private BpmProcessInstanceService processInstanceService;

    /**
     * 获得我的实例分页列表 在【我的流程】菜单中，进行调用
     * @param pageReqVO
     * @return
     */
    @GetMapping("/my-page")
    @PreAuthorize("@ss.hasPermission('bpm:process-instance:query')")
    public R getMyProcessInstancePage(
            @Valid BpmProcessInstanceMyPageReqVO pageReqVO) {
        return R.ok().put("data",processInstanceService.getMyProcessInstancePage(getLoginUserId(), pageReqVO));
    }

    /**
     * 新建流程实例
     * @param createReqVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:process-instance:query')")
    public R createProcessInstance(@Valid @RequestBody BpmProcessInstanceCreateReqVO createReqVO) {
        processInstanceService.createProcessInstance(getLoginUserId(), createReqVO);
        return R.ok();
    }

    /**
     * 获得指定流程实例 在【流程详细】界面中，进行调用
     * @param id 流程实例的编号
     * @return
     */
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('bpm:process-instance:query')")
    public R getProcessInstance(@RequestParam("id") String id) {
        return R.ok().put("data",processInstanceService.getProcessInstanceVO(id));
    }

    /**
     * 取消流程实例 撤回发起的流程
     * @param cancelReqVO
     * @return
     */
    @DeleteMapping("/cancel")
    @PreAuthorize("@ss.hasPermission('bpm:process-instance:cancel')")
    public R cancelProcessInstance(@Valid @RequestBody BpmProcessInstanceCancelReqVO cancelReqVO) {
        processInstanceService.cancelProcessInstance(getLoginUserId(), cancelReqVO);
        return R.ok();
    }
}
