package com.sinoy.workflow.example.listener;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.platform.system.component.utils.SpringContextUtils;
import com.sinoy.workflow.dal.dataobject.task.BpmTaskExtDO;
import com.sinoy.workflow.service.task.BpmProcessInstanceService;
import com.sinoy.workflow.service.task.BpmTaskService;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.engine.impl.el.FixedValue;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.service.delegate.DelegateTask;


/**
 * 任务监听器用于在特定的任务相关事件发生时，执行自定义的Java逻辑或表达式
 *
 * 任务监听器支持下列属性：
 *  event（事件）（必填）：任务监听器将被调用的任务事件类型。可用的事件有：
 *         create（创建）：当任务已经创建，并且所有任务参数都已经设置时触发。
 *         assignment（指派）：当任务已经指派给某人时触发。请注意：当流程执行到达用户任务时，create事件触发前，首先触发
 *         assignment事件。这看起来不是自然顺序，但是有实际原因的：当收到create事件时，我们通常希望查看任务的所有参数，包括
 *         办理人。
 *         complete（完成）：当任务已经完成，从运行时数据中删除前触发。
 *         delete（删除）：在任务即将被删除前触发。请注意当任务通过completeTask正常完成时也会触发
 *
 *   class：需要调用的代理类。这个类必须实现 org.activiti.engine.delegate.TaskListener 接口
 *
 *
 *   expression：（不能与class属性一起使用）：指定在事件发生时要执行的表达式。可以为被调用的对象传递 DelegateTask 对象与事件名（使用 task.eventName ）作为参数
 *
 *
 *
 *   delegateExpression：可以指定一个能够解析为 TaskListener 接口实现类对象的表达式。与服务任务类似
 *
 *
 */
@Slf4j
public class ExampleEvent implements TaskListener {

    public BpmProcessInstanceService processInstanceService = SpringContextUtils.getBean(BpmProcessInstanceService.class);

    public BpmTaskService bpmTaskService = SpringContextUtils.getBean(BpmTaskService.class);

    // 审核节点 流程配置种带入该参数
    private FixedValue approvalNode;

    @Override
    public void notify(DelegateTask delegateTask) {

        log.info("获取流程实例id: {}",delegateTask.getProcessInstanceId());
        log.info("获取流程获取执行id: {}",delegateTask.getExecutionId());
        log.info("获取流程定义id: {}",delegateTask.getProcessDefinitionId());

        ProcessInstance processInstance = processInstanceService.getProcessInstance(delegateTask.getProcessInstanceId());
        log.info("获取业务表单key: {}",processInstance.getBusinessKey());

        BpmTaskExtDO bpmTaskExtDO = bpmTaskService.getOne(new LambdaQueryWrapper<BpmTaskExtDO>().eq(BpmTaskExtDO::getTaskId,delegateTask.getId()));
        log.info("获取任务参数: {}",JSON.toJSONString(bpmTaskExtDO));

        log.info("流程节点参数: {}",JSON.toJSONString(approvalNode));

        //事件
        switch (delegateTask.getEventName()){
            case "complete":
                //调用完成事件
                break;
            case "create":
                //调用创建事件
                break;
            case "assignment":
                //调用指派事件
                break;
            case "delete":
                //调用删除事件
                break;

        }

        System.out.println("事件 >>>>>>"+ JSON.toJSONString(delegateTask));
    }
}
