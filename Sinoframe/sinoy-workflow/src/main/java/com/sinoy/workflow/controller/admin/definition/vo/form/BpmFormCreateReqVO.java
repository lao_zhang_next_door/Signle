package com.sinoy.workflow.controller.admin.definition.vo.form;

import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormBaseVO;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmFormCreateReqVO extends BpmFormBaseVO {

    //表单的配置
    @NotNull(message = "表单的配置不能为空")
    private String conf;

    //表单项的数组
    @NotNull(message = "表单项的数组不能为空")
    private List<String> fields;

}
