package com.sinoy.workflow.controller.admin.definition.vo.process;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmProcessDefinitionPageItemRespVO extends BpmProcessDefinitionRespVO {

    //表单名字
    private String formName;

    //部署时间
    private Date deploymentTime;

}
