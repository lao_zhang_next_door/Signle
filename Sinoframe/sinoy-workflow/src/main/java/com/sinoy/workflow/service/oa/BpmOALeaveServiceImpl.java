package com.sinoy.workflow.service.oa;

import cn.hutool.core.date.DateUtil;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.workflow.api.task.BpmProcessInstanceApi;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeaveCreateReqVO;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeavePageReqVO;
import com.sinoy.workflow.controller.admin.task.dto.BpmProcessInstanceCreateReqDTO;
import com.sinoy.workflow.convert.oa.BpmOALeaveConvert;
import com.sinoy.workflow.dal.dataobject.oa.BpmOALeaveDO;
import com.sinoy.workflow.dal.mysql.oa.BpmOALeaveMapper;
import com.sinoy.workflow.enums.task.BpmProcessInstanceResultEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static com.sinoy.workflow.enums.ErrorCodeConstants.OA_LEAVE_NOT_EXISTS;


/**
 * OA 请假申请 Service 实现类
 *
 */
@Service
@Validated
public class BpmOALeaveServiceImpl extends BaseServiceImpl<BpmOALeaveMapper,BpmOALeaveDO> implements BpmOALeaveService {

    /**
     * OA 请假对应的流程定义 KEY
     */
    public static final String PROCESS_KEY = "oa_leave";

    @Resource
    private BpmOALeaveMapper leaveMapper;

    @Resource
    private BpmProcessInstanceApi processInstanceApi;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long createLeave(Long userId, BpmOALeaveCreateReqVO createReqVO) {
        // 插入 OA 请假单
        long day = DateUtil.betweenDay(createReqVO.getStartTime(), createReqVO.getEndTime(), false);
        BpmOALeaveDO leave = BpmOALeaveConvert.INSTANCE.convert(createReqVO).setUserId(userId).setDay(day)
                .setResult(BpmProcessInstanceResultEnum.PROCESS.getResult());
        leaveMapper.insert(leave);

        // 发起 BPM 流程
        Map<String, Object> processInstanceVariables = new HashMap<>();
        processInstanceVariables.put("day", day);
        String processInstanceId = processInstanceApi.createProcessInstance(userId,
                new BpmProcessInstanceCreateReqDTO().setProcessDefinitionKey(PROCESS_KEY)
                        .setVariables(processInstanceVariables).setBusinessKey(String.valueOf(leave.getRowId())));

        // 将工作流的编号，更新到 OA 请假单中
        leaveMapper.updateById(new BpmOALeaveDO(leave.getRowId()).setProcessInstanceId(processInstanceId));
        return leave.getRowId();
    }

    @Override
    public void updateLeaveResult(Long id, Integer result) {
        validateLeaveExists(id);
        leaveMapper.updateById(new BpmOALeaveDO(id).setResult(result));
    }

    private void validateLeaveExists(Long id) {
        if (leaveMapper.selectById(id) == null) {
            throw new BaseException(OA_LEAVE_NOT_EXISTS);
        }
    }

    @Override
    public BpmOALeaveDO getLeave(Long id) {
        return leaveMapper.selectById(id);
    }

    @Override
    public PageResult<BpmOALeaveDO> getLeavePage(Long userId, BpmOALeavePageReqVO pageReqVO) {
        return leaveMapper.selectPage(userId, pageReqVO);
    }

}
