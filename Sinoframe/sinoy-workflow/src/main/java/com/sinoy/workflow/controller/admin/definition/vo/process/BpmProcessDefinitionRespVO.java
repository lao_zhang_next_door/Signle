package com.sinoy.workflow.controller.admin.definition.vo.process;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class BpmProcessDefinitionRespVO {

    //编号
    private String id;

    //版本
    private Integer version;

    //流程名称
    @NotEmpty(message = "流程名称不能为空")
    private String name;

    //流程描述
    private String description;

    //流程分类
    @NotEmpty(message = "流程分类不能为空")
    private String category;

    //表单类型  bpm_model_form_type 数据字典
    private Integer formType;

    //表单编号
    private Long formId;

    //表单的配置
    private String formConf;

    //表单项的数组
    private List<String> formFields;

    //自定义表单的提交路径，使用 Vue 的路由地址
    private String formCustomCreatePath;

    //自定义表单的查看路径，使用 Vue 的路由地址
    private String formCustomViewPath;

    //中断状态
    private Integer suspensionState;

    //自定义移动端表单的提交路由
    private String appFormCustomCreatePath;

    //自定义移动端表单的查看路由
    private String appFormCustomViewPath;

}
