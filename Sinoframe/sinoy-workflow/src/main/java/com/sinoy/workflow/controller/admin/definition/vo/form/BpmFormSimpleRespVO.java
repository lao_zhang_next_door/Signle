package com.sinoy.workflow.controller.admin.definition.vo.form;

import lombok.Data;

@Data
public class BpmFormSimpleRespVO {

    //表单编号
    private Long rowId;

    //表单名称
    private String name;

}
