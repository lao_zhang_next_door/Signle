package com.sinoy.workflow.controller.admin.definition;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.definition.vo.rule.BpmTaskAssignRuleCreateReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.rule.BpmTaskAssignRuleUpdateReqVO;
import com.sinoy.workflow.service.definition.BpmTaskAssignRuleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 任务分配规则
 */
@RestController
@RequestMapping("/bpm/task-assign-rule")
@Validated
public class BpmTaskAssignRuleController {

    @Resource
    private BpmTaskAssignRuleService taskAssignRuleService;

    /**
     * 获得任务分配规则列表
     * @param modelId 模型编号
     * @param processDefinitionId 流程定义的编号
     * @return
     */
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermission('bpm:task-assign-rule:query')")
    public R getTaskAssignRuleList(
            @RequestParam(value = "modelId", required = false) String modelId,
            @RequestParam(value = "processDefinitionId", required = false) String processDefinitionId) {
        return R.ok().put("data",taskAssignRuleService.getTaskAssignRuleList(modelId, processDefinitionId));
    }

    /**
     * 创建任务分配规则
     * @param reqVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:task-assign-rule:create')")
    public R createTaskAssignRule(@Valid @RequestBody BpmTaskAssignRuleCreateReqVO reqVO) {
        return R.ok().put("data",taskAssignRuleService.createTaskAssignRule(reqVO));
    }

    /**
     * 更新任务分配规则
     * @param reqVO
     * @return
     */
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('bpm:task-assign-rule:update')")
    public R updateTaskAssignRule(@Valid @RequestBody BpmTaskAssignRuleUpdateReqVO reqVO) {
        taskAssignRuleService.updateTaskAssignRule(reqVO);
        return R.ok();
    }
}
