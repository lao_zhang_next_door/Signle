package com.sinoy.workflow.controller.admin.definition.vo.rule;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmTaskAssignRuleUpdateReqVO extends BpmTaskAssignRuleBaseVO {

    //任务分配规则的编号
    @NotNull(message = "任务分配规则的编号不能为空")
    private Long rowId;

}
