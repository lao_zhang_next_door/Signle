package com.sinoy.workflow.controller.admin.definition;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.definition.vo.process.BpmProcessDefinitionListReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.process.BpmProcessDefinitionPageReqVO;
import com.sinoy.workflow.service.definition.BpmProcessDefinitionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 流程定义
 */
@RestController
@RequestMapping("/bpm/process-definition")
@Validated
public class BpmProcessDefinitionController {

    @Resource
    private BpmProcessDefinitionService bpmDefinitionService;

    /**
     * 获得流程定义分页
     * @param pageReqVO
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('bpm:process-definition:query')")
    public R getProcessDefinitionPage(
            BpmProcessDefinitionPageReqVO pageReqVO) {
        return R.ok().put("data",bpmDefinitionService.getProcessDefinitionPage(pageReqVO));
    }

    /**
     * 获得流程定义列表
     * @param listReqVO
     * @return
     */
    @GetMapping ("/list")
    @PreAuthorize("@ss.hasPermission('bpm:process-definition:query')")
    public R getProcessDefinitionList(
            BpmProcessDefinitionListReqVO listReqVO) {
        return R.ok().put("data",bpmDefinitionService.getProcessDefinitionList(listReqVO));
    }

    /**
     * 获得流程定义的 BPMN XML
     * @param id 编号
     * @return
     */
    @GetMapping ("/get-bpmn-xml")
    @PreAuthorize("@ss.hasPermission('bpm:process-definition:query')")
    public R getProcessDefinitionBpmnXML(@RequestParam("id") String id) {
        String bpmnXML = bpmDefinitionService.getProcessDefinitionBpmnXML(id);
        return R.ok().put("data",bpmnXML);
    }
}
