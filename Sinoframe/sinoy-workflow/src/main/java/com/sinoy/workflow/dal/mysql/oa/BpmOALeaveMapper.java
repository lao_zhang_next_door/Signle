package com.sinoy.workflow.dal.mysql.oa;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.base.query.LambdaQueryWrapperX;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeavePageReqVO;
import com.sinoy.workflow.dal.dataobject.oa.BpmOALeaveDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 请假申请 Mapper
 */
public interface BpmOALeaveMapper extends ExtendMapper<BpmOALeaveDO> {

    default PageResult<BpmOALeaveDO> selectPage(Long userId, BpmOALeavePageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<BpmOALeaveDO>()
                .eqIfPresent(BpmOALeaveDO::getUserId, userId)
                .eqIfPresent(BpmOALeaveDO::getResult, reqVO.getResult())
                .eqIfPresent(BpmOALeaveDO::getType, reqVO.getType())
                .likeIfPresent(BpmOALeaveDO::getReason, reqVO.getReason())
                .betweenIfPresent(BpmOALeaveDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(BpmOALeaveDO::getRowId));
    }

}
