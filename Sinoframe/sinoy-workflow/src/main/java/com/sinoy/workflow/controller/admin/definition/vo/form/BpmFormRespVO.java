package com.sinoy.workflow.controller.admin.definition.vo.form;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmFormRespVO extends BpmFormBaseVO {

    //表单编号
    private Long rowId;

    //表单的配置
    @NotNull(message = "表单的配置不能为空")
    private String conf;

    //表单项的数组
    @NotNull(message = "表单项的数组不能为空")
    private List<String> fields;

    //创建时间
    private Date createTime;

}
