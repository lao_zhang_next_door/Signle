package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmTaskRespVO extends BpmTaskDonePageItemRespVO {

    //任务定义的标识
    private String definitionKey;

    /**
     * 审核的用户信息
     */
    private User assigneeUser;

    /**
     * 用户信息
     */
    @Data
    public static class User {

        //用户编号
        private Long rowId;

        //用户昵称
        private String userName;

        //部门编号
        private Long deptId;

        //部门名称
        private String deptName;

    }
}
