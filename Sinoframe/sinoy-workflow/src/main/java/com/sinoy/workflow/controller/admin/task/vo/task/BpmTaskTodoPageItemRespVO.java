package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;
import org.flowable.task.api.DelegationState;

import java.util.Date;

@Data
public class BpmTaskTodoPageItemRespVO {

    //任务编号
    private String id;

    //任务名字
    private String name;

    //接收时间
    private Date claimTime;

    //创建时间
    private Date createTime;

    //激活状态 SuspensionState
    private Integer suspensionState;

    /**
     * 任务协办状态 PENDING表示任务正在协办   RESOLVED表示协办结束
     */
    private DelegationState delegationState;

    /**
     * 所属流程实例
     */
    private ProcessInstance processInstance;

    /**
     * 流程实例
     */
    @Data
    public static class ProcessInstance {

        //流程实例编号
        private String id;

        //流程实例名称
        private String name;

        //发起人的用户编号
        private Long startUserId;

        //发起人的用户昵称
        private String startUserNickname;

        //发起人的头像
        private String startUserAvatar;

        //流程定义的编号
        private String processDefinitionId;

    }

}
