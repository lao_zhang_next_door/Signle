package com.sinoy.workflow.controller.admin.definition.vo.group;

import com.alibaba.fastjson2.util.DateUtils;
import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmUserGroupPageReqVO extends PageParam {

    //组名
    private String name;

    //状态
    private Integer status;

    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date[] createTime;

}
