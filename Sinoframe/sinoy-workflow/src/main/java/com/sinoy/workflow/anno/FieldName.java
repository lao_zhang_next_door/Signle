package com.sinoy.workflow.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段对应数据库注释
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldName {
    // 对应数据库注释
    String value() default "";

    // 对应代码项 dict值
    String type() default "";

    // 是否忽视该字段
    boolean isIgnore() default false;

    // 字段类型
    String fieldType() default "";
}
