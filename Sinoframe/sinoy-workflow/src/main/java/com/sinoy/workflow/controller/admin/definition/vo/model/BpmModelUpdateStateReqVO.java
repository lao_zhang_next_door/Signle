package com.sinoy.workflow.controller.admin.definition.vo.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BpmModelUpdateStateReqVO {

    //编号
    @NotNull(message = "编号不能为空")
    private String id;

    //状态  SuspensionState枚举
    @NotNull(message = "状态不能为空")
    private Integer state;

}
