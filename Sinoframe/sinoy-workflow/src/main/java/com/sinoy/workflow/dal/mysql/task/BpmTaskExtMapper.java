package com.sinoy.workflow.dal.mysql.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.workflow.dal.dataobject.task.BpmTaskExtDO;

import java.util.Collection;
import java.util.List;

public interface BpmTaskExtMapper extends ExtendMapper<BpmTaskExtDO> {

    default void updateByTaskId(BpmTaskExtDO entity) {
        update(entity, new LambdaQueryWrapper<BpmTaskExtDO>().eq(BpmTaskExtDO::getTaskId, entity.getTaskId()));
    }

    default List<BpmTaskExtDO> selectListByTaskIds(Collection<String> taskIds) {
        return selectList(BpmTaskExtDO::getTaskId, taskIds);
    }

    default BpmTaskExtDO selectByTaskId(String taskId) {
        return selectOne(BpmTaskExtDO::getTaskId, taskId);
    }

}
