package com.sinoy.workflow.framework.flowable.core.behavior.script.impl;

import com.sinoy.workflow.enums.definition.BpmTaskRuleScriptEnum;
import com.sinoy.workflow.framework.flowable.core.behavior.script.BpmTaskAssignScript;
import com.sinoy.workflow.service.task.BpmProcessInstanceService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 分配给发起人审批的 Script 实现类
 */
@Component
public class BpmTaskAssignStartUserScript implements BpmTaskAssignScript {

    @Resource
    @Lazy // 解决循环依赖
    private BpmProcessInstanceService bpmProcessInstanceService;

    @Override
    public Set<Long> calculateTaskCandidateUsers(DelegateExecution execution) {
        ProcessInstance processInstance = bpmProcessInstanceService.getProcessInstance(execution.getProcessInstanceId());
        Long startUserId = Long.valueOf(processInstance.getStartUserId());
        return new HashSet<>(Arrays.asList(startUserId));
    }

    @Override
    public BpmTaskRuleScriptEnum getEnum() {
        return BpmTaskRuleScriptEnum.START_USER;
    }

}
