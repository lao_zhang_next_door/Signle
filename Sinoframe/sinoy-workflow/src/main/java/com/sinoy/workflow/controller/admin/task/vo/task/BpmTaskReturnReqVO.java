package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BpmTaskReturnReqVO {

    // 任务id
    @NotNull
    private String taskId;

    // 任务key
    @NotNull
    private String targetKey;

    //
    private String comment;

}
