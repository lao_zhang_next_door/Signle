package com.sinoy.workflow.controller.admin.definition;

import com.sinoy.core.common.utils.file.IoUtils;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.definition.vo.model.*;
import com.sinoy.workflow.convert.definition.BpmModelConvert;
import com.sinoy.workflow.service.definition.BpmModelService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;

/**
 * 流程模型
 */
@RestController
@RequestMapping("/bpm/model")
@Validated
public class BpmModelController {

    @Resource
    private BpmModelService modelService;

    /**
     * 获得模型分页
     * @param pageVO
     * @return
     */
    @GetMapping("/page")
    public R getModelPage(BpmModelPageReqVO pageVO) {
        return R.ok().put("data",modelService.getModelPage(pageVO));
    }

    /**
     * 获得模型
     * @param id 编号
     * @return
     */
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('bpm:model:query')")
    public R getModel(@RequestParam("id") String id) {
        BpmModelRespVO model = modelService.getModel(id);
        return R.ok().put("data",model);
    }

    /**
     * 新建模型
     * @param createRetVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:model:create')")
    public R createModel(@Valid @RequestBody BpmModelCreateReqVO createRetVO) {
        return R.ok().put("data",modelService.createModel(createRetVO, null));
    }

    /**
     * 修改模型
     * @param modelVO
     * @return
     */
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('bpm:model:update')")
    public R updateModel(@Valid @RequestBody BpmModelUpdateReqVO modelVO) {
        modelService.updateModel(modelVO);
        return R.ok();
    }

    /**
     * 导入模型
     * @param importReqVO
     * @return
     * @throws IOException
     */
    @PostMapping("/import")
    @PreAuthorize("@ss.hasPermission('bpm:model:import')")
    public R importModel(@Valid BpmModeImportReqVO importReqVO) throws IOException {
        BpmModelCreateReqVO createReqVO = BpmModelConvert.INSTANCE.convert(importReqVO);
        // 读取文件
        String bpmnXml = IoUtils.readUtf8(importReqVO.getBpmnFile().getInputStream(), false);
        return R.ok().put("data",modelService.createModel(createReqVO, bpmnXml));
    }

    /**
     * 部署模型
     * @param id 编号
     * @return
     */
    @PostMapping("/deploy")
    @PreAuthorize("@ss.hasPermission('bpm:model:deploy')")
    public R deployModel(@RequestParam("id") String id) {
        modelService.deployModel(id);
        return R.ok();
    }

    /**
     * 修改模型的状态 实际更新的部署的流程定义的状态
     * @param reqVO
     * @return
     */
    @PutMapping("/update-state")
    @PreAuthorize("@ss.hasPermission('bpm:model:update')")
    public R updateModelState(@Valid @RequestBody BpmModelUpdateStateReqVO reqVO) {
        modelService.updateModelState(reqVO.getId(), reqVO.getState());
        return R.ok();
    }

    /**
     * 删除模型
     * @param id 编号
     * @return
     */
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('bpm:model:delete')")
    public R deleteModel(@RequestParam("id") String id) {
        modelService.deleteModel(id);
        return R.ok();
    }
}
