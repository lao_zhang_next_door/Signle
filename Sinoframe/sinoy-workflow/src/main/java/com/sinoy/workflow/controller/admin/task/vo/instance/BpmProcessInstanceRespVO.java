package com.sinoy.workflow.controller.admin.task.vo.instance;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 流程实例的 Response VO"
 */
@Data
public class BpmProcessInstanceRespVO {

    //流程实例的编号
    private String id;

    //流程名称
    private String name;

    //流程分类 bpm_model_category
    private String category;

    //流程实例的状态 bpm_process_instance_status
    private Integer status;

    //流程实例的结果 bpm_process_instance_result
    private Integer result;

    //提交时间
    private Date createTime;

    //结束时间
    private Date endTime;

    //提交的表单值
    private Map<String, Object> formVariables;

    //业务的唯一标识
    private String businessKey;

    /**
     * 发起流程的用户
     */
    private User startUser;

    /**
     * 流程定义
     */
    private ProcessDefinition processDefinition;

    /**
     * 用户信息
     */
    @Data
    public static class User {

        //用户编号
        private Long rowId;

        //用户昵称
        private String userName;

        //部门编号
        private Long deptId;

        //部门名称
        private String deptName;

    }

    /**
     * 流程定义信息
     */
    @Data
    public static class ProcessDefinition {

        //编号
        private String id;

        //表单类型 bpm_model_form_type
        private Integer formType;

        //表单编号
        private Long formId;

        //表单的配置
        private String formConf;

        //表单项的数组
        private List<String> formFields;

        //自定义表单的提交路径，使用 Vue 的路由地址
        private String formCustomCreatePath;

        //自定义表单的查看路径，使用 Vue 的路由地址
        private String formCustomViewPath;

        //自定义移动端的提交路径，使用 Vue 的路由地址
        private String appFormCustomCreatePath;

        //自定义移动端的查看路径，使用 Vue 的路由地址
        private String appFormCustomViewPath;

        //BPMN XML
        private String bpmnXml;

    }

}
