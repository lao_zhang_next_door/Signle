package com.sinoy.workflow.controller.admin.definition.vo.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class BpmModelUpdateReqVO {

    //编号
    @NotEmpty(message = "编号不能为空")
    private String id;

    //流程名称
    private String name;

    //流程描述
    private String description;

    //流程分类
    private String category;

    //BPMN XML
    private String bpmnXml;

    //表单类型 参见 bpm_model_form_type 数据字典
    private Integer formType;

    //表单编号
    private Long formId;

    //自定义表单的提交路径，使用 Vue 的路由地址
    private String formCustomCreatePath;

    //自定义表单的查看路径，使用 Vue 的路由地址
    private String formCustomViewPath;

    //自定义移动端表单的提交路由
    private String appFormCustomCreatePath;

    //自定义移动端表单的查看路由
    private String appFormCustomViewPath;

}
