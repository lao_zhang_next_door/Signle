package com.sinoy.workflow.service.definition.dto;

import lombok.Data;

/**
 * BPM 流程 MetaInfo Response DTO
 * 主要用于 { Model#setMetaInfo(String)} 的存储
 *
 */
@Data
public class BpmModelMetaInfoRespDTO {

    /**
     * 流程描述
     */
    private String description;
    /**
     * 表单类型
     */
    private Integer formType;
    /**
     * 表单编号
     *
     */
    private Long formId;
    /**
     * 自定义表单的提交路径，使用 Vue 的路由地址
     *
     */
    private String formCustomCreatePath;
    /**
     * 自定义表单的查看路径，使用 Vue 的路由地址
     *
     */
    private String formCustomViewPath;

    /**
     * 自定义移动端表单的提交路径，使用 Vue 的路由地址
     *
     */
    private String appFormCustomCreatePath;
    /**
     * 自定义移动端表单的查看路径，使用 Vue 的路由地址
     *
     */
    private String appFormCustomViewPath;
}
