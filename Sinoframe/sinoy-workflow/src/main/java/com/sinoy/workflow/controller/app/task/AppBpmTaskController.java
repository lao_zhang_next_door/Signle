package com.sinoy.workflow.controller.app.task;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.task.vo.task.*;
import com.sinoy.workflow.service.task.BpmTaskService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.getLoginUserId;

/**
 * 流程任务实例
 */
@RestController
@RequestMapping("/bpm/task")
@Validated
public class AppBpmTaskController {

    @Resource
    private BpmTaskService taskService;

    /**
     * 获取 Todo 待办任务分页
     * @param pageVO
     * @returnn
     */
    @GetMapping("/todo-page")
    public R getTodoTaskPage(@Valid BpmTaskTodoPageReqVO pageVO) {
        return R.ok().put("data",taskService.getTodoTaskPage(getLoginUserId(), pageVO));
    }

    /**
     * 获取 Done 已办任务分页
     * @param pageVO
     * @return
     */
    @GetMapping("/done-page")
    public R getDoneTaskPage(@Valid BpmTaskDonePageReqVO pageVO) {
        return R.ok().put("data",taskService.getDoneTaskPage(getLoginUserId(), pageVO));
    }

    /**
     * 获得指定流程实例的任务列表 包括完成的、未完成的
     * @param processInstanceId 流程实例的编号
     * @return
     */
    @GetMapping("/list-by-process-instance-id")
    public R getTaskListByProcessInstanceId(@RequestParam("processInstanceId") String processInstanceId) {
        return R.ok().put("data",taskService.getTaskListByProcessInstanceId(processInstanceId));
    }

    /**
     * 通过任务
     * @param reqVO
     * @return
     */
    @PutMapping("/approve")
    public R approveTask(@Valid @RequestBody BpmTaskApproveReqVO reqVO) {
        taskService.approveTask(getLoginUserId(), reqVO);
        return R.ok();
    }

    /**
     * 不通过任务
     * @param reqVO
     * @return
     */
    @PutMapping("/reject")
    public R rejectTask(@Valid @RequestBody BpmTaskRejectReqVO reqVO) {
        taskService.rejectTask(getLoginUserId(), reqVO);
        return R.ok();
    }

}
