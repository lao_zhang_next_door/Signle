package com.sinoy.workflow.controller.admin.definition.vo.group;

import lombok.*;
import java.util.*;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmUserGroupRespVO extends BpmUserGroupBaseVO {

    //编号
    private Long rowId;

    //创建时间
    private Date createTime;

}
