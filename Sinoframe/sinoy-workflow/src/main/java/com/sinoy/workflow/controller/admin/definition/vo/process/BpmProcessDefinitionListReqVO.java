package com.sinoy.workflow.controller.admin.definition.vo.process;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BpmProcessDefinitionListReqVO extends PageParam {

    //中断状态  SuspensionState 枚举
    private Integer suspensionState;

}
