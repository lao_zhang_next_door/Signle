package com.sinoy.workflow.controller.admin.task.vo.instance;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 流程实例的取消 Request VO
 */
@Data
public class BpmProcessInstanceCancelReqVO {

    //流程实例的编号
    @NotEmpty(message = "流程实例的编号不能为空")
    private String id;

    //取消原因
    @NotEmpty(message = "取消原因不能为空")
    private String reason;

}
