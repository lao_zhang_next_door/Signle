package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BpmTaskUpdateAssigneeReqVO {

    //任务编号
    @NotEmpty(message = "任务编号不能为空")
    private String id;

    //新审批人的用户编号
    @NotNull(message = "新审批人的用户编号不能为空")
    private Long assigneeUserId;

}
