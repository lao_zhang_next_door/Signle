package com.sinoy.workflow.controller.admin.definition.vo.form;

import lombok.*;
import javax.validation.constraints.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmFormUpdateReqVO extends BpmFormBaseVO {

    //表单编号
    @NotNull(message = "表单编号不能为空")
    private Long rowId;

    //表单的配置
    @NotNull(message = "表单的配置不能为空")
    private String conf;

    //表单项的数组
    @NotNull(message = "表单项的数组不能为空")
    private List<String> fields;

}
