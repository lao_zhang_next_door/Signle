package com.sinoy.workflow.service.oa.listener;

import com.sinoy.workflow.framework.bpm.core.event.BpmProcessInstanceResultEvent;
import com.sinoy.workflow.framework.bpm.core.event.BpmProcessInstanceResultEventListener;
import com.sinoy.workflow.service.oa.BpmOALeaveService;
import com.sinoy.workflow.service.oa.BpmOALeaveServiceImpl;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * OA 请假单的结果的监听器实现类
 *
 */
@Component
public class BpmOALeaveResultListener extends BpmProcessInstanceResultEventListener {

    @Resource
    private BpmOALeaveService leaveService;

    @Override
    protected String getProcessDefinitionKey() {
        return BpmOALeaveServiceImpl.PROCESS_KEY;
    }

    @Override
    protected void onEvent(BpmProcessInstanceResultEvent event) {
        leaveService.updateLeaveResult(Long.parseLong(event.getBusinessKey()), event.getResult());
    }

}
