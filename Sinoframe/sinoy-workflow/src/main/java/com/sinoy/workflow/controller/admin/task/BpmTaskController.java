package com.sinoy.workflow.controller.admin.task;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.task.vo.task.*;
import com.sinoy.workflow.service.task.BpmTaskService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.getLoginUserId;

/**
 * 流程任务实例
 */
@RestController
@RequestMapping("/bpm/task")
@Validated
public class BpmTaskController {

    @Resource
    private BpmTaskService taskService;

    /**
     * 获取 Todo 待办任务分页
     * @param pageVO
     * @return
     */
    @GetMapping("todo-page")
    @PreAuthorize("@ss.hasPermission('bpm:task:query')")
    public R getTodoTaskPage(@Valid BpmTaskTodoPageReqVO pageVO) {
        return R.ok().put("data",taskService.getTodoTaskPage(getLoginUserId(), pageVO));
    }

    /**
     * 获取 Done 已办任务分页
     * @param pageVO
     * @return
     */
    @GetMapping("done-page")
    @PreAuthorize("@ss.hasPermission('bpm:task:query')")
    public R getDoneTaskPage(@Valid BpmTaskDonePageReqVO pageVO) {
        return R.ok().put("data",taskService.getDoneTaskPage(getLoginUserId(), pageVO));
    }

    /**
     * 获得指定流程实例的任务列表 包括完成的、未完成的
     * @param processInstanceId 流程实例的编号
     * @return
     */
    @GetMapping("/list-by-process-instance-id")
    @PreAuthorize("@ss.hasPermission('bpm:task:query')")
    public R getTaskListByProcessInstanceId(@RequestParam("processInstanceId") String processInstanceId) {
        return R.ok().put("data",taskService.getTaskListByProcessInstanceId(processInstanceId));
    }

    /**
     * 通过任务
     * @param reqVO
     * @return
     */
    @PutMapping("/approve")
    @PreAuthorize("@ss.hasPermission('bpm:task:update')")
    public R approveTask(@Valid @RequestBody BpmTaskApproveReqVO reqVO) {
        taskService.approveTask(getLoginUserId(), reqVO);
        return R.ok();
    }

    /**
     * 不通过任务
     * @param reqVO
     * @return
     */
    @PutMapping("/reject")
    @PreAuthorize("@ss.hasPermission('bpm:task:update')")
    public R rejectTask(@Valid @RequestBody BpmTaskRejectReqVO reqVO) {
        taskService.rejectTask(getLoginUserId(), reqVO);
        return R.ok();
    }

    /**
     * 更新任务的负责人 用于【流程详情】的【转派】按钮
     * @param reqVO
     * @return
     */
    @PutMapping("/update-assignee")
    @PreAuthorize("@ss.hasPermission('bpm:task:update')")
    public R updateTaskAssignee(@Valid @RequestBody BpmTaskUpdateAssigneeReqVO reqVO) {
        taskService.updateTaskAssignee(getLoginUserId(), reqVO);
        return R.ok();
    }

    /**
     * 加签 委派人员
     * @param reqVO
     * @return
     */
    @PutMapping("/countersign-task")
    @PreAuthorize("@ss.hasPermission('bpm:task:update')")
    public R countersignTask(@Valid @RequestBody BpmCountersignTaskReqVO reqVO){
        taskService.countersignTask(getLoginUserId(), reqVO);
        return R.ok();
    }

    /**
     * 完成 加签 委派
     * @return
     */
    @PutMapping("/resolve-task")
    @PreAuthorize("@ss.hasPermission('bpm:task:update')")
    public R resolveTask(@RequestParam String taskId){
        taskService.resolveTask(getLoginUserId(),taskId);
        return R.ok();
    }

    /**
     * 任务 回退
     * @return
     */
    @PostMapping("/task-return")
    public R taskReturn(@RequestBody BpmTaskReturnReqVO bpmTaskReturnReqVO){
        taskService.taskReturn(bpmTaskReturnReqVO);
        return R.ok();
    }

    /**
     * 查询可回退的节点
     * @param taskId
     * @return
     */
    @PostMapping("/find-return-task-list")
    public R findReturnTaskList(@RequestParam String taskId){
        return R.ok().put("data",taskService.findReturnTaskList(taskId));
    }

}
