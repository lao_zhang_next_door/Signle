package com.sinoy.workflow.util;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson2.JSONObject;
import com.sinoy.workflow.anno.FieldName;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 设置FormVariables
 */
public class FormVariablesSetUtils {


    /**
     * 根据注解获取 map   {字段名1:{ name:'',label:'',type:'',filedType:'' },字段名2:{}}
     * @param clazz
     * @return
     */
    private static Map<String,Map> generateFieldMap(Class clazz){

        return new LinkedHashMap(){
            {
                Field[] objFields = clazz.getDeclaredFields();//字段信息集合
                Field.setAccessible(objFields, true);
                if (objFields != null && objFields.length > 0) {
                    for (int i = 0; i < objFields.length; i++) {
                        String objName = objFields[i].getName();//字段名称
                        FieldName fieldName = objFields[i].getAnnotation(FieldName.class);
                        if(fieldName == null){
                            continue;
                        }
                        // 忽视字段 跳过
                        if(fieldName.isIgnore()) {
                            continue;
                        }
                        Map<String,Object> m = new HashMap(){
                            {
                                put("name",objName);
                                put("label",fieldName.value());
                                put("type",fieldName.type());
                                put("filedType",fieldName.fieldType());
                            }
                        };
                        put(objName,m);
                    }
                }
            }
        };
    }

    /**
     * 生成formVariables map
     * @param bean
     * @param clazzName
     * @return
     */
    public static Map<String, Object> beanToMap(Object bean,Class clazzName) {
        Map<String,Map> map = generateFieldMap(clazzName);
        Map<String, Object> objectMap = BeanUtil.beanToMap(bean);

        map.forEach((t, u) -> {
            if(objectMap.containsKey(t)){
//                JSONObject object = new JSONObject();
//                object.put("name",objectMap.get(t));
//                object.put("label",u.get("label"));
//                object.put("key",t);
//                object.put("type",u.get("type"));
//                object.put("fieldType",u.get("fieldType"));
                objectMap.put(t,u);
            }
        });
        return objectMap;
    }



}
