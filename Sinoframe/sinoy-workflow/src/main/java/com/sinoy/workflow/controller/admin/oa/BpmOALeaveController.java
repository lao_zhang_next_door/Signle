package com.sinoy.workflow.controller.admin.oa;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeaveCreateReqVO;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeavePageReqVO;
import com.sinoy.workflow.convert.oa.BpmOALeaveConvert;
import com.sinoy.workflow.dal.dataobject.oa.BpmOALeaveDO;
import com.sinoy.workflow.service.oa.BpmOALeaveService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static com.sinoy.core.security.utils.SecurityFrameworkUtils.*;

/**
 * OA 请假申请 Controller，用于演示自己存储数据，接入工作流的例子
 *
 */
@RestController
@RequestMapping("/bpm/oa/leave")
@Validated
public class BpmOALeaveController {

    @Resource
    private BpmOALeaveService leaveService;

    /**
     * 创建请求申请
     * @param createReqVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:oa-leave:create')")
    public R createLeave(@Valid @RequestBody BpmOALeaveCreateReqVO createReqVO) {
        leaveService.createLeave(getLoginUserId(), createReqVO);
        return R.ok();
    }

    /**
     * 获得请假申请
     * @param id 编号
     * @return
     */
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('bpm:oa-leave:query')")
    public R getLeave(@RequestParam("id") Long id) {
        BpmOALeaveDO leave = leaveService.getLeave(id);
        return R.ok().put("data", BpmOALeaveConvert.INSTANCE.convert(leave));
    }

    /**
     * 获得请假申请分页
     * @param pageVO
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('bpm:oa-leave:query')")
    public R getLeavePage(@Valid BpmOALeavePageReqVO pageVO) {
        PageResult<BpmOALeaveDO> pageResult = leaveService.getLeavePage(getLoginUserId(), pageVO);
        return R.ok().put("data",BpmOALeaveConvert.INSTANCE.convertPage(pageResult));
    }

}
