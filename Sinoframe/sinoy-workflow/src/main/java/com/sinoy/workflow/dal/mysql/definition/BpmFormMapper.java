package com.sinoy.workflow.dal.mysql.definition;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.base.query.LambdaQueryWrapperX;
import com.sinoy.core.database.mybatisplus.base.query.QueryWrapperX;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormPageReqVO;
import com.sinoy.workflow.dal.dataobject.definition.BpmFormDO;
import com.sinoy.workflow.dal.dataobject.task.BpmProcessInstanceExtDO;

/**
 * 动态表单 Mapper
 */
public interface BpmFormMapper extends ExtendMapper<BpmFormDO> {

    default Page<BpmFormDO> selectPage(BpmFormPageReqVO reqVO) {
        Page<BpmFormDO> page = new Page(reqVO.getCurrentPage(), reqVO.getPageSize());
        QueryWrapperX<BpmFormDO> queryWrapper = new QueryWrapperX<BpmFormDO>();
        queryWrapper.likeIfPresent("name", reqVO.getName())
                .orderByDesc("row_id");
        return selectPage(page, queryWrapper);

    }

}
