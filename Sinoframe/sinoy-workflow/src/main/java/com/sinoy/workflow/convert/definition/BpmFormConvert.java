package com.sinoy.workflow.convert.definition;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormCreateReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormRespVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormSimpleRespVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormUpdateReqVO;
import com.sinoy.workflow.dal.dataobject.definition.BpmFormDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 动态表单 Convert
 *
 */
@Mapper
public interface BpmFormConvert {

    BpmFormConvert INSTANCE = Mappers.getMapper(BpmFormConvert.class);

    BpmFormDO convert(BpmFormCreateReqVO bean);

    BpmFormDO convert(BpmFormUpdateReqVO bean);

    BpmFormRespVO convert(BpmFormDO bean);

    List<BpmFormSimpleRespVO> convertList2(List<BpmFormDO> list);

    Page<BpmFormRespVO> convertPage(Page<BpmFormDO> page);

}
