package com.sinoy.workflow.controller.admin.task.vo.task;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 加签 委派
 */
@Data
public class BpmCountersignTaskReqVO {

    //任务编号
    @NotEmpty(message = "任务编号不能为空")
    private String id;

    //委派人的用户编号
    @NotNull(message = "委派人的用户编号不能为空")
    private Long counterSignUserId;

    //委派人的用户名字
    @NotNull(message = "委派人的名字不能为空")
    private String counterSignUser;



}
