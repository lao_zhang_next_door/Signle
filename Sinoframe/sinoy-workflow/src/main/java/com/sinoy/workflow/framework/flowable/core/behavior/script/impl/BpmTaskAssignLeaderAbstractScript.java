package com.sinoy.workflow.framework.flowable.core.behavior.script.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.service.FrameDeptService;
import com.sinoy.platform.system.component.service.FrameUserService;
import com.sinoy.workflow.framework.flowable.core.behavior.script.BpmTaskAssignScript;
import com.sinoy.workflow.service.task.BpmProcessInstanceService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Set;

import static java.util.Collections.emptySet;
import static org.mapstruct.ap.internal.util.Collections.asSet;

/**
 * 分配给发起人的 Leader 审批的 Script 实现类
 * 目前 Leader 的定义是，
 *
 */
public abstract class BpmTaskAssignLeaderAbstractScript implements BpmTaskAssignScript {

    @Resource
    private FrameUserService adminUserApi;
    @Resource
    private FrameDeptService deptApi;
    @Resource
    @Lazy // 解决循环依赖
    private BpmProcessInstanceService bpmProcessInstanceService;

    protected Set<Long> calculateTaskCandidateUsers(DelegateExecution execution, int level) {
        Assert.isTrue(level > 0, "level 必须大于 0");
        // 获得发起人
        ProcessInstance processInstance = bpmProcessInstanceService.getProcessInstance(execution.getProcessInstanceId());
        Long startUserId = Long.valueOf(processInstance.getStartUserId());
        // 获得对应 leve 的部门
        FrameDept dept = null;
        for (int i = 0; i < level; i++) {
            // 获得 level 对应的部门
            if (dept == null) {
                dept = getStartUserDept(startUserId);
                if (dept == null) { // 找不到发起人的部门，所以无法使用该规则
                    return emptySet();
                }
            } else {
                FrameDept parentDept = deptApi.getOne(new LambdaQueryWrapper<FrameDept>().eq(FrameDept::getParentGuid,dept.getRowGuid()).eq(FrameDept::getDelFlag, DelFlag.Normal.getCode()));
                if (parentDept == null) { // 找不到父级部门，所以只好结束寻找。原因是：例如说，级别比较高的人，所在部门层级比较少
                    break;
                }
                dept = parentDept;
            }
        }
        return dept.getLeaderUserId() != null ? asSet(dept.getLeaderUserId()) : emptySet();
    }

    private FrameDept getStartUserDept(Long startUserId) {
        FrameUser startUser = adminUserApi.getOne(new LambdaQueryWrapper<FrameUser>().eq(FrameUser::getRowId,startUserId).eq(FrameUser::getDelFlag, DelFlag.Normal.getCode()));
        if (startUser.getDeptId() == null) { // 找不到部门，所以无法使用该规则
            return null;
        }
        return deptApi.getOne(new LambdaQueryWrapper<FrameDept>().eq(FrameDept::getRowId,startUser.getDeptId()).eq(FrameDept::getDelFlag, DelFlag.Normal.getCode()));
    }

}
