package com.sinoy.workflow.framework.flowable.core.behavior.script.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sinoy.core.common.enums.DelFlag;
import com.sinoy.core.common.utils.dataformat.collection.CollectionUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.database.mybatisplus.entity.BaseEntity;
import com.sinoy.platform.system.component.entity.FrameDept;
import com.sinoy.platform.system.component.entity.FrameUser;
import com.sinoy.platform.system.component.enumeration.DeptTypeEnum;
import com.sinoy.platform.system.component.service.FrameDeptService;
import com.sinoy.platform.system.component.service.FrameUserService;
import com.sinoy.workflow.enums.definition.BpmTaskRuleScriptEnum;
import com.sinoy.workflow.framework.flowable.core.behavior.script.BpmTaskAssignScript;
import com.sinoy.workflow.service.task.BpmProcessInstanceService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;

/**
 * 流程发起人所属单位(公司)的特定(角色) 审批
 *
 */
public abstract class BpmTaskAssignUnityRoleAbstractScript implements BpmTaskAssignScript {

    @Resource
    @Lazy // 解决循环依赖
    private BpmProcessInstanceService bpmProcessInstanceService;

    @Resource
    private FrameUserService frameUserService;

    @Resource
    private FrameDeptService deptService;

    public Set<Long> calculateTaskCandidateUsers(DelegateExecution execution,String roleFlag) {
        ProcessInstance processInstance = bpmProcessInstanceService.getProcessInstance(execution.getProcessInstanceId());
        Long startUserId = Long.valueOf(processInstance.getStartUserId());

        //发起人部门
        FrameDept startUserDept = getStartUserDept(startUserId);

        //获取发起人所属单位
        Optional.ofNullable(startUserDept).orElseThrow(() -> new BaseException("该发起人没有部门"));
        if(startUserDept == null){
            return emptySet();
        }

        String[] deptIds = startUserDept.getDeptCode().split("\\.");
        FrameDept belongOfDept = deptService.getOne(new LambdaQueryWrapper<FrameDept>()
                .eq(FrameDept::getDeptType, DeptTypeEnum.COMPANY.getCode())
                .in(FrameDept::getRowId,deptIds).orderByDesc(FrameDept::getDeptLevel).last(" limit 1"));

        //获取所属单位下 包括子单位 的拥有指定角色的人员
        List<FrameUser> userList = frameUserService.getUnityLeaderRole(belongOfDept.getDeptCode(),roleFlag);
        return userList.stream().map(user -> {
            return user.getRowId();
        }).collect(Collectors.toSet());
    }

    /**
     * 获取发起人部门
     * @param startUserId
     * @return
     */
    private FrameDept getStartUserDept(Long startUserId) {
        FrameUser startUser = frameUserService.getOne(new LambdaQueryWrapper<FrameUser>().eq(FrameUser::getRowId,startUserId).eq(FrameUser::getDelFlag, DelFlag.Normal.getCode()));
        if (startUser.getDeptId() == null) { // 找不到部门，所以无法使用该规则
            return null;
        }
        return deptService.getOne(new LambdaQueryWrapper<FrameDept>().eq(FrameDept::getRowId,startUser.getDeptId()).eq(FrameDept::getDelFlag, DelFlag.Normal.getCode()));
    }

}
