package com.sinoy.workflow.convert.oa;

import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeaveCreateReqVO;
import com.sinoy.workflow.controller.admin.oa.vo.BpmOALeaveRespVO;
import com.sinoy.workflow.dal.dataobject.oa.BpmOALeaveDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 请假申请 Convert
 */
@Mapper
public interface BpmOALeaveConvert {

    BpmOALeaveConvert INSTANCE = Mappers.getMapper(BpmOALeaveConvert.class);

    BpmOALeaveDO convert(BpmOALeaveCreateReqVO bean);

    BpmOALeaveRespVO convert(BpmOALeaveDO bean);

    List<BpmOALeaveRespVO> convertList(List<BpmOALeaveDO> list);

    PageResult<BpmOALeaveRespVO> convertPage(PageResult<BpmOALeaveDO> page);

}
