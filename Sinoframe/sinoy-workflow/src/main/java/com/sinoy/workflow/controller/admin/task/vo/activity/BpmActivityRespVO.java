package com.sinoy.workflow.controller.admin.task.vo.activity;

import lombok.Data;

import java.util.Date;

/**
 * 流程活动的Response VO
 */
@Data
public class BpmActivityRespVO {

    //流程活动的标识
    private String key;

    //流程活动的类型
    private String type;

    //流程活动的开始时间
    private Date startTime;

    //流程活动的结束时间
    private Date endTime;

    //关联的流程任务的编号  关联的流程任务，只有 UserTask 等类型才有
    private String taskId;

}
