package com.sinoy.workflow.dal.mysql.definition;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.database.mybatisplus.base.mapper.ExtendMapper;
import com.sinoy.core.database.mybatisplus.base.query.LambdaQueryWrapperX;
import com.sinoy.workflow.controller.admin.definition.vo.group.BpmUserGroupPageReqVO;
import com.sinoy.workflow.dal.dataobject.definition.BpmUserGroupDO;
import com.sinoy.workflow.dal.dataobject.task.BpmProcessInstanceExtDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户组 Mapper
 *
 */
public interface BpmUserGroupMapper extends ExtendMapper<BpmUserGroupDO> {

    default Page<BpmUserGroupDO> selectPage(BpmUserGroupPageReqVO reqVO) {
        Page<BpmUserGroupDO> page = new Page<>(reqVO.getCurrentPage(), reqVO.getPageSize());
        return selectPage(page,new LambdaQueryWrapperX<BpmUserGroupDO>()
                .likeIfPresent(BpmUserGroupDO::getName, reqVO.getName())
                .eqIfPresent(BpmUserGroupDO::getStatus, reqVO.getStatus())
                .betweenIfPresent(BpmUserGroupDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(BpmUserGroupDO::getRowId));
    }

    default List<BpmUserGroupDO> selectListByStatus(Integer status) {
        return selectList(BpmUserGroupDO::getStatus, status);
    }

}
