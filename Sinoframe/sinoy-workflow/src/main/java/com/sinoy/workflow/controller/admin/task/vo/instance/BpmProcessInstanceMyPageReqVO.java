package com.sinoy.workflow.controller.admin.task.vo.instance;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 流程实例的分页 Item Response VO
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmProcessInstanceMyPageReqVO extends PageParam {

    //流程名称
    private String name;

    //流程定义的编号
    private String processDefinitionId;

    //流程实例的状态  bpm_process_instance_status
    private Integer status;

    //流程实例的结果  bpm_process_instance_result
    private Integer result;

    //流程分类  bpm_model_category
    private Integer category;

    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date[] createTime;

}
