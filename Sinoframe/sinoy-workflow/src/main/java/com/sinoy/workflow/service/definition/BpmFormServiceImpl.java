package com.sinoy.workflow.service.definition;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.dataformat.JsonUtils;
import com.sinoy.core.common.utils.exception.BaseException;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.database.mybatisplus.base.service.BaseServiceImpl;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormCreateReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormPageReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormUpdateReqVO;
import com.sinoy.workflow.convert.definition.BpmFormConvert;
import com.sinoy.workflow.dal.dataobject.definition.BpmFormDO;
import com.sinoy.workflow.dal.mysql.definition.BpmFormMapper;
import com.sinoy.workflow.enums.ErrorCodeConstants;
import com.sinoy.workflow.enums.definition.BpmModelFormTypeEnum;
import com.sinoy.workflow.service.definition.dto.BpmFormFieldRespDTO;
import com.sinoy.workflow.service.definition.dto.BpmModelMetaInfoRespDTO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Pattern;


/**
 * 动态表单 Service 实现类
 */
@Service
@Validated
public class BpmFormServiceImpl extends BaseServiceImpl<BpmFormMapper,BpmFormDO> implements BpmFormService {

    @Resource
    private BpmFormMapper formMapper;

    @Override
    public Long createForm(BpmFormCreateReqVO createReqVO) {
        this.checkFields(createReqVO.getFields());
        // 插入
        BpmFormDO form = BpmFormConvert.INSTANCE.convert(createReqVO);
        formMapper.insert(form);
        // 返回
        return form.getRowId();
    }

    @Override
    public void updateForm(BpmFormUpdateReqVO updateReqVO) {
        this.checkFields(updateReqVO.getFields());
        // 校验存在
        this.validateFormExists(updateReqVO.getRowId());
        // 更新
        BpmFormDO updateObj = BpmFormConvert.INSTANCE.convert(updateReqVO);
        formMapper.updateById(updateObj);
    }

    @Override
    public void deleteForm(Long id) {
        // 校验存在
        this.validateFormExists(id);
        // 删除
        formMapper.deleteById(id);
    }

    private void validateFormExists(Long id) {
        if (formMapper.selectById(id) == null) {
            throw new BaseException(ErrorCodeConstants.FORM_NOT_EXISTS);
        }
    }

    @Override
    public BpmFormDO getForm(Long id) {
        return formMapper.selectById(id);
    }

    @Override
    public List<BpmFormDO> getFormList() {
        return formMapper.selectList();
    }

    @Override
    public List<BpmFormDO> getFormList(Collection<Long> ids) {
        return formMapper.selectBatchIds(ids);
    }

    @Override
    public Page<BpmFormDO> getFormPage(BpmFormPageReqVO pageReqVO) {
        return formMapper.selectPage(pageReqVO);
    }


    @Override
    public BpmFormDO checkFormConfig(String configStr) {
        BpmModelMetaInfoRespDTO metaInfo = JsonUtils.parseObject(configStr, BpmModelMetaInfoRespDTO.class);
        if (metaInfo == null || metaInfo.getFormType() == null) {
            throw new BaseException( ErrorCodeConstants.MODEL_DEPLOY_FAIL_FORM_NOT_CONFIG);
        }
        // 校验表单存在
        if (Objects.equals(metaInfo.getFormType(), BpmModelFormTypeEnum.NORMAL.getType())) {
            BpmFormDO form = getForm(metaInfo.getFormId());
            if (form == null) {
                throw new BaseException(ErrorCodeConstants.FORM_NOT_EXISTS);
            }
            return form;
        }
        return null;
    }

    private void checkKeyNCName(String key) {
        if(!StringUtils.hasText(key)
                && Pattern.compile("[a-zA-Z_][\\-_.0-9_a-zA-Z$]*").matcher(key).matches()){
            throw new BaseException(ErrorCodeConstants.MODEL_KEY_VALID);
        }
    }
    /**
     * 校验 Field，避免 field 重复
     *
     * @param fields field 数组
     */
    private void checkFields(List<String> fields) {
        Map<String, String> fieldMap = new HashMap<>(); // key 是 vModel，value 是 label
        for (String field : fields) {
            BpmFormFieldRespDTO fieldDTO = JsonUtils.parseObject(field, BpmFormFieldRespDTO.class);
            Assert.notNull(fieldDTO);
            String oldLabel = fieldMap.put(fieldDTO.getVModel(), fieldDTO.getLabel());
            // 如果不存在，则直接返回
            if (oldLabel == null) {
                continue;
            }
            // 如果存在，则报错
            throw new BaseException(ErrorCodeConstants.FORM_FIELD_REPEAT, oldLabel, fieldDTO.getLabel(), fieldDTO.getVModel());
        }
    }

}
