package com.sinoy.workflow.controller.app.task;

import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.service.task.BpmActivityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 流程活动实例
 */
@RestController
@RequestMapping("/bpm/activity")
@Validated
public class AppBpmActivityController {

    @Resource
    private BpmActivityService activityService;

    /**
     * 生成指定流程实例的高亮流程图   只高亮进行中的任务。不过要注意，该接口暂时没用，
     * 通过前端的 ProcessViewer.vue 界面的 highlightDiagram 方法生成
     * @param processInstanceId  流程实例的编号
     * @return
     */
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermission('bpm:task:query')")
    public R getActivityList(
            @RequestParam("processInstanceId") String processInstanceId) {
        return R.ok().put("data",activityService.getActivityListByProcessInstanceId(processInstanceId));
    }
}
