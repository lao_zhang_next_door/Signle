package com.sinoy.workflow.controller.admin.definition.vo.model;

import com.sinoy.core.common.utils.request.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmModelPageReqVO extends PageParam {

    //标识
    private String key;

    //名字
    private String name;

    //流程分类
    private String category;

}
