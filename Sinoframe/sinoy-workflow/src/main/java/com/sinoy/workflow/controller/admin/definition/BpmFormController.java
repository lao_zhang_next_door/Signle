package com.sinoy.workflow.controller.admin.definition;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinoy.core.common.utils.request.PageResult;
import com.sinoy.core.common.utils.request.R;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormCreateReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormPageReqVO;
import com.sinoy.workflow.controller.admin.definition.vo.form.BpmFormUpdateReqVO;
import com.sinoy.workflow.convert.definition.BpmFormConvert;
import com.sinoy.workflow.dal.dataobject.definition.BpmFormDO;
import com.sinoy.workflow.service.definition.BpmFormService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 动态表单
 */
@RestController
@RequestMapping("/bpm/form")
@Validated
public class BpmFormController {

    @Resource
    private BpmFormService formService;

    /**
     * 创建动态表单
     * @param createReqVO
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("@ss.hasPermission('bpm:form:create')")
    public R createForm(@Valid @RequestBody BpmFormCreateReqVO createReqVO) {
        return R.ok().put("data",formService.createForm(createReqVO));
    }

    /**
     * 更新动态表单
     * @param updateReqVO
     * @return
     */
    @PutMapping("/update")
    @PreAuthorize("@ss.hasPermission('bpm:form:update')")
    public R updateForm(@Valid @RequestBody BpmFormUpdateReqVO updateReqVO) {
        formService.updateForm(updateReqVO);
        return R.ok();
    }

    /**
     * 删除动态表单
     * @param id 编号
     * @return
     */
    @DeleteMapping("/delete")
    @PreAuthorize("@ss.hasPermission('bpm:form:delete')")
    public R deleteForm(@RequestParam("id") Long id) {
        formService.deleteForm(id);
        return R.ok();
    }

    /**
     * 获得动态表单
     * @param id 编号
     * @return
     */
    @GetMapping("/get")
    @PreAuthorize("@ss.hasPermission('bpm:form:query')")
    public R getForm(@RequestParam("id") Long id) {
        BpmFormDO form = formService.getForm(id);
        return R.ok().put("data", BpmFormConvert.INSTANCE.convert(form));
    }

    /**
     * 获得动态表单的精简列表  用于表单下拉框
     * @return
     */
    @GetMapping("/list-all-simple")
    public R getSimpleForms() {
        List<BpmFormDO> list = formService.getFormList();
        return R.ok().put("data",BpmFormConvert.INSTANCE.convertList2(list));
    }

    /**
     * 获得动态表单分页
     * @param pageVO
     * @return
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('bpm:form:query')")
    public R getFormPage(@Valid BpmFormPageReqVO pageVO) {
        Page<BpmFormDO> pageResult = formService.getFormPage(pageVO);
        return R.ok().put("data",BpmFormConvert.INSTANCE.convertPage(pageResult));
    }

}
