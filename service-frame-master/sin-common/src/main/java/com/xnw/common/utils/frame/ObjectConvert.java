package com.xnw.information.basic.utils.frame;

import com.alibaba.fastjson2.JSON;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ObjectConvert {

    /**
     * Object转换为 基本数据类型int （int占4字节，32位）
     *
     * @param object
     * @return
     */
    public static int toInt32(Object object) {
        return toInteger(object);
    }

    /**
     * Object转换为 包装类Integer
     *
     * @param object
     * @return
     */
    public static Integer toInteger(Object object) {
        if (object == null) {
            return 0;
        }
        try {
            return Integer.parseInt(object.toString());
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     * Object转换为 64位 long
     *
     * @param object
     * @return
     */
    public static long toInt64(Object object) {
        return toLong(object);
    }

    /**
     * Object转换为 long
     *
     * @param object
     * @return
     */
    public static Long toLong(Object object) {
        if (object == null) {
            return 0L;
        }
        try {
            return Long.parseLong(object.toString());
        } catch (Exception ex) {
            return 0L;
        }
    }

    /**
     * Object转换为 基本数据类型double
     *
     * @param object
     * @return
     */
    public static double toDoubled(Object object) {
        return toDouble(object);
    }

    /**
     * Object转换为 包装类Double
     *
     * @param object
     * @return
     */
    public static Double toDouble(Object object) {
        if (object == null) {
            return 0d;
        }
        try {
            return Double.parseDouble(object.toString());
        } catch (Exception ex) {
            return 0d;
        }
    }

    /**
     * Object转换为 boolean
     *
     * @param object
     * @return
     */
    public static boolean toBoolenb(Object object) {
        return toBoolen(object);
    }

    /**
     * Object转换为 包装类Boolean
     *
     * @param object
     * @return
     */
    public static Boolean toBoolen(Object object) {
        if (object == null) {
            return false;
        }
        try {
            return Boolean.parseBoolean(object.toString());
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Object转换为 float
     *
     * @param object
     * @return
     */
    public static float toFloatf(Object object) {
        return toFloat(object);
    }

    /**
     * Object转换为 包装类Float
     *
     * @param object
     * @return
     */
    public static Float toFloat(Object object) {
        if (object == null) {
            return 0f;
        }
        try {
            return Float.parseFloat(object.toString());
        } catch (Exception ex) {
            return 0f;
        }
    }

    /**
     * Object转换为 BigDecimal
     *
     * @param object
     * @return
     */
    public static BigDecimal toDecimal(Object object) {
        BigDecimal bd = new BigDecimal("0");
        if (object == null) {
            return bd;
        }
        try {
            bd = new BigDecimal(object.toString());
            return bd;
        } catch (Exception ex) {
            return bd;
        }
    }

    /**
     * Object转换为 Date
     *
     * @param object
     * @return
     */
    public static Date toDate(Object object) {
        if (object == null) {
            return null;
        }
        try {
            if (object instanceof Date) {
                return (Date) object;
            }
            return new Date((Long) object);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * object按数据样式转换日期时间
     *
     * @param object
     * @param dateFormat 日期类型 如：yyyy-MM-dd
     * @return
     */
    public static Date toDate(Object object, String dateFormat) {
        if (object == null) {
            return null;
        }
        try {
            if (object instanceof Date) {
                return (Date) object;
            }
            SimpleDateFormat df = new SimpleDateFormat(dateFormat);
            return df.parse(object.toString());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Object转换为 想转换的类型泛型<T>
     *
     * @param object
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T convertType(Object object, Class<T> tClass) {
        return convertType(object, tClass, false);
    }

    /**
     * Object转换为 想转换的常见类型 泛型<T>
     *
     * @param object
     * @param tClass
     * @param isAll
     * @param <T>
     * @return
     */
    public static <T> T convertType(Object object, Class<T> tClass, boolean isAll) {
        Object retObject = null;
        if (object == null) {
            return null;
        }
        if (tClass == String.class) {
            retObject = object.toString();
            return (T) retObject;
        }
        if (tClass == boolean.class) {
            retObject = toBoolenb(object);
            return (T) retObject;
        } else if (tClass == Boolean.class) {
            retObject = toBoolen(object);
            return (T) retObject;
        } else if (tClass == int.class) {
            retObject = toInt32(object);
            return (T) retObject;
        } else if (tClass == Integer.class) {
            retObject = toInteger(object);
            return (T) retObject;
        } else if (tClass == long.class) {
            retObject = toInt64(object);
            return (T) retObject;
        } else if (tClass == Long.class) {
            retObject = toLong(object);
            return (T) retObject;
        } else if (tClass == double.class) {
            retObject = toDoubled(object);
            return (T) retObject;
        } else if (tClass == Double.class) {
            retObject = toDouble(object);
            return (T) retObject;
        } else if (tClass == float.class) {
            retObject = toFloatf(object);
            return (T) retObject;
        } else if (tClass == Float.class) {
            retObject = toFloat(object);
            return (T) retObject;
        } else if (tClass == Date.class) {
            retObject = toDate(object);
            return (T) retObject;
        } else if (tClass == BigDecimal.class) {
            retObject = toDecimal(object);
            return (T) retObject;
        }
        if (isAll) {
            if (tClass.isInstance(object)) {
                return (T) object;
            }
            String json = JSON.toJSONString(object);
            return jsonConvert(json, tClass);
        }
        return null;
    }

    public static <T> T jsonConvert(String json, Class<T> tClass) {
        return JSON.parseObject(json, tClass);
    }

    public static <T, K> K entityConvert(T entity, Class<K> tClass) {
        String json = JSON.toJSONString(entity);
        return jsonConvert(json, tClass);
    }

    /**
     * 判断是否为常见的基本数据类型
     *
     * @param clz
     * @return
     */
    public static boolean isBaseType(Class clz) {
        return clz == float.class || clz == Float.class ||
                clz == int.class || clz == Integer.class ||
                clz == long.class || clz == Long.class ||
                clz == boolean.class || clz == Boolean.class ||
                clz == Date.class ||
                clz == BigDecimal.class;
    }

    public static boolean isBaseType(Class clz, boolean hasString) {
        if (hasString) {
            return isBaseType(clz) || clz == String.class;
        }
        return isBaseType(clz);
    }

    public static boolean isNoneBaseType(Class clz) {
        return clz == String.class ||
                clz == Float.class ||
                clz == Integer.class ||
                clz == Long.class ||
                clz == Boolean.class ||
                clz == Date.class ||
                clz == BigDecimal.class;
    }

    /**
     * 泛型数组转为List
     *
     * @param array
     * @param <T>
     * @return
     */
    public static <T> List<T> arrayToList(T[] array) {
        if (array == null) {
            return new ArrayList<T>();
        }
        return new ArrayList<T>(Arrays.asList(array));
    }

    public static <K, T> List<T> toSelect(List<K> ke, Function<K, T> mapper) {
        return ke.stream().map(mapper).collect(Collectors.toList());
    }


    public static <K> List<K> filter(List<K> ke, Predicate<? super K> predicate) {
        return ke.stream().filter(predicate).collect(Collectors.toList());
    }


    public static <K> List<K> sorted(List<K> ke, Comparator<? super K> predicate) {
        return ke.stream().sorted(predicate).collect(Collectors.toList());
    }

    /**
     * 用newInstance构造一个指定对象
     *
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T newEntity(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    /**
     * 将List转化为 泛型 只能获取第一个
     *
     * @param array
     * @param <T>
     * @return
     */
    public static <T> T listDefault(List<T> array) {
        if (array.size() > 0) {
            return array.get(0);
        }
        return null;
    }
}
