package com.xnw.common.utils.exception;

import com.xnw.common.utils.frame.R;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 统一异常处理类
 *
 * @author T470
 * @ClassName: GlobalExceptionHandler
 * @Description: 统一异常处理类
 * @date 2018年11月8日 上午10:31:14
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(BaseException.class)
    public R handleMyException(BaseException e) {
        R r = new R();
        r.put("code", e.getStatus());
        r.put("msg", e.getMsg());
        r.put("detail", e.getMessage());
        return r;
    }

}
