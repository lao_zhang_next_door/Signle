package com.xnw.common.utils.frame;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <p>Title: StringUtil</p>
 * <p>Description: string工具类</p>
 *
 * @author hero
 */
public class StringUtil {


    /**
     * @param
     * @author jd
     * @Description 首字母转小写
     * @date 2017-2-16 下午3:40:32
     */
    public static String toLowerCaseFirstChar(String s) {
        if (s == null || "".equals(s)) {
            return null;
        }
        return s.replaceFirst(s.substring(0, 1), s.substring(0, 1).toLowerCase());
    }


    /**
     * @param
     * @author jd
     * @Description 首字母转大写
     * @date 2017-2-16 下午3:40:42
     */
    public static String toUpCaseFirstChar(String s) {
        if (s == null || "".equals(s)) {
            return null;
        }
        return s.replaceFirst(s.substring(0, 1), s.substring(0, 1).toUpperCase());
    }

    public static String fromDateH(Date date) {
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format1.format(date);
    }

    /**
     * @param
     * @author jd
     * @Description 验证字符串是否为空
     * @date 2017-2-16 下午3:40:52
     */
    public static boolean isEmpty(String s) {
        return s == null || "".equals(s.trim());
    }

    public static boolean isNullOrEmpty(Object obj) {
        if (obj == null)
            return true;

        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        }

        if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        }

        if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        }

        if (obj instanceof Object[]) {
            Object[] object = (Object[]) obj;
            if (object.length == 0) {
                return true;
            }
            boolean empty = true;
            for (Object o : object) {
                if (!isNullOrEmpty(o)) {
                    empty = false;
                    break;
                }
            }
            return empty;
        }
        return false;
    }

    /**
     * @param
     * @author jd
     * @Description 不足位数，左侧补0
     * @date 2017-3-9 上午8:44:07
     */
    public static String flushLeft(int length, String content) {
        char c = '0';
        String str = "";
        StringBuilder cs = new StringBuilder();
        if (content.length() > length) {
            str = content;
        } else {
            for (int i = 0; i < length - content.length(); i++) {
                cs.append(c);
            }
        }
        str = cs + content;
        return str;
    }

    /**
     * @param
     * @author jd
     * @Description 左侧补足十位
     * @date 2017年7月10日 上午11:20:44
     */
    public static String flushTenLeft(String content) {
        return flushLeft(10, content);
    }

    /**
     * @param
     * @author jd
     * @Description 价格补位 首位1+0000
     * @date 2017年7月11日 上午11:13:50
     */
    public static String flushPrice(String content) {
        return "1" + flushLeft(10, content);
    }


    /**
     * @param
     * @author jd
     * @Description list<string> 转成串，用逗号隔开
     * @date 2017-3-10 上午10:39:34
     */
    public static String listToStr(List<String> sList) {
        StringBuilder str = new StringBuilder();
        if (sList != null && sList.size() > 0) {
            for (String s : sList) {
                str.append(s).append(",");
            }
            if (str.toString().endsWith(",")) {
                str = new StringBuilder(str.substring(0, str.length() - 1));
            }
        }
        return str.toString();
    }

    /**
     * @param
     * @author jd
     * @Description 时间格式去掉最后的.0
     * @date 2017-5-2 下午4:02:07
     */
    public static String getStandDate(String date) {
        if (date != null && date.contains(".0")) {
            date = date.replace(".0", "");
        }
        return date;
    }

    public static boolean isBlank(CharSequence cs) {
        int len = 0;
        if (cs == null || (len = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < len; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String camelize(String tabAttr) {
        if (isBlank(tabAttr))
            return tabAttr;
        Pattern pattern = Pattern.compile("(.*)_(\\w)(.*)");
        Matcher matcher = pattern.matcher(tabAttr);
        if (matcher.find()) {
            return camelize(matcher.group(1) + matcher.group(2).toUpperCase() + matcher.group(3));
        } else {
            return tabAttr;
        }
    }

    /**
     * 驼峰式的实体类属性名转换为数据表字段名
     *
     * @param camelCaseStr 驼峰式的实体类属性名
     * @return 转换后的以"_"分隔的数据表字段名
     */
    public static String deCamelize(String camelCaseStr) {
        return isBlank(camelCaseStr) ? camelCaseStr : camelCaseStr.replaceAll("[A-Z]", "_$0").toLowerCase();
    }

    public static String urlEncode(String param) {
        try {
            return URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String urlDecode(String param) {
        try {
            return URLDecoder.decode(param, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }


    public static boolean isNotBlank(CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(flushLeft(10, "aaafaa"));
    }

}
