package com.xnw.common.utils.request;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.common.network.HttpUtil;

/**
 * IP工具类
 *
 * @author hero
 */
public class IPUtil {

    private final static boolean IP_LOCAL = true;

    /**
     * 根据ip获取详细地址
     */
    public static String getCityInfo(String ip) {
        if (IP_LOCAL) {
            //待开发
            return null;
        } else {
            return getHttpCityInfo(ip);
        }
    }

    /**
     * 根据ip获取详细地址
     * 临时使用，待调整
     */
    public static String getHttpCityInfo(String ip) {
        String api = String.format("http://whois.pconline.com.cn/ipJson.jsp?ip=%s&json=true", ip);
        JSONObject object = JSON.parseObject((String) HttpUtil.getRequest(api, "gbk"));
        return object.getString("addr");
    }

}
