package com.xnw.information.basic.utils.aop;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.javassist.ClassClassPath;
import org.apache.ibatis.javassist.ClassPool;
import org.apache.ibatis.javassist.CtClass;
import org.apache.ibatis.javassist.CtMethod;
import org.apache.ibatis.javassist.bytecode.CodeAttribute;
import org.apache.ibatis.javassist.bytecode.LocalVariableAttribute;
import org.apache.ibatis.javassist.bytecode.MethodInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/**
 * @author wzl
 */
public class AopStaticFunction {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 属性字段名、值、数据类型
     */
    public static JSONArray getFieldValueByName(Object object) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Method[] methods = object.getClass().getDeclaredMethods();
        JSONArray jsonArray = new JSONArray();
        if (methods.length > 0) {
            for (Method method : methods) {
                JSONObject jsonObject = new JSONObject();
                String methodName = method.getName();
                jsonObject.put("methodName", methodName);
                if (methodName.startsWith("get")) {
                    Object value = method.invoke(object);
                    jsonObject.put("value", value);
                    //String setMethodName = methodName.replaceFirst("(get)", "set");
                    //Method setMethod = object.getClass().getMethod(setMethodName,
                    //method.getReturnType());
                    //setMethod.invoke(target, value);
                }
                jsonArray.add(jsonObject);
            }
        } else {
            Class<?> type = object.getClass();
            if (type.isArray()) {
                JSONObject jsonObject = new JSONObject();
                Class<?> elementType = type.getComponentType();
                int length = Array.getLength(object);
                for (int i = 0; i < length; i++) {
                    Object value = Array.get(object, i);
                    jsonObject.put("Position-" + i, value);
                }
                jsonObject.put("Array", elementType);
                jsonObject.put("Length", Array.getLength(object));
                jsonArray.add(jsonObject);
            }
        }

        return jsonArray;
    }

    public static String throwChooseId(String choose, String params) {
        JSONArray jsonArray = JSONArray.parseArray(params);
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (jsonObject.get("methodName").equals(choose)) {
                return (String) jsonObject.get("value");
            }
        }
        return "";
    }

    /**
     * 使用javassist来获取方法参数名称
     *
     * @param className  类名
     * @param methodName 方法名
     * @return
     * @throws Exception
     */
    public static String[] getFieldsName(String className, String methodName) throws Exception {
        Class<?> clazz = Class.forName(className);
        String clazzName = clazz.getName();
        ClassPool pool = ClassPool.getDefault();
        ClassClassPath classPath = new ClassClassPath(clazz);
        pool.insertClassPath(classPath);

        CtClass ctClass = pool.get(clazzName);
        CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
        MethodInfo methodInfo = ctMethod.getMethodInfo();
        CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
        LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
        if (attr == null) {
            return null;
        }
        String[] paramsArgsName = new String[ctMethod.getParameterTypes().length];
        int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
        for (int i = 0; i < paramsArgsName.length; i++) {
            paramsArgsName[i] = attr.variableName(i + pos);
        }
        return paramsArgsName;
    }

    /**
     * 判断是否为基本类型：包括String
     *
     * @param clazz clazz
     * @return true：是;     false：不是
     */
    private static boolean isPrimite(Class<?> clazz) {
        return clazz.isPrimitive() || clazz == String.class;
    }

    /**
     * 打印方法参数值  基本类型直接打印，非基本类型需要重写toString方法
     *
     * @param paramsArgsName  方法参数名数组
     * @param paramsArgsValue 方法参数值数组
     */
    public static String logParam(String[] paramsArgsName, Object[] paramsArgsValue) throws Exception {
        if (ArrayUtils.isEmpty(paramsArgsName) || ArrayUtils.isEmpty(paramsArgsValue)) {
            return "该方法没有参数";
        }
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < paramsArgsName.length; i++) {
            //参数名 默认cookie
            String name = paramsArgsName[0];
            //参数值 默认cookie
            Object value = paramsArgsValue[0];
            //buffer.append(name).append(" = ");
            if (isPrimite(value.getClass())) {
                //System.out.println(true);
                buffer.append(value).append("  ,");
            } else {
                //System.out.println(false);
                JSONArray jsonArray = getFieldValueByName(value);
                buffer.append(jsonArray.toString());

            }
            break;
        }
        return buffer.toString();
    }

    public static HashMap objectToMap(Object object) {
        return JSONObject.parseObject(JSONObject.toJSONString(object), HashMap.class);
    }

}
