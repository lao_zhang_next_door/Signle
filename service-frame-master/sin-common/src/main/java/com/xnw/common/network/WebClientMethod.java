package com.xnw.common.network;

public enum WebClientMethod {
    POST, GET, PUT, DELETE
}
