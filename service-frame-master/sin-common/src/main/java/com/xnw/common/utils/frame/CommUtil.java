package com.xnw.common.utils.frame;


import com.xnw.information.basic.utils.frame.ApplicationUtils;

/**
 * 通用工具类
 *
 * @author lim
 * @date 2020-04-13 16:23
 */
public class CommUtil {

    /**
     * 获取uuid
     *
     * @return
     */
    public static String getUUID() {
        return java.util.UUID.randomUUID().toString();
    }

    public static String getPackageName() {
        String domain = ApplicationUtils.getApplicationProperties("spring.jmx.default-domain");
        if (StringUtil.isEmpty(domain)) {
            return "";
        }
        return domain;
    }

    public static String getRedisKey(String key) {
        return getPackageName() + ":" + key;
    }

    private final static String[] AGENT = {"Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser"};

    /**
     * 判断User-Agent 是不是来自于手机
     *
     * @param ua
     * @return
     */
    public static boolean checkAgentIsMobile(String ua) {
        boolean flag = false;
        if (!ua.contains("Windows NT") || (ua.contains("Windows NT") && ua.contains("compatible; MSIE 9.0;"))) {
            if (!ua.contains("Windows NT") && !ua.contains("Macintosh")) {
                for (String item : AGENT) {
                    if (ua.contains(item)) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        return flag;
    }
}
