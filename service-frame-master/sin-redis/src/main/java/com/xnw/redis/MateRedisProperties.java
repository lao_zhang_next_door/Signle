package com.xnw.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * redis配置
 *
 * @author pangu
 */
@ConfigurationProperties(MateRedisProperties.PREFIX)
public class MateRedisProperties {
	/**
	 * 前缀
	 */
	public static final String PREFIX = "mate.lettuce.redis";
	/**
	 * 是否开启Lettuce
	 */
	private Boolean enable = true;

	public static String getPREFIX() {
		return PREFIX;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
}
