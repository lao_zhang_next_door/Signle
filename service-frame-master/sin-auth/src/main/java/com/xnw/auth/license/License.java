package com.xnw.auth.license;

import com.xnw.common.utils.dataformat.FileUtil;
import com.xnw.common.utils.frame.CommUtil;
import com.xnw.common.utils.frame.SpringContextUtils;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.information.basic.utils.frame.ApplicationUtils;
import com.xnw.redis.RedisService;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Set;

/**
 * 注册码
 *
 * @author hero
 */
public class License {

    private static final String FILE_POSTFIX = ".lic";
    private static boolean isAuth = false;
    private static final RedisService redisService = SpringContextUtils.getBean(RedisService.class);
    private static final String LICENSE_PATH = ApplicationUtils.getApplicationProperties("authFile");

    public static String getAuthPath() {
        String packageName = CommUtil.getPackageName();
        if (LICENSE_PATH.endsWith(FILE_POSTFIX)) {
            return LICENSE_PATH.replaceFirst(FILE_POSTFIX, "-" + packageName + FILE_POSTFIX);
        }
        return LICENSE_PATH + "auth-" + packageName + FILE_POSTFIX;
    }

    /**
     * 获取第一个mac地址（最快）
     *
     * @return
     * @throws Exception
     */
    public static String getLocalMacAddress() {
        Enumeration<NetworkInterface> ni = null;
        try {
            ni = NetworkInterface.getNetworkInterfaces();

            while (ni.hasMoreElements()) {
                NetworkInterface netI = ni.nextElement();

                byte[] bytes = netI.getHardwareAddress();
                if (netI.isUp() && bytes != null && bytes.length == 6) {
                    return getByteString(bytes);
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getPackageAddress() {

        String packageName = CommUtil.getPackageName();
        byte[] bytes = packageName.getBytes();
        return getByteString(bytes);
    }

    private static String getByteString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(Integer.toHexString((b & 240) >> 4));
            sb.append(Integer.toHexString(b & 15));
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString().toUpperCase();
    }

    private static String getLocalAuthData() {
        return FileUtil.readFile(getAuthPath(), false);
    }

    public static String saveTip() {
        String packageName = CommUtil.getPackageName();
        return "packageName:" + packageName + ":";
    }

    public static void saveAuthData() {
        String auth = getLocalAuthData();
        if (!StringUtil.isEmpty(auth)) {
            String saveTips = saveTip();
            String[] localSaveds = auth.split(",");
            for (String ls : localSaveds) {
                if (!StringUtil.isEmpty(ls)) {
                    String data = EncodingUtils.decrypt(ls);
                    String[] ar = data.split(",", -1);
                    String packageName = getPackageAddress();
                    if (ar[0].startsWith(packageName)) {
                        String mac = ar[0].replaceFirst(packageName, "");
                        redisService.set(CommUtil.getRedisKey("auth_data_" + mac), EncodingUtils.encrypt(saveTips + data));
                    }
                }
            }
        }
    }


    public static void saveMac() {
        String mac = getLocalMacAddress();
        if (!StringUtil.isEmpty(mac)) {
            String saveTips = saveTip();
            redisService.set(CommUtil.getRedisKey("auth_mac_" + mac), EncodingUtils.encrypt(saveTips + mac));
        }
    }


    /**
     * 验证注册码是否正确
     *
     * @return
     */
    public static boolean checkCode() {
        if (isAuth) {
            return true;
        }
        String mac = getLocalMacAddress();
        if (isTrueMac(mac)) {
            Set<Object> keys = redisService.sGet(CommUtil.getRedisKey("auth_data_*"));
            for (Object key : keys) {
                if (isTrueAuth(key.toString())) {
                    String data = redisService.get(key.toString()).toString();
                    data = EncodingUtils.decrypt(data);
                    String[] ar = data.split(",", -1);
                    if (ar.length < 2) {
                        continue;
                    }
                    if (!StringUtil.isEmpty(ar[1])) {
//                        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//                        if (LocalDateTime.parse(ar[1], df).compareTo(LocalDateTime.now()) > 0) {
//                            continue;
//                        }
                        isAuth = true;
                        return true;
                    } else {
                        isAuth = true;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static boolean isTrueMac(String mac) {
        String saveTips = saveTip();
        if (redisService.exists(CommUtil.getRedisKey("auth_mac_" + mac))) {
            String saveNowMac = redisService.get(CommUtil.getRedisKey("auth_mac_" + mac)).toString();
            saveNowMac = EncodingUtils.decrypt(saveNowMac);
            if (saveNowMac.startsWith(saveTips)) {
                saveNowMac = saveNowMac.replaceFirst(saveTips, "");
                return saveNowMac.equals(mac);
            }
        }
        return false;
    }


    private static boolean isTrueAuth(String authKey) {
        String saveTips = saveTip();
        if (redisService.exists(authKey)) {
            String saveNowAuth = redisService.get(authKey).toString();
            saveNowAuth = EncodingUtils.decrypt(saveNowAuth);
            if (saveNowAuth.startsWith(saveTips)) {
                saveNowAuth = saveNowAuth.replaceFirst(saveTips, "");
                String mac = saveNowAuth.split(",")[0];
                String packageName = getPackageAddress();
                if (mac.startsWith(packageName)) {
                    mac = mac.replaceFirst(packageName, "");
                    return authKey.endsWith("_" + mac) && isTrueMac(mac);
                }
            }
        }
        return false;
    }
}
