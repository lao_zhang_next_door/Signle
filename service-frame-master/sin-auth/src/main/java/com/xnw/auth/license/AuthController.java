package com.xnw.auth.license;


import com.xnw.auth.token.PassToken;
import com.xnw.common.utils.dataformat.FileUtil;
import com.xnw.common.utils.frame.R;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.information.basic.utils.frame.ApplicationUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;


/**
 * <p>Title: AuthController</p>
 * <p>Description: 使用权限控制器</p>
 *
 * @author hero
 */

@CrossOrigin
@Tag(name = "使用权限控制器")
@RestController
@RequestMapping("com/xnw/frame/auth")
public class AuthController {

    private static final String REGISTER_HTML = ApplicationUtils.loadApplicationResource("/values/auth.html");

    /**
     * 注册
     *
     * @param params
     * @return
     * @throws IOException
     */
    @PassToken
    @Operation(summary = "服务器注册")
    @ResponseBody
    @RequestMapping(value = "/authByLicense", method = RequestMethod.POST)
    public R authByLicense(@RequestParam Map<String, String> params) throws IOException {

        String authFile = License.getAuthPath();
        //生成本地对应文件
        File targetFile = new File(authFile);

        if (!targetFile.exists()) {
            targetFile.getParentFile().mkdirs();
            targetFile.createNewFile();
        }

        String keyData = params.get("keyData");
        String authString = FileUtil.readFile(authFile, false);
        if (StringUtil.isEmpty(authString)) {
            authString = keyData;
        } else {
            authString += "," + keyData;
        }
        FileWriter writer = new FileWriter(targetFile, false);
        writer.write(authString);
        writer.close();
        License.saveAuthData();
        return R.ok();
    }

    @ResponseBody
    @RequestMapping(value = "/checkAuth", method = RequestMethod.GET)
    public R checkAuth() {
        return R.ok();
    }

    @PassToken
    @ResponseBody
    @RequestMapping(value = "/check/register", method = RequestMethod.GET)
    public R register(HttpServletResponse response) throws IOException {
        String mac = License.getLocalMacAddress();
        String packageName = License.getPackageAddress();
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().print(REGISTER_HTML.replace("@mac", packageName + mac));
        response.flushBuffer();
        return null;
    }

}


