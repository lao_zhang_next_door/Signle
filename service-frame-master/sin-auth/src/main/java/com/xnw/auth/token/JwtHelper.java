package com.xnw.auth.token;

import com.xnw.common.utils.frame.CommUtil;
import com.xnw.information.basic.utils.frame.Base64Util;
import com.xnw.redis.RedisService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Date;

/**
 * JwtHelper
 *
 * @author keeny
 * @ClassName: JwtHelper
 * @Description: JwtHelper
 * @date 2018年10月8日 下午1:41:49
 */
public class JwtHelper {
    final private static String CLIENT_ID = "098f6bcd4621d373cade4e832627b4f6";
    final private static String BASE64_SECRET = "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY=";
    final private static String NAME = "restapiuser";
    public final static String COOKIE_NAME = "frameSid";
    public final static String AUTH_CODE = "frameSidAuth";
    //final   private static long expiresSecond=1000*60*60*12;

    /**
     * 解析jwt
     *
     * @param @param  jsonWebToken
     * @param @return 设定文件
     * @return Claims    返回类型
     * @throws
     * @Title: parseJWT
     * @Description: 解析jwt
     */
    public static Claims parseJWT(String jsonWebToken) {
        try {
            return Jwts.parser()
                    .setSigningKey(Base64Util.decryptBASE64(BASE64_SECRET))
                    .parseClaimsJws(jsonWebToken).getBody();
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 构建jwt
     *
     * @param @param  uuid
     * @param @param  phone
     * @param @param  time
     * @param @return 设定文件
     * @return String    返回类型
     * @throws
     * @Title: createJWT
     * @Description: 构建jwt
     */
    public static String createJWT(String uuid, String phone, long time) throws Exception {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //生成签名密钥
        byte[] apiKeySecretBytes = Base64Util.decryptBASE64(BASE64_SECRET);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //添加构成JWT的参数
        JwtBuilder builder = Jwts.builder().setHeaderParam("typ", "JWT")
                .claim("uuid", uuid)
                .claim("phone", phone)
                .setIssuer(CLIENT_ID)
                .setAudience(NAME)
                .signWith(signatureAlgorithm, signingKey);
        //添加Token过期时间
        if (time >= 0) {
            long expMillis = nowMillis + time;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp).setNotBefore(now);
        }

        //生成JWT
        return builder.compact();
    }


    /**
     * 解析jwt
     *
     * @param jsonWebToken
     * @param redisService
     * @param redisDataName 路径名称
     * @param @return       设定文件
     * @return boolean    返回类型
     * @throws
     * @Title: checkToken
     * @Description: 解析jwt
     */
    public static boolean checkToken(String jsonWebToken, RedisService redisService, String redisDataName) {
        try {
            jsonWebToken = jsonWebToken.substring(5);
            Claims claims = Jwts.parser()
                    .setSigningKey(Base64Util.decryptBASE64(BASE64_SECRET))
                    .parseClaimsJws(jsonWebToken).getBody();

            String uuid = claims.get("uuid").toString();
            return redisService.exists(CommUtil.getRedisKey(redisDataName + uuid));
        } catch (Exception ex) {
            //System.out.println(ex.toString());
            return false;
        }
    }

    /**
     * 依据头信息解析uuid
     *
     * @param request
     * @param redisService
     * @return
     */
    public static String analysisToken(HttpServletRequest request, RedisService redisService) {
        try {
            final String authHeader = request.getHeader("X-Auth-Token");
            String jsonWebToken = authHeader.substring(5);
            Claims claims = Jwts.parser()
                    .setSigningKey(Base64Util.decryptBASE64(BASE64_SECRET))
                    .parseClaimsJws(jsonWebToken).getBody();

            String uuid = claims.get("uuid").toString();
            if (redisService.exists(CommUtil.getRedisKey(uuid))) {
                return uuid;
            } else {
                return null;
            }
        } catch (Exception ex) {
            //System.out.println(ex.toString());
            return null;
        }
    }

    /**
     * 依据Header头 获取Token信息中的参数
     * 例如：uuid   phone 等
     *
     * @param request
     * @param flag
     * @return
     */
    public static String getTokenParameterByHeader(HttpServletRequest request, String flag) {
        try {
            final String authHeader = request.getHeader("X-Auth-Token");
            if (authHeader == null) {
                return null;
            }
            String jsonWebToken = authHeader.substring(7);
            Claims claims = Jwts.parser()
                    .setSigningKey(Base64Util.decryptBASE64(BASE64_SECRET))
                    .parseClaimsJws(jsonWebToken).getBody();
            if (claims != null) {
                return claims.get(flag).toString();
            }
            return null;
        } catch (Exception ex) {
            //System.out.println(ex.toString());
            return null;
        }
    }

    /**
     * 依据Header头 获取Token信息中的参数
     * 例如：uuid   phone 等
     *
     * @param authHeader
     * @param flag
     * @return
     */
    public static String getTokenParameterByHeader(String authHeader, String flag) {
        try {
            if (authHeader == null) {
                return null;
            }
            String jsonWebToken = authHeader.substring(5);
            Claims claims = Jwts.parser()
                    .setSigningKey(Base64Util.decryptBASE64(BASE64_SECRET))
                    .parseClaimsJws(jsonWebToken).getBody();
            if (claims != null) {
                return claims.get(flag).toString();
            }
            return null;
        } catch (Exception ex) {
            //System.out.println(ex.toString());
            return null;
        }
    }


}
