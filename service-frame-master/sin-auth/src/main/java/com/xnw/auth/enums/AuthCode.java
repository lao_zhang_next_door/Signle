package com.xnw.auth.enums;

/**
 * 权限判断码
 *
 * @author T470
 */
public enum AuthCode {

    /**
     * 权限错误类型枚举
     */
    TokenExpired(401, "用户token已过期，请重新登录！"),
    WithoutToken(402, "无用户token请求头信息！"),
    GetTokenFailed(403, "获取用户token失败！"),
    SystemUnregistered(410, "系统未注册请输入激活码！");

    private final int code;
    private final String value;

    private AuthCode(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
