package com.xnw.application.service;

import com.xnw.application.entity.AppShortMessage;
import com.xnw.frame.basic.base.service.BaseService;
import org.springframework.lang.Nullable;

import java.util.Map;

/**
 * @author
 * @date 2020-07-10 10:12:25
 */
public interface AppShortMessageService extends BaseService<AppShortMessage> {


    //以下皆为国内短信

    /**
     * 单条发送
     * 因为运营商政策，请先在后台完成报备签名、模板及做相关设置(详见接入引导)，再开发 API。验证码类短信，请在手机验证环节，加入行为验证码，以免被恶意攻击。
     *
     * @param mobile 接收的手机号,仅支持单号码发送
     * @param text   需要使用已审核通过的模板或者默认模板
     * @return JSON字符串
     */
    String singleSend(String mobile, String text);

    /**
     * 批量发送相同内容短信
     * 因为运营商政策，请先在后台完成报备签名、模板及做相关设置(详见接入引导)，再开发 API。不建议使用批量发送接口发送验证码短信，可能会造成验证码发送延迟。
     *
     * @param mobile 接收的手机号,多个手机号用英文逗号隔开
     * @param text   需要使用已审核通过的模板或者默认模板
     * @return
     */
    String batchSend(String mobile, String text);

    /**
     * 指定模板单发
     * 验证码短信，请在手机验证环节，加入图片验证码，以免被恶意攻击。相对于此接口，更推荐使用单条发送接口，接入更简单，也更适用于不同系统环境中接口的集成。
     *
     * @param mobile   接收的手机号,仅支持单号码发送
     * @param tpl_id   发送的模版id
     * @param par      模板内容，变量名和变量值 键值对
     * @param ENCODING 编码格式（默认 utf8）
     * @return
     */
    String tplSingleSend(String mobile, long tpl_id, Map<String, String> par, @Nullable String ENCODING);

    /**
     * 指定模板群发
     * 不建议使用批量发送接口发送验证码短信，可能会造成验证码发送延迟。
     * 防骚扰过滤：默认开启。过滤规则：同 1 个手机发相同内容，30 秒内最多发送 1 次，5 分钟内最多发送 3 次。
     *
     * @param mobile   接收的手机号,多个手机号用英文逗号分割
     * @param tpl_id   发送的模版id
     * @param par      模板内容，变量名和变量值 键值对
     * @param ENCODING 编码格式（默认 utf8）
     * @return
     */
    String tplBatchSend(String mobile, long tpl_id, Map<String, String> par, String ENCODING);

    void updateEntity(AppShortMessage message);

    /**
     * 发送短信接口
     * <p>
     * 传参内容含义
     * 1.isSendNow 是否立即发送  是则立即发送并存入数据库  否则存入数据库并不发送
     * 2.tplId  云片网对应 的 模板id  若传入id 则需要同时传入 tplValue 也就是模板中对应的值 （键值对）
     * 若不传入tplId 则需要传入发送的短信内容 即smContent
     *
     * @param appShortmessage
     */
    void sendMessage(AppShortMessage appShortmessage);
}
