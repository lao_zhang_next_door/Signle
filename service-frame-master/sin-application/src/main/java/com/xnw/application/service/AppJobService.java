package com.xnw.application.service;

import com.xnw.application.entity.AppJob;
import com.xnw.frame.basic.base.service.BaseService;

/**
 * 
 * 
 * @author 
 * @date 2020-07-02 13:52:28
 */
public interface AppJobService extends BaseService<AppJob> {
}
