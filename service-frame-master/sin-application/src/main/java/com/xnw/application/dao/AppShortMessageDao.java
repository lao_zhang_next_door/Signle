package com.xnw.application.dao;


import com.xnw.application.entity.AppShortMessage;
import com.xnw.frame.basic.base.BaseDao;

/**
 * 
 * 
 * @author 
 * @date 2020-07-10 10:12:25
 */
public interface AppShortMessageDao extends BaseDao<AppShortMessage> {

}
