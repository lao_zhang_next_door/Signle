package com.xnw.application.quartz.job;

import com.xnw.application.quartz.annotation.AppJobMethod;
import com.xnw.common.utils.frame.SpringContextUtils;
import com.xnw.frame.basic.function.SubModuleUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

@AppJobMethod
public class TestJob implements Job {
    private final SubModuleUtils subModuleUtils = SpringContextUtils.getBean(SubModuleUtils.class);
    public String jobName = "测试job";
    public String jobDetail = "用来测试用";

    @Override
    public void execute(JobExecutionContext context) {

        System.out.println("测试任务执行！");
    }

}
