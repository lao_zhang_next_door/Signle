package com.xnw.application.service;


import com.xnw.application.entity.AppWorkCalendar;
import com.xnw.frame.basic.base.service.BaseService;

/**
 * 
 * 
 * @author 
 * @date 2020-07-03 14:42:24
 */
public interface AppWorkCalendarService extends BaseService<AppWorkCalendar> {

}
