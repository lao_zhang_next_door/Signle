package com.xnw.application.basic.enumeration;


public enum JobIsAutoEnum {

    MANUAL(0, "手动"),
    AUTO(1, "自动");

    private final int code;
    private final String value;

    private JobIsAutoEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
