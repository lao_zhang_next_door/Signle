package com.xnw.application.quartz.start;

import com.xnw.application.quartz.util.QuartzUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wzl
 */
@Component
@Order(101)
public class QuartzRunner implements ApplicationRunner {

    @Autowired
    private QuartzUtil quartzUtil;

    @Override
    public void run(ApplicationArguments args) {
        //获取定时任务
        quartzUtil.setAllJobs();
    }
}
