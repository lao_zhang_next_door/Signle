package com.xnw.application.basic;

import com.xnw.application.basic.enumeration.IsOrNot;
import com.xnw.application.basic.enumeration.JobIsAutoEnum;
import com.xnw.application.basic.enumeration.JobStatusEnum;
import com.xnw.application.entity.AppJob;
import com.xnw.application.entity.AppShortMessage;
import com.xnw.application.service.AppJobService;
import com.xnw.application.service.AppShortMessageService;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.common.utils.dataformat.AESUtil;
import com.xnw.common.utils.dataformat.MD5Util;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.frame.basic.function.SubModuleUtils;
import com.xnw.information.basic.utils.frame.ApplicationUtils;
import com.xnw.information.basic.utils.frame.PackageUtil;
import org.quartz.*;
import org.quartz.impl.SchedulerRepository;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import com.xnw.application.quartz.annotation.AppJobMethod;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;

@Component("applicationCommon")
public class Common {

    @Value(value = "${SystemFrame.passwordKey}")
    public String passwordKey;

    @Autowired
    SchedulerFactoryBean schedulerFactoryBean;

    @Autowired
    private AppJobService appJobService;

    @Autowired
    private SubModuleUtils subModuleUtils;

    @Autowired
    private AppShortMessageService messageService;

    public static String frameDoubleEncode = ApplicationUtils.getApplicationProperties("frame-double-encode");

    /**
     * 设置所有job
     *
     * @throws Exception
     */
    public void setAllJobs() {

        //配置项获取类路径
        String path = subModuleUtils.getConfigVal("系统任务路径");

        if (StringUtil.isBlank(path)) {
            return;
        }

        Set<Class<?>> set = new HashSet();
        try {
            String[] arr = path.split(",");

            if (arr.length == 0) {
                return;
            }

            for (String s : arr) {
                Set<Class<?>> newSet = PackageUtil.getClasses(s, AppJobMethod.class);
                set.addAll(newSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Class<?> aClass : set) {
            Class<? extends Job> class1 = (Class<? extends Job>) aClass;
            //确认是否已存在job 已存在则不做任何更新
            Map<String, Object> map = new HashMap<>();
            map.put("jobFpath", class1.getName());
            AppJob ajob = appJobService.selectOne(map);

            if (ajob != null) {
                try {
                    addJob(class1, ajob);
                } catch (SchedulerException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                continue;
            }

            try {
                AppJob job = new AppJob();
                job.initNull();
                job.setJobName(getField(class1, "jobName"));
                job.setJobDetail(getField(class1, "jobDetail"));
                job.setJobFpath(class1.getName());
                job.setJobStatus(JobStatusEnum.STOP.getCode());
                job.setJobIsAuto(JobIsAutoEnum.MANUAL.getCode());
                appJobService.insert(job);

                addJob(class1, job);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取对应字段的值
     *
     * @param class1
     * @param fieldName
     * @return
     * @throws Exception
     */
    public String getField(Class<?> class1, String fieldName) throws Exception {
        Field f = class1.getField(fieldName);
        f.setAccessible(true);
        return String.valueOf(f.get(class1.newInstance()));
    }

    public void addJob(Class<? extends Job> class1, AppJob job) throws SchedulerException {
        //添加所有job
        JobDetail jobDetail = JobBuilder.newJob(class1).withIdentity(job.getRowGuid()).build();
        Trigger trigger = null;
        if (job.getJobTrigger() != null) {
            trigger = TriggerBuilder.newTrigger().withIdentity(job.getRowGuid()).withSchedule(CronScheduleBuilder.cronSchedule(job.getJobTrigger())).build();
        } else {
            trigger = TriggerBuilder.newTrigger().withIdentity(job.getRowGuid()).build();
        }

        SchedulerRepository schedRep = SchedulerRepository.getInstance();

        if (schedRep.lookup(class1.getName()) != null) {
            schedRep.remove(class1.getName());
        }

        StdSchedulerFactory sf = new StdSchedulerFactory();
        Properties props = new Properties();
        props.put("org.quartz.scheduler.instanceName", class1.getName());
        props.put("org.quartz.threadPool.threadCount", "10");
        sf.initialize(props);
        Scheduler scheduler = sf.getScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        //自动 | 手动
        if (job.getJobIsAuto() == JobIsAutoEnum.AUTO.getCode()) {
            scheduler.start();
            job.setJobStatus(JobStatusEnum.INPROGRESS.getCode());
            appJobService.updateByPrimaryKey(job);

        } else {
            scheduler.standby();
            job.setJobStatus(JobStatusEnum.STOP.getCode());
            appJobService.updateByPrimaryKey(job);
        }
    }

    /**
     * 启动暂停某个job
     * params type: start/stop
     */
    public void runOrStopJob(String scheldName, String type) {

        SchedulerRepository res = SchedulerRepository.getInstance();
        Scheduler Scheduler = res.lookup(scheldName);
        try {
            //确认是否已存在job 已存在则不做任何更新
            Map<String, Object> map = new HashMap<>();
            map.put("jobFpath", scheldName);
            AppJob ajob = appJobService.selectOne(map);


            if ("start".equals(type) && Scheduler.isInStandbyMode()) {
                Scheduler.start();

                ajob.setJobStatus(JobStatusEnum.INPROGRESS.getCode());
                appJobService.updateByPrimaryKey(ajob);

            } else if ("stop".equals(type) && Scheduler.isStarted()) {
                Scheduler.standby();

                ajob.setJobStatus(JobStatusEnum.STOP.getCode());
                appJobService.updateByPrimaryKey(ajob);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    private void updateByStatus(AppShortMessage message, JSONObject obj) {
        if (obj.getInteger("code") == 0) {
            //更新状态为已发送
            message.setIsSend(IsOrNot.IS.getCode());
            message.setSmSendTime(LocalDateTime.now());
            messageService.updateEntity(message);
        } else {
            message.setFailureCode(obj.getInteger("code"));
            message.setFailureReason(obj.getString("msg") + ":" + obj.getString("detail"));
            messageService.updateEntity(message);
        }
    }

    public String getEncodePwd(String pwd) {
        pwd = MD5Util.md5Password(pwd).toLowerCase();
        if (frameDoubleEncode.equals("true")) {
            pwd = doubleEncode(pwd);
        }
        return getEncodeCompairPwd(pwd);
    }

    public String getEncodeCompairPwd(String pwd) {
        return AESUtil.encrypt(pwd, passwordKey);
    }

    private String doubleEncode(String pwd) {
        List<String> ret = new ArrayList<>();
        Map<String, String> douMap = new HashMap<String, String>() {
            {
                put("0", "f");
                put("1", "e");
                put("2", "d");
                put("3", "c");
                put("4", "b");
                put("5", "a");
                put("6", "9");
                put("7", "8");
                put("8", "7");
                put("9", "6");
                put("a", "5");
                put("b", "4");
                put("c", "3");
                put("d", "2");
                put("e", "1");
                put("f", "0");
            }
        };
        for (int i = 0; i < pwd.length(); i++) {
            char item = pwd.charAt(i);
            String key = String.valueOf(item);
            ret.add(douMap.getOrDefault(key, key));
        }
        return String.join("", ret);
    }

}
