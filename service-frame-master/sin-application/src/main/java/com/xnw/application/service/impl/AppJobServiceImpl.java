package com.xnw.application.service.impl;


import com.xnw.application.dao.AppJobDao;
import com.xnw.application.entity.AppJob;
import com.xnw.application.service.AppJobService;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AppJobServiceImpl extends BaseServiceImpl<AppJob> implements AppJobService {

    public AppJobServiceImpl(AppJobDao dao) {
        super(dao);
    }

    @Autowired
    private AppJobDao appJobDao;

//    public void changeTrigger(AppJob appjob, Map<String, Object> map) {
//        if (!StringUtil.isBlank(appjob.getJobTrigger())) {
//            System.out.println("更换触发器为" + appjob.getJobTrigger());
//            //更新任务触发器 直接停止任务
//            SchedulerRepository schedRep = SchedulerRepository.getInstance();
//            Scheduler s = schedRep.lookup(appjob.getJobFpath());
//            try {
//                s.standby();
//                map.put("jobStatus", JobStatusEnum.STOP.getCode());
//
//            } catch (SchedulerException e1) {
//                e1.printStackTrace();
//            }
//            if (s != null) {
//                TriggerKey key = new TriggerKey(appjob.getRowGuid());
//
//                try {
//                    Trigger tri = s.getTrigger(key);
//
//                    //初次修改需要判断
//                    CronTriggerImpl trigger = null;
//
//                    if (tri == null || "org.quartz.impl.triggers.SimpleTriggerImpl".equals(tri.getClass().getName())) {
//                        trigger = (CronTriggerImpl) TriggerBuilder.newTrigger().withIdentity(appjob.getRowGuid()).withSchedule(CronScheduleBuilder.cronSchedule(appjob.getJobTrigger())).build();
//                    } else {
//                        trigger = (CronTriggerImpl) tri;
//                        trigger.setCronExpression(appjob.getJobTrigger());
//                    }
//
//                    s.rescheduleJob(key, trigger);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

}
