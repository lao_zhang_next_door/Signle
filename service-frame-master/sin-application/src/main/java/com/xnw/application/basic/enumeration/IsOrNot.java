package com.xnw.application.basic.enumeration;

public enum IsOrNot {

	IS(0,"是"),
	NOT(1,"否");
	
	private final int code;
    private final String value;
    private IsOrNot(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
