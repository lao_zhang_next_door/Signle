package com.xnw.application.entity;

import com.xnw.frame.basic.base.BaseEntity;

import javax.persistence.Transient;


/**
 * <p>Title: AppJob</p>
 * <p>Description:</p>
 *
 * @author
 */
public class AppJob extends BaseEntity {
    /**
     * @param init true则有默认值
     */
    public AppJob(boolean init) {
        super(init);
    }

    public AppJob(String rowGuid) {
        super(rowGuid);
    }

    public AppJob() {
    }

    /**
     * 任务名
     */
    private String jobName;


    /**
     * 任务说明
     */
    private String jobDetail;


    /**
     * 任务类全路径
     */
    private String jobFpath;


    /**
     * 任务Trigger
     */
    private String jobTrigger;


    /**
     * 任务执行状态
     */
    private Integer jobStatus;

    @Transient
    private String jobStatusVal;
    /**
     * 启动方式
     */
    private Integer jobIsAuto;

    @Transient
    private String jobIsAutoVal;

    /**
     * 获取：任务执行状态
     */
    public String getJobStatusVal() {
        if (jobStatus != null) {
            return codeValueService.getItemTextByValue("任务执行状态", String.valueOf(jobStatus));
        } else {
            return "";
        }
    }

    /**
     * 设置：任务执行状态
     */
    public void setJobStatusVal(String jobStatusVal) {
        this.jobStatusVal = jobStatusVal;
    }

    /**
     * 获取：启动方式
     */
    public String getJobIsAutoVal() {
        if (jobIsAuto != null) {
            return codeValueService.getItemTextByValue("任务启动方式", String.valueOf(jobIsAuto));
        } else {
            return "";
        }
    }

    /**
     * 设置：启动方式
     */
    public void setJobIsAutoVal(String jobIsAutoVal) {
        this.jobIsAutoVal = jobIsAutoVal;
    }

    /**
     * 获取：任务名
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * 设置：任务名
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * 获取：任务说明
     */
    public String getJobDetail() {
        return jobDetail;
    }

    /**
     * 设置：任务说明
     */
    public void setJobDetail(String jobDetail) {
        this.jobDetail = jobDetail;
    }

    /**
     * 获取：任务类全路径
     */
    public String getJobFpath() {
        return jobFpath;
    }

    /**
     * 设置：任务类全路径
     */
    public void setJobFpath(String jobFpath) {
        this.jobFpath = jobFpath;
    }

    /**
     * 获取：任务Trigger
     */
    public String getJobTrigger() {
        return jobTrigger;
    }

    /**
     * 设置：任务Trigger
     */
    public void setJobTrigger(String jobTrigger) {
        this.jobTrigger = jobTrigger;
    }

    /**
     * 获取：任务执行状态
     */
    public Integer getJobStatus() {
        return jobStatus;
    }

    /**
     * 设置：任务执行状态
     */
    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    /**
     * 获取：启动方式
     */
    public Integer getJobIsAuto() {
        return jobIsAuto;
    }

    /**
     * 设置：启动方式
     */
    public void setJobIsAuto(Integer jobIsAuto) {
        this.jobIsAuto = jobIsAuto;
    }
}
