package com.xnw.information.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.information.entity.InformationCategory;

/**
 * @author T470
 */
public interface InformationCategoryService extends BaseService<InformationCategory> {


}
