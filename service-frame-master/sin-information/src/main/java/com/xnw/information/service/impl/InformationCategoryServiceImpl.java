package com.xnw.information.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.information.dao.InformationCategoryDao;
import com.xnw.information.entity.InformationCategory;
import com.xnw.information.service.InformationCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author T470
 */
@Service
public class InformationCategoryServiceImpl extends BaseServiceImpl<InformationCategory> implements InformationCategoryService {

    public InformationCategoryServiceImpl(InformationCategoryDao dao) {
        super(dao);
    }

    @Autowired
    private InformationCategoryDao informationCategoryDao;

}

