package com.xnw.information.service;


import com.xnw.common.utils.frame.PageUtils;
import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.information.entity.InformationInfo;

import java.util.Map;

/**
 * @author T470
 */
public interface InformationInfoService extends BaseService<InformationInfo> {

    /**
     * 信息发布
     * @param informationInfo
     */
    void deliveryInfo(InformationInfo informationInfo);

    /**
     * 根据权限联查栏目表
     * @param params
     * @return
     */
    PageUtils listDataLfCategory(Map<String, Object> params);
}
