package com.xnw.information.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * 用户实体
 * 实体类的字段数量 >= 数据库表中需要操作的字段数量。
 * 默认情况下，实体类中的所有字段都会作为表中的字段来操作，如果有额外的字段，必须加上@Transient注解。
 * <p>
 * 1.表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info。
 * 2.表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
 * 3.字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
 * 4.可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
 * 5.使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
 * 6.建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
 * 7.如果是MySQL的自增字段，加上@GeneratedValue(generator = "JDBC")即可
 * <p>
 * Example方法
 * 方法：List<T> selectByExample(Object example);
 * 说明：根据Example条件进行查询
 * 重点：这个查询支持通过Example类指定查询列，通过selectProperties方法指定查询列
 * <p>
 * 方法：int selectCountByExample(Object example);
 * 说明：根据Example条件进行查询总数
 * <p>
 * 方法：int updateByExample(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的全部属性，null值会被更新
 * <p>
 * 方法：int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的不是null的属性值
 * <p>
 * 方法：int deleteByExample(Object example);
 * 说明：根据Example条件删除数据
 * <p>
 * 例子： // 创建Example
 * Example example = new Example(TestTableVO.class);
 * // 创建Criteria
 * Example.Criteria criteria = example.createCriteria();
 * // 添加条件
 * criteria.andEqualTo("isDelete", 0);
 * criteria.andLike("name", "%abc123%");
 * List<TestTableVO> list = testTableDao.selectByExample(example);
 */

/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 */
public class InformationCategory extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public InformationCategory(boolean init) {
        super(init);
    }

    public InformationCategory(String rowGuid) {
        super(rowGuid);
    }

    public InformationCategory() {
    }


    /**
     *栏目名称
     */
    private String categoryName;


    /**
     *栏目描述
     */
    private String description;


    /**
     *是否需要审核
     */
    private Boolean isNeedAudit;


    /**
     * 设置：栏目名称
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 获取：栏目名称
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 设置：栏目描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取：栏目描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置：是否需要审核
     */
    public void setIsNeedAudit(Boolean isNeedAudit) {
        this.isNeedAudit = isNeedAudit;
    }

    /**
     * 获取：是否需要审核
     */
    public Boolean getIsNeedAudit() {
        return isNeedAudit;
    }

}

