package com.xnw.information.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.information.entity.InformationInfo;

public interface InformationInfoDao extends BaseDao<InformationInfo>{
	
}

