package com.xnw.information.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.information.entity.InformationCategory;

/**
 * @author T470
 */
public interface InformationCategoryDao extends BaseDao<InformationCategory> {

}

