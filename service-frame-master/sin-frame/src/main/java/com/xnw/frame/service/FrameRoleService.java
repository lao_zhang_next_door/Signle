package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.FrameRole;

import java.util.List;
import java.util.Map;

public interface FrameRoleService extends BaseService<FrameRole> {

	/**
	 * 根据角色guid 查询该角色所拥有的菜单模块
	 *
	 * @param roleGuid
	 * @return
	 */
	List<FrameModule> getModuleByRoleGuid(String roleGuid);


	/**
	 * 根据角色名获取Guids
	 *
	 * @param roleNames
	 * @return
	 */
	List<String> getRoleGuidsByNames(String roleNames);

	/**
	 * 根据用户Guid获取角色
	 *
	 * @param userGuid
	 * @return
	 */
	List<Map<String, Object>> getRolesByUserGuid(String userGuid);

	/**
	 * 根据角色Guid获取角色名称
	 *
	 * @param userRoleGuid
	 * @return
	 */
	String getRoleNameByRoleGuid(String userRoleGuid);
}
