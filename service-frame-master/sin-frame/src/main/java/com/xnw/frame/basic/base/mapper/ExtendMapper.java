package com.xnw.frame.basic.base.mapper;


import com.xnw.frame.basic.mybatis.mapper.annotation.RegisterMapper;

@RegisterMapper
public interface ExtendMapper<T> extends ExtendInsertMapper<T>, ExtendSelectMapper<T>, ExtendUpdateMapper<T>, ExtendDeleteMapper<T> {

    
    
	
	
	
}
