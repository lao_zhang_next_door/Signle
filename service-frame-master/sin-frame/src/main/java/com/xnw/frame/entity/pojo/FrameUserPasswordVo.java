package com.xnw.frame.entity.pojo;

/**
 * 用户修改实体类
 */
public class FrameUserPasswordVo {
    /**
     * 原始密码
     */
    private String originPassword;

    /**
     * 新密码
     */
    private String newPassword;

    /**
     * 确认密码
     */
    private String verifyPassword;

    /**
     * 待修改用户guid
     */
    private String userGuid;

    public String getOriginPassword() {
        return originPassword;
    }

    public FrameUserPasswordVo setOriginPassword(String originPassword) {
        this.originPassword = originPassword;
        return this;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public FrameUserPasswordVo setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        return this;
    }

    public String getVerifyPassword() {
        return verifyPassword;
    }

    public FrameUserPasswordVo setVerifyPassword(String verifyPassword) {
        this.verifyPassword = verifyPassword;
        return this;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public FrameUserPasswordVo setUserGuid(String userGuid) {
        this.userGuid = userGuid;
        return this;
    }
}
