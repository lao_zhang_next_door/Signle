package com.xnw.frame.controller;

import com.xnw.auth.token.PassToken;
import com.xnw.common.utils.frame.Query;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FrameConfig;
import com.xnw.frame.service.FrameConfigService;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统参数控制器
 * @author hero
 *
 */
@RestController
@RequestMapping("/frame/FrameConfig")
public class FrameConfigController extends BaseController {

	@Autowired
	private FrameConfigService frameConfigService;
	
	@GetMapping("/listData")
	private R listData(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
        List<FrameConfig> configList = frameConfigService.selectByLimit(query);
        int count = frameConfigService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(configList, count, query.getLimit(), query.getPage());
		return R.list(pageUtil.getTotalCount(), pageUtil.getList());
	}
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		FrameConfig frameConfig = new FrameConfig(); 
		frameConfig.setRowGuid(rowGuid);
		List<FrameConfig> codeList = frameConfigService.select(frameConfig);
		return R.ok().put("data", codeList);
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameConfig frameConfig){
		frameConfig.initNull();
		frameConfigService.insert(frameConfig);
		return R.ok();
	}

	@PutMapping("/update")
	private R update(@RequestBody FrameConfig frameConfig) {
		frameConfigService.updateByRowGuidSelective(frameConfig);
		return R.ok();
	}

	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids) {
		frameConfigService.deleteBatch(rowGuids);
		return R.ok();
	}

	@GetMapping("/getSystemConfig")
	private R getSystemConfig(@RequestParam String configName) {
		String value = frameConfigService.getConfigsVal(configName);
		return R.ok().put("data", value);
	}

	@PassToken
	@GetMapping("/checkPicCaptcha")
	private R checkPicCaptcha() {
		String value = frameConfigService.getConfigsVal("是否开启图形验证");
		return R.ok().put("data", value);
	}
}
 