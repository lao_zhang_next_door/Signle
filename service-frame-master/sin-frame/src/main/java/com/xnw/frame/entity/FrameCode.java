package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * 设计规范:
 * 代码项每一项的名称都要唯一
 * 代码项的每一个子项的value值也是必须唯一
 * @author hero
 */
public class FrameCode extends BaseEntity {

	/**
	 * @param init true则有默认值
	 */
	public FrameCode(boolean init) {
		super(init);
	}

	public FrameCode() {}
	
	/**
	 * 名称
	 */
	private String codeName;
	
	/**
	 * 多维
	 */
	private String codeMask;

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeMask() {
		return codeMask;
	}

	public void setCodeMask(String codeMask) {
		this.codeMask = codeMask;
	}
	
}
