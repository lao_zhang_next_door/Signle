package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * 用户实体
 *	实体类的字段数量 >= 数据库表中需要操作的字段数量。
 *	默认情况下，实体类中的所有字段都会作为表中的字段来操作，如果有额外的字段，必须加上@Transient注解。
 *
 *	1.表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info。
 *	2.表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
 *	3.字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
 *	4.可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
 *	5.使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
 *	6.建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
 *	7.如果是MySQL的自增字段，加上@GeneratedValue(generator = "JDBC")即可
 *
 *	Example方法
 *	方法：List<T> selectByExample(Object example);
 *	说明：根据Example条件进行查询
 *	重点：这个查询支持通过Example类指定查询列，通过selectProperties方法指定查询列

 方法：int selectCountByExample(Object example);
 说明：根据Example条件进行查询总数

 方法：int updateByExample(@Param("record") T record, @Param("example") Object example);
 说明：根据Example条件更新实体record包含的全部属性，null值会被更新

 方法：int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);
 说明：根据Example条件更新实体record包含的不是null的属性值

 方法：int deleteByExample(Object example);
 说明：根据Example条件删除数据

 例子： // 创建Example
 Example example = new Example(TestTableVO.class);
 // 创建Criteria
 Example.Criteria criteria = example.createCriteria();
 // 添加条件
 criteria.andEqualTo("isDelete", 0);
 criteria.andLike("name", "%abc123%");
 List<TestTableVO> list = testTableDao.selectByExample(example);
 *
 *
 */
/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 */
public class XnwClient extends BaseEntity {

	/**
	 * @param init true则有默认值
	 */
	public XnwClient(boolean init) {
		super(init);
	}

	public XnwClient(String rowGuid) {
		super(rowGuid);
	}

	public XnwClient() {}


	/**
	 *客户端Id
	 */
	private String clientId;


	/**
	 *客户端secret
	 */
	private String clientSecret;


	/**
	 *服务资源标识
	 */
	private String resourceIds;


	/**
	 *授予客户端特定权限
	 */
	private String scope;


	/**
	 *令牌生成方式
	 */
	private String authorizedGrantTypes;


	/**
	 *客户端的重定向URI,可为空, 当grant_type为
	 */
	private String webServerRedirectUri;


	/**
	 *指定客户端所拥有的Spring
	 */
	private String authorities;


	/**
	 *设定客户端的access_token的有效时间值(单位:秒
	 */
	private String accessTokenValidity;


	/**
	 *设定客户端的refresh_token的有效时间值(单位:秒
	 */
	private String refreshTokenValidity;


	/**
	 *这是一个预留的字段,在Oauth的流程中没有实际的使用
	 */
	private String additionalInformation;


	/**
	 *设置用户是否自动Approval操作
	 */
	private String autoapprove;


	/**
	 * 设置：客户端Id
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * 获取：客户端Id
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * 设置：客户端secret
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * 获取：客户端secret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
	/**
	 * 设置：服务资源标识
	 */
	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}
	/**
	 * 获取：服务资源标识
	 */
	public String getResourceIds() {
		return resourceIds;
	}
	/**
	 * 设置：授予客户端特定权限
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}
	/**
	 * 获取：授予客户端特定权限
	 */
	public String getScope() {
		return scope;
	}
	/**
	 * 设置：令牌生成方式
	 */
	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}
	/**
	 * 获取：令牌生成方式
	 */
	public String getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}
	/**
	 * 设置：客户端的重定向URI,可为空, 当grant_type为
	 */
	public void setWebServerRedirectUri(String webServerRedirectUri) {
		this.webServerRedirectUri = webServerRedirectUri;
	}
	/**
	 * 获取：客户端的重定向URI,可为空, 当grant_type为
	 */
	public String getWebServerRedirectUri() {
		return webServerRedirectUri;
	}
	/**
	 * 设置：指定客户端所拥有的Spring
	 */
	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}
	/**
	 * 获取：指定客户端所拥有的Spring
	 */
	public String getAuthorities() {
		return authorities;
	}
	/**
	 * 设置：设定客户端的access_token的有效时间值(单位:秒
	 */
	public void setAccessTokenValidity(String accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}
	/**
	 * 获取：设定客户端的access_token的有效时间值(单位:秒
	 */
	public String getAccessTokenValidity() {
		return accessTokenValidity;
	}
	/**
	 * 设置：设定客户端的refresh_token的有效时间值(单位:秒
	 */
	public void setRefreshTokenValidity(String refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}
	/**
	 * 获取：设定客户端的refresh_token的有效时间值(单位:秒
	 */
	public String getRefreshTokenValidity() {
		return refreshTokenValidity;
	}
	/**
	 * 设置：这是一个预留的字段,在Oauth的流程中没有实际的使用
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	/**
	 * 获取：这是一个预留的字段,在Oauth的流程中没有实际的使用
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}
	/**
	 * 设置：设置用户是否自动Approval操作
	 */
	public void setAutoapprove(String autoapprove) {
		this.autoapprove = autoapprove;
	}
	/**
	 * 获取：设置用户是否自动Approval操作
	 */
	public String getAutoapprove() {
		return autoapprove;
	}

}

