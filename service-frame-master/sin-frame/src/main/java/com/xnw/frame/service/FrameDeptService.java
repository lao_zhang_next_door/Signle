package com.xnw.frame.service;

import com.alibaba.fastjson2.JSONArray;
import com.xnw.frame.entity.pojo.FrameDeptPojo;
import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameDept;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FrameDeptService extends BaseService<FrameDept> {

	/**
	 * 根据rowGuid 联查(left join frameDept) 查询上级deptName
	 * @param rowGuid
	 */
	List<FrameDeptPojo> selectParentCode(String rowGuid);


	/**
	 * 根据区guid获取街道以及社区的关联数据
	 * @param qGuid
	 * @return
	 */
	JSONArray getStreetAndCommunity(String qGuid);

	/**
	 * 导入行政区划
	 * @param multipartFileToFile
	 */
    void importFrameDept(MultipartFile multipartFileToFile);

	/**
	 * 新增部门
	 * @param frameDept
	 */
	void save(FrameDept frameDept);

	/**
	 * 修改部门
	 *
	 * @param frameDept
	 */
	void update(FrameDept frameDept);

	/**
	 * 获取所有有经纬度的部门
	 *
	 * @return
	 */
	List<FrameDept> getDeptLocation();

	/**
	 * 根据rowGuid查询该部门下属子部门
	 *
	 * @param rowGuid
	 * @return
	 */
	List<FrameDept> getUserSonDeptByGuid(String rowGuid);

}
