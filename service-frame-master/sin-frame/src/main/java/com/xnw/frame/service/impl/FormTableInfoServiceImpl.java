package com.xnw.frame.service.impl;

import com.xnw.common.utils.exception.BaseException;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.basic.function.GenUtils;
import com.xnw.frame.dao.FormTableInfoDao;
import com.xnw.frame.entity.FormTableInfo;
import com.xnw.frame.service.FormTableInfoService;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:20
 **/

@Service
public class FormTableInfoServiceImpl extends BaseServiceImpl<FormTableInfo> implements FormTableInfoService {

    public FormTableInfoServiceImpl(BaseDao<FormTableInfo> dao) {
        super(dao);
    }

    @Autowired
    private FormTableInfoDao formTableInfoDao;

    public Map<String, String> queryTable(String tableName) {
        return formTableInfoDao.queryTable(tableName);
    }

    public List<Map<String, String>> queryColumns(String tableName) {
        return formTableInfoDao.queryColumns(tableName);
    }

    /**
     * 代码生成
     */
    @Override
    public byte[] generatorCode(String tableName, String tableGuid, Map<String, Object> params) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        //查询表信息
        Map<String, String> table = queryTable(tableName);
        if (table == null) {
            throw new BaseException("该表不存在");
        }
        //查询列信息
        List<Map<String, String>> columns = queryColumns(tableName);
        //生成代码
        GenUtils.generatorCode(table, columns, tableGuid, params, zip);
        IOUtils.closeQuietly(zip);

        return outputStream.toByteArray();

    }

}
