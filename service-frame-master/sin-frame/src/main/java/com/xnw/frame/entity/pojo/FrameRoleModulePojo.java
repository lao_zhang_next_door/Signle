package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.FrameRoleModule;

public class FrameRoleModulePojo extends FrameRoleModule {
	
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
