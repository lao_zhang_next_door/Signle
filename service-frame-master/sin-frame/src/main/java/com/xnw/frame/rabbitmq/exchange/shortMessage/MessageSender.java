package com.xnw.frame.rabbitmq.exchange.shortMessage;

import com.alibaba.fastjson2.JSON;
import com.xnw.common.utils.frame.CommUtil;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

	@Autowired
    private RabbitTemplate rabbitTemplate;
     
    public void sendMessage(Object object){
    	CorrelationData correlationData = new CorrelationData(JSON.toJSONString(object));
        rabbitTemplate.convertAndSend(CommUtil.getPackageName() + "_messageExchange",
                CommUtil.getPackageName() + "_messageQueue.send","",correlationData);
    }
}
