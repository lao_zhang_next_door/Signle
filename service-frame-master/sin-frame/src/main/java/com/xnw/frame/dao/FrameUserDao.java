package com.xnw.frame.dao;

import com.xnw.common.utils.frame.Query;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.pojo.FrameUserPojo;
import com.xnw.frame.entity.pojo.XnwUser;

import java.util.List;

public interface FrameUserDao extends BaseDao<FrameUser> {

    /**
     * 查询用户信息列表带部门信息 (联查部门表)
     *
     * @param query
     * @return
     */
    List<FrameUserPojo> getList(Query query);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息(联查部门表 附件表)
     *
     * @param userGuid
     * @return
     */
    FrameUserPojo selectDetailByUserGuid(String userGuid);

    /**
     * 查询guid用户详情信息 包括头像,部门等信息(联查部门表 附件表)
     *
     * @param loginId
     * @return
     */
    FrameUserPojo selectDetailByLoginId(String loginId);

    /**
     * 查询guid用户 关联角色信息
     *
     * @param userGuid
     * @return
     */
    List<FrameRole> selectRoleListByUserGuid(String userGuid);

    /**
     * 获取用户登录信息
     *
     * @param loginId
     * @return
     */
    XnwUser getUserInfoByLogin(String loginId);
}
