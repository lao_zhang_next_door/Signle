package com.xnw.frame.basic.function;

import com.rabbitmq.client.Channel;
import com.xnw.common.utils.frame.SpringContextUtils;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.information.basic.utils.frame.ObjectConvert;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author T470
 */
@Component
public class SubModuleUtils {
    public static String frameCommon = "com.xnw.frame.basic.function.Common";
    public static String applicationCommon = "com.xnw.application.basic.Common";

    public List<FrameCodeValue> getCacheByCodeName(String codeName) {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(frameCommon));
            Method m = service.getClass().getMethod("getCacheByCodeName", String.class);
            return (List<FrameCodeValue>) m.invoke(service, codeName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public String getConfigVal(String path) {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(frameCommon));
            Method m = service.getClass().getMethod("getConfigVal", String.class);
            return (String) m.invoke(service, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getUserName(String name) {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(frameCommon));
            Method m = service.getClass().getMethod("getUserName", String.class);
            return (String) m.invoke(service, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveLog(ProceedingJoinPoint joinPoint, long time, String userName, HttpServletRequest request) {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(frameCommon));
            Method m = service.getClass().getMethod("saveLog", ProceedingJoinPoint.class, long.class, String.class, HttpServletRequest.class);
            m.invoke(service, joinPoint, time, userName, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendShortMessage(Message message, Channel channel) {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(applicationCommon));
            Method m = service.getClass().getMethod("sendShortMessage", Message.class, Channel.class);
            m.invoke(service, message, channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shortMessageExecute() {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(applicationCommon));
            Method m = service.getClass().getMethod("shortMessageExecute");
            m.invoke(service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAllJobs() {
        try {
            Object service = SpringContextUtils.getBean(Class.forName(applicationCommon));
            Method m = service.getClass().getMethod("setAllJobs");
            m.invoke(service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 代码项遍历赋值
     *
     * @param frameCodeValueList
     * @param value
     * @return
     */
    public String getTextByValue(List<FrameCodeValue> frameCodeValueList, Object value) {
        return getTextByValue(frameCodeValueList, value, ";");
    }

    public String getTextByValue(List<FrameCodeValue> frameCodeValueList, Object value, String arrSplit) {
        String[] values = value.toString().split(arrSplit);
        List<String> retValue = new ArrayList<>();
        for (String val : values) {
            FrameCodeValue frameCodeValue = ObjectConvert.listDefault(ObjectConvert.filter(frameCodeValueList,
                    a -> a.getItemValue().equals(val)));
            if (frameCodeValue != null) {
                retValue.add(frameCodeValue.getItemText());
            }
        }
        return String.join(arrSplit, retValue);
    }

    public String getCodeValue(String codeName, String codeValue) {

        if (!StringUtil.isEmpty(codeValue)) {
            return getTextByValue(getCacheByCodeName(codeName), codeValue);
        } else {
            return "";
        }
    }
}
