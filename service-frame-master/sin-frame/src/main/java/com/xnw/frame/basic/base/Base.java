package com.xnw.frame.basic.base;

import com.alibaba.fastjson2.JSON;
import com.xnw.auth.HttpContextUtils;
import com.xnw.common.utils.exception.BaseException;
import com.xnw.common.utils.frame.CommUtil;
import com.xnw.frame.entity.pojo.XnwUser;
import com.xnw.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;

public class Base {

	@Autowired
	protected HttpSession session;
	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpServletResponse response;
	@Autowired
	protected RedisService redisService;
	//定义一个全局的记录器，通过LoggerFactory获取

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value(value = "${upload.file.path}")
	protected String filePath;

	@Value(value = "${SystemFrame.passwordKey}")
	protected String passwordKey;

	@Value("${token.head.name}")
	protected String tokenHead;

	@Value("${request.session.attribute}")
	protected String sessionAttribute;

	@Value("${user.redis-name}")
	protected String userDataRedisName;


	/**
	 * 判断实体中字段某字段是否存在 (不包括父类继承下来的字段)
	 *
	 * @param obj
	 * @param fieldNames
	 * @return
	 */
	protected boolean isFieldExist(Object obj, String... fieldNames) {
		if (obj == null || fieldNames == null || fieldNames.length == 0) {
			throw new BaseException("参数错误");
		}
		Class<?> aClass = obj.getClass();

		for (String fieldName : fieldNames) {
			try {
				aClass.getDeclaredField(fieldName);
			} catch (NoSuchFieldException e) {
				//为空
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断当前实体中 字段是否为空 不支持父类继承字段
	 * @param obj
	 * @param fieldNames
	 * @return
	 */
	protected boolean isFieldBlank(Object obj, String... fieldNames) {

		if(obj == null || fieldNames == null || fieldNames.length == 0){
			throw new BaseException("参数错误");
		}
		Class<?> aClass = obj.getClass();

		for (String fieldName : fieldNames) {
			try {
				Field f = aClass.getDeclaredField(fieldName);
				f.setAccessible(true);
				if (f.get(obj) == null || f.get(obj).equals("")) {
					// 判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
					return true;
				}
			} catch (Exception e) {
				//为空
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取当前用户信息
	 *
	 * @return
	 */
	protected XnwUser getCurrentUser() {
		HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
		String userGuidKey = HttpContextUtils.getSessionUserGuid(request, userDataRedisName);
		String userGuid = userGuidKey + "_session";
		String json = String.valueOf(redisService.get(CommUtil.getRedisKey(userDataRedisName+userGuid)));
		return JSON.parseObject(json, XnwUser.class);
	}

}
