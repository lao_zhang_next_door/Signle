package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.entity.FrameUser;

import javax.persistence.Transient;
import java.util.List;

public class FrameUserPojo extends FrameUser {

    @Transient
    private FrameUser user;

    @Transient
    private String deptGuidStr;

    @Transient
    private String deptGuidVal;

    @Transient
    private String statusVal;

    @Transient
    private List<FrameRole> roleList;

    @Transient
    private List<String> roleNameList;

    @Transient
    private List<String> deptNames;

    @Transient
    private List<String> deptGuids;

    @Transient
    private String sexVal;

    @Transient
    private String imgCode;

    /**
     * 是否禁用
     */
    @Transient
    private String isLimitVal;

    /**
     * 用户头像
     */
    @Transient
    private String headimgGuidVal;

    /**
     * 政治面貌
     */
    @Transient
    private String politicsStatusVal;

    /**
     * 文化程度
     */
    @Transient
    private String educationVal;

    public String getHeadimgGuidVal() {
        return headimgGuidVal;
    }

    public void setHeadimgGuidVal(String headimgGuidVal) {
        this.headimgGuidVal = headimgGuidVal;
    }

    public List<FrameRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<FrameRole> roleList) {
        this.roleList = roleList;
    }

    public List<String> getRoleNameList() {
        return roleNameList;
    }

    public void setRoleNameList(List<String> roleNameList) {
        this.roleNameList = roleNameList;
    }

    public String getDeptGuidVal() {
        return deptGuidVal;
    }

    public void setDeptGuidVal(String deptGuidVal) {
        this.deptGuidVal = deptGuidVal;
    }

    public String getStatusVal() {
        if (getStatus() != null) {
            return codeValueService.getItemTextByValue("用户状态", String.valueOf(getStatus()));
        } else {
            return "";
        }
    }

    public String getIsLimitVal() {
        if (getIsLimit() != null) {
            return codeValueService.getItemTextByValue("是否", String.valueOf(getIsLimit()));
        } else {
            return "";
        }
    }

    public String getSexVal() {
        if (getSex() != null) {
            return codeValueService.getItemTextByValue("性别", String.valueOf(getSex()));
        } else {
            return "";
        }
    }

    public FrameUser getUser() {
        return user;
    }

    public void setUser(FrameUser user) {
        this.user = user;
    }

    public String getImgCode() {
        return imgCode;
    }

    public FrameUserPojo setImgCode(String imgCode) {
        this.imgCode = imgCode;
        return this;
    }

    public List<String> getDeptNames() {
        return deptNames;
    }

    public void setDeptNames(List<String> deptNames) {
        this.deptNames = deptNames;
    }

    public List<String> getDeptGuids() {
        return deptGuids;
    }

    public void setDeptGuids(List<String> deptGuids) {
        this.deptGuids = deptGuids;
    }

    public String getDeptGuidStr() {
        return deptGuidStr;
    }

    public void setDeptGuidStr(String deptGuidStr) {
        this.deptGuidStr = deptGuidStr;
    }
}
