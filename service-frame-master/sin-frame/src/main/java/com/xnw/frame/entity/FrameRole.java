package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-20-10:49
 **/
public class FrameRole extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
    public FrameRole(boolean init) {
		super(init);
	}

	public FrameRole() {}
	
	/**
     * 角色名
     */
    private String roleName;
    /**
     * 角色类型
     */
    private String roleType;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }
}
