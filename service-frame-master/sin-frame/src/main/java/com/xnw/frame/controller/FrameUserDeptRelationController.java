package com.xnw.frame.controller;

import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.entity.FrameUserDeptRelation;
import com.xnw.frame.service.FrameUserDeptRelationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


/**
 * @author T470
 */
@Tag(name = "用户部门一对多关联控制器")
@RestController
@RequestMapping("/FrameUserDeptRelation")
public class FrameUserDeptRelationController {

    @Autowired
    private FrameUserDeptRelationService frameUserDeptRelationService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameUserDeptRelation> frameUserDeptRelationList = frameUserDeptRelationService.selectByLimit(query);
        int count = frameUserDeptRelationService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(frameUserDeptRelationList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameUserDeptRelation frameUserDeptRelation = new FrameUserDeptRelation(rowGuid);
        return R.ok().put("data", frameUserDeptRelationService.select(frameUserDeptRelation));
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameUserDeptRelation frameUserDeptRelation) {
        frameUserDeptRelation.initNull();
        frameUserDeptRelationService.insert(frameUserDeptRelation);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameUserDeptRelation frameUserDeptRelation) {
        frameUserDeptRelation.setUpdateTime(LocalDateTime.now());
        frameUserDeptRelationService.updateByRowGuidSelective(frameUserDeptRelation);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameUserDeptRelationService.deleteBatch(rowGuids);
        return R.ok();
    }
}