package com.xnw.frame.service.impl;

import com.alibaba.fastjson2.JSON;
import com.xnw.common.utils.exception.BaseException;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FormTableFieldDao;
import com.xnw.frame.entity.FormTableField;
import com.xnw.frame.entity.FormTableInfo;
import com.xnw.frame.service.FormTableFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:00
 **/
@Transactional
@Service("tableFieldService")
public class FormTableFieldServiceImpl extends BaseServiceImpl<FormTableField> implements FormTableFieldService {
   

	@Value(value = "${spring.datasource.driver-class-name}")
    public String driver;

    @Value(value = "${spring.datasource.url}")
    public String url;

    @Value(value = "${spring.datasource.username}")
    public String userName;

    @Value(value = "${spring.datasource.password}")
    public String password;

    @Autowired
    private FormTableFieldDao formTableFieldDao;

    public FormTableFieldServiceImpl(FormTableFieldDao dao) {
		super(dao);
	}

    @Override
    public List<FormTableField> selectFormTableFieldList(Map<String, Object> map) {
        return null;
    }

    @Override
    public Boolean insertFormField(Map<String, Object> params) throws Exception{
        FormTableField tablefield = JSON.parseObject(JSON.toJSONString(params.get("field")),
                FormTableField.class);

        FormTableInfo tableInfo = new FormTableInfo();
        tableInfo.setPhysicalName(params.get("tableInfo").toString());
        String mustfill;
        if (tablefield.getMustFill().equals("是")) {
            mustfill = "not null";
        } else {
            mustfill = "null";
        }
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        DatabaseMetaData mdata = conn.getMetaData();
        ResultSet rs = mdata.getTables(null, null, tableInfo.getPhysicalName(), null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            String SQL = "alter table " + params.get("tableInfo").toString();
            SQL += " add " + tablefield.getFieldName();
            SQL += " " + tablefield.getFieldType();
            if ("varchar".equals(tablefield.getFieldType().toLowerCase())) {
                SQL += "(" + tablefield.getFieldLength() + ")";
            }
            if ("decimal".equals(tablefield.getFieldType().toLowerCase())) {
                SQL += " (" + (Integer.parseInt(tablefield.getFieldLength()) > 38 ? 38 : tablefield.getFieldLength()) + "," + (tablefield.getDecimalLength() == null ? '2' : tablefield.getDecimalLength()) + ") ";
            }
            SQL += " " + mustfill + " comment '" + tablefield.getFieldDisplayName() + "'";
            logger.info("sql:{}", SQL);
            stat.executeUpdate(SQL);

        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();
        //生成uuid作为rowguid
        String uuid = UUID.randomUUID().toString();
        tablefield.setRowGuid(uuid);
        tablefield.setAllowTo(params.get("rowGuid").toString());
        //改为通用方法
        formTableFieldDao.insert(tablefield);
        return true;
    }


    @Override
    public Boolean updateFormField(Map<String, Object> params) throws Exception{
        FormTableField formTableField = JSON.parseObject(JSON.toJSONString(params.get("field")), FormTableField.class);
        String tableName = params.get("tableName").toString();
        String originName = params.get("originalName").toString();
        formTableFieldDao.updateByRowGuid(formTableField);

        String mustfill;
        if (formTableField.getMustFill().equals("是")) {
            mustfill = "not null";
        } else {
            mustfill = "null";
        }
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            //创建行政区划表
            System.out.println("alter table " + tableName + "  change  " + originName + "  " + formTableField.getFieldName() + "  " + formTableField.getFieldType() + " (" + formTableField.getFieldLength() + ")   " + mustfill + " comment  '" + formTableField.getFieldDisplayName() + "';");
            stat.executeUpdate("alter table " + tableName + "  change  " + originName + "  " + formTableField.getFieldName() + "  " + formTableField.getFieldType() + " (" + formTableField.getFieldLength() + ")   " + mustfill + " comment  '" + formTableField.getFieldDisplayName() + "';");
        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();
        return true;
    }

    @Override
    public Boolean deleteFormField(Map<String, Object> params) throws Exception {
        String tableName = params.get("tableName").toString();
        List<String> fieldNames = (List<String>) params.get("fieldNames");

        List<String> rgList = (List)params.get("rowGuids");
        formTableFieldDao.deleteBatch(rgList.toArray(new String[rgList.size()]));

        FormTableInfo tableInfo = new FormTableInfo();
        tableInfo.setPhysicalName(params.get("tableName").toString());
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableInfo.getPhysicalName(), null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            //遍历字段名
            for (String fieldName : fieldNames) {
                stat.executeUpdate("alter table " + tableName + " drop " + fieldName + ";");
            }
        } else {
            return false;
        }
        // 释放资源
        stat.close();
        conn.close();

        return true;
    }

    /**
     * 删除表
     * @param tableName
     * @return
     */
    public void delTable(String tableName) throws Exception {
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则删除表
        if (rs.next()) {
            String sql = "drop table "+tableName;
            stat.execute(sql);
        } else {}
        // 释放资源
        stat.close();
        conn.close();
    }

    @Override
    public void addTable(String tableName) throws Exception {
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);
        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (rs.next()) {
            throw new BaseException("表已存在");
        } else {
            //创建行政区划表
            stat.execute("CREATE TABLE " + tableName + "("
                    + "row_id bigint(0) NOT NULL AUTO_INCREMENT,"
                    + "row_guid varchar(50) NOT NULL,"
                    + "create_time datetime,"
                    + "update_time datetime,"
                    + "del_flag int(0) default '0',"
                    + "sort_sq bigInt(0) default '0',"
                    + "PRIMARY KEY (row_id)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
            );
        }
        // 释放资源
        stat.close();
        conn.close();
    }

    @Override
    public void updateTable(String tableName,String newTableName) throws Exception {
        //连接数据库
        Class.forName(driver);
        //测试url中是否包含useSSL字段，没有则添加设该字段且禁用
        if (!url.contains("?")) {
            url = url + "?useSSL=false";
        } else if (!url.contains("useSSL=false") || !url.contains("useSSL=true")) {
            url = url + "&useSSL=false";
        }
        Connection conn = DriverManager.getConnection(url, userName, password);
        Statement stat = conn.createStatement();
        //获取数据库表名
        ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null);

        // 判断表是否存在，如果存在则什么都不做，否则创建表
        if (!rs.next()) {
            throw new BaseException("表不存在");
        } else {
            //创建行政区划表
            stat.executeUpdate("alter table " + tableName + " rename " + newTableName);
        }
        // 释放资源
        stat.close();
        conn.close();
    }


}
