package com.xnw.frame.basic.function;

import com.xnw.common.utils.request.IpUtils;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.SystemLog;
import com.xnw.frame.service.*;
import com.xnw.frame.systemLog.SystemLogAop;
import com.xnw.information.basic.utils.aop.AopStaticFunction;
import com.xnw.information.basic.utils.frame.ApplicationUtils;
import com.xnw.redis.RedisService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * @author T470
 */
@Component("sysCommon")
public class Common {

    @Value(value = "${SystemFrame.passwordKey}")
    public String passwordKey;

    @Autowired
    public RedisService redis;

    @Autowired
    public FrameCodeService codeService;
    @Autowired
    public FrameUserService userService;
    @Autowired
    public FrameDeptService deptService;
    @Autowired
    private FrameConfigService configService;
    @Autowired
    private SystemLogService systemLogService;

    public static String frameDoubleEncode = ApplicationUtils.getApplicationProperties("frame-double-encode");

    public void saveLog(ProceedingJoinPoint joinPoint, long time, String userName, HttpServletRequest request) {
        String parmas = "";
        try {
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            SystemLog sysLog = new SystemLog();
            sysLog.initNull();
            SystemLogAop logAnnotation = method.getAnnotation(SystemLogAop.class);
            if (logAnnotation != null) {
                // 注解上的描述
                sysLog.setOperation(logAnnotation.value());
                sysLog.setActionType(logAnnotation.action());
            }
            // 请求的方法名
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = signature.getName();
            sysLog.setMethod(className + "." + methodName + "()");
            // 请求的方法参数值
            // 请求的方法参数名称
            try {
                Object[] methodArgs = joinPoint.getArgs();
                String class_name = joinPoint.getTarget().getClass().getName();
                String method_name = joinPoint.getSignature().getName();
                //重新定义日志
                Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
                logger.info("class_name = {}", class_name);
                logger.info("method_name = {}", method_name);
                String[] paramNames = AopStaticFunction.getFieldsName(class_name, method_name);
                parmas = AopStaticFunction.logParam(paramNames, methodArgs);
                sysLog.setParams(parmas);
                sysLog.setMethod(method_name);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // 获取request
            // 设置IP地址
            sysLog.setIp(IpUtils.getIpAddr(request));
            // 模拟一个用户名
            if ("".equals(userName) || userName == null) {
                userName = AopStaticFunction.throwChooseId("getLoginId", parmas);
            }
            sysLog.setUserName(userName);
            sysLog.setTime((int) time);
            sysLog.setCreateTime(LocalDateTime.now());
            // 保存系统日志
            systemLogService.insert(sysLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getConfigVal(String name) {
        return configService.getConfigsVal(name);
    }

    public String getUserName(String userGuid) {
        return userService.selectOne(new FrameUser(userGuid)).getUserName();
    }
}
