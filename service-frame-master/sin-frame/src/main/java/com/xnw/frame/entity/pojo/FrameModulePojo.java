package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.FrameModule;

public class FrameModulePojo extends FrameModule {

    /**
     * 父级名称
     */
    private String parentGuidVal;

    public String getParentGuidVal() {
        return parentGuidVal;
    }

    public void setParentGuidVal(String parentGuidVal) {
        this.parentGuidVal = parentGuidVal;
    }

}
