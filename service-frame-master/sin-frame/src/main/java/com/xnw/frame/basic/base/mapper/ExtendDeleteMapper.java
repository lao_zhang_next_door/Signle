package com.xnw.frame.basic.base.mapper;

import com.xnw.frame.basic.mybatis.mapper.annotation.RegisterMapper;
import com.xnw.frame.basic.mybatis.mapper.provider.base.BaseDeleteProvider;
import org.apache.ibatis.annotations.DeleteProvider;


@RegisterMapper
public interface ExtendDeleteMapper<T> {

	/**
	 * 根据rowGuids 数组批量删除
	 *
	 * @param rowGuids
	 * @return
	 */
	@DeleteProvider(type = BaseDeleteProvider.class, method = "dynamicSQL")
	int deleteBatch(String[] rowGuids);

}
