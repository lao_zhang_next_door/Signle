package com.xnw.frame.dao;

import com.xnw.frame.entity.FormTableField;
import com.xnw.frame.basic.base.BaseDao;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:05
 **/
public interface FormTableFieldDao extends BaseDao<FormTableField> {


}
