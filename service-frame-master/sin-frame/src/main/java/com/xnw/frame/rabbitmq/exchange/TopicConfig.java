package com.xnw.frame.rabbitmq.exchange;

import com.xnw.common.utils.frame.CommUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.util.SerializationUtils;

/**
 * @author T470
 */
@Configuration
public class TopicConfig implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {

    private RabbitTemplate rabbitTemplate;

    private final Logger logger = LoggerFactory.getLogger(TopicConfig.class);

    @Bean
    @Scope("prototype")
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        this.rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setConfirmCallback(this);
        //指定 ConfirmCallback
        rabbitTemplate.setReturnsCallback(this);
        //指定 ReturnCallback
        return rabbitTemplate;
    }

    /**
     * 验证注册码队列
     **/
    @Bean
    public Queue licenseQueue() {
        return new Queue(CommUtil.getPackageName() + "_licenseQueue");
    }

    @Bean
    public TopicExchange licenseExchange() {
        return new TopicExchange(CommUtil.getPackageName() + "_licenseExchange");
    }

    @Bean
    public Binding bindingCoreExchange(Queue licenseQueue, TopicExchange licenseExchange) {
        return BindingBuilder.bind(licenseQueue).to(licenseExchange).with(
                CommUtil.getPackageName() + "_licenseQueue.*");
    }


    /**
     * 短信队列
     **/
    @Bean
    public Queue messageQueue() {
        return new Queue(CommUtil.getPackageName() + "_messageQueue");
    }

    @Bean
    public TopicExchange messageExchange() {
        return new TopicExchange(CommUtil.getPackageName() + "_messageExchange");
    }

    @Bean
    public Binding bindingMessageExchange(Queue messageQueue, TopicExchange messageExchange) {
        return BindingBuilder.bind(messageQueue).to(messageExchange).with(
                CommUtil.getPackageName() + "_messageQueue.*");
    }

    /**
     * 如果消息从交换器发送到对应队列失败时触发（比如根据发送消息时指定的routingKey找不到队列时会触发）
     */
    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        // 反序列化对象输出
        logger.info("消息主体: {}", SerializationUtils.deserialize(returnedMessage.getMessage().getBody()));
        logger.info("应答码: {}", returnedMessage.getReplyCode());
        logger.info("描述：{}", returnedMessage.getReplyText());
        logger.info("消息使用的交换器 exchange : {}", returnedMessage.getExchange());
        logger.info("消息使用的路由键 routing : {}", returnedMessage.getRoutingKey());
    }

    /**
     * 消息发送到交换器Exchange后触发回调。
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            logger.info("消息发送成功: {}", correlationData);
        } else {
            logger.error("消息发送失败: {}", cause);
        }
    }


}
