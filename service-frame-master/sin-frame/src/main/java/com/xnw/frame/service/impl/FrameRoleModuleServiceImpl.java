package com.xnw.frame.service.impl;

import com.xnw.frame.dao.FrameRoleModuleDao;
import com.xnw.frame.entity.FrameRoleModule;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.entity.pojo.FrameRoleModulePojo;
import com.xnw.frame.service.FrameRoleModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrameRoleModuleServiceImpl extends BaseServiceImpl<FrameRoleModule> implements FrameRoleModuleService {

	public FrameRoleModuleServiceImpl(FrameRoleModuleDao dao) {
		super(dao);
	}

	@Autowired
	private FrameRoleModuleDao frameRoleModuleDao;

	@Override
	public List<FrameRoleModulePojo> selectLJRole(String moduleGuid) {
		return frameRoleModuleDao.selectLJRole(moduleGuid);
	}

}
