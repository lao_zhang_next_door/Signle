package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;
 /**
  * 实体类设计原则：
  * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
  * 2.若需添加额外字段，请写pojo,vo等类
  * 3.所有字段均为私有权限
  */
public class FrameUserDeptRelation extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
	public FrameUserDeptRelation(boolean init) {
		super(init);
	}

	public FrameUserDeptRelation(String rowGuid) {
	    super(rowGuid);
	}

	public FrameUserDeptRelation() {}

			
	/**
	 *用户Guid
	 */
	private String userGuid;
	
			
	/**
	 *部门Guid
	 */
	private String deptGuid;
	
	
	/**
	 * 设置：用户Guid
	 */
	public void setUserGuid(String userGuid) {
		this.userGuid = userGuid;
	}
	/**
	 * 获取：用户Guid
	 */
	public String getUserGuid() {
		return userGuid;
	}
	/**
	 * 设置：部门Guid
	 */
	public void setDeptGuid(String deptGuid) {
		this.deptGuid = deptGuid;
	}
	/**
	 * 获取：部门Guid
	 */
	public String getDeptGuid() {
		return deptGuid;
	}

}

