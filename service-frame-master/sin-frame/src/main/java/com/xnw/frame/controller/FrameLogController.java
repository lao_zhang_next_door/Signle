package com.xnw.frame.controller;

import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.entity.CommonLog;
import com.xnw.frame.service.FrameLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 系统操作日志(微服务)
 */
@RestController
@RequestMapping("/frame/FrameLog")
public class FrameLogController {

    @Autowired
    private FrameLogService frameLogService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<CommonLog> frameLogList = frameLogService.selectByLimit(query);
        int count = frameLogService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(frameLogList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        CommonLog frameLog = new CommonLog();
        frameLog.setRowGuid(rowGuid);
        return R.ok().put("data", frameLogService.select(frameLog));
    }

    @PostMapping("/add")
    private R add(@RequestBody CommonLog frameLog) {
        frameLog.initNull();
        frameLogService.insert(frameLog);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody CommonLog frameLog) {
        frameLog.setUpdateTime(LocalDateTime.now());
        frameLogService.updateByRowGuidSelective(frameLog);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameLogService.deleteBatch(rowGuids);
        return R.ok();
    }
}