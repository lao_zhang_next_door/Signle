package com.xnw.frame.service.impl;


import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.SystemLogDao;
import com.xnw.frame.entity.SystemLog;
import com.xnw.frame.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("systemLogService")
public class SystemLogServiceImpl extends BaseServiceImpl<SystemLog> implements SystemLogService {

    @Autowired
    private SystemLogDao systemlogDao;

    public SystemLogServiceImpl(SystemLogDao dao) {
        super(dao);
    }
}
