package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameConfig;


public interface FrameConfigService extends BaseService<FrameConfig> {

    /**
     * 缓存系统设置
     **/
    void saveAllConfigToRedis();

    /**
     * 获取缓存系统设置
     *
     * @param configName
     **/
    String getConfigsVal(String configName);
}
