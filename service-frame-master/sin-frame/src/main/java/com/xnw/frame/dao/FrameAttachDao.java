package com.xnw.frame.dao;

import com.xnw.frame.entity.FrameAttach;
import com.xnw.frame.basic.base.BaseDao;

/**
 * @author wzl
 */
public interface FrameAttachDao extends BaseDao<FrameAttach> {

}
