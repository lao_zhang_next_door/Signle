package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameDept;
import com.xnw.frame.entity.FrameUserDeptRelation;

import java.util.List;

/**
 * @author T470
 */
public interface FrameUserDeptRelationDao extends BaseDao<FrameUserDeptRelation> {

    /**
     * 根据用户Guid删除关联
     *
     * @param userGuid
     */
    void deleteByUserGuid(String userGuid);

    /**
     * 获取用户关联部门信息
     *
     * @param userGuid
     * @return
     */
    List<FrameDept> getUserRelationDept(String userGuid);
}

