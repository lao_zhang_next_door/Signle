package com.xnw.frame.service.impl;

import com.alibaba.fastjson2.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xnw.common.utils.exception.BaseException;
import com.xnw.frame.dao.FrameCodeDao;
import com.xnw.frame.dao.FrameCodeValueDao;
import com.xnw.frame.entity.FrameCode;
import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.entity.pojo.FrameCodeValuePojo;
import com.xnw.frame.service.FrameCodeValueService;
import com.xnw.redis.RedisService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wzl
 */
@Service
public class FrameCodeValueServiceImpl extends BaseServiceImpl<FrameCodeValue> implements FrameCodeValueService {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RedisService redisService;

    public FrameCodeValueServiceImpl(FrameCodeValueDao dao, RedisService redisService) {
        super(dao);
        this.redisService = redisService;
    }

    @Autowired
    private FrameCodeValueDao frameCodeValueDao;

    @Autowired
    private FrameCodeDao frameCodeDao;

    @Value(value = "${spring.jmx.default-domain}")
    public String projectName;

    @Override
    public List<FrameCodeValuePojo> selectParentCode(String rowGuid) {
        return frameCodeValueDao.selectParentCode(rowGuid);
    }

    @Override
    public String getItemTextByCodeNameAndCodeValue(String codeName, String codeValue) {

        FrameCode code = new FrameCode();
        code.setCodeName(codeName);
        List<FrameCode> codeList = frameCodeDao.select(code);
        //code = null;

        if (codeList.size() > 1) {
            throw new BaseException("有重复的代码项>>" + code);
        }

        FrameCodeValue frameCodeValue = new FrameCodeValue();
        frameCodeValue.setCodeGuid(codeList.get(0).getRowGuid());
        frameCodeValue.setItemValue(String.valueOf(codeValue));
        List<FrameCodeValue> codeValueList = frameCodeValueDao.select(frameCodeValue);

        if (codeValueList.size() > 1) {
            throw new BaseException("有重复的代码项值>>" + code);
        }

        return codeValueList.get(0).getItemText();
    }

    @Override
    public void cacheAllCodeValue() {
        List<FrameCodeValuePojo> frameCodeValueList = getCodeValueToCache();
        Map<String, List<FrameCodeValue>> dataMap = Maps.newHashMap();

        if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
            //筛选二维代码
            List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                    .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                    .collect(Collectors.toList());
            //@代码项转为Hash存储
            frameCodeValueList.forEach(frameCodeValue -> {
                //这里确保名称唯一
                List<FrameCodeValue> list = dataMap.get(frameCodeValue.getCodeNames());
                if (list == null || list.isEmpty()) {
                    list = Lists.newLinkedList();
                }
                //若此代码为二维代码的父类
                if ("".equals(frameCodeValue.getParentGuid())) {
                    //获取他的子类
                    List<FrameCodeValuePojo> frameCodeValueChildrenList;
                    frameCodeValueChildrenList = multCodeList.stream()
                            .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                            .collect(Collectors.toList());
                    List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);
                    frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                }
                //若遍历到子类，忽略
                if (!StringUtils.isBlank(frameCodeValue.getParentGuid())) {

                } else {
                    list.add(frameCodeValue);
                    if (frameCodeValue.getCodeNames() != null) {
                        dataMap.put(frameCodeValue.getCodeNames(), list);
                    }
                }
            });

            //@存入redis
//            HashOperations<String, String, List<FrameCodeValue>> hashOperations = redisTemplate.opsForHash();
//            hashOperations.putAll(projectName + ":codeValue", dataMap);
        }

//        redisService.del(projectName+":codeValue");
        redisService.hmset(projectName + ":codeValue", JSON.parseObject(JSON.toJSONString(dataMap)));
        logger.info("codeValue存入redis");

    }

    @Override
    public List<FrameCodeValuePojo> getCodeValueToCache() {
        return frameCodeValueDao.getCodeValueToCache();
    }

    /**
     * 获取子类的子类(递归)
     *
     * @param childrenList 子类
     * @param allMultiList 所有二维代码数据
     * @return list
     */
    private List<FrameCodeValuePojo> getChildrenToChildren(List<FrameCodeValuePojo> childrenList, List<FrameCodeValuePojo> allMultiList) {
        //遍历子类的子类
        for (FrameCodeValuePojo children : childrenList) {
            List<FrameCodeValuePojo> frameCodeValueChildrenList = new ArrayList<>();
            //筛选数据，获取所有数据中子类ParentGuid等于父类CodeLevel的
            frameCodeValueChildrenList = allMultiList.stream()
                    .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(children.getRowGuid()))
                    .collect(Collectors.toList());
            children.setFrameCodeValueList(frameCodeValueChildrenList);
            // 递归查询
            getChildrenToChildren(frameCodeValueChildrenList, allMultiList);
        }
        return childrenList;
    }

    @Override
    public void cacheCodesMapByValue(String rowGuid) {
        try {
            String codeName = getCodeNameByGuid(rowGuid);
            List<FrameCodeValuePojo> frameCodeValueList = getValueByCodeName(codeName);
            // 若还有代码项值存在
            if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
                // 是否为多维代码标识
                boolean isMultidimensional = false;
                List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                        .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                        .collect(Collectors.toList());
                if (multCodeList.size() > 0) {
                    isMultidimensional = true;
                }
                //新建多维列表
                List<FrameCodeValuePojo> dimensionaCodeList = new ArrayList<>();
                Map<String, List<FrameCodeValuePojo>> dataMap = Maps.newHashMap();
                //@:代码项转为Hash存储
                frameCodeValueList.forEach(frameCodeValue -> {
                    //这里确保名称唯一
                    //若此代码为二维代码的父类
                    if ("".equals(frameCodeValue.getParentGuid())) {
                        //获取他的子类
                        List<FrameCodeValuePojo> frameCodeValueChildrenList;
                        frameCodeValueChildrenList = multCodeList.stream()
                                .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                                .collect(Collectors.toList());
                        List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);
                        frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                        dimensionaCodeList.add(frameCodeValue);
                    }
                    //若遍历到子类，忽略

                });
                if (isMultidimensional) {
                    dataMap.put(codeName, dimensionaCodeList);
                } else {
                    dataMap.put(codeName, frameCodeValueList);
                }
                //@:存入redis
                redisService.hmset(projectName + ":codeValue", JSON.parseObject(JSON.toJSONString(dataMap)));
                //HashOperations<String, String, List<FrameCodeValuePojo>> hashOperations = redisTemplate.opsForHash();
                //hashOperations.putAll(projectName + ":codeValue", JSON.parseObject(JSON.toJSONString(dataMap)));
                logger.info("更新codeValue存入redis");
            } else {
                //删没了就全删了
                redisService.del(projectName + ":codeValue" + ":" + codeName);
//                HashOperations<String, String, List<FrameCodeValuePojo>> hashOperations = redisTemplate.opsForHash();
//                hashOperations.delete(projectName + ":codeValue" + ":" + codeName);
            }
        } catch (Exception e) {
            logger.error("根据详细代码项名称更新存储失败", e.fillInStackTrace());
        }
    }

    @Override
    public List<FrameCodeValuePojo> getValueByCodeName(String codeName) {
        return frameCodeValueDao.getValueByCodeName(codeName);
    }

    @Override
    public String getCodeNameByGuid(String rowGuid) {
        return frameCodeValueDao.getCodeNameByGuid(rowGuid);
    }

    @Override
    public List<FrameCodeValue> getCacheByCodeName(String codeName) {
        List<FrameCodeValue> temp = null;
        try {
            Object object = redisService.hget(projectName + ":codeValue", codeName);
            if (object != null) {
                //防止类对应失败
                temp = JSON.parseArray(JSON.toJSONString(object), FrameCodeValue.class);
            } else {
                logger.error("从缓存中获取codeValue失败,代码项名称：{}", codeName);
            }
        } catch (Exception e) {
            logger.error("从缓存中获取codeValue失败", e.fillInStackTrace());
        }
        return temp;
    }

    /**
     * 代码项遍历赋值
     *
     * @param codeName
     * @param value
     * @return
     */
    @Override
    public String getItemTextByValue(String codeName, Object value) {
        List<FrameCodeValue> frameCodeValueList = getCacheByCodeName(codeName);
        if (!frameCodeValueList.isEmpty()) {
            for (FrameCodeValue frameCodeValue : frameCodeValueList) {
                if (frameCodeValue.getItemValue().equals(value)) {
                    return frameCodeValue.getItemText();
                }
            }
        }
        return null;
    }

    @Override
    public void cacheCodesMapByName(String codeName) {
        try {
            List<FrameCodeValuePojo> frameCodeValueList = getValueByCodeName(codeName);
            // 若还有代码项值存在
            if (frameCodeValueList != null && !frameCodeValueList.isEmpty()) {
                // 是否为多维代码标识
                boolean isMultidimensional = false;
                List<FrameCodeValuePojo> multCodeList = frameCodeValueList.stream()
                        .filter(frameCodeValue -> frameCodeValue.getParentGuid() != null)
                        .collect(Collectors.toList());
                if (multCodeList.size() > 0) {
                    isMultidimensional = true;
                }
                //新建多维列表
                List<FrameCodeValuePojo> dimensionaCodeList = new ArrayList<>();
                Map<String, List<FrameCodeValuePojo>> dataMap = Maps.newHashMap();
                //@:代码项转为Hash存储
                frameCodeValueList.forEach(frameCodeValue -> {
                    //这里确保名称唯一
                    //若此代码为二维代码的父类
                    if ("".equals(frameCodeValue.getParentGuid())) {
                        //获取他的子类
                        List<FrameCodeValuePojo> frameCodeValueChildrenList;
                        frameCodeValueChildrenList = multCodeList.stream()
                                .filter(frameCodeValue1 -> frameCodeValue1.getParentGuid().equals(frameCodeValue.getRowGuid()))
                                .collect(Collectors.toList());
                        List<FrameCodeValuePojo> frameCodeValueGrandSon = getChildrenToChildren(frameCodeValueChildrenList, multCodeList);
                        frameCodeValue.setFrameCodeValueList(frameCodeValueGrandSon);
                        dimensionaCodeList.add(frameCodeValue);
                    }
                    //若遍历到子类，忽略

                });
                if (isMultidimensional) {
                    dataMap.put(codeName, dimensionaCodeList);
                } else {
                    dataMap.put(codeName, frameCodeValueList);
                }
                //@:存入redis
                redisService.hmset(projectName + ":codeValue", JSON.parseObject(JSON.toJSONString(dataMap)));
                logger.info("更新codeValue存入redis");
            } else {
                //删没了就全删了
                redisService.del(projectName + ":codeValue" + ":" + codeName);
//
            }
        } catch (Exception e) {
            logger.error("根据详细代码项名称更新存储失败", e.fillInStackTrace());
        }
    }

    @Override
    public String getItemValueByText(String codeName, String text) {
        List<FrameCodeValue> frameCodeValueList = getCacheByCodeName(codeName);
        if (!frameCodeValueList.isEmpty()) {
            for (FrameCodeValue frameCodeValue : frameCodeValueList) {
                if (frameCodeValue.getItemText().equals(text)) {
                    return frameCodeValue.getItemValue();
                }
            }
        }
        return null;
    }
}
