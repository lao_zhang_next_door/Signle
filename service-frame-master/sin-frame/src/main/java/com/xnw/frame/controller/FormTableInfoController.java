package com.xnw.frame.controller;

import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FormTableInfo;
import com.xnw.frame.service.FormTableInfoService;
import com.xnw.frame.service.FormTableFieldService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * 表信息控制器
 * @creatTime 2021-04-23-13:14
 **/
@CrossOrigin
@RestController
@RequestMapping("/frame/FormTableInfo")
public class FormTableInfoController extends BaseController {

    @Autowired
    private FormTableFieldService formTablefieldService;

    @Autowired
    private FormTableInfoService formTableInfoService;

    @GetMapping("/listData")
    public R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FormTableInfo> list = formTableInfoService.selectByLimit(query);
        int count = formTableInfoService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(list, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @PostMapping("/add")
    private R add(@RequestBody FormTableInfo tableInfo) {

        try {
            formTablefieldService.addTable(tableInfo.getPhysicalName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        tableInfo.init();
        int code = formTableInfoService.insert(tableInfo);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FormTableInfo tableInfo) {
        try {

            FormTableInfo f = formTableInfoService.selectOne(new FormTableInfo(tableInfo.getRowGuid()));
            formTablefieldService.updateTable(f.getPhysicalName(), tableInfo.getPhysicalName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        int code = formTableInfoService.updateByRowGuid(tableInfo);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        for (String rowGuid : rowGuids) {
            FormTableInfo formTableInfo = new FormTableInfo(rowGuid);
            List<FormTableInfo> list = formTableInfoService.select(formTableInfo);
            for (FormTableInfo tableInfo : list) {
                //删除表
                try {
                    formTablefieldService.delTable(tableInfo.getPhysicalName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        formTableInfoService.deleteBatch(rowGuids);
        return R.ok();
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FormTableInfo formTableInfo = new FormTableInfo();
        formTableInfo.setRowGuid(rowGuid);
        List<FormTableInfo> list = formTableInfoService.select(formTableInfo);
        return R.ok().put("data", list);
    }

    /**
     * 代码生成
     *
     * @return
     */
    @PostMapping("/generateCode")
    private void generateCode(@RequestParam Map<String, Object> params) {
        byte[] data = formTableInfoService.generatorCode((String) params.get("tableName"), (String) params.get("tableGuid"), params);

        response.addHeader("Content-Length", data.length + "");
        response.setContentType("application/zip");
//        response.setContentType("/applicationoctet-stream; charset=UTF-8");
        String filename = "code.zip";
        response.addHeader("Content-Disposition", "attachment; filename=" + filename);
        try {
            IOUtils.write(data, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
