package com.xnw.frame.controller;

import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FormTableField;
import com.xnw.frame.service.FormTableFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author T470
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FormTableField")
public class FormTableFieldController extends BaseController {

    @Autowired
    private FormTableFieldService tableFieldService;

    @GetMapping("/listData")
    public R listData(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
        List<FormTableField> list = tableFieldService.selectByLimit(query);
        int count = tableFieldService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(list, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @PostMapping("/add")
    public R addTableField(@RequestBody Map<String, Object> params) throws Exception {
        //插入表字段方法
        Boolean flag = tableFieldService.insertFormField(params);
        return flag ? R.ok(): R.error("表不存在");
    }

    @PutMapping("/updateTableField")
    public R updateTableField(@RequestBody Map<String, Object> params) throws Exception {
        //插入表字段方法
        Boolean flag = tableFieldService.updateFormField(params);
        return flag ? R.ok(): R.error("表不存在");
    }

    @PostMapping("/deleteTableField")
    public R deleteTableField(@RequestBody Map<String, Object> params) throws Exception {
        tableFieldService.deleteFormField(params);
        return R.ok();
    }
}
