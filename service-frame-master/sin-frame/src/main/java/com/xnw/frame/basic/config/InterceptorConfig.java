package com.xnw.frame.basic.config;

import com.xnw.auth.AuthenticationInterceptor;
import com.xnw.frame.basic.base.Base;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.io.IOException;

/**
 * 配置拦截器
 * <p>Title: InterceptorConfig</p>
 * <p>Description: </p>
 *
 * @author hero
 */
@Configuration
public class InterceptorConfig extends Base implements WebMvcConfigurer {


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor()).addPathPatterns("/**").excludePathPatterns("/ws/**", "/com/xnw/frame/auth/**", "/app/**", "/file/**", "/logs/**", "/templates/**").
                excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v3/**", "/swagger-ui.html/**", "/swagger-ui/**", "/mgr", "/mgr/**", "/h5", "/h5/**");
        //添加不拦截路径
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        //registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        //文件磁盘url 映射
        File fileSave = new File(filePath);
        //文件路径（路径+文件名）
        String path = "";
        if (!fileSave.exists()) {
            fileSave.mkdirs();
        }
        try {
            path = fileSave.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String remoteUrl = path.replace("\\", "/") + "/";
        //windows:
//        registry.addResourceHandler("/file/**","/logs/**","/templates/**").addResourceLocations("file:/"+remoteUrl,"file:/"+remoteUrl+"../logs/","classpath:/templates/");
        //linux 
        registry.addResourceHandler("/file/**", "/logs/**", "/templates/**").addResourceLocations("file:" + remoteUrl, "file:/" + remoteUrl + "../logs/", "classpath:/templates/");
    }

    @Bean
    public AuthenticationInterceptor authenticationInterceptor() {
        return new AuthenticationInterceptor();
    }

}
