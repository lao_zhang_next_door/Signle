package com.xnw.frame.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.common.utils.exception.BaseException;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameDeptDao;
import com.xnw.frame.entity.FrameDept;
import com.xnw.frame.entity.pojo.FrameDeptPojo;
import com.xnw.frame.service.FrameDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class FrameDeptServiceImpl extends BaseServiceImpl<FrameDept> implements FrameDeptService {

	public FrameDeptServiceImpl(FrameDeptDao dao) {
		super(dao);
	}

	@Autowired
	private FrameDeptDao frameDeptDao;

	@Override
	public List<FrameDeptPojo> selectParentCode(String rowGuid) {
		return frameDeptDao.selectParentCode(rowGuid);
	}

	@Override
	public JSONArray getStreetAndCommunity(String qGuid) {

		FrameDept dept = new FrameDept();
		dept.setParentGuid(qGuid);
		List<FrameDept> streetList = frameDeptDao.select(dept);
		JSONArray array = new JSONArray();
		streetList.forEach(new Consumer<FrameDept>() {
			@Override
			public void accept(FrameDept frameDept) {
				FrameDept dept = new FrameDept();
				dept.setParentGuid(frameDept.getRowGuid());
				List<FrameDept> communityList = frameDeptDao.select(dept);
				JSONObject object = JSON.parseObject(JSON.toJSONString(frameDept));
				object.put("children",communityList);
				array.add(object);
			}
		});

		return array;
	}

	@Override
	@Transactional
	public void importFrameDept(MultipartFile file) {
		String[] baens = {"streetDeptName","communityDeptName"};

		int startRow = 3;

		JSONObject res = null;
		try {
//			res = PoiUtil.readExcel(file.getInputStream(), file.getOriginalFilename(), baens, startRow);
			res = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(500 == res.getInteger("code")){throw new BaseException(res.getString("msg"));}

		/**
		 * 处理导入数据
		 */
		JSONArray array = res.getJSONArray("data");
		array.forEach(new Consumer<Object>() {
			@Override
			public void accept(Object o) {
				JSONObject json = (JSONObject)o;
				String streetDeptName = json.getString("streetDeptName");
				String communityDeptName = json.getString("communityDeptName");

				FrameDept dept = new FrameDept();
				dept.setDeptName(streetDeptName);
				FrameDept queryDept = frameDeptDao.selectOne(dept);
				if(queryDept == null){
					queryDept = dept;
					queryDept.init();

					//袁州区 固定写死 后续需要传入上级部门guid
					queryDept.setParentGuid("0d848af0-af2b-491c-87e8-88fb0e934eb3");

					frameDeptDao.insertSelective(queryDept);
				};

				FrameDept communityDept = new FrameDept();
				communityDept.setDeptName(communityDeptName);
				if(frameDeptDao.selectCount(communityDept) == 0){
					communityDept.init();
					communityDept.setParentGuid(queryDept.getRowGuid());
					frameDeptDao.insertSelective(communityDept);
				};
			}
		});

		System.out.println(res);

	}

	@Override
	public void save(FrameDept frameDept) {
		frameDeptDao.insertSelective(frameDept);
		System.out.println(frameDept.getRowId());
		if(StringUtil.isBlank(frameDept.getParentGuid())){
			//说明是根节点
			frameDept.setDeptCode(String.valueOf(frameDept.getRowId()));
		}else{
			//子节点
			FrameDept parentDept = frameDeptDao.selectOne(new FrameDept(frameDept.getParentGuid()));
			frameDept.setDeptCode(parentDept.getDeptCode() + "." + frameDept.getRowId());
		}
		frameDeptDao.updateByRowGuidSelective(frameDept);
	}

	@Override
	public void update(FrameDept frameDept) {
		frameDeptDao.updateByRowGuidSelective(frameDept);
	}

	@Override
	public List<FrameDept> getDeptLocation() {
		return frameDeptDao.getDeptLocation();
	}

	@Override
	public List<FrameDept> getUserSonDeptByGuid(String rowGuid) {
		FrameDept frameDept = frameDeptDao.selectOne(new FrameDept(rowGuid));
		if (frameDept != null) {
			FrameDept item = new FrameDept();
			item.setParentGuid(frameDept.getRowGuid());
			return frameDeptDao.select(item);
		} else {
			return new ArrayList<>();
		}
	}
}
