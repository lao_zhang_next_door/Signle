package com.xnw.frame.dao;

import com.xnw.frame.entity.FrameRoleModule;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.pojo.FrameRoleModulePojo;

import java.util.List;

public interface FrameRoleModuleDao extends BaseDao<FrameRoleModule>{

	/**
	 * 联查role表 获取roleName
	 * @return
	 */
	List<FrameRoleModulePojo> selectLJRole(String moduleGuid);

	
}
