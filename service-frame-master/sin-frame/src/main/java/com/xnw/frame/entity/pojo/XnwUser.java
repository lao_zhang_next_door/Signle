package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.FrameRole;

import java.util.List;

/**
 * 登录用户信息
 */
public class XnwUser {

    private String userName;

    private String rowGuid;

    private List<String> deptNames;

    private List<String> deptGuids;

    private String mobile;

    private Integer delFlag;

    private Integer isLimit;

    private String loginId;

    private String password;

    private String sessionId;

    private List<FrameRole> roleList;

    private List<String> roleNameList;

    private List<String> roleGuidList;

    private String avatar;

    private String token;

    private String headimgGuid;

    private String headimgGuidVal;

    private String gongHao;

    public List<String> getDeptNames() {
        return deptNames;
    }

    public void setDeptNames(List<String> deptNames) {
        this.deptNames = deptNames;
    }

    public List<String> getDeptGuids() {
        return deptGuids;
    }

    public void setDeptGuids(List<String> deptGuids) {
        this.deptGuids = deptGuids;
    }

    public List<FrameRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<FrameRole> roleList) {
        this.roleList = roleList;
    }

    public List<String> getRoleNameList() {
        return roleNameList;
    }

    public void setRoleNameList(List<String> roleNameList) {
        this.roleNameList = roleNameList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<String> getRoleGuidList() {
        return roleGuidList;
    }

    public void setRoleGuidList(List<String> roleGuidList) {
        this.roleGuidList = roleGuidList;
    }

    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getIsLimit() {
        return isLimit;
    }

    public void setIsLimit(Integer isLimit) {
        this.isLimit = isLimit;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHeadimgGuidVal() {
        return headimgGuidVal;
    }

    public void setHeadimgGuidVal(String headimgGuidVal) {
        this.headimgGuidVal = headimgGuidVal;
    }

    public String getHeadimgGuid() {
        return headimgGuid;
    }

    public void setHeadimgGuid(String headimgGuid) {
        this.headimgGuid = headimgGuid;
    }

    public String getGongHao() {
        return gongHao;
    }

    public void setGongHao(String gongHao) {
        this.gongHao = gongHao;
    }
}
