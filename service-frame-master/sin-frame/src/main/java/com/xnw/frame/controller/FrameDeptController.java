package com.xnw.frame.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.R;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FrameDept;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.pojo.FrameDeptPojo;
import com.xnw.frame.service.FrameDeptService;
import com.xnw.frame.service.FrameUserService;
import com.xnw.frame.basic.mybatis.mapper.entity.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 部门控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameDept")
public class FrameDeptController extends BaseController {

    @Autowired
    private FrameDeptService frameDeptService;
    @Autowired
    private FrameUserService frameUserService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameDept> deptList = frameDeptService.selectByLimit(query);
        int count = frameDeptService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(deptList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameDept frameDept = new FrameDept();
        frameDept.setRowGuid(rowGuid);
        List<FrameDept> deptList = frameDeptService.select(frameDept);
        return R.ok().put("data", deptList);
    }

    /**
     * 新增部门
     *
     * @param frameDept
     * @return
     */
    @PostMapping("/add")
    private R add(@RequestBody FrameDept frameDept) {
        frameDept.initNull();
        frameDeptService.save(frameDept);
        return R.ok();
    }

    /**
     * 修改部门
     *
     * @param frameDept
     * @return
     */
    @PutMapping("/update")
    private R update(@RequestBody FrameDept frameDept) {
        frameDeptService.update(frameDept);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameDeptService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 获取树数据
     *
     * @param frameDept
     * @return
     */
    @PostMapping("/treeData")
    private R treeData(@RequestBody FrameDept frameDept) {
        Example example = new Example(FrameDept.class);
        // 创建Criteria
        Example.Criteria criteria = example.createCriteria();
        example.orderBy("sortSq").desc();
        criteria.andEqualTo("parentGuid", frameDept.getParentGuid());
        List<FrameDept> deptList = frameDeptService.selectByExample(example);
        JSONArray array = JSON.parseArray(JSON.toJSONString(deptList));
        JSONObject object = null;
        FrameDept mod = null;
        for (int i = 0; i < array.size(); i++) {
            object = array.getJSONObject(i);
            mod = new FrameDept();
            mod.setParentGuid(object.getString("rowGuid"));
            int count = frameDeptService.selectCount(mod);
            if (count > 0) {
                object.put("children", new ArrayList());
            }
        }
        return R.ok().put("data", array);
    }

    /**
     * 根据rowGuid 联查(left join frameDept) 查询上级deptName
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    private R selectParentCode(@RequestParam String rowGuid) {
        List<FrameDeptPojo> deptList = frameDeptService.selectParentCode(rowGuid);
        return R.ok().put("data", deptList);
    }

    /**
     * 以固定格式 获取当前部门的上级部门所有人员的树数据
     *
     * @param frameDept
     * @return
     */
    @PostMapping("/treeDataPerson")
    private R treeDataPerson(@RequestBody FrameDept frameDept) {
        List<FrameUser> frameUserList = null;
        FrameDept frameDeptEntity = frameDeptService.selectOne(frameDept);
        if (frameDeptEntity != null && StringUtil.isNotBlank(frameDeptEntity.getParentGuid())) {
            String parentGuid = frameDeptEntity.getParentGuid();
            if (StringUtil.isNotBlank(parentGuid)) {
                FrameUser frameUser = new FrameUser();
                // @TODO
                // frameUser.setDeptGuid(parentGuid);
                frameUserList = frameUserService.select(frameUser);
            }
        }

        JSONArray array = JSON.parseArray(JSON.toJSONString(frameUserList));
        return R.ok().put("data", array);
    }

    /**
     * 导入行政区划
     *
     * @return
     */
    @PostMapping("/importFrameDept")
    public R importFrameDept(@RequestParam(value = "file") MultipartFile multipartFile) {
        try {
            frameDeptService.importFrameDept(multipartFile);
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
        return R.ok();
    }


    /**
     * 获取所有有经纬度的部门
     *
     * @return
     */
    @GetMapping("/getDeptLocation")
    private R getDeptLocation() {
        List<FrameDept> deptList = frameDeptService.getDeptLocation();
        return R.ok().put("data", deptList);
    }

}
 