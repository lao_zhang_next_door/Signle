package com.xnw.frame.dao;

import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.pojo.FrameCodeValuePojo;

import java.util.List;

public interface FrameCodeValueDao extends BaseDao<FrameCodeValue> {

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    List<FrameCodeValuePojo> selectParentCode(String rowGuid);

    /**
     * 获取所有代码项
     *
     * @return list
     */
    List<FrameCodeValuePojo> getCodeValueToCache();

    /**
     * 根据代码项名称查询
     *
     * @param codeName
     * @return
     */
    List<FrameCodeValuePojo> getValueByCodeName(String codeName);

    /**
     * 按guid查询代码项名称
     *
     * @param rowGuid
     * @return
     */
    String getCodeNameByGuid(String rowGuid);
}
