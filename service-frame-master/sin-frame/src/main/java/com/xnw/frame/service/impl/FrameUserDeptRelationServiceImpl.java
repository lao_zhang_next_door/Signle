package com.xnw.frame.service.impl;

import com.xnw.frame.entity.FrameDept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameUserDeptRelationDao;
import com.xnw.frame.entity.FrameUserDeptRelation;
import com.xnw.frame.service.FrameUserDeptRelationService;

import java.util.List;

/**
 * @author T470
 */
@Service
public class FrameUserDeptRelationServiceImpl extends BaseServiceImpl<FrameUserDeptRelation> implements FrameUserDeptRelationService {

    public FrameUserDeptRelationServiceImpl(FrameUserDeptRelationDao dao) {
        super(dao);
    }

    @Autowired
    private FrameUserDeptRelationDao frameUserDeptRelationDao;

    @Override
    public void addUserDeptRelation(String userGuid, String[] deptGuids) {
        // 先删除再新增
        frameUserDeptRelationDao.deleteByUserGuid(userGuid);
        for (String deptGuid : deptGuids) {
            FrameUserDeptRelation frameUserDeptRelation = new FrameUserDeptRelation();
            frameUserDeptRelation.initNull();
            frameUserDeptRelation.setUserGuid(userGuid);
            frameUserDeptRelation.setDeptGuid(deptGuid);
            frameUserDeptRelationDao.insert(frameUserDeptRelation);
        }

    }

    @Override
    public void deleteByUserGuid(String userGuid) {
        frameUserDeptRelationDao.deleteByUserGuid(userGuid);
    }

    @Override
    public List<FrameDept> getUserRelationDept(String userGuid) {
        return frameUserDeptRelationDao.getUserRelationDept(userGuid);
    }
}

