package com.xnw.frame.controller;

import com.xnw.common.utils.frame.Query;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FrameCode;
import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.frame.service.FrameCodeService;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.service.FrameCodeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 代码项控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameCode")
public class FrameCodeController extends BaseController {

    @Autowired
    private FrameCodeService frameCodeService;

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameCode> roleList = frameCodeService.selectByLimit(query);
        int count = frameCodeService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(roleList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameCode frameCode = new FrameCode();
        frameCode.setRowGuid(rowGuid);
        List<FrameCode> codeList = frameCodeService.select(frameCode);
        return R.ok().put("data", codeList);
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameCode frameCode) {
        frameCode.initNull();
        int code = frameCodeService.insert(frameCode);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameCode frameCode) {
        int code = frameCodeService.updateByRowGuidSelective(frameCode);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameCodeService.deleteBatch(rowGuids);
        return R.ok();
    }

    @GetMapping("/selectByCodeName")
    private R selectByCodeName(@RequestParam String codeName) {
        FrameCode frameCode = new FrameCode();
        frameCode.setCodeName(codeName);
        List<FrameCode> codeList = frameCodeService.select(frameCode);
        if(codeList.isEmpty()){
           return R.error(codeName+"代码项为空");
        }
        if (codeList.size() > 1) {
            return R.error("系统异常,有重复代码项》》》" + codeName);
        }
        FrameCodeValue codeValue = new FrameCodeValue();
        codeValue.setCodeGuid(codeList.get(0).getRowGuid());
        return R.ok().put("data", frameCodeValueService.select(codeValue));
    }

    @GetMapping("/selectCacheByCodeName")
    private R selectCacheByCodeName(@RequestParam String codeName) {
        return R.ok().put("data", frameCodeValueService.getCacheByCodeName(codeName));
    }
}
 