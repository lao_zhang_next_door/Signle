package com.xnw.frame.rabbitmq.basic;

import org.springframework.amqp.core.*;

import java.util.Map;

public class ExchangeBuilder extends AbstractBuilder {

    private final String name;

    private final String type;

    private boolean durable;

    private boolean autoDelete;

    private boolean internal;

    private boolean delayed;

    /**
     * Construct an instance of the appropriate type.
     *
     * @param name the exchange name
     * @param type the type name
     * @see ExchangeTypes
     * @since 1.6.7
     */
    public ExchangeBuilder(String name, String type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Return a {@link DirectExchange} builder.
     *
     * @param name the name.
     * @return the builder.
     */
    public static ExchangeBuilder directExchange(String name) {
        return new ExchangeBuilder(name, ExchangeTypes.DIRECT);
    }

    /**
     * Return a {@link TopicExchange} builder.
     *
     * @param name the name.
     * @return the builder.
     */
    public static ExchangeBuilder topicExchange(String name) {
        return new ExchangeBuilder(name, ExchangeTypes.TOPIC);
    }

    /**
     * Return a {@link FanoutExchange} builder.
     *
     * @param name the name.
     * @return the builder.
     */
    public static ExchangeBuilder fanoutExchange(String name) {
        return new ExchangeBuilder(name, ExchangeTypes.FANOUT);
    }

    /**
     * Return a {@link HeadersExchange} builder.
     *
     * @param name the name.
     * @return the builder.
     */
    public static ExchangeBuilder headersExchange(String name) {
        return new ExchangeBuilder(name, ExchangeTypes.HEADERS);
    }

    /**
     * 当该exchange所有的队列都被unbind之后，该exchange自动被删除
     * Set the auto delete flag.
     *
     * @return the builder.
     */
    public ExchangeBuilder autoDelete() {
        this.autoDelete = true;
        return this;
    }

    /**
     * Set the durable flag to true.
     *
     * @return the builder.
     * @see #durable(boolean)
     * @deprecated - in 2.0, durable will be true by default
     */
    @Deprecated
    public ExchangeBuilder durable() {
        this.durable = true;
        return this;
    }

    /**
     * 持久化 当RabbitMQ崩溃了重启后exchange仍然存在
     * Set the durable flag.
     *
     * @param durable the durable flag (default false).
     * @return the builder.
     * @see #durable
     * <p>
     * 设置exchange为持久化之后，并不能保证消息不丢失，
     * 因为此时发送往exchange中的消息并不是持久化的，需要配置delivery_mode=2指明message为持久的。
     * FanoutExchange 发送的消息默认就是持久化的。
     */
    public ExchangeBuilder durable(boolean durable) {
        this.durable = durable;
        return this;
    }

    /**
     * Add an argument.
     *
     * @param key   the argument key.
     * @param value the argument value.
     * @return the builder.
     */
    public ExchangeBuilder withArgument(String key, Object value) {
        getOrCreateArguments().put(key, value);
        return this;
    }

    /**
     * Add the arguments.
     *
     * @param arguments the arguments map.
     * @return the builder.
     */
    public ExchangeBuilder withArguments(Map<String, Object> arguments) {
        this.getOrCreateArguments().putAll(arguments);
        return this;
    }

    /**
     * Set the internal flag.
     *
     * @return the builder.
     */
    public ExchangeBuilder internal() {
        this.internal = true;
        return this;
    }

    /**
     * Set the delayed flag.
     *
     * @return the builder.
     */
    public ExchangeBuilder delayed() {
        this.delayed = true;
        return this;
    }

    public Exchange build() {
        AbstractExchange exchange;
        if (ExchangeTypes.DIRECT.equals(this.type)) {
            exchange = new DirectExchange(this.name, this.durable, this.autoDelete, getArguments());
        } else if (ExchangeTypes.TOPIC.equals(this.type)) {
            exchange = new TopicExchange(this.name, this.durable, this.autoDelete, getArguments());
        } else if (ExchangeTypes.FANOUT.equals(this.type)) {
            exchange = new FanoutExchange(this.name, this.durable, this.autoDelete, getArguments());
        } else if (ExchangeTypes.HEADERS.equals(this.type)) {
            exchange = new HeadersExchange(this.name, this.durable, this.autoDelete, getArguments());
        } else {
            throw new IllegalStateException("Invalid type: " + this.type);
        }
        exchange.setInternal(this.internal);
        exchange.setDelayed(this.delayed);
        return exchange;
    }
}
