package com.xnw.frame.service;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.frame.entity.pojo.FrameModulePojo;
import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameModule;

import java.util.List;

public interface FrameModuleService extends BaseService<FrameModule> {

    /**
     * 根据rowGuid 联查(left join frameModule)
     *
     * @param rowGuid
     * @return
     */
    List<FrameModulePojo> selectParentCode(String rowGuid);

    /**
     * 一次获取全部树数据
     *
     * @return
     */
    JSONObject allTreeData();

    /**
     * 一次获取全部路由vue2
     *
     * @return
     */
    JSONArray allTreeDataRoute();

    /**
     * 获取全部路由vue3
     *
     * @return
     */
    JSONArray getVue3Route();
}
