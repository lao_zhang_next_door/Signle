package com.xnw.frame.service.impl;

import com.xnw.common.utils.dataformat.MD5Util;
import com.xnw.common.utils.exception.BaseException;
import com.xnw.common.utils.frame.CommUtil;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.StringUtil;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameUserDao;
import com.xnw.frame.entity.*;
import com.xnw.frame.entity.pojo.FrameUserPasswordVo;
import com.xnw.frame.entity.pojo.FrameUserPojo;
import com.xnw.frame.entity.pojo.XnwUser;
import com.xnw.frame.enumeration.IsOrNot;
import com.xnw.frame.enumeration.UserStatus;
import com.xnw.frame.service.*;
import com.xnw.redis.RedisService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class FrameUserServiceImpl extends BaseServiceImpl<FrameUser> implements FrameUserService {

    public FrameUserServiceImpl(FrameUserDao dao) {
        super(dao);
    }

    @Value("${user.redis-name}")
    private String userDataRedisName;

    @Autowired
    private FrameUserDao frameUserDao;

    @Autowired
    private FrameUserRoleService frameUserRoleService;

    @Autowired
    private FrameConfigService frameConfigService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private FrameRoleService frameRoleService;

    @Autowired
    private FrameUserDeptRelationService frameUserDeptRelationService;

    @Override
    public List<FrameUserPojo> getList(Query query) {

        AtomicReference<List<FrameDept>> frameDeptList = new AtomicReference<>();
        List<FrameUserPojo> frameUserPojoList = frameUserDao.getList(query);
        frameUserPojoList.forEach(frameUserPojo -> {
            List<String> deptNames = new ArrayList<>();
            frameDeptList.set(frameUserDeptRelationService.getUserRelationDept(frameUserPojo.getRowGuid()));
            frameDeptList.get().forEach(item -> deptNames.add(item.getDeptName()));
            frameUserPojo.setDeptNames(deptNames);
        });
        return frameUserPojoList;
    }

    @Override
    public FrameUserPojo selectDetailByUserGuid(String userGuid) {
        return frameUserDao.selectDetailByUserGuid(userGuid);
    }

    @Override
    public FrameUserPojo selectDetailByLoginId(String userGuid) {
        return frameUserDao.selectDetailByLoginId(userGuid);
    }

    @Override
    public List<FrameRole> selectRoleListByUserGuid(String userGuid) {
        return frameUserDao.selectRoleListByUserGuid(userGuid);
    }

    @Override
    public String addFrameUser(FrameUserPojo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())) {
            throw new BaseException("登录名不可为空");
        }

        //查询登录名是否重复
        FrameUser searchData = new FrameUser();
        searchData.setLoginId(frameUserPojo.getLoginId());
        int count = selectCount(searchData);
        if (count > 0) {
            throw new BaseException("已存在该登录账户");
        }

        //初始化用户并设置初始密码
        frameUserPojo.init();
        frameUserPojo.setStatus(UserStatus.NORMAL.getCode());
        frameUserPojo.setIsLimit(IsOrNot.NOT.getCode());
        //设置初始密码 设置失败则禁用
        if (StringUtils.isBlank(this.getInitPassword())) {
            frameUserPojo.setStatus(UserStatus.DISABLE.getCode());
        } else {
            frameUserPojo.setPassword(this.getInitPassword());
        }
        //新增用户部门关联
        String[] deptGuids = frameUserPojo.getDeptGuidStr().split(",");
        frameUserDeptRelationService.addUserDeptRelation(frameUserPojo.getRowGuid(), deptGuids);
        this.insertSelective(frameUserPojo);

        //插入用户所拥有的角色
        List<FrameRole> roleList = frameUserPojo.getRoleList();
        List<FrameUserRole> frameUserRoleList = new ArrayList<FrameUserRole>();

        if (roleList != null) {
            FrameUserRole fur = null;
            for (FrameRole role : roleList) {
                fur = new FrameUserRole(true);
                fur.setUserGuid(frameUserPojo.getRowGuid());
                fur.setRoleGuid(role.getRowGuid());
                frameUserRoleList.add(fur);
            }
            if (!frameUserRoleList.isEmpty()) {
                frameUserRoleService.insertList(frameUserRoleList);
            }
        }
        return frameUserPojo.getRowGuid();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateFrameUser(FrameUserPojo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())
                || StringUtil.isBlank(frameUserPojo.getRowGuid())) {
            throw new BaseException("参数不可为空");
        }

        FrameUser searchData = new FrameUser();
        searchData.setRowGuid(frameUserPojo.getRowGuid());
        FrameUser originUser = selectOne(searchData);

        if (!StringUtil.isBlank(frameUserPojo.getPassword())) {
            throw new BaseException("参数不可更新");
        }

        if (!frameUserPojo.getLoginId().equals(originUser.getLoginId())) {
            throw new BaseException("登录名不可修改");
        }
        //新增用户部门关联
        String[] deptGuids = frameUserPojo.getDeptGuidStr().split(",");
        frameUserDeptRelationService.addUserDeptRelation(frameUserPojo.getRowGuid(), deptGuids);
        updateByRowGuidSelective(frameUserPojo);

        //删除
        frameUserRoleService.delete(new FrameUserRole().setUserGuid(frameUserPojo.getRowGuid()));
        //再新增
        List<FrameRole> roleList = frameUserPojo.getRoleList();
        if (roleList != null && !roleList.isEmpty()) {
            List<FrameUserRole> userRoles = new ArrayList<>();
            roleList.forEach(frameRole -> {
                FrameUserRole frameUserRole = new FrameUserRole(true)
                        .setRoleGuid(frameRole.getRowGuid())
                        .setUserGuid(frameUserPojo.getRowGuid());
                userRoles.add(frameUserRole);
            });
            frameUserRoleService.insertList(userRoles);
        }

    }

    @Override
    public String getInitPassword() {

        FrameConfig config = new FrameConfig();
        config.setConfigName("initPassword");
        config = frameConfigService.selectOne(config);

        //加密
        if (!StringUtils.isBlank(config.getConfigValue())) {
            return MD5Util.md5Password(config.getConfigValue()).toLowerCase();
        } else {
            return "";
        }
    }

    @Override
    public void changePassword(FrameUserPasswordVo frameUserPasswordVo) {
        isFieldBlank(frameUserPasswordVo, "userGuid", "originPassword", "newPassword");

        if (frameUserPasswordVo.getNewPassword().equals(frameUserPasswordVo.getOriginPassword())) {
            throw new BaseException("修改后的密码不可与原来的一致");
        }

        if (!frameUserPasswordVo.getNewPassword()
                .equals(frameUserPasswordVo.getVerifyPassword())) {
            throw new BaseException("两次输入密码不一致");
        }

        FrameUser frameUser = frameUserDao.selectOne(new FrameUser(frameUserPasswordVo.getUserGuid()));
        if (frameUser == null) {
            throw new BaseException("修改密码失败");
        }

        if (!frameUser.getPassword().equals(frameUserPasswordVo.getOriginPassword())) {
            throw new BaseException("修改密码失败");
        }

        frameUserDao.updateByRowGuidSelective(
                new FrameUser(frameUser.getRowGuid()).setPassword(frameUserPasswordVo.getNewPassword()));

    }

    @Override
//	@PreAuthorize("hasAuthority('管理员') or hasAuthority('社区管理员')")
    public List<FrameUser> getUserByCurrentAuthority() {

//		Map<String,Object> userMap = getCurrentUser();
//		String deptGuid = (String) userMap.get("deptGuid");
//
//		Example example = new Example(FrameUser.class);
//		example.selectProperties("userName","loginId","rowGuid","createTime","deptGuid","mobile");
//		if(permissionService.hasPerm("管理员")){
//			example.createCriteria().andEqualTo("delFlag", DelFlag.Normal.getCode());
//			return frameUserDao.selectByExample(example);
//		}else{
//			example.createCriteria().andEqualTo("deptGuid",deptGuid).andEqualTo("delFlag", DelFlag.Normal.getCode());
//			return frameUserDao.selectByExample(example);
//		}
        return null;
    }

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     *
     * @return
     */
    @Override
    public List<FrameUser> getParentDeptUser() {
//		Map<String,Object> userMap = getCurrentUser();
//		FrameDept dept = frameDeptService.selectOne(new FrameDept(String.valueOf(userMap.get("deptGuid"))));
        FrameUser user = new FrameUser();
//		user.setDeptGuid(dept.getParentGuid());
        return frameUserDao.select(user);
    }

    /**
     * 用户注册
     *
     * @param user
     * @param session
     */
    @Override
    public void registerAppUser(FrameUserPojo user, HttpSession session) {

        //校验手机号码是否已经注册
        FrameUser frame = new FrameUser();
        frame.setLoginId(user.getLoginId());
        List<FrameUser> list = frameUserDao.select(frame);
        if (list.size() != 0) {
            throw new BaseException("该用户已经注册，请不要重复注册！");
        }
        //校验ImgCode(验证码是否正确)
        if (session.getAttribute("img_code") == null) {
            throw new BaseException("请刷新验证码再试");
        }
        if (user.getImgCode() != null && !user.getImgCode().toUpperCase().equals(session.getAttribute("img_code").toString())) {
            throw new BaseException("验证码错误");
        }

        //将数据保存
        user.initNull();
        frameUserDao.insert(user);

    }

    /**
     * 校验验证码
     *
     * @param isForget
     * @param session
     */
    @Override
    public void checkIsSetImgCode(String isForget, HttpSession session) {
        if ("forget".equals(isForget)) {
            if (session.getAttribute("img_code_forget") != null) {
                throw new BaseException("请不要重复点击");
            }
        } else if (session.getAttribute("img_code") != null) {
            throw new BaseException("请不要重复点击");
        }
    }

    /**
     * 忘记密码
     *
     * @param user
     * @param session
     */
    @Override
    public void forgetPassword(FrameUserPojo user, HttpSession session) {

        //校验手机号码是否已经注册
        FrameUser frame = new FrameUser();
        frame.setLoginId(user.getLoginId());
        List<FrameUser> list = frameUserDao.select(frame);
        if (list.size() == 0) {
            throw new BaseException("该用户尚未注册！");
        }
        //校验ImgCode(验证码是否正确)
        if (session.getAttribute("img_code_forget") == null) {
            throw new BaseException("请刷新验证码再试");
        }
        if (user.getImgCode() != null && !user.getImgCode().toUpperCase().equals(session.getAttribute("img_code_forget").toString())) {
            throw new BaseException("验证码错误");
        }

        //将数据更新
        FrameUser userInfo = list.get(0);
        userInfo.setPassword(user.getPassword());
        frameUserDao.updateByRowGuidSelective(userInfo);

    }

    @Override
    public void cleanCacheUser() {
        redisService.removePattern(CommUtil.getRedisKey(userDataRedisName + "*"));
        logger.info("用户缓存信息已清除！");
    }

    @Override
    public XnwUser getUserInfoByLogin(String loginId) {
        XnwUser xnwUser = frameUserDao.getUserInfoByLogin(loginId);
        if (xnwUser != null) {
            //用户部门
            List<FrameDept> frameDeptList = frameUserDeptRelationService.getUserRelationDept(xnwUser.getRowGuid());
            List<String> deptNames = new ArrayList<>();
            List<String> deptGuids = new ArrayList<>();
            frameDeptList.forEach(item -> {
                deptNames.add(item.getDeptName());
                deptGuids.add(item.getRowGuid());
            });
            xnwUser.setDeptGuids(deptGuids);
            xnwUser.setDeptNames(deptNames);
            //用户角色
            List<Map<String, Object>> mapList = frameRoleService.getRolesByUserGuid(xnwUser.getRowGuid());
            List<String> roleNameList = new ArrayList<>();
            List<String> roleGuidList = new ArrayList<>();
            for (Map<String, Object> map : mapList) {
                roleNameList.add(map.get("roleName").toString());
                roleGuidList.add(map.get("roleGuid").toString());
            }
            xnwUser.setRoleGuidList(roleGuidList);
            xnwUser.setRoleNameList(roleNameList);
        }
        return xnwUser;
    }
}
