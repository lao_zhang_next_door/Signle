package com.xnw.frame.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.xnw.common.utils.frame.Query;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.pojo.FrameModulePojo;
import com.xnw.frame.service.FrameModuleService;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 模块控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameModule")
public class FrameModuleController extends BaseController {

    @Autowired
    private FrameModuleService frameModuleService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameModule> moduleList = frameModuleService.selectByLimit(query);
        int count = frameModuleService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(moduleList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameModule frameModule = new FrameModule();
        frameModule.setRowGuid(rowGuid);
        List<FrameModule> moduleList = frameModuleService.select(frameModule);
        return R.ok().put("data", moduleList);
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameModule frameModule) {
        frameModule.initNull();
        frameModuleService.insert(frameModule);
        return R.ok().put("data", frameModule);
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameModule frameModule) {
        frameModuleService.updateByRowGuidSelective(frameModule);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameModuleService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 异步获取树数据
     *
     * @param frameModule
     * @return
     */
    @PostMapping("/treeData")
    private R treeData(@RequestBody FrameModule frameModule) {
        List<FrameModule> moduleList = frameModuleService.select(frameModule);
        JSONArray array = JSON.parseArray(JSON.toJSONString(moduleList));
        JSONObject object = null;
        FrameModule mod = null;
        for (int i = 0; i < array.size(); i++) {
            object = array.getJSONObject(i);
            mod = new FrameModule();
            mod.setParentGuid(object.getString("rowGuid"));
            int count = frameModuleService.selectCount(mod);
            if (count > 0) {
                object.put("children", new ArrayList());
            }
        }
        return R.ok().put("data", array);
    }

    /**
     * 一次获取全部树数据
     *
     * @return
     */
    @GetMapping("/allTreeData")
    private R allTreeData() {
        JSONObject obj = frameModuleService.allTreeData();
        return R.ok().put("data", obj.get("array"));
    }

    /**
     * 一次获取全部路由Vue2
     *
     * @return
     */
    @GetMapping("/allTreeDataRoute")
    private R allTreeDataRoute() {
        JSONArray array = frameModuleService.allTreeDataRoute();
        return R.ok().put("data", array);
    }

    /**
     * 一次获取全部路由Vue3
     *
     * @return
     */
    @GetMapping("/getAllVue3Route")
    private R getAllVue3Route() {
        JSONArray array = frameModuleService.getVue3Route();
        return R.ok().put("data", array);
    }

    /**
     * 根据rowGuid 联查(left join frameModule)
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    private R selectParentCode(@RequestParam String rowGuid) {
        List<FrameModulePojo> CodeValueList = frameModuleService.selectParentCode(rowGuid);
        return R.ok().put("data", CodeValueList);
    }

}
 