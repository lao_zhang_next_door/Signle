package com.xnw.frame.rabbitmq.basic;
 
public abstract class ExchangeTypes {

	public static final String DIRECT = "direct";

	public static final String TOPIC = "topic";

	public static final String FANOUT = "fanout";

	public static final String HEADERS = "headers";

	public static final String SYSTEM = "system";
}
