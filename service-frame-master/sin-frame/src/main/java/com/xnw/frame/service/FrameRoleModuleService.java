package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameRoleModule;
import com.xnw.frame.entity.pojo.FrameRoleModulePojo;

import java.util.List;

public interface FrameRoleModuleService extends BaseService<FrameRoleModule> {
	
	/**
	 * 根据 模块guid 联查role表 
	 * @return
	 */
	List<FrameRoleModulePojo> selectLJRole(String moduleGuid);

}
