package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FormTableField;

import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-21-10:59
 **/
public interface FormTableFieldService extends BaseService<FormTableField> {

    /**
     * @param map
     * @return
     */
    List<FormTableField> selectFormTableFieldList(Map<String, Object> map);

    Boolean insertFormField(Map<String, Object> params) throws Exception;

    /**
     * 修改表单
     *
     * @param params
     * @return
     */
    Boolean updateFormField(Map<String, Object> params) throws Exception;

    /**
     * 删除表单
     *
     * @param params
     * @return
     */
    Boolean deleteFormField(Map<String, Object> params) throws Exception;

    /**
     * 删除数据表
     *
     * @param tableName
     */
    void delTable(String tableName) throws Exception;

    /**
     * 新增数据表
     *
     * @param tableName
     * @throws Exception
     */
    void addTable(String tableName) throws Exception;

    /**
     * 更新数据表
     *
     * @param tableName
     * @throws Exception
     */
    void updateTable(String tableName, String newTableName) throws Exception;
}
