package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FormTableInfo;

import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:16
 **/
public interface FormTableInfoService extends BaseService<FormTableInfo> {

	/**
	 * 代码生成
	 * @param tableName
	 * @param tableGuid
	 * @param params
	 */
	byte[] generatorCode(String tableName, String tableGuid, Map<String, Object> params);
	
	

}
