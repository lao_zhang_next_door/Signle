package com.xnw.frame.dao;

import com.xnw.frame.entity.FormTableInfo;
import com.xnw.frame.basic.base.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * @author jtr
 * @creatTime 2021-04-21-11:05
 **/
public interface FormTableInfoDao extends BaseDao<FormTableInfo> {

	/**
	 * 根据表名查询表信息
	 * @param tableName
	 * @return
	 */
	Map<String, String> queryTable(String tableName);

	/**
	 * 根据表名查询列信息
	 * @param tableName
	 * @return
	 */
	List<Map<String, String>> queryColumns(String tableName);

    
}
