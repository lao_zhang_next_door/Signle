package com.xnw.frame.basic.base.service;

public interface BaseDeleteService<T> {
	
	/**
     * 根据实体属性作为条件进行删除，查询条件使用等号
     *
     * @param record
     * @return
     */
    int delete(T record);
    
    /**
     * 根据主键字段进行删除，方法参数必须包含完整的主键属性
     *
     * @param key
     * @return
     */
    int deleteByPrimaryKey(Object key);
    
    /**
     * 根据rowGuids 数组批量删除
     * @param rowGuids
     * @return
     */
    int deleteBatch(String[] rowGuids);

}
