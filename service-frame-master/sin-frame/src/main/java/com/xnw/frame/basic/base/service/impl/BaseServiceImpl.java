package com.xnw.frame.basic.base.service.impl;

import com.xnw.frame.basic.base.Base;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.basic.mybatis.mapper.entity.Example;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public class BaseServiceImpl<T> extends Base implements BaseService<T> {

    BaseDao<T> dao;

    public BaseServiceImpl(BaseDao<T> dao) {
        this.dao = dao;
    }

    public BaseServiceImpl() {
    }

    /**
     * 根据实体中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     */
    @Override
    public T selectOne(T record) {
        return dao.selectOne(record);
    }

    /**
     * 根据map中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     */
    @Override
    public T selectOne(Map param) {
        return dao.selectOneByMap(param);
    }

    /**
     * 根据实体中的属性值进行查询，查询条件使用等号
     */
    @Override
    public List<T> select(T record) {
        return dao.select(record);
    }

    /**
     * 根据map中的属性值进行查询，查询条件使用等号
     *
     * @param param
     * @return
     */
    @Override
    public List<T> select(Map param) {
        return dao.selectByMap(param);
    }

    /**
     * (分页)根据实体中的属性值进行查询，查询条件使用等号
     */
    @Override
    public List<T> selectByLimit(Map record) {
        return dao.selectByLimit(record);
    }

    /**
     * 查询全部结果
     */
    @Override
    public List<T> selectAll() {
        return dao.selectAll();
    }

    /**
     * 根据实体中的属性查询总数，查询条件使用等号
     */
    @Override
    public int selectCount(T record) {
        return dao.selectCount(record);
    }

    /**
     * (分页)根据实体中的属性查询总数，查询条件使用等号
     */
    @Override
    public int selectCountByLimit(Map record) {
        return dao.selectCountByLimit(record);
    }

    /**
     * 根据主键字段进行查询，方法参数必须包含完整的主键属性，查询条件使用等号
     */
    @Override
    public T selectByPrimaryKey(Object key) {
        return dao.selectByPrimaryKey(key);
    }

    /**
     * 根据example查询
     *
     * @param example
     * @return
     */
    @Override
    public List<T> selectByExample(Example example) {
        return dao.selectByExample(example);
    }

    /**
     * 根据example查询
     *
     * @param example
     * @return
     */
    @Override
    public T selectOneByExample(Example example) {
        return dao.selectOneByExample(example);
    }

    /**
     * 根据实体中的属性进行查询，返回单一集合，返回List<String>
     *
     * @param example
     * @return
     */
    @Override
    public List<String> selectListOnlyByExample(Example example) {
        return dao.selectListOnlyByExample(example);
    }

    @Override
    public List<T> selectByExampleAndRowBounds(Example example, RowBounds rowBounds) {
        return dao.selectByExampleAndRowBounds(example, rowBounds);
    }

    @Override
    public Integer selectCountByExample(Example example) {
        return dao.selectCountByExample(example);
    }

    /**
     * 保存一个实体，null的属性也会保存，不会使用数据库默认值
     */
    @Override
    public int insert(T record) {
        return dao.insert(record);
    }

    /**
     * 保存一个实体，null的属性不会保存，会使用数据库默认值
     */
    @Override
    public int insertSelective(T record) {
        return dao.insertSelective(record);
    }

    /**
     * 批量保存
     */
    @Override
    public int insertList(List<T> list) {
        return dao.insertList(list);
    }

    /**
     * 根据实体属性作为条件进行删除，查询条件使用等号
     */
    @Override
    public int delete(T record) {
        return dao.delete(record);
    }

    /**
     * 根据主键字段进行删除，方法参数必须包含完整的主键属性
     */
    @Override
    public int deleteByPrimaryKey(Object key) {
        return dao.deleteByPrimaryKey(key);
    }

    /**
     * 根据rowGuids 数组批量删除
     */
    @Override
    public int deleteBatch(String[] rowGuids) {
        return dao.deleteBatch(rowGuids);
    }

    /**
     * 根据rowGuids 数组批量删除
     */
    @Override
    public int deleteBatchLogic(String[] rowGuids) {
        return dao.deleteBatchLogic(rowGuids);
    }

    /**
     * 根据主键更新实体全部字段，null值会被更新
     */
    @Override
    public int updateByPrimaryKey(T record) {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * 根据主键更新属性不为null的值
     */
    @Override
    public int updateByPrimaryKeySelective(T record) {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据rowGuid更新实体全部字段，null值会被更新
     */
    @Override
    public int updateByRowGuid(T record) {
        return dao.updateByRowGuid(record);
    }

    /**
     * 根据rowGuid更新属性不为null的值
     */
    @Override
    public int updateByRowGuidSelective(T record) {
        return dao.updateByRowGuidSelective(record);
    }

    /**
     * 根据Example条件更新实体`record`包含的全部属性，null值会被更新
     *
     * @param record
     * @param example
     * @return
     */
    @Override
    public int updateByExample(T record, Object example) {
        return dao.updateByExample(record, example);
    }

    ;

    /**
     * 根据Example条件更新实体`record`包含的全部属性，null值不会被更新
     *
     * @param record
     * @param example
     * @return
     */
    @Override
    public int updateByExampleSelective(T record, Object example) {
        return dao.updateByExampleSelective(record, example);
    }

    ;
}
