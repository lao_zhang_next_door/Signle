package com.xnw.frame.basic.base.service;

import com.xnw.frame.basic.mybatis.mapper.entity.Example;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface BaseSelectService<T> {

	/**
	 * 根据实体中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
	 *
	 * @param record
	 * @return
	*/
	T selectOne(T record);

    /**
     * 根据map中的属性进行查询 只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     * @param param
     * @return
     */
    T selectOne(Map param);

	/**
     * 根据实体中的属性值进行查询，查询条件使用等号
     *
     * @param record
     * @return
     */
    List<T> select(T record);

    /**
     * 根据map中的属性进行查询 查询条件使用等号
     * @param param
     * @return
     */
    List<T> select(Map param);

    /**
     * (分页)根据实体中的属性值进行查询，查询条件使用等号
     * @return
     */
    List<T> selectByLimit(Map param);
    
    /**
     * 查询全部结果
     *
     * @return
     */
    List<T> selectAll();
    
    /**
     * 根据实体中的属性查询总数，查询条件使用等号
     *
     * @param record
     * @return
     */
    int selectCount(T record);
    
    /**
     * (分页)根据实体中的属性查询总数，查询条件使用等号
     *
     * @param param
     * @return
     */
    int selectCountByLimit(Map param);
    
    /**
     * 根据主键字段进行查询，方法参数必须包含完整的主键属性，查询条件使用等号
     *
     * @param key
     * @return
     */
    T selectByPrimaryKey(Object key);

    /**
     * 根据example查询
     * @param example
     * @return
     */
    List<T> selectByExample(Example example);

    /**
     * 根据example查询
     * @param example
     * @return
     */
    T selectOneByExample(Example example);

    /**
     * 根据实体中的属性进行查询,返回单一集合,返回List<String>
     * @param example
     * @return
     */
    List<String> selectListOnlyByExample(Example example);

    /**
     * 根据example查询 分页
     * @param example
     * @return
     */
    List<T> selectByExampleAndRowBounds(Example example, RowBounds rowBounds);

    /**
     * 根据example查询总数
     * @param example
     * @return
     */
    Integer selectCountByExample(Example example);
}
