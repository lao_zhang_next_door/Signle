package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.XnwClient;

public interface XnwClientDao extends BaseDao<XnwClient> {
	
}

