package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.XnwClientDao;
import com.xnw.frame.entity.XnwClient;
import com.xnw.frame.service.XnwClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XnwClientServiceImpl extends BaseServiceImpl<XnwClient> implements XnwClientService {

	public XnwClientServiceImpl(XnwClientDao dao) { super(dao); }

	@Autowired
	private XnwClientDao xnwClientDao;

}

