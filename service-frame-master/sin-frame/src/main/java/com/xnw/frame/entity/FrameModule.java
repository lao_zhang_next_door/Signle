package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

import javax.persistence.Transient;

public class FrameModule extends BaseEntity {

    public FrameModule() {
    }

    public FrameModule(boolean init) {
        super(init);
    }

    /**
     * 模块标题名
     */
    private String moduleName;

    /**
     * 组件名称
     */
    private String componentName;

    /**
     * 父级模块
     */
    private String parentGuid;

    /**
     * 子集数量
     */
    private Integer hasChild;

    /**
     * 模块地址
     */
    private String moduleAddress;

    /**
     * 小图标
     */
    private String smallIcon;

    /**
     * 大图标
     */
    private String bigIcon;

    /**
     * 目标框架
     */
    private String target;

    /**
     * 是否隐藏
     */
    private String isVisible;

    /**
     * 是否隐藏Text
     */
    @Transient
    private String isVisibleText;

    /**
     * 转发路径
     */
    private String redirect;

    /**
     * 是否缓存路由
     */
    private String keepAlive;

    /**
     * 配置的访问路径
     */
    private String path;

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getParentGuid() {
        return parentGuid;
    }

    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }

    public Integer getHasChild() {
        return hasChild;
    }

    public void setHasChild(Integer hasChild) {
        this.hasChild = hasChild;
    }

    public String getModuleAddress() {
        return moduleAddress;
    }

    public void setModuleAddress(String moduleAddress) {
        this.moduleAddress = moduleAddress;
    }

    public String getSmallIcon() {
        return smallIcon;
    }

    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }

    public String getBigIcon() {
        return bigIcon;
    }

    public void setBigIcon(String bigIcon) {
        this.bigIcon = bigIcon;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(String keepAlive) {
        this.keepAlive = keepAlive;
    }

    public String getIsVisibleText() {
        if (getIsVisible() != null) {
            return codeValueService.getItemTextByValue("是否", getIsVisible());
        } else {
            return "";
        }
    }

    public void setIsVisibleText(String isVisibleText) {
        this.isVisibleText = isVisibleText;
    }
}
