package com.xnw.frame.enumeration;

/**
 * 是否枚举
 *
 * @author T470
 */
public enum IsOrNot {

    /**
     *
     */
    IS(0, "是", true),
    NOT(1, "否", false);

    private final int code;
    private final String value;
    private final boolean flag;

    IsOrNot(int code, String value, boolean flag) {
        this.code = code;
        this.value = value;
        this.flag = flag;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public boolean getFlag() {
        return flag;
    }

    public static boolean getFlagByCode(int code) {
        IsOrNot[] isOrNots = values();
        for (IsOrNot isOrNot : isOrNots) {
            if (isOrNot.getCode() == code) {
                return isOrNot.getFlag();
            }
        }
        return false;
    }

}
