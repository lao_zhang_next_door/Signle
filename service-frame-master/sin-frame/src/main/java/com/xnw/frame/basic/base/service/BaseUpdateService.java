package com.xnw.frame.basic.base.service;

public interface BaseUpdateService<T> {
	
	/**
     * 根据主键更新实体全部字段，null值会被更新
     *
     * @param record
     * @return
     */
    int updateByPrimaryKey(T record);
    
    
    /**
     * 根据主键更新属性不为null的值
     *
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(T record);
    
    
    /**
     * 根据rowGuid更新实体全部字段，null值会被更新
     * @param record
     * @return
     */
    int updateByRowGuid(T record);
    
    
    /**
     * 根据rowGuid更新属性不为null的值
     * @param record
     * @return
     */
    int updateByRowGuidSelective(T record);


    /**
     * 根据Example条件更新实体`record`包含的全部属性，null值会被更新
     * @param record
     * @param example
     * @return
     */
    int updateByExample(T record, Object example);


    /**
     * 根据Example条件更新实体`record`包含的全部属性，null值不会被更新
     * @param record
     * @param example
     * @return
     */
    int updateByExampleSelective(T record, Object example);

    /**
     * 根据rowGuids 数组批量逻辑删除
     * @param rowGuids
     * @return
     */
    int deleteBatchLogic(String[] rowGuids);
}
