package com.xnw.frame.service;


import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameDept;
import com.xnw.frame.entity.FrameUserDeptRelation;

import java.util.List;

/**
 * @author T470
 */
public interface FrameUserDeptRelationService extends BaseService<FrameUserDeptRelation> {

    /**
     * 新增用户部门一对多关联
     * @param userGuid
     * @param deptGuids
     */
    void addUserDeptRelation(String userGuid, String[] deptGuids);

    /**
     * 根据用户Guid删除关联
     * @param userGuid
     */
    void deleteByUserGuid(String userGuid);

    /**
     * 获取用户关联部门信息
     *
     * @param userGuid
     * @return
     */
    List<FrameDept> getUserRelationDept(String userGuid);
}
