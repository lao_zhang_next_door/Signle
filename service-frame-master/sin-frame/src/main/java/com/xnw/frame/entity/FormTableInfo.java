package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-23-13:05
 **/
public class FormTableInfo extends BaseEntity {

    public FormTableInfo(String rowGuid) {
        super(rowGuid);
    }

    public FormTableInfo() {
    }

    //数据表表单名称
    private String tableName;
    //数据表物理名称
    private String physicalName;
    //项目名称
    private String projectName;
    //模块名称
    private String componentName;
    //表单样式
    private String formStyle;

    public String getFormStyle() {
        return formStyle;
    }

    public void setFormStyle(String formStyle) {
        this.formStyle = formStyle;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }
}
