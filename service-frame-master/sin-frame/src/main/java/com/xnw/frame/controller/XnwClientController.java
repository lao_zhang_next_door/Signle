package com.xnw.frame.controller;

import com.xnw.common.utils.frame.Query;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.XnwClient;
import com.xnw.frame.service.XnwClientService;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/frame/XnwClient")
public class XnwClientController extends BaseController {

    @Autowired
    private XnwClientService xnwClientService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<XnwClient> xnwClientList = xnwClientService.selectByLimit(query);
        int count = xnwClientService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(xnwClientList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        XnwClient xnwClient = new XnwClient(rowGuid);
        return R.ok().put("data", xnwClientService.select(xnwClient));
    }

    @PostMapping("/add")
    private R add(@RequestBody XnwClient xnwClient) {
        xnwClient.initNull();
        xnwClientService.insert(xnwClient);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody XnwClient xnwClient) {
        xnwClient.setUpdateTime(LocalDateTime.now());
        xnwClientService.updateByRowGuidSelective(xnwClient);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        xnwClientService.deleteBatch(rowGuids);
        return R.ok();
    }
}