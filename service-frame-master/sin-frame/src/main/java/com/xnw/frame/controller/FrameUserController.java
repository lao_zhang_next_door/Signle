package com.xnw.frame.controller;

import com.xnw.auth.token.PassToken;
import com.xnw.common.utils.frame.CommUtil;
import com.xnw.common.utils.frame.PageUtils;
import com.xnw.common.utils.frame.Query;
import com.xnw.common.utils.frame.R;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.entity.FrameAttach;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.FrameUserRole;
import com.xnw.frame.entity.pojo.FrameUserPojo;
import com.xnw.frame.entity.pojo.XnwUser;
import com.xnw.frame.enumeration.DelFlag;
import com.xnw.frame.enumeration.IsOrNot;
import com.xnw.frame.enumeration.UserStatus;
import com.xnw.frame.service.FrameAttachService;
import com.xnw.frame.basic.mybatis.mapper.entity.Example;
import com.xnw.frame.entity.pojo.FrameUserPasswordVo;
import com.xnw.frame.service.FrameUserRoleService;
import com.xnw.frame.service.FrameUserService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 用户控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameUser")
public class FrameUserController extends BaseController {

    @Autowired
    public FrameUserService frameUserService;

    @Autowired
    public FrameUserRoleService frameUserRoleService;

    @Autowired
    private FrameAttachService frameAttachService;

    @GetMapping("/listData")
    public R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameUser> userList = frameUserService.selectByLimit(query);
        PageUtils pageUtil = new PageUtils(userList, userList.size(), query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    public R getDetailByGuid(@RequestParam String rowGuid) {
        FrameUser frameUser = new FrameUser(rowGuid);
        frameUser.setRowGuid(rowGuid);
        List<FrameUser> userList = frameUserService.select(frameUser);
        return R.ok().put("data", userList);
    }

    @PostMapping("/add")
    public R add(@RequestBody FrameUser frameUser) {
        frameUser.init();
        frameUserService.insertSelective(frameUser);
        return R.ok();
    }

    @PutMapping("/update")
    public R update(@RequestBody FrameUser frameUser) {
        frameUserService.updateByRowGuidSelective(frameUser);
        return R.ok();
    }

    @DeleteMapping("/delete")
    public R delete(@RequestBody String[] rowGuids) {
        frameUserService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 逻辑删除
     *
     * @param rowGuids
     * @return
     */
    @DeleteMapping("/deleteLogic")
    public R deleteLogic(@RequestBody String[] rowGuids) {
        for (String userGuid : rowGuids) {
            FrameUser user = new FrameUser();
            user.setRowGuid(userGuid);
            user.setDelFlag(DelFlag.Del.getCode());
            frameUserService.updateByRowGuidSelective(user);
        }
        return R.ok();
    }

    /**
     * 物理删除
     *
     * @param rowGuids
     * @return
     */
    @DeleteMapping("/deletePhysics")
    public R deletePhysics(@RequestBody String[] rowGuids) {
        frameUserService.deleteBatch(rowGuids);
        //删除用户角色关联
        for (String userGuid : rowGuids) {
            FrameUserRole userRole = new FrameUserRole();
            userRole.setUserGuid(userGuid);
            frameUserRoleService.delete(userRole);
        }
        return R.ok();
    }

    /**
     * 新增框架用户
     *
     * @param frameUserPojo
     * @return
     */
    @PostMapping("/addFrameUser")
    public R add(@RequestBody FrameUserPojo frameUserPojo) {
        frameUserService.addFrameUser(frameUserPojo);
        return R.ok();
    }

    /**
     * 更新框架用户
     *
     * @param frameUserPojo
     * @return
     */
    @PutMapping("/updateFrameUser")
    public R updateFrameUser(@RequestBody FrameUserPojo frameUserPojo) {
        frameUserService.updateFrameUser(frameUserPojo);
        return R.ok();
    }

    /**
     * 查询用户信息列表带部门信息 (联查部门表)
     *
     * @param params
     * @return
     */
    @GetMapping("/getList")
    public R getList(@RequestParam Map<String, Object> params) {
        params.put("delFlag", DelFlag.Normal.getCode());
        Query query = new Query(params);
        List<FrameUserPojo> userList = frameUserService.getList(query);
        PageUtils pageUtil = new PageUtils(userList, userList.size(), query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 验证登录名是否唯一
     *
     * @param loginId
     * @return
     */
    @GetMapping("/verifyUnique")
    public R verifyUnique(@RequestParam String loginId) {
        FrameUser frameUser = new FrameUser();
        frameUser.setLoginId(loginId);
        int count = frameUserService.selectCount(frameUser);
        if (count == 0) {
            return R.ok().put("data", true);
        } else {
            return R.ok().put("data", false);
        }
    }

    /**
     * 查询guid用户详情信息 包括角色,头像,部门等信息
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/getUserDataByGuid")
    public R getUserDataByGuid(@RequestParam String rowGuid) {
        FrameUserPojo user = frameUserService.selectDetailByUserGuid(rowGuid);
        List<FrameRole> roleList = frameUserService.selectRoleListByUserGuid(rowGuid);
        user.setRoleList(roleList);
        //获取部门信息关联
        XnwUser xnwUser = frameUserService.getUserInfoByLogin(user.getLoginId());
        user.setDeptNames(xnwUser.getDeptNames());
        user.setDeptGuids(xnwUser.getDeptGuids());
        //获取头像
        if (user.getHeadimgGuid() != null) {
            FrameAttach searchData = new FrameAttach();
            searchData.setRowGuid(user.getHeadimgGuid());
            FrameAttach frameAttach = frameAttachService.selectOne(searchData);
            if (frameAttach != null) {
                user.setHeadimgGuidVal(frameAttach.getContentUrl());
            }
        }
        return R.ok().put("data", user);
    }

    /**
     * 启用或者禁用用户
     *
     * @param userGuid
     * @param type
     * @return
     */
    @PutMapping("/enableOrDisable")
    public R enableOrDisable(@RequestParam String userGuid, String type) {
        FrameUser user = new FrameUser();
        user.setRowGuid(userGuid);
        //启用
        if ("enable".equals(type)) {
            user.setStatus(UserStatus.NORMAL.getCode());
        }

        //禁用
        if ("disable".equals(type)) {
            user.setStatus(UserStatus.DISABLE.getCode());
        }

        frameUserService.updateByRowGuidSelective(user);
        return R.ok();
    }

    /**
     * 重置用户密码
     *
     * @return
     */
    @PutMapping("/resetPassword")
    public R resetPassword(@RequestParam String userGuid) {
        String initPass = frameUserService.getInitPassword();
        if (!StringUtils.isBlank(initPass)) {
            FrameUser user = new FrameUser();
            user.setRowGuid(userGuid);
            user.setPassword(initPass);
            frameUserService.updateByRowGuidSelective(user);
        }
        return R.ok();
    }

    /**
     * 修改用户密码
     *
     * @return
     */
    @Transactional
    @PutMapping("/changePassword")
    public R changePassword(@RequestBody FrameUserPasswordVo frameUserPasswordVo) {
        frameUserService.changePassword(frameUserPasswordVo);
        return R.ok("修改密码成功");
    }

    /**
     * 查询某个部门下的所有人员
     *
     * @param deptGuid
     * @return
     */
    @GetMapping("/getUserListByDeptGuid")
    public R getUserListByDeptGuid(@RequestParam String deptGuid) {
        Example example = new Example(FrameUser.class);
        example.selectProperties("userName", "loginId", "rowGuid", "createTime", "deptGuid", "mobile");
        example.createCriteria().andEqualTo("deptGuid", deptGuid).andEqualTo("delFlag", DelFlag.Normal.getCode());
        return R.ok().put("data", frameUserService.selectByExample(example));
    }

    /**
     * 按照权限查询当前部门下的所有人员
     *
     * @param
     * @return
     */
    @PostMapping("/getUserByCurrentAuthority")
    public R getUserByCurrentAuthority() {
        return R.ok().put("data", frameUserService.getUserByCurrentAuthority());
    }

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     *
     * @return
     */
    @GetMapping("/getParentDeptUser")
    public R getParentDeptUser() {
        return R.ok().put("data", frameUserService.getParentDeptUser());
    }

    /**
     * 根据rowGuid 获取用户部分信息
     *
     * @param userGuid
     * @return
     */
    @PassToken
    @GetMapping("/getUserInfoByGuid")
    public R getUserInfoByGuid(@RequestParam String userGuid) {
        Example example = new Example(FrameUser.class);
        Example.Criteria criteria = example.selectProperties("userName", "loginId", "deptGuid").createCriteria();
        criteria.andEqualTo("rowGuid", userGuid)
                .andEqualTo("delFlag", DelFlag.Normal.getCode())
                .andEqualTo("status", UserStatus.NORMAL.getCode());

        return R.ok().put("data", frameUserService.selectByExample(example));
    }

    @Operation(summary = "解除用户登录限制")
    @PostMapping("/removeLoginRestrictions")
    public R removeLoginRestrictions(@RequestBody String[] loginIds) {
        for (String id : loginIds) {
            this.redisService.remove(CommUtil.getRedisKey("LoginIdName:Fail:" + id));
            FrameUserPojo frameUserPojo = this.frameUserService.selectDetailByLoginId(id);
            frameUserPojo.setIsLimit(IsOrNot.NOT.getCode());
            this.frameUserService.updateByRowGuid(frameUserPojo);
        }
        return R.ok();
    }
}
 