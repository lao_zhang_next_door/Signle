package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * 设计原则:
 * 1.不产生垃圾文件，垃圾数据(待完善)
 * 2.formRowGuid 对应 一个 或者 多个 关联记录
 * 3.记录上传人 ip,若为平台用户上传 则记录上传用户guid
 * 4.存储方式:数据库二进制流 或者 本地文件形式
 */
/**
 * <p>Title: FrameAttach</p>
 * <p>Description: 附件表</p>
 *
 * @author wzl
 */
public class FrameAttach extends BaseEntity {

    public FrameAttach(String rowGuid){super(rowGuid);};

    public FrameAttach(){};

    public FrameAttach(boolean init){super(init);};
    /**
     * 附件名
     */
    private String attachName;

    /**
     * 文件类型
     **/
    private String contentType;

    /**
     * 文件长度
     */
    private int contentLength;
    /**
     * 文件地址或内容
     */

    private String contentUrl;

    /**
     * 与记录关联唯一标识
     **/
    private String formRowGuid;
    
    /**
     * 上传人ip
     */
    private String uploadIp;

    /**
     * 字节流内容
     */
    private byte[] content;

    /**
     * 上传人Guid
     */
    private String uploadUserGuid;
    
    public String getUploadIp() {
		return uploadIp;
	}

	public void setUploadIp(String uploadIp) {
		this.uploadIp = uploadIp;
	}

	public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public int getContentLength() {
        return contentLength;
    }

    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getFormRowGuid() {
        return formRowGuid;
    }

    public void setFormRowGuid(String formRowGuid) {
        this.formRowGuid = formRowGuid;
    }

    public String getUploadUserGuid() {
        return uploadUserGuid;
    }

    public void setUploadUserGuid(String userGuid) {
        this.uploadUserGuid = uploadUserGuid;
    }


}
