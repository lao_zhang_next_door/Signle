package com.xnw.frame.basic.base.service;

public interface BaseService<T> extends
        BaseSelectService<T>, BaseInsertService<T>, BaseDeleteService<T>,
        BaseUpdateService<T> {



}
