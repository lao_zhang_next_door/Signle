package com.xnw.frame.service.impl;

import com.xnw.common.utils.frame.CommUtil;
import com.xnw.frame.dao.FrameConfigDao;
import com.xnw.frame.entity.FrameConfig;
import com.xnw.frame.service.FrameConfigService;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrameConfigServiceImpl extends BaseServiceImpl<FrameConfig> implements FrameConfigService {

	Logger logger = LoggerFactory.getLogger(FrameConfigServiceImpl.class);

	public FrameConfigServiceImpl(FrameConfigDao dao) {
		super(dao);
	}

	@Autowired
	private FrameConfigDao frameConfigDao;
	@Autowired
	private RedisService redisService;

	@Override
	public void saveAllConfigToRedis() {
		logger.info("系统设置缓存开始");
		//redisService.setRemove(CommUtil.getRedisKey("systemConfig*"),null);
		List<FrameConfig> list = frameConfigDao.selectAll();
		if (!list.isEmpty()) {
			for (FrameConfig fc : list) {
				redisService.set(CommUtil.getRedisKey("systemConfig:" + fc.getConfigName()), fc.getConfigValue());
			}
		}
		logger.info("系统设置缓存结束");
	}

	@Override
	public String getConfigsVal(String configName) {
		return (String) redisService.get(CommUtil.getRedisKey("systemConfig:" + configName));
	}
}
