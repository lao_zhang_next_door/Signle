package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameAttach;

/**
 * @author wzl
 */
public interface FrameAttachDao extends BaseDao<FrameAttach> {

}
