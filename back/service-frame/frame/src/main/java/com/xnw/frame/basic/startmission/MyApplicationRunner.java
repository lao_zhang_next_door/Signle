package com.xnw.frame.basic.startmission;

import com.xnw.frame.service.FrameCodeValueService;
import com.xnw.frame.service.FrameConfigService;
import com.xnw.frame.service.FrameUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wzl
 */
@Component
@Order(100)
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @Autowired
    private FrameConfigService frameConfigService;

    @Autowired
    private FrameUserService frameUserService;

    @Override
    public void run(ApplicationArguments args) {
        //代码项缓存
        frameCodeValueService.cacheAllCodeValue();
        //系统配置项缓存
        frameConfigService.saveAllConfigToRedis();
        //清空缓存用户信息
        frameUserService.cleanCacheUser();
    }
}
