package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameCode;

public interface FrameCodeService extends BaseService<FrameCode> {

}
