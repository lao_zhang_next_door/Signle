package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * 权限
 * @author hero
 *
 */
public class FrameRoleModule extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
    public FrameRoleModule(boolean init) {
		super(init);
	}

	public FrameRoleModule() {}
	
	/**
	 * 模块guid
	 */
	private String moduleGuid;
	
	/**
	 * 所属角色或其他guid
	 */
	private String allowTo;

	public String getModuleGuid() {
		return moduleGuid;
	}

	public void setModuleGuid(String moduleGuid) {
		this.moduleGuid = moduleGuid;
	}

	public String getAllowTo() {
		return allowTo;
	}

	public void setAllowTo(String allowTo) {
		this.allowTo = allowTo;
	}
}
