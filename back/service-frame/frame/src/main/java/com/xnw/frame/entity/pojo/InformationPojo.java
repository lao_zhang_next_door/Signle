package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.InformationInfo;

public class InformationPojo extends InformationInfo {

    /**
     * 栏目名
     */
    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
