package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameRoleDao;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.service.FrameRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FrameRoleServiceImpl extends BaseServiceImpl<FrameRole> implements FrameRoleService {

	public FrameRoleServiceImpl(FrameRoleDao dao) {
		super(dao);
	}

	@Autowired
	private FrameRoleDao frameRoleDao;

	@Override
	public List<FrameModule> getModuleByRoleGuid(String roleGuid) {
		return frameRoleDao.getModuleByRoleGuid(roleGuid);
	}

	@Override
	public List<String> getRoleGuidsByNames(String roleNames) {
		return frameRoleDao.getRoleGuidsByNames(roleNames);
	}

	@Override
	public List<Map<String, Object>> getRolesByUserGuid(String userGuid) {
		return frameRoleDao.getRolesByUserGuid(userGuid);
	}

	@Override
	public String getRoleNameByRoleGuid(String userRoleGuid) {
		FrameRole entity = new FrameRole();
		entity.setRowGuid(userRoleGuid);
		return frameRoleDao.selectOne(entity).getRoleName();
	}
}
