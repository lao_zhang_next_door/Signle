package com.xnw.frame.basic.utils.frame;

import com.google.common.base.CaseFormat;
import com.xnw.frame.basic.sql.SqlFilter;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 查询参数
 *
 * @author keeny
 * @ClassName: Query
 * @Description: 查询参数
 * @date 2019年1月8日 上午10:53:55
 */
public class Query extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = 1L;
    //当前页码
    private int page;
    //每页条数
    private int limit;

    public Query(Map<String, Object> params) {
        this.putAll(params);

        //分页参数
        if (params.get("page") != null && params.get("limit") != null) {
            this.page = Integer.parseInt(params.get("page").toString());
            this.limit = Integer.parseInt(params.get("limit").toString());
            this.put("offset", (page - 1) * limit);
            this.put("page", page);
            this.put("limit", limit);
        }

//        
//        if(params.containsKey("sidx") && params.containsKey("order")){
//        	String sidx = params.get("sidx").toString();
//            String order = params.get("order").toString();
//            this.put("sidx", SQLFilter.sqlInject(sidx,false));
//            this.put("order", SQLFilter.sqlInject(order,false));
//        }
        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        if (params.containsKey("order")) {
            String order = params.get("order").toString();

            Matcher matcherAES = Pattern.compile("(?i)asc").matcher(order);
            while (matcherAES.find()) {
                order = order.replaceAll("(?i)ASC", "asc");
            }

            Matcher matcherDESC = Pattern.compile("(?i)desc").matcher(order);
            while (matcherDESC.find()) {
                order = order.replaceAll("(?i)DESC", "desc");
            }

            //转下划线 asc desc 必须全为小写
            order = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, order);
            this.put("order", SqlFilter.sqlInject(order, false));
        }

    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
