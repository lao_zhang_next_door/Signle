package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.SystemLog;

/**
 * @author wzl
 * @date 2020-05-06 17:06:02
 */
public interface SystemLogDao extends BaseDao<SystemLog> {
}
