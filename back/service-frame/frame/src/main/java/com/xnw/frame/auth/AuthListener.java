package com.xnw.frame.auth;

import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.auth.token.JwtHelper;
import com.xnw.frame.basic.network.HttpContextUtils;
import com.xnw.frame.basic.utils.frame.CommUtil;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.enumeration.AuthCode;
import com.xnw.frame.redis.RedisService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 系统使用权监听器
 *
 * @author T470
 */
@Component
public class AuthListener {
    @Autowired
    private RedisService redisService;

    @Value("${token.expiration.time}")
    private long tokenExpirationTime;

    @Value("${token.head.name}")
    private String authName;

    @Value("${user.redis-name}")
    private String userDataRedisName;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

//        if(!license.checkCode()){
//            response.getWriter().print(JSONObject.toJSONString(R.error(AuthCode.SystemUnregistered.getCode(), AuthCode.SystemUnregistered.getValue())));
//            return false;
//        }
        //等到请求头信息authorization信息
        final String authHeader = request.getHeader(authName);
        if (authHeader == null) {
            response.getWriter().print(JSONObject.toJSONString(R.error(AuthCode.WithoutToken.getCode(), AuthCode.WithoutToken.getValue())));
            return false;
        } else {
            try {
                final Claims claims = JwtHelper.parseJWT(authHeader);
                if (claims != null) {
                    String uuid = claims.get("uuid").toString();
                    if (redisService.exists(CommUtil.getRedisKey(userDataRedisName + uuid)) && HttpContextUtils.getFrameSidValue(request) != null) {
                        //说明token未过期
                        long now = new java.util.Date().getTime();
                        now = now + tokenExpirationTime;
                        claims.setExpiration(new java.util.Date(now));
                        redisService.expire(CommUtil.getRedisKey(userDataRedisName + uuid), tokenExpirationTime * (long) HttpContextUtils.getVertifyOutDay());
                        HttpContextUtils.addFrameCookieTime(request, response, HttpContextUtils.getVertifyOutDay());
                        return true;
                    } else {
                        response.getWriter().print(JSONObject.toJSONString(R.error(AuthCode.TokenExpired.getCode(), AuthCode.TokenExpired.getValue())));
                        return false;
                    }
                } else {
                    response.getWriter().print(JSONObject.toJSONString(R.error(AuthCode.GetTokenFailed.getCode(), AuthCode.GetTokenFailed.getValue())));
                    return false;
                }

            } catch (final Exception e) {
                return false;
            }
        }
    }
}
