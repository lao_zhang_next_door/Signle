package com.xnw.frame.controller;

import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.InformationCategory;
import com.xnw.frame.service.InformationCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 信息栏目控制器
 */
@RestController
@RequestMapping("/frame/InformationCategory")
public class InformationCategoryController {

    @Autowired
    private InformationCategoryService informationCategoryService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<InformationCategory> informationCategoryList = informationCategoryService.selectByLimit(query);
        int count = informationCategoryService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(informationCategoryList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        InformationCategory informationCategory = new InformationCategory(rowGuid);
        return R.ok().put("data", informationCategoryService.select(informationCategory));
    }

    @PostMapping("/add")
    private R add(@RequestBody InformationCategory informationCategory) {
        informationCategory.initNull();
        informationCategoryService.insert(informationCategory);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody InformationCategory informationCategory) {
        informationCategory.setUpdateTime(LocalDateTime.now());
        informationCategoryService.updateByRowGuidSelective(informationCategory);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        informationCategoryService.deleteBatch(rowGuids);
        return R.ok();
    }
}