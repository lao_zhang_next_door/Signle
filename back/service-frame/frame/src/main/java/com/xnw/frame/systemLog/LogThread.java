package com.xnw.frame.systemLog;

import com.xnw.frame.basic.utils.frame.SubModuleUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wzl
 */
@Service("logThread")
public class LogThread {
    @Autowired
    private SubModuleUtils subModuleUtils;

    @Async("threadPoolExecutor")
    public void saveLog(ProceedingJoinPoint joinPoint, long time, String userName,HttpServletRequest request) {
        subModuleUtils.saveLog(joinPoint, time, userName, request);
    }


}
