package com.xnw.frame.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xnw.frame.basic.base.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 用户实体
 *
 * @author hero
 * 实体类的字段数量 >= 数据库表中需要操作的字段数量。
 * 默认情况下，实体类中的所有字段都会作为表中的字段来操作，如果有额外的字段，必须加上@Transient注解。
 * <p>
 * 1.表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info。
 * 2.表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
 * 3.字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
 * 4.可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
 * 5.使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
 * 6.建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
 * 7.如果是MySQL的自增字段，加上@GeneratedValue(generator = "JDBC")即可
 * <p>
 * Example方法
 * 方法：List<T> selectByExample(Object example);
 * 说明：根据Example条件进行查询
 * 重点：这个查询支持通过Example类指定查询列，通过selectProperties方法指定查询列
 * <p>
 * 方法：int selectCountByExample(Object example);
 * 说明：根据Example条件进行查询总数
 * <p>
 * 方法：int updateByExample(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的全部属性，null值会被更新
 * <p>
 * 方法：int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);
 * 说明：根据Example条件更新实体record包含的不是null的属性值
 * <p>
 * 方法：int deleteByExample(Object example);
 * 说明：根据Example条件删除数据
 * <p>
 * 例子： // 创建Example
 * Example example = new Example(TestTableVO.class);
 * // 创建Criteria
 * Example.Criteria criteria = example.createCriteria();
 * // 添加条件
 * criteria.andEqualTo("isDelete", 0);
 * criteria.andLike("name", "%abc123%");
 * List<TestTableVO> list = testTableDao.selectByExample(example);
 */

/**
 * 实体类设计原则：
 * 1.所有的字段皆和数据库一一对应，不可添加额外字段(除了代码项的文本值以外)
 * 2.若需添加额外字段，请写pojo,vo等类
 * 3.所有字段均为私有权限
 *
 * @author hero
 */
public class FrameUser extends BaseEntity {
    /**
     * @param init true则有默认值
     */
    public FrameUser(boolean init) {
        super(init);
    }

    public FrameUser(String rowGuid) {
        super(rowGuid);
    }

    public FrameUser() {
    }

    /**
     * 最近登录时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime lastLoginTime;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 登录名
     */
    private String loginId;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 性别
     */
    private Character sex;

    /**
     * 工号
     */
    private String gongHao;

    /**
     * 部门关联字段
     */
    private String deptGuid;

    /**
     * 职位
     */
    private String duty;

    /**
     * 电话
     */
    private String tel;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 微信关联号
     */
    private String openId;

    /**
     * 状态(初始值0 >>> 正常)
     */
    private Character status;
    /**
     * 头像
     */
    private String headimgGuid;

    /**
     * 主题
     */
    private String themeGuid;

    /**
     * 是否禁用
     */
    private Integer isLimit;

    /**
     * email
     */
    private String email;

    /**
     * 出生日期
     */
    private LocalDate birthDate;


    public LocalDateTime getLastLoginTime() {
        return lastLoginTime;
    }

    public FrameUser setLastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public FrameUser setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getLoginId() {
        return loginId;
    }

    public FrameUser setLoginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public FrameUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public Character getSex() {
        return sex;
    }

    public FrameUser setSex(Character sex) {
        this.sex = sex;
        return this;
    }

    public String getGongHao() {
        return gongHao;
    }

    public FrameUser setGongHao(String gongHao) {
        this.gongHao = gongHao;
        return this;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public FrameUser setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
        return this;
    }

    public String getDuty() {
        return duty;
    }

    public FrameUser setDuty(String duty) {
        this.duty = duty;
        return this;
    }

    public String getTel() {
        return tel;
    }

    public FrameUser setTel(String tel) {
        this.tel = tel;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public FrameUser setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public FrameUser setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public Character getStatus() {
        return status;
    }

    public FrameUser setStatus(Character status) {
        this.status = status;
        return this;
    }

    public String getHeadimgGuid() {
        return headimgGuid;
    }

    public FrameUser setHeadimgGuid(String headimgGuid) {
        this.headimgGuid = headimgGuid;
        return this;
    }

    public String getThemeGuid() {
        return themeGuid;
    }

    public FrameUser setThemeGuid(String themeGuid) {
        this.themeGuid = themeGuid;
        return this;
    }

    public Integer getIsLimit() {
        return isLimit;
    }

    public FrameUser setIsLimit(Integer isLimit) {
        this.isLimit = isLimit;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public FrameUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public FrameUser setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

}
