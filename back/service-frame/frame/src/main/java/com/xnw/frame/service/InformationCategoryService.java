package com.xnw.frame.service;


import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.InformationCategory;

public interface InformationCategoryService extends BaseService<InformationCategory> {


}
