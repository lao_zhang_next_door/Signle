package com.xnw.frame.controller;

import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.SystemLog;
import com.xnw.frame.service.SystemLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author wzl
 * @date 2020-05-06 17:06:02
 */
@Tag(name = "系统操作日志")
@RestController
@CrossOrigin
@RequestMapping("/frame/SystemLog")
public class SystemLogController {

    @Autowired
    private SystemLogService systemLogService;

    /**
     * 列表数据
     */
    @Operation(summary = "列表数据")
    @ResponseBody
    @RequestMapping(value = "/listData", produces = "application/json;charset=utf-8", method = RequestMethod.GET)
    public R listData(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<SystemLog> informationInfoList = systemLogService.selectByLimit(query);
        int count = systemLogService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(informationInfoList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 新增
     **/
    @Operation(summary = "新增")
    @ResponseBody
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R add(@RequestBody SystemLog systemLog) {
        systemLog.init();
        systemLogService.insert(systemLog);
        return R.ok();
    }

    /**
     * 删除
     **/
    @Operation(summary = "删除")
    @ResponseBody
    @RequestMapping(value = "/deleteLog", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R deleteLog(@RequestBody String[] rowGuids) {
        systemLogService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 通过rowGuid获取一条记录
     *
     * @param rowGuid
     * @return
     */
    @Operation(summary = "通过rowGuid获取一条记录")
    @ResponseBody
    @RequestMapping(value = "/getDetailByGuid", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R getDetailByGuid(@RequestParam String rowGuid) {
        SystemLog systemLog = systemLogService.selectByPrimaryKey(new SystemLog((rowGuid)));
        return R.ok().put("data", systemLog);
    }

    /**
     * 下载当前日志文件
     *
     * @return
     */
    @Operation(summary = "通过rowGuid获取一条记录")
    @RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
    public R downloadFile(@RequestParam String file) {
        String nowDate = new SimpleDateFormat("yyyy-MM").format(new Date());
        String filePath = "/logs/" + nowDate + "/" + "java_frame." + file + "." + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".0.log";
        return R.ok().put("data", filePath);
    }

}
