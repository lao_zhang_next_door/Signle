package com.xnw.frame.entity.pojo;

import com.xnw.frame.entity.FrameDept;

public class FrameDeptPojo extends FrameDept {
	
	/**
	 * 父级名称
	 */
	private String parentGuidVal;

	public String getParentGuidVal() {
		return parentGuidVal;
	}

	public void setParentGuidVal(String parentGuidVal) {
		this.parentGuidVal = parentGuidVal;
	}
	
}
