package com.xnw.frame.basic.network;

import com.xnw.frame.auth.token.JwtHelper;
import com.xnw.frame.basic.utils.frame.*;
import com.xnw.frame.redis.RedisService;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @author wzl
 */
public class HttpContextUtils {

    private static final RedisService redisService = SpringContextUtils.getBean(RedisService.class);

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    public static String getCookieValue(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (key.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void setCookieValue(HttpServletResponse response, String key, String value, int second) {
        Cookie cookie = new Cookie(key, value);       //(key,value)
        cookie.setPath("/");// 这个要设置
        cookie.setMaxAge(second);// 不设置的话，则cookies不写入硬盘,而是写在内存,只在当前页面有用,以秒为单位
        response.addCookie(cookie);         //添加第一个Cookie
    }

    public static String getFrameSidName() {
        return CommUtil.getPackageName() + "_sid";
    }

    public static String getFrameSidValue(HttpServletRequest request, HttpSession session) {
        return getFrameSidValue(request, null, session);
    }

    public static String getFrameSidValue(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String value = getCookieValue(request, getFrameSidName());
        if (value == null) {
            value = session.getId();
            if (response != null) {
                setCookieValue(response, getFrameSidName(), value, getVertifyOutDay() * 24 * 60 * 60);
            }
        }
        return value;
    }

    public static int getVertifyOutDay() {
        String day = ApplicationUtils.getApplicationProperties("default-vertify-timeout-day");
        if (StringUtil.isEmpty(day)) {
            return 1;
        }
        return ObjectConvert.toInt32(day.trim());
    }

    public static String getRedisVertityName(String frameSid, HttpServletRequest request) {
        return frameSid + "_" + JwtHelper.AUTH_CODE;
    }

    public static String getFrameSidValue(HttpServletRequest request) {
        return HttpContextUtils.getCookieValue(request, HttpContextUtils.getFrameSidName());
    }

    public static void addFrameCookieTime(HttpServletRequest request, HttpServletResponse response, int day) {

        Cookie[] coo = request.getCookies();
        if (coo.length > 0) {
            for (Cookie cookie : coo) {
                if (HttpContextUtils.getFrameSidName().equals(cookie.getName())) {
                    cookie.setMaxAge(day * 60 * 60 * 24);
                    cookie.setPath("/");
                    response.addCookie(cookie);
                    break;
                }
            }
        }

        redisService.expire(CommUtil.getRedisKey(getCookieValue(request, getFrameSidName())), 1000 * 60 * 60 * 24 * (long) getVertifyOutDay());

    }

    public static String getSessionUserGuid(HttpServletRequest request, String redisData) {
        return redisService.get(CommUtil.getRedisKey(redisData + HttpContextUtils.getCookieValue(request, getFrameSidName()))).toString();
    }

    public static void setSessionUserGuid(String cookieValue, String userGuid) {
        redisService.set(CommUtil.getRedisKey(cookieValue), userGuid, 1000 * 60 * 60 * 24 * (long) getVertifyOutDay());
    }
}
