package com.xnw.frame.basic.base.mapper;

import com.xnw.frame.basic.mybatis.mapper.annotation.RegisterMapper;
import com.xnw.frame.basic.mybatis.mapper.provider.base.BaseSelectProvider;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

@RegisterMapper
public interface ExtendSelectMapper<T> {
	
	/**
	 * (分页)根据实体中的属性值进行查询，查询条件使用等号
	 * @param record
	 * @return
	 */
    @SelectProvider(type = BaseSelectProvider.class, method = "dynamicSQL")
	List<T> selectByLimit(Map record);
    
    /**
	 * (分页)根据实体中的属性值进行查询总数，查询条件使用等号
	 * @param record
	 * @return
	 */
    @SelectProvider(type = BaseSelectProvider.class, method = "dynamicSQL")
    int selectCountByLimit(Map record);

	/**
	 * 根据实体中的属性进行查询单一字段集合，返回List<String>
	 * @param example
	 * @return
	 */
	@SelectProvider(type = BaseSelectProvider.class, method = "dynamicSQL")
	List<String> selectListOnlyByExample(Object example);

}
