package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;


/**
 * <p>Title: Systemlog</p>
 * <p>Description:</p>
 *
 * @author wzl
 */

public class SystemLog extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public SystemLog(boolean init) {
        super(init);
    }

    public SystemLog(String rowGuid) {
        super(rowGuid);
    }

    public SystemLog() {}


    /**
     * 用户名
     **/
    private String userName;
    /**
     * 操作名称
     **/
    private String operation;
    /**
     * 方法名
     **/
    private String method;
    /**
     * 参数
     **/
    private String params;
    /**
     * ip
     **/
    private String ip;
    /**
     * 耗时
     **/
    private Integer time;

    /**
     * 操作类型
     */
    private String actionType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
