package com.xnw.frame.service;


import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.entity.InformationInfo;

import java.util.Map;

public interface InformationInfoService extends BaseService<InformationInfo> {

    /**
     * 信息发布
     * @param informationInfo
     */
    void deliveryInfo(InformationInfo informationInfo);

    /**
     * 根据权限联查栏目表
     * @param params
     * @return
     */
    PageUtils listDataLfCategory(Map<String, Object> params);
}
