package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.pojo.FrameModulePojo;

import java.util.List;

public interface FrameModuleDao extends BaseDao<FrameModule> {

	/**
	 * 根据rowGuid 联查(left join frameModule)
	 * @param rowGuid
	 * @return
	 */
	List<FrameModulePojo> selectParentCode(String rowGuid);

	
}
