package com.xnw.frame.controller;

import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.FrameRoleModule;
import com.xnw.frame.service.FrameRoleModuleService;
import com.xnw.frame.service.FrameRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 角色控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameRoleModule")
public class FrameRoleModuleController extends BaseController {

    @Autowired
    private FrameRoleModuleService frameRoleModuleService;

    @Autowired
    private FrameRoleService frameRoleService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameRoleModule> RoleModuleList = frameRoleModuleService.selectByLimit(query);
        int count = frameRoleModuleService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(RoleModuleList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameRoleModule frameRoleModule = new FrameRoleModule();
        frameRoleModule.setRowGuid(rowGuid);
        List<FrameRoleModule> RoleModuleList = frameRoleModuleService.select(frameRoleModule);
        return R.ok().put("data", RoleModuleList);
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameRoleModule frameRoleModule) {
        frameRoleModule.initNull();
        frameRoleModuleService.insert(frameRoleModule);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameRoleModule frameRoleModule) {
        frameRoleModuleService.updateByRowGuidSelective(frameRoleModule);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameRoleModuleService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 设置权限树（批量新增）
     *
     * @param FrameRoleModuleList
     * @return
     */
    @PostMapping("/addBatch")
    private R addBatch(@RequestBody List<FrameRoleModule> FrameRoleModuleList, @RequestParam String roleGuid) {
        FrameRoleModule rm = new FrameRoleModule();
        rm.setAllowTo(roleGuid);
        List<FrameRoleModule> rmList = frameRoleModuleService.select(rm);

        /**
         * 先删除该角色之前设置的权限
         * 若长度为0 则只删除之前设置的权限
         */
        List<String> guidArr = rmList.stream()
                .map(FrameRoleModule::getRowGuid).collect(Collectors.toList());
        if (rmList.size() != 0) {
            frameRoleModuleService.deleteBatch(guidArr.toArray(new String[guidArr.size()]));
        }

        if (FrameRoleModuleList.size() == 0) {
            return R.ok("删除成功");
        }

        for (FrameRoleModule roleModule : FrameRoleModuleList) {
            roleModule.initNull();
        }
        frameRoleModuleService.insertList(FrameRoleModuleList);
        return R.ok("设置成功");
    }
}
 