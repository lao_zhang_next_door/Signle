package com.xnw.frame.enumeration;

public enum TrueOrFalseEnum {

	TRUE(0,"true"),
	FALSE(1,"false");

	private final int code;
    private final String value;
    private TrueOrFalseEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
