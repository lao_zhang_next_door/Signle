package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameAttachDao;
import com.xnw.frame.entity.FrameAttach;
import com.xnw.frame.service.FrameAttachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author wzl
 */
@Service
public class FrameAttachServiceImpl extends BaseServiceImpl<FrameAttach> implements FrameAttachService {

    public FrameAttachServiceImpl(FrameAttachDao dao) {
        super(dao);
    }

    @Autowired
    private FrameAttachDao frameAttachDao;


    @Override
    public void deleteFileByFormRowGuid(String formRowGuid) {

        FrameAttach attach = new FrameAttach();
        attach.setFormRowGuid(formRowGuid);
        List<FrameAttach> attachList = frameAttachDao.select(attach);

        attachList.forEach(new Consumer<FrameAttach>() {
            @Override
            public void accept(FrameAttach frameAttach) {
                File f = new File(filePath + frameAttach.getContentUrl());
                if(f.exists()){
                    f.delete();
                }

                frameAttachDao.deleteByRowGuid(frameAttach.getRowGuid());
            }
        });

    }

    @Override
    public void deleteFileByRowGuid(List<String> rowGuids) {

        rowGuids.forEach(new Consumer<String>() {
            @Override
            public void accept(String rowGuid) {
                FrameAttach attach = new FrameAttach(rowGuid);
                FrameAttach resAttach = frameAttachDao.selectOne(attach);
                if(resAttach != null){
                    File f = new File(filePath + resAttach.getContentUrl());
                    if (f.exists()) {
                        f.delete();
                    }
                    frameAttachDao.deleteByRowGuid(rowGuid);
                }

            }
        });

    }

    /**
     * 根据formRowGuid查找contentUrl
     *
     * @param formRowGuid
     * @param index       序号
     * @param isLatest    是否为最新的一条
     * @return Result
     */
    @Override
    public String getContentUrlByFormRowGuid(String formRowGuid, Integer index, boolean isLatest) {
        FrameAttach frameAttach = new FrameAttach();
        frameAttach.setFormRowGuid(formRowGuid);
        List<FrameAttach> attachList = frameAttachDao.select(frameAttach);
        String contentUrl = null;
        if (!attachList.isEmpty()) {
            // 有序号查询下标
            if (index != null && attachList.size() > index) {
                contentUrl = attachList.get(index).getContentUrl();
            }
            // 查询最新一条
            if (isLatest) {
                contentUrl = attachList.get(attachList.size() - 1).getContentUrl();
            }

        }
        return contentUrl;
    }
}
