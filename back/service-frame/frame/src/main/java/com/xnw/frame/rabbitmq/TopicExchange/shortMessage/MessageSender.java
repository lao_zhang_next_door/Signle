package com.xnw.frame.rabbitmq.TopicExchange.shortMessage;

import com.alibaba.fastjson.JSON;
import com.xnw.frame.basic.utils.frame.CommUtil;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

	@Autowired
    private RabbitTemplate rabbitTemplate;
     
    public void sendMessage(Object object){
    	CorrelationData correlationData = new CorrelationData(JSON.toJSONString(object));
        rabbitTemplate.convertAndSend(CommUtil.getPackageName() + "_messageExchange",
                CommUtil.getPackageName() + "_messageQueue.send","",correlationData);
    }
}
