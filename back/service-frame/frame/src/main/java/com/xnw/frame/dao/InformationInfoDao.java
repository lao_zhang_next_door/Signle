package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.InformationInfo;

public interface InformationInfoDao extends BaseDao<InformationInfo>{
	
}

