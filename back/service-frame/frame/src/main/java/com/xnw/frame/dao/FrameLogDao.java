package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.CommonLog;

public interface FrameLogDao extends BaseDao<CommonLog> {
	
}

