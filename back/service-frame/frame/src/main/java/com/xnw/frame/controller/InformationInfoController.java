package com.xnw.frame.controller;

import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.InformationInfo;
import com.xnw.frame.service.InformationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 信息控制器
 */
@RestController
@RequestMapping("/frame/InformationInfo")
public class InformationInfoController {

    @Autowired
    private InformationInfoService informationInfoService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<InformationInfo> informationInfoList = informationInfoService.selectByLimit(query);
        int count = informationInfoService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(informationInfoList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        InformationInfo informationInfo = new InformationInfo(rowGuid);
        return R.ok().put("data", informationInfoService.select(informationInfo));
    }

    @PostMapping("/add")
    private R add(@RequestBody InformationInfo informationInfo) {
        informationInfo.initNull();
        informationInfoService.insert(informationInfo);
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody InformationInfo informationInfo) {
        informationInfo.setUpdateTime(LocalDateTime.now());
        informationInfoService.updateByRowGuidSelective(informationInfo);
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        informationInfoService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 根据权限联查栏目表
     *
     * @param params
     * @return
     */
    @GetMapping("/listDataLfCategory")
    private R listDataLfCategory(@RequestParam Map<String, Object> params) {
        PageUtils pageUtil = informationInfoService.listDataLfCategory(params);
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 信息发布
     *
     * @param informationInfo
     * @return
     */
    @PostMapping("/deliveryInfo")
    private R deliveryInfo(@RequestBody InformationInfo informationInfo) {
        informationInfoService.deliveryInfo(informationInfo);
        return R.ok();
    }
}