package com.xnw.frame.basic.base.mapper;


import com.xnw.frame.basic.mybatis.mapper.annotation.RegisterMapper;
import com.xnw.frame.basic.mybatis.mapper.provider.base.BaseUpdateProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@RegisterMapper
public interface ExtendUpdateMapper<T> {
	
	/**
     * 根据rowGuid更新实体全部字段，null值会被更新
     *
     * @param record
     * @return
     */
    @UpdateProvider(type = BaseUpdateProvider.class, method = "dynamicSQL")
    int updateByRowGuid(T record);

    /**
     * 根据rowGuid更新属性不为null的值
     *
     * @param record
     * @return
     */
    @UpdateProvider(type = BaseUpdateProvider.class, method = "dynamicSQL")
    int updateByRowGuidSelective(T record);


    /**
     * 根据rowGuids 数组批量逻辑删除
     *
     * @param rowGuids
     * @return
     */
    @UpdateProvider(type = BaseUpdateProvider.class, method = "dynamicSQL")
    int deleteBatchLogic(String[] rowGuids);
}
