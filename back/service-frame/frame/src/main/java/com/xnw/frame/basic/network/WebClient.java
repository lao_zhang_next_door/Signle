package com.xnw.frame.basic.network;

import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.basic.utils.frame.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class WebClient {

    private HttpCookie cookie = null;
    private final Map<String, String> headers = new LinkedHashMap<>();

    private String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)";
    private String CONNECTION = "Keep-Alive";
    private String ACCEPT = "*/*";
    private String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private String CHARSET = "UTF-8";

    public HttpCookie getCookie() {
        return cookie;
    }

    public void setCookie(HttpCookie cookie) {
        this.cookie = cookie;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        for (String key : headers.keySet()) {
            setHeader(key, headers.get(key));
        }
    }

    public void setHeader(String name, String value) {
        this.headers.put(name, value);
    }

    public String getUserAgent() {
        return USER_AGENT;
    }

    public void setUserAgent(String userAgent) {
        this.USER_AGENT = userAgent;
        headers.put("User-Agent", userAgent);
    }

    public String getAccept() {
        return ACCEPT;
    }

    public void setAccept(String accept) {
        this.ACCEPT = accept;
        headers.put("Accept", accept);
    }

    public String getContentType() {
        return CONTENT_TYPE;
    }

    public void setContentType(String contentType) {
        this.CONTENT_TYPE = contentType;
        headers.put("Content-Type", contentType);
    }

    public String getCharSet() {
        return CHARSET;
    }

    public void setCharSet(String charset) {
        this.CHARSET = charset;
        headers.put("Charset", charset);
    }

    public WebClient() {
        headers.put("Content-Type", CONTENT_TYPE);
        headers.put("Accept", ACCEPT);
        headers.put("Connection", CONNECTION);
        headers.put("User-Agent", USER_AGENT);
        headers.put("Charset", CHARSET);
    }

    /*接口请求方法 */
    //请求返回文本
    public String uploadString(String url, WebClientMethod method) {
        byte[] data = downloadData(url, method, null);
        return new String(data, StandardCharsets.UTF_8);
    }

    public String uploadString(String url, WebClientMethod method, String body) {
        byte[] data = downloadData(url, method, body == null ? null : body.getBytes(StandardCharsets.UTF_8));
        return new String(data, StandardCharsets.UTF_8);
    }

    public String downloadString(String url) {
        return downloadString(url, WebClientMethod.GET, null);
    }

    public String downloadString(String url, WebClientMethod method, byte[] body) {
        byte[] data = downloadData(url, method, body);
        return new String(data, StandardCharsets.UTF_8);
    }

    //下载附件
    public byte[] downloadData(String url) {
        return downloadStream(url, WebClientMethod.GET, null);
    }

    public byte[] downloadData(String url, WebClientMethod method, byte[] data) {
        return downloadStream(url, method, data);
    }

    //上传附件
    public String uploadFiles(String url, MultipartFile file) {
        return uploadFiles(url, null, file);
    }

    //上传附件
    public String uploadFiles(String url, Map<String, String> parameters, MultipartFile file) {
        byte[] data = uploadFilesData(url, WebClientMethod.POST, parameters, new MultipartFile[]{file});
        return new String(data, StandardCharsets.UTF_8);
    }

    public byte[] uploadFilesData(String url, WebClientMethod method, Map<String, String> parameters,
                                  MultipartFile[] files) {
        return uploadFilesStream(url, method, parameters, files);
    }

    private byte[] downloadStream(String url, WebClientMethod method, byte[] data) {

        InputStream in = null;// 读取响应输入流
        DataOutputStream out = null;
        HttpURLConnection httpConn = null;
        try {
            // 创建URL对象
            if (cookie != null) {
                headers.put("Cookie", cookie.getValues());
            }
            URL connURL = new URL(url);
            // 打开URL连接
            httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性
            for (String key : headers.keySet()) {
                httpConn.setRequestProperty(key, headers.get(key));
            }
            if (method == WebClientMethod.GET) {
                httpConn.setRequestMethod("GET");
                httpConn.connect();
                // 定义BufferedReader输入流来读取URL的响应,并设置编码方式
                if (httpConn.getResponseCode() == 200) {
                    // 定义BufferedReader输入流来读取URL的响应，设置编码方式
                    updateCookie(httpConn);
                    in = httpConn.getInputStream();

                } else {
                    in = httpConn.getErrorStream();
                }
            } else {
                if (method == WebClientMethod.POST) {
                    httpConn.setRequestMethod("POST");

                } else if (method == WebClientMethod.PUT) {
                    httpConn.setRequestMethod("PUT");

                } else if (method == WebClientMethod.DELETE) {
                    httpConn.setRequestMethod("DELETE");
                }
                // 设置POST方式
                httpConn.setDoInput(true);
                httpConn.setDoOutput(true);
                httpConn.setUseCaches(false);
                // 获取HttpURLConnection对象对应的输出流
                out = new DataOutputStream(httpConn.getOutputStream());
                // 发送请求参数
                if (data != null) {
                    out.write(data);
                }
                // flush输出流的缓冲
                out.flush();
                if (httpConn.getResponseCode() == 200) {
                    // 定义BufferedReader输入流来读取URL的响应，设置编码方式
                    updateCookie(httpConn);
                    in = httpConn.getInputStream();

                } else {
                    in = httpConn.getErrorStream();
                }
            }
            return inputToByte(in);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (httpConn != null) {
                    httpConn.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private byte[] uploadFilesStream(String url, WebClientMethod method, Map<String, String> parameters,
                                     MultipartFile[] files) {
        String BOUNDARY = UUID.randomUUID().toString();
        String PREFIX = "--", LINEND = "\r\n";
        String MULTIPART_FROM_DATA = "multipart/form-data";

        InputStream in = null;// 读取响应输入流
        DataOutputStream outStream = null;
        HttpURLConnection conn = null;
        try {
            // 创建URL对象
            if (cookie != null) {
                headers.put("Cookie", cookie.getValues());
            }
            URL uri = new URL(url);
            conn = (HttpURLConnection) uri.openConnection();
            conn.setReadTimeout(30 * 1000); // 缓存的最长时间
            conn.setDoInput(true);// 允许输入
            conn.setDoOutput(true);// 允许输出
            conn.setUseCaches(false); // 不允许使用缓存
            if (method == WebClientMethod.GET) {
                conn.setRequestMethod("GET");
            } else if (method == WebClientMethod.POST) {
                conn.setRequestMethod("POST");
            } else if (method == WebClientMethod.PUT) {
                conn.setRequestMethod("PUT");
            } else if (method == WebClientMethod.DELETE) {
                conn.setRequestMethod("DELETE");
            }
            for (String key : headers.keySet()) {
                conn.setRequestProperty(key, headers.get(key));
            }
            conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
                    + ";boundary=" + BOUNDARY);

            StringBuilder sb = new StringBuilder();

            if (parameters != null) {
                // 首先组拼文本类型的参数
                for (Map.Entry<String, String> entry : parameters.entrySet()) {
                    sb.append(PREFIX);
                    sb.append(BOUNDARY);
                    sb.append(LINEND);
                    sb.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"").append(LINEND);
                    sb.append("Content-Type: text/plain; charset=").append(CHARSET).append(LINEND);
                    sb.append("Content-Transfer-Encoding: 8bit").append(LINEND).append(LINEND);
                    sb.append(entry.getValue());
                    sb.append(LINEND);
                }

            }

            outStream = new DataOutputStream(
                    conn.getOutputStream());
            if (!StringUtils.isEmpty(sb.toString())) {
                outStream.write(sb.toString().getBytes());
            }


            // 发送文件数据
            if (files != null) {
                for (MultipartFile file : files) {
                    String fileName = new String(Objects.requireNonNull(file.getOriginalFilename()).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                    StringBuilder sb1 = new StringBuilder();
                    sb1.append(PREFIX);
                    sb1.append(BOUNDARY);
                    sb1.append(LINEND);
                    sb1.append("Content-Disposition: form-data; name=\"").append(file.getName()).append("\"; filename=\"").append(fileName).append("\"").append(LINEND);
                    sb1.append("Content-Type: ").append(file.getContentType()).append("; charset=").append(CHARSET).append(LINEND).append(LINEND);
                    outStream.write(sb1.toString().getBytes());

                    InputStream is = file.getInputStream();
                    byte[] buffer = new byte[1024];
                    int len = 0;
                    while ((len = is.read(buffer)) != -1) {
                        outStream.write(buffer, 0, len);
                    }

                    is.close();
                    outStream.write(LINEND.getBytes());
                }

            }

            // 请求结束标志
            byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
            outStream.write(end_data);
            outStream.flush();
            // 得到响应码
            if (conn.getResponseCode() == 200) {
                // 定义BufferedReader输入流来读取URL的响应，设置编码方式
                updateCookie(conn);
                in = conn.getInputStream();

            } else {
                in = conn.getErrorStream();
            }
            return inputToByte(in);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public void updateCookie(HttpURLConnection conn) {
        if (cookie != null) {
            Map<String, List<String>> headers = conn.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : headers.keySet()) {
                if (!StringUtil.isEmpty(key) && "Set-Cookie".equalsIgnoreCase(key)) {
                    for (int i = headers.get(key).size() - 1; i >= 0; i--) {
                        cookie.addCookie(headers.get(key).get(i));
                    }
                    //System.out.println(key + "\t：\t" + headers.get(key));
                }
            }
        }
    }


    public byte[] inputToByte(InputStream input) {
        if (input == null) {
            return new byte[0];
        }
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int rc = 0;
            while ((rc = input.read(buff, 0, buff.length)) > 0) {
                baos.write(buff, 0, rc);
            }
            input.close();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public String getPostBody(Map<String, String> parameters) {
        List<String> lstParams = new ArrayList<>();
        for (String name : parameters.keySet()) {
            lstParams.add(name + "=" + StringUtil.urlEncode(parameters.get(name)));
        }
        return String.join("&", lstParams);

    }

    public static void main(String[] args) {
        WebClient webClient = new WebClient();
        String body = webClient.getPostBody(
                new HashMap<String, String>() {
                    {
                        put("loginId", "admin");
                        put("password", "e10adc3949ba59abbe56e057f20f883e");
                        put("hasVerification", "normal");
                        put("img_code", "");
                    }
                }
        );
        HttpCookie cookie = new HttpCookie();
        webClient.setCookie(cookie);
        String ret = webClient.uploadString("http://localhost:8081/zsjt/sys/login", WebClientMethod.POST, body);
        String token = JSONObject.parseObject(ret).getJSONObject("data").getString("token");
        System.out.println(ret);

        webClient.setHeader("X-Auth-Token", token);
        ret = webClient.uploadString("http://localhost:8081/zsjt/zsjt/amountAndPersonnelInfo/test", WebClientMethod.POST, null);
        System.out.println(ret);
        byte[] fileData = webClient.downloadData("http://localhost/ld/nginx-1.20.0.zip");
        System.out.println(fileData.length);
    }
}
