package com.xnw.frame.controller;

import com.xnw.captcha.model.common.ResponseModel;
import com.xnw.captcha.model.vo.CaptchaVO;
import com.xnw.captcha.service.CaptchaService;
import com.xnw.captcha.util.StringUtils;
import com.xnw.frame.auth.token.PassToken;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 图形验证码控制器
 *
 * @author T470
 */
@RestController
@RequestMapping("/frame/captcha")
@Tag(name = "图形验证码控制器")
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    /**
     * 获取验证图形
     *
     * @param data
     * @param request
     * @return
     */
    @Operation(summary = "获取验证图形")
    @Parameters({
            @Parameter(name = "验证码参数"),
            @Parameter(name = "HttpServletRequest")
    })
    @PassToken
    @PostMapping("/get")
    public ResponseModel get(@RequestBody CaptchaVO data, HttpServletRequest request) {
        assert request.getRemoteHost() != null;
        data.setBrowserInfo(getRemoteId(request));
        return captchaService.get(data);
    }

    /**
     * 检查验证结果
     *
     * @param data
     * @param request
     * @return
     */
    @Operation(summary = "检查验证结果")
    @PassToken
    @PostMapping("/check")
    public ResponseModel check(@RequestBody CaptchaVO data, HttpServletRequest request) {
        data.setBrowserInfo(getRemoteId(request));
        return captchaService.check(data);
    }

    //@PostMapping("/verify")
    public ResponseModel verify(@RequestBody CaptchaVO data, HttpServletRequest request) {
        return captchaService.verification(data);
    }

    public static String getRemoteId(HttpServletRequest request) {
        String xfwd = request.getHeader("X-Forwarded-For");
        String ip = getRemoteIpFromXfwd(xfwd);
        String ua = request.getHeader("user-agent");
        if (StringUtils.isNotBlank(ip)) {
            return ip + ua;
        }
        return request.getRemoteAddr() + ua;
    }

    private static String getRemoteIpFromXfwd(String xfwd) {
        if (StringUtils.isNotBlank(xfwd)) {
            String[] ipList = xfwd.split(",");
            return StringUtils.trim(ipList[0]);
        }
        return null;
    }

}
