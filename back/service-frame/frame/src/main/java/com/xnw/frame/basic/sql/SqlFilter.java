package com.xnw.frame.basic.sql;


import com.xnw.frame.basic.utils.dataformat.Underline2Camel;
import org.springframework.util.StringUtils;


/**
 * SQL过滤
 *
 * @author keeny
 * @ClassName: SQLFilter
 * @Description: SQL过滤
 * @date 2019年1月8日 下午1:37:55
 */
public class SqlFilter {

    /**
     * SQL注入过滤
     *
     * @param str 待验证的字符串
     */
    public static String sqlInject(String str, boolean flag) {
        if (StringUtils.hasLength(str)) {
            return null;
        }
        //去掉'|"|;|\字符
        str = StringUtils.replace(str, "'", "");
        str = StringUtils.replace(str, "\"", "");
        str = StringUtils.replace(str, ";", "");
        str = StringUtils.replace(str, "\\", "");
        //转换成小写
        str = flag ? Underline2Camel.camel2Underline(str) : str.toLowerCase();
        //非法字符
        String[] keywords = {"master", "truncate", "insert", "select", "delete", "update", "declare", "alert", "drop"};

        //判断是否包含非法字符
        for (String keyword : keywords) {
            if (str.contains(keyword)) {
                throw new RuntimeException("包含非法字符");
            }
        }
        return str;
    }


}
