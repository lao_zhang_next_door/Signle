package com.xnw.frame.controller;

import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.FrameAttach;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.service.FrameAttachService;
import com.xnw.frame.service.FrameUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 附件控制器
 *
 * @author wzl
 */
@Tag(name = "附件表")
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameAttach")
public class FrameAttachController extends BaseController {

    @Autowired
    private FrameAttachService frameAttachService;

    /**
     * 存储物理路径
     */
    @Value(value = "${upload.file.path}")
    private String filePath;

    @Autowired
    private FrameUserService frameUserService;

    @Operation(summary = "附件列表")
    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameAttach> attachList = frameAttachService.selectByLimit(query);
        PageUtils pageUtil = new PageUtils(attachList, attachList.size(), query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameAttach frameAttach = new FrameAttach();
        frameAttach.setRowGuid(rowGuid);
        List<FrameAttach> attachList = frameAttachService.select(frameAttach);
        return R.ok().put("data", attachList);
    }

    @Operation(summary = "附件新增")
    @PostMapping("/add")
    private R add(@RequestBody FrameAttach frameAttach) {
        frameAttach.init();
        frameAttachService.insert(frameAttach);
        return R.ok();
    }

    @Operation(summary = "附件修改")
    @PutMapping("/update")
    private R update(@RequestBody FrameAttach frameAttach) {
        frameAttachService.updateByRowGuidSelective(frameAttach);
        return R.ok();
    }

    @Operation(summary = "附件删除")
    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids) {
        frameAttachService.deleteBatch(rowGuids);
        return R.ok();
    }

    @Operation(summary = "上传控件")
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public R uploadFile(
            @RequestParam(value = "formRowGuid", required = false) String formRowGuid,
            @RequestParam(value = "uploadify", required = false) MultipartFile file,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString();
        //文件存放路径
        String filePath = uuid + fileName.substring(fileName.lastIndexOf("."));
        File fileSave = new File(this.filePath + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if (!fileSave.exists()) {
            fileSave.mkdirs();
        }
        String path = fileSave.getCanonicalPath();
        File targetFile = new File(path, filePath);
        if (!targetFile.exists()) {
            File dir = new File(targetFile.getParent());
            dir.mkdirs();
            targetFile.createNewFile();
        }
        try {

            file.transferTo(targetFile);
            //往附件表中插入记录，返回唯一标识
            FrameAttach frameAttach = new FrameAttach();
            frameAttach.init();
            frameAttach.setAttachName(fileName);
            frameAttach.setContentType(fileName.substring(fileName.lastIndexOf(".")));
            frameAttach.setContentUrl(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/" + filePath);
            frameAttach.setContentLength((int) targetFile.length());
            frameAttach.setFormRowGuid(formRowGuid);
//            frameAttach.setUploadUserGuid((String) getCurrentUser().get("userGuid"));
            frameAttachService.insert(frameAttach);

            Map<String, String> map = new HashMap<>(3);
            map.put("src", frameAttach.getContentUrl());
            map.put("name", "fileName");
            map.put("rowGuid", frameAttach.getRowGuid());
            return R.ok().put("data", map);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return R.error("附件上传出错");
        }
    }

    @Operation(summary = "上传控件多选")
    @RequestMapping(value = "/uploadMultiFile", method = RequestMethod.POST)
    public R uploadMultiFile(
            @RequestParam(value = "formRowGuid", required = false) String formRowGuid,
            @RequestParam(value = "userGuid", required = true) String userGuid,
            @RequestParam(value = "uploadify", required = false) MultipartFile[] files,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Map<String, String> map = new HashMap<>(3);
            for (MultipartFile file : files) {
                String fileName = file.getOriginalFilename();
                String uuid = UUID.randomUUID().toString();//文件存放路径
                assert fileName != null;
                String filePath = uuid + fileName.substring(fileName.lastIndexOf("."));
                File fileSave = new File(this.filePath + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                if (!fileSave.exists()) {
                    fileSave.mkdirs();
                }
                String path = fileSave.getCanonicalPath();
                File targetFile = new File(path, filePath);
                if (!targetFile.exists()) {
                    File dir = new File(targetFile.getParent());
                    dir.mkdirs();
                    targetFile.createNewFile();
                }


                file.transferTo(targetFile);
                //往附件表中插入记录，返回唯一标识
                FrameAttach frameAttach = new FrameAttach();
                frameAttach.init();
                frameAttach.setAttachName(fileName);
                frameAttach.setContentType(fileName.substring(fileName.lastIndexOf(".")));
                frameAttach.setContentUrl(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/" + filePath);
                frameAttach.setContentLength((int) targetFile.length());
                frameAttach.setFormRowGuid(formRowGuid);
                frameAttach.setUploadUserGuid(userGuid);
                frameAttachService.insert(frameAttach);


                map.put("src", frameAttach.getContentUrl());
                map.put("name", "fileName");
                map.put("rowGuid", frameAttach.getRowGuid());

            }
            return R.ok().put("data", map);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return R.error("附件上传出错");
        }
    }

    /**
     * <p>Title: uploadFile</p>
     * <p>Description: </p>
     *
     * @param file
     * @param formRowGuid
     * @param userGuid
     * @return
     * @throws IOException
     * @author hero
     */
    @Operation(summary = "上传头像")
    @ResponseBody
    @RequestMapping(value = "/uploadAvatar", method = RequestMethod.POST)
    public R uploadHeadImg(@RequestParam(value = "avatar", required = false) MultipartFile file,
                           @RequestParam(value = "formRowGuid", required = false) String formRowGuid,
                           @RequestParam(value = "userGuid", required = false) String userGuid,
                           HttpServletRequest request, HttpServletResponse response) throws IOException {

        String fileName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString();
        //文件存放路径
        assert fileName != null;
        String filePath = uuid + fileName.substring(fileName.lastIndexOf("."));
        File fileSave = new File(this.filePath + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if (!fileSave.exists()) {
            fileSave.mkdirs();
        }
        String path = fileSave.getCanonicalPath();
        File targetFile = new File(path, filePath);
        if (!targetFile.exists()) {
            File dir = new File(targetFile.getParent());
            dir.mkdirs();
            targetFile.createNewFile();
        }
        try {
            file.transferTo(targetFile);
            //往附件表中插入记录，返回唯一标识
            FrameAttach frameAttach = new FrameAttach();
            frameAttach.init();
            frameAttach.setAttachName(fileName);
            frameAttach.setContentType(fileName.substring(fileName.lastIndexOf(".")));
            frameAttach.setContentUrl(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/" + filePath);
            frameAttach.setContentLength((int) targetFile.length());
            frameAttach.setFormRowGuid(formRowGuid);
            frameAttach.setUploadUserGuid(userGuid);
            frameAttachService.insert(frameAttach);

            Map<String, String> map = new HashMap<>(3);
            map.put("src", frameAttach.getContentUrl());
            map.put("name", "fileName");
            map.put("rowGuid", frameAttach.getRowGuid());

            FrameUser user = new FrameUser();
            user.setRowGuid(request.getParameter("userGuid"));
            //
            user.setHeadimgGuid(frameAttach.getRowGuid());
            frameUserService.updateByRowGuidSelective(user);
            return R.ok().put("data", map);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return R.error("用户头像上传出错");
        }

    }

    /**
     * 根据formRowGuid查询附件列表
     *
     * @param formRowGuid
     * @return
     */
    @GetMapping("/getListByFormRowGuid")
    private R getListByFormRowGuid(@RequestParam String formRowGuid) {
        FrameAttach frameAttach = new FrameAttach();
        frameAttach.setFormRowGuid(formRowGuid);
        List<FrameAttach> attachList = frameAttachService.select(frameAttach);
        return R.ok().put("data", attachList);
    }

    /**
     * 根据formRowGuid删除附件
     *
     * @param formRowGuid
     * @return
     */
    @DeleteMapping("/deleteFileByFormRowGuid")
    private R deleteFileByFormRowGuid(@RequestParam String formRowGuid) {
        frameAttachService.deleteFileByFormRowGuid(formRowGuid);
        return R.ok();
    }

    /**
     * 根据formRowGuid删除附件
     *
     * @param rowGuid
     * @return
     */
    @DeleteMapping("/deleteFileByRowGuid")
    public R deleteFileByRowGuid(@RequestParam String rowGuid) {
        frameAttachService.deleteFileByRowGuid(Collections.singletonList(rowGuid));
        return R.ok();
    }
}
 