package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.FrameRole;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

public interface FrameRoleDao extends BaseDao<FrameRole> {

	/**
	 * 根据角色guid 查询该角色所拥有的菜单模块
	 *
	 * @param roleGuid
	 * @return
	 */
	List<FrameModule> getModuleByRoleGuid(String roleGuid);

	/**
	 * 根据角色名获取Guids
	 *
	 * @param roleNames
	 * @return
	 */
	List<String> getRoleGuidsByNames(String roleNames);

	/**
	 * 根据用户Guid获取角色
	 *
	 * @param userGuid
	 * @return
	 */
	@MapKey("roleGuid")
	List<Map<String, Object>> getRolesByUserGuid(String userGuid);
}
