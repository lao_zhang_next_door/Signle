package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.frame.entity.pojo.FrameCodeValuePojo;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

/**
 * @author wzl
 */
public interface FrameCodeValueService extends BaseService<FrameCodeValue> {

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    List<FrameCodeValuePojo> selectParentCode(String rowGuid);

    /**
     * 根据codeName 以及对应的value 值返回对应的文本
     *
     * @param codeName
     * @param codeValue
     * @return
     */
    String getItemTextByCodeNameAndCodeValue(String codeName, String codeValue);

    /**
     * 缓存所有代码项：方式HashList
     */
//    @Async("threadPoolExecutor")
    void cacheAllCodeValue();

    /**
     * 获取所有代码项
     *
     * @return list
     */
    List<FrameCodeValuePojo> getCodeValueToCache();

    /**
     * 按rowGuid更新缓存代码
     *
     * @param rowGuid
     */
    @Async("threadPoolExecutor")
    void cacheCodesMapByValue(String rowGuid);

    /**
     * 根据codeName更新
     *
     * @param codeName
     */
    @Async("threadPoolExecutor")
    void cacheCodesMapByName(String codeName);

    /**
     * 根据代码项名称查询
     *
     * @param codeName
     * @return
     */
    List<FrameCodeValuePojo> getValueByCodeName(String codeName);

    /**
     * 按guid查询代码项名称
     *
     * @param rowGuid
     * @return
     */
    String getCodeNameByGuid(String rowGuid);

    /**
     * 根据codeName获取缓存
     *
     * @param codeName
     * @return list
     */
    List<FrameCodeValue> getCacheByCodeName(String codeName);

    /**
     * 通过代码项值获取代码项文本
     *
     * @param codeName
     * @param value
     * @return
     */
    String getItemTextByValue(String codeName, Object value);

    /**
     * 通过代码项文本获取代码项值
     *
     * @param codeName
     * @param text
     * @return
     */
    String getItemValueByText(String codeName, String text);

}
