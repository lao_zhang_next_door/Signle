package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.FrameUserRole;

import java.util.List;

public interface FrameUserRoleDao extends BaseDao<FrameUserRole> {

    /**
     * 通过用户guid 获取角色名集合
     *
     * @param userGuid
     * @return
     */
    List<String> selectRoleNameListByUserGuid(String userGuid);

    /**
     * 根据角色Guid查询用户列表
     *
     * @param roleGuid
     * @return
     */
    List<FrameUser> getUserListByRoleName(String roleGuid);
}
