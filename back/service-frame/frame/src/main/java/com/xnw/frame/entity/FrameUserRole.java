package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

public class FrameUserRole extends BaseEntity{
	
	public FrameUserRole(){}
	
	/**
	 * @param init true则有默认值
	 */
	public FrameUserRole(Boolean init){
		super(init);
	}
	
	private String userGuid;
	
	private String roleGuid;

	public String getUserGuid() {
		return userGuid;
	}

	public FrameUserRole setUserGuid(String userGuid) {
		this.userGuid = userGuid;
		return this;
	}

	public String getRoleGuid() {
		return roleGuid;
	}

	public FrameUserRole setRoleGuid(String roleGuid) {
		this.roleGuid = roleGuid;
		return this;
	}
}
