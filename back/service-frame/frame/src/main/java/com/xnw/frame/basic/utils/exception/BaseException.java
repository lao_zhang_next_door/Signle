package com.xnw.frame.basic.utils.exception;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * 通用异常
 *
 * @author hero
 */
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 5782968730281544562L;

	private int status = INTERNAL_SERVER_ERROR.value();

	private final String msg;

	public BaseException(String message) {
		super(message);
		this.msg = message;
	}

	public BaseException(HttpStatus status, String message) {
		super(message);
		this.status = status.value();
		this.msg = message;
	}

	public BaseException(HttpStatus status) {
		super(status.getReasonPhrase());
		this.status = status.value();
		this.msg = status.getReasonPhrase();
	}

	public int getStatus() {
		return status;
	}

	public String getMsg() {
		return msg;
	}
}
