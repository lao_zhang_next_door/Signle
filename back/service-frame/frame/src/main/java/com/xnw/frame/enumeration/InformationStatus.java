package com.xnw.frame.enumeration;

/**
 * 信息发布状态枚举
 * @author hero
 *
 */
public enum InformationStatus {

	YFB("已发布"),
	DSH("待审核");

    private final String value;
    private InformationStatus(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
}
