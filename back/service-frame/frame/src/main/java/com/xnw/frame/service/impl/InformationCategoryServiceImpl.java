package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.InformationCategoryDao;
import com.xnw.frame.entity.InformationCategory;
import com.xnw.frame.service.InformationCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InformationCategoryServiceImpl extends BaseServiceImpl<InformationCategory> implements InformationCategoryService  {

	public InformationCategoryServiceImpl(InformationCategoryDao dao) { super(dao); }

	@Autowired
	private InformationCategoryDao informationCategoryDao;

}

