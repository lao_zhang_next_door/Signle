package com.xnw.frame.basic.base;


import com.xnw.frame.basic.base.mapper.ExtendMapper;
import com.xnw.frame.basic.mybatis.mapper.common.Mapper;

public interface BaseDao<T> extends Mapper<T>, ExtendMapper<T> {



}
