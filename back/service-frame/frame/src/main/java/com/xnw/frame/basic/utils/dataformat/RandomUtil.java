package com.xnw.frame.basic.utils.dataformat;

import java.util.Random;

/**
 * 随机数工具类
 *
 * @author WuYB
 * @date 2017/5/12
 */
public class RandomUtil {

    private static class SingletonHolder {
        private static final RandomUtil INSTANCE = new RandomUtil();
    }

    private RandomUtil() {
    }

    public static RandomUtil getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * 获取任意区间的随机整数
     *
     * @param min 最小值（能到）
     * @param max 最大值（不到）
     * @return 随机数
     */
    public int random(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    /**
     * 产生默认验证码，4位不重复
     *
     * @return String
     */
    public String getSecurityCode() {
        return getSecCode(4, true);
    }

    /**
     * 产生随机位数验证码，不重复
     *
     * @param length 位数
     * @return String
     */
    public String getSecurityCode(int length) {
        return getSecCode(length, true);
    }

    /**
     * 产生随机位数验证码
     *
     * @param length    位数
     * @param canRepeat 是否重复
     * @return String
     */
    public String getSecurityCode(int length, boolean canRepeat) {
        return getSecCode(length, canRepeat);
    }

    /**
     * 产生任意长度大小写任意的随机字符串
     *
     * @param length 长度
     * @return String
     */
    public String getRandomStrByLength(int length) {
        String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 产生任意长度随机数字的字符串
     *
     * @param length 长度
     * @return String
     */
    public String getRandomNumByLength(int length) {
        StringBuilder buf = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; ++i) {
            buf.append(random.nextInt(10));
        }
        return buf.toString();
    }

    /**
     * 产生长度任意的验证码
     *
     * @param length    长度
     * @param canRepeat 是否能够出现重复的字符，如果为true，则可能出现 5578这样包含两个5,如果为false，则不可能出现这种情况
     * @return String 验证码
     */
    private String getSecCode(int length, boolean canRepeat) {
        // 随机抽取len个字符
        // 字符集合(除去易混淆的数字0、数字1、字母l、字母o、字母O)
        char[] codes = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b',
                'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'p',
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
                'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        // 字符集合长度
        int n = codes.length;
        // 存放抽取出来的字符
        char[] result = new char[length];
        // 判断能否出现重复的字符
        if (canRepeat || length > n) {
            for (int i = 0; i < result.length; i++) {
                // 索引 0 and n-1
                int r = (int) (Math.random() * n);
                // 将result中的第i个元素设置为codes[r]存放的数值
                result[i] = codes[r];
            }
        } else {
            for (int i = 0; i < result.length; i++) {
                // 索引 0 and n-1
                int r = (int) (Math.random() * n);
                // 将result中的第i个元素设置为codes[r]存放的数值
                result[i] = codes[r];
                // 必须确保不会再次抽取到那个字符，因为所有抽取的字符必须不相同。
                // 因此，这里用数组中的最后一个字符改写codes[r]，并将n减1
                codes[r] = codes[n - 1];
                n--;
            }
        }
        return String.valueOf(result);
    }
}
