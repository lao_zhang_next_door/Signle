package com.xnw.frame.basic.network;

public enum WebClientMethod {
    POST, GET, PUT, DELETE
}
