package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.basic.utils.dataformat.MD5Util;
import com.xnw.frame.basic.utils.exception.BaseException;
import com.xnw.frame.basic.utils.frame.CommUtil;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.StringUtil;
import com.xnw.frame.dao.FrameUserDao;
import com.xnw.frame.entity.FrameConfig;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.FrameUserRole;
import com.xnw.frame.entity.pojo.FrameUserPasswordVo;
import com.xnw.frame.entity.pojo.FrameUserPojo;
import com.xnw.frame.entity.pojo.XnwUser;
import com.xnw.frame.enumeration.IsOrNot;
import com.xnw.frame.enumeration.UserStatus;
import com.xnw.frame.redis.RedisService;
import com.xnw.frame.service.FrameConfigService;
import com.xnw.frame.service.FrameUserRoleService;
import com.xnw.frame.service.FrameUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class FrameUserServiceImpl extends BaseServiceImpl<FrameUser> implements FrameUserService {

    public FrameUserServiceImpl(FrameUserDao dao) {
        super(dao);
    }

    @Value("${user.redis-name}")
    private String userDataRedisName;

    @Autowired
    private FrameUserDao frameUserDao;

    @Autowired
    private FrameUserRoleService frameUserRoleService;

    @Autowired
    private FrameConfigService frameConfigService;

    @Autowired
    private RedisService redisService;

    @Override
    public List<FrameUserPojo> getList(Query query) {
        return frameUserDao.getList(query);
    }

    @Override
    public FrameUserPojo selectDetailByUserGuid(String userGuid) {
        return frameUserDao.selectDetailByUserGuid(userGuid);
    }

    @Override
    public FrameUserPojo selectDetailByLoginId(String userGuid) {
        return frameUserDao.selectDetailByLoginId(userGuid);
    }

    @Override
    public List<FrameRole> selectRoleListByUserGuid(String userGuid) {
        return frameUserDao.selectRoleListByUserGuid(userGuid);
    }

    @Override
    public String addFrameUser(FrameUserPojo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())) {
            throw new BaseException("登录名不可为空");
        }

        //查询登录名是否重复
        FrameUser searchData = new FrameUser();
        searchData.setLoginId(frameUserPojo.getLoginId());
        int count = selectCount(searchData);
        if (count > 0) {
            throw new BaseException("已存在该登录账户");
        }

        //初始化用户并设置初始密码
        frameUserPojo.init();
        frameUserPojo.setStatus(UserStatus.NORMAL.getCode());
        frameUserPojo.setIsLimit(IsOrNot.NOT.getCode());
        //设置初始密码 设置失败则禁用
        if (StringUtils.isBlank(this.getInitPassword())) {
            frameUserPojo.setStatus(UserStatus.DISABLE.getCode());
        } else {
            frameUserPojo.setPassword(this.getInitPassword());
        }

        this.insertSelective(frameUserPojo);

        //插入用户所拥有的角色
        List<FrameRole> roleList = frameUserPojo.getRoleList();
        List<FrameUserRole> frameUserRoleList = new ArrayList<FrameUserRole>();

        if (roleList != null) {
            FrameUserRole fur = null;
            for (FrameRole role : roleList) {
                fur = new FrameUserRole(true);
                fur.setUserGuid(frameUserPojo.getRowGuid());
                fur.setRoleGuid(role.getRowGuid());
                frameUserRoleList.add(fur);
            }
            if (!frameUserRoleList.isEmpty()) {
                frameUserRoleService.insertList(frameUserRoleList);
            }
        }
        return frameUserPojo.getRowGuid();
    }

    @Override
    @Transactional
    public void updateFrameUser(FrameUserPojo frameUserPojo) {

        if (StringUtil.isBlank(frameUserPojo.getLoginId())
                || StringUtil.isBlank(frameUserPojo.getRowGuid())) {
            throw new BaseException("参数不可为空");
        }

        FrameUser originUser = selectOne(new FrameUser(frameUserPojo.getRowGuid()));

        if (!StringUtil.isBlank(frameUserPojo.getPassword())) {
            throw new BaseException("参数不可更新");
        }

        if (!frameUserPojo.getLoginId().equals(originUser.getLoginId())) {
            throw new BaseException("登录名不可修改");
        }

        updateByRowGuidSelective(frameUserPojo);

        //删除
        frameUserRoleService.delete(new FrameUserRole().setUserGuid(frameUserPojo.getRowGuid()));
        //再新增
        List<FrameRole> roleList = frameUserPojo.getRoleList();
        if (roleList != null && !roleList.isEmpty()) {
            List<FrameUserRole> userRoles = new ArrayList<>();
            roleList.forEach(new Consumer<FrameRole>() {
                @Override
                public void accept(FrameRole frameRole) {
                    FrameUserRole frameUserRole = new FrameUserRole(true)
                            .setRoleGuid(frameRole.getRowGuid())
                            .setUserGuid(frameUserPojo.getRowGuid());
                    userRoles.add(frameUserRole);
                }
            });
            frameUserRoleService.insertList(userRoles);
        }

    }

    @Override
    public String getInitPassword() {

        FrameConfig config = new FrameConfig();
        config.setConfigName("initPassword");
        config = frameConfigService.selectOne(config);

        //加密
        if (!StringUtils.isBlank(config.getConfigValue())) {
            return MD5Util.md5Password(config.getConfigValue()).toLowerCase();
        } else {
            return "";
        }
    }

    @Override
    public void changePassword(FrameUserPasswordVo frameUserPasswordVo) {
        isFieldBlank(frameUserPasswordVo, "userGuid", "originPassword", "newPassword");

        if (frameUserPasswordVo.getNewPassword().equals(frameUserPasswordVo.getOriginPassword())) {
            throw new BaseException("修改后的密码不可与原来的一致");
        }

        if (!frameUserPasswordVo.getNewPassword()
                .equals(frameUserPasswordVo.getVerifyPassword())) {
            throw new BaseException("两次输入密码不一致");
        }

        FrameUser frameUser = frameUserDao.selectOne(new FrameUser(frameUserPasswordVo.getUserGuid()));
        if (frameUser == null) {
            throw new BaseException("修改密码失败");
        }

        if (!frameUser.getPassword().equals(frameUserPasswordVo.getOriginPassword())) {
            throw new BaseException("修改密码失败");
        }

        frameUserDao.updateByRowGuidSelective(
                new FrameUser(frameUser.getRowGuid()).setPassword(frameUserPasswordVo.getNewPassword()));

    }

    @Override
//	@PreAuthorize("hasAuthority('管理员') or hasAuthority('社区管理员')")
    public List<FrameUser> getUserByCurrentAuthority() {

//		Map<String,Object> userMap = getCurrentUser();
//		String deptGuid = (String) userMap.get("deptGuid");
//
//		Example example = new Example(FrameUser.class);
//		example.selectProperties("userName","loginId","rowGuid","createTime","deptGuid","mobile");
//		if(permissionService.hasPerm("管理员")){
//			example.createCriteria().andEqualTo("delFlag", DelFlag.Normal.getCode());
//			return frameUserDao.selectByExample(example);
//		}else{
//			example.createCriteria().andEqualTo("deptGuid",deptGuid).andEqualTo("delFlag", DelFlag.Normal.getCode());
//			return frameUserDao.selectByExample(example);
//		}
        return null;
    }

    /**
     * 获取当前用户的部门的上级部门的所有人员数据
     *
     * @return
     */
    @Override
    public List<FrameUser> getParentDeptUser() {
//		Map<String,Object> userMap = getCurrentUser();
//		FrameDept dept = frameDeptService.selectOne(new FrameDept(String.valueOf(userMap.get("deptGuid"))));
        FrameUser user = new FrameUser();
//		user.setDeptGuid(dept.getParentGuid());
        return frameUserDao.select(user);
    }

    /**
     * 用户注册
     *
     * @param user
     * @param session
     */
    @Override
    public void registerAppUser(FrameUserPojo user, HttpSession session) {

        //校验手机号码是否已经注册
        FrameUser frame = new FrameUser();
        frame.setLoginId(user.getLoginId());
        List<FrameUser> list = frameUserDao.select(frame);
        if (list.size() != 0) {
            throw new BaseException("该用户已经注册，请不要重复注册！");
        }
        //校验ImgCode(验证码是否正确)
        if (session.getAttribute("img_code") == null) {
            throw new BaseException("请刷新验证码再试");
        }
        if (user.getImgCode() != null && !user.getImgCode().toUpperCase().equals(session.getAttribute("img_code").toString())) {
            throw new BaseException("验证码错误");
        }

        //将数据保存
        user.initNull();
        frameUserDao.insert(user);

    }

    /**
     * 校验验证码
     *
     * @param isForget
     * @param session
     */
    @Override
    public void checkIsSetImgCode(String isForget, HttpSession session) {
        if ("forget".equals(isForget)) {
            if (session.getAttribute("img_code_forget") != null)
                throw new BaseException("请不要重复点击");
        } else if (session.getAttribute("img_code") != null) {
            throw new BaseException("请不要重复点击");
        }
    }

    /**
     * 忘记密码
     *
     * @param user
     * @param session
     */
    @Override
    public void forgetPassword(FrameUserPojo user, HttpSession session) {

        //校验手机号码是否已经注册
        FrameUser frame = new FrameUser();
        frame.setLoginId(user.getLoginId());
        List<FrameUser> list = frameUserDao.select(frame);
        if (list.size() == 0) {
            throw new BaseException("该用户尚未注册！");
        }
        //校验ImgCode(验证码是否正确)
        if (session.getAttribute("img_code_forget") == null) {
            throw new BaseException("请刷新验证码再试");
        }
        if (user.getImgCode() != null && !user.getImgCode().toUpperCase().equals(session.getAttribute("img_code_forget").toString())) {
            throw new BaseException("验证码错误");
        }

        //将数据更新
        FrameUser userInfo = list.get(0);
        userInfo.setPassword(user.getPassword());
        frameUserDao.updateByRowGuidSelective(userInfo);

    }

    @Override
    public void cleanCacheUser() {
        redisService.removePattern(CommUtil.getRedisKey(userDataRedisName + "*"));
        logger.info("用户缓存信息已清除！");
    }

    @Override
    public XnwUser getUserInfoByLogin(String loginId) {
        return frameUserDao.getUserInfoByLogin(loginId);
    }
}
