package com.xnw.frame.service;


import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.pojo.FrameUserPasswordVo;
import com.xnw.frame.entity.pojo.FrameUserPojo;
import com.xnw.frame.entity.pojo.XnwUser;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface FrameUserService extends BaseService<FrameUser> {

	/**
	 * 查询用户信息列表带部门信息 (联查部门表)
	 * @param query
	 * @return
	 */
	List<FrameUserPojo> getList(Query query);

	/**
	 * 查询guid用户详情信息 包括头像,部门等信息 (联查部门表 附件表)
	 * @param userGuid
	 * @return
	 */
	FrameUserPojo selectDetailByUserGuid(String userGuid);

	/**
	 * 查询guid用户详情信息 包括头像,部门等信息 (联查部门表 附件表) 注意会返回加密过后的密码
	 * @param loginId
	 * @return
	 */
	FrameUserPojo selectDetailByLoginId(String loginId);

	/**
	 * 查询guid用户 关联角色信息
	 * @param userGuid
	 * @return
	 */
	List<FrameRole> selectRoleListByUserGuid(String userGuid);
	
	/**
	 * 新增框架用户(设置密码 设置权限)
	 * @param frameUserPojo
	 * @return str
	 */
	String addFrameUser(FrameUserPojo frameUserPojo);

	/**
	 * 更新框架用户
	 * @param frameUserPojo
	 */
	void updateFrameUser(FrameUserPojo frameUserPojo);

	/**
	 * 获取配置项设置的初始密码(md5 加密过后 再aes 加密)
	 * @return str
	 */
	String getInitPassword();

	/**
	 * 修改密码
	 * @param frameUserPasswordVo
	 */
	void changePassword(FrameUserPasswordVo frameUserPasswordVo);

	/**
	 * 根据权限返回当前部门下的所有人员
	 * @return
	 */
	List<FrameUser> getUserByCurrentAuthority();

	/**
	 * 获取当前用户的部门的上级部门的所有人员数据
	 * @return
	 */
	List<FrameUser> getParentDeptUser();

	/**
	 * 用户注册
	 * @param user
	 * @param session
	 * @return
	 */
    void registerAppUser(FrameUserPojo user, HttpSession session);

	/**
	 * 校验验证码
	 * @param isForget
	 * @param session
	 * @return
	 */
    void checkIsSetImgCode(String isForget, HttpSession session);

	/**
	 * 忘记密码
	 * @param user
	 * @param session
	 * @return
	 */
	void forgetPassword(FrameUserPojo user, HttpSession session);

	/**
	 * 清空所有缓存用户信息
	 */
	void cleanCacheUser();

	/**
	 * 获取用户登录信息
	 *
	 * @param loginId
	 * @return
	 */
	XnwUser getUserInfoByLogin(String loginId);

}
