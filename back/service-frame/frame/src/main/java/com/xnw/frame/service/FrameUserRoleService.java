package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.FrameUserRole;

import java.util.List;

public interface FrameUserRoleService extends BaseService<FrameUserRole> {

    /**
     * 通过用户guid 获取角色名集合
     *
     * @param userGuid
     * @return
     */
    List<String> selectRoleNameListByUserGuid(String userGuid);

    /**
     * 根据角色Guid查询用户列表
     *
     * @param roleGuid
     * @return
     */
    List<FrameUser> getUserListByRoleName(String roleGuid);
}
