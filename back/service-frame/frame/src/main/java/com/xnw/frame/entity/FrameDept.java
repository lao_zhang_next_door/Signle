package com.xnw.frame.entity;

import com.xnw.frame.basic.base.BaseEntity;

/**
 * @author jtr
 * @creatTime 2021-04-20-10:49
 **/
public class FrameDept extends BaseEntity {
	
	/**
	 * @param init true则有默认值
	 */
    public FrameDept(boolean init) {
		super(init);
	}

	public FrameDept() {}

	public FrameDept(String rowGuid){
    	super(rowGuid);
	}
	
	/**
	 * 部门名称
	 */
	private String deptName;
	
	/**
	 * 部门简称
	 */
	private String shortName;
	
	/**
	 * 部门编码
	 */
	private String oucode;
	
	/**
	 * 部门电话
	 */
	private String tel;
	
	/**
	 * 传真
	 */
	private String fax;
	
	/**
	 * 经度
	 */
	private String longitude;

	/**
	 * 纬度
	 */
	private String latitude;

	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 部门简介
	 */
	private String description;
	
	/**
	 * 父级guid
	 */
	private String parentGuid;

	/**
	 * 部门级别
	 */
	private String deptCode;

	/**
	 * 部门级别
	 */
	private String deptLevel;

	/**
	 * 部门图片附件
	 */
	private String photoFileGuid;

	public String getDeptName() {
		return deptName;
	}

	public FrameDept setDeptName(String deptName) {
		this.deptName = deptName;
		return this;
	}

	public String getShortName() {
		return shortName;
	}

	public FrameDept setShortName(String shortName) {
		this.shortName = shortName;
		return this;
	}

	public String getOucode() {
		return oucode;
	}

	public FrameDept setOucode(String oucode) {
		this.oucode = oucode;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public FrameDept setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public FrameDept setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public FrameDept setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public FrameDept setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getParentGuid() {
		return parentGuid;
	}

	public FrameDept setParentGuid(String parentGuid) {
		this.parentGuid = parentGuid;
		return this;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public FrameDept setDeptCode(String deptCode) {
		this.deptCode = deptCode;
		return this;
	}

	public String getDeptLevel() {
		return deptLevel;
	}

	public FrameDept setDeptLevel(String deptLevel) {
		this.deptLevel = deptLevel;
		return this;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getPhotoFileGuid() {
		return photoFileGuid;
	}

	public void setPhotoFileGuid(String photoFileGuid) {
		this.photoFileGuid = photoFileGuid;
	}
}
