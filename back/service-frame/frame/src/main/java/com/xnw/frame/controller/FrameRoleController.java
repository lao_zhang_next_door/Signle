package com.xnw.frame.controller;

import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.FrameRole;
import com.xnw.frame.service.FrameRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 角色控制器
 * @author hero
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameRole")
public class FrameRoleController extends BaseController{

	@Autowired
	private FrameRoleService frameRoleService;

	@GetMapping("/listData")
	private R listData(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<FrameRole> roleList = frameRoleService.selectByLimit(query);
		int count = frameRoleService.selectCountByLimit(query);
		PageUtils pageUtil = new PageUtils(roleList, count, query.getLimit(), query.getPage());
		return R.list(pageUtil.getTotalCount(), pageUtil.getList());
	}
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		FrameRole frameRole = new FrameRole(); 
		frameRole.setRowGuid(rowGuid);
		List<FrameRole> roleList = frameRoleService.select(frameRole);
		return R.ok().put("data", roleList);
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameRole frameRole){
		frameRole.initNull();
		frameRoleService.insert(frameRole);
		return R.ok();
	}
	
	@PutMapping("/update")
	private R update(@RequestBody FrameRole frameRole){
		frameRoleService.updateByRowGuidSelective(frameRole);
		return R.ok();
	}
	
	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		frameRoleService.deleteBatch(rowGuids);
		return R.ok();
	}
	
	@GetMapping("/getAllRole")
	private R getAllRole(){
		return R.ok().put("data", frameRoleService.selectAll());
	}

	/**
	 * 根据角色guid 查询该角色所拥有的菜单模块
	 *
	 * @return
	 */
	@PostMapping("/getModuleByRoleGuid")
	private R getModuleByRoleGuid(@RequestParam String roleGuid) {
		List<FrameModule> moduleList = frameRoleService.getModuleByRoleGuid(roleGuid);
		return R.ok().put("data", moduleList);
	}

	/**
	 * 根据角色名获取Guids
	 *
	 * @return list
	 */
	@PostMapping("/getRoleGuidsByNames")
	private R getRoleGuidsByNames(@RequestParam String roleNames) {
		List<String> roleGuids = frameRoleService.getRoleGuidsByNames(roleNames);
		return R.ok().put("data", roleGuids);
	}
}
 