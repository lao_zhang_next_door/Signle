package com.xnw.frame.basic.network;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class HttpCookie extends HashMap<String, String> {

    public void addCookie(String cookie){
        String ck = cookie.split(";")[0];
        String[] splits = ck.split("=");
        put(splits[0], splits[1]);
    }

    public String getValues(){
        List<String> cookies = new LinkedList<>();
        for(String key: keySet()){
            cookies.add(key+ "="+ get(key));
        }
        return String.join("; ", cookies);
    }
}
