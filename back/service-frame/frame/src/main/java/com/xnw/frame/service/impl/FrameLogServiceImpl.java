package com.xnw.frame.service.impl;


import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameLogDao;
import com.xnw.frame.entity.CommonLog;
import com.xnw.frame.service.FrameLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FrameLogServiceImpl extends BaseServiceImpl<CommonLog> implements FrameLogService  {

	public FrameLogServiceImpl(FrameLogDao dao) { super(dao); }

	@Autowired
	private FrameLogDao frameLogDao;

}

