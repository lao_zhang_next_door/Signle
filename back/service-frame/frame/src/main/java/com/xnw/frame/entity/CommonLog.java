package com.xnw.frame.entity;


import com.xnw.frame.basic.base.BaseEntity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 日志对象
 *
 * @author xuzhanfu
 */
@Table(name = "frame_log")
public class CommonLog extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public CommonLog(){}

    public CommonLog(String rowGuid){
        super(rowGuid);
    }

    public CommonLog(boolean init) {
        super(init);
    }

    /**
     * 日志类型
     */
    private String type;
    /**
     * 跟踪ID
     */
    private String traceId;
    /**
     * 日志标题
     */
    private String title;
    /**
     * 操作内容
     */
    private String operation;
    /**
     * 执行方法
     */
    private String method;

    /**
     * 请求路径
     */
    private String url;
    /**
     * 参数
     */
    private String params;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 耗时
     */
    private Long executeTime;
    /**
     * 地区
     */
    private String location;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 异常信息
     */
    private String exception;

    public String getType() {
        return type;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getTitle() {
        return title;
    }

    public String getOperation() {
        return operation;
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getParams() {
        return params;
    }

    public String getIp() {
        return ip;
    }

    public Long getExecuteTime() {
        return executeTime;
    }

    public String getLocation() {
        return location;
    }

    public String getCreateBy() {
        return createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public String getException() {
        return exception;
    }

    public CommonLog setType(String type) {
        this.type = type;
        return this;
    }

    public CommonLog setTraceId(String traceId) {
        this.traceId = traceId;
        return this;
    }

    public CommonLog setTitle(String title) {
        this.title = title;
        return this;
    }

    public CommonLog setOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public CommonLog setMethod(String method) {
        this.method = method;
        return this;
    }

    public CommonLog setUrl(String url) {
        this.url = url;
        return this;
    }

    public CommonLog setParams(String params) {
        this.params = params;
        return this;
    }

    public CommonLog setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public CommonLog setExecuteTime(Long executeTime) {
        this.executeTime = executeTime;
        return this;
    }

    public CommonLog setLocation(String location) {
        this.location = location;
        return this;
    }

    public CommonLog setCreateBy(String createBy) {
        this.createBy = createBy;
        return this;
    }

    public CommonLog setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public CommonLog setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public CommonLog setException(String exception) {
        this.exception = exception;
        return this;
    }
}
