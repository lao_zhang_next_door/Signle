package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.FrameDept;
import com.xnw.frame.entity.pojo.FrameDeptPojo;

import java.util.List;

public interface FrameDeptDao extends BaseDao<FrameDept> {

	/**
	 * 根据rowGuid 联查(left join frameDept) 查询上级deptName
	 * @param rowGuid
	 * @return
	 */
	List<FrameDeptPojo> selectParentCode(String rowGuid);

	/**
	 * 获取所有有经纬度的部门
	 * @return
	 */
	List<FrameDept> getDeptLocation();
	
}
