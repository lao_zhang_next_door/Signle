package com.xnw.frame.service;

import com.xnw.frame.basic.base.service.BaseService;
import com.xnw.frame.entity.FrameAttach;

import java.util.List;

/**
 * @author wzl
 */
public interface FrameAttachService extends BaseService<FrameAttach> {

    /**
     * 删除附件（数据+本地文件）
     * @param formRowGuid
     */
    void deleteFileByFormRowGuid(String formRowGuid);

    /**
     * 删除附件
     *
     * @param rowGuids
     */
    void deleteFileByRowGuid(List<String> rowGuids);

    /**
     * 根据formRowGuid查找一个contentUrl
     *
     * @param formRowGuid
     * @param index       序号
     * @param isLatest    是否为最新的一条
     * @return
     */
    String getContentUrlByFormRowGuid(String formRowGuid, Integer index, boolean isLatest);
}
