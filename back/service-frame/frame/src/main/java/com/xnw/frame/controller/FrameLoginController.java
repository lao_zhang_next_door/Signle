package com.xnw.frame.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xnw.captcha.model.common.RepCodeEnum;
import com.xnw.captcha.model.common.ResponseModel;
import com.xnw.captcha.service.CaptchaService;
import com.xnw.frame.auth.token.JwtHelper;
import com.xnw.frame.auth.token.PassToken;
import com.xnw.frame.auth.token.UserLoginToken;
import com.xnw.frame.basic.base.Base;
import com.xnw.frame.basic.network.HttpContextUtils;
import com.xnw.frame.basic.utils.frame.CommUtil;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.basic.utils.request.IpUtils;
import com.xnw.frame.entity.FrameAttach;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.pojo.UserLoginPojo;
import com.xnw.frame.entity.pojo.XnwUser;
import com.xnw.frame.enumeration.DelFlag;
import com.xnw.frame.enumeration.IsOrNot;
import com.xnw.frame.enumeration.TrueOrFalseEnum;
import com.xnw.frame.service.FrameAttachService;
import com.xnw.frame.service.FrameConfigService;
import com.xnw.frame.service.FrameRoleService;
import com.xnw.frame.service.FrameUserService;
import com.xnw.frame.systemLog.StaticValue;
import com.xnw.frame.systemLog.SystemLogAop;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.xnw.frame.systemLog.StaticValue.LOGIN_ID_NAME;
import static com.xnw.frame.systemLog.StaticValue.LOGIN_IP_ADDRESS;

/**
 * 登录控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameLogin")
public class FrameLoginController extends Base {

    @Value("${token.expiration.time}")
    private long tokenExpirationTime;

    @Autowired
    private FrameUserService userService;

    @Autowired
    private FrameRoleService frameRoleService;

    @Autowired
    private FrameAttachService frameAttachService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private FrameConfigService frameConfigService;

    /**
     * 根据token获取用户信息
     *
     * @return
     */
    @GetMapping("/getUserInfo")
    private R getUserInfo(HttpServletRequest request) {
        return R.ok().put("data", getCurrentUser());
    }

    /**
     * 登录
     **/
    @SystemLogAop(value = "用户登录", action = "登录")
    @PassToken
    @Operation(summary = "登录接口")
    @ResponseBody
    @RequestMapping(value = "/login", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R login(@RequestBody UserLoginPojo user, HttpServletRequest request, HttpSession session) throws Exception {
        String sessionId = HttpContextUtils.getFrameSidValue(request, session);
        //二次验证图形验证码
        String flag = frameConfigService.getConfigsVal("是否开启图形验证");
        if (flag.equals(TrueOrFalseEnum.TRUE.getValue())) {
            ResponseModel responseModel = captchaService.verification(user.getCaptchaVO());
            if (responseModel.getRepCode().equals(RepCodeEnum.SUCCESS.getCode())) {
                return R.error("图形验证码验证失败");
            }
        }
        XnwUser xnwUser = userService.getUserInfoByLogin(user.getLoginId());
        //若redis中存在超过次数
        if (!checkUserLogin(IpUtils.getIpAddr(request), user.getLoginId())) {
            //更新限制字段
            FrameUser frameUser = new FrameUser(xnwUser.getRowGuid());
            frameUser.setIsLimit(IsOrNot.IS.getCode());
            userService.updateByPrimaryKey(frameUser);
            return R.error("您现在尝试登录次数过多，请稍后再进行登录。");
        }
        if (xnwUser == null) {
            return R.error("登录失败,用户不存在");
        } else {
            if (!xnwUser.getPassword().equals(user.getPassword())) {
                return R.error("登录失败,密码错误").put("data", user.getLoginId());
            } else {
                if (xnwUser.getIsLimit() == IsOrNot.IS.getCode()) {
                    return R.error("用户已经禁用");
                }
                if (xnwUser.getDelFlag() == DelFlag.Del.getCode()) {
                    return R.error("用户不存在");
                }
                //这里为24小时
                String token = JwtHelper.createJWT(xnwUser.getRowGuid(), xnwUser.getLoginId(), tokenExpirationTime * 4);
                List<Map<String, Object>> mapList = frameRoleService.getRolesByUserGuid(xnwUser.getRowGuid());
                List<String> roleNameList = new ArrayList<>();
                List<String> roleGuidList = new ArrayList<>();
                for (Map<String, Object> map : mapList) {
                    roleNameList.add(map.get("roleName").toString());
                    roleGuidList.add(map.get("roleGuid").toString());
                }
                //更新登录时间
                FrameUser frameUser = new FrameUser(xnwUser.getRowGuid());
                frameUser.setLastLoginTime(LocalDateTime.now());
                userService.updateByRowGuid(frameUser);
                HttpContextUtils.setSessionUserGuid(userDataRedisName + sessionId, xnwUser.getRowGuid());
                //用户头像
                if (xnwUser.getHeadimgGuid() != null) {
                    FrameAttach frameAttach = new FrameAttach();
                    frameAttach.setFormRowGuid(xnwUser.getHeadimgGuid());
                    List<FrameAttach> heads = frameAttachService.select(frameAttach);
                    if (!heads.isEmpty()) {
                        xnwUser.setHeadimgGuidVal(heads.get(heads.size() - 1).getContentUrl());
                    }
                }
                xnwUser.setRoleGuidList(roleGuidList);
                xnwUser.setRoleNameList(roleNameList);
                xnwUser.setToken(token);
                xnwUser.setSessionId(sessionId);
                String jsonUserInfo = JSON.toJSONString(xnwUser);
                redisService.set(CommUtil.getRedisKey(userDataRedisName + xnwUser.getRowGuid()), xnwUser.getLoginId(), (long) (1000 * 60 * 60 * 6));
                redisService.set(CommUtil.getRedisKey(userDataRedisName + xnwUser.getRowGuid() + "_session"), jsonUserInfo);
                return R.ok().put("data", xnwUser);
            }
        }
    }

    @UserLoginToken
    @GetMapping("/getMessage")
    public String getMessage() {
        return "你已通过验证";
    }

    /**
     * 验证用户重复登录失败
     *
     * @param loginIp
     * @return boolean
     */
    private boolean checkUserLogin(String loginIp, String loginId) {
        try {
            boolean loginIpFlag = redisService.exists(CommUtil.getRedisKey(LOGIN_IP_ADDRESS + loginIp));
            boolean loginIdFlag = redisService.exists(CommUtil.getRedisKey(LOGIN_ID_NAME + loginId));
            //如果都不存在ip:key,Id:key
            if (!loginIpFlag && !loginIdFlag) {
                return true;
            } else {
//				int loginLimit = Integer.parseInt(frameConfigService.getConfigByName("loginFailedFrequency").getConfigValue());
                int loginLimit = 5;
                JSONObject jsonObject = JSONObject.parseObject(redisService.get(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId)).toString());
                int loginIpLimit = Integer.parseInt(jsonObject.getString("loginIpTimes"));
                int loginIdLimit = Integer.parseInt(jsonObject.getString("loginIdTimes"));
                //用户IP与ID需要同时小于错误上限
                return loginIpLimit < loginLimit && loginIdLimit < loginLimit;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


}
