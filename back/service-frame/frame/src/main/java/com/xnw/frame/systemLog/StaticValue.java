package com.xnw.frame.systemLog;

/**
 * @author wzl
 * redis静态文件路径参数
 */
public class StaticValue {

    /**
     * 用户Ip重复路径
     */
    public final static String LOGIN_IP_ADDRESS = "LoginIpAddress:Fail:";

    /**
     * 用户LoginId重复路径
     */
    public final static String LOGIN_ID_NAME = "LoginIdName:Fail:";
}
