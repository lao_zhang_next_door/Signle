package com.xnw.frame.entity.pojo;

import com.xnw.captcha.model.vo.CaptchaVO;

public class UserLoginPojo {

    /**
     * 登录名
     */
    private String loginId;

    /**
     * 密码
     */
    private String password;

    /**
     * 图形验证码信息
     */
    private CaptchaVO captchaVO;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CaptchaVO getCaptchaVO() {
        return captchaVO;
    }

    public void setCaptchaVO(CaptchaVO captchaVO) {
        this.captchaVO = captchaVO;
    }
}
