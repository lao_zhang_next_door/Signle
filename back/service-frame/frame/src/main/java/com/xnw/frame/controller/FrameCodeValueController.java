package com.xnw.frame.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.FrameCodeValue;
import com.xnw.frame.entity.pojo.FrameCodeValuePojo;
import com.xnw.frame.service.FrameCodeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 代码项控制器
 *
 * @author hero
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameCodeValue")
public class FrameCodeValueController extends BaseController {

    @Autowired
    private FrameCodeValueService frameCodeValueService;

    @GetMapping("/listData")
    private R listData(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<FrameCodeValue> roleList = frameCodeValueService.selectByLimit(query);
        int count = frameCodeValueService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(roleList, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    @GetMapping("/getDetailByGuid")
    private R getDetailByGuid(@RequestParam String rowGuid) {
        FrameCodeValue frameCodeValue = new FrameCodeValue();
        frameCodeValue.setRowGuid(rowGuid);
        List<FrameCodeValue> CodeValueList = frameCodeValueService.select(frameCodeValue);
        return R.ok().put("data", CodeValueList);
    }

    @PostMapping("/add")
    private R add(@RequestBody FrameCodeValue frameCodeValue) {
        frameCodeValue.initNull();
        int code = frameCodeValueService.insert(frameCodeValue);
        if (code > 0) {
            frameCodeValueService.cacheCodesMapByValue(frameCodeValue.getRowGuid());
        }
        return R.ok();
    }

    @PutMapping("/update")
    private R update(@RequestBody FrameCodeValue frameCodeValue) {
        int code = frameCodeValueService.updateByRowGuidSelective(frameCodeValue);
        if (code > 0) {
            frameCodeValueService.cacheCodesMapByValue(frameCodeValue.getRowGuid());
        }
        return R.ok();
    }

    @DeleteMapping("/delete")
    private R deleteBatch(@RequestBody String[] rowGuids, @RequestParam("codeName") String codeName) {
        frameCodeValueService.deleteBatch(rowGuids);
        frameCodeValueService.cacheCodesMapByName(codeName);
        return R.ok();
    }

    /**
     * 获取树数据
     *
     * @param frameCodeValue
     * @return
     */
    @PostMapping("/treeData")
    private R treeData(@RequestBody FrameCodeValue frameCodeValue) {
        List<FrameCodeValue> codeValueList = frameCodeValueService.select(frameCodeValue);
        JSONArray array = JSON.parseArray(JSON.toJSONString(codeValueList));
        JSONObject object = null;
        FrameCodeValue codev = null;
        for (int i = 0; i < array.size(); i++) {
            object = array.getJSONObject(i);
            codev = new FrameCodeValue();
            codev.setParentGuid(object.getString("rowGuid"));
            int count = frameCodeValueService.selectCount(codev);
            if (count > 0) {
                object.put("children", new ArrayList());
            }
        }

        return R.ok().put("data", array);
    }

    /**
     * 根据rowGuid 联查(left join frameCodeValue)
     *
     * @param rowGuid
     * @return
     */
    @GetMapping("/selectParentCode")
    private R selectParentCode(@RequestParam String rowGuid) {
        List<FrameCodeValuePojo> CodeValueList = frameCodeValueService.selectParentCode(rowGuid);
        return R.ok().put("data", CodeValueList);
    }
}
 