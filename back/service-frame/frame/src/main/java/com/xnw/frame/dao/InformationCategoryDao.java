package com.xnw.frame.dao;

import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.entity.InformationCategory;

public interface InformationCategoryDao extends BaseDao<InformationCategory>{
	
}

