package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameCodeDao;
import com.xnw.frame.entity.FrameCode;
import com.xnw.frame.service.FrameCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FrameCodeServiceImpl extends BaseServiceImpl<FrameCode> implements FrameCodeService {

	public FrameCodeServiceImpl(FrameCodeDao dao) {
		super(dao);
	}

	@Autowired
	private FrameCodeDao frameCodeDao;

}
