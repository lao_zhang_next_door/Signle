package com.xnw.frame.controller;

import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import com.xnw.frame.entity.FrameUserRole;
import com.xnw.frame.service.FrameUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 权限控制器
 * @author hero
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/frame/FrameUserRole")
public class FrameUserRoleController extends BaseController{

	@Autowired
	private FrameUserRoleService frameUserRoleService;
	
	@GetMapping("/listData")
	private R listData(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
        List<FrameUserRole> UserRoleList = frameUserRoleService.selectByLimit(query);
        int count = frameUserRoleService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(UserRoleList, count, query.getLimit(), query.getPage());
		return R.list(pageUtil.getTotalCount(), pageUtil.getList());
	}
	
	@GetMapping("/getDetailByGuid")
	private R getDetailByGuid(@RequestParam String rowGuid){
		FrameUserRole frameUserRole = new FrameUserRole(); 
		frameUserRole.setRowGuid(rowGuid);
		List<FrameUserRole> UserRoleList = frameUserRoleService.select(frameUserRole);
		return R.ok().put("data", UserRoleList);
	}
	
	@PostMapping("/add")
	private R add(@RequestBody FrameUserRole frameUserRole){
		frameUserRole.initNull();
		frameUserRoleService.insert(frameUserRole);
		return R.ok();
	}
	
	@PutMapping("/update")
	private R update(@RequestBody FrameUserRole frameUserRole){
		frameUserRoleService.updateByRowGuidSelective(frameUserRole);
		return R.ok();
	}
	
	@DeleteMapping("/delete")
	private R deleteBatch(@RequestBody String[] rowGuids){
		frameUserRoleService.deleteBatch(rowGuids);
		return R.ok();
	}

//	@PostMapping("/getRoleArrByToken")
//	private R getRoleArrByToken(@RequestParam String token){
//
//		Claims claims = SecurityUtil.getClaims(token);
//
//		return R.ok();
//	}
}
 