package com.xnw.frame.basic.config;

import com.xnw.frame.auth.AuthListener;
import com.xnw.frame.auth.token.PassToken;
import com.xnw.frame.basic.network.HttpContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;

/**
 * 拦截器去获取token并验证token
 * <p>Title: AuthenticationInterceptor</p>
 * <p>Description: </p>
 *
 * @author hero
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

    /**
     * header token 拦截器
     *
     * @ClassName: HeaderOptionsInterceptor
     * @Description: header token 拦截器
     * @author lhq
     * @date 2018年11月6日 下午2:30:46
     */
    @Autowired
    private AuthListener authListener;
    @Autowired
    private HttpSession session;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handle) throws Exception {
        //创建cookie
        String sessionId = HttpContextUtils.getFrameSidValue(request, response, session);
        HandlerMethod handlerMethod = (HandlerMethod) handle;
        Method method = handlerMethod.getMethod();
        //检查是否有PassToken注释，有则跳过认证
        if (method.isAnnotationPresent(PassToken.class)) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if (passToken.required()) {
                return true;
            }
        }
        return authListener.preHandle(request, response);
    }


    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
    }

    public static long getDatePoor(java.util.Date expireTime, java.util.Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = expireTime.getTime() - nowDate.getTime();
        // 计算差多少分钟
        long min = diff / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return min;
    }
}