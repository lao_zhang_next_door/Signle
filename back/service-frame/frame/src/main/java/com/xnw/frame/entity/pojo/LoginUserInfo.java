package com.xnw.frame.entity.pojo;

public class LoginUserInfo {
    private String userName = "";
    private String loginId = "";
    private String mobile = "";
    private String sex = "";
    private String userRowGuid = "";
    private String deptGuid = "";
    private String deptName = "";
    private String token = "";
    private Integer isManage;
    private String mainIndex = "";
    private String templateGuid = "";
    private String sessionId = "";
    private String roleNames = "";
    private String roleGuids = "";

    public Integer getIsManage() {
        return isManage;
    }

    public void setIsManage(Integer isManage) {
        this.isManage = isManage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUserRowGuid() {
        return userRowGuid;
    }

    public void setUserRowGuid(String userRowGuid) {
        this.userRowGuid = userRowGuid;
    }

    public String getDeptGuid() {
        return deptGuid;
    }

    public void setDeptGuid(String deptGuid) {
        this.deptGuid = deptGuid;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMainIndex() {
        return mainIndex;
    }

    public void setMainIndex(String mainIndex) {
        this.mainIndex = mainIndex;
    }

    public String getTemplateGuid() {
        return templateGuid;
    }

    public void setTemplateGuid(String templateGuid) {
        this.templateGuid = templateGuid;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }

    public String getRoleGuids() {
        return roleGuids;
    }

    public void setRoleGuids(String roleGuids) {
        this.roleGuids = roleGuids;
    }
}
