package com.xnw.frame.rabbitmq.TopicExchange.shortMessage;

import com.rabbitmq.client.Channel;
import com.xnw.frame.basic.utils.frame.SubModuleUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageReceive {

	@Autowired
	SubModuleUtils subModuleUtils;
	
	@RabbitListener(queues = "#{messageQueue.name}")
    public void send(Message message, Channel channel) {
		subModuleUtils.sendShortMessage(message, channel);
    }

}
