package com.xnw.frame.basic.utils.dataformat;

import java.time.format.DateTimeFormatter;

/**
 * 日期解析
 * @author hero
 */
public class Formatter {
	public static final DateTimeFormatter Y_M_D = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter YMDHMS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	public static final DateTimeFormatter YMDHMS_FNAME = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	public static final DateTimeFormatter YMD = DateTimeFormatter.ofPattern("yyyyMMdd");
	public static final DateTimeFormatter YMD_X = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter M_D = DateTimeFormatter.ofPattern("MM-dd");
	public static final DateTimeFormatter MD = DateTimeFormatter.ofPattern("MMdd");
	public static final DateTimeFormatter MDX = DateTimeFormatter.ofPattern("MM/dd");
}
