package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.basic.utils.exception.BaseException;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.dao.InformationCategoryDao;
import com.xnw.frame.dao.InformationInfoDao;
import com.xnw.frame.entity.InformationCategory;
import com.xnw.frame.entity.InformationInfo;
import com.xnw.frame.enumeration.InformationStatus;
import com.xnw.frame.service.InformationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class InformationInfoServiceImpl extends BaseServiceImpl<InformationInfo> implements InformationInfoService  {

	public InformationInfoServiceImpl(InformationInfoDao dao) { super(dao); }

	@Autowired
	private InformationInfoDao informationInfoDao;

	@Autowired
	private InformationCategoryDao informationCategoryDao;

	@Override
	public void deliveryInfo(InformationInfo informationInfo) {
		if(isFieldBlank(informationInfo,"infoCategoryGuid")){
			throw new BaseException("infoCategoryGuid(栏目guid) 值为null");
		}

		InformationCategory category = informationCategoryDao.
				selectOne(new InformationCategory(informationInfo.getInfoCategoryGuid()));
		/**
		 * 需要审核
		 */
		if(category.getIsNeedAudit() != null && category.getIsNeedAudit()){
			informationInfo.setInfoStatus(InformationStatus.DSH.getValue());
		}else{
			/**
			 * 不需要
			 */
			informationInfo.setInfoStatus(InformationStatus.YFB.getValue());
		}
		informationInfo.initNull();
		informationInfoDao.insert(informationInfo);
	}

	@Override
	public PageUtils listDataLfCategory(Map<String, Object> params) {
//		Map<String,Object> userMap = getCurrentUser();
//		if(permissionService.hasPerm("管理员")){
//
//		}else{
//			//除管理员以外其余人查看自己相关的 信息
//			params.put("infoCreateUserGuid",userMap.get("userGuid"));
//		}

		Query query = new Query(params);
		List<InformationInfo> informationInfoList = informationInfoDao.selectByLimit(query);
		Integer count = informationInfoDao.selectCountByLimit(query);
		return new PageUtils(informationInfoList, count, query.getLimit(), query.getPage());
	}
}

