package com.xnw.frame.systemLog;

import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.auth.token.JwtHelper;
import com.xnw.frame.basic.utils.aop.AopStaticFunction;
import com.xnw.frame.basic.utils.frame.CommUtil;
import com.xnw.frame.basic.utils.frame.SubModuleUtils;
import com.xnw.frame.basic.utils.request.IpUtils;
import com.xnw.frame.redis.RedisService;
import io.jsonwebtoken.Claims;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统日志切面
 *
 * @author wzl
 */
@Aspect
@Component
public class LogAspect {

    private final static String ERROR_CODE = "500";

    private final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Value("${token.head.name}")
    private String authName;

    @Autowired
    private RedisService redisService;

    @Autowired
    private LogThread logThread;

    @Autowired
    private SubModuleUtils subModuleUtils;

    @Pointcut("@annotation(com.xnw.frame.systemLog.SystemLogAop)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object result = null;
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader(authName);
        final Claims claims = JwtHelper.parseJWT(token);
        String userName = "";
        if (claims != null) {
            String uuid = claims.get("uuid").toString();
            userName = subModuleUtils.getUserName(uuid);
        }
        long beginTime = System.currentTimeMillis();
        try {
            // 执行方法
            result = point.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        // 执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;
        // 保存日志
        logThread.saveLog(point, time, userName, request);
        return result;
    }

    @AfterReturning(returning = "result", pointcut = "pointcut()")
    public Object userLoginPoint(JoinPoint joinPoint, Object result) throws Exception {
        if (result != null) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String loginIp = IpUtils.getIpAddr(request);
            //logger.info(result.toString());
            Object code = AopStaticFunction.objectToMap(result).get("code");
            Object msg = AopStaticFunction.objectToMap(result).get("msg");
            Object data = AopStaticFunction.objectToMap(result).get("data");
            if (ERROR_CODE.equals(code.toString())) {
                switch (msg.toString()) {
                    case "登录失败,用户不存在":
                    case "用户已经禁用":
                    case "登录失败,密码错误":
                    case "用户不存在":
                        userLoginFail(loginIp, data.toString());
                        logger.info("IP:" + loginIp + "多次登录失败");
                        break;
                    default:
                        break;
                }
            }
        }
        return result;
    }

    private synchronized void userLoginFail(String loginIp, String loginId) {
        Object object = redisService.get(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId));
        initIpAndId(loginIp, loginId, object);
    }

    private void initIpAndId(String loginIp, String loginId, Object dataExist) {
        if (dataExist != null) {
            int loginLimit = Integer.parseInt(subModuleUtils.getConfigVal("登录失败次数"));
            //多次
            JSONObject jsonObject = JSONObject.parseObject(redisService.get(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId)).toString());
            int loginIpTimes = Integer.parseInt(jsonObject.get("loginIpTimes").toString());
            int loginIdTimes = Integer.parseInt(jsonObject.get("loginIdTimes").toString());
            //若小于限制次数
            long timeLimit = Integer.parseInt(subModuleUtils.getConfigVal("登录失败冷却时间"));
            if (loginIpTimes < loginLimit) {
                jsonObject.put("loginIpTimes", loginIpTimes + 1);
                redisService.set(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId), jsonObject.toJSONString(), timeLimit);
            }
            if (loginIdTimes < loginLimit) {
                jsonObject.put("loginIdTimes", loginIdTimes + 1);
                redisService.set(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId), jsonObject.toJSONString(), timeLimit);
            }
        } else {
            //设置默认
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("loginIp", loginIp);
            jsonObject.put("loginId", loginId);
            jsonObject.put("loginIpTimes", "1");
            jsonObject.put("loginIdTimes", "1");
            redisService.set(CommUtil.getRedisKey(StaticValue.LOGIN_ID_NAME + loginId), jsonObject.toJSONString(), 300L);
        }
    }

}