package com.xnw.frame.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameModuleDao;
import com.xnw.frame.dao.FrameRoleModuleDao;
import com.xnw.frame.entity.FrameModule;
import com.xnw.frame.entity.pojo.FrameModulePojo;
import com.xnw.frame.entity.pojo.FrameRoleModulePojo;
import com.xnw.frame.enumeration.DelFlag;
import com.xnw.frame.service.FrameModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FrameModuleServiceImpl extends BaseServiceImpl<FrameModule> implements FrameModuleService {

    public FrameModuleServiceImpl(FrameModuleDao dao) {
        super(dao);
    }

    @Autowired
    private FrameModuleDao frameModuleDao;

    @Autowired
    private FrameRoleModuleDao roleModuleDao;

    @Override
    public List<FrameModulePojo> selectParentCode(String rowGuid) {
        return frameModuleDao.selectParentCode(rowGuid);
    }

    @Override
    public JSONObject allTreeData() {

        /**
         * 获取所有根目录数据
         */
        FrameModule module = new FrameModule();
        module.setParentGuid("");
        List<FrameModule> pList = frameModuleDao.select(module);
        module = null;
        List<String> strList = new ArrayList();
        JSONArray arr = getChildModules(pList, strList);

        JSONObject obj = new JSONObject();
        obj.put("array", arr);
        obj.put("list", strList);
        return obj;
    }

    public JSONArray getChildModules(List<FrameModule> deptTopTrees, List<String> strList) {
        JSONArray array = new JSONArray();
        for (FrameModule frameModule : deptTopTrees) {
            strList.add(frameModule.getRowGuid());

            JSONObject json = new JSONObject();
            json.put("moduleName", frameModule.getModuleName());
            json.put("rowGuid", frameModule.getRowGuid());
            //获取子模块
            FrameModule module = new FrameModule();
            module.setParentGuid(frameModule.getRowGuid());
            List<FrameModule> childDept = frameModuleDao.select(module);
            json.put("children", getChildModules(childDept, strList));
            array.add(json);
        }
        return array;
    }

    @Override
    public JSONArray allTreeDataRoute() {
        /**
         * 获取所有根目录数据
         */
        FrameModule module = new FrameModule();
        module.setParentGuid("");
        List<FrameModule> pList = frameModuleDao.select(module);
        module = null;
        return getChildModulesRoute(pList);
    }

    public JSONArray getChildModulesRoute(List<FrameModule> deptTopTrees) {
        JSONArray array = new JSONArray();
        for (FrameModule frameModule : deptTopTrees) {
            JSONObject json = new JSONObject();
            json.put("path", frameModule.getPath() == null ? "" : frameModule.getPath());
            json.put("name", frameModule.getModuleName());
            json.put("component", frameModule.getModuleAddress());
            json.put("isVisible", frameModule.getIsVisible());
            json.put("redirect", frameModule.getRedirect());
            json.put("icon", frameModule.getSmallIcon());
            JSONObject metaObj = new JSONObject();

            //根据moduleGuid获取对应角色
            List<FrameRoleModulePojo> moduleRight = roleModuleDao.selectLJRole(frameModule.getRowGuid());

            JSONArray arr = new JSONArray();
            if (moduleRight != null && moduleRight.size() != 0) {
                for (FrameRoleModulePojo roleModulePojo : moduleRight) {
                    arr.add(roleModulePojo.getRoleName());
                }
            }
            metaObj.put("roles", arr);
            metaObj.put("keepAlive", frameModule.getKeepAlive());
            metaObj.put("menuKey", frameModule.getRowId());
            metaObj.put("componentName", frameModule.getComponentName());
            json.put("meta", metaObj);

            //获取子模块
            FrameModule module = new FrameModule();
            module.setParentGuid(frameModule.getRowGuid());
            List<FrameModule> childDept = frameModuleDao.select(module);
            json.put("children", getChildModulesRoute(childDept));
            array.add(json);
        }
        return array;
    }

    @Override
    public JSONArray getVue3Route() {
        /**
         * 获取所有根目录数据
         */
        FrameModule module = new FrameModule();
        module.setParentGuid("");
        module.setDelFlag(DelFlag.Normal.getCode());
        List<FrameModule> pList = frameModuleDao.select(module);
        return getChildModulesVue3Route(pList);
    }

    public JSONArray getChildModulesVue3Route(List<FrameModule> deptTopTrees) {
        JSONArray array = new JSONArray();
        for (FrameModule frameModule : deptTopTrees) {
            JSONObject json = new JSONObject();
            json.put("path", frameModule.getPath() == null ? "" : frameModule.getPath());
            json.put("componentName", frameModule.getComponentName() == null ? "" : frameModule.getComponentName());
            json.put("component", frameModule.getModuleAddress());
            JSONObject metaObj = new JSONObject();

            //根据moduleGuid获取对应角色
            List<FrameRoleModulePojo> moduleRight = roleModuleDao.selectLJRole(frameModule.getRowGuid());
            JSONArray arr = new JSONArray();
            if (!moduleRight.isEmpty()) {
                for (FrameRoleModulePojo roleModulePojo : moduleRight) {
                    arr.add(roleModulePojo.getRoleName());
                }
            }
            metaObj.put("hidden", frameModule.getIsVisible());
            /**
             * 设置该路由在侧边栏和面包屑中展示的名字
             */
            metaObj.put("title", frameModule.getModuleName());
            /**
             * 当设置 noredirect 的时候该路由在面包屑导航中不可被点击
             */
            metaObj.put("redirect", frameModule.getRedirect());
            /**
             * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式，
             * 只有一个时，会将那个子路由当做根路由显示在侧边栏，
             * 若你想不管路由下面的 children 声明的个数都显示你的根路由，
             * 可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，
             */
            metaObj.put("alwaysShow", true);
            /**
             * 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
             */
            metaObj.put("noCache", false);
            /**
             * 设置该路由的图标
             */
            metaObj.put("icon", frameModule.getSmallIcon());
            //则会一直固定在tag项中(默认 false)
            metaObj.put("affix", false);
            //则不会出现在tag中(默认 false)
            metaObj.put("noTagsView", false);
            /**
             * 设置为true即使hidden为true，也依然可以进行路由跳转(默认 false)
             */
            metaObj.put("canTo", false);
            metaObj.put("roles", arr);
            json.put("meta", metaObj);

            //获取子模块
            FrameModule module = new FrameModule();
            module.setParentGuid(frameModule.getRowGuid());
            module.setDelFlag(DelFlag.Normal.getCode());
            List<FrameModule> childDept = frameModuleDao.select(module);
            json.put("children", getChildModulesVue3Route(childDept));
            array.add(json);
        }
        return array;
    }
}
