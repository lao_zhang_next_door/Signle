package com.xnw.frame.service.impl;

import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.dao.FrameUserRoleDao;
import com.xnw.frame.entity.FrameUser;
import com.xnw.frame.entity.FrameUserRole;
import com.xnw.frame.service.FrameUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrameUserRoleServiceImpl extends BaseServiceImpl<FrameUserRole> implements FrameUserRoleService {

	public FrameUserRoleServiceImpl(FrameUserRoleDao dao) {
		super(dao);
	}

	@Autowired
	private FrameUserRoleDao frameUserRoleDao;

	@Override
	public List<String> selectRoleNameListByUserGuid(String userGuid) {
		return frameUserRoleDao.selectRoleNameListByUserGuid(userGuid);
	}

	@Override
	public List<FrameUser> getUserListByRoleName(String roleGuid) {
		return frameUserRoleDao.getUserListByRoleName(roleGuid);
	}
}
