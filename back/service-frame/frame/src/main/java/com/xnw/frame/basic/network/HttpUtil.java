package com.xnw.frame.basic.network;

import com.alibaba.fastjson.JSONObject;
import com.xnw.frame.basic.utils.request.ResultCode;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Http 工具类
 *
 * @author keeny
 * @ClassName: HttpUtil
 * @Description: 用于模拟HTTP请求中GET/POST方式
 * @date 2019年1月8日 上午10:51:11
 */
public class HttpUtil {

    protected static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    /**
     * 发送GET请求
     *
     * @param requestUrl
     * @return
     */
    public static Object getRequest(String requestUrl, String charSetName) {
        String res = "";
        StringBuilder buffer = new StringBuilder();
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            if (ResultCode.SUCCESS.getCode() == urlCon.getResponseCode()) {
                InputStream is = urlCon.getInputStream();
                InputStreamReader isr = new InputStreamReader(is, charSetName);
                BufferedReader br = new BufferedReader(isr);
                String str = null;
                while ((str = br.readLine()) != null) {
                    buffer.append(str);
                }
                br.close();
                isr.close();
                is.close();
                res = buffer.toString();
                return res;
            } else {
                throw new Exception("连接失败");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }


    /**
     * 发送GET请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     */
    public static String sendGet(String url, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder();
        BufferedReader in = null;
        // 读取响应输入流
        StringBuilder sb = new StringBuilder();
        // 存储参数
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数  
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);
            String fullUrl = url + "?" + params;
            //System.out.println(fullUrl);
            // 创建URL对象  
            URL connURL = new URL(fullUrl);
            // 打开URL连接  
            HttpURLConnection httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性  
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 建立实际的连接  
            httpConn.connect();
            // 响应头部获取  
            Map<String, List<String>> headers = httpConn.getHeaderFields();
            // 遍历所有的响应头字段  
            //for (String key : headers.keySet()) {
            //System.out.println(key + "\t：\t" + headers.get(key));
            //}
            // 定义BufferedReader输入流来读取URL的响应,并设置编码方式  
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), StandardCharsets.UTF_8));
            String line;
            // 读取返回的内容  
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 发送GET请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     */
    public static String sendGetLine(String url, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder();
        BufferedReader in = null;
        // 读取响应输入流
        // 存储参数
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数  
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);
            String fullUrl = url + "?" + params;
            //System.out.println(full_url);
            // 创建URL对象  
            URL connURL = new URL(fullUrl);
            // 打开URL连接  
            HttpURLConnection httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性  
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 建立实际的连接  
            httpConn.connect();
            // 响应头部获取  
            Map<String, List<String>> headers = httpConn.getHeaderFields();
            // 遍历所有的响应头字段  
            //for (String key : headers.keySet()) {
            //System.out.println(key + "\t：\t" + headers.get(key));
            //}
            // 定义BufferedReader输入流来读取URL的响应,并设置编码方式  
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), StandardCharsets.UTF_8));
            String line;
            // 读取返回的内容  
            while ((line = in.readLine()) != null) {
                result.append(line).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 发送POST请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     */
    public static String sendPost(String url, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder();
        // 返回的结果
        BufferedReader in = null;
        // 读取响应输入流
        PrintWriter out = null;
        // 处理请求参数
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数  
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);
            // 创建URL对象  
            URL connURL = new URL(url);
            // 打开URL连接  
            HttpURLConnection httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性  
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 设置POST方式  
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            // 获取HttpURLConnection对象对应的输出流  
            out = new PrintWriter(httpConn.getOutputStream());
            // 发送请求参数  
            out.write(params);
            // flush输出流的缓冲  
            out.flush();
            if (httpConn.getResponseCode() == ResultCode.SUCCESS.getCode()) {
                // 定义BufferedReader输入流来读取URL的响应，设置编码方式  
                in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), StandardCharsets.UTF_8));

            } else {
                in = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(), StandardCharsets.UTF_8));
            }

            String line;
            // 读取返回的内容  
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 发送POST请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     */
    public static String sendPostWithXWwwForm(String url, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder();
        // 返回的结果
        BufferedReader in = null;
        // 读取响应输入流
        PrintWriter out = null;
        // 处理请求参数
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);
            // 创建URL对象
            URL connURL = new URL(url);
            // 打开URL连接
            HttpURLConnection httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性
            httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 设置POST方式
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            // 获取HttpURLConnection对象对应的输出流
            out = new PrintWriter(httpConn.getOutputStream());
            // 发送请求参数
            out.write(params);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应，设置编码方式
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), StandardCharsets.UTF_8));
            String line;
            // 读取返回的内容
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 发送POST请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     * @throws Exception
     */
    public static String sendPostWithFile(String url, Map<String, String> parameters, MultipartFile file) throws Exception {
        String BOUNDARY = java.util.UUID.randomUUID().toString();
        String PREFIX = "--", LINEND = "\r\n";
        String MULTIPART_FROM_DATA = "multipart/form-data";
        String CHARSET = "UTF-8";

        URL uri = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
        conn.setReadTimeout(30 * 1000);
        // 缓存的最长时间
        conn.setDoInput(true);
        // 允许输入
        conn.setDoOutput(true);
        // 允许输出
        conn.setUseCaches(false);
        // 不允许使用缓存
        conn.setRequestMethod("POST");
        conn.setRequestProperty("connection", "keep-alive");
        conn.setRequestProperty("Charsert", "UTF-8");
        conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
                + ";boundary=" + BOUNDARY);
        StringBuilder sb = new StringBuilder();
        if (parameters != null) {
            // 首先组拼文本类型的参数  
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                sb.append(PREFIX);
                sb.append(BOUNDARY);
                sb.append(LINEND);
                sb.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"").append(LINEND);
                sb.append("Content-Type: text/plain; charset=").append(CHARSET).append(LINEND);
                sb.append("Content-Transfer-Encoding: 8bit").append(LINEND);
                sb.append(LINEND);
                sb.append(entry.getValue());
                sb.append(LINEND);
            }
        }
        DataOutputStream outStream = new DataOutputStream(
                conn.getOutputStream());
        if (!StringUtils.isEmpty(sb.toString())) {
            outStream.write(sb.toString().getBytes());
        }
        // 发送文件数据  
        if (file != null) {
            StringBuilder sb1 = new StringBuilder();
            sb1.append(PREFIX);
            sb1.append(BOUNDARY);
            sb1.append(LINEND);
            sb1.append("Content-Disposition: form-data; name=\"file\"; filename=\"").append(file.getOriginalFilename()).append("\"").append(LINEND);
            sb1.append("Content-Type: application/octet-stream; charset=").append(CHARSET).append(LINEND);
            sb1.append(LINEND);
            outStream.write(sb1.toString().getBytes());

            InputStream is = file.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }

            is.close();
            outStream.write(LINEND.getBytes());
        }

        // 请求结束标志  
        byte[] endData = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
        outStream.write(endData);
        outStream.flush();

        // 得到响应码  
        int res = conn.getResponseCode();
        InputStream in = conn.getInputStream();
        if (res == ResultCode.SUCCESS.getCode()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            StringBuilder buffer = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }

//          int ch;  
//          StringBuilder sb2 = new StringBuilder();  
//          while ((ch = in.read()) != -1) {  
//              sb2.append((char) ch);  
//          }  
            return buffer.toString();
        }
        outStream.close();
        conn.disconnect();
        return in.toString();

    }


    /**
     * 发送POST请求
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     * @throws Exception
     */
    public static String sendPostWithFiles(String url, Map<String, String> parameters, MultipartFile file) throws Exception {
        String BOUNDARY = java.util.UUID.randomUUID().toString();
        String PREFIX = "--", LINEND = "\r\n";
        String MULTIPART_FROM_DATA = "multipart/form-data";
        String CHARSET = "UTF-8";

        URL uri = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
        conn.setReadTimeout(30 * 1000); // 缓存的最长时间  
        conn.setDoInput(true);// 允许输入  
        conn.setDoOutput(true);// 允许输出  
        conn.setUseCaches(false); // 不允许使用缓存  
        conn.setRequestMethod("POST");
        conn.setRequestProperty("connection", "keep-alive");
        conn.setRequestProperty("Charsert", "UTF-8");
        conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
                + ";boundary=" + BOUNDARY);

        StringBuilder sb = new StringBuilder();

        if (parameters != null) {
            // 首先组拼文本类型的参数  
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                sb.append(PREFIX);
                sb.append(BOUNDARY);
                sb.append(LINEND);
                sb.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"").append(LINEND);
                sb.append("Content-Type: text/plain; charset=").append(CHARSET).append(LINEND);
                sb.append("Content-Transfer-Encoding: 8bit").append(LINEND);
                sb.append(LINEND);
                sb.append(entry.getValue());
                sb.append(LINEND);
            }

        }

        DataOutputStream outStream = new DataOutputStream(
                conn.getOutputStream());
        if (!StringUtils.isEmpty(sb.toString())) {
            outStream.write(sb.toString().getBytes());
        }


        // 发送文件数据  
        if (file != null) {
            StringBuilder sb1 = new StringBuilder();
            sb1.append(PREFIX);
            sb1.append(BOUNDARY);
            sb1.append(LINEND);
            sb1.append("Content-Disposition: form-data; name=\"files\"; filename=\"").append(file.getOriginalFilename()).append("\"").append(LINEND);
            sb1.append("Content-Type: application/octet-stream; charset=").append(CHARSET).append(LINEND);
            sb1.append(LINEND);
            outStream.write(sb1.toString().getBytes());

            InputStream is = file.getInputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }

            is.close();
            outStream.write(LINEND.getBytes());
        }

        // 请求结束标志  
        byte[] endData = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
        outStream.write(endData);
        outStream.flush();

        // 得到响应码  
        int res = conn.getResponseCode();
        InputStream in = conn.getInputStream();
        if (res == ResultCode.SUCCESS.getCode()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            StringBuilder buffer = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }

//          int ch;  
//          StringBuilder sb2 = new StringBuilder();  
//          while ((ch = in.read()) != -1) {  
//              sb2.append((char) ch);  
//          }  
            return buffer.toString();
        }
        outStream.close();
        conn.disconnect();
        return in.toString();

    }

    /**
     * 多文件
     *
     * @param @param  url
     * @param @param  parameters
     * @param @param  file
     * @param @return
     * @param @throws Exception    设定文件
     * @return String    返回类型
     * @throws
     * @Title: sendPostWithFiles
     * @Description: TODO(这里用一句话描述这个方法的作用)
     */
    public static String sendPostWithFiles(String url, Map<String, String> parameters, MultipartFile[] files) throws Exception {
        String BOUNDARY = java.util.UUID.randomUUID().toString();
        String PREFIX = "--", LINEND = "\r\n";
        String MULTIPART_FROM_DATA = "multipart/form-data";
        String CHARSET = "UTF-8";

        URL uri = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
        conn.setReadTimeout(30 * 1000);
        // 缓存的最长时间
        conn.setDoInput(true);
        // 允许输入
        conn.setDoOutput(true);
        // 允许输出
        conn.setUseCaches(false);
        // 不允许使用缓存
        conn.setRequestMethod("POST");
        conn.setRequestProperty("connection", "keep-alive");
        conn.setRequestProperty("Charsert", "UTF-8");
        conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
                + ";boundary=" + BOUNDARY);

        StringBuilder sb = new StringBuilder();

        if (parameters != null) {
            // 首先组拼文本类型的参数  
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                sb.append(PREFIX);
                sb.append(BOUNDARY);
                sb.append(LINEND);
                sb.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"").append(LINEND);
                sb.append("Content-Type: text/plain; charset=").append(CHARSET).append(LINEND);
                sb.append("Content-Transfer-Encoding: 8bit").append(LINEND);
                sb.append(LINEND);
                sb.append(entry.getValue());
                sb.append(LINEND);
            }

        }

        DataOutputStream outStream = new DataOutputStream(
                conn.getOutputStream());
        if (!StringUtils.isEmpty(sb.toString())) {
            outStream.write(sb.toString().getBytes());
        }


        // 发送文件数据  
        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                StringBuilder sb1 = new StringBuilder();
                sb1.append(PREFIX);
                sb1.append(BOUNDARY);
                sb1.append(LINEND);
                sb1.append("Content-Disposition: form-data; name=\"files\"; filename=\"").append(file.getOriginalFilename()).append("\"").append(LINEND);
                sb1.append("Content-Type: application/octet-stream; charset=").append(CHARSET).append(LINEND);
                sb1.append(LINEND);
                outStream.write(sb1.toString().getBytes());

                InputStream is = file.getInputStream();
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = is.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }

                is.close();
                outStream.write(LINEND.getBytes());
            }

        }

        // 请求结束标志  
        byte[] endData = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
        outStream.write(endData);
        outStream.flush();

        // 得到响应码  
        int res = conn.getResponseCode();
        InputStream in = conn.getInputStream();
        if (res == ResultCode.SUCCESS.getCode()) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            StringBuilder buffer = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }

//          int ch;  
//          StringBuilder sb2 = new StringBuilder();  
//          while ((ch = in.read()) != -1) {  
//              sb2.append((char) ch);  
//          }  
            return buffer.toString();
        }
        outStream.close();
        conn.disconnect();
        return in.toString();

    }


    /**
     * 发送POST请求  换行
     *
     * @param url        目的地址
     * @param parameters 请求参数，Map类型。
     * @return 远程响应结果
     */
    public static String sendPostLine(String url, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder();
        // 返回的结果
        BufferedReader in = null;
        // 读取响应输入流
        PrintWriter out = null;
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);
            // 创建URL对象  
            URL connURL = new URL(url);
            // 打开URL连接  
            HttpURLConnection httpConn = (HttpURLConnection) connURL
                    .openConnection();
            // 设置通用属性  
            httpConn.setRequestProperty("Accept", "*/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 设置POST方式  
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            // 获取HttpURLConnection对象对应的输出流  
            out = new PrintWriter(httpConn.getOutputStream());
            // 发送请求参数  
            out.write(params);
            // flush输出流的缓冲  
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应，设置编码方式  
            in = new BufferedReader(new InputStreamReader(httpConn
                    .getInputStream(), StandardCharsets.UTF_8));
            String line;
            // 读取返回的内容  
            while ((line = in.readLine()) != null) {
                result.append(line).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * @param urlPath 下载路径
     *                下载存放目录
     * @return 返回下载文件
     */

    public static InputStream downloadFileStream(String urlPath, Map<String, String> parameters) {
        List<String> lstParams = new ArrayList<>();
        String params = "";
        // 编码之后的参数
        try {
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);

            if (lstParams.size() > 0) {
                if (urlPath.contains("?")) {
                    urlPath += "&";
                } else {
                    urlPath += "?";
                }
                urlPath += params;
            }
            // 统一资源
            URL url = new URL(urlPath);
            // 连接类的父类，抽象类
            URLConnection urlConnection = url.openConnection();
            // http的连接类
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            // 设定请求的方法，默认是GET
            httpURLConnection.setRequestMethod("GET");
            // 设置字符编码
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            // 打开到此 URL 引用的资源的通信链接（如果尚未建立这样的连接）。
            httpURLConnection.connect();
            // 文件大小
            int fileLength = httpURLConnection.getContentLength();

            //System.out.println("file length---->" + fileLength);
            return httpURLConnection.getInputStream();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return null;
        }
    }

    public static byte[] downloadFile(String urlPath, Map<String, String> parameters) {

        InputStream input = downloadFileStream(urlPath, parameters);
        if (input == null) {
            return new byte[0];
        }
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int rc = 0;
            while ((rc = input.read(buff, 0, buff.length)) > 0) {
                baos.write(buff, 0, rc);
            }

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static File downloadFile(String urlPath, String downloadDir, Map<String, String> parameters, String fileName) {
        File file = null;
        try {
            InputStream input = downloadFileStream(urlPath, parameters);
            if (input == null) {
                return null;
            }
            BufferedInputStream bin = new BufferedInputStream(input);
            // 编码请求参数
            String path = downloadDir + File.separatorChar + fileName;
            file = new File(path);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
                if (!file.exists()) {
                    file.createNewFile();
                }
            }

            OutputStream out = Files.newOutputStream(file.toPath());
            int size = 0;
            byte[] buf = new byte[1024];
            while ((size = bin.read(buf)) != -1) {
                out.write(buf, 0, size);
                // 打印下载百分比
                // System.out.println("下载了-------> " + len * 100 / fileLength +
                // "%\n");
            }
            bin.close();
            out.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            return file;
        }

    }

    /**
     * @param urlPath 下载路径
     * @return 返回下载文件
     */
    public static void provideDownloadFile(String urlPath, Map<String, String> parameters, HttpServletResponse response) {
        PrintWriter outConn = null;
        // 处理请求参数
        String params = "";
        // 编码之后的参数
        try {
            // 编码请求参数  
            List<String> lstParams = new ArrayList<>();
            for (String name : parameters.keySet()) {
                lstParams.add(name + "=" + java.net.URLEncoder.encode(parameters.get(name),
                        "UTF-8"));
            }
            params = String.join("&", lstParams);

            // 统一资源
            URL url = new URL(urlPath);
            // 连接类的父类，抽象类
            URLConnection urlConnection = url.openConnection();
            // http的连接类
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            // 设定请求的方法，默认是GET
            httpURLConnection.setRequestMethod("POST");
            // 设置字符编码
            httpURLConnection.setRequestProperty("Charset", "UTF-8");

            // 设置POST方式  
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            // 获取HttpURLConnection对象对应的输出流  
            outConn = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数  
            outConn.write(params);
            // flush输出流的缓冲  
            outConn.flush();

            // 打开到此 URL 引用的资源的通信链接（如果尚未建立这样的连接）。
            httpURLConnection.connect();

            response.setHeader("Accept-Ranges", "bytes");
            //response.setCharacterEncoding("utf-8");
            //response.setContentType("application/octet-stream");
            response.setContentType("application/zip");

            BufferedInputStream bin = new BufferedInputStream(httpURLConnection.getInputStream());
            OutputStream os = new BufferedOutputStream(response.getOutputStream());
            byte[] buf = new byte[1024];
            int size = 0;
            while (((size = bin.read(buf)) != -1)) {
                os.write(buf, 0, size);
            }
            os.flush();
            os.close();
            bin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String method = "POST";
    private static String charset = "UTF-8";
    private static String contentType = "text/xml";

    public static String sendXmlMsg(String address, String xmlMsg) throws Exception {
        StringBuilder sb = new StringBuilder();
        URL url = new URL(address);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setInstanceFollowRedirects(true);
        conn.setRequestProperty("Content-Type", contentType);
        conn.connect();
        // 向输出流写出数据
        PrintWriter pw = new PrintWriter(conn.getOutputStream());
        pw.write(xmlMsg);
        pw.flush();
        pw.close();
        String contentType = conn.getContentType();
        Pattern pattern = Pattern.compile("charset=.*:?");
        Matcher matcher = pattern.matcher(contentType);
        if (matcher.find()) {
            charset = matcher.group();
            if (charset.endsWith(";")) {
                charset = charset.substring(charset.indexOf("=") + 1, charset.indexOf(";"));
            } else {
                charset = charset.substring(charset.indexOf("=") + 1);
            }
            if (charset.contains("\"")) {
                charset = charset.substring(1, charset.length() - 1);
            }
            charset = charset.trim();
        }

        InputStream inStream = conn.getInputStream();
        BufferedReader ufferedReader = new BufferedReader(new InputStreamReader(inStream, charset));
        String line;
        while ((line = ufferedReader.readLine()) != null) {
            sb.append(line);
        }
        ufferedReader.close();
        conn.disconnect();
        return sb.toString();
    }

    /**
     * 文件下载
     *
     * @param url      文件输出流的路径
     * @param filePath 文件路径和文件名
     * @param method   方法 默认get方式
     * @return
     */
    public static File saveUrlAs(String url, String filePath, String method) {
        //System.out.println("fileName---->"+filePath);
        //创建不同的文件夹目录
        File file = new File(filePath);

        FileOutputStream fileOut = null;
        HttpURLConnection conn = null;
        InputStream inputStream = null;
        try {
            // 建立链接
            URL httpUrl = new URL(url);
            conn = (HttpURLConnection) httpUrl.openConnection();
            //以Post方式提交表单，默认get方式
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            // post方式不能使用缓存
            conn.setUseCaches(false);
            //连接指定的资源
            conn.connect();
            //获取网络输入流
            inputStream = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(inputStream);

            //写入到文件（注意文件保存路径的后面一定要加上文件的名称）
            fileOut = new FileOutputStream(filePath);
            BufferedOutputStream bos = new BufferedOutputStream(fileOut);

            byte[] buf = new byte[4096];
            int length = bis.read(buf);
            //保存文件
            while (length != -1) {
                bos.write(buf, 0, length);
                length = bis.read(buf);
            }
            bos.close();
            bis.close();
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("抛出异常！！");
        }

        return file;

    }

    /**
     * 主函数，测试请求
     *
     * @param args
     */
    public static void main(String[] args) {
        String signMap = "{\"appId\":\"jdxjcxml180814@jdtest\",\"businessType\":\"ResultCode.SUCCESS.getCode()0\",\"caseNumber\":\"AJ20190128160746478290181\",\"charset\":\"utf-8\",\"entrustedAgentInfos\":\"[{\\\"lawOfficeName\\\":\\\"李一仁\\\",\\\"entrustedAgentName\\\":\\\"单杨\\\",\\\"identityTypeText\\\":\\\"32091119940127121X\\\",\\\"entrustedAgentMobile\\\":\\\"18762671144\\\",\\\"agentAuthTypeText\\\":\\\"0\\\",\\\"email\\\":\\\"liyiren@qq.com\\\"}]\",\"executorInfos\":\"[{\\\"permanentAddress\\\":\\\"江苏省无锡市梁溪区兴隆桥社区6号\\\",\\\"userId\\\":\\\"ff808081688795df016893299c55014c\\\",\\\"habitualResidenceAddress\\\":\\\"江苏省无锡市梁溪区兴隆桥社区6号\\\",\\\"shroffAccountInfos\\\":[{\\\"shroffAccountOpenBankName\\\":\\\"中国工商银行无锡东绛支行\\\",\\\"shroffAccountName\\\":\\\"吴杰\\\",\\\"shroffAccountCardNumber\\\":\\\"9558801103107432891\\\"}]}]\",\"format\":\"json\",\"orderNumbers\":\"[{\\\"orderNumber\\\":\\\"2019012861026\\\"}]\",\"performanceInfo\":\"{\\\"repaymentMethod\\\":\\\"还款方式(按文书约定填写，如按月等额本息还款/按月等额本金还款，按日计息)\\\",\\\"penaltyCalculationFormula\\\":\\\"违约金计算公式（违约金=应还未还本金×违约金比例×违约天数\\\",\\\"paidInterest\\\":\\\"1000.00\\\",\\\"loanLimitEndTime\\\":\\\"20190126\\\",\\\"ensureDeadline\\\":\\\"三年\\\",\\\"penalty\\\":\\\"20.00\\\",\\\"interestCalculationFormula\\\":\\\"年收益率\\\",\\\"compoundInterest\\\":\\\"6.58\\\",\\\"debtOverdueCalculationDeadlineTime\\\":\\\"20190130\\\",\\\"loanRecordInfos\\\":[{\\\"loanDuration\\\":\\\"1年\\\",\\\"loanPeriods\\\":\\\"1期\\\",\\\"interest\\\":\\\"1ResultCode.SUCCESS.getCode().00\\\",\\\"agreementName\\\":\\\"合同jc20190128\\\",\\\"borrowStartTime\\\":\\\"20180127\\\",\\\"agreementCode\\\":\\\"jc20190128\\\",\\\"borrowEndTime\\\":\\\"20190126\\\",\\\"loanAmount\\\":\\\"1ResultCode.SUCCESS.getCode()0.00\\\",\\\"loanIssueTime\\\":\\\"20180126\\\"}],\\\"loadDefaultTime\\\":\\\"20190128\\\",\\\"eachRepaymentCalculationFormula\\\":\\\"每期还款金额的计算公式,每期还款金额=贷款本金/贷款期数+贷款本金×贷款月利率+违约金（若有）每期还款金额=每期还款金额=贷款本金/贷款期数+（贷款本金-累计已还本金）×贷款日利率×当期实际天数+违约金（若有）\\\",\\\"owePrincipal\\\":\\\"ResultCode.SUCCESS.getCode()0.00\\\",\\\"notaryFees\\\":\\\"ResultCode.SUCCESS.getCode()0.00\\\",\\\"attorneyFees\\\":\\\"1000.00\\\",\\\"lendingRate\\\":\\\"10%\\\",\\\"collectionNotificationTime\\\":\\\"20190128\\\",\\\"fine\\\":\\\"6.58\\\",\\\"repaymentDate\\\":\\\"10日\\\",\\\"applicationAmount\\\":\\\"2ResultCode.SUCCESS.getCode().00\\\",\\\"fineCalculationFormula\\\":\\\"罚息计算公式=年利息÷365×违约天数\\\",\\\"ensureForm\\\":\\\"书面保证\\\",\\\"returnedPrincipal\\\":\\\"10000.00\\\",\\\"oweInterest\\\":\\\"ResultCode.SUCCESS.getCode().00\\\",\\\"loanLimitStartTime\\\":\\\"20180127\\\",\\\"maximumLoanAmount\\\":\\\"1ResultCode.SUCCESS.getCode()0.00\\\",\\\"compoundInterestCalculationFormula\\\":\\\"复利计算公式=年利息÷365×违约天数\\\"}\",\"sign\":\"f563e6bdb7cdc00f918607e85cf75e6a\",\"signTime\":\"20190129095216\",\"signType\":\"MD5\",\"token\":\"3d28f1cb86f508ac1aaf1762bfec5407\",\"version\":\"2.1\"}";
        JSONObject jsonObject = JSONObject.parseObject(signMap);
        Map maps = jsonObject;
        String result = sendPostWithXWwwForm("https://testsign.egongzheng.com/opensignapi/sign/bidorder/addNotarizationApplyByCase.json", maps);
        System.out.println(result);
    }
}