package com.xnw.frame.basic.utils.frame;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * @author T470
 */
public class ApplicationUtils {
    public final static Properties SYSTEM_PROPERTIES = loadAllApplicationProperties();


    public static Properties loadConfProperties(String name) {
        Properties properties = new Properties();
        InputStream in = null;

        // 优先从项目路径获取连接信息
        String dirClass = System.getProperty("user.dir");
        String confPath = dirClass + File.separator + name + ".properties";
        File file = new File(confPath);
        if (file.exists()) {
            try {
                in = new FileInputStream(new File(confPath));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            in = ApplicationUtils.class.getClassLoader().getResourceAsStream(name + ".properties");
        }
        try {
            if (in != null) {
                BufferedReader bf = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                properties.load(bf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }

    public static String loadApplicationResource(String path) {
        return loadResource(ApplicationUtils.class, path);
    }

    public static String loadResource(Class<?> cls, String path) {
        StringBuilder sb = new StringBuilder();
        InputStream in = loadResourceInputStream(cls, path);
        if (in != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                String contentLine = br.readLine();
                while (contentLine != null) {
                    sb.append(contentLine);
                    contentLine = br.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static InputStream loadResourceInputStream(Class<?> cls, String path) {
        return cls.getResourceAsStream(path);
    }

    public static Properties loadAllApplicationProperties() {
        Properties properties = loadConfProperties("application");
        String active = properties.getProperty("spring.profiles.active");
        // 读取配置文件获取定制数据源，也可以通过数据库获取数据源

        if (!StringUtil.isEmpty(active)) {
            Properties propertiesNew = loadConfProperties("application-" + active);
            properties.putAll(propertiesNew);
        }
        return properties;
    }

    public static Properties loadApplicationProperties() {
        return SYSTEM_PROPERTIES;
    }

    public static String getApplicationProperties(String key) {
        Properties properties = loadApplicationProperties();
        return properties.containsKey(key) ? properties.getProperty(key) : "";
    }
}
