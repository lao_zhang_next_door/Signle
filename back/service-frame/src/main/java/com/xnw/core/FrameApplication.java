package com.xnw.core;

import com.xnw.frame.basic.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author hero
 */
//@SpringBootApplication(exclude = {
//        DataSourceAutoConfiguration.class
//})
@SpringBootApplication(scanBasePackages = {"com.xnw"})
@EnableScheduling
@EnableAsync
//@ComponentScan(basePackages = {"com.xnw.*"})
@MapperScan(basePackages = {"com.xnw.frame.dao"})
@EnableTransactionManagement
public class FrameApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(FrameApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FrameApplication.class);
    }


}
