package com.xnw.core;

import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author T470
 * springDoc控制器
 */
@Configuration
public class OpenApiConfig {

    @Bean
    public GroupedOpenApi frameApi() {
        return GroupedOpenApi.builder()
                .group("Frame")
                .pathsToMatch("/frame/**")
                .addOpenApiCustomiser(openApi -> openApi.info(new Info().title("Frame API").version("1.0")))
                .build();
    }

    @Bean
    public GroupedOpenApi applicationApi() {
        return GroupedOpenApi.builder()
                .group("Application")
                .pathsToMatch("/application/**")
                .addOpenApiCustomiser(openApi -> openApi.info(new Info().title("Application API").version("1.0")))
                .build();
    }
}
