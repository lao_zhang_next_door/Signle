package com.xnw.application.service.impl;

import com.xnw.application.basic.enumeration.IsOrNot;
import com.xnw.application.dao.AppShortMessageDao;
import com.xnw.application.entity.AppShortMessage;
import com.xnw.application.service.AppShortMessageService;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import com.xnw.frame.rabbitmq.TopicExchange.shortMessage.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class AppShortMessageServiceImpl extends BaseServiceImpl<AppShortMessage> implements AppShortMessageService {

    @Autowired
    private AppShortMessageDao appShortMessageDao;


    public AppShortMessageServiceImpl(BaseDao<AppShortMessage> dao) {
        super(dao);
    }

    @Autowired
    private MessageSender sender;

    @Override
    public String singleSend(String mobile, String text) {
        return "";

    }

    @Override
    public String batchSend(String mobile, String text) {
        return "";
    }

    @Override
    public String tplSingleSend(String mobile, long tpl_id, Map<String, String> par, String ENCODING) {
        return "";
    }

    @Override
    public String tplBatchSend(String mobile, long tpl_id, Map<String, String> par, String ENCODING) {
        return "";
    }

    @Override
    public void updateEntity(AppShortMessage message) {
        appShortMessageDao.updateByRowGuid(message);
    }

    @Override
    public void sendMessage(AppShortMessage appShortMessage) {
        if (appShortMessage.getIsSend() == null) {
            /**
             * 给与默认值 (未发送)
             */
            appShortMessage.setIsSend(IsOrNot.NOT.getCode());
        }


        this.insert(appShortMessage);
        //根据是否立即发送 来选择发送方式
        if (appShortMessage.getIsSendNow() == IsOrNot.IS.getCode()) {
            //需要立即发送的话 直接塞入短信队列
            sender.sendMessage(appShortMessage);
        }
    }


}
