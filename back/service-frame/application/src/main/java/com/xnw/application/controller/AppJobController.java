package com.xnw.application.controller;

import com.alibaba.fastjson.JSONArray;
import com.xnw.application.basic.enumeration.IsOrNot;
import com.xnw.application.basic.utils.Common;
import com.xnw.application.entity.AppJob;
import com.xnw.application.service.AppJobService;
import com.xnw.frame.auth.token.PassToken;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.quartz.TriggerUtils;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author
 * @date 2020-07-02 13:52:28
 */
@Tag(name = "定时任务应用服务")
@RestController
@CrossOrigin
@RequestMapping("/application/AppJob")
public class AppJobController extends BaseController {
    @Autowired
    private AppJobService appJobService;
    @Autowired
    private Common common;

    /**
     * 列表数据
     */
    @PassToken
    @Operation(summary = "列表数据")
    @ResponseBody
    @RequestMapping(value = "/listData", produces = "application/json;charset=utf-8", method = RequestMethod.GET)
    public R listData(@RequestParam Map<String, Object> params) {
        //查询列表数据
        params.put("isSend", String.valueOf(IsOrNot.IS.getCode()));
        Query query = new Query(params);
        List<AppJob> list = appJobService.selectByLimit(query);
        int count = appJobService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(list, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());

    }

    /**
     * 新增
     **/
    @Operation(summary = "新增")
    @ResponseBody
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R add(@RequestBody AppJob appJob) {
        appJob.initNull();
        appJobService.insert(appJob);
        return R.ok();
    }

    /**
     * 修改
     */
    @Operation(summary = "修改")
    @ResponseBody
    @RequestMapping(value = "/update", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    public R update(@RequestBody AppJob appJob) {
        appJobService.updateByRowGuidSelective(appJob);
        return R.ok();
    }

    /**
     * 删除
     */
    @Operation(summary = "删除")
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=utf-8", method = RequestMethod.DELETE)
    public R delete(@RequestBody String[] rowGuids) {
        appJobService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 通过rowGuid获取一条记录
     *
     * @param rowGuid
     * @return
     */
    @PassToken
    @Operation(summary = "通过rowGuid获取一条记录")
    @ResponseBody
    @RequestMapping(value = "/getDetailByGuid", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R getDetailByGuid(@RequestParam String rowGuid) {
        AppJob appJob = new AppJob(rowGuid);
        AppJob model = appJobService.selectByPrimaryKey(appJob);
        return R.ok().put("data", model);
    }

    /**
     * 验证cron表达式
     *
     * @param cron
     * @param count
     * @return
     */
    @PassToken
    @ResponseBody
    @PostMapping(value = "/getRecentTriggerTime")
    public List<String> getRecentTriggerTime(@RequestParam String cron, int count) {
        ArrayList list = new ArrayList();
        try {
            CronTriggerImpl cronTriggerImpl = new CronTriggerImpl();
            cronTriggerImpl.setCronExpression(cron);
            // 6 代表获取6次
            List<Date> dates = TriggerUtils.computeFireTimes(cronTriggerImpl, null, count);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (Date date : dates) {
                list.add(dateFormat.format(date));
            }
        } catch (Exception e) {
            logger.error("cron表达式输入有误.......................");
            list.add("cron表达式输入有误");
        }
        return list;
    }

    /**
     * 执行任务
     *
     * @return
     */
    @PassToken
    @PostMapping(value = "/startJob")
    public R startJob(@RequestBody Map<String, Object> params) {
        String arr = JSONArray.toJSONString(params.get("jobList"));
        List<AppJob> jobList = JSONArray.parseArray(arr, AppJob.class);
        String type = (String) params.get("type");
        for (AppJob appJob : jobList) {
            common.runOrStopJob(appJob.getJobFpath(), type);
        }
        return R.ok();
    }

}
