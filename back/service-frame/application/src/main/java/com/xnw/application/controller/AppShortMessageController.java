package com.xnw.application.controller;


import com.xnw.application.basic.enumeration.IsOrNot;
import com.xnw.application.entity.AppShortMessage;
import com.xnw.application.service.AppShortMessageService;
import com.xnw.frame.auth.token.PassToken;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.dataformat.Formatter;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @date 2020-07-10 10:12:25
 */
@Tag(name = "短信应用")
@RestController
@CrossOrigin
@RequestMapping("application/appshortmessage")
public class AppShortMessageController extends BaseController {

    @Autowired
    private AppShortMessageService appShortMessageService;


    /**
     * 列表数据
     */
    @Operation(summary = "数据列表")
    @ResponseBody
    @RequestMapping(value = "/listData", produces = "application/json;charset=utf-8", method = RequestMethod.GET)
    public R listData(@RequestParam Map<String, Object> params) {
        //查询列表数据
        params.put("isSend", String.valueOf(IsOrNot.IS.getCode()));
        Query query = new Query(params);
        List<AppShortMessage> list = appShortMessageService.selectByLimit(query);
        int count = appShortMessageService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(list, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 新增
     **/
    @Operation(summary = "新增")
    @ResponseBody
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R add(@RequestBody AppShortMessage appShortmessage) {
        appShortmessage.initNull();
        appShortMessageService.insert(appShortmessage);
        return R.ok();
    }

    /**
     * 修改
     */
    @Operation(summary = "修改")
    @ResponseBody
    @RequestMapping(value = "/update", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    public R update(@RequestBody AppShortMessage appShortmessage) {
        appShortMessageService.updateEntity(appShortmessage);
        return R.ok();
    }

    /**
     * 删除
     */
    @Operation(summary =  "删除")
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R delete(@RequestBody String[] rowGuids) {
        appShortMessageService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 通过rowGuid获取一条记录
     *
     * @param rowGuid
     * @return
     */
    @Operation(summary =  "通过rowGuid获取一条记录")
    @ResponseBody
    @RequestMapping(value = "/getDetailByGuid", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R getDetailByGuid(@RequestParam String rowGuid) {
        AppShortMessage appShortMessage = new AppShortMessage(rowGuid);
        AppShortMessage model = appShortMessageService.selectByPrimaryKey(appShortMessage);
        return R.ok().put("data", model);
    }


    /**
     * 发送短信
     * 若模板发送 则tplId必须有值
     * 否则则为普通发送i
     **/
    @PassToken
    @Operation(summary =  "发送短信")
    @ResponseBody
    @RequestMapping(value = "/sendMessage", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R sendMessage(@RequestParam Map<String, String> params) {
        String smContent = params.get("content");
        //短信内容
        String receivePeopleStr = params.get("recipient");
        //收件人姓名
        String receiveMobilesStr = params.get("receiveMobile");
        //收件人手机号码
        String currentUserName = getCurrentUser().getUserName();
        //发送人
        LocalDateTime sendTime = LocalDateTime.now();
        if ("1".equals(params.get("status")) && !"".equals(params.get("infoDate"))) {
            //是否指定发送时间  若为空  默认当前时间
            sendTime = LocalDateTime.parse(params.get("infoDate"), Formatter.YMDHMS);
        }
        String[] receiveMobiles = receiveMobilesStr.split(",");
        String[] receivePeople = receivePeopleStr.split(",");

        for (int i = 0; i < receiveMobiles.length; i++) {
            AppShortMessage appShortmessage = new AppShortMessage();
            appShortmessage.setSmContent(smContent);
            //短信内容
            appShortmessage.setSmReceiveMobile(receiveMobiles[i]);
            //收件人手机号码
            appShortmessage.setSmSendPeople(currentUserName);
            //发送人
            appShortmessage.setSmSendTime(sendTime);
            //发送时间
            appShortmessage.setIsSend(0);
            if (i >= receivePeople.length) {
                appShortmessage.setSmReceivePeople("");
            } else {
                appShortmessage.setSmReceivePeople(receivePeople[i]);
            }
            appShortMessageService.insert(appShortmessage);
        }

        return R.ok();
    }

}
