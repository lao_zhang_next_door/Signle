package com.xnw.application.dao;

import com.xnw.application.entity.AppJob;
import com.xnw.frame.basic.base.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author 
 * @date 2020-07-02 13:52:28
 */
@Mapper
public interface AppJobDao extends BaseDao<AppJob> {
	
}
