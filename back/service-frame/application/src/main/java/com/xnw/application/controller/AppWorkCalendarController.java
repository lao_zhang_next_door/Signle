package com.xnw.application.controller;

import com.xnw.application.basic.enumeration.IsOrNot;
import com.xnw.application.entity.AppWorkCalendar;
import com.xnw.application.service.AppWorkCalendarService;
import com.xnw.frame.auth.token.PassToken;
import com.xnw.frame.basic.base.BaseController;
import com.xnw.frame.basic.utils.dataformat.Formatter;
import com.xnw.frame.basic.utils.frame.PageUtils;
import com.xnw.frame.basic.utils.frame.Query;
import com.xnw.frame.basic.utils.frame.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author 日历
 * @date 2020-07-03 14:42:24
 */
@Tag(name = "工作日历")
@RestController
@CrossOrigin
@RequestMapping("application/appworkcalendar")
public class AppWorkCalendarController extends BaseController {
    @Autowired
    private AppWorkCalendarService appWorkCalendarService;

    /**
     * 列表数据,根据年月来查询，tjh
     */
    @PassToken
    @Operation(summary = "")
    @ResponseBody
    @RequestMapping(value = "/listData", produces = "application/json;charset=utf-8", method = RequestMethod.GET)
    public R listData(@RequestParam Map<String, Object> params) {
        //查询列表数据
        params.put("isSend", String.valueOf(IsOrNot.IS.getCode()));
        Query query = new Query(params);
        List<AppWorkCalendar> list = appWorkCalendarService.selectByLimit(query);
        int count = appWorkCalendarService.selectCountByLimit(query);
        PageUtils pageUtil = new PageUtils(list, count, query.getLimit(), query.getPage());
        return R.list(pageUtil.getTotalCount(), pageUtil.getList());
    }

    /**
     * 新增
     **/
    @ResponseBody
    @Operation(summary = "")
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R add(@RequestBody AppWorkCalendar appWorkCalendar) {
        appWorkCalendar.initNull();
        appWorkCalendarService.insert(appWorkCalendar);
        return R.ok();
    }

    /**
     * 修改
     */
    @Operation(summary = "")
    @ResponseBody
    @RequestMapping(value = "/update", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    public R update(@RequestBody AppWorkCalendar appWorkCalendar) {
        appWorkCalendarService.updateByPrimaryKey(appWorkCalendar);
        return R.ok();
    }

    /**
     * tjh
     * 保存所有行的修改
     */
    @Operation(summary = "")
    @ResponseBody
    @RequestMapping(value = "/updateAllRows", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    public R saveAllRows(@RequestParam(value = "arrRowGuid[]") String[] arrRowGuid,
                         @RequestParam(value = "arrIsWorkDay[]") String[] arrIsWorkDay,
                         @RequestParam(value = "arrAmStart[]") String[] arrAmStart,
                         @RequestParam(value = "arrAmEnd[]") String[] arrAmEnd,
                         @RequestParam(value = "arrPmStart[]") String[] arrPmStart,
                         @RequestParam(value = "arrPmEnd[]") String[] arrPmEnd
    ) {
        for (int i = 0; i < arrRowGuid.length; i++) {
            AppWorkCalendar appWorkCalendarModel = new AppWorkCalendar();
            appWorkCalendarModel.setRowGuid(arrRowGuid[i]);
            AppWorkCalendar model = appWorkCalendarService.selectByPrimaryKey(appWorkCalendarModel);
            model.setIsWorkDay(Integer.parseInt(arrIsWorkDay[i]));
            model.setAmStart(arrAmStart[i]);
            model.setAmEnd(arrAmEnd[i]);
            model.setPmStart(arrPmStart[i]);
            model.setPmEnd(arrPmEnd[i]);
            appWorkCalendarService.updateByPrimaryKey(model);
        }

        return R.ok();
    }

    /**
     * tjh
     * 导入当前月初始数据
     */
    @Operation(summary = "")
    @ResponseBody
    @RequestMapping(value = "/initMonthData", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    public R initMonthData(@RequestParam(value = "arrWDate[]") String[] arrWDate,
                           @RequestParam(value = "arrMYear[]") String[] arrMYear,
                           @RequestParam(value = "arrWMonth[]") String[] arrWMonth,
                           @RequestParam(value = "arrWeek[]") String[] arrWeek,
                           @RequestParam(value = "arrIsWorkDay[]") String[] arrIsWorkDay,
                           @RequestParam(value = "arrAmStart[]") String[] arrAmStart,
                           @RequestParam(value = "arrAmEnd[]") String[] arrAmEnd,
                           @RequestParam(value = "arrPmStart[]") String[] arrPmStart,
                           @RequestParam(value = "arrPmEnd[]") String[] arrPmEnd
    ) {

        int dataLength = arrWeek.length;
        List<AppWorkCalendar> list = new ArrayList<AppWorkCalendar>();
        for (int i = 0; i < dataLength; i++) {
            AppWorkCalendar temp = new AppWorkCalendar();
            temp.initNull();
            // 设置各项
            temp.setWDate(LocalDateTime.parse(arrWDate[i], Formatter.YMDHMS));
            temp.setMYear(arrMYear[i]);
            temp.setWMonth(arrWMonth[i]);
            temp.setWeek(Integer.parseInt(arrWeek[i]));
            temp.setIsWorkDay(Integer.parseInt(arrIsWorkDay[i]));
            temp.setAmStart(arrAmStart[i]);
            temp.setAmEnd(arrAmEnd[i]);
            temp.setPmStart(arrPmStart[i]);
            temp.setPmEnd(arrPmEnd[i]);
            list.add(temp);
            appWorkCalendarService.insert(temp);
        }
        return R.ok();
    }

    /**
     * 删除
     */
    @Operation(summary = "")
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R delete(@RequestBody String[] rowGuids) {
        appWorkCalendarService.deleteBatch(rowGuids);
        return R.ok();
    }

    /**
     * 通过rowGuid获取一条记录
     *
     * @param rowGuid
     * @return
     */
    @Operation(summary = "通过rowGuid获取一条记录")
    @ResponseBody
    @RequestMapping(value = "/getDetailByGuid", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public R getDetailByGuid(@RequestParam String rowGuid) {
        AppWorkCalendar model = appWorkCalendarService.selectByPrimaryKey(new AppWorkCalendar(rowGuid));
        return R.ok().put("data", model);
    }

}
