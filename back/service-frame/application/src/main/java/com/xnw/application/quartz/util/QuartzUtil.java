package com.xnw.application.quartz.util;

import com.xnw.frame.basic.utils.frame.SubModuleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzUtil {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SubModuleUtils subModuleUtils;

    public void setAllJobs() {
        logger.info("获取定时任务");
        subModuleUtils.setAllJobs();
    }
}
