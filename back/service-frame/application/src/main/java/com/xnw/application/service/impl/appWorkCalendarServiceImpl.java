package com.xnw.application.service.impl;

import com.xnw.application.dao.AppWorkCalendarDao;
import com.xnw.application.entity.AppWorkCalendar;
import com.xnw.application.service.AppWorkCalendarService;
import com.xnw.frame.basic.base.BaseDao;
import com.xnw.frame.basic.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class appWorkCalendarServiceImpl extends BaseServiceImpl<AppWorkCalendar> implements AppWorkCalendarService {

    @Autowired
    private AppWorkCalendarDao appWorkCalendarDao;

    public appWorkCalendarServiceImpl(BaseDao<AppWorkCalendar> dao) {
        super(dao);
    }

}

