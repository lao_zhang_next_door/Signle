package com.xnw.application.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xnw.frame.basic.base.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * <p>Title: AppWorkcalendar</p>
 * <p>Description:</p>
 *
 * @author
 */

public class AppWorkCalendar extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public AppWorkCalendar(boolean init) {
        super(init);
    }

    public AppWorkCalendar(String rowGuid) {
        super(rowGuid);
    }

    public AppWorkCalendar() {
    }


    /**
     * 日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime wDate;


    /**
     * 月份
     */
    private String wMonth;


    /**
     * 年度
     */
    private String mYear;


    /**
     * 星期
     */
    private Integer week;


    /**
     * 是否是工作日
     */
    private Integer isWorkDay;


    /**
     * 上午上班时间
     */
    private String amStart;


    /**
     * 上午下班时间
     */
    private String amEnd;


    /**
     * 下午上班时间
     */
    private String pmStart;


    /**
     * 下午下班时间
     */
    private String pmEnd;


    /**
     * 设置：日期
     */
    public void setWDate(LocalDateTime wDate) {
        this.wDate = wDate;
    }

    /**
     * 获取：日期
     */
    public LocalDateTime getWDate() {
        return wDate;
    }

    /**
     * 设置：月份
     */
    public void setWMonth(String wMonth) {
        this.wMonth = wMonth;
    }

    /**
     * 获取：月份
     */
    public String getWMonth() {
        return wMonth;
    }

    /**
     * 设置：年度
     */
    public void setMYear(String mYear) {
        this.mYear = mYear;
    }

    /**
     * 获取：年度
     */
    public String getMYear() {
        return mYear;
    }

    /**
     * 设置：星期
     */
    public void setWeek(Integer week) {
        this.week = week;
    }

    /**
     * 获取：星期
     */
    public Integer getWeek() {
        return week;
    }

    /**
     * 设置：是否是工作日
     */
    public void setIsWorkDay(Integer isWorkDay) {
        this.isWorkDay = isWorkDay;
    }

    /**
     * 获取：是否是工作日
     */
    public Integer getIsWorkDay() {
        return isWorkDay;
    }

    /**
     * 设置：上午上班时间
     */
    public void setAmStart(String amStart) {
        this.amStart = amStart;
    }

    /**
     * 获取：上午上班时间
     */
    public String getAmStart() {
        return amStart;
    }

    /**
     * 设置：上午下班时间
     */
    public void setAmEnd(String amEnd) {
        this.amEnd = amEnd;
    }

    /**
     * 获取：上午下班时间
     */
    public String getAmEnd() {
        return amEnd;
    }

    /**
     * 设置：下午上班时间
     */
    public void setPmStart(String pmStart) {
        this.pmStart = pmStart;
    }

    /**
     * 获取：下午上班时间
     */
    public String getPmStart() {
        return pmStart;
    }

    /**
     * 设置：下午下班时间
     */
    public void setPmEnd(String pmEnd) {
        this.pmEnd = pmEnd;
    }

    /**
     * 获取：下午下班时间
     */
    public String getPmEnd() {
        return pmEnd;
    }
}
