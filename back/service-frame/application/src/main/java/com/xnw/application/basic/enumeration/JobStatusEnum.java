package com.xnw.application.basic.enumeration;


public enum JobStatusEnum {

	INPROGRESS(1,"进行中"),
	STOP(0,"停止中");

	private final int code;
    private final String value;
    private JobStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }
	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
}
