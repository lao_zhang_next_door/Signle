package com.xnw.application.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xnw.frame.basic.base.Base;
import com.xnw.frame.basic.base.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * <p>Title: AppShortmessage</p>
 * <p>Description:</p>
 *
 * @author
 */

public class AppShortMessage extends BaseEntity {

    /**
     * @param init true则有默认值
     */
    public AppShortMessage(boolean init) {
        super(init);
    }

    public AppShortMessage(String rowGuid) {
        super(rowGuid);
    }

    public AppShortMessage() {
    }

    /**
     * 短信内容
     **/
    private String smContent;
    /**
     * 收件人
     **/
    private String smReceivePeople;
    /**
     * 发件人
     **/
    private String smSendPeople;
    /**
     * 收件人手机号
     **/
    private String smReceiveMobile;
    /**
     * 是否已发送
     **/
    private Integer isSend;
    /**
     * 发送时间
     **/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime smSendTime;
    private String frameKey;

    /**
     * 是否立即发送
     */
    private Integer isSendNow;

    /**
     * 模板id
     */
    private Long tplId;

    /**
     * 模板对应键值对
     */
    private String tplValue;

    /**
     * 失败原因
     */
    private String failureReason;

    /**
     * 失败code
     */
    private Integer failureCode;

//	/**
//	 * 发送方式对应shortmessage 四个方法名
//	 */
//	public String sendType;

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public Integer getFailureCode() {
        return failureCode;
    }

    public void setFailureCode(Integer failureCode) {
        this.failureCode = failureCode;
    }

    public Integer getIsSendNow() {
        return isSendNow;
    }

    public Long getTplId() {
        return tplId;
    }

    public void setTplId(Long tplId) {
        this.tplId = tplId;
    }

    public String getTplValue() {
        return tplValue;
    }

    public void setTplValue(String tplValue) {
        this.tplValue = tplValue;
    }

    public void setIsSendNow(Integer isSendNow) {
        this.isSendNow = isSendNow;
    }

    /**
     * 设置：短信内容
     */
    public void setSmContent(String smContent) {
        this.smContent = smContent;
    }

    /**
     * 获取：短信内容
     */
    public String getSmContent() {
        return smContent;
    }

    /**
     * 设置：收件人
     */
    public void setSmReceivePeople(String smReceivePeople) {
        this.smReceivePeople = smReceivePeople;
    }

    /**
     * 获取：收件人
     */
    public String getSmReceivePeople() {
        return smReceivePeople;
    }

    /**
     * 设置：发件人
     */
    public void setSmSendPeople(String smSendPeople) {
        this.smSendPeople = smSendPeople;
    }

    /**
     * 获取：发件人
     */

    public String getSmSendPeople() {
        return smSendPeople;
    }

    /**
     * 设置：收件人手机号
     */
    public void setSmReceiveMobile(String smReceiveMobile) {
        this.smReceiveMobile = smReceiveMobile;
    }

    /**
     * 获取：收件人手机号
     */

    public String getSmReceiveMobile() {
        return smReceiveMobile;
    }

    /**
     * 设置：是否已发送
     */
    public void setIsSend(Integer isSend) {
        this.isSend = isSend;
    }

    /**
     * 获取：是否已发送
     */

    public Integer getIsSend() {
        return isSend;
    }

    /**
     * 设置：发送时间
     */
    public void setSmSendTime(LocalDateTime smSendTime) {
        this.smSendTime = smSendTime;
    }

    /**
     * 获取：发送时间
     */

    public LocalDateTime getSmSendTime() {
        return smSendTime;
    }

    public String getFrameKey() {
        return frameKey;
    }

    public void setFrameKey(String frameKey) {
        this.frameKey = frameKey;
    }
}
