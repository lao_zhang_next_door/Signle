package com.xnw.application.dao;

import com.xnw.application.entity.AppWorkCalendar;
import com.xnw.frame.basic.base.BaseDao;

/**
 * 
 * 
 * @author 
 * @date 2020-07-03 14:42:24
 */
public interface AppWorkCalendarDao extends BaseDao<AppWorkCalendar> {

}
