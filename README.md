
# 单体服务应用+vue
登录名：admin
登录密码：xnw_2022
#### 介绍
back 后台代码
front 前端代码
mobile-front 移动端代码

#### 软件架构
软件架构说明
后台集成 springboot + redis + rabbitMq + mysql + mybatis
前端集成 element-ui + vue2 + vuetify +vuecli
移动端集成 ts + vue3 + vant + vite


#### 安装教程

1.  后端导入数据库，配置相关参数启动即可
2.  前端 npm install 后 npm run dev 启动 (若install报错，后缀请加上 --force 或 --legacy-peer-deps），可选执行npm audit fix -force
3.  移动端 npm install 后 vite 启动

#### 使用说明

1.后台目录信息：
src - main - 
        | - application 系统应用

        | - captcha 图形验证码

        | - frame 基础框架应用

        | - quartz 定时任务

        | - rabbitMq 队列应用

        | - redis 缓存应用

 resources - 
        | - captcha 图形验证码图片资源(新项目记得删除打包jar文件下的)

        | - generator 文件生成

        | - mapper mybatis文件(新项目记得复制到文件夹下)

        | - META-INF 便签文件

2.  前端目录信息：
src - 
      | - api axios请求接口

      | - assets 静态图片资源

      | - common 通用方法

      | - components 框架通用组件

      | - icons svg图标资源

      | - plugins 框架中资源解耦引用

      | - router vue路由

      | - store vue信息托管

      | - style 全局样式

      | - views 框架页面

            | - dashboard 登录后主页

            | - frame 框架功能页面

            | - home 框架组件

            | - information 信息发布

            | - login 登录页

2.  移动端端目录信息：
src - 
      | - assets 静态图片资源

      | - components 框架通用组件

      | - layout 通用主页页面

      | - model ts强类型声明

      | - pages 页面

      | - plugins 框架中资源解耦引用

      | - router vue路由

      | - service api axois服务

      | - store vue信息托管

      | - style 全局样式

      | - types ts声明文件

      | - utils 全局方法


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md

![如图](doc/cat.jpg)