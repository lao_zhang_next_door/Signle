import { App } from 'vue'
import { vantPlugins } from '../vant/vant'

export const setupPlugins = (app: App) => {
  app.use(vantPlugins)
}
