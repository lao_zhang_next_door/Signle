import { createApp } from 'vue'
import App from './App.vue'
// 引入状态管理
import { setupStore } from './store'
// 初始化多语言
import { setupI18n } from '@/plugins/vueI18n'
import { setupPlugins } from './plugins/vant'
// 路由
import { setupRouter } from './router'
//xss过滤器
import VueDOMPurifyHTML from 'vue-dompurify-html'

import 'vant/lib/index.css'
import 'animate.css'
import 'amfe-flexible'
import '@/styles/index.scss'
import '@/router/router-guards'

const setupAll = async () => {
  const app = createApp(App)
  await setupI18n(app)
  setupPlugins(app)
  setupStore(app)
  app.use(VueDOMPurifyHTML)
  setupRouter(app)
  app.mount('#app')
}

setupAll()
