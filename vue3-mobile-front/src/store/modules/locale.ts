import { defineStore } from 'pinia'
import { store } from '../index'
import { useCache } from '@/hooks/useCache'
import { localeModules, elLocaleMap } from '@/config/app/locale'
import type { LocaleState } from '@/config/app/locale'

const { wsCache } = useCache()

export const useLocaleStore = defineStore({
  id: 'locales',
  persist: {
    storage: sessionStorage,
    key: 'locales'
  },
  state: (): LocaleState => localeModules,
  getters: {
    getCurrentLocale(): LocaleDropdownType {
      return this.currentLocale
    },
    getLocaleMap(): LocaleDropdownType[] {
      return this.localeMap
    }
  },
  actions: {
    setCurrentLocale(localeMap: LocaleDropdownType) {
      // this.locale = Object.assign(this.locale, localeMap)
      this.currentLocale.lang = localeMap?.lang
      this.currentLocale.elLocale = elLocaleMap[localeMap?.lang]
      wsCache.set('lang', localeMap?.lang)
    }
  }
})

export const useLocaleStoreWithOut = () => {
  return useLocaleStore(store)
}
