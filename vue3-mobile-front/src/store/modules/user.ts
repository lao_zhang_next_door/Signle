import { defineStore } from 'pinia'
import { removeAccessToken } from '@/utils/auth'
import { useCache } from '@/hooks/useCache'
import { useAppStore } from '@/store/modules/app'
import { getUserInfo, userLogout } from '@/api/frame/frame-login'

const { wsCache } = useCache()
interface UserState {
  token?: string
  name: string
  avatar?: string
  roleNameList?: Array<string>
  roleGuidList?: Array<string>
  deptGuid: string
  userName: string
  userGuid: string
  deptName: string
  mobile?: string
  loginId: string
  headimgGuid?: string
  headimgGuidVal?: string
  openid?: string
}

export const useUserStore = defineStore({
  id: 'user',
  persist: {
    key: 'user',
    storage: sessionStorage
  },
  state: (): UserState => {
    return {
      token: '',
      name: '',
      avatar: '',
      roleNameList: [],
      roleGuidList: [],
      userName: '',
      userGuid: '',
      deptName: '',
      deptGuid: '',
      mobile: '',
      loginId: '',
      headimgGuid: '',
      headimgGuidVal: '',
      openid: ''
    }
  },
  getters: {
    getRoleNameList(): Array<string> | undefined {
      return this.roleNameList
    }
  },
  actions: {
    setRoleNameList(roleNameList: Array<string> | undefined) {
      this.roleNameList = roleNameList
    },
    setCurrentUserInfo(data: FrameUserVo) {
      Object.keys(this.$state).forEach((key) => (this.$state[key] = data[key]))
    },
    getCurrentUserInfo() {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then((res) => {
            console.log(res.data)
            if (!res.data) {
              reject('Verification failed, please Login again.')
            }
            const { wsCache } = useCache()
            const appStore = useAppStore()
            const { roleNameList, userName } = res.data
            // roles must be a non-empty array
            if (!roleNameList || roleNameList.length <= 0) {
              reject('getInfo: roleNames must be a non-null array!')
            }
            Object.keys(this.$state).forEach((key) => (this.$state[key] = res.data[key]))
            wsCache.set(appStore.getUserInfo, res.data)
            // this.roles = roleNameList
            // this.rolesGuids = roleGuidList
            // this.name = userName
            // this.userInfo = data
            // commit('SET_AVATAR', avatar)
            resolve(res.data)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    logOut() {
      return new Promise((resolve) => {
        console.log('用户登出...')
        wsCache.delete('userInfo')
        wsCache.delete('user')
        userLogout().then(() => {
          Object.keys(this).forEach((key) => (this[key] = ''))
          //this.userInfo = {}
          removeAccessToken()
        })
        resolve(1)
      })
    }
  }
})
