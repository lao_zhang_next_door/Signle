import { defineStore } from 'pinia'
import { store } from '../index'

export interface RouterDataState {
  data: any
  action: string
}

export const useRouterDataStore = defineStore({
  id: 'routerData',
  persist: {
    storage: sessionStorage,
    key: 'routerData'
  },
  state: (): RouterDataState => ({
    data: null,
    action: ''
  }),
  getters: {
    getRouterData(): any {
      return this.data
    },
    getAction(): string {
      return this.action
    }
  },
  actions: {
    setRouterData(data: any): void {
      this.data = data
    },
    setAction(action: string): void {
      this.action = action
    }
  }
})

export const useRouterDataStoreWithOut = () => {
  return useRouterDataStore(store)
}
