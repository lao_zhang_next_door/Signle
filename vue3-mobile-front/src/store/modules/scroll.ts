import { defineStore } from 'pinia'
import { store } from '../index'
import { useCache } from '@/hooks/useCache'

const { wsCache } = useCache()
export interface ScrollState {
  scrollLocation: number
}

export const useScrollStore = defineStore({
  id: 'scrollLocation',
  persist: {
    storage: sessionStorage,
    key: 'scrollLocation'
  },
  state: (): ScrollState => ({
    scrollLocation: 0
  }),
  // persist: {
  //   enabled: true,
  // },
  getters: {
    getScrollLocation(): number {
      return this.scrollLocation
      // return wsCache.get('scrollLocation')
    }
  },
  actions: {
    setScrollLocation(scrollLocation: number): void {
      this.scrollLocation = scrollLocation
      // wsCache.set('scrollLocation', scrollLocation)
      console.log('scrollLocation:' + scrollLocation)
    }
  }
})

export const useScrollStoreWithOut = () => {
  return useScrollStore(store)
}
