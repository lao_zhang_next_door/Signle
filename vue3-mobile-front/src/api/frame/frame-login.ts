import request from '@/config/axios'
import CryptoUtil from '@/utils/crypto'

const client_id = import.meta.env.VITE_CLIENT_ID
const client_secret = import.meta.env.VITE_CLIENT_SECRET
const grant_type = import.meta.env.VITE_GRANT_TYPE
/**
 * 用户登录
 * @param FrameLogin 参数
 * @returns list
 */
export const userLogin = (data: UserLoginType) => {
  data.password = CryptoUtil.encryptMD5(data.password)
  data.grant_type = grant_type
  data.client_id = client_id
  data.client_secret = client_secret
  return request.post({ url: 'frame/auth/login', data, needRSA: true })
}

/**
 * 获取当前用户信息
 * @returns info
 */
export const getUserInfo = () => {
  return request.get({ url: 'frame/FrameUser/getUserInfo' })
}

/**
 * 当前用户退出系统
 * @returns info
 */
export const userLogout = () => {
  return request.get({ url: 'frame/auth/logout' })
}

/**
 * 用户刷新令牌
 * @returns info
 */
export const userRefreshToken = (refreshToken: string) => {
  return request.post({ url: 'frame/auth/refresh-token?refreshToken=' + refreshToken })
}
