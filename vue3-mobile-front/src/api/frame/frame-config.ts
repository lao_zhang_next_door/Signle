import request from '@/config/axios'

/**
 * 获取配置项参数
 * @param codeName 配置项名称
 * @returns list
 */
export const getFrameConfigValue = (params: FrameConfigName) => {
  return request.get({
    url: 'frame/FrameConfig/getFrameConfigValue',
    params
  })
}

/**
 * 获取配置项列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameConfigList = (data: any) => {
  return request.post({ url: 'frame/FrameConfig/listData', data })
}

/**
 * 获取是否启用验证码信息
 * @returns boolean
 */
export const checkCaptchaEnable = () => {
  return request.get({
    url: '/frame/FrameConfig/checkCaptchaEnable'
  })
}
