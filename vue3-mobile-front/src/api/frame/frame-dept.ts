import request from '@/config/axios'

/**
 * 部门数据列表
 * @param any 参数
 * @returns list
 */
export const listData = (data: any) => {
  return request.post({ url: 'frame/FrameDept/listData', data })
}

/**
 * 新增部门
 * @returns void
 */
export const addData = (data: FrameDept) => {
  return request.post({
    url: 'frame/FrameDept/add',
    data
  })
}

/**
 * 修改部门
 * @returns void
 */
export const updateData = (data: FrameDept) => {
  return request.put({
    url: 'frame/FrameDept/update',
    data
  })
}

/**
 * 获取树数据
 * @returns FrameDept
 */
export const deptTreeData = (data: FrameDeptTree) => {
  return request.post({
    url: 'frame/FrameDept/allTreeData',
    data
  })
}

/**
 * 根据rowGuid 联查查询上级部门
 * @returns FrameDept
 */
export const selectParentCode = (params: any) => {
  return request.get({
    url: 'frame/FrameDept/selectParentCode',
    params
  })
}
