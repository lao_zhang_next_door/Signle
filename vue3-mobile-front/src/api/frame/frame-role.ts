import request from '@/config/axios'

/**
 * 获取角色列表
 * @param data
 * @returns list
 */
export const getFrameRoleList = (data: FrameSearch) => {
  return request.post({ url: 'frame/FrameRole/listData', data })
}

/**
 * 获取所有角色
 * @returns void
 */
export const getAllRole = () => {
  return request.get({
    url: 'frame/FrameRole/getAllRole'
  })
}
