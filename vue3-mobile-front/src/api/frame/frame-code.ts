import request from '@/config/axios'

/**
 * 获取代码项参数
 * @param codeName 代码项名称
 * @returns list
 */
export const getFrameCodeValue = (params: FrameCodeName) => {
  return request.get({
    url: 'frame/FrameCode/selectCacheByCodeName',
    params
  })
}

/**
 * 获取代码列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameCodeList = (data: FrameSearch) => {
  return request.post({ url: 'frame/FrameCode/listData', data })
}
