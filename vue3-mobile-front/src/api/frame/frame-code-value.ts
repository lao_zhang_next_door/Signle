import request from '@/config/axios'

/**
 * 获取代码值列表
 * @param codeName 代码名称
 * @returns list
 */
export const getFrameCodeValueList = (data: any) => {
  return request.post({ url: 'frame/FrameCodeValue/listData', data })
}
