import request from '@/config/axios'

const appId = import.meta.env.VITE_APP_ID
/**
 * 获取微信公众号用户信息
 * @returns info
 */
export const getUserWxData = (code: string) => {
  return request.get({
    url: '/wx/redirect/' + appId + '/getUserWxData?code=' + code
  })
}

/**
 * 获取微信签名信息
 * @returns info
 */
export const getJsTicket = (pageUrl: string) => {
  return request.get({
    url: '/wx/jsapi/' + appId + '/getJsapiTicket?pageUrl=' + pageUrl
  })
}
