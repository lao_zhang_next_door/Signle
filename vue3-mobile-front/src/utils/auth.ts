import Cookies from 'js-cookie'

const accessTokenKey = 'xnw_access_token'
const refreshTokenKey = 'xnw_refresh_token'

/**
 * 获取accessToken
 * @returns
 */
export function getAccessToken() {
  return Cookies.get(accessTokenKey)
}

/**
 * 设置accessToken
 * @param accessToken
 * @returns
 */
export function setAccessToken(accessToken: string | undefined) {
  return Cookies.set(accessTokenKey, accessToken)
}

/**
 * 设置refreshToken
 * @param refreshToken
 * @returns
 */
export function setRefreshToken(refreshToken: string) {
  return Cookies.set(refreshTokenKey, refreshToken)
}

/**
 * 获取refreshToken
 * @returns
 */
export function getRefreshToken() {
  return Cookies.get(refreshTokenKey)
}

/**
 * 删除accessToken
 * @returns
 */
export function removeAccessToken() {
  return Cookies.remove(accessTokenKey)
}

/**
 * 删除refreshToken
 * @returns
 */
export function removeRefreshToken() {
  return Cookies.remove(refreshTokenKey)
}
