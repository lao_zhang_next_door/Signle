import type {
  RouteLocationNormalized,
  RouteLocationNormalizedLoaded,
  RouteRecordNormalized
} from 'vue-router'
import { isUrl } from './is'
import { returnRefreshPage } from '@/router/index'

export const getRawRoute = (route: RouteLocationNormalized): RouteLocationNormalized => {
  if (!route) {
    return route
  }
  const { matched, ...opt } = route
  return {
    ...opt,
    matched: (matched
      ? matched.map((item) => ({
          meta: item.meta,
          name: item.name,
          path: item.path
        }))
      : undefined) as RouteRecordNormalized[]
  }
}

export const pathResolve = (parentPath: string, path: string) => {
  if (isUrl(path)) {
    return path
  }
  const childPath = path.startsWith('/') || !path ? path : `/${path}`
  return `${parentPath}${childPath}`.replace(/\/\//g, '/')
}

/**
 * 是否删除上一个缓存的页面
 * @param newRouteData
 * @return boolean
 */
export const removeLastCachePage = (newRouteData: RouteLocationNormalizedLoaded) => {
  if (returnRefreshPage.indexOf(newRouteData?.name as string) !== -1) {
    //如果从页面离开
    return true
  }
  return false
}
