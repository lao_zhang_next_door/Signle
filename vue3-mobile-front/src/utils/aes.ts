import CryptoJS from 'crypto-js'

export const defaultKey = 'sinoway1234'

export default {
  // 加密
  encrypt(word: string, keyStr: string) {
    keyStr = keyStr || defaultKey // 判断是否存在ksy，不存在就用定义好的key
    const mode = CryptoJS.mode.ECB
    const pad = CryptoJS.pad.Pkcs7
    const keyHex = CryptoJS.enc.Utf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    const wordHex = CryptoJS.enc.Utf8.parse(word)
    const encrypted = CryptoJS.AES.encrypt(wordHex, keyHex, {
      mode: mode,
      padding: pad
    })
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64)
  },
  // 解密
  decrypt(word: string, keyStr: string) {
    keyStr = keyStr || defaultKey
    const mode = CryptoJS.mode.ECB
    const pad = CryptoJS.pad.Pkcs7
    const keyHex = CryptoJS.enc.Utf8.parse(keyStr)
    // var ivHex = CryptoJS.enc.Utf8.parse(this.iv);
    const decrypted = CryptoJS.AES.decrypt(word, keyHex, {
      mode: mode,
      padding: pad
    })
    return CryptoJS.enc.Utf8.stringify(decrypted).toString()
  },

  // 加密
  encryptMD5(word: string) {
    return CryptoJS.MD5(word).toString().toLowerCase()
  }
}
