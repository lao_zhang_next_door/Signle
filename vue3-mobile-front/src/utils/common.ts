import type { App, Plugin } from 'vue'
/**
 * 获取url链接中参数
 * @param name 参数名称
 * @param url 链接
 * @returns 值
 */
export function getUrlKey(name: string, url: string): string {
  const reg = new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || ['', '']
  return decodeURIComponent(reg[1].replace(/\+/g, '%20')) || 'null'
}

/**
 *
 * @param component 需要注册的组件
 * @param alias 组件别名
 * @returns any
 */
export const withInstall = <T>(component: T, alias?: string) => {
  const comp = component as any
  comp.install = (app: App) => {
    app.component(comp.name || comp.displayName, component)
    if (alias) {
      app.config.globalProperties[alias] = component
    }
  }
  return component as T & Plugin
}

/**
 * @param str 需要转下划线的驼峰字符串
 * @returns 字符串下划线
 */
export const humpToUnderline = (str: string): string => {
  return str.replace(/([A-Z])/g, '-$1').toLowerCase()
}

/**
 * @param str 需要转驼峰的下划线字符串
 * @returns 字符串驼峰
 */
export const underlineToHump = (str: string): string => {
  return str.replace(/\\-(\w)/g, (_, letter: string) => {
    return letter.toUpperCase()
  })
}

export const setCssVar = (prop: string, val: any, dom = document.documentElement) => {
  dom.style.setProperty(prop, val)
}

/**
 * 查找数组对象的某个下标
 * @param {Array} ary 查找的数组
 * @param {Functon} fn 判断的方法
 */
// eslint-disable-next-line
export const findIndex = <T = Recordable>(ary: Array<T>, fn: Fn): number => {
  if (ary.findIndex) {
    return ary.findIndex(fn)
  }
  let index = -1
  ary.some((item: T, i: number, ary: Array<T>) => {
    const ret: T = fn(item, i, ary)
    if (ret) {
      index = i
      return ret
    }
  })
  return index
}

export const trim = (str: string) => {
  return str.replace(/(^\s*)|(\s*$)/g, '')
}

/**
 * @param {Date | number | string} time 需要转换的时间
 * @param {String} fmt 需要转换的格式 如 yyyy-MM-dd、yyyy-MM-dd HH:mm:ss
 */
export function formatTime(time: Date | number | string, fmt: string) {
  if (!time) {
    return ''
  }

  const date = new Date(time)
  const o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'H+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
    'q+': Math.floor((date.getMonth() + 3) / 3),
    S: date.getMilliseconds()
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (const k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
      )
    }
  }
  return fmt
}

/**
 * 生成随机字符串
 */
export function toAnyString() {
  const str: string = 'xxxxx-xxxxx-4xxxx-yxxxx-xxxxx'.replace(/[xy]/g, (c: string) => {
    const r: number = (Math.random() * 16) | 0
    const v: number = c === 'x' ? r : (r & 0x3) | 0x8
    return v.toString()
  })
  return str
}

/**
 * 获取本地图片文件
 * @param name 图片文件名
 * @returns imgUrl
 */
export function getAssetsImages(name) {
  return new URL(`/src/assets/images/${name}`, import.meta.url).href
}

/**
 * 获取本地ICON文件
 * @param name 图片文件名
 * @returns imgUrl
 */
export function getAssetsIcon(name) {
  return new URL(`/src/assets/icon/${name}`, import.meta.url).href
}

/**
 * 获取本地Svg文件
 * @param name 图片文件名
 * @returns imgUrl
 */
export function getLocalSvg(name) {
  return new URL(`/src/assets/svgs/${name}`, import.meta.url).href
}

/**
 * 检查头像文件是否存在，没有则默认
 * @param value 图片文件Url
 * @returns imgUrl
 */
export function checkAvatarExist(value: string | undefined | null) {
  if (value === '' || value === null) {
    return new URL(`/src/assets/icon/default-head.png`, import.meta.url).href
  }
  return value
}

/**
 * 检查图片是否存在，没有则默认
 * @param value 图片文件Url
 * @returns imgUrl
 */
export function checkShowImageExist(defaultImage: string, value: string) {
  if (value === '' || value === null) {
    return new URL(`/src/assets/images/` + `${defaultImage}`, import.meta.url).href
  }
  return value
}

export function triggerADownload(url, fileName) {
  // console.log(url)
  // url.then(res => {
  // console.log(res)
  const a = document.createElement('a')
  document.body.appendChild(a)
  a.href = url
  a.download = fileName
  a.click()
  document.body.removeChild(a)
  // })
}

export async function createBlobOrDataUrl(url) {
  const response = await fetch(url)
  const blob = await response.blob()
  return URL.createObjectURL(blob)
}

/**
 * 获取附件文件类型
 * @param fileName
 * @returns fileType
 */
export const getFileType = (fileName: string) => {
  let suffix = ''
  // 后缀获取
  let result: string | undefined = ''
  // 获取类型结果
  if (fileName) {
    const flieArr = fileName.split('.')
    // 根据.分割数组
    suffix = flieArr[flieArr.length - 1]
    // 取最后一个
  }
  if (!suffix) {
    return false
  } // fileName无后缀返回false
  suffix = suffix.toLocaleLowerCase()
  // 将后缀所有字母改为小写方便操作
  // 匹配图片
  const imgList = ['png', 'jpg', 'jpeg', 'bmp', 'gif']
  // 图片格式
  result = imgList.find((item) => item === suffix)
  if (result) {
    return 'image'
  }
  // 匹配txt
  const txtList = ['txt']
  result = txtList.find((item) => item === suffix)
  if (result) {
    return 'txt'
  }
  // 匹配excel
  const excelList = ['xls', 'xlsx']
  result = excelList.find((item) => item === suffix)
  if (result) {
    return 'excel'
  }
  // 匹配word
  const wordList = ['doc', 'docx']
  result = wordList.find((item) => item === suffix)
  if (result) {
    return 'word'
  }
  // 匹配pdf
  const pdfList = ['pdf']
  result = pdfList.find((item) => item === suffix)
  if (result) {
    return 'pdf'
  }
  // 匹配ppt
  const pptList = ['ppt', 'pptx']
  result = pptList.find((item) => item === suffix)
  if (result) {
    return 'ppt'
  }
  // 匹配zip
  const zipList = ['rar', 'zip', '7z']
  result = zipList.find((item) => item === suffix)
  if (result) {
    return 'zip'
  }
  // 匹配视频
  const videoList = ['mp4', 'm2v', 'mkv', 'rmvb', 'wmv', 'avi', 'flv', 'mov', 'm4v']
  result = videoList.find((item) => item === suffix)
  if (result) {
    return 'video'
  }
  // 匹配音频
  const radioList = ['mp3', 'wav', 'wmv']
  result = radioList.find((item) => item === suffix)
  if (result) {
    return 'radio'
  }
  // 其他文件类型
  return 'other'
}

/**
 * 根据文件类型设置图片
 * @param fileName
 * @param fileUrl
 * @returns
 */
export const setFileCoverPicture = (fileName: string, fileUrl: string) => {
  const fileType = getFileType(fileName)
  let reviewImage = ''
  switch (fileType) {
    case 'image':
      reviewImage = fileUrl
      break
    case 'excel':
      reviewImage = getAssetsIcon('EXCEL.png')
      break
    case 'word':
      reviewImage = getAssetsIcon('WORD.png')
      break
    case 'pdf':
      reviewImage = getAssetsIcon('PDF.png')
      break
    case 'zip':
      reviewImage = getAssetsIcon('ZIP.png')
      break
    default:
      reviewImage = getAssetsIcon('OTHER.png')
      break
  }
  return reviewImage
}

/**
 * 根据状态设置tag颜色
 * @param item
 * @returns
 */
export const initTagColor = (item: MissionRelease | MissionExecution) => {
  let backColor = ''
  let textColor = ''
  switch (item.executeStatus) {
    case 0:
      //未完成
      backColor = '#1989fa'
      textColor = 'white'
      break
    case 1:
      //任务完成
      backColor = '#07c160'
      textColor = 'white'
      break
    case 2:
      //待审核
      backColor = 'orange'
      textColor = 'white'
      break
    case 3:
      //审核不通过
      backColor = 'red'
      textColor = 'white'
      break
    default:
      break
  }
  return { backColor: backColor, textColor: textColor }
}

/**
 * 数字转中文
 * @param digit
 * @returns ChineseDigit
 */
export const digitToChinese = (digit: number): string => {
  const chnNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
  const chnNumUnit = ['', '十', '百', '千']
  let tmp = ''
  let chnString = ''
  let zero = true
  let unitIndex = 0
  let isTen = false
  if (digit > 9 && digit < 20) {
    isTen = true
  }
  while (digit > 0) {
    const num = digit % 10
    if (num === 0) {
      if (!zero) {
        zero = true
        chnString = chnNum[num] + chnString
      }
    } else {
      zero = false
      if (isTen && unitIndex === 1) {
        tmp = ''
      } else {
        tmp = chnNum[num]
      }
      tmp += chnNumUnit[unitIndex]
      chnString = tmp + chnString
    }
    unitIndex++
    digit = Math.floor(digit / 10)
  }
  return chnString
}
