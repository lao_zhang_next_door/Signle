import moment from 'moment'

/**
 * 日期格式转换
 * @param date Fri Apr 08 2022 00:00:00 GMT+0800
 * @param type 类型
 * @returns Date
 */
export function getDateFromStr(dateStr: string | Date, type?: string) {
  moment.locale('zh-cn')
  let format = 'YYYY/MM/DD'
  switch (type) {
    case 'date':
      format = 'YYYY/MM/DD'
      break
    case 'month-day':
    case 'year-month':
      format = 'YYYY/MM/DD'
      break
    case 'time':
      format = 'HH:mm:ss'
      break
    case 'datetime':
      format = 'YYYY/MM/DD HH:mm:ss'
      break
    case 'datehour':
      format = 'YYYY/MM/DD HH:mm'
      break
    default:
      break
  }
  const momentDate = moment(dateStr).format(format)
  return momentDate
}

/**
 * 获取月份的月末日期
 * @param date
 */
export function getMonthLastDay(date?: Date) {
  if (date === undefined || date === null) {
    date = new Date()
  }
  const nextMonthFirstDay = new Date(date.getFullYear(), date.getMonth() + 1, 1)
  nextMonthFirstDay.setDate(nextMonthFirstDay.getDate() - 1)
  //根据下个月第一天，再减去一天，获得当月月末
  const end = getDateFromStr(nextMonthFirstDay)
  return end
}

/**
 * 获取周的开始或结束日期
 * @param type 为字符串类型，有两种选择，"start"代表开始,"end"代表结束
 * @param dates 为数字类型，不传或0代表本周，-1代表上周，1代表下周
 * @returns string
 */
export function getStartOrEndDay(type: string, dates: number) {
  const now = new Date()
  const nowTime = now.getTime()
  const day = now.getDay()
  const longTime = 24 * 60 * 60 * 1000
  const n = longTime * 7 * (dates || 0)
  let dd = 0
  if (type === 'start') {
    dd = nowTime - (day - 1) * longTime + n
  }
  if (type === 'end') {
    dd = nowTime + (7 - day) * longTime + n
  }
  const getDay = new Date(dd)
  const y = getDay.getFullYear()
  const m = getDay.getMonth() + 1
  const d = getDay.getDate()
  const mStr = m < 10 ? '0' + m : m
  const dStr = d < 10 ? '0' + d : d
  return y + '/' + mStr + '/' + dStr
}

/**
 * 格式化开始与结束日期
 * @param startTime YYYY/MM/DD
 * @param endTime YYYY/MM/DD
 * @returns string
 */
export function formatStartOrEndDay(startTime: string, endTime: string) {
  const startArray = startTime.split('/')
  const endArray = endTime.split('/')
  const startStr = startArray[0] + '年/' + startArray[1] + '月/' + startArray[2] + '日'
  const endStr = endArray[1] + '月/' + endArray[2] + '日'
  return startStr + '~' + endStr
}
