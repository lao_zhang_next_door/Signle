import router from './index'
import { useCache } from '@/hooks/useCache'
import { useNProgress } from '@/hooks/useNProgress'
import { useScrollStore } from '@/store/modules/scroll'

const { start, done } = useNProgress()
const { wsCache } = useCache()

router.beforeEach((to, _from, next) => {
  start()
  document.title = (to?.meta?.title as string) || document.title
  if (_from?.meta?.scrollTo) {
    const useScroll = useScrollStore()
    const top = document.documentElement.scrollTop || document.body.scrollTop || window.pageYOffset
    // wsCache.set('scrollLocation', top)
    useScroll.setScrollLocation(top)
  }
  next()
})
router.afterEach((to, from) => {
  // 如果进入后的页面是要滚动到顶部，则设置scrollTop = 0
  // 否则从vuex中读取上次离开本页面记住的高度，恢复它
  done()
  if (to?.meta?.scrollTo === false) {
    setTimeout(() => {
      document.documentElement.scrollTop = 0
      document.body.scrollTop = 0
    }, 10)
  } else {
    const useScroll = useScrollStore()
    setTimeout(() => {
      document.documentElement.scrollTop = useScroll.getScrollLocation
      document.body.scrollTop = useScroll.getScrollLocation
      // document.documentElement.scrollTop = wsCache.get('scrollLocation')
      // document.body.scrollTop = wsCache.get('scrollLocation')
    }, 10)
  }
})
