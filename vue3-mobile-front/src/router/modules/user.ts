//用户主页模块
export const userMenuRouter: AppRouteRecordRaw[] = [
  {
    path: '/userHome',
    name: 'UserHome',
    component: () => import('@/pages/User/Main/UserHome.vue'),
    meta: {
      title: '首页',
      icon: 'wap-home',
      noCache: false,
      showTabber: true
    }
  },
  {
    path: '/userNews',
    name: 'UserNews',
    component: () => import('@/pages/User/Main/UserNews.vue'),
    meta: {
      title: '新闻',
      icon: 'comment',
      noCache: false,
      showTabber: true
    }
  },
  {
    path: '/UserInfos',
    name: 'UserInfos',
    component: () => import('@/pages/User/Main/UserInfos.vue'),
    meta: {
      title: '动态',
      icon: 'todo-list',
      noCache: false,
      scrollTo: true,
      showTabber: true
    }
  },
  {
    path: '/userCenter',
    name: 'UserCenter',
    component: () => import('@/pages/User/Main/UserCenter.vue'),
    meta: {
      title: '个人中心',
      icon: 'manager',
      noCache: false,
      showTabber: true
    }
  }
]
