import { App } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import type { RouteRecordRaw } from 'vue-router'
import { useCache } from '@/hooks/useCache'
import { useScrollStore } from '@/store/modules/scroll'
import { userMenuRouter } from './modules/user'

const { wsCache } = useCache()
// 返回后需要刷新的页面
export const returnRefreshPage = ['Page1', 'Page2']

export const routes: AppRouteRecordRaw[] = [
  {
    path: '/',
    name: 'Layout1',
    redirect: { name: 'UserHome' },
    component: () => import('@/layout/default-index.vue'),
    meta: {
      title: '首页1',
      noCache: false
    },
    children: [...userMenuRouter]
  },
  {
    path: '/testPage',
    name: 'TestPage',
    // component: () => import('@/pages/home/test-page.vue'),
    component: () => import('@/pages/test-page.vue'),
    meta: {
      title: '测试页'
    }
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '账号登录页',
      icon: 'home-o',
      noCache: true
    },
    component: () => import('@/pages/Login/LoginPage.vue')
  },
  {
    path: '/openIdLogin',
    name: 'OpenIdLogin',
    meta: {
      title: '微信自动登录页',
      icon: 'home-o',
      noCache: true
    },
    component: () => import('@/pages/Login/OpenIdLogin.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  strict: true,
  routes: routes as RouteRecordRaw[],
  scrollBehavior(to, from, savedPosition) {
    // return 期望滚动到哪个的位置
    if (to.meta?.scrollTo) {
      const useScroll = useScrollStore()
      console.log('useScroll:' + useScroll.getScrollLocation)
      if (useScroll.getScrollLocation !== null) {
        const scrollLocation = useScroll.getScrollLocation
        return {
          top: scrollLocation,
          behavior: 'smooth'
        }
      }
    }
  }
})

export function setupRouter(app: App) {
  app.use(router)
  // 创建路由守卫
  //createRouterGuards(router)
}

export default router
