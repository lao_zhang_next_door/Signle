import { useCache } from '@/hooks/useCache'
import { Locale } from 'vant'
// 引入英文语言包
import enUS from 'vant/es/locale/lang/en-US'
import zhCN from 'vant/es/locale/lang/zh-CN'
const { wsCache } = useCache()

export const elLocaleMap = {
  'zh-CN': zhCN,
  en: enUS
}
export interface LocaleState {
  currentLocale: LocaleDropdownType
  localeMap: LocaleDropdownType[]
}

export const localeModules: LocaleState = {
  currentLocale: {
    lang: wsCache.get('lang') || 'zh-CN',
    elLocale: elLocaleMap[wsCache.get('lang') || 'zh-CN']
  },
  // 多语言
  localeMap: [
    {
      lang: 'zh-CN',
      name: '简体中文'
    },
    {
      lang: 'en',
      name: 'English'
    }
  ]
}
