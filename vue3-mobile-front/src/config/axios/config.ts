const config: {
  base_url: {
    base: string
    dev: string
    pro: string
    test: string
  }
  result_code: number | string
  default_headers: AxiosHeaders
  request_timeout: number
  //是否需要签名加密
  needSignature?: boolean
  //是否需要RSA加密
  needRSA?: boolean
  //需要忽略的提示
  ignoreMsgs?: string[]
  //后台返回的错误code信息
  errerCode: {
    bad_request: {
      code: number
      message: string
    }
    unauthorized: {
      code: number
      message: string
    }
    forbidden: {
      code: number
      message: string
    }
    server_error: {
      code: number
      message: string
    }
  }
} = {
  /**
   * api请求基础路径
   */
  base_url: {
    // 开发环境接口前缀
    base: '',

    // 打包开发环境接口前缀
    dev: '',

    // 打包生产环境接口前缀
    pro: '',

    // 打包测试环境接口前缀
    test: ''
  },

  /**
   * 接口成功返回状态码
   */
  result_code: 0,

  /**
   * 接口请求超时时间
   */
  request_timeout: 60000,

  /**
   * 默认接口请求类型
   * 可选值：application/x-www-form-urlencoded multipart/form-data
   */
  default_headers: 'application/json',
  //是否需要签名加密
  needSignature: false,
  //是否需要RSA加密
  needRSA: false,
  ignoreMsgs: ['无效的刷新令牌', '刷新令牌已过期'],
  errerCode: {
    bad_request: {
      code: 400,
      message: '请求参数不正确'
    },
    unauthorized: {
      code: 401,
      message: '账号未登录'
    },
    forbidden: {
      code: 403,
      message: '没有该操作权限'
    },
    server_error: {
      code: 500,
      message: '系统异常'
    }
  }
}

export { config }
