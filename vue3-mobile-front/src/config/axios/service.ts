import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios'
import qs from 'qs'
import { config } from './config'
import { showDialog, showNotify } from 'vant'
import {
  getAccessToken,
  removeAccessToken,
  getRefreshToken,
  setAccessToken,
  setRefreshToken
} from '@/utils/auth'
import CryptoUtil from '@/utils/crypto'
import { userRefreshToken } from '@/api/frame/frame-login'

const publicKey = import.meta.env.VITE_PUBLIC_KEY

const { result_code, ignoreMsgs, errerCode } = config

export const BASE_URL = import.meta.env.VITE_APP_BASE_URL
const TOKEN_NAME: string = import.meta.env.VITE_APP_TOKEN
//请求队列
let requestList: any[] = []
// 是否正在刷新中
let isRefreshToken = false

// 创建axios实例
const service: AxiosInstance = axios.create({
  // api 的 base_url
  baseURL: BASE_URL,
  // 请求超时时间
  timeout: config.request_timeout
})

// request拦截器
service.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (
      config.method === 'post' &&
      (config.headers as any)['Content-Type'] === 'application/x-www-form-urlencoded'
    ) {
      config.data = qs.stringify(config.data)
    }
    const accessToken = getAccessToken()
    if (accessToken) {
      config.headers![TOKEN_NAME] = 'Bearer ' + accessToken
    } else {
      config.headers![TOKEN_NAME] = 'null'
    }
    // get参数编码
    if (config.method === 'get' && config.params) {
      let url = config.url as string
      url += '?'
      const keys = Object.keys(config.params)
      // console.log(keys)
      for (const key of keys) {
        const value = config.params[key]
        if (value !== void 0 && value !== null) {
          // console.log('key-value:' + value)
          if (Object.prototype.toString.call(value) === '[object Object]') {
            for (const paramsKey of Object.keys(value)) {
              const params = key + '[' + paramsKey + ']'
              const subPart = encodeURIComponent(params) + '='
              url += subPart + encodeURIComponent(value[paramsKey]) + '&'
            }
          } else {
            url += `${key}=${encodeURIComponent(config.params[key])}&`
          }
        }
      }
      url = url.substring(0, url.length - 1)
      config.params = {}
      config.url = url
    }
    if (config.needRSA) {
      //若需RSA加密
      const RSAData = CryptoUtil.encodeRSA(JSON.stringify(config.data), publicKey)
      for (const key in config.data) {
        delete config.data[key]
      }
      config.data.data = RSAData
    }
    return config
  },
  (error: AxiosError) => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  (response: AxiosResponse<any>) => {
    const code = response.data.code
    if (response.config.responseType === 'blob') {
      // 如果是文件流，直接过
      return response
    }
    if (code === result_code || response.data.repCode === '0000') {
      return response.data
    }
    switch (code) {
      case errerCode.bad_request.code:
        showNotify({ type: 'danger', message: errerCode.bad_request.message })
        break
      case errerCode.forbidden.code:
        showNotify({ type: 'danger', message: errerCode.forbidden.message })
        break
      case errerCode.unauthorized.code:
        // ElMessage.error(errerCode.unauthorized.message)
        refreshUserToken(response)
        break
      case errerCode.server_error.code:
        showNotify({ type: 'danger', message: errerCode.server_error.message })
        break
      default:
        showNotify({ type: 'danger', message: response.data.msg })
        break
    }
    console.log(response)
    return response.data
  },
  (error: AxiosError) => {
    console.log(error) // for debug
    const message = (error.response?.data as any).message
    showNotify({ type: 'danger', message: message })
    return Promise.reject(error)
  }
)

const refreshUserToken = async (response: AxiosResponse<any>) => {
  if (!isRefreshToken) {
    isRefreshToken = true
    // 1. 如果获取不到刷新令牌，则只能执行登出操作
    // if (!getAccessToken()) {
    //   window.$loginOutFunction('登录状态已过期，您可以继续留在该页面，或者重新登录')
    // }
    // 2. 进行刷新访问令牌
    try {
      const refreshTokenRes = await userRefreshToken(getRefreshToken())
      console.log(refreshTokenRes)
      // 2.1 刷新成功，则回放队列的请求 + 当前请求
      setAccessToken(refreshTokenRes.data.accessToken)
      setRefreshToken(refreshTokenRes.data.refreshToken)
      requestList.forEach((cb) => cb())
      return service(response.config)
    } catch (e) {
      // 为什么需要 catch 异常呢？刷新失败时，请求因为 Promise.reject 触发异常。
      // 2.2 刷新失败，只回放队列的请求
      requestList.forEach((cb) => cb())
      // 提示是否要登出。即不回放当前请求！不然会形成递归
      // window.$loginOutFunction('登录状态已过期，您可以继续留在该页面，或者重新登录')
    } finally {
      requestList = []
      isRefreshToken = false
    }
  } else {
    // 添加到队列，等待刷新获取到新的令牌
    return new Promise((resolve) => {
      requestList.push(() => {
        resolve(service(response.config))
      })
    })
  }
}

export { service }
