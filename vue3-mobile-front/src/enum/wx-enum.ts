/**
 * 信息状态枚举
 */
export enum MessageStatusEnum {
  /**已读**/
  READ = 1,
  /**未读**/
  UNREAD = 0
}

/**
 * 附件accept类型枚举
 */
export enum AcceptTypeEnum {
  /**所有图片 */
  Images = 'image/*',
  /**word、excel、pdf、zip文档文件 */
  File = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,aplication/zip',
  /**所有视频 */
  Video = 'video/*'
}
