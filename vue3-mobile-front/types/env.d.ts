/// <reference types="vite/client" />

declare module '*.vue' {
  import { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface ImportMetaEnv {
  // 系统名称
  readonly VITE_NAME: string
  // 请求Url
  readonly VITE_APP_BASE_URL: string
  // 文件Url
  readonly VITE_APP_FILE_URL: string
  // app名称
  readonly VITE_APP_TITLE: string
  // 打包路径
  readonly VITE_BASE_PATH: string
  readonly VITE_DROP_DEBUGGER: string
  readonly VITE_DROP_CONSOLE: string
  readonly VITE_SOURCEMAP: string
  readonly VITE_OUT_DIR: string
}

declare global {
  interface ImportMeta {
    readonly env: ImportMetaEnv
  }
}
