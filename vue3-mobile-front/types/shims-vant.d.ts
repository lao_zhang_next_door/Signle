import { UploaderFileListItem } from 'vant'

declare module 'vant' {
  export interface UploaderFileListItem {
    /**
     * @description 附件Guid
     */
    rowGuid?: string
    /**
     * @description 文件名称
     */
    attachName?: string
  }
}
