declare interface Fn<T = any> {
  (...arg: T[]): T
}

declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>

declare type Nullable<T> = T | null

declare type Recordable<T = any, K = string> = Record<K extends null | undefined ? string : K, T>

declare type ComponentRef<T> = InstanceType<T>

declare type VantRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>

declare type LocaleType = 'zh-CN' | 'en'

declare type VantIsDark = false | true

declare type AxiosHeaders =
  | 'application/json'
  | 'application/x-www-form-urlencoded'
  | 'multipart/form-data'

declare type AxiosMethod = 'get' | 'post' | 'delete' | 'put'

declare type AxiosResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'

declare type AxiosConfig<T = Recordable, K = Recordable> = {
  params?: T
  data?: K
  url?: string
  method?: AxiosMethod
  headersType?: string
  responseType?: AxiosResponseType
}

declare interface SignatureParams {
  //数据
  data?: string
  //签名字符串
  signStr?: string
  //时间戳
  timestamp?: string
  //加密秘钥
  key?: string
  //随机字符串
  nonce?: string
}

declare interface Window {
  $loginOutFunction: any
  offsetWidth: any
  offsetHeight: any
}
