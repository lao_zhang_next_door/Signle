export interface CodeRadioProps {
  checked: string
  labelName: string
  codeName: string
}

interface SinglePickerProps {
  selectedValue?: string
  codeName: string
  show?: boolean
}
