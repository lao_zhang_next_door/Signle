import type { TimePickerProps, TimePickerColumnType, DatePickerColumnType } from 'vant'
export interface DateProps {
  dateValue?: string
  show?: boolean
  minDate?: Date
  maxDate?: Date
  title?: string
  columnsType?: DatePickerColumnType[]
}

export interface TimeProps {
  timeValue?: string
  show?: boolean
  columnsType?: TimePickerColumnType[]
}
