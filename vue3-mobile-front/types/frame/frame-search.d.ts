export interface FrameSearch {
  // 排序属性
  prop?: string[]
  // 排序方式
  order?: string[]
  // 当前页
  currentPage?: number
  // 页数
  pageSize?: number
  // 自定义参数
  params?: any
}
