import { FrameBase } from './frame-base'
export interface FrameUser {
  userName: string
  lastLoginTime?: string
  sex?: string
  gongHao?: string
  duty?: string
  tel?: string
  deptGuid: string
  mobile?: string
  loginId: string
  openId?: string
  themeGuid?: string
  headimgGuid?: string
  isLimit?: numebr
  status?: number
  statusText?: string
  email?: string
}
export interface FrameUserVo extends FrameUser {
  userGuid: string
  deptName: string
  deptGuid: string
  roleNameList?: string[]
  roleGuidList?: string[]
  token?: string
  headimgGuidVal?: string
  deptCode?: string
  openid?: string
}
export interface LoginData {
  userInfo: FrameUserVo
  code: number
  accessToken: string
  jti: string
  refreshToken: string
}
