import { FrameBase } from './frame-base'

export interface FrameDept extends FrameBase {
  // 部门编号
  deptCode: string
  // 部门名称
  deptName?: string
  // 父级编号
  parentDeptCode?: string
  // 部门简称
  shortName?: string
  // 部门编码
  ouCode?: string
  // 电话
  tel?: string
  // 传真
  fax?: string
  // 地址
  address?: string
  // 描述
  description?: string
  // 父级部门名称
  parentDeptName?: string
  // 父级guid
  parentDeptGuid?: string
}

export interface FrameDeptTree {
  frameDept: {
    parentGuid: string
  }
  needList?: boolean
  sort: {
    prop?: string[]
    // 排序方式
    order?: string[]
  }
}
