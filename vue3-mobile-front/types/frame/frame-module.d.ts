import { FrameBase } from './frame-base'
export interface FrameModule extends FrameBase {
  rowGuid: string
  sortSq?: number
  moduleName?: string
  moduleCode: string
  parentModuleGuid?: string
  parentModuleName?: string
  parentGuid?: string
  hasChild?: number
  moduleAddress?: string
  moduleLevel?: number
  smallIcon?: string
  bigIcon?: string
  target?: string
  isVisible?: string
  redirect?: string
  path?: string
  moduleType?: number
  menuType?: string
  perms?: string
  appId?: string
  openType?: string
}

export interface FrameModuleTree {
  frameModule: {
    parentGuid: string
    moduleType: number
  }
  sort: {
    prop?: string[]
    // 排序方式
    order?: string[]
  }
  needList?: boolean
}
