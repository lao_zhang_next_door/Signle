import { FrameBase } from './frame-base'
export interface FrameCodeValue extends FrameBase {
  itemText: string
  itemValue: string
  codeGuid?: string
  parentGuid?: string
  colorStyle?: string
}
