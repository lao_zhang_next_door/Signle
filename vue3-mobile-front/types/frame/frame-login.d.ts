export interface UserLoginType {
  username: string
  password: string
  client_id?: string
  client_secret?: string
  grant_type?: string
  captchaVO?: any
  openId?: string
}

export interface UserType {
  username: string
  password: string
  role: string
  roleId: string
  permissions: string | string[]
}
