import { FrameBase } from './frame-base'
export interface FrameCode extends FrameBase {
  //rowGuid
  codeName: string
  codeMask: string
  sortSq: number
}
export interface FrameCodeName {
  // 代码项名称
  codeName: string
}
