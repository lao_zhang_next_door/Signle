import { FrameBase } from './frame-base'
export interface FrameAttachUpload {
  // 上传文件
  uploadify: Blob
  // 关联表Guid
  formRowGuid: string
}

export interface FrameAttach extends FrameBase {
  // 上传文件关联Guid
  formRowGuid?: string
  // 名称
  attachName?: string
  //文件类型
  contentType?: string
  //文件长度
  contentLength?: number
  // 文件地址或内容
  contentUrl?: string
  //上传人IP
  uploadIp?: string
  //上传人Guid
  uploadUserGuid?: string
}
