export interface WxCpOauth2UserInfo {
  openId: string
  deviceId: string
  userId: string
  userTicket: string
  expiresIn: string
  externalUserId: string
}
/**
 * 企业微信签名信息
 */
export interface WxCpAgentJsapiSignature {
  url: string
  corpid: string
  agentid: number
  timestamp: long
  nonceStr: string
  signature: string
}
/**
 * 微信签名信息
 */
export interface WxJsapiSignature {
  url: string
  appid: string
  timestamp: long
  nonceStr: string
  signature: string
}
