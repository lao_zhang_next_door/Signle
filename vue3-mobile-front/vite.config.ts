import type { UserConfig, ConfigEnv } from 'vite'
import { loadEnv } from 'vite'
import { resolve } from 'path'
// import Components from 'unplugin-vue-components/vite'
// import { VantResolver } from 'unplugin-vue-components/resolvers'
import DefineOptions from 'unplugin-vue-define-options'
import VueI18nPlugin from '@intlify/unplugin-vue-i18n/vite'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import { ViteEjsPlugin } from 'vite-plugin-ejs'
import EslintPlugin from 'vite-plugin-eslint'
// import VueMacros from 'unplugin-vue-macros'
import vue from '@vitejs/plugin-vue'
import viteCompression from 'vite-plugin-compression'
// import topLevelAwait from 'vite-plugin-top-level-await'
import pkg from './package.json'
import moment from 'moment'

const root = process.cwd()

function pathResolve(dir: string) {
  return resolve(root, '.', dir)
}

const { dependencies, devDependencies, name, version } = pkg
const __APP_INFO__ = {
  pkg: { dependencies, devDependencies, name, version },
  lastBuildTime: moment().format('YYYY-MM-DD HH:mm:ss')
}

export default ({ command, mode }: ConfigEnv): UserConfig => {
  let env = {} as any
  const isBuild = command === 'build'
  if (!isBuild) {
    env = loadEnv(process.argv[3] === '--mode' ? process.argv[4] : process.argv[3], root)
  } else {
    env = loadEnv(mode, root)
  }
  return {
    base: env.VITE_BASE_PATH,
    root,
    plugins: [
      vue(),
      DefineOptions.vite(),
      // VueMacros.vite({
      //   plugins: {
      //     vue: vue()
      //   }
      // }),
      // Components({
      //   resolvers: [VantResolver()]
      // }),
      // gzip压缩 生产环境生成 .gz 文件
      viteCompression({
        verbose: true,
        disable: false,
        threshold: 10240,
        algorithm: 'gzip',
        ext: '.gz'
      }),
      VueI18nPlugin({
        runtimeOnly: true,
        compositionOnly: true,
        include: [resolve(__dirname, 'src/locales/**')]
      }),
      createSvgIconsPlugin({
        iconDirs: [pathResolve('src/assets/svgs')],
        symbolId: 'icon-[dir]-[name]',
        svgoOptions: true
      }),
      EslintPlugin({
        cache: false,
        include: ['src/**/*.vue', 'src/**/*.ts', 'src/**/*.tsx'] // 检查的文件
      }),
      ViteEjsPlugin({
        title: env.VITE_APP_TITLE
      })
      // topLevelAwait({
      //   // The export name of top-level await promise for each chunk module
      //   promiseExportName: '__tla',
      //   // The function to generate import names of top-level await promise in each chunk module
      //   promiseImportName: (i) => `__tla_${i}`
      // })
    ],

    css: {
      preprocessorOptions: {}
    },
    resolve: {
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.less', '.css'],
      alias: [
        {
          find: /\@\//,
          replacement: `${pathResolve('src')}/`
        }
      ]
    },
    build: {
      minify: 'terser',
      outDir: env.VITE_OUT_DIR || 'dist',
      sourcemap: env.VITE_SOURCEMAP === 'true' ? 'inline' : false,
      // brotliSize: false,
      terserOptions: {
        compress: {
          drop_debugger: env.VITE_DROP_DEBUGGER === 'true',
          drop_console: env.VITE_DROP_CONSOLE === 'true'
        }
      },
      rollupOptions: {
        input: {
          index: pathResolve('index.html')
        },
        // 静态资源分类打包
        output: {
          chunkFileNames: 'public/js/[name]-[hash].js',
          entryFileNames: 'public/js/[name]-[hash].js',
          assetFileNames: 'public/[ext]/[name]-[hash].[ext]'
        }
      }
    },
    define: {
      __INTLIFY_PROD_DEVTOOLS__: false,
      __APP_INFO__: JSON.stringify(__APP_INFO__)
    },
    //启动服务配置
    server: {
      host: '127.0.0.1',
      port: 9527,
      open: true,
      https: false,
      // headers: {
      //   'Cross-Origin-Opener-Policy': 'same-origin',
      //   'Cross-Origin-Embedder-Policy': 'require-corp'
      // },
      proxy: {
        // '/api': {
        //   target: 'http://localhost:10933/', // 目标接口域名
        //   changeOrigin: true, // 是否跨域
        //   rewrite: (path) => path.replace(/^\/api/, '/')
        // },
        '/wx': {
          target: 'http://localhost:10933/', // 目标接口域名
          changeOrigin: true, // 是否跨域
          rewrite: (path) => path.replace(/^\/wx/, '/wx')
        },
        '/file': {
          target: 'http://localhost:10933/', // 目标接口域名
          changeOrigin: true, // 是否跨域
          rewrite: (path) => path.replace(/^\/file/, '/file')
        }
      }
    },
    optimizeDeps: {
      include: [
        'vant',
        'vue-router',
        'vue-types',
        '@vant/touch-emulator',
        'axios',
        'qs',
        '@vueuse/core',
        '@vueuse/components',
        '@iconify/iconify'
      ]
    }
  }
}
